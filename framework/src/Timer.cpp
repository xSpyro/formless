
#include "Timer.h"


Timer::Timer()
{
	QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
	QueryPerformanceCounter((LARGE_INTEGER*)&start);

	accumulated = 0.0f;
	delta = 0.0f;
}

float Timer::now()
{
	// Get the frequency and save it, it shouldn't change

	// do some stuff that takes up time
	QueryPerformanceCounter((LARGE_INTEGER*)&end);

	//find the time
	float time = (float)(end - start) / (float)freq;

	return time;
}

bool Timer::accumulate(float peak)
{
	float temp = delta;
	delta = now();
	
	float diff = fabs(delta - temp);

	accumulated += diff;

	if (accumulated >= peak)
	{
		accumulated -= peak;
		return true;
	}

	return false;
}

void Timer::restart()
{
	QueryPerformanceCounter((LARGE_INTEGER*)&start);

	accumulated = 0.0f;
}

float Timer::getAccumulated()
{
	return accumulated;
}