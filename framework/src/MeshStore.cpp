
#include "MeshStore.h"

#include "Mesh.h"

MeshStore::MeshStore()
	: log("Meshes")
{
}

void MeshStore::init(Renderer * renderer)
{
	this->renderer = renderer;
}

Mesh * MeshStore::create()
{
	Mesh * mesh = new Mesh(renderer);

	store(mesh);

	return mesh;
}

void MeshStore::release(Mesh * mesh)
{
	if (mesh == NULL)
		return;

	for (MeshMap::iterator i = meshes.begin(); i != meshes.end(); ++i)
	{
		if (i->second == mesh)
		{
			delete i->second;

			meshes.erase(i);
			return;
		}
	}
}

void MeshStore::store(Mesh * mesh)
{
	meshes[meshes.size()] = mesh;
}