
#ifndef MODEL
#define MODEL

#include "Common.h"

#include "Timer.h"
#include "Animation.h"
#include "Matrix.h"

class Mesh;
class Renderable;
class BaseShader;
class ModelData;
class AnimationGroup;

struct Material;

class Model
{
	typedef std::vector < Renderable > RenderableVector; 

public:

	Model();
	Model(Mesh * mesh);
	Model(Renderable renderable);
	Model(const ModelData * data);

	void create(const ModelData * data);

	void add(Renderable renderable);
	void clear();


	void setMaterial(const Material & material);

	void setVisible(bool state);


	Animation getAnimation() const;

	void update();

	const RenderableVector & getRenderables() const;


	const Matrix & getTransform() const;
	Matrix & getTransform();

	const Matrix & getNativeMatrix() const;
	Matrix & getNativeMatrix();
	

	Matrix getInvTransform() const;

	void setSharedMatrix(const Matrix * matrix);
	

	void submit(BaseShader * shader);



	float getRadius() const;


	// animation
	bool play(const std::string & name);
	void stop();
	void resume();
	void setAnimationLoop(bool state);

	void setAnimationSpeed(float speed);

	unsigned int getFrame() const;
	void setFrame(unsigned int frame);

	void setUserdata(void * ud);
	void * getUserdata() const;

	const char * getAnimationName() const;

	void detach();

private:

	const ModelData * parent;

	RenderableVector renderables;

	Matrix transform;
	const Matrix * target_transform;

	// local animation frame
	bool paused;
	bool loop;
	float frame;
	float frame_speed;
	const AnimationGroup * animation;

	void * userdata;
};

#endif