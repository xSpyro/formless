
#ifndef EFFECT
#define EFFECT

#include "Common.h"

class VertexShader;
class PixelShader;
class GeometryShader;
class BaseShader;

class Effect
{
public:

	Effect();
	~Effect();

	void bind(BaseShader * vs, BaseShader * ps, BaseShader * gs);

	void additiveBlendingNoDepth();

	void blendModeSolid();
	void blendModeSolidAlpha();
	void blendModeWireframe();
	void blendModeReverseSolid();
	void blendModeParticle();
	void blendModeParticleSub();
	void blendModeAdd();
	void blendModeDebug();
	void blendModeAlpha();
	void blendModeOverlay();

	void blendModeDepthWithAdd();
	void blendModeDepthWithAlpha();

	void apply();

	VertexShader * getVS();
	PixelShader * getPS();
	GeometryShader *getGS();

private:
	
	VertexShader * vs;
	PixelShader * ps;
	GeometryShader * gs;

	ID3D11BlendState * blend;
	D3D11_BLEND_DESC blend_state;

	ID3D11DepthStencilState * depth;
	D3D11_DEPTH_STENCIL_DESC depth_state;

	ID3D11RasterizerState * raster;
	D3D11_RASTERIZER_DESC raster_state;

};

#endif