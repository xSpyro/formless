
#include "VertexShader.h"

#include "Renderer.h"
#include "ConstantBuffer.h"

VertexShader::VertexShader()
{

}

bool VertexShader::load(Renderer * renderer, File * file, int flags)
{
	this->renderer = renderer;

	bool result = compile(file, "vs_4_0");

	if (!result)
		return false;

	HRESULT hr;

    // create the vertex shader
    hr = renderer->getDevice()->CreateVertexShader(getCompiledChunk(), getCompiledChunkSize(), NULL, &shader);

	if (FAILED(hr))
		return false;

	return true;
}

void VertexShader::apply()
{
	// send cbuffers
	for (BufferMap::iterator i = buffers.begin(); i != buffers.end(); ++i)
	{
		renderer->getContext()->VSSetConstantBuffers(i->first, 1, &i->second);
	}

	renderer->getContext()->VSSetShader(shader, NULL, 0);
}

void VertexShader::unbind(Renderer * renderer)
{
	renderer->getContext()->VSSetShader(NULL, NULL, 0);
}

void VertexShader::setConstantBuffer(int slot, ConstantBuffer * buffer)
{
	buffers[slot] = buffer->getBuffer();
}