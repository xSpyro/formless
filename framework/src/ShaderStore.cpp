
#include "ShaderStore.h"

#include "BaseShader.h"
#include "FileSystem.h"

#include "VertexShader.h"
#include "PixelShader.h"
#include "GeometryShader.h"

#include "Renderer.h"

ShaderStore::ShaderStore()
	: log("Shaders")
{
}

ShaderStore::~ShaderStore()
{
	clear();
}

void ShaderStore::init(Renderer * renderer)
{
	this->renderer = renderer;
}

BaseShader * ShaderStore::get(const std::string & resource, int flags)
{
	BaseShader * shader = find(resource);
	if (shader)
	{
		log.notice("Return \"", resource, "\": ", LOG_OK);
		return shader;
	}

	File file;

	if (FileSystem::get(resource, &file))
	{
		BaseShader * shader = NULL;
		
		if (resource.find(".vs") != std::string::npos)
			shader = new VertexShader;

		else if (resource.find(".ps") != std::string::npos)
			shader = new PixelShader;

		else if (resource.find(".gs") != std::string::npos)
			shader = new GeometryShader;

		else
			return NULL;


		if (shader->load(renderer, &file, flags))
		{
			shader->flags = flags;

			// store texture
			store(resource, shader);

			return shader;
		}
		else
		{
			log.warn("Loaded \"", resource, "\": ", LOG_FAILED);

			// free memory
			delete shader;
		}
	}

	return NULL;
}

BaseShader * ShaderStore::create(const std::string & name, const char * data, unsigned int len)
{
	// see if it exists already
	BaseShader * shader = find(name);
	if (shader)
	{
		log.notice("Return \"", name, "\": ", LOG_OK);
		return shader;
	}

	if (name.find(".vs") != std::string::npos)
		shader = new VertexShader;

	else if (name.find(".ps") != std::string::npos)
		shader = new PixelShader;

	else if (name.find(".gs") != std::string::npos)
		shader = new GeometryShader;

	else
		return NULL;


	File file;
	file.data = const_cast < char* > (data);
	file.size = len;

	if (shader->load(renderer, &file))
	{
		file.data = NULL;

		shader->flags = 0;

		// store texture
		store(name, shader);

		return shader;
	}
	else
	{
		log.warn("Loaded \"", name, "\": ", LOG_FAILED);

		// free memory
		delete shader;
	}	

	file.data = NULL;

	return NULL;
}

void ShaderStore::reload()
{
	for (ShaderMap::iterator i = shaders.begin(); i != shaders.end(); ++i)
	{
		File file;
		if (FileSystem::get(i->first, &file))
		{
			BaseShader * shader = i->second;

			if (shader->load(renderer, &file, i->second->flags))
			{
				log.notice("Loaded \"", i->first, "\": ", LOG_OK);
			}
			else
			{
				log.warn("Loaded \"", i->first, "\": ", LOG_FAILED);
			}
		}
	}
}

BaseShader * ShaderStore::find(const std::string & name) const
{
	ShaderMap::const_iterator i = shaders.find(name);
	if (i != shaders.end())
	{
		return i->second;
	}

	return NULL;
}

void ShaderStore::clear()
{
	for (ShaderMap::iterator i = shaders.begin(); i != shaders.end(); ++i)
	{
		BaseShader * shader = i->second;

		log.notice("Freed \"", i->first, "\": ", LOG_OK);
	}

	shaders.clear();
}

void ShaderStore::store(const std::string & name, BaseShader * shader)
{
	// check for duplicate
	ShaderMap::iterator i = shaders.find(name);
	if (i == shaders.end())
	{
		log.notice("Loaded \"", name, "\": ", LOG_OK);

		renderer->onLoadAsset(name, 2);

		shaders[name] = shader;
	}

}