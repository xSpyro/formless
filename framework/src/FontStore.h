
#ifndef FONT_STORE
#define FONT_STORE

#include "Common.h"

#include "Logger.h"

class Font;
class Renderer;
class FontFreeType;

class FontStore
{
	typedef std::map < std::string, Font* > FontMap;

public:

	FontStore();
	~FontStore();

	void init(Renderer * renderer);

	Font * get(const std::string & resource);

private:

	void clear();

	void store(const std::string & name, Font * font);

	static FontFreeType * engineFreeType();

private:

	Logger log;

	Renderer * renderer;

	FontMap fonts;
};

#endif