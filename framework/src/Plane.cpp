#include "Plane.h"

void Plane::setPlane(Vector3 p0, Vector3 p1, Vector3 p2)
{
	Vector3 v1 = p1 - p0;
	Vector3 v2 = p2 - p0;
	Vector3 n(a, b, c);
	n = v1.cross(v2);
	n.normalize();

	a = n.x;
	b = n.y;
	c = n.z;
	d = -n.dot(p1);
}

void Plane::awayFrom(Vector3 pos, Vector3 p2)
{
	Vector3 n(a, b, c);
	if (n.dot(pos) + d >= 0)
	{
		a = -a;
		b = -b;
		c = -c;
		d = -d;
	}
}