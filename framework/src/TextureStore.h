
#ifndef TEXTURE_STORE
#define TEXTURE_STORE

#include "Common.h"

#include "Logger.h"

class Texture;
class Renderer;

class TextureStore
{
	typedef std::map < std::string, Texture* > TextureMap;

public:

	TextureStore();
	~TextureStore();

	void init(Renderer * renderer);

	Texture * get(const std::string & resource, int flags = 0);
	Texture * create(int width, int height, int flags);

	void free(Texture * texture);

	void reload();

private:

	void clear();

	void store(const std::string & name, Texture * texture);

private:

	Logger log;

	Renderer * renderer;

	TextureMap textures;
};

#endif