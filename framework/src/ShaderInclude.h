
#ifndef SHADER_INCLUDE
#define SHADER_INCLUDE

#include "Common.h"

class ShaderInclude : public ID3D10Include
{

public:

	HRESULT WINAPI Open(D3D10_INCLUDE_TYPE type, LPCSTR fname, LPCVOID parent_data, LPCVOID * data, UINT * bytes);

	HRESULT WINAPI Close(LPCVOID data);

	// singleton
	static ShaderInclude * get()
	{
		static ShaderInclude * include = new ShaderInclude;

		return include;
	}
};

#endif