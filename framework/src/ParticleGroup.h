
#ifndef PARTICLE_GROUP
#define PARTICLE_GROUP

#include "Common.h"

#include "Vector3.h"

class ParticleSystem;

class ParticleGroup
{

	struct Emitter
	{
		std::string name;
		int life;
		
		float spawn_rate;
		float affect_rate;

		std::string type_name;
	};

	typedef std::vector < Emitter > Emitters;

public:

	void addEmitter(const std::string type_n, const std::string & name, int life, float spawn_rate, float affect_rate);

	void emit(ParticleSystem * ps, const Vector3 & position, const Vector3 & normal);

private:

	Emitters emitters;
};

#endif
