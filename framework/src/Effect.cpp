
#include "Effect.h"

#include "Renderer.h"
#include "VertexShader.h"
#include "PixelShader.h"
#include "GeometryShader.h"

#include "Logger.h"

Effect::Effect()
	: vs(NULL), ps(NULL), gs(NULL)
{
	blend = NULL;
	depth = NULL;
	raster = NULL;

	blendModeSolid();
}

void Effect::bind(BaseShader * vs, BaseShader * ps, BaseShader * gs)
{
	this->vs = reinterpret_cast < VertexShader* > (vs);
	this->ps = reinterpret_cast < PixelShader* > (ps);
	this->gs = reinterpret_cast < GeometryShader* > (gs);

	Renderer * renderer = vs->getRenderer();

	HRESULT hr;

	hr = renderer->getDevice()->CreateBlendState(&blend_state, &blend);
	if (FAILED(hr))
	{
		Logger::print().alert("CreateBlendState: ", LOG_FAILED);
		Logger::print().info("Description: ", DXGetErrorDescription(hr));
	}

	hr = renderer->getDevice()->CreateDepthStencilState(&depth_state, &depth);
	if (FAILED(hr))
	{
		Logger::print().alert("CreateDepthStencilState: ", LOG_FAILED);
		Logger::print().info("Description: ", DXGetErrorDescription(hr));
	}

	hr = renderer->getDevice()->CreateRasterizerState(&raster_state, &raster);
	if (FAILED(hr))
	{
		Logger::print().alert("CreateRasterizerState: ", LOG_FAILED);
		Logger::print().info("Description: ", DXGetErrorDescription(hr));
	}
	
}

Effect::~Effect()
{
	if (blend)
		blend->Release();

	if (depth)
		depth->Release();

	if (raster)
		raster->Release();
}

void Effect::additiveBlendingNoDepth()
{

	ZeroMemory(&blend_state, sizeof(blend_state));

	/*
    DepthEnable = TRUE;
    DepthWriteMask = ZERO;

    AlphaToCoverageEnable = FALSE;
    BlendEnable[0] = TRUE;
    SrcBlend = SRC_ALPHA;
    DestBlend = ONE;
    BlendOp = ADD;
    SrcBlendAlpha = ZERO;
    DestBlendAlpha = ZERO;
    BlendOpAlpha = ADD;
    RenderTargetWriteMask[0] = 0x0F;
	*/

	/*BlendEnable = true;
	RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	SrcBlend = D3D11_BLEND_ONE;
	SrcBlendAlpha = D3D11_BLEND_ONE;
	DestBlend = D3D11_BLEND_ONE;
	DestBlendAlpha = D3D11_BLEND_ONE;
	BlendOp = D3D11_BLEND_OP_ADD;
	BlendOpAlpha = D3D11_BLEND_OP_ADD;*/

	blend_state.AlphaToCoverageEnable = false;
	blend_state.RenderTarget[0].BlendEnable = TRUE;
	blend_state.RenderTarget[0].RenderTargetWriteMask = 0x0f;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_ALWAYS;
	depth_state.DepthEnable = true;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_SOLID;
}

void Effect::blendModeSolid()
{
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = false;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;


	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_LESS;
	depth_state.DepthEnable = true;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.FrontCounterClockwise = false;
	raster_state.CullMode = D3D11_CULL_FRONT;
	raster_state.FillMode = D3D11_FILL_SOLID;
}

void Effect::blendModeSolidAlpha()
{
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = true;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;


	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_ALWAYS;
	depth_state.DepthEnable = true;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.FrontCounterClockwise = false;
	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_SOLID;
}

void Effect::blendModeWireframe()
{
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = false;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;


	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_LESS;
	depth_state.DepthEnable = true;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.FrontCounterClockwise = false;
	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_WIREFRAME;
}


void Effect::blendModeReverseSolid()
{
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = false;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;


	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_LESS;
	depth_state.DepthEnable = true;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_SOLID;
}

void Effect::blendModeParticle()
{
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = true;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_LESS;
	depth_state.DepthEnable = true;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_SOLID;
}

void Effect::blendModeParticleSub()
{
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = true;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_REV_SUBTRACT;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_LESS;
	depth_state.DepthEnable = true;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_SOLID;

	/*
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = true;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_LESS;
	depth_state.DepthEnable = true;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_SOLID;
	*/
}

void Effect::blendModeAdd()
{
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = true;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	blend_state.RenderTarget[1].BlendEnable = false;
	blend_state.RenderTarget[1].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[1].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[1].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[1].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[1].DestBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[1].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[1].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_LESS;
	depth_state.DepthEnable = false;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_SOLID;
}

void Effect::blendModeDebug()
{
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = false;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_ALWAYS;
	depth_state.DepthEnable = false;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_SOLID;
}

void Effect::blendModeAlpha()
{
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = true;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_SRC_ALPHA;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_INV_SRC_ALPHA;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_LESS;
	depth_state.DepthEnable = false;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_SOLID;
}

void Effect::blendModeOverlay()
{
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = false;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_LESS;
	depth_state.DepthEnable = false;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_SOLID;
}

void Effect::blendModeDepthWithAdd()
{
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = true;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_ALWAYS;
	depth_state.DepthEnable = true;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_SOLID;
}

void Effect::blendModeDepthWithAlpha()
{
	ZeroMemory(&blend_state, sizeof(blend_state));

	blend_state.RenderTarget[0].BlendEnable = true;
	blend_state.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[0].DestBlend = D3D11_BLEND_ONE; // ZERO
	blend_state.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO; // ZERO
	blend_state.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	blend_state.RenderTarget[1].BlendEnable = true;
	blend_state.RenderTarget[1].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[1].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[1].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[1].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[1].DestBlendAlpha = D3D11_BLEND_ZERO; // ZERO
	blend_state.RenderTarget[1].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[1].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	
	blend_state.RenderTarget[2].BlendEnable = true;
	blend_state.RenderTarget[2].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blend_state.RenderTarget[2].SrcBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[2].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[2].DestBlend = D3D11_BLEND_ONE;
	blend_state.RenderTarget[2].DestBlendAlpha = D3D11_BLEND_ONE;
	blend_state.RenderTarget[2].BlendOp = D3D11_BLEND_OP_ADD;
	blend_state.RenderTarget[2].BlendOpAlpha = D3D11_BLEND_OP_ADD;

	ZeroMemory(&depth_state, sizeof(depth_state));

	depth_state.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
	depth_state.DepthEnable = true;
	depth_state.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

	ZeroMemory(&raster_state, sizeof(raster_state));

	raster_state.CullMode = D3D11_CULL_NONE;
	raster_state.FillMode = D3D11_FILL_SOLID;
}

void Effect::apply()
{
	Renderer * renderer = vs->getRenderer();

	if (vs)
		vs->apply();
	else
		VertexShader::unbind(renderer);

	if (ps)
	{
		float factor[4] = {0, 0, 0, 0};
		renderer->getContext()->OMSetBlendState(blend, factor, 0xffffffff);
		renderer->getContext()->RSSetState(raster);
		renderer->getContext()->OMSetDepthStencilState(depth, 0);

		ps->apply();
	}
	else
		PixelShader::unbind(renderer);

	if (gs)
		gs->apply();
	else
		GeometryShader::unbind(renderer);
}

VertexShader * Effect::getVS()
{
	return vs;
}

PixelShader * Effect::getPS()
{
	return ps;
}

GeometryShader * Effect::getGS()
{
	return gs;
}