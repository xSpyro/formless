
#ifndef MODEL_IMPORT_OBJECT_FORMAT_ENHANCED
#define MODEL_IMPORT_OBJECT_FORMAT_ENHANCED

#include "Common.h"

#include "ModelImportFormat.h"

#include "Vector3.h"
#include "Vector2.h"

class Renderer;
class Mesh;

class Texture;

#include "Material.h"

#include "ModelImportObjectStream.h"
#include "ModelImportObjectGroup.h"

class ModelImportObjectFormatEnhanced : public ModelImportFormat
{
	typedef ModelImportObjectStream Stream;

	typedef std::map < std::string, ModelImportObjectGroup > Groups;

	struct GroupMaterial
	{
		Texture * texture;
		Material material;

		GroupMaterial()
			: texture(NULL)
		{
		}
	};

	typedef std::map < std::string, GroupMaterial > Materials;

public:

	ModelImportObjectFormatEnhanced(Renderer * renderer);

	bool import(File * file, ModelData * model);

private:

	bool parse(Stream & stream);
	
	void parseG(Stream & stream);
	void parseGAsMaterial(Stream & stream);
	void parseGDefault(Stream & stream);

	void parseMaterialLib(std::stringstream & stream);


private:

	Renderer * renderer;

	Groups groups;

	Materials materials;
};

#endif