
#ifndef STAGE
#define STAGE

struct Stage
{
	virtual ~Stage() { }

	virtual void apply() = 0;
	virtual void free() { }

	virtual unsigned int performance() const { return 0; }
};

#endif