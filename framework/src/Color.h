
#ifndef COLOR
#define COLOR

#include "Common.h"

#undef min

struct Color
{
	union
	{
		struct
		{
			float r, g, b, a;
		};

		struct
		{
			float v[4];
		};
	};

	Color();
	Color(float r, float g, float b, float a);
	Color(unsigned char gray); // grayscale

	float & operator [] (unsigned i);
	const float & operator [] (unsigned i) const;

	Color add(float value) const;

	Color mul(float scalar) const;

	Color & add(const Color & rhs);

	Color lerp(const Color & rhs, float delta) const;

	Color min(const Color & rhs) const;

	Color operator*(const Color & color) const;
	Color operator*(float scalar) const;
		  
	Color operator+(const Color & color) const;

	friend std::ostream & operator << (std::ostream & stream, const Color & me);
};

#endif