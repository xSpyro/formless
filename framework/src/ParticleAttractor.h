
#ifndef PARTICLE_ATTRACTOR
#define PARTICLE_ATTRACTOR

#include "Common.h"

#include "Vector3.h"

struct Particle;

class ParticleAttractor
{
public:

	ParticleAttractor();
	ParticleAttractor(const Vector3 & position, float range);

	void setPosition(const Vector3 & position);
	void setRange(float range);

	void apply(Particle * particle);

private:

	Vector3 position;
	float range;
};

#endif