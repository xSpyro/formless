
#include "ConstantBuffer.h"

#include "Logger.h"
#include "Renderer.h"

ConstantBuffer::ConstantBuffer(Renderer * renderer, unsigned int size)
{
	this->renderer = renderer;

	D3D11_BUFFER_DESC desc;
	ZeroMemory(&desc, sizeof(desc));

	desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	desc.ByteWidth = size;
	desc.MiscFlags = 0;

	HRESULT hr = renderer->getDevice()->CreateBuffer(&desc, NULL, &cbuffer);

	if (FAILED(hr))
	{
		Logger::print().alert("ConstantBuffer: ", LOG_FAILED);
		Logger::print().info("Description: ", DXGetErrorDescription(hr));
	}
}

ConstantBuffer::~ConstantBuffer()
{
	cbuffer->Release();
}

void ConstantBuffer::set(void * data, unsigned int size)
{
	HRESULT hr;
	D3D11_MAPPED_SUBRESOURCE ptr;

	hr = renderer->getContext()->Map(cbuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &ptr);

	if (SUCCEEDED(hr))
	{
		std::memcpy(ptr.pData, data, size);

		renderer->getContext()->Unmap(cbuffer, 0);
	}
}

ID3D11Buffer * ConstantBuffer::getBuffer() const
{
	return cbuffer;
}