
#include "Surface.h"

#include "Vector2.h"

void Surface::create(int width, int height, const Color & color)
{
	data.assign(width * height, color);

	this->width = width;
	this->height = height;
}

void Surface::resize(int width, int height, const Color & color)
{
	create(width, height, color);
}

bool Surface::save(std::ostream & f)
{
	f.write((char *)&width, sizeof(int));
	f.write((char * )&height, sizeof(int));
	f.write((char *)data.data(), sizeof(Color) * width * height);
	return true;
}

bool Surface::load(std::istream & f)
{
	f.read((char *)&width, sizeof(int));
	f.read((char *)&height, sizeof(int));
	data.assign(width * height, Color());
	f.read((char *)data.data(), sizeof(Color) * width * height);
	return true;
}

void Surface::clear(const Color & color)
{
	data.assign(width * height, color);
}

void Surface::draw(int x, int y, const Color & color)
{
	set(x, y, color);
}

void Surface::draw(int x, int y, float radius, const Color & color, float fallof)
{
	int half = static_cast < int > (radius);


	for (int iy = 1; iy < half; ++iy)
	{
		int width = static_cast < int > (floor(sqrt((2.0 * radius * iy) - (iy * iy))));

		for (int ix = -width; ix <= width; ++ix)
		{
			float dist = std::pow(ix / radius, 2) + std::pow((radius - iy) / radius, 2);

			add(x + ix, y + half - iy, color.mul((1 - dist * fallof)));
			add(x + ix, y - half + iy, color.mul((1 - dist * fallof)));
		}
	}

	// inner band
	for (int ix = -half; ix <= half; ++ix)
	{
		float dist = std::pow(ix / radius, 2) + std::pow((radius - (half - 1)) / radius, 2);

		add(x + ix, y, color.mul((1 - dist * fallof)));
	}

}

void Surface::drawToValue(int x, int y, float radius, const Color & color)
{
	int half = static_cast < int > (radius);

	for (int iy = 1; iy < half; ++iy)
	{
		int width = static_cast < int > (floor(sqrt((2.0 * radius * iy) - (iy * iy))));

		for (int ix = -width; ix <= width; ++ix)
		{
			set(x + ix, y + half - iy, Color(color.r, 0.0f, 0.0f, 0.0f));
			set(x + ix, y - half + iy, Color(color.r, 0.0f, 0.0f, 0.0f));
		}
	}

	// inner band
	for (int ix = -half; ix <= half; ++ix)
	{
		set(x + ix, y, Color(color.r, 0.0f, 0.0f, 0.0f));
	}

}

void Surface::mapTo(Color * dst) const
{
	if (!data.empty())
	{
		std::memcpy(dst, &data.front(), width * height * sizeof(Color));
	}
}

void Surface::write(Color * src)
{
	if (!data.empty())
	{
		std::memcpy(&data.front(), src, width * height * sizeof(Color));
	}
}

void Surface::blit(unsigned char * src, int width, int height, int flags)
{
	switch ( flags )
	{
	case BLIT_GRAYSCALE_8:
		{
			// each 8 bytes is a pixel
			for (int y = 0; y < height; ++y)
			for (int x = 0; x < width; ++x)
			{
				set(x, y, src[x + y * width]);
			}
		}
		break;
	}
}

void Surface::blit(const Surface & src, int ox, int oy)
{
	for (int y = 0; y < src.height; ++y)
	for (int x = 0; x < src.width; ++x)
	{
		add(x + ox, y + oy, src.at(x, y));
	}
}

void Surface::blitAlpha(const Surface & src, int ox, int oy)
{
	for (int y = 0; y < src.height; ++y)
	for (int x = 0; x < src.width; ++x)
	{
		data[x + ox + (y + oy) * width].a = src.at(x, y).a;
	}
}

const Color & Surface::at(int x, int y) const
{
	return data[x + y * width];
}

Color Surface::sample(float u, float v) const
{
	int x = (int)(u * width);
	int y = (int)(v * height);

	return data[x + y * width];
}

float Surface::sample(float u, float v, float radius) const
{
	int half = static_cast < int > (radius);

	float avg = 0.0f;
	float total = 0.0f;
	int times = 0;

	int x = (int)(u * width);
	int y = (int)(v * height);
	
	std::cout << "Sample at coordinates: " << x << "," << y << std::endl;

	//std::cout << "u " << u << " v " << v << " radius " << radius << " half " << half << std::endl;
	for (int iy = 1; iy < half; ++iy)
	{
		int width = static_cast < int > (floor(sqrt((2.0 * radius * iy) - (iy * iy))));

		//std::cout << "width " << width << std::endl;
		for (int ix = -width; ix <= width; ++ix)
		{
			times++;
			times++;

			total += data[ (x + ix) + (y + half - iy) * width].r;
			total += data[ (x + ix) + (y - half + iy) * width].r;
			
			std::cout << data[ (x + ix) + (y + half - iy) * width] << std::endl;
			std::cout << data[ (x + ix) + (y - half + iy) * width] << std::endl;
			
			//add(x + ix, y + half - iy, color.mul((1 - dist * fallof)));
			//add(x + ix, y - half + iy, color.mul((1 - dist * fallof)));
			//std::cout << "total " << total << std::endl;
		}
	}

	// inner band
	for (int ix = -half; ix <= half; ++ix)
	{
		float dist = std::pow(ix / radius, 2) + std::pow((radius - (half - 1)) / radius, 2);
		times++;
		total += data[ (x + ix) + y * width].r;
	}

	std::cout << "total: " << total << std::endl;
	std::cout << "times: " << times << std::endl;

	avg = (float) total/times;

	std::cout << "avg: " << avg << std::endl;



	return avg;
}

int Surface::getWidth() const
{
	return width;
}

int Surface::getHeight() const
{
	return height;
}

void Surface::blur(float amount)
{

	// make a buffer
	ColorData buffer = data;

	// do a box blur for each pixel
	// 3x3 pass

	float scalar = 1 / 9.0f;

	for (int y = 1; y < height - 1; ++y)
	{
		for (int x = 1; x < width - 1; ++x)
		{
			Color color;

			for (int ix = -1; ix <= 1; ++ix)
			for (int iy = -1; iy <= 1; ++iy)
				color.add(data[(x + ix) + (y + iy) * width]);


			buffer[x + y * width] = buffer[x + y * width].lerp(color.mul(scalar), amount);
		}
	}

	data = buffer;
}

void Surface::blur(int x, int y, float radius, float amount)
{
	int half = static_cast < int > (radius);

	// make a buffer
	ColorData buffer = data;

	// do a box blur for each pixel
	// 3x3 pass
	
	float scalar = 1 / 9.0f;

	//for (int ky = 1; ky < half; ++ky)
	//{
	//	int width = static_cast < int > (floor(sqrt((2.0 * radius * ky) - (ky * ky))));

	//	for (int kx = -width; kx <= width; ++kx)
	//	{
	//		Color color;

	//		for (int ix = -1; ix <= 1; ++ix)
	//		for (int iy = -1; iy <= 1; ++iy)
	//			color.add(data[(kx + ix) + (ky + half - iy) * width]);

	//		for (int ix = -1; ix <= 1; ++ix)
	//		for (int iy = -1; iy <= 1; ++iy)
	//			color.add(data[(kx + ix) + (ky - half + iy) * width]);

	//		buffer[kx + ky * width] = buffer[kx + ky * width].lerp(color.mul(scalar), amount);
	//	}
	//}


	for (int ky = (y - half); ky < (y + half); ++ky)
	{
		for (int kx = (x - half); kx < (x + half); ++kx)
		{
			Color color;

			for (int ix = -1; ix <= 1; ++ix)
			for (int iy = -1; iy <= 1; ++iy)
				color.add(data[(kx + ix) + (ky + iy) * width]);


			buffer[kx + ky * width] = buffer[kx + ky * width].lerp(color.mul(scalar), amount);
		}
	}

	data = buffer;
}

void Surface::desaturate(float amount)
{
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			data[x + y * width] = data[x + y * width].mul(1 - amount);
		}
	}
}

void Surface::spread(float amount)
{
	amount += 1.0f;
	float trim = amount / 4.0f;

	// make a buffer
	ColorData buffer = data;

	for (int y = 1; y < height - 1; ++y)
	{
		for (int x = 1; x < width - 1; ++x)
		{
			/*
			float strength = data[x + y * width].r;

			Vector2 dir(0, 0);

			dir.x += data[(x + 1) + (y + 0) * width].r;
			dir.y += data[(x + 0) + (y + 1) * width].r;
			dir.x -= data[(x - 1) + (y + 0) * width].r;
			dir.y -= data[(x + 0) + (y - 1) * width].r;

			//float len = dir.length();

			//dir.x /= len;
			//dir.y /= len;

			buffer[(x + 1) + (y + 0) * width].r += dir.x * 0.1f;
			buffer[(x - 1) + (y + 0) * width].r -= dir.x * 0.1f;

			buffer[(x + 0) + (y + 1) * width].r += dir.y * 0.1f;
			buffer[(x + 0) + (y - 1) * width].r -= dir.y * 0.1f;
			
			*/

			/*
			Color color(0, 0, 0, 0);// = data[x + y * width];

			color.add(data[(x - 1) + (y - 1) * width].mul(trim));
			color.add(data[(x + 1) + (y - 1) * width].mul(trim));
			color.add(data[(x + 1) + (y + 1) * width].mul(trim));
			color.add(data[(x - 1) + (y + 1) * width].mul(trim));

			buffer[x + y * width] = color;//.min(data[x + y * width].mul(1.1f));

			//buffer[x + y * width].r -= data[x + y * width].r * 0.2f;

			buffer[x + y * width].a = data[x + y * width].a;
			*/
		}
	}

	data = buffer;
}

void Surface::clamp()
{
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			Color & c = data[x + y * width];
			Color newColor;
			newColor.r = std::max(std::min(c.r, 1.0f), 0.0f);
			newColor.g = std::max(std::min(c.g, 1.0f), 0.0f);
			newColor.b = std::max(std::min(c.b, 1.0f), 0.0f);
			newColor.a = std::max(std::min(c.a, 1.0f), 0.0f);


			data[x + y * width] = newColor;
		}
	}
}

void Surface::set(int x, int y, const Color & color)
{
	data[x + y * width] = color;
}

void Surface::add(int x, int y, const Color & color)
{
	data[x + y * width].add(color);
}