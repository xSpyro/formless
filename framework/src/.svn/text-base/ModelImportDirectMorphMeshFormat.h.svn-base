
#ifndef MODEL_IMPORT_DIRECT_MORPH_MEST_FORMAT
#define MODEL_IMPORT_DIRECT_MORPH_MEST_FORMAT

#include "Common.h"

#include "ModelImportFormat.h"
#include "Vector3.h"
#include "Vector2.h"

class Renderer;
class Mesh;
class MeshStream;

class ModelImportDirectMorphMeshFormat : public ModelImportFormat
{
	struct Header
	{
		unsigned int magic;
		unsigned int version;
		unsigned int vertices;
		unsigned int flags;
	};

	struct Stream
	{
		std::stringstream str;
		char * data;
		Mesh * mesh;
		MeshStream * positions;
		MeshStream * normals;
		MeshStream * texcoords;
		bool parse_faces;
		Header * header;
	};

public:

	ModelImportDirectMorphMeshFormat(Renderer * renderer);

	bool import(File * file, ModelData * model);

private:

	bool parse(Stream & stream);
	bool parseBinary(Stream & stream);

	void parseV(Stream & stream);
	void parseN(Stream & stream);
	void parseT(Stream & stream);
	void parseF(Stream & stream);

private:

	Renderer * renderer;
};
#endif