#ifndef OBB_H
#define OBB_H
#include "Vector3.h"
#include "Quaternion.h"
#include "Plane.h"
#include "IntersectResult.h"

union Slab
{
	struct 
	{
		Plane max;
		Plane min;
	};
	Plane p[2];
};

class OBB
{
public:
	OBB(Vector3 dimensions = Vector3(0, 0, 0), Vector3 position = Vector3(0, 0, 0), Quaternion rotation = Quaternion(0, 0, 0, 1));
	~OBB();

	Vector3 getMax() const;
	Vector3 getMin() const;
	Vector3 getPosition() const;
	Quaternion getRotation() const;
	Vector3 getDimensions() const;

	void setPosition(Vector3 pos);
	void setRotation(Quaternion rotation);
	void setDimensions(Vector3 dimension);

	void set(Vector3 dimensions, Vector3 pos, Quaternion rotation, bool setDim = true, bool setPos = true, bool setRot = true);

	Matrix createMatrix();
public:
	union {
		Slab slab[3];
		Plane plane[6];
	};
	

	// Vector3 points[8];
private:
	void updateSlabs();

	Vector3 dimensions;
	Vector3 pos;
	Quaternion rotation;
};

#endif