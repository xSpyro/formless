
#include "Color.h"


Color::Color()
	: r(0), g(0), b(0), a(0)
{
}

Color::Color(float r, float g, float b, float a)
	: r(r), g(g), b(b), a(a)
{
}

Color::Color(unsigned char gray)
	: r(gray / 255.0f), g(gray / 255.0f), b(gray / 255.0f), a(gray / 255.0f)
{
}

float & Color::operator [] (unsigned i)
{
	return v[i];
}

const float & Color::operator [] (unsigned i) const
{
	return v[i];
}

Color Color::add(float value) const
{
	return Color(r + value, g + value, b + value, a + value);
}

Color Color::mul(float scalar) const
{
	return Color(r * scalar, g * scalar, b * scalar, a * scalar);
}

Color & Color::add(const Color & rhs)
{
	r += rhs.r;
	g += rhs.g;
	b += rhs.b;
	a += rhs.a;

	return *this;
}

Color Color::lerp(const Color & rhs, float delta) const
{
	return Color(
		r + (rhs.r - r) * delta,
		g + (rhs.g - g) * delta,
		b + (rhs.b - b) * delta,
		a + (rhs.a - a) * delta);
}

Color Color::min(const Color & rhs) const
{
	return Color(
		r > rhs.r ? rhs.r : r,
		g > rhs.g ? rhs.g : g,
		b > rhs.b ? rhs.b : b,
		a);
}
Color Color::operator*(const Color & color)const
{
	Color c;
	c.r = color.r * r;
	c.g = color.g * g;
	c.b = color.b * b;
	c.a = color.a * a;

	return c;
}
Color Color::operator*(float scalar)const
{
	Color c;
	c.r = scalar * r;
	c.g = scalar * g;
	c.b = scalar * b;
	c.a = scalar * a;

	return c;
}
Color Color::operator+(const Color & color) const
{
	Color c;

	c.r = color.r * r;
	c.g = color.g * g;
	c.b = color.b * b;
	c.a = color.a * a;

	return c;
}
std::ostream & operator << (std::ostream & stream, const Color & me)
{
	return stream << me.r << ", " << me.g << ", " << me.b << ", " << me.a;
}