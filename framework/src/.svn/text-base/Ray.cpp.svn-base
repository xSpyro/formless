
#include "Ray.h"

#include "Sphere.h"
#include "Plane.h"
#include "IntersectResult.h"

bool Ray::intersect(const Plane & plane, IntersectResult * result) const
{
	const Vector3 * plane_normal = reinterpret_cast < const Vector3* > (&plane.a);
	float coherence = plane_normal->dot(direction);

	// if ray and plane are parallell or not facing the same direction
	if (coherence == 0) // only if parallell
	{
		return false;
	}

	float t = -(plane_normal->dot(origin) + plane.d) / coherence;

	// now if t is >= 0 we have a hit
	if (t >= 0)
	{
		result->normal = *plane_normal;
		result->dist = t;

		return true;
	}

	return false;
}

bool Ray::intersect(const Sphere & sphere, IntersectResult * result) const
{
	/*
	Vector3 dist = sphere.origin - origin;

    float B = direction.dot(dist);
    float D = B * B - dist.dot(dist) + sphere.radius * sphere.radius; 

    if (D < 0.0f) 
        return false;

	float temp = sqrt(D);

    float t0 = B - temp; 
    float t1 = B + temp;

    if (t0 > 0.1f && t0 < t1) 
    {
		result->dist = t0;
		result->normal = origin + direction * t0 - sphere.origin;
        return true;
    } 
    if (t1 > 0.1f) 
    {
		result->dist = t1;
		result->normal = origin + direction * t1 - sphere.origin;
        return true;
    }

	return false;
	*/
    //Compute A, B and C coefficients
    float a = direction.dot(direction);

    float b = 2 * direction.dot(origin);
	float c = origin.dot(origin) - (sphere.radius * sphere.radius);

    //Find discriminant
    float disc = b * b - 4 * a * c;
    
    // if discriminant is negative there are no real roots, so return 
    // false as ray misses sphere
    if (disc < 0)
        return false;

    // compute q as described above
    float distSqrt = sqrtf(disc);
    float q;
    if (b < 0)
        q = (-b - distSqrt)/2.0f;
    else
        q = (-b + distSqrt)/2.0f;

    // compute t0 and t1
    float t0 = q / a;
    float t1 = c / q;

    // make sure t0 is smaller than t1
    if (t0 > t1)
    {
        // if t0 is bigger than t1 swap them around
        float temp = t0;
        t0 = t1;
        t1 = temp;
    }

    // if t1 is less than zero, the object is in the ray's negative direction
    // and consequently the ray misses the sphere
    if (t1 < 0)
        return false;

    // if t0 is less than zero, the intersection point is at t1
    if (t0 < 0)
    {
		result->dist = t1;
        return true;
    }
    // else the intersection point is at t0
    else
    {
		result->dist = t0;
        return true;
    }

	
}