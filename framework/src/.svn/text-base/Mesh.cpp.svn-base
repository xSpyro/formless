
#include "Mesh.h"

#include "MeshStream.h"
#include "Renderer.h"
#include "BaseShader.h"

#include "Ray.h"
#include "Vector2.h"
#include "Vector3.h"
#include "IntersectResult.h"

#include "Helpers.h"

Mesh::Mesh(Renderer * renderer)
	: renderer(renderer)
{
	positions = NULL;
	topology = D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	flags = 0;

	layout = NULL;
	index_buffer = NULL;


	first = true;
}

Mesh::~Mesh()
{
	for (StreamMap::const_iterator i = streams.begin(); i != streams.end(); ++i)
	{
		delete i->second;
	}

	streams.clear();

	// buffers are from stream

	if (layout)
		layout->Release();

	if (index_buffer)
		index_buffer->Release();
}

void Mesh::release()
{
	renderer->mesh()->release(this);
}

void Mesh::createSwapMesh(Mesh * m)
{
	swapmesh = m;
	swapme = true;

	flags = MESH_SWAP_RENDER;

	first = true;
}

void Mesh::swap()
{
	swapme = !swapme;
}

void Mesh::appendStreamData(void * data, unsigned int size)
{
}

MeshStream * Mesh::createEmptyStream(const std::string & name, int flags)
{
	StreamMap::iterator i = streams.find(name);
	if (i == streams.end())
	{
		// semantic empty
		MeshStream * stream = new MeshStream(flags);

		streams[name] = stream;

		return stream;
	}

	return NULL;
}

MeshStream * Mesh::createSingleStream(const std::string & semantic, int stride, int flags)
{
	StreamMap::iterator i = streams.find(semantic);
	if (i == streams.end())
	{
		// semantic empty
		MeshStream * stream = new MeshStream(semantic, stride, flags);

		streams[semantic] = stream;

		return stream;
	}

	return NULL;
}

MeshStream * Mesh::createPositionStream(const std::string & semantic, int stride)
{
	positions = createSingleStream(semantic, stride, MESH_STREAM_POSITIONS);

	return positions;
}

MeshStream * Mesh::getStream(const std::string & semantic)
{
	StreamMap::iterator i = streams.find(semantic);
	if (i != streams.end())
	{
		return i->second;
	}

	return NULL;
}

const MeshStream * Mesh::getStream(const std::string & semantic) const
{
	StreamMap::const_iterator i = streams.find(semantic);
	if (i != streams.end())
	{
		return i->second;
	}

	return NULL;
}

MeshStream * Mesh::getFirstStream()
{
	return streams.begin()->second;
}

void Mesh::addIndex(unsigned int index)
{
	indices.push_back(index);
}

unsigned int Mesh::getIndexCount() const
{
	return indices.size();
}

unsigned int Mesh::getVertexCount() const
{
	return streams.begin()->second->getElements();
}

unsigned int Mesh::getMorphCount() const
{
	return streams.begin()->second->getMorphCount();
}

unsigned int Mesh::getMorphVertexCount() const
{
	return streams.begin()->second->getMorphElements();
}

Renderer * Mesh::getRenderer() const
{
	return renderer;
}

void Mesh::submit(BaseShader * shader)
{
	// submit the streams to vertexbuffers

	// if we have a positions stream we grab
	// the min and max positions

	const MeshStream * stream = positions;

	if (stream == NULL)
	{
		const MeshStream * temp = streams.begin()->second;

		stream = temp;
	}

	boundings.clear();


	if (stream)
	{
		int stride = stream->getStride();

		unsigned int start = 0;
		unsigned int n = 0;
		const char * data = stream->get(0);

		for (unsigned int m = 0; m < stream->getMorphCount(); ++m)
		{

			BoundingData bd;

			Vector3 min = Vector3(99999, 99999, 99999);
			Vector3 max = Vector3(-99999, -99999, -99999);

			Vector3 avg;

			float max_dist = 0;

			if (indices.empty())
			{

				n = stream->getMorphElements();

				// find min max and center
				for (unsigned int i = start; i < start + n; ++i)
				{
					//const Vector3 & position = stream[i];
					const Vector3 & position = *(const Vector3 * )&data[i * stride];

					if (position.x < min.x)
						min.x = position.x;
					if (position.y < min.y)
						min.y = position.y;
					if (position.z < min.z)
						min.z = position.z;

					if (position.x > max.x)
						max.x = position.x;
					if (position.y > max.y)
						max.y = position.y;
					if (position.z > max.z)
						max.z = position.z;

					avg += position;
				}

				avg /= static_cast < float > (n);

				// find radius from center
				for (unsigned int i = start; i < start + n; ++i)
				{
					const Vector3 & position = *(const Vector3 * )&data[i * stride];

					float d = (position - avg).lengthNonSqrt();

					if (d > max_dist)
						max_dist = d;
				}

			} // indices
			else
			{
				n = indices.size();

				// find min max and center
				for (unsigned int i = start; i < start + n; ++i)
				{
					//const Vector3 & position = stream[i];
					const Vector3 & position = *(const Vector3 * )&data[(start + indices[i - start]) * stride];

					if (position.x < min.x)
						min.x = position.x;
					if (position.y < min.y)
						min.y = position.y;
					if (position.z < min.z)
						min.z = position.z;

					if (position.x > max.x)
						max.x = position.x;
					if (position.y > max.y)
						max.y = position.y;
					if (position.z > max.z)
						max.z = position.z;

					avg += position;
				}

				avg /= static_cast < float > (n);

				// find radius from center
				for (unsigned int i = start; i < start + n; ++i)
				{
					const Vector3 & position = *(const Vector3 * )&data[(start + indices[i - start]) * stride];

					float d = (position - avg).lengthNonSqrt();

					if (d > max_dist)
						max_dist = d;
				}
			}

			// bounding sphere
			bd.sphere.origin = avg;
			bd.sphere.radius = sqrtf(max_dist);

			// bounding box
			bd.bb[0] = min;
			bd.bb[1] = max;

			boundings.push_back(bd);

			Logger::print().info("Mesh Sphere[", start, "]: ", bd.sphere.origin, " - ", bd.sphere.radius);

			start += n;
		}
	}

	// and prepare the input layouts

	// if we have no renderer just close here
	if (renderer == NULL)
		return;

	desc.clear();
	desc.reserve(streams.size());

	buffers.clear();
	buffers.reserve(streams.size());

	strides.clear();
	strides.reserve(streams.size());

	unsigned int index = 0;

	for (StreamMap::const_iterator i = streams.begin(); i != streams.end(); ++i)
	{
		MeshStream * stream = i->second;
		bool morph_target = (stream->getMorphCount() > 1);

		stream->submit(renderer);



		// the reason we create 2 inputs for each stream is so we can
		// blend between 2 streams in the vertex shader and animate the mesh
		for (int m = 0; m < morph_target + 1; ++m)
		{

			// if we have more than 1 description we loop them
			unsigned int offset = 0;
			for (int d = 0; d < stream->getDescCount(); ++d)
			{
				D3D11_INPUT_ELEMENT_DESC element;
				element.AlignedByteOffset = offset;
				element.Format = getDescFormat(stream->getStride(d));
				element.InputSlot = index + m;
				element.InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
				element.SemanticIndex = m;
				element.SemanticName = stream->getSemantic(d).c_str();
				element.InstanceDataStepRate = 0;

				
				Logger::print()("  * InputElement: ", element.SemanticName, " ", 
					element.InputSlot, " ", element.SemanticIndex, " ", element.AlignedByteOffset);

				desc.push_back(element);

				offset += stream->getStride(d);
			}
		}

		// setup buffers and strides

		buffers.push_back(stream->getBuffer());
		strides.push_back(stream->getStride());

		if (morph_target)
		{
			buffers.push_back(stream->getBuffer());
			strides.push_back(stream->getStride());

			++index;
		}

		++index;
	}
	

	// create the layout
	if (layout == NULL)
	{
		HRESULT hr = renderer->getDevice()->CreateInputLayout(&desc.front(), desc.size(), 
			shader->getCompiledChunk(), shader->getCompiledChunkSize(), &layout);

		if (FAILED(hr))
		{
			Logger::print().alert("InputLayout: ", LOG_FAILED);
			Logger::print().info("Description: ", DXGetErrorDescription(hr));
		}
	}


	// if we have indices, create an index buffer aswell
	if (getIndexCount() > 0)
	{
		D3D11_BUFFER_DESC desc;
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = sizeof(unsigned int) * indices.size();
		desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;
	
		D3D11_SUBRESOURCE_DATA initdata;
		initdata.pSysMem = &indices[0];
		initdata.SysMemPitch = 0;
		initdata.SysMemSlicePitch = 0;

		if (index_buffer)
		{
			index_buffer->Release();
			index_buffer = NULL;
		}

		HRESULT hr = renderer->getDevice()->CreateBuffer(&desc, &initdata, &index_buffer);
	}
}

void Mesh::update()
{
	for (StreamMap::const_iterator i = streams.begin(); i != streams.end(); ++i)
	{
		MeshStream * stream = i->second;

		stream->submit(renderer);
	}
}

void Mesh::clear()
{
	for (StreamMap::iterator i = streams.begin(); i != streams.end(); ++i)
	{
		MeshStream * stream = i->second;

		stream->clear();
	}
}

void Mesh::bind() const
{
	if (flags == MESH_SWAP_RENDER)
	{
		switch ( swapme )
		{
		case true:
			bind(0, 0);
			break;

		case false:
			swapmesh->bind(0, 0);
			break;
		}
	}
	else
	{
		bind(0, 0);
	}
}

void Mesh::bind(int stream1, int stream2) const
{
	std::vector < unsigned int > offsets;

	// set our input layout will be configured for morhp streaming if
	// neccessary
	renderer->getContext()->IASetInputLayout(layout);

	// morph from this
	offsets.assign(buffers.size(), 0);

	unsigned int index = 0;
	for (StreamMap::const_iterator i = streams.begin(); i != streams.end(); ++i)
	{
		const MeshStream * stream = i->second;

		if (stream->getMorphCount() > 1)
		{
			offsets[index] = stream->getMorphOffset(stream1);
			++index;

			offsets[index] = stream->getMorphOffset(stream2);
			++index;
		}
		else
		{
			// 0 offset
			++index;
		}
	}

	renderer->getContext()->IASetVertexBuffers(0, buffers.size(), &buffers.front(), &strides.front(), &offsets.front());

	// index buffer
	if (getIndexCount() > 0)
	{
		renderer->getContext()->IASetIndexBuffer(index_buffer, DXGI_FORMAT_R32_UINT, 0);
	}

	renderer->getContext()->IASetPrimitiveTopology((D3D11_PRIMITIVE_TOPOLOGY)topology);
}

unsigned int Mesh::streamFrom(const Mesh * src, unsigned int in) const
{
	// not multiple stream friendly

	if (in == 0)
		return 0;

	ID3D11DeviceContext * ctx = renderer->getContext();


	ctx->IASetPrimitiveTopology((D3D11_PRIMITIVE_TOPOLOGY)topology);

	const unsigned int offsets[1] = {0};

	ctx->SOSetTargets(1, &buffers.front(), offsets);

	D3D11_QUERY_DESC queryDesc;
	queryDesc.MiscFlags = 0;
	queryDesc.Query = D3D11_QUERY_SO_STATISTICS_STREAM0;
	// Fill out queryDesc structure
	static ID3D11Query * pQuery = NULL;
	static HRESULT res = renderer->getDevice()->CreateQuery(&queryDesc, &pQuery);
	
	
	src->bind();
	

	ctx->Begin(pQuery);
	src->draw(in);
	ctx->End(pQuery);

	UINT queryData[4]; // This data type is different depending on the query type

	while( S_OK != ctx->GetData(pQuery, &queryData, sizeof(UINT) * 4, 0) )
	{
	}

	//pQuery->Release();



	ID3D11Buffer * empty[1] = {NULL};

	ctx->SOSetTargets(1, empty, offsets);

	return queryData[0];
}

void Mesh::streamFromAuto(const Mesh * src) const
{
	// not multiple stream friendly

	renderer->getContext()->IASetPrimitiveTopology((D3D11_PRIMITIVE_TOPOLOGY)topology);

	const unsigned int offsets[1] = {streams.begin()->second->getOffsetSO()};
	const unsigned int empty_offsets[1] = {0};

	renderer->getContext()->SOSetTargets(1, &buffers.front(), offsets);

	src->bind();

	renderer->getContext()->DrawAuto();

	ID3D11Buffer * empty[1] = {NULL};

	renderer->getContext()->SOSetTargets(1, empty, empty_offsets);

	first = false;
}

void Mesh::draw() const
{
	if (flags == MESH_SWAP_RENDER)
	{
		if (!first)
			renderer->getContext()->DrawAuto();
		else
			renderer->getContext()->Draw( 1, 0 );
	}
	else
	{
		if (getIndexCount() > 0)
		{
			renderer->getContext()->DrawIndexed(getIndexCount(), 0, 0);
		}
		else
		{
			if (getMorphCount() > 1) // every mesh has 1 morph as default
			{
				renderer->getContext()->Draw(getMorphVertexCount(), 0);
			}
			else
			{
				renderer->getContext()->Draw(getVertexCount(), 0);
			}
		}
	}
}

void Mesh::draw(unsigned int n) const
{
	renderer->getContext()->Draw(n, 0);
}

void Mesh::setTopology(int type)
{
	topology = type;
}

bool Mesh::pick(const Ray & ray, IntersectResult * result) const
{
	// now we just need to loop the position stream
	const MeshStream * stream = positions;

	int offset = 0;
	int stride = 0;

	// if not found try search for it in the first stream
	// for sub streams
	if (stream == NULL)
	{
		// WIP
		const MeshStream * temp = streams.begin()->second;

		// JUST FOR NOW ASSUME POSITIONS AT 0 offset
		// and stride stream stride
		stream = temp;
	}

	if (stream == NULL)
		return false;

	stride = stream->getStride();

	// assume Vector3 on positions for now

	bool hit = false;
	result->dist = 9999999.9f;

	const char * positions = reinterpret_cast < const char* > (stream->get(0));

	if (indices.empty())
	{
		unsigned int n = stream->getMorphElements();
		for (unsigned int i = 0; i < n; i += 3)
		{
			
			const Vector3 * tri[3] = { (const Vector3*)&positions[(i + 0) * stride], 
									   (const Vector3*)&positions[(i + 1) * stride], 
									   (const Vector3*)&positions[(i + 2) * stride] };

			float u, v, d;
			if (D3DXIntersectTri(
				(const D3DXVECTOR3 *)tri[0],
				(const D3DXVECTOR3 *)tri[1],
				(const D3DXVECTOR3 *)tri[2],
				(const D3DXVECTOR3 *)&ray.origin,
				(const D3DXVECTOR3 *)&ray.direction, 
				&u, &v, &d))
			{
				// we found a hit
				if (d < result->dist)
				{
					result->dist = d;
					result->u = u;
					result->v = v;
				}

				hit = true;
			}
			
		}
	}
	else
	{
		unsigned int hit_index = 0;
		unsigned int n = indices.size();
		for (unsigned int i = 0; i < n; i += 3)
		{
			const Vector3 * tri[3] = { (const Vector3*)&positions[indices[i + 0] * stride], 
									   (const Vector3*)&positions[indices[i + 1] * stride], 
									   (const Vector3*)&positions[indices[i + 2] * stride] };

			float u, v, d;
			if (D3DXIntersectTri(
				(const D3DXVECTOR3 *)tri[0],
				(const D3DXVECTOR3 *)tri[1],
				(const D3DXVECTOR3 *)tri[2],
				(const D3DXVECTOR3 *)&ray.origin,
				(const D3DXVECTOR3 *)&ray.direction, 
				&u, &v, &d))
			{
				// we found a hit
				if (d < result->dist)
				{
					result->dist = d;
					result->u = u;
					result->v = v;

					hit_index = i;
				}

				hit = true;
			}
		}


		// if hit fill uv and normals
		if (hit)
		{
			// assume TEXCOORD for now
			const Vector2 * uv[3] = {
				(const Vector2*)&positions[indices[hit_index + 0] * stride + sizeof(Vector3) * 2],
				(const Vector2*)&positions[indices[hit_index + 1] * stride + sizeof(Vector3) * 2],
				(const Vector2*)&positions[indices[hit_index + 2] * stride + sizeof(Vector3) * 2] };

			// assume NORMAL lies after position floats
			const Vector3 * n[3] = {
				(const Vector3*)&positions[indices[hit_index + 0] * stride + sizeof(Vector3) * 1],
				(const Vector3*)&positions[indices[hit_index + 1] * stride + sizeof(Vector3) * 1],
				(const Vector3*)&positions[indices[hit_index + 2] * stride + sizeof(Vector3) * 1] };

			Vector2 out;
			D3DXVec2BaryCentric((D3DXVECTOR2*)&out, 
				(D3DXVECTOR2*)uv[0], (D3DXVECTOR2*)uv[1], (D3DXVECTOR2*)uv[2], result->u, result->v);

			result->u = out.x;
			result->v = out.y;

			D3DXVec3BaryCentric((D3DXVECTOR3*)&result->normal, 
				(D3DXVECTOR3*)n[0], (D3DXVECTOR3*)n[1], (D3DXVECTOR3*)n[2], result->u, result->v);
		}
	}

	// if we have a hit we must fill in the correct values for the uv coordinates
	// for later

	return hit;
}

bool Mesh::pick2(const Ray & ray, IntersectResult * result, int stream1, int stream2, float delta) const
{
	
	// validate input
	stream1 = std::min(stream1, (int)boundings.size() - 1);
	stream2 = std::min(stream2, (int)boundings.size() - 1);



	// now we just need to loop the position stream
	const MeshStream * stream = positions;

	int offset = 0;
	int stride = 0;

	// if not found try search for it in the first stream
	// for sub streams
	if (stream == NULL)
	{
		// WIP
		const MeshStream * temp = streams.begin()->second;

		// JUST FOR NOW ASSUME POSITIONS AT 0 offset
		// and stride stream stride
		stream = temp;
	}

	if (stream == NULL)
		return false;

	stride = stream->getStride();

	// assume Vector3 on positions for now

	bool hit = false;
	result->dist = 9999999.9f;

	const char * positions = reinterpret_cast < const char* > (stream->get(0));

	if (indices.empty())
	{
		unsigned int offset[2];
		unsigned int n = stream->getMorphElements();

		offset[0] = stream1 * n;
		offset[1] = stream2 * n;

		for (unsigned int i = 0; i < n; i += 3)
		{
			
			const Vector3 * tri1[3] = { (const Vector3*)&positions[(i + 0 + offset[0]) * stride], 
									    (const Vector3*)&positions[(i + 1 + offset[0]) * stride], 
									    (const Vector3*)&positions[(i + 2 + offset[0]) * stride] };

			const Vector3 * tri2[3] = { (const Vector3*)&positions[(i + 0 + offset[1]) * stride], 
									    (const Vector3*)&positions[(i + 1 + offset[1]) * stride], 
									    (const Vector3*)&positions[(i + 2 + offset[1]) * stride] };

			float u, v, d;
			if (D3DXIntersectTri(
				(const D3DXVECTOR3 *)&(tri1[0]->lerp(*tri2[0], delta)),
				(const D3DXVECTOR3 *)&(tri1[1]->lerp(*tri2[1], delta)),
				(const D3DXVECTOR3 *)&(tri1[2]->lerp(*tri2[2], delta)),
				(const D3DXVECTOR3 *)&ray.origin,
				(const D3DXVECTOR3 *)&ray.direction, 
				&u, &v, &d))
			{
				// we found a hit
				if (d < result->dist)
				{
					result->dist = d;
					result->u = u;
					result->v = v;
				}

				hit = true;
			}
			
		}
	}
	else
	{
		unsigned int offset[2];
		unsigned int hit_index = 0;
		unsigned int n = indices.size();

		offset[0] = stream1 * n;
		offset[1] = stream2 * n;

		if (!(stream1 | stream2))
		{
			for (unsigned int i = 0; i < n; i += 3)
			{
				const Vector3 * tri[3] = { (const Vector3*)&positions[indices[i + 0] * stride], 
										   (const Vector3*)&positions[indices[i + 1] * stride], 
										   (const Vector3*)&positions[indices[i + 2] * stride] };

				float u, v, d;
				if (D3DXIntersectTri(
					(const D3DXVECTOR3 *)tri[0],
					(const D3DXVECTOR3 *)tri[1],
					(const D3DXVECTOR3 *)tri[2],
					(const D3DXVECTOR3 *)&ray.origin,
					(const D3DXVECTOR3 *)&ray.direction, 
					&u, &v, &d))
				{
					// we found a hit
					if (d < result->dist)
					{
						result->dist = d;
						result->u = u;
						result->v = v;

						hit_index = i;
					}

					hit = true;
				}
			}
		} // without interpolation of morph targets
		else
		{
			for (unsigned int i = 0; i < n; i += 3)
			{
				const Vector3 * tri1[3] = { (const Vector3*)&positions[(offset[0] + indices[i + 0]) * stride], 
											(const Vector3*)&positions[(offset[0] + indices[i + 1]) * stride], 
											(const Vector3*)&positions[(offset[0] + indices[i + 2]) * stride] };

				const Vector3 * tri2[3] = { (const Vector3*)&positions[(offset[1] + indices[i + 0]) * stride], 
											(const Vector3*)&positions[(offset[1] + indices[i + 1]) * stride], 
											(const Vector3*)&positions[(offset[1] + indices[i + 2]) * stride] };


				float u, v, d;
				if (D3DXIntersectTri(
					(const D3DXVECTOR3 *)&(tri1[0]->lerp(*tri2[0], delta)),
					(const D3DXVECTOR3 *)&(tri1[1]->lerp(*tri2[1], delta)),
					(const D3DXVECTOR3 *)&(tri1[2]->lerp(*tri2[2], delta)),
					(const D3DXVECTOR3 *)&ray.origin,
					(const D3DXVECTOR3 *)&ray.direction, 
					&u, &v, &d))
				{
					// we found a hit
					if (d < result->dist)
					{
						result->dist = d;
						result->u = u;
						result->v = v;
					}

					hit = true;
				}
			}
		}


		// if hit fill uv
		if (hit)
		{
			// assume TEXCOORD for now
			const Vector2 * uv[3] = {
				(const Vector2*)&positions[indices[hit_index + 0] * stride + sizeof(Vector3) * 2],
				(const Vector2*)&positions[indices[hit_index + 1] * stride + sizeof(Vector3) * 2],
				(const Vector2*)&positions[indices[hit_index + 2] * stride + sizeof(Vector3) * 2] };

			// assume NORMAL lies after position floats
			const Vector3 * n[3] = {
				(const Vector3*)&positions[indices[hit_index + 0] * stride + sizeof(Vector3) * 1],
				(const Vector3*)&positions[indices[hit_index + 1] * stride + sizeof(Vector3) * 1],
				(const Vector3*)&positions[indices[hit_index + 2] * stride + sizeof(Vector3) * 1] };

			Vector2 out;
			D3DXVec2BaryCentric((D3DXVECTOR2*)&out, 
				(D3DXVECTOR2*)uv[0], (D3DXVECTOR2*)uv[1], (D3DXVECTOR2*)uv[2], result->u, result->v);

			result->u = out.x;
			result->v = out.y;

			D3DXVec3BaryCentric((D3DXVECTOR3*)&result->normal, 
				(D3DXVECTOR3*)n[0], (D3DXVECTOR3*)n[1], (D3DXVECTOR3*)n[2], result->u, result->v);
		}
	}

	// if we have a hit we must fill in the correct values for the uv coordinates
	// for later

	return hit;
}

Sphere Mesh::getBoundingSphere(int stream1, int stream2, float delta) const
{
	//Sphere out;

	//stream1 = std::min(stream1, (int)boundings.size() - 1);
	//stream2 = std::min(stream2, (int)boundings.size() - 1);

	//out.origin = boundings[stream1].sphere.origin.lerp(boundings[stream2].sphere.origin, delta);
	//out.radius = lerp(boundings[stream1].sphere.radius, boundings[stream2].sphere.radius, delta);

	//return out;

	return boundings[0].sphere;
}

float Mesh::getXZRadius(int stream1, int stream2, float delta) const
{
	stream1 = std::min(stream1, (int)boundings.size() - 1);
	stream2 = std::min(stream2, (int)boundings.size() - 1);

	Vector3 min = boundings[stream1].bb[0].lerp(boundings[stream2].bb[0], delta);
	Vector3 max = boundings[stream1].bb[1].lerp(boundings[stream2].bb[1], delta);

	return sqrt(pow(max.x - min.x, 2) + pow(max.z - min.z, 2)) / 2;
}

DXGI_FORMAT Mesh::getDescFormat(unsigned int stride)
{
	switch ( stride )
	{
	case 4:
		return DXGI_FORMAT_R32_FLOAT;

	case 8:
		return DXGI_FORMAT_R32G32_FLOAT;

	case 12:
		return DXGI_FORMAT_R32G32B32_FLOAT;

	case 16:
		return DXGI_FORMAT_R32G32B32A32_FLOAT;

	default:
		return DXGI_FORMAT_UNKNOWN;
	}
}