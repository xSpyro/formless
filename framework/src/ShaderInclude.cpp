
#include "ShaderInclude.h"

#include "FileSystem.h"

HRESULT WINAPI ShaderInclude::Open(D3D10_INCLUDE_TYPE type, LPCSTR fname, LPCVOID parent_data, LPCVOID * data, UINT * bytes)
{
	File file;
	if (FileSystem::get(fname, &file))
	{
		// we can remove ownership of the data from the file
		// by setting its data to NULL

		*data = file.data;
		*bytes = file.size;

		file.data = NULL;

		return S_OK;
	}

	return S_FALSE;
}

HRESULT WINAPI ShaderInclude::Close(LPCVOID data)
{
	delete[] data;

	return S_OK;
}