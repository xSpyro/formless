
#include "Viewport.h"

#include "Camera.h"
#include "VertexShader.h"
#include "GeometryShader.h"
#include "Renderer.h"

#include "Renderable.h"
#include "Model.h"

#include "ConstantBuffer.h"

Viewport::Viewport(Renderer * renderer, Camera * camera, Texture * target, int flags)
	: renderer(renderer), camera(camera), target(target), flags(flags), sort(0)
{
	visible = true;
}

Effect * Viewport::getEffect()
{
	return &effect;
}

void Viewport::setShaders(BaseShader * vs, BaseShader * gs, BaseShader * ps)
{
	effect.bind(vs, ps, gs);
}

void Viewport::setSorting(int sort)
{
	this->sort = sort;
}

void Viewport::prepare()
{
	camera->update();

	// okay in essence we use a single static constant buffer that we will
	// update with the latest camera position and transformation
	// we do this for each viewport since we might use different cameras

	prepareCamera(renderer, camera);

	// set render target
	renderer->setRenderTarget(target, flags);

	// and apply shader
	effect.apply();
}

void Viewport::setTarget(Texture * target)
{
	this->target = target;
}

Texture * Viewport::getTarget() const
{
	return target;
}

void Viewport::setVisible(bool state)
{
	visible = state;
}

bool Viewport::isVisible() const
{
	return visible;
}

bool Viewport::hasSortOp() const
{
	return sort != 0;
}

void Viewport::prepareCamera(Renderer * renderer, Camera * camera)
{
	getConstantBuffer(renderer)->set(camera->getShaderData(), camera->getShaderDataSize());
	ID3D11Buffer * b = getConstantBuffer(renderer)->getBuffer();

	renderer->getContext()->VSSetConstantBuffers(0, 1, &b);
	renderer->getContext()->GSSetConstantBuffers(0, 1, &b);
}

void Viewport::sortOp(const SortIn & in, SortByCameraOut & result) const
{
	// loop all objects in "in" and add them in result map
	// with the distance to the camera as key

	result.clear();

	for (SortIn::const_iterator i = in.begin(); i != in.end(); ++i)
	{
		const Renderable * r = *i;

		const Model * parent = r->getParent();

		float dist = 0;

		if (parent)
		{
			Vector3 p;
			p.transform(r->getParent()->getTransform());

			dist = (p - camera->getPosition()).lengthNonSqrt();

			result[dist] = r;
		}
		else
		{

		}
	}
}

ConstantBuffer * Viewport::getConstantBuffer(Renderer * renderer)
{
	static ConstantBuffer * cbuffer = new ConstantBuffer(renderer, 512);

	return cbuffer;
}