
#ifndef STATE_CHANGE
#define STATE_CHANGE

#include "Common.h"

#include "Stage.h"

class Texture;
class Renderer;

class StateChange : public Stage
{
public:

	StateChange(Renderer * renderer);

	void apply();
	void free();

	// temporary
	Texture * texture;

private:

	Renderer * renderer;
};

#endif