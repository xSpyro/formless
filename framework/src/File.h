
#ifndef FILE
#define FILE

struct File
{
	char * data;
	unsigned int size;

	File()
		: data(NULL), size(0)
	{
	}

	~File()
	{
		delete[] data;
	}
};

#endif