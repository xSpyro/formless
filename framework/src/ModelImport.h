
#ifndef MODEL_IMPORT
#define MODEL_IMPORT

#include "Common.h"

#include "Logger.h"

class ModelData;
class Renderer;
class ModelImportFormat;

class BaseShader;

class ModelImport
{
	typedef std::map < std::string, ModelImportFormat* > FormatMap;

	typedef std::map < std::string, ModelData* > ModelDataMap;

public:

	ModelImport();

	void init(Renderer * renderer);

	bool import(const std::string & resource, ModelData ** data);

	ModelData * importSafe(const std::string & resource, BaseShader * vs = NULL);

private:

	Logger log;

	FormatMap formats;
	ModelDataMap models;

	Renderer * renderer;



};

#endif