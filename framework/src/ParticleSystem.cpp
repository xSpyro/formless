
#include "ParticleSystem.h"

#include "ParticleType.h"
#include "ParticleDefaultBehavior.h"

void ParticleSystem::init(Renderer * renderer)
{
	this->renderer = renderer;
}

void ParticleSystem::update()
{
	for (ParticleTypes::iterator i = types.begin(); i != types.end(); ++i)
	{
		i->second->update();
	}
}

bool ParticleSystem::createType(const std::string & name, BaseShader * shader)
{
	// check for existing type first
	ParticleTypes::iterator i = types.find(name);
	if (i != types.end())
	{
		// we have old already

		return false;
	}
	else
	{
		ParticleType * pt = new ParticleType;

		pt->create(renderer, shader);

		types[name] = pt;
	}

	return true;
}

bool ParticleSystem::createBehavior(const std::string & name, const ParticleBehavior * behavior)
{
	// check for existing type first
	ParticleBehaviors::iterator i = behaviors.find(name);
	if (i != behaviors.end())
	{
		// we have old already
		// we first clear all particles and load a new set
		kill();

		delete i->second;
		i->second = behavior;
	}
	else
	{
		behaviors[name] = behavior;
	}

	return true;
}

void ParticleSystem::addToGroup(const std::string type_n, const std::string & name, const std::string & behavior, int life, float spawn_rate, float affect_rate)
{
	Groups::iterator i = groups.find(name);

	if (i != groups.end())
	{
		i->second.addEmitter(type_n, behavior, life, spawn_rate, affect_rate);
	}
	else
	{
		ParticleGroup group;

		group.addEmitter(type_n, behavior, life, spawn_rate, affect_rate);

		groups[name] = group;
	}
}

ParticleEmitter * ParticleSystem::spawnEmitter(const std::string & type)
{
	// we use default behavior since we specified none
	static ParticleBehavior * def = new ParticleDefaultBehavior;

	ParticleTypes::iterator i = types.find(type);
	if (i != types.end())
	{
		return i->second->addEmitter(def);
	}

	return NULL;
}

ParticleEmitter * ParticleSystem::spawnEmitter(const std::string & type, const std::string & behavior)
{
	ParticleTypes::iterator i = types.find(type);
	if (i != types.end())
	{
		ParticleBehaviors::iterator b = behaviors.find(behavior);
		if (b != behaviors.end())
		{
			return i->second->addEmitter(b->second);
		}
	}

	return NULL;
}

void ParticleSystem::spawnGroup(const std::string & name, const Vector3 & position, const Vector3 & normal)
{
	Groups::iterator i = groups.find(name);

	if (i != groups.end())
	{
		i->second.emit(this, position, normal);
	}
}

Mesh * ParticleSystem::getParticleMesh(const std::string & type)
{
	ParticleTypes::iterator i = types.find(type);

	if (i != types.end())
	{
		return i->second->getMesh();
	}

	return NULL;
}

bool ParticleSystem::kill()
{
	for (ParticleTypes::iterator i = types.begin(); i != types.end(); ++i)
	{
		i->second->kill();
	}

	return true;
}
void ParticleSystem::killBehavior(const std::string & type, const std::string & behavior)
{
	//ParticleTypes::iterator t = types.find(type);

	//if (t != types.end())
	//{
	//	t->second->kill();
	//}
	kill();


	ParticleBehaviors::iterator b = behaviors.find(behavior);

	if (b != behaviors.end())
	{
		delete b->second;
		b->second = NULL;
		behaviors.erase(b);
	}
}
Renderer * ParticleSystem::getRenderer()
{
	return renderer;
}