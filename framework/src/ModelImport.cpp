
#include "ModelImport.h"

#include "FileSystem.h"
#include "ModelImportObjectFormat.h"
#include "ModelImportDirectDrawMeshFormat.h"
#include "ModelImportDirectMorphMeshFormat.h"
#include "ModelImportObjectFormatEnhanced.h"
#include "ModelData.h"

#include "Renderer.h"


ModelImport::ModelImport()
	: log("ModelImport"), renderer(NULL)
{
}

void ModelImport::init(Renderer * renderer)
{
	this->renderer = renderer;

	// create formats
	formats["obj"] = new ModelImportObjectFormatEnhanced(renderer);
	formats["ddm"] = new ModelImportDirectDrawMeshFormat(renderer);
	formats["dmm"] = new ModelImportDirectMorphMeshFormat(renderer);

	log("Supported Formats");
	for (FormatMap::iterator i = formats.begin(); i != formats.end(); ++i)
		log("[", i->first, "]"); 
}

bool ModelImport::import(const std::string & resource, ModelData ** model)
{
	// find the extension
	std::string ext = "def";
	std::string name = "def";
	unsigned int part = 0;
	
	std::string next = "";

	unsigned int p = resource.rfind(".");
	if (p != std::string::npos)
	{
		ext = resource.substr(p + 1);

		// find the name without the extension
		name = resource.substr(0, p);
	}
	else
	{
		// name is the end
		name = resource;
	}

	// see if there is any numbers in the name
	unsigned int number_start = name.find_last_of("0123456789");
	if (number_start != std::string::npos)
	{
		// format to integer
		std::stringstream format;
		format << name.substr(number_start);
		format >> part;

		// subtract the part id from the name
		name = name.substr(0, number_start);

		// now if we find the model with part id + 1
		// we prepare to load another model

		std::stringstream res;
		res << name << (part + 1) << "." << ext;

		if (FileSystem::exists(res.str()))
		{
			next = res.str();
		}
	}


	log.notice("Model Name: ", name);
	log.notice("Part: ", part);

	// find an importer
	ModelImportFormat * format = NULL;
	FormatMap::iterator i = formats.find(ext);
	if (i != formats.end())
	{
		format = i->second;
	}

	if (format)
	{
		File file;
		if (FileSystem::get(resource, &file))
		{
			if (*model == NULL)
				*model = new ModelData;

			if (format->import(&file, *model))
			{
				log.notice("Loaded \"", resource, "\": ", LOG_OK);

				// see if we have another model to load
				if (next != "")
				{
					return import(next, model);
				}

				renderer->onLoadAsset(name, 0);

				return true;
			}
			else
			{
				log.alert("Loaded \"", resource, "\": ", LOG_FAILED);
			}
		}
	}
	else
	{
		log.warn("Unknown format: ", resource);
	}

	return false;
}

ModelData * ModelImport::importSafe(const std::string & resource, BaseShader * vs)
{
	// check if it exists already
	ModelDataMap::iterator i = models.find(resource);
	if (i != models.end())
	{
		return i->second;
	}

	ModelData * res = NULL;

	if (import(resource, &res))
	{
		models[resource] = res;

		// submit if we have vs
		if (vs)
		{
			res->submit(vs);
		}

		return res;
	}

	return NULL;
}