
#include "AudioSystem.h"

#include "FileSystem.h"

AudioSystem::AudioSystem()
	: log("Audio")
{
	FMOD_RESULT result = FMOD::System_Create(&system);


	log.notice("SystemCreate: ", result != FMOD_OK ? LOG_FAILED : LOG_OK);

	if (result == FMOD_OK)
	{
		result = system->init(48, FMOD_INIT_NORMAL, 0);

		log.notice("InitNormal: ", result != FMOD_OK ? LOG_FAILED : LOG_OK);
	}

	system->set3DSettings(50.0f, 50.0f, 50.0f);

	
	system->set3DNumListeners(1);
	
	FMOD_RESULT res= system->set3DListenerAttributes(0, (FMOD_VECTOR*)&Vector3(0,0,0), NULL, (FMOD_VECTOR*)&Vector3(0,0,1), (FMOD_VECTOR*)&Vector3(0,1,0));
	
	muted = false;
}

AudioSystem::~AudioSystem()
{
	stopAllSounds();
	system->release();
}

int AudioSystem::play(const std::string & resource, float volume, bool loop)
{
	FMOD::Sound * sound;
	
	if(loop)
		sound = query(resource, FMOD_LOOP_NORMAL);
	else
		sound = query(resource);

	if (sound == NULL)
		return -1;

	FMOD::Channel * channel = NULL;
	if(channelIDs.size() < 48)
	{
		system->playSound(FMOD_CHANNEL_FREE, sound, false, &channel);

		if (channel)
		{
			channel->setCallback(&channelCB);
			channel->setVolume(volume);
			//std::cout << "assign callback" << std::endl;

			int index;
			channel->getIndex(&index);
			channelIDs[index] = resource;

			channel->setMute(muted);
			return index;
		}
	}
	else if (channelIDs.size() >= 48)
	{
		int id = isChannelReady();
		if(id != -1)
		{
			system->getChannel(id, &channel);
			system->playSound(FMOD_CHANNEL_REUSE, sound, false, &channel);

			if (channel)
			{
				channel->setCallback(&channelCB);
				channel->setVolume(volume);
				//std::cout << "assign callback" << std::endl;

				int index;
				channel->getIndex(&index);
				channelIDs[index] = resource;

				channel->setMute(muted);
				return index;
			}
		}
	}

	return -1;
}

int AudioSystem::play3D(const std::string & resource, const Vector3 & position, float volume, bool loop, bool notSilent)
{
	FMOD::Sound * sound;

	if(loop)
		sound = query(resource, FMOD_3D | FMOD_3D_LINEARROLLOFF | FMOD_LOOP_NORMAL);
	else
		sound = query(resource, FMOD_3D | FMOD_3D_LINEARROLLOFF);

	if (sound == NULL)
		return -1;


	FMOD::Channel * channel = NULL;
	if(channelIDs.size() < 48)
	{
		system->playSound(FMOD_CHANNEL_FREE, sound, !notSilent, &channel);

		if (channel)
		{
			channel->setCallback(&channelCB);
			if (notSilent)
				channel->setVolume(volume);
			else
				channel->setVolume(0.0f);

			channel->set3DMinMaxDistance(0.0f, 300.0f);
			channel->set3DAttributes((FMOD_VECTOR*)&position, (FMOD_VECTOR*)&Vector3(0));

			int index;
			channel->getIndex(&index);
			channelIDs[index] = resource;

			channel->setMute(muted);

			return index;
		}
	}
	else if (channelIDs.size() >= 48)
	{
		int id = isChannelReady();
		if(id != -1)
		{
			system->getChannel(id, &channel);
			system->playSound(FMOD_CHANNEL_REUSE, sound, !notSilent, &channel);


			if (channel)
			{
				channel->setCallback(&channelCB);
				if (notSilent)
					channel->setVolume(volume);
				else
					channel->setVolume(0.0f);

				channel->set3DMinMaxDistance(0.0f, 300.0f);
				channel->set3DAttributes((const FMOD_VECTOR*)&position, (FMOD_VECTOR*)&Vector3(0));
				//std::cout << "assign callback" << std::endl;

				int index;
				channel->getIndex(&index);
				channelIDs[index] = resource;

				channel->setMute(muted);
				return index;
			}
		}
	}

	return -1;
}

void AudioSystem::stop(const int id)
{
	ChannelIDMap::iterator i = channelIDs.find(id);
	if (i == channelIDs.end())
		return;

	FMOD::Channel* channel;
	system->getChannel(i->first, &channel);
	if(channel)
		channel->stop();
}

void AudioSystem::stop(const std::string & resource)
{
	for(ChannelIDMap::iterator it = channelIDs.begin(); it != channelIDs.end(); it++)
	{
		if(it->second == resource)
		{
			FMOD::Channel* channel = NULL;
			system->getChannel(it->first, &channel);

			if(channel)
				channel->stop();
		}
	}
}

void AudioSystem::stopAllSounds()
{
	FMOD::Channel* channel;

	for(ChannelIDMap::iterator i = channelIDs.begin(); i != channelIDs.end(); ++i)
	{
		system->getChannel(i->first, &channel);
		if(channel)
			channel->stop();
	}
}

void AudioSystem::fadeIn(const int id, const float volume)
{
	ChannelIDMap::iterator i = channelIDs.find(id);
	if (i == channelIDs.end())
		return;

	FMOD::Channel* channel;
	system->getChannel(i->first, &channel);
	if(channel)
		inFades[channel] = volume;
}

void AudioSystem::fadeOut(const int id, const float volume)
{
	ChannelIDMap::iterator i = channelIDs.find(id);
	if (i == channelIDs.end())
		return;

	FMOD::Channel* channel;
	system->getChannel(i->first, &channel);
	if(channel)
		outFades[channel] = volume;
}

void AudioSystem::setListener(const Vector3 & position, const Vector3 & orientation, const Vector3 & up)
{
	listener = position;

	FMOD_VECTOR p;
	p.x = position.x;
	p.y = position.y;
	p.z = position.z;

	FMOD_VECTOR o;
	o.x = orientation.x;
	o.y = orientation.y;
	o.z = orientation.z;

	FMOD_VECTOR u;
	u.x = up.x;
	u.y = up.y;
	u.z = up.z;

	FMOD_VECTOR n;
	n.x = n.y = n.z = 0;

	system->set3DNumListeners(1);
	system->set3DListenerAttributes(0, &p, &n, &o, &u);
}

void AudioSystem::update()
{
	system->update();

	for(std::map<FMOD::Channel*, float>::iterator i = inFades.begin(); i != inFades.end(); ++i)
	{
		float volume;
		i->first->getVolume(&volume);
		if(volume >= i->second)
		{
			inFades.erase(i);
			break;
		}
		else
		{
			unsigned int time;
			i->first->getPosition(&time, FMOD_TIMEUNIT_MS);

			
			i->first->setVolume(volume += 0.01f);
		}
	}
	for(std::map<FMOD::Channel*, float>::iterator i = outFades.begin(); i != outFades.end(); ++i)
	{
		float volume;
		bool playing;
		i->first->getVolume(&volume);
		i->first->isPlaying(&playing);
		if(volume <= 0.0f || !playing)
		{
			i->first->stop();
			outFades.erase(i);
			break;
		}
		else
		{
			
			unsigned int time;
			unsigned int maxTime;
			i->first->getPosition(&time, FMOD_TIMEUNIT_MS);

			FMOD::Sound* sound;

			i->first->getCurrentSound(&sound);

			sound->getLength(&maxTime, FMOD_TIMEUNIT_MS);			
			 
			i->first->setVolume(volume - 0.004f);
		}
	}
}

void AudioSystem::mute(const int id)
{
	ChannelIDMap::iterator i = channelIDs.find(id);
	if (i == channelIDs.end())
		return;

	FMOD::Channel* channel = NULL;
	system->getChannel(i->first, &channel);
	FMOD_RESULT hr;
	if(channel)
		hr = channel->setMute(true);
}

void AudioSystem::unmute(const int id)
{
	ChannelIDMap::iterator i = channelIDs.find(id);
	if (i == channelIDs.end())
		return;

	FMOD::Channel* channel = NULL;
	system->getChannel(i->first, &channel);
	if(channel)
		channel->setMute(false);
}

void AudioSystem::muteAllSounds()
{
	FMOD::Channel* channel;

	for(ChannelIDMap::iterator i = channelIDs.begin(); i != channelIDs.end(); ++i)
	{
		system->getChannel(i->first, &channel);
		if(channel)
			channel->setMute(true);
	}

	muted = true;
}

void AudioSystem::unmuteAllSounds()
{
	FMOD::Channel* channel;

	for(ChannelIDMap::iterator i = channelIDs.begin(); i != channelIDs.end(); ++i)
	{
		system->getChannel(i->first, &channel);
		if(channel)
			channel->setMute(false);
	}

	muted = false;
}

void AudioSystem::setVolume(const int id, float volume)
{
	ChannelIDMap::iterator i = channelIDs.find(id);
	if (i == channelIDs.end())
		return;

	FMOD::Channel* channel;
	system->getChannel(i->first, &channel);
	
	if(channel)
		channel->setVolume(volume);
}

int AudioSystem::getSoundLengthMS(const std::string & resource)
{
	FMOD::Sound * sound = query(resource );

	if (sound == NULL)
		return -1;

	unsigned int length = -1;
	FMOD_RESULT result = sound->getLength(&length, FMOD_TIMEUNIT_MS);

	return length;
}

int	AudioSystem::getSoundLengthMS(const int id)
{
	FMOD::Channel* chan = NULL;
	FMOD::Sound* sound = NULL;

	unsigned int length = 0;
	if (system->getChannel(id, &chan) == FMOD_OK)
	{

		if (chan->getCurrentSound(&sound) == FMOD_OK)
		{
			sound->getLength(&length, FMOD_TIMEUNIT_MS);

			return length;
		}

	}

	return -1;

}

int AudioSystem::getPlayingSoundMS(const int id)
{
	ChannelIDMap::iterator it = channelIDs.find(id);
	unsigned int length = 0;
	if (it != channelIDs.end())
	{
		bool playing;
		FMOD::Channel* chan = NULL;
		system->getChannel(it->first, &chan);

		chan->isPlaying(&playing);

		chan->getPosition(&length, FMOD_TIMEUNIT_MS);
		if (playing)	return length;

		else			return -1;
	}

	return -1;
}

FMOD::System* AudioSystem::getSystem() const
{
	return system;
}

FMOD::Sound * AudioSystem::query(const std::string & resource, int extras)
{
	FMOD_RESULT result;
	FMOD::Sound * sound;

	SoundMap::iterator i = sounds.find(resource);
	if (i == sounds.end())
	{
		
		File file;
		if (FileSystem::get(("sounds/" + resource), &file))
		{
			FMOD_CREATESOUNDEXINFO info;
			ZeroMemory(&info, sizeof(info));
			info.length = file.size;

			//result = system->createSound(file.data, (FMOD_MODE)(FMOD_DEFAULT | FMOD_OPENMEMORY), &info, &sound);
			result = system->createSound((FileSystem::base_dir + "sounds/" + resource).c_str(), FMOD_DEFAULT | extras, NULL, &sound);

			if (result == FMOD_OK)
			{
				sounds[resource] = sound;
				return sound;
			}
		
		}

	}
	else
	{
		return i->second;
	}

	return NULL;
}

FMOD_RESULT F_CALLBACK AudioSystem::channelCB(FMOD_CHANNEL * channel, FMOD_CHANNEL_CALLBACKTYPE type, void * c1, void * c2)
{
	//std::cout << "audio callback: ";

	if(type == FMOD_CHANNEL_CALLBACKTYPE_END)
	{
		//std::cout << "a song has ended." << std::endl;
		
	}
	else
	{
		//std::cout << "unknown callback." << std::endl;
	}

	return FMOD_OK;
}


FMOD::Channel* AudioSystem::getChannel(const int id)
{
	FMOD::Channel* channel = NULL;

	FMOD_RESULT res = system->getChannel(id, &channel);

	if(res == FMOD_OK)
		return channel;
	else
		return NULL;


}

/*
FMOD::Channel * AudioSystem::getFreeChannel()
{
	for (int i = 0; i < 32; ++i)
	{
		bool playing;
		channels[i].isPlaying(&playing);

		if (!playing)
			return &channels[i];
	}

	return NULL;
}
*/


const int AudioSystem::isChannelReady()
{
	FMOD::Channel* channel = NULL;

	for(ChannelIDMap::iterator it = channelIDs.begin(); it != channelIDs.end(); it++)
	{
		channel = NULL;

		FMOD_RESULT res = system->getChannel(it->first, &channel);

		if(res == FMOD_OK)
		{
			bool channelActivity;
			channel->isPlaying(&channelActivity);
			
			if(!channelActivity)
				return it->first;
		}
	}

	return -1;
}

bool AudioSystem::isPlaying(const int id)
{
	FMOD::Channel* chan = NULL;

	system->getChannel(id, &chan);


	bool playing = false;

	chan->isPlaying(&playing);

	return playing;

}

bool AudioSystem::isPlaying(const std::string& song)
{
	for (ChannelIDMap::iterator it = channelIDs.begin(); it != channelIDs.end(); it++)
	{
		if (it->second == song)
		{
			FMOD::Channel* chan = NULL;

			system->getChannel(it->first, &chan);

			bool playing = false;

			chan->isPlaying(&playing);

			return playing;
		}
	}

	return false;
}

const bool AudioSystem::isMuted() const
{
	return muted;
}


void AudioSystem::setPositionForChannel(const int id, const unsigned int position)
{
	FMOD::Channel* channel = getChannel(id);

	channel->setPosition(position, FMOD_TIMEUNIT_MS);
}