
#ifndef FILESYSTEM
#define FILESYSTEM

#include "Common.h"

#include "File.h"

class Logger;

namespace FileSystem
{
	void init();

	void addResource(const std::string & resource);

	bool get(const std::string & src, File * file, bool useBase = true);

	bool exists(const std::string & src, bool useBase = true);

	bool write(const std::string & dst, const File * file, bool useBase = true);

	void setBaseDirectory(const std::string & path);


	typedef void (*Callback)(const std::string & file, void * userdata);
	void iterateUsingCallback(const std::string & folder, Callback callback, void * userdata = NULL);

	Logger & log();


	extern std::string base_dir;
}



#endif