
#ifndef HELPERS
#define HELRERS

#include "Common.h"

float lerp(float a, float b, float l);

float lerp3(float a, float b, float c, float t, float n);

float quadraticBezier(float a, float b, float c, float t);
float random(float from, float to);

#endif