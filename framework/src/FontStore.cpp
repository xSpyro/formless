
#include "FontStore.h"

#include "FontFreeType.h"
#include "FileSystem.h"
#include "Renderer.h"
#include "Texture.h"

FontStore::FontStore()
	: log("Fonts")
{
}

FontStore::~FontStore()
{
}

void FontStore::init(Renderer * renderer)
{
	this->renderer = renderer;
}

Font * FontStore::get(const std::string & resource)
{
	// check for record
	FontMap::iterator i = fonts.find(resource);
	if (i != fonts.end())
	{
		return i->second;
	}

	std::string fname = resource;
	int size = 11;

	// see if we can find a size at the end
	unsigned int p = resource.rfind(":");
	if (p != std::string::npos)
	{
		fname = resource.substr(0, p);
		std::stringstream format;
		format << resource.substr(p + 1);

		format >> size;
	}
	
	File file;
	if (FileSystem::get(fname, &file))
	{
		Font * font = new Font;
		if (engineFreeType()->createFontDataFromFile(&file, font, size))
		{
			// create texture from surface
			font->texture = renderer->texture()->create(
				font->surface.getWidth(), font->surface.getHeight(), TEXTURE_WRITE);

			if (font->texture)
			{
				font->texture->render(font->surface);
				font->texture->save("font.png");
			}
		}
		else
		{
			delete font;
			return NULL;
		}

		// success
		return font;
	}

	return NULL;
}

FontFreeType * FontStore::engineFreeType()
{
	static FontFreeType * freetype = new FontFreeType;

	return freetype;
}