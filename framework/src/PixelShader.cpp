
#include "PixelShader.h"

#include "Renderer.h"
#include "Texture.h"

#include "ConstantBuffer.h"

PixelShader::PixelShader()
{
}

bool PixelShader::load(Renderer * renderer, File * file, int flags)
{
	this->renderer = renderer;

	bool result = compile(file, "ps_4_0");

	if (!result)
		return false;

	HRESULT hr;

    // create the vertex shader
    hr = renderer->getDevice()->CreatePixelShader(getCompiledChunk(), getCompiledChunkSize(), NULL, &shader);

	if (FAILED(hr))
	{
		std::cout << DXGetErrorDescription(hr) << std::endl;

		return false;
	}

	return result;
}

void PixelShader::apply()
{
	// apply bound textures
	for (BindTextures::iterator i = binds.begin(); i != binds.end(); ++i)
	{
		setTexture(i->first, i->second);
	}

	ID3D11Buffer * temp[8] = {0};

	// send cbuffers
	for (BufferMap::iterator i = buffers.begin(); i != buffers.end(); ++i)
	{
		temp[i->first] = i->second;
	}

	renderer->getContext()->PSSetConstantBuffers(0, 8, temp);

	renderer->getContext()->PSSetShader(shader, NULL, 0);
}

void PixelShader::unbind(Renderer * renderer)
{
	renderer->getContext()->OMSetBlendState(NULL, NULL, 0xffffffff);
	renderer->getContext()->RSSetState(NULL);
	renderer->getContext()->OMSetDepthStencilState(NULL, 0);
	renderer->getContext()->PSSetShader(NULL, NULL, 0);
}

void PixelShader::setSamplers()
{
	// BIG MEMORY LEAK MUST BE FIXED

	D3D11_SAMPLER_DESC desc;
	HRESULT hr;

	ID3D11SamplerState * sampler;
	ZeroMemory(&desc, sizeof(desc));

	desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	hr = renderer->getDevice()->CreateSamplerState(&desc, &sampler);

	if (FAILED(hr))
	{
		// fail
	}


	ID3D11SamplerState * cmp_sampler;
	ZeroMemory(&desc, sizeof(desc));

	desc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
	desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	desc.ComparisonFunc = D3D11_COMPARISON_LESS;

	hr = renderer->getDevice()->CreateSamplerState(&desc, &cmp_sampler);

	if (FAILED(hr))
	{
		// fail
		std::cout << "phail" << std::endl;
	}
	

	ID3D11SamplerState * mirror_sampler;
	{
		
		ZeroMemory(&desc, sizeof(desc));

		desc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		desc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
		desc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
		desc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;

		hr = renderer->getDevice()->CreateSamplerState(&desc, &mirror_sampler);

		if (FAILED(hr))
		{
			// fail
		}
	}

	ID3D11SamplerState * const samplers[3] = { sampler, cmp_sampler, mirror_sampler };

	renderer->getContext()->PSSetSamplers(0, 3, samplers);
}

void PixelShader::setTexture(int slot, Texture * texture)
{
	// depending on if the texture is a multi texture
	// we fill more slots
	// if the texture has 5 textures in it we fill
	// slot + 5 slots

	static ID3D11ShaderResourceView * views[16];

	unsigned int num = texture->getTextureCount();
	if (texture->isMultiTexture())
	{
		for (unsigned int i = 0; i < num; ++i)
			views[i] = texture->getView(i);
	}
	else
	{
		views[0] = texture->getView();
	}

	renderer->getContext()->PSSetShaderResources(slot, num, views);
}

void PixelShader::bindTexture(int slot, Texture * texture)
{
	binds[slot] = texture;
}

void PixelShader::setConstantBuffer(int slot, ConstantBuffer * buffer)
{
	buffers[slot] = buffer->getBuffer();
}