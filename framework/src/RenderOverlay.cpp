
#include "RenderOverlay.h"

#include "Renderer.h"
#include "PixelShader.h"

static const char vertex_shader[] = 
"														\
struct OutData											\
{														\
	float4 position : SV_POSITION;						\
	float2 uv : TEXCOORD;								\
};														\
														\
OutData main(in uint vertexId : SV_VertexId)			\
{														\
	OutData output = (OutData)0;						\
														\
	if (vertexId == 0)									\
		output.position = float4(-1, -1, 0, 1);			\
	else if (vertexId == 1)								\
		output.position = float4(1, -1, 0, 1);			\
	else if (vertexId == 2)								\
		output.position = float4(-1, 1, 0, 1);			\
	else if (vertexId == 3)								\
		output.position = float4(1, 1, 0, 1);			\
														\
	output.uv = (output.position.xy + 1) / 2;			\
	output.uv.y = 1 - output.uv.y;						\
														\
	return output;										\
}														\
";

static const char pixel_shader[] = 
"														\
SamplerState MySampler : register(s0);					\
Texture2D MyTexture : register(t0);						\
														\
struct InData											\
{														\
	float4 position : SV_POSITION;						\
	float2 uv : TEXCOORD;								\
};														\
														\
float4 main(InData input) : SV_Target					\
{														\
	return MyTexture.Sample(MySampler, input.uv);		\
}														\
";

RenderOverlay::RenderOverlay(Renderer * renderer, Texture * texture, BaseShader * ps)
	: renderer(renderer), texture(texture)
{
	BaseShader * vs = renderer->shader()->create("ToScreen.vs", vertex_shader, strlen(vertex_shader));

	if (ps == NULL)
		ps = renderer->shader()->create("ToScreen.ps", pixel_shader, strlen(pixel_shader));

	effect.blendModeAlpha();
	//effect.blendModeOverlay();
	effect.bind(vs, ps, NULL);
}


void RenderOverlay::apply()
{
	//renderer->getFrameBufferTexture()->useDepthAddon(false);
	renderer->setRenderTarget(NULL);
	//renderer->getFrameBufferTexture()->useDepthAddon(true);
	effect.apply();

	
	if (texture)
	{
		if (texture->isMultiTexture())
		{
			effect.getPS()->setTexture(0, texture);
		}
		else
		{
			ID3D11ShaderResourceView * view = texture->getView();
			renderer->getContext()->PSSetShaderResources(0, 1, &view);
		}
	}


	
	

	renderer->getContext()->IASetInputLayout(NULL);
	renderer->getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	renderer->getContext()->Draw(4, 0);
}

void RenderOverlay::free()
{
	delete this;
}