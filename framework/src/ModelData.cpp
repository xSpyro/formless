
#include "ModelData.h"

#include "Model.h"
#include "Renderable.h"
#include "Mesh.h"

void ModelData::create(Model * model) const
{
	for (Renderables::const_iterator i = renderables.begin(); i != renderables.end(); ++i)
	{
		model->add(*i);
	}
}

void ModelData::add(Renderable renderable)
{
	renderables.push_back(renderable);
}

void ModelData::overrideTexture(unsigned int renderable, Texture * texture)
{
	renderables.at(renderable).setTexture(texture);
}

bool ModelData::empty() const
{
	return renderables.empty();
}

void ModelData::submit(BaseShader * shader) const
{
	for (Renderables::const_iterator i = renderables.begin(); i != renderables.end(); ++i)
	{
		const Mesh * mesh = i->getMesh();
		
		const_cast < Mesh* > (mesh)->submit(shader);
	}
}

const AnimationGroup * ModelData::getAnimationGroup(const std::string & name) const
{
	Animations::const_iterator i = animations.find(name);

	if (i != animations.end())
	{
		return &i->second;
	}

	return NULL;
}

void ModelData::insertAnimationGroup(const std::string & name, const AnimationGroup & animation)
{
	animations[name] = animation;

	animations[name].setName(name);
}

Mesh * ModelData::getFirstMesh()
{
	return renderables.front().getMesh();
}