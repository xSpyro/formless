
#include "MeshStream.h"

#include "Renderer.h"

MeshStream::MeshStream(int flags)
	: flags(flags)
{
	buffer = NULL;
	offsets.assign(1, 0);

	stride = 0;
	prepend = 0;
}

MeshStream::MeshStream(const std::string & semantic, int stride, int flags)
	: flags(flags)
{
	buffer = NULL;
	offsets.assign(1, 0);

	Desc desc;
	desc.semantic = semantic;
	desc.stride = stride;

	this->stride = stride;
	prepend = 0;

	descs.push_back(desc);
}

MeshStream::~MeshStream()
{
	if (buffer)
		buffer->Release();
}

void MeshStream::addDesc(const std::string & semantic, int stride)
{
	// we must add all streams before we apply our data

	Desc desc;
	desc.semantic = semantic;
	desc.stride = stride;

	this->stride += stride;

	descs.push_back(desc);
}

int MeshStream::getStride() const
{
	return stride;
}

const std::string & MeshStream::getSemantic() const
{
	// kinda deprecated
	return descs.front().semantic;
}

int MeshStream::getDescCount() const
{
	return descs.size();
}

int MeshStream::getStride(int index) const
{
	return descs.at(index).stride;
}

const std::string & MeshStream::getSemantic(int index) const
{
	return descs.at(index).semantic;
}

int MeshStream::find(const std::string & semantic) const
{
	int n = 0;
	for (Descriptions::const_iterator i = descs.begin(); i != descs.end(); ++i, ++n)
	{
		if (i->semantic == semantic)
			return n;
	}

	return -1;
}

ID3D11Buffer * MeshStream::getBuffer() const
{
	return buffer;
}

unsigned int MeshStream::getMorphCount() const
{
	// if this is zero we treat out mesh as a single solid entity i.e. no morphing
	return offsets.size();
}

unsigned int MeshStream::getMorphOffset(unsigned int index) const
{
	index = std::min(index, offsets.size() - 1);

	return offsets.at(index);
}

unsigned int MeshStream::getElements() const
{
	return stream.size() / stride;
}

unsigned int MeshStream::getMorphElements() const
{
	return (stream.size() / stride) / offsets.size();
}

char * MeshStream::get(unsigned int n)
{
	return &stream[n * stride];
}

const char * MeshStream::get(unsigned int n) const
{
	return &stream[n * stride];
}

void MeshStream::fill(unsigned int n)
{
	stream.assign(n * stride, 0);
}

void MeshStream::append(char * data, unsigned int n)
{
	unsigned int bytes = n * stride;
	stream.reserve(stream.size() + bytes);

	for (unsigned int i = 0; i < bytes; ++i)
	{
		stream.push_back(data[i]);
	}
}

void MeshStream::update(char * data, unsigned int index)
{
	char * dst = &stream[index * stride];

	std::memcpy(dst, data, stride);
}

void MeshStream::clear()
{
	stream.clear();
}

void MeshStream::addOffset(unsigned int offset)
{
	// put a byte offsets where we start render the
	// next sequence
	offsets.push_back(offset * stride);
}

void MeshStream::submit(Renderer * renderer)
{
	HRESULT hr;

	if (stream.empty())
		return;

	if (buffer == NULL)
	{
		D3D11_BUFFER_DESC desc;
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.ByteWidth = stream.size();
		desc.BindFlags = D3D11_BIND_VERTEX_BUFFER | D3D11_BIND_STREAM_OUTPUT;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;
	
		D3D11_SUBRESOURCE_DATA initdata;
		initdata.pSysMem = &stream[0];
		initdata.SysMemPitch = 0;
		initdata.SysMemSlicePitch = 0;

		hr = renderer->getDevice()->CreateBuffer(&desc, &initdata, &buffer);
	}
	else
	{
		// only update current data

		renderer->getContext()->UpdateSubresource(buffer, 0, NULL, &stream[0], stream.size(), 0);

		hr = S_OK;
	}

	if (FAILED(hr))
	{
		Logger::print().warn("MeshStream: ", LOG_FAILED);
		Logger::print().info("Description: ", DXGetErrorDescription(hr));
	}
}

void MeshStream::pushAndUpdate(Renderer * renderer, void * data, unsigned int n, unsigned int offset)
{
	if (buffer)
	{
		D3D11_BOX box;
		box.left = offset * stride;
		box.right = box.left + n * stride;
		box.top = 0;
		box.bottom = 1;
		box.front = 0;
		box.back = 1;

		renderer->getContext()->UpdateSubresource(buffer, 0, &box, data, n * stride, 0);
	}
}

void MeshStream::prependBeforeSO(Renderer * renderer, void * data, unsigned int n)
{
	if (buffer)
	{
		D3D11_BOX box;
		box.left = 0;
		box.right = n * stride;
		box.top = 0;
		box.bottom = 1;
		box.front = 0;
		box.back = 1;

		renderer->getContext()->UpdateSubresource(buffer, 0, &box, data, n * stride, 0);

		prepend = n;
	}
}

unsigned int MeshStream::getOffsetSO() const
{
	
	unsigned int out = prepend * stride;
	prepend = 0;

	return out;
}