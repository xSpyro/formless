
#include "ModelImportObjectFormat.h"

#include "ModelData.h"
#include "Renderable.h"
#include "Mesh.h"



ModelImportObjectFormat::ModelImportObjectFormat(Renderer * renderer)
	: renderer(renderer)
{
}

bool ModelImportObjectFormat::import(File * file, ModelData * model)
{
	positions.clear();
	normals.clear();
	texcoords.clear();



	Stream stream;

	stream.str.write(file->data, file->size);

	if (model->empty())
	{
		stream.mesh = new Mesh(renderer);
		Renderable renderable;
		renderable.setMesh(stream.mesh);
		model->add(renderable);

		stream.mesh->createPositionStream("POSITION", sizeof(Vector3));
		stream.mesh->createSingleStream("NORMAL", sizeof(Vector3));
		stream.mesh->createSingleStream("TEXCOORD", sizeof(Vector2));

		stream.append = false;
	}
	else
	{
		// MUST FIX
		stream.mesh = model->getFirstMesh();

		MeshStream * in;

		in = stream.mesh->getStream("POSITION");
		in->addOffset(in->getElements());

		in = stream.mesh->getStream("NORMAL");
		in->addOffset(in->getElements());

		in = stream.mesh->getStream("TEXCOORD");
		in->addOffset(in->getElements());

		stream.append = true;
	}

	

	parse(stream);

	return true;
}

bool ModelImportObjectFormat::parse(Stream & stream)
{
	while ( !stream.str.eof() )
	{
		std::string token;
		stream.str >> token;

		if (token == "v")
		{
			parseV(stream);
		}
		else if (token == "vn")
		{
			parseVN(stream);
		}
		else if (token == "vt")
		{
			parseVT(stream);
		}
		else if (token == "f")
		{
			parseF(stream);
		}
		else
		{
			stream.str.ignore(1000, '\n');
		}

		//std::cout << "token: " << token << std::endl;
	}

	return true;
}

void ModelImportObjectFormat::parseV(Stream & stream)
{
	Vector3 value;
	stream.str >> value.x >> value.y >> value.z;

	positions.push_back(value);

	//std::cout << value.x << ", " << value.y << ", " <<  value.z << std::endl;
}

void ModelImportObjectFormat::parseVN(Stream & stream)
{
	// we cant put the normals directly in the stream since
	// there is less than there is vertices

	Vector3 value;
	stream.str >> value.x >> value.y >> value.z;

	normals.push_back(value);
}

void ModelImportObjectFormat::parseVT(Stream & stream)
{
	Vector2 value;
	stream.str >> value.x >> value.y;

	value.y = 1 - value.y;

	texcoords.push_back(value);
}

void ModelImportObjectFormat::parseF(Stream & stream)
{
	unsigned int index[3];

	MeshStream * vin = stream.mesh->getStream("POSITION");
	MeshStream * vnin = stream.mesh->getStream("NORMAL");
	MeshStream * vtin = stream.mesh->getStream("TEXCOORD");

	//std::cout << "index: ";
	for (int i = 0; i < 3; ++i)
	{
		stream.str >> index[0];
		
		stream.str.ignore(1);

		stream.str >> index[1];

		stream.str.ignore(1);

		stream.str >> index[2];

		if (!stream.append)
		{
			stream.mesh->addIndex(stream.mesh->getIndexCount());
		}

		
		// add the vertex
		vin->append((char *)&positions[index[0] -1], 1);

		// add the normal
		vnin->append((char *)&normals[index[2] -1], 1);

		// add the texcoord
		vtin->append((char *)&texcoords[index[1] -1], 1);


		//std::cout << "normal added: " << normals[temp -1].x << ", " << normals[temp -1].y << ", " << normals[temp -1].z << std::endl;

	}

	//std::cout << std::endl;
}