
#ifndef MESH_STORE
#define MESH_STORE

#include "Common.h"

#include "Logger.h"

class Renderer;
class Mesh;

class MeshStore
{
	typedef std::map < int, Mesh* > MeshMap;

public:

	MeshStore();

	void init(Renderer * renderer);

	Mesh * create();

	void release(Mesh * mesh);

private:

	void store(Mesh * mesh);

private:

	Logger log;

	MeshMap meshes;

	Renderer * renderer;
};

#endif