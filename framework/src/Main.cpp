
#ifndef _LIBRARY

#include "AppMain.h"

#ifdef _DEBUG
int main(int argc, char ** argv)
{
	AppMain app;
	app.run(argc, argv);
}
#else
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	int argc;
	LPWSTR * wargs = CommandLineToArgvW(GetCommandLineW(), &argc);

	// dont mind the allocated cmdlines since its a one timer
	char ** argv = new char * [argc];
	for (int i = 0; i < argc; ++i)
	{
		wchar_t * src = wargs[i];

		int l = wcslen(src);
		char * dst = new char[l + 1];

		for (int c = 0; c < l; ++c)
			dst[c] = (char)src[c];
		dst[l] = 0;

		argv[i] = dst;
	}

	LocalFree(wargs);

	AppMain app;
	return app.run(argc, argv);
}
#endif

#endif