
#ifndef MODEL_IMPORT_FORMAT
#define MODEL_IMPORT_FORMAT

#include "File.h"

class ModelData;

class ModelImportFormat
{
public:

	virtual bool import(File * file, ModelData * model) = 0;
};

#endif