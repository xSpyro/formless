
#ifndef PARTICLE_EMITTER
#define PARTICLE_EMITTER

#include "Common.h"

#include "Timer.h"
#include "ParticleAttractor.h"

class ParticleType;
class ParticleBehavior;
class MeshStream;
struct Particle;

class ParticleEmitter
{
	friend class ParticleType;

	typedef std::vector < ParticleAttractor > Attractors;

	typedef void (*callback)(ParticleEmitter * self, void * userdata);

public:

	ParticleEmitter();
	ParticleEmitter(int start, int end, ParticleType * type, const ParticleBehavior * behavior);
	~ParticleEmitter();

	bool update(MeshStream * stream);

	void bind(callback cb, void * userdata);

	void move(const Vector3 & pos);
	void attach(const Vector3 * pos);
	void detach();

	void setSpawnRate(float rate);
	void setSpawnBox(const Vector3 & box);
	void setLife(int life);
	void setLifeAffectRate(float amount);

	int addAttractor(const Vector3 & rel, float range);
	void attractorMove(int index, const Vector3 & rel);
	void attractorRange(int index, float range);
	void setNormal(const Vector3 & n);

private:

	Particle * getNextDeadParticle();
	
private:

	void * userdata;
	callback onDestroy;

	int start;
	int end;

	Vector3 position;
	const Vector3 * ptr_position;

	ParticleType * type;
	const ParticleBehavior * behavior;
	MeshStream * stream;

	// properties

	Attractors attractors;

	float rate;
	float spawn;

	Vector3 spawn_box;

	int life, life_max;
	float life_affect_rate;

	Vector3 normalW;
};

#endif