
#ifndef CAMERA
#define CAMERA

#include "Common.h"

#include "Vector2.h"
#include "Vector3.h"
#include "Matrix.h"

#include "Ray.h"

#undef near
#undef far

class Camera
{
public:

	Camera();
	virtual ~Camera();
	void setPosition(const Vector3 & vec);
	void setLookAt(const Vector3 & vec);
	void setOrientation(const Vector3 & vec);
	void setUp(const Vector3 & vec);

	void setClipRange(float near, float far);
	Vector2 getClipRange() const;

	void setProjection3D(float fovy, float aspect);
	void setProjection2D(float width, float height);
	void setProjectionOrtho(float l, float r, float b, float t);

	const Vector3 & getPosition() const;
	const Vector3 & getOrientation() const;
	const Vector3 & getUp() const;

	Matrix & getViewProjectionMatrix();
	
	const Matrix & getViewProjectionMatrixNonTransposed() const;

	const Matrix & getViewProjectionMatrix() const;

	
	const Matrix & getInvViewMatrix() const;

	const Matrix & getViewMatrix() const;

	const Matrix & getInvViewProjectionMatrix() const;

	Vector2 projectToScreenUV(const Vector3 & pos);

	// controlling

	void move(float amount);
	void strafe(float amount);
	
	void yaw(float amount);
	void pitch(float amount);
	void roll(float amount);

	bool update();

	void debug();

	bool pick(const Vector2 & offset, Ray * out) const;

	Vector2 project(const Vector3 & position) const;


	void updateTimeFrame(float tf);
	void * getShaderData();
	unsigned int getShaderDataSize();

protected:
	float yaw_angle;
	float pitch_angle;
	float roll_angle;
private:

	float near;
	float far;

	Matrix view;
	Matrix inv_view;
	
	Matrix projection;
	Matrix inv_projection;

	Matrix view_projection;
	Matrix inv_view_projection;

	Matrix view_projection_t;
	Matrix inv_view_t;
	Matrix view_t;
	Matrix inv_view_projection_t;
	Matrix inv_projection_t;
	Vector3 far_frustum[4];

	Vector3 position;
	float timeframe;
	Vector3 orientation;
	Vector3 up;
	Vector3 left;
	//Vector3 temp;

	//float timeframe;

	Matrix projection_t;


	
};

#endif