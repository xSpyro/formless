
#include "ModelImportDirectMorphMeshFormat.h"

#include "ModelData.h"
#include "Renderable.h"
#include "Mesh.h"

#include "Vector3.h"

ModelImportDirectMorphMeshFormat::ModelImportDirectMorphMeshFormat(Renderer * renderer)
	: renderer(renderer)
{
}

bool ModelImportDirectMorphMeshFormat::import(File * file, ModelData * model)
{
	Stream stream;

	// read header
	stream.header = reinterpret_cast < Header* > (file->data);

	std::cout << "magic " << stream.header->magic << std::endl;
	std::cout << "version " << stream.header->version << std::endl;
	std::cout << "vertices " << stream.header->vertices << std::endl;
	std::cout << "flags " << stream.header->flags << std::endl;

	if (stream.header->magic != 1042)
	{
		stream.header->flags = 0;
	}

	
	stream.data = file->data + sizeof(Header);

	if (model->empty())
	{
		stream.mesh = new Mesh(renderer);
		Renderable renderable;
		renderable.setMesh(stream.mesh);
		model->add(renderable);

		stream.positions = stream.mesh->createPositionStream("POSITION", sizeof(Vector3));
		stream.normals = stream.mesh->createSingleStream("NORMAL", sizeof(Vector3));
		stream.texcoords = stream.mesh->createSingleStream("TEXCOORD", sizeof(Vector2));

		stream.parse_faces = true;
	}
	else
	{
		stream.mesh = model->getFirstMesh();

		stream.positions = stream.mesh->getStream("POSITION");
		stream.positions->addOffset(stream.positions->getElements());

		stream.normals = stream.mesh->getStream("NORMAL");
		stream.normals->addOffset(stream.normals->getElements());

		stream.texcoords = stream.mesh->getStream("TEXCOORD");
		stream.texcoords->addOffset(stream.texcoords->getElements());

		stream.parse_faces = false;
	}

	if (stream.header->flags == 0) // binary
	{
		stream.str.write(stream.data, file->size - sizeof(Header));
		parse(stream);
	}
	else
	{
		parseBinary(stream);
	}

	return true;
}

bool ModelImportDirectMorphMeshFormat::parse(Stream & stream)
{
	while ( !stream.str.eof() )
	{
		std::string token;
		stream.str >> token;

		if (token == "v")
		{
			parseV(stream);
		}
		else if (token == "n")
		{
			parseN(stream);
		}
		else if (token == "t")
		{
			parseT(stream);
		}
		else if (stream.parse_faces && token == "f")
		{
			parseF(stream);
		}

		//std::cout << "token: " << token << std::endl;
	}

	return true;
}

bool ModelImportDirectMorphMeshFormat::parseBinary(Stream & stream)
{
	char * token = NULL;

	token = &stream.data[0 * stream.header->vertices * sizeof(Vector3)];
	stream.positions->append(token, stream.header->vertices);

	token = &stream.data[1 * stream.header->vertices * sizeof(Vector3)];
	stream.normals->append(token, stream.header->vertices);

	token = &stream.data[2 * stream.header->vertices * sizeof(Vector3)];
	stream.texcoords->append(token, stream.header->vertices);

	return true;
}

void ModelImportDirectMorphMeshFormat::parseV(Stream & stream)
{
	Vector3 value;
	stream.str >> value.x >> value.y >> value.z;

	stream.positions->append((char *)&value, 1);
}

void ModelImportDirectMorphMeshFormat::parseN(Stream & stream)
{
	Vector3 value;
	stream.str >> value.x >> value.y >> value.z;

	stream.normals->append((char *)&value, 1);
}

void ModelImportDirectMorphMeshFormat::parseT(Stream & stream)
{
	Vector2 value;
	stream.str >> value.x >> value.y;

	stream.texcoords->append((char *)&value, 1);
}

void ModelImportDirectMorphMeshFormat::parseF(Stream & stream)
{
	unsigned int index[3];
	stream.str >> index[0] >> index[1] >> index[2];

	stream.mesh->addIndex(index[0]);
	stream.mesh->addIndex(index[1]);
	stream.mesh->addIndex(index[2]);
}