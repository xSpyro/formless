
#ifndef RENDERER
#define RENDERER

#include "Common.h"

#include "TextureStore.h"
#include "ShaderStore.h"
#include "MeshStore.h"
#include "FontStore.h"

#include "ModelImport.h"

#include "ParticleSystem.h"

#include "Texture.h"

#include "Logger.h"

class Window;
class Layer;
struct Stage;

enum 
{
	RENDERER_ENABLE_WPF = 1
};

class Renderer
{

	typedef std::vector < D3D11_VIEWPORT > Viewports;
	typedef std::vector < ID3D11RenderTargetView* > RenderTargets;

	typedef std::multimap < unsigned int, Stage* > StageMap;

public:

	Renderer();

	void setUserdata(void * ud);

	int getWidth() const;
	int getHeight() const;

	bool join(Window * window, int flags = 0);
	bool join(HWND hwnd, int width, int height, int flags = 0);

	bool switchWindowFullScreenMode();
	bool setFullscreen(bool state);

	const bool getFullscreen() const;

	void render();
	void flush();
	void present(bool vertical_sync = true);

	void clear(Texture * target);

	void setClearColor(const Color & color);
	void setRenderTarget(const Texture * target, int flags = 0);

	void redirectFrameBuffer(const Texture * target);


	Layer * createLayer();

	void insertStage(unsigned int priority, Stage * stage);
	void insertLayer(unsigned int priority, Layer * layer);
	void insertStateChange(unsigned int priority, Texture * texture);
	void insertOverlay(unsigned int priority, Texture * texture, BaseShader * ps);

	void clearStage(unsigned int priority);
	void clearLayers();

	unsigned int getStageCount();
	unsigned int queryPerformance() const;

	ID3D11Device * getDevice();
	ID3D11DeviceContext * getContext();

	Texture * getDepthTexture() const;
	Texture * getFrameBufferTexture();

	void * getSharedWPFSurface() const;

	const D3D_FEATURE_LEVEL getFeatureLevel() const;

	TextureStore * texture();
	ShaderStore * shader();
	MeshStore * mesh();
	FontStore * font();

	ModelImport * modelImport();

	ParticleSystem * ps();

	void onResizeWindow(const unsigned int width, const unsigned int height);

	
	void getResolutions(std::vector<DXGI_MODE_DESC>& resolutions);

public:

	virtual void onLoadAsset(const std::string & name, int type) { }

	void * getUserdata();

private:

	void setViewport(int x, int y, int width, int height);

	void beginRenderSequence();
	void endRenderSequence(ID3D11DepthStencilView * view);

	void addViewport(int x, int y, int width, int height);
	void addRenderTarget(ID3D11RenderTargetView * target);

	bool createDeviceAndSwapChain(HWND hwnd);

	bool createDevice9Ex(HWND hwnd);

private:

	Logger log;
	void * userdata;

	// directx9ex device for wpf
	IDirect3DDevice9Ex * device9ex;
	ID3D11Texture2D * shared_surface;
	//HANDLE shared_handle;

	// directx10.1 device for direct2d
	// NOT YET IMPLEMENTED

	// directx 11 main platform
	ID3D11Device * device;
	ID3D11DeviceContext * context;
	IDXGISwapChain * swap_chain;

	ID3D11RenderTargetView * rtv_screen;

	int width;
	int height;
	int flags;

	Color clear_color;

	IDXGISurface * dxgi_surface;
	ID2D1Factory * d2_factory;
	IDWriteFactory * d2_write;
	ID2D1RenderTarget * d2_rt;

	Texture * depth_texture;
	Texture * offscreen_texture;

	// screen will bind to our rtv_screen and depth buffer
	Texture screen;

	// by default our framebuffer points to screen
	const Texture * framebuffer;



	// stores
	TextureStore texture_store;
	ShaderStore shader_store;
	MeshStore mesh_store;
	FontStore font_store;

	// importers
	ModelImport model_import;

	ParticleSystem particle_system;

	// layers
	StageMap stages;

	// viewports
	Viewports viewports;

	// rendertargets
	RenderTargets rendertargets;

	D3D_FEATURE_LEVEL featureLevel;
};

#endif
