
#ifndef PARTICLE_BEHAVIOR
#define PARTICLE_BEHAVIOR

#include "Common.h"

#include "Particle.h"

class ParticleBehavior
{
public:

	virtual void spawn(Particle & particle) const = 0;
	virtual void process(Particle & particle) const = 0;

	virtual void setNormal(const Vector3 & n) const {}

};

#endif