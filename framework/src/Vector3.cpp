
#include "Vector3.h"

#include "Helpers.h"

Vector3::Vector3()
	: x(0), y(0), z(0)
{
}

Vector3::Vector3(float v)
	: x(v), y(v), z(v)
{
}

Vector3::Vector3(float x, float y, float z)
	: x(x), y(y), z(z)
{
}

Vector3 Vector3::operator + (const Vector3 & rhs) const
{
	return Vector3(x + rhs.x, y + rhs.y, z + rhs.z);
}

Vector3 Vector3::operator - (const Vector3 & rhs) const
{
	return Vector3(x - rhs.x, y - rhs.y, z - rhs.z);
}

Vector3 Vector3::operator * (const Vector3 & rhs) const
{
	return Vector3(x * rhs.x, y * rhs.y, z * rhs.z);
}

Vector3 Vector3::operator + (float offset) const
{
	return Vector3(x + offset, y + offset, z + offset);
}

Vector3 Vector3::operator - (float offset) const
{
	return Vector3(x - offset, y - offset, z - offset);
}

Vector3 Vector3::operator / (float divider) const
{
	return Vector3(x / divider, y / divider, z / divider);
}

Vector3 Vector3::operator * (float scalar) const
{
	return Vector3(x * scalar, y * scalar, z * scalar);
}

Vector3 & Vector3::operator += (const Vector3 & rhs)
{
	x += rhs.x;
	y += rhs.y;
	z += rhs.z;

	return *this;
}

Vector3 & Vector3::operator -= (const Vector3 & rhs)
{
	x -= rhs.x;
	y -= rhs.y;
	z -= rhs.z;

	return *this;
}

Vector3 & Vector3::operator /= (float divider)
{
	x /= divider;
	y /= divider;
	z /= divider;

	return *this;
}

bool Vector3::operator != (const Vector3 &rhs) const
{
	return !(rhs == *this);
}

bool Vector3::operator == (const Vector3 &rhs) const
{
	return (x == rhs.x && y == rhs.y && z == rhs.z);
}


void Vector3::normalize()
{
	float m = sqrt(pow(x, 2) + pow(y, 2) + powf(z, 2));

	x /= m;
	y /= m;
	z /= m;
}

Vector3 Vector3::cross(const Vector3 & rhs) const
{
	Vector3 out;
	D3DXVec3Cross((D3DXVECTOR3*)&out, (D3DXVECTOR3*)this, (D3DXVECTOR3*)&rhs);

	return out;
}

float Vector3::dot(const Vector3 & rhs) const
{
	return x * rhs.x + y * rhs.y + z * rhs.z;
}

void Vector3::transform(const Matrix & mat)
{
	D3DXVec3TransformCoord((D3DXVECTOR3*)this, (D3DXVECTOR3*)this, (D3DXMATRIX*)&mat);
}

void Vector3::transformNormal(const Matrix & mat)
{
	D3DXVec3TransformNormal((D3DXVECTOR3*)this, (D3DXVECTOR3*)this, (D3DXMATRIX*)&mat);
}

float Vector3::lengthNonSqrt() const
{
	return pow(x, 2) + pow(y, 2) + pow(z, 2);
}

float Vector3::length() const
{
	return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
}

Vector3 Vector3::lerp(const Vector3 & rhs, float delta) const
{
	return Vector3(x + (rhs.x - x) * delta, y + (rhs.y - y) * delta, z + (rhs.z - z) * delta);
}

Vector3 Vector3::random(float x, float y, float z)
{
	return Vector3(::random(-x, x), ::random(-y, y), ::random(-z, z));
}

std::ostream & operator << (std::ostream & stream, const Vector3 & me)
{
	return stream << me.x << ", " << me.y << ", " << me.z;
}