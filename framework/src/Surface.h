
#ifndef SURFACE
#define SURFACE

#include "Common.h"

#include "Color.h"

enum
{
	BLIT_GRAYSCALE_8
};

class Surface
{
	typedef std::vector < Color > ColorData;

public:

	void create(int width, int height, const Color & color = Color(0, 0, 0, 1));
	void resize(int width, int height, const Color & color = Color(0, 0, 0, 1));

	bool save(std::ostream & stream);
	bool load(std::istream & stream);

	void clear(const Color & color);

	void draw(int x, int y, const Color & color);
	void draw(int x, int y, float radius, const Color & color, float fallof = 0.0f);
	void drawToValue(int x, int y, float radius, const Color & color);

	void mapTo(Color * dst) const;
	void write(Color * src);

	void blit(unsigned char * src, int width, int height, int flags);
	void blit(const Surface & src, int ox, int oy);
	void blitAlpha(const Surface & src, int ox, int oy);

	const Color & at(int x, int y) const;
	Color sample(float u, float v) const;
	float sample(float u, float v, float radius) const;

	int getWidth() const;
	int getHeight() const;

	// filters
	void blur(float amount = 1.0f);
	void blur(int x, int y, float radius, float amount = 1.0f);
	void desaturate(float amount);
	void spread(float amount = 1.0f);

	void clamp();

private:

	void set(int x, int y, const Color & color);
	void add(int x, int y, const Color & color);

private:

	int width;
	int height;

	ColorData data;
};

#endif