
#ifndef RESOURCE
#define RESOURCE

class Resource
{
public:

	Resource();
	virtual ~Resource();

private:

	int refcount;
};

#endif