
#ifndef RENDER_OVERLAY
#define RENDER_OVERLAY

#include "Common.h"

#include "Stage.h"
#include "Effect.h"
#include "Renderable.h"


class Texture;
class Renderer;

class BaseShader;

class RenderOverlay : public Stage
{
public:

	RenderOverlay(Renderer * renderer, Texture * texture, BaseShader * ps = NULL);

	void apply();
	void free();


private:

	Texture * texture;

	Effect effect;
	Renderable renderable;

	Renderer * renderer;
};

#endif