
#include "Quaternion.h"

#include "Vector3.h"
#include "Matrix.h"

Quaternion::Quaternion()
	: x(0), y(0), z(0), w(1)
{
}

Quaternion::Quaternion(float v)
	: x(v), y(v), z(v), w(v)
{
}

Quaternion::Quaternion(float x, float y, float z, float w)
	: x(x), y(y), z(z), w(w)
{
}

Quaternion & Quaternion::operator += (const Quaternion & rhs)
{
	D3DXQUATERNION q1(x, y, z, w);
	D3DXQUATERNION q2(rhs.x, rhs.y, rhs.z, rhs.w);

	q1 += q2;

	x = q1.x;
	y = q1.y;
	z = q1.z;
	w = q1.w;

	return *this;
}

Quaternion & Quaternion::operator -= (const Quaternion & rhs)
{
	D3DXQUATERNION q1(x, y, z, w);
	D3DXQUATERNION q2(rhs.x, rhs.y, rhs.z, rhs.w);

	q1 -= q2;

	x = q1.x;
	y = q1.y;
	z = q1.z;
	w = q1.w;

	return *this;
}

Quaternion & Quaternion::operator *= (const Quaternion & rhs)
{
	D3DXQUATERNION q1(x, y, z, w);
	D3DXQUATERNION q2(rhs.x, rhs.y, rhs.z, rhs.w);

	q1 *= q2;

	x = q1.x;
	y = q1.y;
	z = q1.z;
	w = q1.w;

	return *this;
}

Quaternion & Quaternion::operator *= (float scalar)
{
	D3DXQUATERNION q1(x, y, z, w);

	q1 *= scalar;

	x = q1.x;
	y = q1.y;
	z = q1.z;
	w = q1.w;

	return *this;
}

Quaternion & Quaternion::operator /= (float divider)
{
	D3DXQUATERNION q1(x, y, z, w);

	q1 *= divider;

	x = q1.x;
	y = q1.y;
	z = q1.z;
	w = q1.w;

	return *this;
}

Quaternion Quaternion::operator + (const Quaternion & rhs) const
{
	D3DXQUATERNION q1(x, y, z, w);
	D3DXQUATERNION q2(rhs.x, rhs.y, rhs.z, rhs.w);

	q1 = q1 + q2;

	return Quaternion(q1.x, q1.y, q1.z, q1.w);
}

Quaternion Quaternion::operator - (const Quaternion & rhs) const
{
	D3DXQUATERNION q1(x, y, z, w);
	D3DXQUATERNION q2(rhs.x, rhs.y, rhs.z, rhs.w);

	q1 = q1 - q2;

	return Quaternion(q1.x, q1.y, q1.z, q1.w);
}

Quaternion Quaternion::operator * (const Quaternion & rhs) const
{
	D3DXQUATERNION q1(x, y, z, w);
	D3DXQUATERNION q2(rhs.x, rhs.y, rhs.z, rhs.w);

	q1 = q1 * q2;

	return Quaternion(q1.x, q1.y, q1.z, q1.w);
}

Quaternion Quaternion::operator * (float scalar) const
{
	D3DXQUATERNION q1(x, y, z, w);

	q1 = q1 * scalar;

	return Quaternion(q1.x, q1.y, q1.z, q1.w);
}

Quaternion Quaternion::operator / (float divider) const
{
	D3DXQUATERNION q1(x, y, z, w);

	q1 = q1 / divider;

	return Quaternion(q1.x, q1.y, q1.z, q1.w);
}

Quaternion Quaternion::operator = (const Quaternion & rhs)
{
	identity();

	x = rhs.x;
	y = rhs.y;
	z = rhs.z;
	w = rhs.w;

	return *this;
}

float Quaternion::dot(const Quaternion & rhs) const
{
	return D3DXQuaternionDot((D3DXQUATERNION*)this, (D3DXQUATERNION*)&rhs);
}

void Quaternion::identity()
{
	D3DXQuaternionIdentity((D3DXQUATERNION*)this);
}

Quaternion Quaternion::inverseOf() const
{
	Quaternion q;
	D3DXQuaternionInverse((D3DXQUATERNION*)&q, (D3DXQUATERNION*)this);
	
	return q;
}

void Quaternion::invert()
{
	D3DXQuaternionInverse((D3DXQUATERNION*)this, (D3DXQUATERNION*)this);
}

float Quaternion::length() const
{
	return D3DXQuaternionLength((D3DXQUATERNION*)this);
}

float Quaternion::lengthNonSqrt() const
{
	return D3DXQuaternionLengthSq((D3DXQUATERNION*)this);
}

void Quaternion::multiply(const Quaternion & rhs)
{
	D3DXQuaternionMultiply((D3DXQUATERNION*)this, (D3DXQUATERNION*)this, (D3DXQUATERNION*)&rhs);
}

void Quaternion::normalize()
{
	D3DXQuaternionNormalize((D3DXQUATERNION*)this, (D3DXQUATERNION*)this);
}

void Quaternion::rotationAxis(Vector3 axis, float angle)
{
	axis.normalize();
	normalize();
	D3DXQuaternionRotationAxis((D3DXQUATERNION*)this, (D3DXVECTOR3*)&axis, angle);
}

void Quaternion::rotationMatrix(Matrix matrix)
{
	normalize();
	D3DXQuaternionRotationMatrix((D3DXQUATERNION*)this, (D3DXMATRIX*)&matrix);
}

void Quaternion::rotationYawPitchRoll(float yaw, float pitch, float roll)
{
	D3DXQuaternionRotationYawPitchRoll((D3DXQUATERNION*)this, yaw, pitch, roll);
}

void Quaternion::rotationAxisApply(Vector3 axis, float angle)
{
	Quaternion q1;
	Quaternion q2(x, y, z, w);
	q1.rotationAxis(axis, angle);

	q2 = q1 * q2;

	x = q2.x;
	y = q2.y;
	z = q2.z;
	w = q2.w;

	normalize();
}

void Quaternion::rotationMatrixApply(Matrix matrix)
{
	Quaternion q1;
	Quaternion q2(x, y, z, w);
	q1.rotationMatrix(matrix);

	q2 = q1 * q2;

	x = q2.x;
	y = q2.y;
	z = q2.z;
	w = q2.w;

	normalize();
}

void Quaternion::rotationYawPitchRollApply(float yaw, float pitch, float roll)
{
	Quaternion q1;
	Quaternion q2(x, y, z, w);
	q1.rotationYawPitchRoll(yaw, pitch, roll);

	q2 = q1 * q2;

	x = q2.x;
	y = q2.y;
	z = q2.z;
	w = q2.w;

	normalize();
}

void Quaternion::slerp(const Quaternion & rhs, float t)
{
	D3DXQuaternionSlerp((D3DXQUATERNION*)this, (D3DXQUATERNION*)this, (D3DXQUATERNION*)&rhs, t);
}

void Quaternion::getAxisAngle(Vector3 & axis, float & angle) const
{
	D3DXQuaternionToAxisAngle((D3DXQUATERNION*)this, (D3DXVECTOR3*)&axis, &angle);
}

void Quaternion::setAxisAngle(Vector3 axis, float angle)
{
	identity();
	rotationAxis(axis, angle);
}

std::ostream & operator << (std::ostream & stream, const Quaternion & self)
{
	return stream << self.x << ", " << self.y << ", " << self.z << ", " << self.w;
}