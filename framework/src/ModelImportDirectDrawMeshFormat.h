
#ifndef MODEL_IMPORT_DIRECT_DRAW_MESH_FORMAT
#define MODEL_IMPORT_DIRECT_DRAW_MESH_FORMAT

#include "Common.h"

#include "ModelImportFormat.h"
#include "Vector3.h"
#include "Vector2.h"

class Renderer;
class Mesh;
class MeshStream;

class ModelImportDirectDrawMeshFormat : public ModelImportFormat
{
	struct Stream
	{
		std::stringstream str;
		Mesh * mesh;
		MeshStream * positions;
		MeshStream * normals;
		MeshStream * texcoords;
	};

public:

	ModelImportDirectDrawMeshFormat(Renderer * renderer);

	bool import(File * file, ModelData * model);

private:

	bool parse(Stream & stream);

	void parseV(Stream & stream);
	void parseN(Stream & stream);
	void parseT(Stream & stream);

private:

	Renderer * renderer;
};

#endif
