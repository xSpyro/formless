
#ifndef SHADER_STORE
#define SHADER_STORE

#include "Common.h"

#include "Logger.h"

class BaseShader;
class Renderer;

class ShaderStore
{
	typedef std::map < std::string, BaseShader* > ShaderMap;

public:

	ShaderStore();
	~ShaderStore();

	void init(Renderer * renderer);

	BaseShader * get(const std::string & resource, int flags = 0);

	BaseShader * create(const std::string & name, const char * data, unsigned int len);

	// will reload all current shaders again from files
	void reload();

private:

	BaseShader * find(const std::string & name) const;

	void clear();

	void store(const std::string & name, BaseShader * shader);

private:

	Logger log;

	Renderer * renderer;

	ShaderMap shaders;
};

#endif