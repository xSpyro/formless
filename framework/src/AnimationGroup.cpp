
#include "AnimationGroup.h"

void AnimationGroup::insertFrame(int pos, int mesh, float curve)
{
	Frame frame;
	frame.mesh = mesh;
	frame.curve = curve;

	timeline[pos] = frame;


	// get last frame in the timeline
	if (!timeline.empty())
	{
		length = (--timeline.end())->first;
	}
}



unsigned int AnimationGroup::getLength() const
{
	return length;
}

Animation AnimationGroup::getFrame(unsigned int pos) const
{
	Animation out;


	// validate pos
	if (pos >= length)
	{
		return out;
	}


	// see which two frames we are blending between
	if (timeline.size() == 1)
	{
		// single static pose

		const Frame & frame = timeline.begin()->second;

		out.src = out.dst = frame.mesh;
		out.lerp = 0;
	}
	else if (timeline.size() > 1)
	{
		// animated
		unsigned int lerp[2];
		Timeline::const_iterator i1 = getLeftBound(pos);

		const Frame & frame1 = i1->second;
		lerp[0] = i1->first;

		i1++;
		const Frame & frame2 = i1->second;
		lerp[1]= i1->first;

		out.src = frame1.mesh;
		out.dst = frame2.mesh;
		out.lerp = (pos - lerp[0]) / static_cast < float > (lerp[1] - lerp[0]);

		// now lerp is in the range of 0 to 1

		// with dst curve we make it 0 to 1 * curve

		out.lerp *= (frame2.curve * frame1.curve);
		
		// now we might have 0 to 0.2

		// adjust with base to have 0.8 to 1

		out.lerp += (1 - frame1.curve);


	}
	else
	{
		// default frame 0 0 0
	}

	return out;
}

const std::string & AnimationGroup::getName() const
{
	return name;
}

void AnimationGroup::setName(const std::string & name)
{
	this->name = name;
}

AnimationGroup::Timeline::const_iterator AnimationGroup::getLeftBound(unsigned int pos) const
{
	for (Timeline::const_iterator i = timeline.begin(); i != timeline.end();)
	{
		Timeline::const_iterator i1 = i;
		++i;

		if (pos >= i1->first && pos < i->first)
			return i1;
	}

	return timeline.end();
}