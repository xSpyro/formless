
#include "Model.h"

#include "ModelData.h"
#include "Renderable.h"
#include "BaseShader.h"

#include "Mesh.h"
#include "Material.h"

Model::Model()
	: parent(NULL), frame(0.0f), animation(NULL)
{
	transform.identity();
	frame_speed = 1.0f;
	userdata = NULL;
	paused = false;
	loop = true;

	target_transform = &transform;
}

Model::Model(Mesh * mesh)
	: parent(NULL), frame(0.0f), animation(NULL)
{
	transform.identity();
	frame_speed = 1.0f;
	userdata = NULL;
	paused = false;
	loop = true;

	target_transform = &transform;

	add(Renderable(mesh, NULL, this));
}

Model::Model(Renderable renderable)
	: parent(NULL), frame(0.0f), animation(NULL)
{
	transform.identity();
	frame_speed = 1.0f;
	userdata = NULL;
	paused = false;
	loop = true;

	target_transform = &transform;

	add(renderable);
}

Model::Model(const ModelData * data)
	: parent(data), frame(0.0f), animation(NULL)
{
	transform.identity();
	frame_speed = 1.0f;
	userdata = NULL;
	paused = false;
	loop = true;

	target_transform = &transform;

	if (data)
	{
		data->create(this);
	}
}

void Model::create(const ModelData * data)
{
	//transform.identity();

	parent = data;

	if (data)
	{
		data->create(this);
	}
}

void Model::add(Renderable renderable)
{
	renderable.setParent(this);

	renderables.push_back(renderable);
}

void Model::clear()
{
	renderables.clear();
}

void Model::setMaterial(const Material & material)
{
	for (RenderableVector::iterator i = renderables.begin(); i != renderables.end(); ++i)
	{
		i->setMaterial(material);
	}
}

void Model::setVisible(bool state)
{
	for (RenderableVector::iterator i = renderables.begin(); i != renderables.end(); ++i)
	{
		i->setVisible(state);
	}
}

Animation Model::getAnimation() const
{
	if (animation)
	{
		return animation->getFrame(static_cast < unsigned int > (frame));
	}
	else
	{
		return Animation();
	}
}

void Model::update()
{
	if (!paused && animation)
	{

		frame += frame_speed;

		if (static_cast < unsigned int > (frame) >= animation->getLength())
		{
			if (loop)
			{
				frame = 0;
			}
			else
			{
				paused = true;
				frame = static_cast < float > (animation->getLength() - 1);
			}
		}
	}
}

const std::vector < Renderable > & Model::getRenderables() const
{
	return renderables;
}

const Matrix & Model::getTransform() const
{
	return *target_transform;
}

Matrix & Model::getTransform()
{
	// not really good
	return const_cast < Matrix & > (*target_transform);
}

const Matrix & Model::getNativeMatrix() const
{
	return transform;
}

Matrix & Model::getNativeMatrix()
{
	return transform;
}

Matrix Model::getInvTransform() const
{
	Matrix out;
	D3DXMatrixInverse((D3DXMATRIX*)&out, NULL, (const D3DXMATRIX*)target_transform);

	return out;
}

void Model::setSharedMatrix(const Matrix * matrix)
{
	if (matrix)
		target_transform = matrix;
	else
		target_transform = &transform;
}

void Model::submit(BaseShader * shader)
{
	for (RenderableVector::iterator i = renderables.begin(); i != renderables.end(); ++i)
	{
		i->getMesh()->submit(shader);
	}
}

float Model::getRadius() const
{
	Animation a = getAnimation();
	float scale = 0.0f;

	// use 1 mesh only
	for (RenderableVector::const_iterator i = renderables.begin(); i != renderables.end(); ++i)
	{
		//scale = i->getMesh()->getBoundingSphere(a.src, a.dst, a.lerp).radius;
		scale = i->getMesh()->getXZRadius(a.src, a.dst, a.lerp);
	}

	Vector3 n(scale);
	n.transform(transform);
	scale =  n.length();

	return scale;
}

bool Model::play(const std::string & name)
{
	if (parent)
	{

		// start an animation sequence
		const AnimationGroup * a = parent->getAnimationGroup(name);
		if (a)
		{
			animation = a;
			frame = 0;
			paused = false;

			return true;
		}
	}

	return false;
}

void Model::stop()
{
	paused = true;
}

void Model::resume()
{
	paused = false;
}

void Model::setAnimationLoop(bool state)
{
	loop = state;
}

void Model::setAnimationSpeed(float speed)
{
	frame_speed = speed;
}

unsigned int Model::getFrame() const
{
	return static_cast < unsigned int > (frame);
}

void Model::setFrame(unsigned int frame)
{
	this->frame = static_cast < float > (frame);
}

void Model::setUserdata(void * ud)
{
	userdata = ud;
}

void * Model::getUserdata() const
{
	return userdata;
}

const char * Model::getAnimationName() const
{
	if (animation)
	{
		return animation->getName().c_str();
	}

	return NULL;
}

void Model::detach()
{
	for (RenderableVector::iterator i = renderables.begin(); i != renderables.end(); ++i)
	{
		i->detach();
	}

	renderables.clear();
}