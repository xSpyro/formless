
#ifndef SPHERE
#define SPHERE

#include "Vector3.h"

struct Sphere
{
	Vector3 origin;
	float radius;
};

#endif