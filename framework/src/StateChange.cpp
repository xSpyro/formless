
#include "StateChange.h"

#include "Renderer.h"

StateChange::StateChange(Renderer * renderer)
	: renderer(renderer)
{
}

void StateChange::apply()
{
	renderer->redirectFrameBuffer(texture);
}

void StateChange::free()
{
	delete this;
}