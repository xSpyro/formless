
#ifndef INTERSECT_H

#define INTERSECT_H

#include "Ray.h"
#include "Plane.h"
#include "Sphere.h"
#include "OBB.h"


#include "IntersectResult.h"

class Intersect
{
public:

	static bool intersect(const Ray& ray, const Plane& plane, IntersectResult* res);

	static bool intersect(const Ray & ray, const Sphere & sphere,  IntersectResult* res);

	static bool intersect(const Sphere & leftSphere, const Sphere & rightSphere, IntersectResult * res);

	static bool intersect(const Ray & ray, const OBB & box, IntersectResult * res);
	
	static bool intersect(const Sphere & sphere, const OBB & box, IntersectResult * res); // Sphere radius not included in the distance on 

	static bool intersect(const OBB & lhs, const OBB & rhs, IntersectResult * res);

	/*
	static bool intersect(const Ray & ray, const Box & box,  IntersectResult* res) const
	*/
	/*
	static bool intersect(const Ray & ray, const Triangle & triangle,  IntersectResult* res);
	*/
};

#endif