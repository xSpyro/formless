
#include "ModelImportDirectDrawMeshFormat.h"

#include "ModelData.h"
#include "Renderable.h"
#include "Mesh.h"

#include "Vector3.h"

ModelImportDirectDrawMeshFormat::ModelImportDirectDrawMeshFormat(Renderer * renderer)
	: renderer(renderer)
{
}

bool ModelImportDirectDrawMeshFormat::import(File * file, ModelData * model)
{
	Stream stream;

	stream.str.write(file->data, file->size);

	if (model->empty())
	{

		stream.mesh = new Mesh(renderer);
		Renderable renderable;
		renderable.setMesh(stream.mesh);
		model->add(renderable);

		stream.positions = stream.mesh->createPositionStream("POSITION", sizeof(Vector3));
		stream.normals = stream.mesh->createSingleStream("NORMAL", sizeof(Vector3));
		stream.texcoords = stream.mesh->createSingleStream("TEXCOORD", sizeof(Vector2));
	}
	else
	{
		stream.mesh = model->getFirstMesh();

		stream.positions = stream.mesh->getStream("POSITION");
		stream.positions->addOffset(stream.positions->getElements());

		stream.normals = stream.mesh->getStream("NORMAL");
		stream.normals->addOffset(stream.normals->getElements());

		stream.texcoords = stream.mesh->getStream("TEXCOORD");
		stream.texcoords->addOffset(stream.texcoords->getElements());
	}

	parse(stream);

	return true;
}

bool ModelImportDirectDrawMeshFormat::parse(Stream & stream)
{
	while ( !stream.str.eof() )
	{
		std::string token;
		stream.str >> token;

		if (token == "v")
		{
			parseV(stream);
		}
		else if (token == "n")
		{
			parseN(stream);
		}

		else if (token == "t")
		{
			parseT(stream);
		}

		//std::cout << "token: " << token << std::endl;
	}

	return true;
}

void ModelImportDirectDrawMeshFormat::parseV(Stream & stream)
{
	Vector3 value;
	stream.str >> value.x >> value.y >> value.z;

	stream.positions->append((char *)&value, 1);
}

void ModelImportDirectDrawMeshFormat::parseN(Stream & stream)
{
	Vector3 value;
	stream.str >> value.x >> value.y >> value.z;

	stream.normals->append((char *)&value, 1);
}

void ModelImportDirectDrawMeshFormat::parseT(Stream & stream)
{
	Vector2 value;
	stream.str >> value.x >> value.y;

	stream.texcoords->append((char *)&value, 1);
}