
#ifndef BASE_SHADER
#define BASE_SHADER

#include "Common.h"

#include "File.h"

class Renderer;
class Texture;
class ConstantBuffer;

enum
{
	SHADER_STREAM_OUT = 1
};

class BaseShader
{
	typedef std::map < int, ID3D11Buffer* > BufferMap;

public:

	BaseShader();
	virtual ~BaseShader();

	virtual bool load(Renderer * renderer, File * file, int flags = 0) = 0;

	virtual void apply() = 0;


	// properties
	virtual void setSamplers() { }
	virtual void setTexture(int slot, Texture * texture) { }
	virtual void bindTexture(int slot, Texture * texture) { }

	virtual void setConstantBuffer(int slot, ConstantBuffer * buffer) { }

	DWORD * getCompiledChunk();
	SIZE_T getCompiledChunkSize() const;

	Renderer * getRenderer();

public:

	int flags;

protected:

	bool compile(File * file, const std::string & version);

	Renderer * renderer;

	BufferMap buffers;

private:

	ID3D10Blob * compiled;
};

#endif