
#include "Renderable.h"

#include "Model.h"
#include "Mesh.h"
#include "Ray.h"
#include "Matrix.h"

#include "Layer.h"

#include "IntersectResult.h"

Renderable::Renderable()
	: mesh(NULL), texture(NULL), model(NULL), forced_verts(0), visible(true)
{
	init();
}

Renderable::Renderable(Mesh * mesh, Texture * texture, Model * model)
	: mesh(mesh), texture(texture), model(model), forced_verts(0), visible(true)
{
	init();
}

Renderable::Renderable(unsigned int verts, Texture * texture)
	: mesh(NULL), texture(texture), model(NULL), forced_verts(verts), visible(true)
{
	init();
}

Renderable::~Renderable()
{
	detach();
}

void Renderable::setMesh(Mesh * mesh)
{
	this->mesh = mesh;
}

void Renderable::setTexture(Texture * texture)
{
	this->texture = texture;
}

void Renderable::setMaterial(const Material & material)
{
	this->material = material;
}

void Renderable::setMaterial(Material * material)
{
	// we make a copy of the material
	this->material = *material;
}	

void Renderable::setVisible(bool state)
{
	visible = state;
}

Mesh * Renderable::getMesh()
{
	return mesh;
}

const Mesh * Renderable::getMesh() const
{
	return mesh;
}

unsigned int Renderable::getForcedVertices() const
{
	return forced_verts;
}

Material & Renderable::getMaterial()
{
	return material;
}

const Material & Renderable::getMaterial() const
{
	return material;
}

Texture * Renderable::getTexture()
{
	return texture;
}

const Texture * Renderable::getTexture() const
{
	return texture;
}

void Renderable::setParent(Model * model)
{
	this->model = model;
}

const Model * Renderable::getParent() const
{
	return model;
}

bool Renderable::pick(const Ray & ray, IntersectResult * result) const
{
	// if we have a parent
	// we use the matrix from it
	// otherwise assume 0 0 0 position

	if (model)
	{
		Ray transformed;
		D3DXVec3TransformCoord(
			(D3DXVECTOR3*)&transformed.origin, 
			(const D3DXVECTOR3*)&ray.origin, 
			(const D3DXMATRIX*)&model->getInvTransform());

		D3DXVec3TransformNormal(
			(D3DXVECTOR3*)&transformed.direction, 
			(const D3DXVECTOR3*)&ray.direction, 
			(const D3DXMATRIX*)&model->getInvTransform());

		// first check for boundings
		//Animation a = model->getAnimation();
		Sphere sphere = mesh->getBoundingSphere(0, 0, 0);

		//if (transformed.intersect(sphere, result))
		//{
		//	return mesh->pick(transformed, result);
		//}
		return mesh->pick(transformed, result);
	}
	else
	{
		return mesh->pick(ray, result);
	}

	return false;
}

void Renderable::detach() const
{
	// detach from all layers
	for (int i = 0; i < 4; ++i)
	{
		if (holder[i])
			holder[i]->remove(this);
		holder[i] = NULL;
	}
}

void Renderable::detach(Layer * layer) const
{
	// remove from layer
	layer->remove(this);

	// remove from list
	for (int i = 0; i < 4; ++i)
	{
		if (holder[i] == layer)
		{
			holder[i] = NULL;
			break;
		}
	}
}

void Renderable::attach(Layer * layer) const
{
	// add to first free spot
	for (int i = 0; i < 4; ++i)
	{
		if (holder[i] == NULL)
		{
			holder[i] = layer;
			break;
		}
	}
}

void Renderable::init()
{
	std::memset(holder, 0, sizeof(void*) * 4);
}