
#include "ParticleType.h"

#include "Renderer.h"
#include "Mesh.h"

#include "Particle.h"

#include "MeshStream.h"

#include "Vector3.h"
#include "Vector2.h"

void ParticleType::create(Renderer * renderer, BaseShader * shader)
{
	// play with mesh

	mesh = renderer->mesh()->create();

	stream = mesh->createEmptyStream("ParticleStream");
	stream->addDesc("POSITION", sizeof(Vector3));
	stream->addDesc("VELOCITY", sizeof(Vector3));
	stream->addDesc("COLOR", sizeof(Color));
	stream->addDesc("SCALE", sizeof(float));
	stream->addDesc("ANGLE", sizeof(float));
	stream->addDesc("OANGLE", sizeof(float));
	stream->addDesc("LIFE", sizeof(float));
	stream->fill(6400);
	
	mesh->submit(shader);

	mesh->setTopology(D3D10_PRIMITIVE_TOPOLOGY_POINTLIST);
}

ParticleEmitter * ParticleType::addEmitter(const ParticleBehavior * behavior)
{
	int start;
	int end;

	if (getFreeParticleBatch(&start, &end))
	{
		ParticleEmitter * emitter = new ParticleEmitter(start, end, this, behavior);

		std::pair < ParticleEmitters::iterator, bool > res = emitters.insert(std::pair<int, ParticleEmitter*>(start, emitter));

		if (res.second)
		{
			return res.first->second;
		}
		else
		{
			return NULL;
		}
	}

	return NULL;
}

bool ParticleType::kill(ParticleEmitter * emitter)
{
	// find the emitter
	ParticleEmitters::iterator i = emitters.find(emitter->start);

	if (i != emitters.end())
	{
		killParticles(emitter->start, emitter->end);

		emitters.erase(i);
		delete emitter;

		return true;
	}

	return false;
}

bool ParticleType::kill()
{
	// kill all particles
	killParticles(0, stream->getElements());

	// kill all emitters
	for (ParticleEmitters::iterator i = emitters.begin(); i != emitters.end(); ++i)
	{
		delete i->second;
	}

	emitters.clear();

	return true;
}

void ParticleType::update()
{
	for (ParticleEmitters::iterator i = emitters.begin(); i != emitters.end();)
	{
		ParticleEmitter * emitter = i->second;
		++i;

		if (!emitter->update(stream))
		{
			//std::cout << "emitter killed: " << emitter->start << " - " << emitter->end << std::endl;
			kill(emitter);
		}
	}

	mesh->update();
}

Mesh * ParticleType::getMesh()
{
	return mesh;
}

MeshStream * ParticleType::getStream()
{
	return stream;
}
/*
void ParticleType::spawn(Particle & particle)
{
	particle.position = Vector3(
		-1 + (rand() / (float)RAND_MAX) * 2, 
		-1 + (rand() / (float)RAND_MAX) * 2, 
		-1 + (rand() / (float)RAND_MAX) * 2) * 0.01f;
	particle.life = 1.0f;
	particle.scale = 0.0f;

	particle.velocity = Vector3(
		-1 + (rand() / (float)RAND_MAX) * 2, 
		-1 + (rand() / (float)RAND_MAX) * 2,
		-1 + (rand() / (float)RAND_MAX) * 2) * 0.05f;
}

void ParticleType::process(Particle & particle)
{
	particle.position.x += particle.velocity.x;
	particle.position.y += particle.velocity.y;
	particle.position.z += particle.velocity.z;

	particle.life -= 0.018f;
	particle.scale = 0.3f - particle.life * 0.2f;
}
*/

bool ParticleType::getFreeParticleBatch(int * start, int * end)
{
	// this will give us 10 emitters at once
	int wanted_range = stream->getElements() / 240;

	int t1 = 0;
	int t2 = stream->getElements();

	// if its empty we dont need to search
	if (emitters.empty())
	{
		*start = 0;
		*end = wanted_range;
		//std::cout << "found range: " << *start << " > " << *end << std::endl;
		return true;

	}

	// search for an open range 
	for (ParticleEmitters::iterator i = emitters.begin(); i != emitters.end();)
	{
		ParticleEmitter & pe1 = *i->second;
		++i;

		// if we are front
		if (t1 + wanted_range <= pe1.start)
		{
			t2 = pe1.start;
		}
		else if (i != emitters.end())
		{
			ParticleEmitter & pe2 = *i->second;

			// were in the between to emitters
			if (pe1.end > t1)
				t1 = pe1.end;

			if (pe2.start < t2)
				t2 = pe2.start;
		}
		else
		{
			// were at the end

			t1 = pe1.end;
			t2 = stream->getElements();
		}

		// if there is space

		if (wanted_range <= t2 - t1)
		{
			*start = t1;
			*end = t1 + wanted_range;
			//std::cout << "found range: " << *start << " > " << *end << std::endl;

			return true;
		}
	}

	return false;
}

void ParticleType::killParticles(int start, int end)
{
	char * mem = stream->get(start);

	int size = end - start;

	std::memset(mem, 0, size * sizeof(Particle));
}