
#ifndef MODEL_DATA
#define MODEL_DATA

#include "AnimationGroup.h"

#include "Renderable.h"

class Model;
class BaseShader;

class ModelData
{
	typedef std::vector < Renderable > Renderables;

	typedef std::map < std::string, AnimationGroup > Animations;

public:

	void create(Model * model) const;

	void add(Renderable renderable);

	void overrideTexture(unsigned int renderable, Texture * texture);

	bool empty() const;

	void submit(BaseShader * shader) const;

	const AnimationGroup * getAnimationGroup(const std::string & name) const;


	void insertAnimationGroup(const std::string & name, const AnimationGroup & animation);


	// temp
	Mesh * getFirstMesh();

private:

	Renderables renderables;

	Animations animations;
};

#endif