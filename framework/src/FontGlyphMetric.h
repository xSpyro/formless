
#ifndef FONT_GLYPH_METRIC
#define FONT_GLYPH_METRIC

#include "Rect.h"
#include "Vector2.h"

struct FontGlyphMetric
{
	// each glyph has a place in the texture
	Rect coord;

	// a position in space
	Vector2 offset;

	// size of original glyph
	Vector2 dim;

	// advance
	Vector2 advance;
};

#endif