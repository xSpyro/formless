
#include "ModelImportObjectGroup.h"

ModelImportObjectGroup::ModelImportObjectGroup(const std::string & name)
	: name(name)
{
	has_material = false;
}

void ModelImportObjectGroup::parse(Stream & stream)
{
	while ( !stream.str.eof() )
	{
		std::string token;
		stream.str >> token;

		if (token == "v")
		{
			parseV(stream);
		}
		else if (token == "vn")
		{
			parseVN(stream);
		}
		else if (token == "vt")
		{
			parseVT(stream);
		}
		else if (token == "f")
		{
			parseF(stream);
		}
		else if (token == "g")
		{
			// leave group
			stream.str.unget();
			break;
		}
		else if (token == "usemtl")
		{
			if (has_material || name == "default")
			{
				for (int i = 0; i < 6; ++i, stream.str.unget());
				break;
			}

			parseMaterial(stream);

			has_material = true;
		}
		else
		{
			stream.str.ignore(1000, '\n');
		}
	}

	std::cout << "end group " << name << std::endl;
}

void ModelImportObjectGroup::parseV(Stream & stream)
{
	Vector3 vec;
	stream.str >> vec.x >> vec.y >> vec.z;

	positions.push_back(vec);

	stream.str.ignore(1000, '\n');
}

void ModelImportObjectGroup::parseVN(Stream & stream)
{
	Vector3 vec;
	stream.str >> vec.x >> vec.y >> vec.z;

	normals.push_back(vec);

	stream.str.ignore(1000, '\n');
}

void ModelImportObjectGroup::parseVT(Stream & stream)
{
	Vector2 vec;
	stream.str >> vec.x >> vec.y;

	texcoords.push_back(vec);

	stream.str.ignore(1000, '\n');
}

void ModelImportObjectGroup::parseF(Stream & stream)
{
	unsigned int f = 0;
	FaceTemp temp[32] = {0};

	for (f = 0; f < 32; ++f)
	{
		

		// we always have an index to a position
		unsigned int position = 1;
		unsigned int texcoord = 1;
		unsigned int normal = 1;
	
		stream.str >> position;

		// now check for number of "/" chars

		int i = 0;
		for (i = 0; i < 3; ++i)
		{
			if (stream.str.peek() == '/')
				stream.str.ignore();
			else 
				break;
		}


		if (i == 1) // we have texcoord
		{
			stream.str >> texcoord;

			if (stream.str.peek() == '/') // and normal
			{
				stream.str.ignore();
				stream.str >> normal;
			}
		}
		else if (i == 2) // only normal
		{
			stream.str >> normal;
		}

		temp[f].v = position;
		temp[f].vt = texcoord;
		temp[f].vn = normal;

		bool found_end = false;

		// check for end of line
		for (int x = 0; x < 4; ++x)
		{
			if (stream.str.peek() == ' ')
			{
				stream.str.ignore();
				if (stream.str.peek() == '\n' || stream.str.peek() == 13)
				{
					found_end = true;
					break;
				}
				else if (stream.str.peek() != ' ')
				{
					stream.str.unget();
				}
			}
			else if (stream.str.peek() == '\n' || stream.str.peek() == 13)
			{
				found_end = true;
				break;
			}
		}

		if (found_end)
			break;
	}

	unsigned int indices = f + 1;

	// build the indices to be tris only

	//std::cout << "indices a face: " << indices << std::endl;

	// for each index above 3 create another face


	for (unsigned int i = 0; i < indices - 2; ++i)
	{
		Face face;

		face.v[0] = temp[i].v;
		face.vt[0] = temp[i].vt;
		face.vn[0] = temp[i].vn;

		face.v[1] = temp[i + 2].v;
		face.vt[1] = temp[i + 2].vt;
		face.vn[1] = temp[i + 2].vn;

		face.v[2] = temp[i + 1].v;
		face.vt[2] = temp[i + 1].vt;
		face.vn[2] = temp[i + 1].vn;


		faces.push_back(face);
	}

	stream.str.ignore(1000, '\n');
}

void ModelImportObjectGroup::parseMaterial(Stream & stream)
{
	stream.str >> material;
	//stream.str.ignore(1000, '\n');
}

void ModelImportObjectGroup::printStats()
{
	std::cout << name << "> v" << positions.size() << " vt" << texcoords.size() << " vn" << normals.size() << " f" << faces.size() << std::endl;
}

Mesh * ModelImportObjectGroup::createMesh(Renderer * renderer, ModelImportObjectGroup * def)
{
	// BAD MUST BE FIXED WITH PROPER ALLOCATOR
	mesh = new Mesh(renderer);

	MeshStream * in_positions = mesh->createPositionStream("POSITION", sizeof(Vector3));
	MeshStream * in_normals = mesh->createSingleStream("NORMAL", sizeof(Vector3));
	MeshStream * in_texcoords = mesh->createSingleStream("TEXCOORD", sizeof(Vector2));

	// we contain the faces and def contains the vertex data
	
	in_positions->fill(faces.size() * 3);
	in_normals->fill(faces.size() * 3);
	in_texcoords->fill(faces.size() * 3);

	Vector3 * beg_positions = reinterpret_cast < Vector3* > (in_positions->get(0));
	Vector3 * beg_normals = reinterpret_cast < Vector3* > (in_normals->get(0));
	Vector2 * beg_texcoords = reinterpret_cast < Vector2* > (in_texcoords->get(0));

	for (unsigned int i = 0; i < faces.size(); ++i)
	{
		beg_positions[i * 3 + 0] = def->positions.at(faces.at(i).v[0] - 1);
		beg_positions[i * 3 + 1] = def->positions.at(faces.at(i).v[1] - 1);
		beg_positions[i * 3 + 2] = def->positions.at(faces.at(i).v[2] - 1);

		if (!def->normals.empty())
		{
			beg_normals[i * 3 + 0] = def->normals.at(faces.at(i).vn[0] - 1);
			beg_normals[i * 3 + 1] = def->normals.at(faces.at(i).vn[1] - 1);
			beg_normals[i * 3 + 2] = def->normals.at(faces.at(i).vn[2] - 1);
		}
		
		if (!def->texcoords.empty())
		{
			beg_texcoords[i * 3 + 0] = def->texcoords.at(faces.at(i).vt[0] - 1);
			beg_texcoords[i * 3 + 1] = def->texcoords.at(faces.at(i).vt[1] - 1);
			beg_texcoords[i * 3 + 2] = def->texcoords.at(faces.at(i).vt[2] - 1);
		}

		mesh->addIndex(i * 3 + 0);
		mesh->addIndex(i * 3 + 1);
		mesh->addIndex(i * 3 + 2);
	}

	// no optimisation what so ever currently

	return mesh;
}