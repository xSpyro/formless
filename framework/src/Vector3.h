
#ifndef VECTOR3
#define VECTOR3

#include "Common.h"

struct Matrix;

struct Vector3
{
	float x, y, z;

	Vector3();
	Vector3(float v);
	Vector3(float x, float y, float z);

	Vector3 operator + (const Vector3 & rhs) const;
	Vector3 operator - (const Vector3 & rhs) const;
	Vector3 operator * (const Vector3 & rhs) const;

	Vector3 operator + (float offset) const;
	Vector3 operator - (float offset) const;
	Vector3 operator / (float divider) const;
	Vector3 operator * (float scalar) const;

	Vector3 & operator += (const Vector3 & rhs);
	Vector3 & operator -= (const Vector3 & rhs);
	Vector3 & operator /= (float divider);

	bool operator != (const Vector3 &rhs) const; // Mostly for purposes of checking against zero to remove divisions by zero
	bool operator == (const Vector3 &rhs) const; // Mostly for purposes of checking against zero to remove divisions by zero

	void normalize();

	Vector3 cross(const Vector3 & rhs) const;
	float dot(const Vector3 & rhs) const;

	void transform(const Matrix & mat);
	void transformNormal(const Matrix & mat);

	float lengthNonSqrt() const;
	float length() const;

	Vector3 lerp(const Vector3 & rhs, float delta) const;


	static Vector3 random(float x, float y, float z);


	friend std::ostream & operator << (std::ostream & stream, const Vector3 & me);
};

#endif