
#include "GeometryShader.h"

#include "Renderer.h"
#include "ConstantBuffer.h"

GeometryShader::GeometryShader()
{

}

bool GeometryShader::load(Renderer * renderer, File * file, int flags)
{
	this->renderer = renderer;

	bool result = compile(file, "gs_4_0");

	if (!result)
		return false;

	HRESULT hr;

    // create the vertex shader
	if (flags == SHADER_STREAM_OUT)
	{
		D3D11_SO_DECLARATION_ENTRY pDecl[] =
		{
			// VERY BAD HARDCODED

			// semantic name, semantic index, start component, component count, output slot
			{ 0, "POSITION", 0, 0, 3, 0 },   // output all components of position
			{ 0, "VELOCITY", 0, 0, 3, 0 },     // output the first 3 of the normal
			{ 0, "TARGET", 0, 0, 3, 0 },
			{ 0, "ORIGIN", 0, 0, 3, 0 },
			{ 0, "FROM", 0, 0, 1, 0 },
			{ 0, "ROUTE", 0, 0, 1, 0 },
			{ 0, "LIFE", 0, 0, 1, 0 },
			{ 0, "SEED", 0, 0, 1, 0 },
		};

		unsigned int strides[1] = {16 * sizeof(float)};

		D3D_FEATURE_LEVEL f = renderer->getFeatureLevel();

		if (f == D3D_FEATURE_LEVEL_11_0)
		{
			
			hr = renderer->getDevice()->CreateGeometryShaderWithStreamOutput(getCompiledChunk(), 
 				getCompiledChunkSize(), pDecl, 8, strides, 1, D3D11_SO_NO_RASTERIZED_STREAM, NULL, &shader);
		}

		else if (f == D3D_FEATURE_LEVEL_10_1 || f == D3D_FEATURE_LEVEL_10_0)
		{
			hr = renderer->getDevice()->CreateGeometryShaderWithStreamOutput(getCompiledChunk(), 
							getCompiledChunkSize(), pDecl, 8, strides, 1, 0, NULL, &shader);
		}
	
		//D3D11_SO_NO_RASTERIZED_STREAM
	}
	else
		hr = renderer->getDevice()->CreateGeometryShader(getCompiledChunk(), getCompiledChunkSize(), NULL, &shader);

	if (FAILED(hr))
	{
		return false;
	}


	return true;
}

void GeometryShader::apply()
{
	// send cbuffers
	for (BufferMap::iterator i = buffers.begin(); i != buffers.end(); ++i)
	{
		renderer->getContext()->GSSetConstantBuffers(i->first, 1, &i->second);
	}

	renderer->getContext()->GSSetShader(shader, NULL, 0);
}

void GeometryShader::unbind(Renderer * renderer)
{
	renderer->getContext()->GSSetShader(NULL, NULL, 0);
}


void GeometryShader::setConstantBuffer(int slot, ConstantBuffer * buffer)
{
	buffers[slot] = buffer->getBuffer();
}

void GeometryShader::setTexture(int slot, Texture * texture)
{
	ID3D11ShaderResourceView * views[1];
	views[0] = texture->getView();

	renderer->getContext()->GSSetShaderResources(slot, 1, views); 
}