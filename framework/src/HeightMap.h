
#ifndef HEIGHTMAP
#define HEIGHTMAP

#include "Vector3.h"
#include "Vector2.h"

class Renderer;
class BaseShader;
class Mesh;
class Surface;

class HeightMap
{
	struct Vertex
	{
		Vector3 position;
		Vector3 normal;
		Vector2 texcoord;
	};

public:

	struct Desc
	{
		float scale;
		float height;
		float texscale;
		float offset;
		Surface * surface;
		bool ccw;
	};

	static bool generate(Renderer * renderer, BaseShader * shader, Desc * desc, Mesh ** out);
	static bool update(Renderer * renderer, BaseShader * shader, Desc * desc, Mesh * out);

private:
};

#endif