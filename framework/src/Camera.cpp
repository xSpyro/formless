
#include "Camera.h"

Camera::Camera()
	: position(0, 0, 0), orientation(0, 0, 1), up(0, 1, 0), left(1,0,0)
{
	near = 3;
	far = 60;

	setProjection3D(45, 16 / 9.0f);
}

Camera::~Camera()
{
}

void Camera::setPosition(const Vector3 & vec)
{
	position = vec;
}

void Camera::setLookAt(const Vector3 & vec)
{
	// get the direction to the point from the camera position and normalize it

	orientation = (vec - position);
	orientation.normalize();
}

void Camera::setOrientation(const Vector3 & vec)
{
	orientation = vec;
}

void Camera::setUp(const Vector3 & vec)
{
	up = vec;
}

void Camera::setClipRange(float near, float far)
{
	this->near = near;
	this->far = far;
}

Vector2 Camera::getClipRange() const
{
	return Vector2(near, far);
}

void Camera::setProjection3D(float fovy, float aspect)
{
	D3DXMatrixPerspectiveFovLH((D3DXMATRIX*)&projection, (float)D3DXToRadian(fovy), aspect, near, far);
}

void Camera::setProjection2D(float width, float height)
{
	D3DXMatrixOrthoOffCenterLH((D3DXMATRIX*)&projection, 0, width, height, 0, near, far);
}

void Camera::setProjectionOrtho(float l, float r, float b, float t)
{
	D3DXMatrixOrthoOffCenterLH((D3DXMATRIX*)&projection, l, r, b, t, near, far);
}

const Vector3 & Camera::getPosition() const
{
	return position;
}

const Vector3 & Camera::getOrientation() const
{
	return orientation;
}

const Vector3 & Camera::getUp() const
{
	return up;
}

Matrix & Camera::getViewProjectionMatrix()
{
	return view_projection_t;
}

const Matrix & Camera::getViewProjectionMatrixNonTransposed() const
{
	return view_projection;
}

const Matrix & Camera::getViewProjectionMatrix() const
{
	return view_projection_t;
}

const Matrix & Camera::getInvViewMatrix() const
{
	return inv_view_t;
}

const Matrix & Camera::getViewMatrix() const
{
	return view_t;
}

const Matrix & Camera::getInvViewProjectionMatrix() const
{
	return inv_view_projection_t;
}

Vector2 Camera::projectToScreenUV(const Vector3 & pos)
{
	float temp[4];
	D3DXVec3Transform((D3DXVECTOR4*)temp, (const D3DXVECTOR3*)&pos, (const D3DXMATRIX*)&view_projection);

	temp[0] /= temp[3];
	temp[1] /= temp[3];

	Vector2 out(temp[0], temp[1]);

	out.x = (out.x + 1) / 2;
	out.y = (out.y + 1) / 2;

	// invert y
	out.y = 1 - out.y;

	return out;
}

void Camera::move(float amount)
{
	position += orientation * amount;
}

void Camera::strafe(float amount)
{
	position += left * amount;
}
	
void Camera::yaw(float amount)
{
	yaw_angle += amount;
}

void Camera::pitch(float amount)
{
	pitch_angle += amount;
}

void Camera::roll(float amount)
{
	roll_angle += amount;
}

bool Camera::update()
{
	Vector3 cross_look = orientation.cross(up);
	Vector3 eye = position + orientation;

	cross_look.normalize();
	left = cross_look;

	D3DXMatrixLookAtLH((D3DXMATRIX*)&view, (D3DXVECTOR3*)&position, (D3DXVECTOR3*)&eye, (D3DXVECTOR3*)&up);
	
	view_projection = view * projection;

	inv_view = view.inverseOf();
	inv_projection = projection.inverseOf();
	inv_view_projection = view_projection.inverseOf();


	// p
	projection_t = projection;
	projection_t.transpose();

	inv_projection_t = inv_projection;
	inv_projection_t.transpose();

	// v
	view_t = view;
	view_t.transpose();

	inv_view_t = inv_view;
	inv_view_t.transpose();

	// vp
	view_projection_t = view_projection;
	view_projection_t.transpose();

	inv_view_projection_t = inv_view_projection;
	inv_view_projection_t.transpose();



	//float delta = far / near;

	// update frustum
	far_frustum[0] = Vector3(-1, 1, 1);
	far_frustum[1] = Vector3(1, 1, 1);
	far_frustum[2] = Vector3(1, -1, 1);
	far_frustum[3] = Vector3(-1, -1, 1);

	for (int i = 0; i < 4; ++i)
	{
		D3DXVec3TransformCoord((D3DXVECTOR3*)&far_frustum[i], 
			(const D3DXVECTOR3*)&far_frustum[i], (const D3DXMATRIX*)&inv_view_projection);

		far_frustum[i] = far_frustum[i] - position;
		far_frustum[i].normalize();	
	}

	return true;
}

void Camera::debug()
{
	for (int i = 0; i < 4; ++i)
	{
		std::cout << "frustum: " << i << " " << far_frustum[i] << std::endl;
	}
}

bool Camera::pick(const Vector2 & offset, Ray * out) const
{
	out->direction.x = offset.x / projection[0];
	out->direction.y = offset.y / projection[5];
	out->direction.z = 1.0f;

	out->origin = position;

	D3DXVec3TransformNormal((D3DXVECTOR3*)&out->direction, (D3DXVECTOR3*)&out->direction, (D3DXMATRIX*)&inv_view);

	return true;
}

Vector2 Camera::project(const Vector3 & position) const
{
	Vector3 temp;

	D3DXVec3TransformCoord((D3DXVECTOR3*)&temp, (D3DXVECTOR3*)&position, (D3DXMATRIX*)&view_projection);

	return Vector2(temp.x, temp.y);
}

void Camera::updateTimeFrame(float tf)
{
	timeframe = tf;
}

void * Camera::getShaderData()
{
	return &view_projection_t;
}

unsigned int Camera::getShaderDataSize()
{
	return sizeof(Matrix) * 6 + sizeof(Vector3);
}