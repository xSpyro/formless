
#ifndef MESH_STREAM
#define MESH_STREAM

#include "Common.h"

class Renderer;

enum
{
	MESH_STREAM_DEFAULT,
	MESH_STREAM_POSITIONS
};

class MeshStream
{
	struct Desc
	{
		std::string semantic;
		int stride;
	};

	typedef std::vector < char > Vector;
	typedef std::vector < unsigned int > Offsets;
	typedef std::vector < Desc > Descriptions;

public:

	MeshStream(int flags);
	MeshStream(const std::string & semantic, int stride, int flags);
	~MeshStream();

	void addDesc(const std::string & semantic, int stride);

	// these gives the first desc
	int getStride() const;
	const std::string & getSemantic() const;

	int getDescCount() const;
	int getStride(int index) const;
	const std::string & getSemantic(int index) const;

	int find(const std::string & semantic) const;


	ID3D11Buffer * getBuffer() const;

	unsigned int getMorphCount() const;
	unsigned int getMorphOffset(unsigned int index) const;

	unsigned int getElements() const;
	unsigned int getMorphElements() const;


	char * get(unsigned int n);
	const char * get(unsigned int n) const;
	void fill(unsigned int n);
	void append(char * data, unsigned int n);
	void update(char * data, unsigned int index);
	void clear();

	void addOffset(unsigned int offset);

	void submit(Renderer * renderer);

	void pushAndUpdate(Renderer * renderer, void * data, unsigned int n, unsigned int offset);

	void prependBeforeSO(Renderer * renderer, void * data, unsigned int n);

	unsigned int getOffsetSO() const;

private:

	Vector stream;

	// offsets will separate the different stages of the model
	// in the stream
	Offsets offsets;

	Descriptions descs;
	int stride;

	mutable unsigned int prepend;

	ID3D11Buffer * buffer;

	int flags;
};

#endif