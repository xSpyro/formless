
#ifndef GEOMETRY_SHADER
#define GEOMETRY_SHADER

#include "Common.h"

#include "File.h"
#include "BaseShader.h"

// might not look like this at all

class GeometryShader : public BaseShader
{

public:

	GeometryShader();

	bool load(Renderer * renderer, File * file, int flags);

	void apply();
	static void unbind(Renderer * renderer);

	void setConstantBuffer(int slot, ConstantBuffer * buffer);

	void setTexture(int slot, Texture * texture);

private:

	ID3D11GeometryShader * shader;

};

#endif