
#include "ModelImportObjectFormatEnhanced.h"

#include "ModelData.h"
#include "Renderable.h"
#include "Mesh.h"

#include "FileSystem.h"

#include "Renderer.h"


ModelImportObjectFormatEnhanced::ModelImportObjectFormatEnhanced(Renderer * renderer)
	: renderer(renderer)
{
}

bool ModelImportObjectFormatEnhanced::import(File * file, ModelData * model)
{
	Stream stream;

	stream.parent = this;

	stream.str.write(file->data, file->size);

	parse(stream);

	// okay now we combine all positions & vertices & normals
	// into 3 same sized streams

	// each group except 'default' contributes towards a separate renderable with 
	// a separate mesh


	Groups::iterator def = groups.find("default");

	if (def != groups.end())
	{
		for (Groups::iterator i = groups.begin(); i != groups.end(); ++i)
		{
			if (i == def)
				continue;

			Mesh * mesh = i->second.createMesh(renderer, &def->second);
			i->second.printStats();

			// get corresponding texture
			Materials::iterator m = materials.find(i->first);
			if (m == materials.end())
			{
				m = materials.find(i->second.material);
			}

			Renderable r(mesh, NULL, NULL);

			

			if (m != materials.end())
			{
				if (m->second.texture)
					r.setTexture(m->second.texture);
				else
					r.setTexture(renderer->texture()->get("textures/texture_gray.png"));

			}
			else
			{
				r.setTexture(renderer->texture()->get("textures/texture_gray.png"));
			}

			model->add(r);
		}
	}

	groups.clear();
	materials.clear();

	return true;
}

bool ModelImportObjectFormatEnhanced::parse(Stream & stream)
{
	while ( !stream.str.eof() )
	{
		std::string token;
		stream.str >> token;

		if (token == "mtllib")
		{
			//std::cout << "parse mtllib" << std::endl;
			
			// get name
			std::string lib;
			stream.str >> lib;

			std::cout << lib << std::endl;

			// open
			File file;
			if (FileSystem::get(std::string("models/") + lib, &file))
			{
				std::stringstream stream;
				stream.write(file.data, file.size);

				parseMaterialLib(stream);
			}
		}
		else if (token == "usemtl")
		{
			// unexpected but we make the name of the group the 
			// same as the material and feeds the material directly

			parseGAsMaterial(stream);
		}
		else if (token == "g")
		{
			parseG(stream);
		}
		else if (token == "v")
		{
			// very wrong
			// create a default group and go from there

			stream.str.unget();

			parseGDefault(stream);
		}
		else
		{
			stream.str.ignore(1000, '\n');
		}

		//std::cout << "token: " << token << std::endl;
	}

	return true;
}

void ModelImportObjectFormatEnhanced::parseG(Stream & stream)
{
	// get name of group

	std::string name;
	std::getline(stream.str, name);

	// cut of whitespace at front
	name = name.substr(name.find_first_not_of(" "));

	//std::cout << "Group name '" << name << "'" << std::endl;

	Groups::iterator i = groups.find(name);

	if (i != groups.end())
	{
		i->second.parse(stream);
	}
	else
	{
		// create new
		Groups::iterator at = groups.insert(groups.end(), Groups::value_type(name, ModelImportObjectGroup(name)));

		at->second.parse(stream);
	}
}

void ModelImportObjectFormatEnhanced::parseGAsMaterial(Stream & stream)
{
	// get name of group

	std::string name;
	stream.str >> name;

	// cut of whitespace at front
	//name = name.substr(name.find_first_not_of(" "));

	//std::cout << "Group name '" << name << "'" << std::endl;

	Groups::iterator i = groups.find(name);

	if (i != groups.end())
	{
		i->second.parse(stream);
	}
	else
	{
		// create new
		Groups::iterator at = groups.insert(groups.end(), Groups::value_type(name, ModelImportObjectGroup(name)));

		// parse material
		at->second.parseMaterial(stream);

		at->second.parse(stream);
	}
}

void ModelImportObjectFormatEnhanced::parseGDefault(Stream & stream)
{
	std::string name = "default";

	//std::cout << "Group name '" << name << "'" << std::endl;

	Groups::iterator i = groups.find(name);

	if (i != groups.end())
	{
		i->second.parse(stream);
	}
	else
	{
		// create new
		Groups::iterator at = groups.insert(groups.end(), Groups::value_type(name, ModelImportObjectGroup(name)));

		// parse material
		//at->second.parseMaterial(stream);

		at->second.parse(stream);
	}
}

void ModelImportObjectFormatEnhanced::parseMaterialLib(std::stringstream & stream)
{

	GroupMaterial * mat = NULL;
	

	

	while ( !stream.eof() )
	{
		std::string token;
		stream >> token;

		if (token == "newmtl")
		{
			std::string name;
			stream >> name;

			Materials::iterator at = materials.insert(materials.end(), Materials::value_type(name, GroupMaterial()));

			mat = &at->second;

			mat->texture = renderer->texture()->create(0, 0, TEXTURE_MULTI);
		}
		else if (token == "map_Kd")
		{
			std::string name;
			stream >> name;

			mat->texture->addMultiTexture(renderer->texture()->get(std::string("models/") + name));
		}
		else if (token == "map_Ks")
		{
			std::string name;
			stream >> name;

			mat->texture->addMultiTexture(renderer->texture()->get(std::string("models/") + name));
		}
		else
		{
			stream.ignore(1000, '\n');
		}
	}
}