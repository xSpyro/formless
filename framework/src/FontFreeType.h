
#ifndef FONT_FREETYPE
#define FONT_FREETYPE

#include "Common.h"

#include "Font.h"
#include "FileSystem.h"
#include "Logger.h"
#include "FontGlyphMetric.h"

class FontFreeType
{

	typedef std::vector < FontGlyphMetric > Glyphs;

public:

	FontFreeType();
	~FontFreeType();

	bool init();

	bool createFontDataFromFile(File * file, Font * font, int height);

private:

	bool parseFont(FT_Face face, int height, Surface & surface, Glyphs & glyphs);

	bool preProcessFace(FT_Face face, int height, int * p);

	Rect glyphMetrics(FT_Face face, int height);

private:

	Logger log;

	FT_Library library;
};

#endif