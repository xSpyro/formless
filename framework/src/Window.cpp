
#include "Window.h"

#include "Input.h"

Window::Window()
	: log("Window")
{
	hwnd = NULL;
	width = 640;
	height = 480;

	input.assign(INPUT_BUFFER_SIZE, INPUT_STATE_UP);

	cmdShow = 1;
	hinstance = NULL;
}

Window::~Window()
{
}

int Window::getWidth() const
{
	return width;
}

int Window::getHeight() const
{
	return height;
}

HWND Window::getHandle()
{
	return hwnd;
}

void Window::setStartupData(int show, HINSTANCE inst)
{
	cmdShow = show;
	hinstance = inst;
}

void Window::init(int width, int height, const std::string & title)
{
	this->width = width;
	this->height = height;
	this->title = title;
}

bool Window::open()
{
	// here we create the window

	WNDCLASSEX wc;

    //Step 1: Registering the Window Class
    wc.cbSize        = sizeof(WNDCLASSEX);
    wc.style         = 0;
    wc.lpfnWndProc   = UpdateCB;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = hinstance;
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
   // wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hCursor		 = LoadCursorFromFile("../resources/textures/trio normal.cur");
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW);
    wc.lpszMenuName  = NULL;
	wc.lpszClassName = "FormlessProc";
    wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

    if (!RegisterClassEx(&wc))
    {
        log.fatal("RegisterClass: ", LOG_FAILED);
        return false;
    }
	else
	{
		log.notice("RegisterClass: ", LOG_OK);
	}

	// adjust the window to create exactly the correct size of the internal window
	RECT rect = {0, 0, width, height};

	AdjustWindowRectEx(&rect, WS_OVERLAPPEDWINDOW, false, 0);
	int adjusted_width = rect.right - rect.left;
	int adjusted_height = rect.bottom - rect.top;


    // Step 2: Creating the Window
    hwnd = CreateWindow(
        "FormlessProc",
        title.c_str(),
        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
        CW_USEDEFAULT, CW_USEDEFAULT, 
		adjusted_width, adjusted_height,
        NULL, NULL, hinstance, NULL);

	if (hwnd == NULL)
	{
		log.fatal("CreateWindow: ", LOG_FAILED);
		return false;
	}
	else
	{
		log.notice("CreateWindow: ", LOG_OK);
		log.info("Dimensions: ", width, "x", height);
		log.info("Border Dimensions: ", adjusted_width, "x", adjusted_height);
	}

	// assign userdata
	SetWindowLong(hwnd, GWL_USERDATA, reinterpret_cast < int > (this));

	// to prevent startup exeception
	//ShowWindow(hwnd, cmdShow);

    UpdateWindow(hwnd);

	return true;
}

void Window::close()
{
	DestroyWindow(hwnd);
	log("WindowDestroyed: ", LOG_OK);
}

bool Window::isOpen() const
{
	return IsWindow(hwnd) == 1;
}

bool Window::isInputDown(int index) const
{
	return (input[index] & INPUT_STATE_DOWN) > 0;
}

bool Window::isInputPressed(int index) const
{
	return (input[index] & INPUT_STATE_PRESSED) > 0;
}

bool Window::isInputReleased(int index) const
{
	return (input[index] & INPUT_STATE_RELEASED) > 0;
}

void Window::getMouseVector(float * x, float * y) const
{
	int w = 0;
	int h = 0;

	if (hwnd)
	{
		RECT rect;
		GetClientRect(hwnd, &rect);

		w = rect.right - rect.left;
		h = rect.bottom - rect.top;
	}
	else
	{
		w = width;
		h = height;
	}

	float tx = mx / static_cast < float > (w);
	float ty = my / static_cast < float > (h);

	*x = (tx * 2) - 1;
	*y = ((1 - ty) * 2) - 1;
}

void Window::process()
{
	// clear old records
	for (InputState::iterator i = input.begin(); i != input.end(); ++i)
	{
		int & v = *i;
		v = v & 1;
	}

	MSG msg;
	while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
        DispatchMessage(&msg);
	}
}

void Window::injectInput(int sym, int state)
{
	handleInput(sym, state);
}

void Window::injectMouseMovement(int x, int y, float dx, float dy)
{
	float real_x = ((dx + 1) / 2.0f) * width;
	float real_y = ((dy + 1) / 2.0f) * height;

	handleMouseMovement((int)real_x, (int)real_y);
}

void Window::handleInput(int key, int state)
{
	// if we pressed
	if (state == INPUT_STATE_DOWN)
	{
		if ((input[key] & INPUT_STATE_DOWN) == 0)
		{
			input[key] |= INPUT_STATE_PRESSED;
		}

		input[key] += 1;
	}
	else
	{
		input[key] = 0;
	}

	onInput(key, state);
}

void Window::handleMouseMovement(int x, int y)
{
	mx = x;
	my = y;

	onMouseMovement(x, y);
}



LRESULT CALLBACK Window::UpdateCB(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	

	// read userdata
	Window * me = reinterpret_cast < Window* > (GetWindowLong(hwnd, GWL_USERDATA));

	// this is because the first messages will be before we have set the userdata handle
	if (me == NULL)
	{
		return DefWindowProc(hwnd, msg, wparam, lparam);
	}

    switch (msg)
    {

	case WM_MOUSEMOVE:

		me->handleMouseMovement(LOWORD(lparam), HIWORD(lparam));
		break;

	case WM_MBUTTONDOWN:
		me->handleInput(MB_MIDDLE, INPUT_STATE_DOWN);
		break;

	case WM_MBUTTONUP:
		me->handleInput(MB_MIDDLE, INPUT_STATE_UP);
		break;

	case WM_XBUTTONDOWN:
		if (wparam & MK_XBUTTON1)
			me->handleInput(MB_EXTRA, INPUT_STATE_DOWN);
		if (wparam & MK_XBUTTON2)
			me->handleInput(MB_EXTRA2, INPUT_STATE_DOWN);
		break;

	case WM_XBUTTONUP:
		if (!(wparam & MK_XBUTTON1))
			me->handleInput(MB_EXTRA, INPUT_STATE_UP);
		if (!(wparam & MK_XBUTTON1))
			me->handleInput(MB_EXTRA2, INPUT_STATE_UP);
		break;

	case WM_MOUSEWHEEL:
		{
			short temp = HIWORD(wparam);
			temp >>= 8;
			//std::cout << (temp >> 8) << (temp) << std::endl;
			int s = (temp >= 0) ? INPUT_SCROLL_UP : INPUT_SCROLL_DOWN;
			if (s == 1)
			{
				me->handleInput(MB_WHEEL_UP, INPUT_STATE_DOWN);
			}
			else
			{
				me->handleInput(MB_WHEEL_DOWN, INPUT_STATE_DOWN);
			}

			me->handleInput(MB_MOUSEWHEEL, s);
		}
		break;

	case WM_LBUTTONDOWN:

		me->handleInput(MB_LEFT, INPUT_STATE_DOWN);
		break;

	case WM_RBUTTONDOWN:

		me->handleInput(MB_RIGHT, INPUT_STATE_DOWN);
		break;

	case WM_LBUTTONUP:

		me->handleInput(MB_LEFT, INPUT_STATE_UP);
		break;

	case WM_RBUTTONUP:

		me->handleInput(MB_RIGHT, INPUT_STATE_UP);
		break;
		

	case WM_KEYDOWN:

		me->handleInput(wparam, INPUT_STATE_DOWN);
		break;

	case WM_KEYUP:

		me->handleInput(wparam, INPUT_STATE_UP);
		break;
		
    case WM_CLOSE:
        DestroyWindow(hwnd);
		break;

    case WM_DESTROY:
        PostQuitMessage(0);
		break;

	case WM_SIZE:
		RECT rect;
		GetWindowRect(hwnd, &rect);


		me->onResizeWindow(rect.right, rect.bottom);

		break;
    default:
        return DefWindowProc(hwnd, msg, wparam, lparam);
    }
    return 0;
}