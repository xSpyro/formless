
#include "SkySphere.h"

#include "Renderer.h"
#include "Mesh.h"

#include "MeshStream.h"

bool SkySphere::generate(Renderer * renderer, BaseShader * shader, Desc * desc, Mesh ** out)
{

	// create a sphere with th

	Desc opt;
	opt.scale = 1.0f;
	opt.wireframe = false;

	if (desc)
	{
		opt = *desc;
	}

	int columns = 8;
	int rows = 8;
	int n_face;

	if (opt.wireframe)
	{
		n_face = 8;
	}
	else
	{
		n_face = 6;
	}

	unsigned int index = 0;
	unsigned int vertices = (columns + 1) * rows;
	unsigned int indices = (columns + 1) * (rows + 1) * n_face;


	*out = renderer->mesh()->create();
	Mesh * mesh = *out;

	MeshStream * stream = mesh->createEmptyStream("SkyStream");

	stream->addDesc("POSITION", sizeof(Vector3));
	stream->addDesc("NORMAL", sizeof(Vector3));
	stream->addDesc("TEXCOORD", sizeof(Vector2));

	stream->fill(vertices);
	
	float wc = 1 * M_PI / columns;
	float wr = 2 * M_PI / rows;

	for (int c = 0; c <= columns; ++c)
	{
		for (int r = 0; r < rows; ++r)
		{
			float y = cosf(c * wc);
			float x = cosf(r * wr) * sinf(c * wc);
			float z = sinf(r * wr) * sinf(c * wc);

			Vertex * vertex = reinterpret_cast < Vertex* > (stream->get(index));

			vertex->position = Vector3(x, y, z) * opt.scale;
			vertex->normal = Vector3(x, y, z);

			index += 1;
		}
	}

	if (opt.wireframe)
	{
		for (int y = 0; y < columns; ++y)
		{
			for (int x = 0; x < rows - 1; ++x)
			{
			
				mesh->addIndex((x) + (y) * rows);
				mesh->addIndex((x + 1) + (y) * rows);
				mesh->addIndex((x + 1) + (y) * rows);
				mesh->addIndex((x + 1) + (y + 1) * rows);
				mesh->addIndex((x + 1) + (y + 1) * rows);
				mesh->addIndex((x) + (y + 1) * rows);
				mesh->addIndex((x) + (y + 1) * rows);
				mesh->addIndex((x) + (y) * rows);
			}

		
			// stitch
			int x = rows - 1;

			mesh->addIndex((x) + (y) * rows);
			mesh->addIndex((0) + (y) * rows);
			mesh->addIndex((0) + (y) * rows);
			mesh->addIndex((0) + (y + 1) * rows);
			mesh->addIndex((0) + (y + 1) * rows);
			mesh->addIndex((x) + (y + 1) * rows);
			mesh->addIndex((x) + (y + 1) * rows);
			mesh->addIndex((x) + (y) * rows);
		}

		mesh->setTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	}
	else
	{
		for (int y = 0; y < columns; ++y)
		{
			for (int x = 0; x < rows - 1; ++x)
			{
			
				mesh->addIndex((x) + (y) * rows);
				mesh->addIndex((x + 1) + (y) * rows);
				mesh->addIndex((x + 1) + (y + 1) * rows);
				mesh->addIndex((x + 1) + (y + 1) * rows);
				mesh->addIndex((x) + (y + 1) * rows);
				mesh->addIndex((x) + (y) * rows);
			}

		
			// stitch
			int x = rows - 1;

			mesh->addIndex((x) + (y) * rows);
			mesh->addIndex((0) + (y) * rows);
			mesh->addIndex((0) + (y + 1) * rows);
			mesh->addIndex((0) + (y + 1) * rows);
			mesh->addIndex((x) + (y + 1) * rows);
			mesh->addIndex((x) + (y) * rows);
		}

		mesh->setTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	}

	
	mesh->submit(shader);

	return true;
}