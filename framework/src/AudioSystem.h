
#ifndef AUDIO_SYSTEM
#define AUDIO_SYSTEM

#include "Common.h"

#include "Logger.h"

#include "Vector3.h"

class AudioSystem
{
	typedef std::map < std::string, FMOD::Sound* > SoundMap;
	typedef std::map < int, std::string > ChannelIDMap;
	typedef std::list < FMOD::Channel* > ChannelList;

public:

	AudioSystem();
	~AudioSystem();

	int		play(const std::string & resource, float volume, bool loop);
	int		play3D(const std::string & resource, const Vector3 & position, float volume, bool loop, bool notSilent = false);
	void	stop(const int id);

	// stops all songs with a resource
	void	stop(const std::string & resource);
	void	stopAllSounds();

	/*
	*	The song needs to be played before it can be faded! 
	*/
	void	fadeIn(const int id, const float volume);
	void	fadeOut(const int id, const float time);

	void	setListener(const Vector3 & position, const Vector3 & orientation, const Vector3 & up);

	void	update();

	// mutes and unmutes a channel
	void	mute(const int id);
	void	unmute(const int id);

	// mutes and unmutas all existing channels
	void	muteAllSounds();
	void	unmuteAllSounds();

	// set volume to a channel
	void	setVolume(const int id, float volume);

	// returns the length of a song from a name
	int		getSoundLengthMS(const std::string & resource);

	// returns the length of a song currently playing in the channelID
	int		getSoundLengthMS(const int id);

	// returns the position where the channel currently is playing the song at
	int		getPlayingSoundMS(const int id);

	FMOD::Channel* getChannel(const int id);

	FMOD::System* getSystem() const;

	FMOD::Sound * query(const std::string & resource, int extras = 0);

	bool	isPlaying(const int id);
	bool	isPlaying(const std::string& song);

	const bool isMuted() const;

	void setPositionForChannel(const int id, const unsigned int position);


private:

	static FMOD_RESULT F_CALLBACK channelCB(FMOD_CHANNEL * channel, FMOD_CHANNEL_CALLBACKTYPE type, void * c1, void * c2);

	const int isChannelReady();
private:

	Logger log;

	FMOD::System * system;

	Vector3 listener;

	SoundMap sounds;

	ChannelIDMap channelIDs;

	// stores a channel and a volume
	std::map<FMOD::Channel*, float> inFades;
	std::map<FMOD::Channel*, float> outFades;

	bool muted;
};

#endif