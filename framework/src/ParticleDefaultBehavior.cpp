

#include "ParticleDefaultBehavior.h"

void ParticleDefaultBehavior::spawn(Particle & particle) const
{
	particle.position = Vector3(
		-1 + (rand() / (float)RAND_MAX) * 2, 
		-1 + (rand() / (float)RAND_MAX) * 2, 
		-1 + (rand() / (float)RAND_MAX) * 2) * 0.004f;
	particle.life = 1.0f;
	particle.scale = 0.0f;

	particle.velocity = Vector3(
		-1 + (rand() / (float)RAND_MAX) * 2, 
		-1 + (rand() / (float)RAND_MAX) * 2,
		-1 + (rand() / (float)RAND_MAX) * 2) * 0.003f;
}

void ParticleDefaultBehavior::process(Particle & particle) const
{
	particle.position.x += particle.velocity.x;
	particle.position.y += particle.velocity.y;
	particle.position.z += particle.velocity.z;

	particle.life -= 0.018f;
	particle.scale = 0.2f + particle.life * 0.8f;
}