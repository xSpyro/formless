
#ifndef VIEWPORT
#define VIEWPORT

#include "Common.h"

#include "Effect.h"

class Renderer;
class Camera;
class Texture;
class ConstantBuffer;

class Renderable;
class Model;

enum
{
	VIEWPORT_DEFAULT,
	VIEWPORT_SCREEN_AND_TEXTURE
};

class Viewport
{
public:
	
	Viewport(Renderer * renderer, Camera * camera, Texture * target = NULL, int flags = VIEWPORT_DEFAULT);

	Effect * getEffect();

	void setShaders(BaseShader * vs, BaseShader * gs, BaseShader * ps);
	void setSorting(int sort);
	void setVisible(bool state);

	bool isVisible() const;
	bool hasSortOp() const;

	void prepare();

	void setTarget(Texture * target);
	Texture * getTarget() const;

	static void prepareCamera(Renderer * renderer, Camera * camera);

	// utility

	typedef std::vector < const Renderable* > SortIn;
	typedef std::map < float, const Renderable* > SortByCameraOut;

	void sortOp(const SortIn & in, SortByCameraOut & result) const;

private:

	static ConstantBuffer * getConstantBuffer(Renderer * renderer);

private:

	// viewport owns the effect
	// and the effect is just a container to a
	// vs, ps and gs
	Effect effect;

	Camera * camera;
	Texture * target;
	int flags;
	bool visible;
	Renderer * renderer;

	int sort;
};

#endif