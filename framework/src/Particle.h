
#ifndef PARTICLE
#define PARTICLE

#include "Common.h"

#include "Vector3.h"
#include "Color.h"

struct Particle
{
	Vector3 position;
	Vector3 velocity;
	Color color;
	
	float scale;
	float angle;
	float oangle;
	float life;
};

#endif