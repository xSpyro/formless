
#ifndef WINDOW
#define WINDOW

#include "Common.h"

#include "Logger.h"

class Window
{

	typedef std::vector < int > InputState;

public:

	Window();
	virtual ~Window();

	int getWidth() const;
	int getHeight() const;
	HWND getHandle();

	void setStartupData(int show, HINSTANCE inst);
	void init(int width, int height, const std::string & title);

	bool open();
	void close();

	bool isOpen() const;

	bool isInputDown(int index) const;
	bool isInputPressed(int index) const;
	bool isInputReleased(int index) const;

	void getMouseVector(float * x, float * y) const;

	void process();

	void injectInput(int sym, int state);
	void injectMouseMovement(int x, int y, float dx, float dy);

	virtual void onInput(int key, int state) { }
	virtual void onMouseMovement(int x, int y) { }

	virtual void onResizeWindow(unsigned int width, unsigned int height) { }


	static LRESULT CALLBACK UpdateCB(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);

private:

	void handleInput(int key, int state);
	void handleMouseMovement(int x, int y);

private:

	Logger log;

	HWND hwnd;

	int cmdShow;
	HINSTANCE hinstance;


	int width;
	int height;
	int mx, my;

	std::string title;


	// input
	InputState input;
};

#endif