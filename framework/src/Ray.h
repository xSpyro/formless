
#ifndef RAY
#define RAY

#include "Vector3.h"

struct IntersectResult;
struct Sphere;
struct Plane;

struct Ray
{
	Vector3 origin;
	Vector3 direction;


	bool intersect(const Plane & plane, IntersectResult * result) const;

	bool intersect(const Sphere & sphere, IntersectResult * result) const;
};

#endif