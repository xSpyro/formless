
#ifndef __RECT
#define __RECT

#include "Vector2.h"

struct Rect
{
	float x, y, width, height;

	Rect();
	Rect(float x, float y, float width, float height);
};

#endif