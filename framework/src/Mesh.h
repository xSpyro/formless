
#ifndef MESH
#define MESH

#include "Common.h"

// mesh is a collection of static meshes
// and must be supplied with a set of weights to be able to animate

#include "MeshStream.h"
#include "Sphere.h"

class Renderer;
class BaseShader;

struct Ray;
struct IntersectResult;

enum
{
	MESH_STREAM_OUT = 1,
	MESH_SWAP_RENDER = 2
};

class Mesh
{
	typedef std::map < std::string, MeshStream* > StreamMap;

	typedef std::vector < D3D11_INPUT_ELEMENT_DESC > LayoutDesc;
	typedef std::vector < ID3D11Buffer* > BufferVector;
	typedef std::vector < unsigned int > StrideVector;

	typedef std::vector < unsigned int > IndexVector;

	struct BoundingData
	{
		Sphere sphere;
		Vector3 bb[2];
	};

	typedef std::vector < BoundingData > Boundings;

public:

	Mesh(Renderer * renderer);
	~Mesh();

	void release();

	void createSwapMesh(Mesh * m);
	void swap();

	void appendStreamData(void * data, unsigned int size);

	MeshStream * createEmptyStream(const std::string & name, int flags = MESH_STREAM_DEFAULT);
	MeshStream * createSingleStream(const std::string & semantic, int stride, int flags = MESH_STREAM_DEFAULT);
	MeshStream * createPositionStream(const std::string & semantic, int stride);

	MeshStream * getStream(const std::string & semantic);
	const MeshStream * getStream(const std::string & semantic) const;

	MeshStream * getFirstStream();

	void addIndex(unsigned int index);
	unsigned int getIndexCount() const;
	unsigned int getVertexCount() const;
	unsigned int getMorphCount() const;
	unsigned int getMorphVertexCount() const;

	Renderer * getRenderer() const;

	void submit(BaseShader * shader);
	void update();
	void clear();

	void bind() const;
	void bind(int stream1, int stream2) const;

	unsigned int streamFrom(const Mesh * src, unsigned int in) const;
	void streamFromAuto(const Mesh * src) const;

	void draw() const;
	void draw(unsigned int n) const;

	void setTopology(int type);



	// helpers

	bool pick(const Ray & ray, IntersectResult * result) const;

	bool pick2(const Ray & ray, IntersectResult * result, int stream1, int stream2, float delta) const;

	// boundings

	Sphere getBoundingSphere(int stream1, int stream2, float delta) const;

	float getXZRadius(int stream1, int stream2, float delta) const;

private:

	DXGI_FORMAT getDescFormat(unsigned int stride);

private:

	Renderer * renderer;
	StreamMap streams;
	MeshStream * positions; // stream that contains the positions

	LayoutDesc desc;
	ID3D11InputLayout * layout;

	BufferVector buffers;
	StrideVector strides;

	// only one index vector
	// since all parts will have the same layout

	// swap data
	Mesh * swapmesh;
	bool swapme;

	mutable bool first;

	
	IndexVector indices;
	ID3D11Buffer * index_buffer;

	int flags;
	int topology;

	Boundings boundings;
};

#endif