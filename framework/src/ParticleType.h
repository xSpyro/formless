
#ifndef PARTICLE_TYPE
#define PARTICLE_TYPE

#include "Common.h"

#include "Particle.h"
#include "ParticleEmitter.h"

class Renderer;
class Mesh;
class ParticleBehavior;
class BaseShader;

class ParticleType
{
	typedef std::vector < Particle > ParticleContainer;

	typedef std::map < int, ParticleEmitter* > ParticleEmitters;

public:

	void create(Renderer * renderer, BaseShader * shader);

	ParticleEmitter * addEmitter(const ParticleBehavior * behavior);
	bool kill(ParticleEmitter * emitter);
	bool kill();


	void update();

	Mesh * getMesh();
	MeshStream * getStream();

	//virtual void spawn(Particle & particle);
	//virtual void process(Particle & particle);

private:

	bool getFreeParticleBatch(int * start, int * end);

	void killParticles(int start, int end);

private:

	MeshStream * stream;
	Mesh * mesh;

	ParticleEmitters emitters;
};

#endif