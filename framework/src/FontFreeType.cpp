
#include "FontFreeType.h"

FontFreeType::FontFreeType()
	: log("FontFreeType")
{
	library = NULL;

	init();
}

FontFreeType::~FontFreeType()
{
	FT_Done_FreeType(library);
}

bool FontFreeType::init()
{
	FT_Error error;
	error = FT_Init_FreeType(&library ); 
	
	if (error)
	{
		log.alert("Failed to init");
		return false;
	}
	else
	{
		log("Created successfully");
		return true;
	}
}

bool FontFreeType::createFontDataFromFile(File * file, Font * font, int height)
{
	FT_Face face; /* handle to face object */  
	FT_Error error;
	
	error = FT_New_Memory_Face(library, (const FT_Byte*)file->data, file->size, 0, &face); 

	if (error)
	{
		log.alert("NewMemoryFace: ", LOG_FAILED);
		return false;
	}

	log("  * Number of glyps ", face->num_glyphs);


	// data
	font->height = height;

	// now in the future we might do a batch of different font sizes here and
	// and create mipmapping from them
	// like for example sizes
	// 8, 16, 32, 64
	parseFont(face, font->height, font->surface, font->glyphs);

	FT_Done_Face(face);

	return true;
}

bool FontFreeType::parseFont(FT_Face face, int height, Surface & surface, Glyphs & glyphs)
{
	FT_Set_Pixel_Sizes(face, 0, height); 

	// pre process
	int size;
	preProcessFace(face, height, &size);

	// resize will clear aswell
	surface.resize(size, size, Color(1, 1, 1, 0));

	log("Surface size ", size, "x", size);

	Vector2 offset(0, 0);
	Vector2 spacing(1, 1);

	Surface glyph;
	
	for (int n = 32; n < 127; ++n) 
	{ 
		// load char data
		FT_Load_Char(face, n, FT_LOAD_RENDER);

		// copy to our surface
		const FT_Bitmap & bitmap = face->glyph->bitmap;

		glyph.create(bitmap.width, bitmap.rows, Color(0, 0, 0, 0));
		glyph.blit(bitmap.buffer, bitmap.width, bitmap.rows, BLIT_GRAYSCALE_8);

		// now blit this glyph surface at the correct position in the big surface
		Rect metrics = glyphMetrics(face, height);

		// check if we need a newline
		if (offset.x + spacing.x + metrics.width >= surface.getWidth())
		{
			offset.x = 0;
			offset.y += height + spacing.y;
		}

		// blit it
		surface.blitAlpha(glyph, (int)(offset.x + metrics.x), (int)(offset.y + metrics.y));

		// mapping
		float mx = static_cast < float > (offset.x + metrics.x) / surface.getWidth();
		float my = static_cast < float > (offset.y + metrics.y) / surface.getHeight();
		float mw = static_cast < float > (metrics.width) / surface.getWidth();
		float mh = static_cast < float > (metrics.height) / surface.getHeight();


		FontGlyphMetric gm;

		gm.coord = Rect(mx, my, mw, mh);
		gm.offset = Vector2(static_cast < float > (metrics.x), static_cast < float > (metrics.y));
		gm.dim = Vector2(static_cast < float > (metrics.width), static_cast < float > (metrics.height));
		gm.advance = Vector2(static_cast < float > (face->glyph->advance.x >> 6), 0);

		glyphs.push_back(gm);

		// increase offset
		offset.x += metrics.width + spacing.x;		
	}

	return true;
}

bool FontFreeType::preProcessFace(FT_Face face, int height, int * p)
{
	int d = 0;

	// loop the glyphs we want and see how much space they take
	for (int i = 32; i < 127; ++i)
	{
		FT_Load_Glyph(face, i, FT_LOAD_DEFAULT); 
		Rect rect = glyphMetrics(face, height);

		d += (int)rect.width + 3;
	}

	// decide the best texture size power of 2
	int x = 64;


	while ( ((d / x) * height) >= x )
	{
		x *= 2;
	}

	*p = x;

	return true;
}

Rect FontFreeType::glyphMetrics(FT_Face face, int height)
{
	// glyph will now be updated with the loaded glyph
	FT_GlyphSlot slot = face->glyph; 

	//int height = (face->height >> 6) + (face->ascender >> 6);
	const FT_Glyph_Metrics & metrics = face->glyph->metrics;

	return Rect(0,  (float)(height - (metrics.horiBearingY >> 6)), 
		(float)(metrics.width >> 6), (float)(metrics.height >> 6));
}