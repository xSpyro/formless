
#ifndef _LIBRARY

#include "AppMain.h"

#include "Layer.h"
#include "Renderable.h"

#include "FileSystem.h"
#include "Effect.h"

#include "Viewport.h"

#include "BaseShader.h"

#include "Mesh.h"
#include "MeshStream.h"
#include "Vector3.h"
#include "Vector2.h"
#include "Color.h"

#include "Model.h"
#include "ModelData.h"

#include "Texture.h"
#include "Surface.h"

#include "Camera.h"

#include "ParticleSystem.h"

#include "Input.h"

#include "AnimationGroup.h"

#include "HeightMap.h"
#include "SkySphere.h"

#include "IntersectResult.h"

#include "ConstantBuffer.h"


AppMain::AppMain()
	: log("Main")
{
}

int AppMain::run(int argc, char ** argv)
{
	log("Application Started");
	log.notice("CommandList Arguments");

	for (int i = 0; i < argc; ++i)
		log.notice("Arg ", i, ": ", argv[i]);


	init(1280, 720, "Barren Lands");
	if (!open())
	{
		return 1;
	}

	if (!renderer.join(this))
	{
		return 1;
	}

	while ( isOpen() )
	{
		process();

		if (timeframe.accumulate(1.0f / 60.0f))
		{
	
		}

		renderer.clear(NULL);
		renderer.render();
		renderer.present();
	}

	log("Exit: ", LOG_OK);

	return 0;
}

void AppMain::onMouseMovement(int x, int y)
{

}

void AppMain::onInput(int key, int state)
{

}

#endif