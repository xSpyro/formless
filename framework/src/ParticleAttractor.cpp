
#include "ParticleAttractor.h"

#include "Particle.h"

ParticleAttractor::ParticleAttractor()
	: range(1.0f)
{
}

ParticleAttractor::ParticleAttractor(const Vector3 & position, float range)
	: position(position), range(range * range)
{
	if (range < 0)
		this->range = -this->range;
}

void ParticleAttractor::setPosition(const Vector3 & position)
{
	this->position = position;
}

void ParticleAttractor::setRange(float range)
{
	this->range = range;
}

void ParticleAttractor::apply(Particle * particle)
{
	// get distance
	Vector3 to_me = (position - particle->position);
	float dist = to_me.lengthNonSqrt();

	if (dist <= 0)
		dist = 0.0001f;

	float force = std::max(std::min(range / dist, 10.0f), -10.0f);

	to_me = to_me / sqrt(dist);

	particle->velocity += to_me * force * 0.001f;
}