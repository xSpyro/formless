
#ifndef PARTICLE_DEFAULT_BEHAVIOR
#define PARTICLE_DEFAULT_BEHAVIOR

#include "Common.h"

#include "ParticleBehavior.h"

class ParticleDefaultBehavior : public ParticleBehavior
{
public:

	void spawn(Particle & particle) const;
	void process(Particle & particle) const;
};

#endif