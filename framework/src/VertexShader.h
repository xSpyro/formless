
#ifndef VERTEX_SHADER
#define VERTEX_SHADER

#include "Common.h"

#include "File.h"
#include "BaseShader.h"

// might not look like this at all

class VertexShader : public BaseShader
{

public:

	VertexShader();

	bool load(Renderer * renderer, File * file, int flags);

	void apply();
	static void unbind(Renderer * renderer);

	void setConstantBuffer(int slot, ConstantBuffer * buffer);

private:

	ID3D11VertexShader * shader;

};

#endif