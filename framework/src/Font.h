
#ifndef FONT
#define FONT

#include "Common.h"

#include "FontGlyphMetric.h"
#include "Surface.h"

#include "Renderable.h"

class Texture;
class Mesh;
class Renderer;
class BaseShader;

class Layer;

class Font
{
	typedef std::vector < FontGlyphMetric > Glyphs;

	struct Vertex
	{
		Vector2 position;
		Vector2 uv;
	};

public:

	Font();

	Mesh * create(Renderer * renderer, BaseShader * vs);

	void draw(Mesh * mesh, const std::string & text);

	// intermediate drawing
	// not optimized
	void draw(Layer * layer, const std::string & text, float x, float y);

	void setBlendColor(const Color & color);

	float width(const std::string & text);


//private:

	int height;
	Glyphs glyphs;
	Texture * texture;
	Surface surface;

	// hmm might optimize this and store
	// it on another place
	Renderable renderable;

	Mesh * mesh;
};

#endif