
#include "TextureStore.h"

#include "FileSystem.h"

#include "Texture.h"

#include "Renderer.h"

TextureStore::TextureStore()
	: log("Textures")
{
}

TextureStore::~TextureStore()
{
	clear();
}

void TextureStore::init(Renderer * renderer)
{
	this->renderer = renderer;
}

Texture * TextureStore::get(const std::string & resource, int flags)
{

	// check for existing
	TextureMap::iterator i = textures.find(resource);
	if (i != textures.end())
	{
		return i->second;
	}


	File file;

	if (FileSystem::get(resource, &file))
	{
		Texture * texture = new Texture;
		if (texture->load(renderer, &file, flags))
		{
			// store texture
			store(resource, texture);

			return texture;
		}
		else
		{
			log.alert("Loaded \"", resource, "\": ", LOG_FAILED);

			// free memory
			delete texture;
		}
	}

	return NULL;
}

Texture * TextureStore::create(int width, int height, int flags)
{
	Texture * texture = new Texture;
	if (texture->create(renderer, width, height, flags))
	{
		// store texture
		std::stringstream format;
		format << "__generated" << reinterpret_cast < int > (texture);
		store(format.str(), texture);

		return texture;
	}
	else
	{
		// free memory
		delete texture;
	}

	return NULL;
}

void TextureStore::free(Texture * texture)
{
	for (TextureMap::iterator i = textures.begin(); i != textures.end(); ++i)
	{
		if (i->second == texture)
		{
			// remove from store
			log.notice("Freed \"", i->first, "\": ", LOG_OK);
			textures.erase(i);

			// free memory
			texture->free();

			Texture * addon = texture->getDepthAddon();
			if (addon)
			{
				free(addon);
			}

			delete texture;

			break;
		}

		
	}

}

void TextureStore::reload()
{
	for (TextureMap::iterator i = textures.begin(); i != textures.end(); ++i)
	{
		Texture * texture = i->second;

		File file;

		if (FileSystem::get(i->first, &file))
		{
			if (texture->load(renderer, &file, texture->getFlags()))
			{
			}
			else
			{
			}
		}
	}
}

void TextureStore::clear()
{
	for (TextureMap::iterator i = textures.begin(); i != textures.end(); ++i)
	{
		Texture * texture = i->second;

		texture->free();
		log.notice("Freed \"", i->first, "\": ", LOG_OK);
	}

	textures.clear();
}

void TextureStore::store(const std::string & name, Texture * texture)
{
	// check for duplicate
	TextureMap::iterator i = textures.find(name);
	if (i == textures.end())
	{
		textures[name] = texture;

		log.notice("Loaded \"", name, "\": ", LOG_OK);

		renderer->onLoadAsset(name, 1);
	}
}