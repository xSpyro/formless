
#ifndef PIXEL_SHADER
#define PIXEL_SHADER

#include "Common.h"

#include "BaseShader.h"

class Texture;

class PixelShader : public BaseShader
{
	typedef std::map < int, Texture* > BindTextures;

public:

	PixelShader();

	bool load(Renderer * renderer, File * file, int flags = 0);

	void apply();
	static void unbind(Renderer * renderer);

	void setSamplers();
	void setTexture(int slot, Texture * texture);
	void bindTexture(int slot, Texture * texture);

	void setConstantBuffer(int slot, ConstantBuffer * buffer);

private:

	ID3D11PixelShader * shader;

	BindTextures binds;
};

#endif