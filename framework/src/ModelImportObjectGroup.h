
#ifndef MODEL_IMPORT_OBJECT_GROUP
#define MODEL_IMPORT_OBJECT_GROUP

#include "ModelImportObjectStream.h"

#include "Vector3.h"
#include "Vector2.h"

#include "Mesh.h"

class ModelImportObjectGroup
{

	typedef ModelImportObjectStream Stream;

	struct Face
	{
		unsigned int v[3];
		unsigned int vt[3];
		unsigned int vn[3];
	};

	struct FaceTemp
	{
		unsigned int v, vt, vn;
	};

public:

	ModelImportObjectGroup(const std::string & name);

	void parse(Stream & stream);

	void parseV(Stream & stream);
	void parseVN(Stream & stream);
	void parseVT(Stream & stream);
	void parseF(Stream & stream);
	void parseMaterial(Stream & stream);

	void printStats();

	Mesh * createMesh(Renderer * renderer, ModelImportObjectGroup * def);

	

private:
public:
	std::string name;
	std::string material;


	Mesh * mesh;

	bool has_material;

	std::vector < Vector3 > positions;
	std::vector < Vector3 > normals;
	std::vector < Vector2 > texcoords;

	std::vector < Face > faces;
};

#endif