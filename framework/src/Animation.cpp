
#include "Animation.h"

Animation::Animation()
	: src(0), dst(0), lerp(0)
{
}

Animation::Animation(int src, int dst, float lerp)
	: src(src), dst(dst), lerp(lerp)
{
}