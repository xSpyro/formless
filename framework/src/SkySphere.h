
#ifndef SKYSPHERE
#define SKYSPHERE

#include "Vector3.h"
#include "Vector2.h"

class Renderer;
class BaseShader;
class Mesh;

class SkySphere
{
	struct Vertex
	{
		Vector3 position;
		Vector3 normal;
		Vector2 texcoord;
	};

public:

	struct Desc
	{
		float scale;
		bool wireframe;
	};

	static bool generate(Renderer * renderer, BaseShader * shader, Desc * desc, Mesh ** out);
};

#endif