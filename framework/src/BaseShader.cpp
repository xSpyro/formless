
#include "BaseShader.h"

#include "Logger.h"

#include "ShaderInclude.h"

BaseShader::BaseShader()
	: compiled(NULL)
{
}

BaseShader::~BaseShader()
{
}


bool BaseShader::compile(File * file, const std::string & version)
{
	HRESULT hr;
	ID3D10Blob * errors = NULL;

	unsigned int flags = 0;
#ifndef NDEBUG
	//flags |= D3D10_SHADER_DEBUG;
#endif

	//flags |= D3D10_SHADER_DEBUG;
	
	// compile the vertex shader from the file
	hr = D3D10CompileShader(file->data, file->size, "", NULL, ShaderInclude::get(), 
		"main", version.c_str(), flags, &compiled, &errors); 

	if (FAILED(hr))
	{
		if (errors)
			Logger::print().alert("Shader Error: ", (char *)errors->GetBufferPointer());
		return false;
	}
	else
	{

	}

	return true;
}

DWORD * BaseShader::getCompiledChunk()
{
	return reinterpret_cast < DWORD* > (compiled->GetBufferPointer());
}

SIZE_T BaseShader::getCompiledChunkSize() const
{
	return compiled->GetBufferSize();
}

Renderer * BaseShader::getRenderer()
{
	return renderer;
}