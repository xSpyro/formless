
#ifndef LAYER
#define LAYER

#include "Common.h"

#include "Matrix.h"
#include "Color.h"

#include "Stage.h"

// for now put renderables here
class Renderable;
class Model;
class Renderer;
class Viewport;
class Camera;
class Effect;
class Mesh;
class Texture;
class VertexShader;

struct Vector2;
struct Vector3;

struct Ray;
struct IntersectResult;

class Layer : public Stage
{
	typedef std::vector < const Renderable* > RenderableContainer;
	typedef std::vector < Viewport* > ViewportContainer;

	struct PerRenderable
	{
		Matrix model;
		Matrix normal;
		float lerp;
		unsigned int has_texture;
		unsigned int vertices;
		float lol[1];
		Color blend;
	};

public:

	Layer();
	Layer(Renderer * renderer);

	void init(Renderer * renderer);

	void add(const Renderable * renderable);
	void add(const Model * model);

	void remove(const Renderable * renderable);
	void remove(const Model * model);

	void clear();

	Viewport * addViewport(Camera * camera, Texture * target = NULL, int flags = 0);

	const Renderable * pick(const Ray & ray, IntersectResult * result) const;

	float pickY(const Vector3 & position, Vector2 * uv) const;

	void debug();

	// will force the model to be drawn instantly
	void intermediate(const Model * model);

	void apply();

	void setVisible(bool state);

	void getRenderables(RenderableContainer& renderables);

	unsigned int getCount() const;
	unsigned int getVisibleCount() const;

	const bool isVisible() const;

	unsigned int performance() const;

private:

	void render(const Renderable * renderable, Effect * effect);
	void debug(const Renderable * renderable, Effect * effect);

	void updateShadersPerRenderable(const Renderable * renderable, float lerp, const Matrix & offset);
	void updateShadersPerRenderableWithoutModel(const Renderable * renderable);

private:

	Renderer * renderer;

	// viewports are how we see the renderables
	// i.e what camera sees them and what to render to
	ViewportContainer viewports;


	// container of renderables
	RenderableContainer renderables;

	// debug sphere mesh
	Mesh * mesh_sphere;
	bool debugging;

	bool visible;
};

#endif