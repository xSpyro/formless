#include "Intersect.h"

bool Intersect::intersect(const Ray& ray, const Plane& plane, IntersectResult* res)
{
	/*
	so R(t) = R0 + t * Rd , t > 0

	A(X0 + Xd * t) + B(Y0 + Yd * t) + (Z0 + Zd * t) + D = 0

	t = -(AX0 + BY0 + CZ0 + D) / (AXd + BYd + CZd)

	= -(Pn� R0 + D) / (Pn � Rd)
	*/



	Vector3 plane_normal(plane.a, plane.b, plane.c);
	float coherence = plane_normal.dot(ray.direction);

	// if ray and plane are parallell or not facing the same direction
	if (coherence == 0) // only if parallell
	{
		return false;
	}

	float t = -(plane_normal.dot(ray.origin) + plane.d) / coherence;

	// now if t is >= 0 we have a hit
	if (t >= 0)
	{

		if(res)
		{
			res->normal = Vector3(plane.a, plane.b, plane.c);
			res->dist = t;
		}

		return true;
	}

	return false;
}


bool Intersect::intersect(const Ray & ray, const Sphere & sphere,  IntersectResult* res)
{
	Vector3 dist = sphere.origin - ray.origin;

    float B = ray.direction.dot(dist);
	float D = B * B - dist.dot(dist) + sphere.radius * sphere.radius; 

    if (D < 0.0f)
        return false;

    float t0 = B - sqrt(D); 
    float t1 = B + sqrt(D);

    if (t0 > 0.1f && t0 < t1) 
    {
		if(res)
		{
			res->dist = t0;
			res->normal = ray.origin + ray.direction * t0 - sphere.origin;
		}
        return true;
    } 
    if (t1 > 0.1f) 
    {
		if(res)
		{
			res->dist = t1;
			res->normal = ray.origin + ray.direction * t1 - sphere.origin;
		}
        return true;
    }

	return false;
}

bool Intersect::intersect(const Sphere & lhs, const Sphere & rhs, IntersectResult * res)
{
	Vector3 dist = lhs.origin - rhs.origin;
	if (dist.x == 0 && dist.y == 0 && dist.z == 0)
		dist = Vector3(0.01f, 0.0f, 0.0f);
	float length = dist.length();
	float radius = lhs.radius + rhs.radius;
	if (res)
	{
		dist.normalize();
		res->normal = dist;
		res->dist = radius - length;
		res->squared = false;
	}
	if (radius < length)
		return false;
	return true;
}

bool Intersect::intersect(const Ray & ray, const OBB & box, IntersectResult * res)
{
	bool changed = false;

	float min = -std::numeric_limits<float>::infinity();
	float max = std::numeric_limits<float>::infinity();

	for (int n = 0; n < 3; ++n)
	{
		IntersectResult r0;
		IntersectResult r1;

		if (intersect(ray, box.slab[n].max, &r0) && intersect(ray, box.slab[n].min, &r0))
		{
			if (r0.getDist() > r1.getDist())
			{
				min = std::max(r1.getDist(), min);
				max = std::min(r0.getDist(), max);
			}
			else     
			{
				min = std::max(r0.getDist(), min);
				max = std::min(r1.getDist(), max);
			}
			changed = true;
		}
	}
	if (min <= max && changed && min > 0)
	{
		res->squared = false;
		res->dist = min;
		res->normal = ray.origin - box.getPosition();
		res->normal.normalize();
		return true;
	}
	
	return false;
}

bool Intersect::intersect(const Sphere & sphere, const OBB & box, IntersectResult * res)
{
	float min = -std::numeric_limits<float>::infinity();
	int closest = -1;
	bool originInside = true;
	for (int n = 0; n < 6; ++n)
	{
		Plane p = box.plane[n];
		Vector3 norm = Vector3(p.a, p.b, p.c);

		float dist = norm.dot(sphere.origin) + p.d - sphere.radius;

		if (dist <= 0)
		{

			if (min <= dist)
				closest = n;
			min = std::max(min, dist);
			if (dist <= - sphere.radius)
				originInside = false;
		}
		else
			return false;
	}
	if (originInside)
	{
		res->squared = true;
		res->normal = sphere.origin - box.getPosition();
		res->dist = min;
		res->normal.normalize();
		return true;
	}

	res->squared = true;
	res->dist = min;
	res->normal = Vector3(box.plane[closest].a, box.plane[closest].b, box.plane[closest].c);
	res->normal.normalize();
	return true;
}

bool Intersect::intersect(const OBB & lhs, const OBB & rhs, IntersectResult * res) // The normal is kinda broken
{
	Quaternion rot = rhs.getRotation();
	rot = lhs.getRotation() * rot;
	OBB box(rhs.getDimensions(), rhs.getPosition(), rot);

	Vector3 min = lhs.getMin();
	Vector3 max = lhs.getMax();

	float minDist = -std::numeric_limits<float>::infinity();

	for (int n = 0; n < 6; ++n)
	{
		Vector3 p = min;
		Vector3 norm(box.plane[n].a, box.plane[n].b, box.plane[n].c);

		if (norm.x < 0)
		{
			p.x = max.x;
		}
		if (norm.y < 0)
		{
			p.y = max.y;
		}
		if (norm.z < 0)
		{
			p.z = max.z;
		}

		float dist = norm.dot(p) + box.plane[n].d;
		if (dist < 0)
		{
			minDist = std::max(minDist, dist);
		}
		else
			return false;
	}
	res->squared = false;
	res->dist = minDist;
	res->normal = rhs.getPosition() - lhs.getPosition();
	if (res->normal.x == 0 && res->normal.y == 0  && res->normal.z == 0 )
		res->normal.x = 0.001f; // Low number
	res->normal.normalize();

	return true;
}

/*
IntersectResult Intersect::Intersect(const Ray & ray, const Box & box,  IntersectResult* res) const
{
	// now 

	float min = -1, max = 99999;
	IntersectResult out = false;

	for (int i = 0; i < 6; i += 2)
	{
		const Plane & p1 = box.p[i + 0];
		const Plane & p2 = box.p[i + 1];

		// check front and back
		IntersectResult rt0 = intersect(ray, p1);
		IntersectResult rt1 = intersect(ray, p2);

		if (rt0 && rt1)
		{
			float t0 = rt0.getIntersect();
			float t1 = rt1.getIntersect();

			if (t0 < t1)
			{
				if (t0 > min) 
				{
					min = t0;
					out = rt0;
				}
				if (t1 < max) max = t1;
			}
			else
			{
				if (t1 > min) 
				{
					min = t1;
					out = rt1;
				}
				if (t0 < max) max = t0;
			}

			if (min > max)
				return false;
		}
		else if (p1.side(ray.origin) * p2.side(ray.origin) < 0)
		{
			return false;
		}
	}

	return out;
}
*/
/*

IntersectResult Intersect::intersect(const Ray & ray, const Triangle & triangle,  IntersectResult* res)
{
	Vector3 e1 = triangle.p2 - triangle.p1;
	Vector3 e2 = triangle.p3 - triangle.p1;

	
	// 1 - u + v = 0
	//ray.origin + ray.direction * t = triangle.p1 + s * u + t * v;


	// cross the line with the vertical going vert and we get a vector
	// pointing in the x direction that we can use to produce a u
	Vector3 ray_cross_e2 = ray.direction.cross(e2);

	// if this new vector produced is facing 90% away from the other
	// 'e1' leg of the triangle the direction of the ray is parallell to the plane
	// of the triangle

	float a = ray_cross_e2.dot(e1);

	if (std::abs(a) < 0.00001) // triangle plane is parallell to ray
		return false; // no hit

	// angle diff is used a way to determine how the triangle is facing the ray
	// a 100% facing returns 1 / 1
	float angle_diff = 1 / a;


	// get a vector from the 0 0 uv point on the triangle to the ray origin
	Vector3 to_ray_origin = ray.origin - triangle.p1;


	// 0 is equal to the ray 0 0 1 dot 1 0 0 = 0

	float u = angle_diff * to_ray_origin.dot(ray_cross_e2);

	// now we can check to see if the u is within the triangle
	if (u < 0 || u > 1)
		return false;

	// same as before but use ray and e1 instead
	Vector3 ray_cross_e1 = to_ray_origin.cross(e1);


	float v = angle_diff * ray.direction.dot(ray_cross_e1);

	if (v < 0 || u + v > 1)
		return false;

	// now we have u and v and possibly a ray hit

	// project to get t
	float t = to_ray_origin.dot(e2);

	if (t > 0.00001)
		return IntersectResult(t, e1.cross(e2));

	return false;
}*/