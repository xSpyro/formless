
#include "Layer.h"

#include "Renderer.h"
#include "Viewport.h"
#include "Renderable.h"
#include "Mesh.h"

#include "VertexShader.h"
#include "PixelShader.h"
#include "Model.h"
#include "Ray.h"
#include "IntersectResult.h"

#include "SkySphere.h"
#include "Sphere.h"

#include "ConstantBuffer.h"

Layer::Layer()
	: renderer(NULL)
{
	debugging = false;
	visible = true;
}

Layer::Layer(Renderer * renderer)
	: renderer(renderer)
{
	debugging = false;
	visible = true;
}

void Layer::init(Renderer * renderer)
{
	this->renderer = renderer;
}

void Layer::add(const Renderable * renderable)
{
	renderables.push_back(renderable);

	// register
	renderable->attach(this);
}

void Layer::add(const Model * model)
{
	if (model)
	{

		const std::vector < Renderable > & container = model->getRenderables();

		for (std::vector < Renderable > ::const_iterator i = container.begin(); i != container.end(); ++i)
		{
			add(&(*i));
		}

	}
}

void Layer::remove(const Renderable * renderable)
{
	for (RenderableContainer::const_iterator i = renderables.begin(); i != renderables.end(); ++i)
	{
		if (renderable == *i)
		{
			renderables.erase(i);
			return;
		}
	}
}

void Layer::clear()
{
	renderables.clear();
}

void Layer::remove(const Model * model)
{
	const std::vector < Renderable > & res = model->getRenderables();

	for (unsigned int i = 0; i < res.size(); ++i)
	{
		remove(&res[i]);
	}
}

Viewport * Layer::addViewport(Camera * camera, Texture * target, int flags)
{
	Viewport * viewport = new Viewport(renderer, camera, target, flags);

	viewports.push_back(viewport);

	return viewport;
}

const Renderable * Layer::pick(const Ray & ray, IntersectResult * result) const
{
	// do ray picking on all renderables in this layer

	// if result == NULL
	// make temp result

	IntersectResult temp;
	
	if (result == NULL)
	{
		result = &temp;
	}

	for (RenderableContainer::const_iterator i = renderables.begin(); i != renderables.end(); ++i)
	{
		const Renderable * renderable = *i;

		if (renderable->pick(ray, result))
		{
			return *i;
		}
	}

	return NULL;
}

float Layer::pickY(const Vector3 & position, Vector2 * uv) const
{
	Ray ray;
	ray.origin = Vector3(position.x, 1000, position.z);
	ray.direction = Vector3(0, -1, 0);

	IntersectResult res;

	if (pick(ray, &res))
	{
		if (uv)
		{
			uv->x = res.u;
			uv->y = res.v;
		}

		return (ray.origin + ray.direction * res.dist).y;
	}

	return position.y;
}

void Layer::debug()
{
	Effect * effect = viewports.front()->getEffect();
	
	SkySphere::Desc desc;
	desc.scale = 1.0f;
	desc.wireframe = true;

	if (SkySphere::generate(renderer, effect->getVS(), &desc, &mesh_sphere))
	{
		Logger::print()("LayerDebug: ", LOG_OK);

		debugging = true;
	}
}

void Layer::apply()
{
	if (!visible)
		return;

	// for each viewport
	for (ViewportContainer::iterator v = viewports.begin(); v != viewports.end(); ++v)
	{
		Viewport * viewport = *v;
		if( !viewport->isVisible())
			continue;

		Effect * effect = NULL;

		viewport->prepare();

		effect = viewport->getEffect();

		if (debugging)
		{
			for (RenderableContainer::const_iterator i = renderables.begin(); i != renderables.end(); ++i)
			{
				debug(*i, effect);
			}
		}
		else
		{
			if (viewport->hasSortOp())
			{
				Viewport::SortByCameraOut result;
				viewport->sortOp(renderables, result);

				for (Viewport::SortByCameraOut::const_iterator i = result.begin(); i != result.end(); ++i)
				{
					render(i->second, effect);
				}
			}
			else
			{
				for (RenderableContainer::const_iterator i = renderables.begin(); i != renderables.end(); ++i)
				{
					render(*i, effect);
				}
			}
		}
	}
}

void Layer::render(const Renderable * renderable, Effect * effect)
{
	if (!renderable->isVisible())
		return;
	
	const Mesh * mesh = renderable->getMesh();
	const Texture * texture = renderable->getTexture();
	const Model * model = renderable->getParent();

	// ugly
	Matrix offset;
	offset.identity();

	if (texture)
	{
		PixelShader * ps = effect->getPS();
		if (ps)
			ps->setTexture(0, const_cast < Texture* > (texture));
	}
	else
	{
		ID3D11ShaderResourceView * view = NULL;
		renderer->getContext()->PSGetShaderResources(0, 1, &view);
	}

	if (mesh)
	{

		if (model)
		{
			// bind index / vertex buffers and set input layouts
			Animation animation = model->getAnimation();
			mesh->bind(animation.src, animation.dst);

			updateShadersPerRenderable(renderable, animation.lerp, offset);
		}
		else
		{
			mesh->bind(0, 0);
			updateShadersPerRenderableWithoutModel(renderable);
		}

		mesh->draw();
	}
	else
	{
		if (model)
		{
			updateShadersPerRenderable(renderable, 0, offset);
		}


		renderer->getContext()->IASetInputLayout(NULL);

		unsigned int verts = renderable->getForcedVertices();

		// TEMP not very serious
		switch ( verts )
		{
		case SHAPE_POINT:

			renderer->getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
			break;

		case SHAPE_FILLED_RECT:

			renderer->getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
			break;

		case SHAPE_RECT:

			renderer->getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP);
			break;
		}

		// we use forced rendering
		renderer->getContext()->Draw(verts, 0);
	}
}

void Layer::debug(const Renderable * renderable, Effect * effect)
{
	// we can only debug renderables with models
	if (!renderable->isVisible())
		return;


	const Mesh * mesh = renderable->getMesh();
	const Model * model = renderable->getParent();

	if (model)
	{
		// bind index / vertex buffers and set input layouts
		Animation animation = model->getAnimation();
		mesh->bind(animation.src, animation.dst);

		Matrix offset;
		offset.identity();

		Sphere sphere = mesh->getBoundingSphere(animation.src, animation.dst, animation.lerp);

		offset.scale(sphere.radius);
		offset.translate(sphere.origin);

		updateShadersPerRenderable(renderable, animation.lerp, offset);

		mesh_sphere->bind();
		mesh_sphere->draw();
	}
}

void Layer::intermediate(const Model * model)
{
	if (model)
	{
		for (ViewportContainer::iterator v = viewports.begin(); v != viewports.end(); ++v)
		{
			Viewport * viewport = *v;
			Effect * effect = NULL;

			viewport->prepare();

			effect = viewport->getEffect();

			const std::vector < Renderable > & container = model->getRenderables();
			for (std::vector < Renderable > ::const_iterator i = container.begin(); i != container.end(); ++i)
			{
				const Renderable * r = &(*i);

				render(r, effect);
			}
		}
	}
}

void Layer::setVisible(bool state)
{
	visible = state;
}

void Layer::updateShadersPerRenderable(const Renderable * renderable, float lerp, const Matrix & offset)
{
	static PerRenderable buffer;
	static ConstantBuffer * cbuffer = new ConstantBuffer(renderer, sizeof(buffer));

	// set constant buffer per renderable
	memcpy(&buffer.model, &renderable->getParent()->getTransform(), sizeof(Matrix));

	// add offset
	buffer.model = offset * buffer.model;

	buffer.normal = buffer.model;
	buffer.normal.invert();
	

	// transpose
	buffer.model.transpose();
			
	// and add the lerp
	buffer.lerp = lerp;

	buffer.has_texture = (renderable->getTexture() != NULL);
	buffer.vertices = renderable->getForcedVertices();
	buffer.blend = renderable->getMaterial().blend;

	cbuffer->set(&buffer, sizeof(buffer));

	ID3D11Buffer * b = cbuffer->getBuffer();
	renderer->getContext()->VSSetConstantBuffers(1, 1, &b);
	renderer->getContext()->GSSetConstantBuffers(1, 1, &b);
}

void Layer::updateShadersPerRenderableWithoutModel(const Renderable * renderable)
{
	// MUST FIX SO NO MEMORY LEAKAGE
	static PerRenderable buffer;
	static ConstantBuffer * cbuffer = new ConstantBuffer(renderer, sizeof(buffer));

	// set constant buffer per renderable


	buffer.has_texture = (renderable->getTexture() != NULL);
	buffer.vertices = renderable->getForcedVertices();
	buffer.blend = renderable->getMaterial().blend;

	cbuffer->set(&buffer, sizeof(buffer));

	ID3D11Buffer * b = cbuffer->getBuffer();
	renderer->getContext()->VSSetConstantBuffers(1, 1, &b);
	renderer->getContext()->GSSetConstantBuffers(1, 1, &b);
}


void Layer::getRenderables(RenderableContainer& renderables)
{
	for(unsigned int i = 0; i < this->renderables.size(); i++)
	{
		renderables.push_back(this->renderables[i]);
	}
}

unsigned int Layer::getCount() const
{
	return renderables.size();
}

unsigned int Layer::getVisibleCount() const
{
	unsigned int count = 0;
	for (RenderableContainer::const_iterator i = renderables.begin(); i != renderables.end(); ++i)
	{
		if ((*i)->isVisible())
			++count;
	}

	return count;
}

const bool Layer::isVisible() const
{
	return visible;
}

unsigned int Layer::performance() const
{
	return renderables.size();
}