
#ifndef QUATERNION
#define QUATERNION

#include "Common.h"

struct Vector3;
struct Matrix;

struct Quaternion
{
	float x, y, z, w;

	Quaternion();
	Quaternion(float v);
	Quaternion(float x, float y, float z, float w);

	Quaternion & operator += (const Quaternion & rhs);
	Quaternion & operator -= (const Quaternion & rhs);
	Quaternion & operator *= (const Quaternion & rhs);
	Quaternion & operator *= (float scalar);
	Quaternion & operator /= (float divider);

	Quaternion operator + () const;
	Quaternion operator - () const;

	Quaternion operator + (const Quaternion & rhs) const;
	Quaternion operator - (const Quaternion & rhs) const;
	Quaternion operator * (const Quaternion & rhs) const;
	Quaternion operator * (float scalar) const;
	Quaternion operator / (float divider) const;

	Quaternion operator = (const Quaternion & rhs);

	float		dot(const Quaternion & rhs) const;
	void		identity();
	Quaternion	inverseOf() const;
	void		invert();
	float		length() const;
	float		lengthNonSqrt() const;
	void		multiply(const Quaternion & rhs);
	void		normalize();
	
	void		rotationAxis(Vector3 axis, float angle);
	void		rotationMatrix(Matrix matrix);
	void		rotationYawPitchRoll(float yaw, float pitch, float roll);

	void		rotationAxisApply(Vector3 axis, float angle);
	void		rotationMatrixApply(Matrix matrix);
	void		rotationYawPitchRollApply(float yaw, float pitch, float roll);

	void		slerp(const Quaternion & rhs, float t);

	void		getAxisAngle(Vector3 & axis, float & angle) const;

	void		setAxisAngle(Vector3 axis, float angle);

	friend std::ostream & operator << (std::ostream & stream, const Quaternion & me);
};

#endif