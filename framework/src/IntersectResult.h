
#ifndef INTERSECT_RESULT
#define INTERSECT_RESULT
#include "Math.h"
#include "Vector3.h"

struct IntersectResult
{
	float dist;
	float u, v;

	bool squared;

	Vector3 normal;

	float getDist()
	{
		if (squared)
			return dist;
		if (dist < 0)
			dist = -sqrt(-dist);
		else
			dist = sqrt(dist);
		squared = true;
		return dist;
	}
	float getDirection()
	{
		return dist;
	}
};

#endif