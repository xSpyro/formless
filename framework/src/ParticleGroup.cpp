
#include "ParticleGroup.h"

#include "ParticleSystem.h"
#include "ParticleEmitter.h"

void ParticleGroup::addEmitter(const std::string type_n, const std::string & name, int life, float spawn_rate, float affect_rate)
{
	Emitter e;

	for(int i = 0; i < (int)emitters.size(); i++)
	{
		if( emitters[i].name == name )
		{
			emitters[i].life = life;
			emitters[i].affect_rate = affect_rate;
			emitters[i].spawn_rate = spawn_rate;
			emitters[i].type_name = type_n;
			return;
		}
		else
		{

		}
	}
	e.affect_rate = affect_rate;
	e.name = name;
	e.life = life;
	e.spawn_rate = spawn_rate;
	e.type_name = type_n;

	emitters.push_back(e);
}

void ParticleGroup::emit(ParticleSystem * ps, const Vector3 & position, const Vector3 & normal)
{
	for (Emitters::iterator i = emitters.begin(); i != emitters.end(); ++i)
	{
		ParticleEmitter * e = ps->spawnEmitter(i->type_name, i->name);
		
		if (e)
		{
			e->move(position);
			e->setLifeAffectRate(i->affect_rate);
			e->setLife(i->life);
			e->setSpawnRate(i->spawn_rate);
			e->setNormal(normal);

		}
	}
}