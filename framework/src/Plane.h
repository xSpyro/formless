
#ifndef PLANE
#define PLANE
#include "Vector3.h"

struct Plane
{
	float a, b, c, d;

	void setPlane(Vector3 p0, Vector3 p1, Vector3 p2);
	void awayFrom(Vector3 pos, Vector3 p2);
};

#endif