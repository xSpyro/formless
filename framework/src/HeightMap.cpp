
#include "HeightMap.h"

#include "Renderer.h"
#include "Mesh.h"

#include "MeshStream.h"

#include "Surface.h"

bool HeightMap::generate(Renderer * renderer, BaseShader * shader, Desc * desc, Mesh ** out)
{
	*out = renderer->mesh()->create();

	int width = 256;
	int depth = 256;
	Surface * surface = NULL;
	float height = 1.0f;
	float size = 1.0f;
	float texscale = 1.0f;
	float yoffset = 0;
	bool ccw = true;

	int quality = 2;

	if (desc)
	{
		if (desc->surface)
		{
			surface = desc->surface;
			width = surface->getWidth() / quality;
			depth = surface->getHeight() / quality;
		}

		size = desc->scale;
		height = desc->height;
		texscale = desc->texscale;
		yoffset = desc->offset;
		ccw = desc->ccw;
	}

	// create the needed streams
	Mesh * mesh = *out;

	MeshStream * stream = mesh->createEmptyStream("TerrainStream");

	stream->addDesc("POSITION", sizeof(Vector3));
	stream->addDesc("NORMAL", sizeof(Vector3));
	stream->addDesc("TEXCOORD", sizeof(Vector2));

	stream->fill(width * depth);

	float xscale = 1 / static_cast < float > (width);
	float zscale = 1 / static_cast < float > (depth);

	
	Vector3 scale = Vector3(xscale, height, zscale) * size;
	Vector3 offset = Vector3((width - 1) / -2.0f, yoffset, (depth - 1) / -2.0f);


	Vertex * vertex = reinterpret_cast < Vertex* > (stream->get(0));


	// fill the vertices with correct data

	for (int z = 0; z < depth; ++z)
	{
		for (int x = 0; x < width; ++x)
		{
			Vertex & v = vertex[z * width + x];

			float h = 0;
			
			if (surface)
			{
				h = surface->at(x * quality, z * quality).r;
			}

			v.position = (Vector3((float)x, h, (float)z) + offset) * scale;
			v.normal = Vector3(0, 1, 0);
			v.texcoord = Vector2(x * xscale, z * zscale) * texscale;
		}
	}

	// fill in normals
	for (int z = 0; z < depth - 1; ++z)
	{
		for (int x = 0; x < width - 1; ++x)
		{
			Vertex & v = vertex[(x + 0) + (z + 0) * width];

			Vertex & a = vertex[(x + 1) + (z + 0) * width];
			Vertex & b = vertex[(x + 0) + (z + 1) * width];

			v.normal = (b.position - v.position).cross(a.position - v.position);

			v.normal.normalize();
		}
	}

	// smooth the normals

	Vector3 * temp = new Vector3[width * depth];
	for (int z = 1; z < depth - 1; ++z)
	{
		for (int x = 1; x < width - 1; ++x)
		{
			Vertex & v = vertex[(x + 0) + (z + 0) * width];

			Vertex & a = vertex[(x + 1) + (z + 0) * width];
			Vertex & b = vertex[(x + 0) + (z + 1) * width];
			Vertex & c = vertex[(x - 1) + (z + 0) * width];
			Vertex & d = vertex[(x + 0) + (z - 1) * width];

			temp[x + z * width] = (a.normal + b.normal + c.normal + d.normal + v.normal) / 5;
		}
	}
	
	// assign them again
	for (int z = 0; z < depth - 1; ++z)
	{
		for (int x = 0; x < width - 1; ++x)
		{
			Vertex & v = vertex[(x + 0) + (z + 0) * width];
			v.normal = temp[x + z * width];
		}
	}
	delete[] temp;

	

	// create the indices

	if (ccw)
	{
		for (int z = 0; z < depth - 1; ++z)
		{
			for (int x = 0; x < width - 1; ++x)
			{
				mesh->addIndex((x + 0) + (z + 0) * width);
				mesh->addIndex((x + 1) + (z + 0) * width);
				mesh->addIndex((x + 0) + (z + 1) * width);

				mesh->addIndex((x + 0) + (z + 1) * width);
				mesh->addIndex((x + 1) + (z + 0) * width);
				mesh->addIndex((x + 1) + (z + 1) * width);
			}
		}
	}
	else
	{
		for (int z = 0; z < depth - 1; ++z)
		{
			for (int x = 0; x < width - 1; ++x)
			{
				mesh->addIndex((x + 0) + (z + 0) * width);
				mesh->addIndex((x + 1) + (z + 0) * width);
				mesh->addIndex((x + 1) + (z + 1) * width);

				mesh->addIndex((x + 0) + (z + 1) * width);
				mesh->addIndex((x + 0) + (z + 0) * width);
				mesh->addIndex((x + 1) + (z + 1) * width);
			}
		}
	}

	// submit
	mesh->submit(shader);

	return true;
}

bool HeightMap::update(Renderer * renderer, BaseShader * shader, Desc * desc, Mesh * out)
{
	int width = 256;
	int depth = 256;
	Surface * surface = NULL;
	float height = 1.0f;
	float size = 1.0f;
	float texscale = 1.0f;
	float yoffset = 0;
	bool ccw = true;

	int quality = 2;

	if (desc)
	{
		if (desc->surface)
		{
			surface = desc->surface;
			width = surface->getWidth() / quality;
			depth = surface->getHeight() / quality;
		}

		size = desc->scale;
		height = desc->height;
		texscale = desc->texscale;
		yoffset = desc->offset;
		ccw = desc->ccw;
	}

	// create the needed streams
	Mesh * mesh = out;
	//mesh->clear();

	MeshStream * stream = mesh->getStream("TerrainStream");

	//stream->addDesc("POSITION", sizeof(Vector3));
	//stream->addDesc("NORMAL", sizeof(Vector3));
	//stream->addDesc("TEXCOORD", sizeof(Vector2));

	//stream->fill(width * depth);

	float xscale = 1 / static_cast < float > (width);
	float zscale = 1 / static_cast < float > (depth);

	
	Vector3 scale = Vector3(xscale, height, zscale) * size;
	Vector3 offset = Vector3((width - 1) / -2.0f, yoffset, (depth - 1) / -2.0f);


	Vertex * vertex = reinterpret_cast < Vertex* > (stream->get(0));


	// fill the vertices with correct data

	for (int z = 0; z < depth; ++z)
	{
		for (int x = 0; x < width; ++x)
		{
			Vertex & v = vertex[z * width + x];

			float h = 0;
			
			if (surface)
			{
				h = surface->at(x * quality, z * quality).r;
			}

			v.position = (Vector3((float)x, h, (float)z) + offset) * scale;
			v.normal = Vector3(0, 1, 0);
			v.texcoord = Vector2(x * xscale, z * zscale) * texscale;
		}
	}

	// fill in normals
	for (int z = 0; z < depth - 1; ++z)
	{
		for (int x = 0; x < width - 1; ++x)
		{
			Vertex & v = vertex[(x + 0) + (z + 0) * width];

			Vertex & a = vertex[(x + 1) + (z + 0) * width];
			Vertex & b = vertex[(x + 0) + (z + 1) * width];

			v.normal = (b.position - v.position).cross(a.position - v.position);

			v.normal.normalize();
		}
	}

	// smooth the normals

	Vector3 * temp = new Vector3[width * depth];
	for (int z = 1; z < depth - 1; ++z)
	{
		for (int x = 1; x < width - 1; ++x)
		{
			Vertex & v = vertex[(x + 0) + (z + 0) * width];

			Vertex & a = vertex[(x + 1) + (z + 0) * width];
			Vertex & b = vertex[(x + 0) + (z + 1) * width];
			Vertex & c = vertex[(x - 1) + (z + 0) * width];
			Vertex & d = vertex[(x + 0) + (z - 1) * width];

			temp[x + z * width] = (a.normal + b.normal + c.normal + d.normal + v.normal) / 5;
		}
	}
	
	// assign them again
	for (int z = 0; z < depth - 1; ++z)
	{
		for (int x = 0; x < width - 1; ++x)
		{
			Vertex & v = vertex[(x + 0) + (z + 0) * width];
			v.normal = temp[x + z * width];
		}
	}
	delete[] temp;

	// submit
	mesh->submit(shader);

	return true;
}