
#include "Renderer.h"

#include "Window.h"
#include "Color.h"
#include "Layer.h"
#include "Stage.h"

#include "Viewport.h"

#include "Texture.h"
#include "StateChange.h"
#include "RenderOverlay.h"

Renderer::Renderer()
	: log("Renderer")
{
	userdata = NULL;
}

void Renderer::setUserdata(void * ud)
{
	userdata = ud;
}

void * Renderer::getUserdata()
{
	return userdata;
}

int Renderer::getWidth() const
{
	return width;
}

int Renderer::getHeight() const
{
	return height;
}


bool Renderer::join(Window * window, int flags)
{
	return join(window->getHandle(), window->getWidth(), window->getHeight(), flags);
}

bool Renderer::join(HWND hwnd, int width, int height, int flags)
{
	HRESULT hr;

	this->width = width;
	this->height = height;
	this->flags = flags;

	// check flags
	if (flags & RENDERER_ENABLE_WPF)
	{
		createDevice9Ex(hwnd);
	}

	if (!createDeviceAndSwapChain(hwnd))
	{
		return false;
	}

	// get dxgi buffer
	//hr = swap_chain->GetBuffer(0, __uuidof(IDXGISurface), (void **)&dxgi_surface);

	//D2D1_FACTORY_OPTIONS options;
	//options.debugLevel = D2D1_DEBUG_LEVEL_INFORMATION;

	/*hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &d2_factory);

	if (FAILED(hr))
	{
		log.alert("D2D1CreateFactory: ", DXGetErrorDescription(hr));
	}
	else
	{
		log.notice("D2D1CreateFactory: ", LOG_OK);

		hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED,
            __uuidof(d2_write), reinterpret_cast<IUnknown **>(&d2_write));

		if (FAILED(hr))
		{
			log.alert("DWriteCreateFactory: ", DXGetErrorDescription(hr));
		}
		else
		{
			log.notice("DWriteCreateFactory: ", LOG_OK);
		}
	}*/

    // create a render target view
	// this creates a link so we can render to the screen

	// if we want to render to wpf we dont render to screen
	// and dont grab our render view target from the backbuffer
	// but point it to our shared surface instead

	ID3D11Texture2D * pBuffer;

	if (flags & RENDERER_ENABLE_WPF)
	{
		// create shared link
		/*
		ID3D11Resource* ptempResource = NULL;
		hr = device->OpenSharedResource(shared_handle,__uuidof(ID3D11Resource), (void**)&ptempResource);	
		if (FAILED(hr))
		{
			log.alert("OpenSharedResource (WPF): ", DXGetErrorDescription(hr));
		}

		hr = ptempResource->QueryInterface(__uuidof(ID3D11Texture2D), (void**)&pBuffer);
		if (FAILED(hr))
		{
			log.alert("QueryInterface (WPF): ", DXGetErrorDescription(hr));
		}
		ptempResource->Release();
		*/

		D3D11_TEXTURE2D_DESC Desc;
		Desc.Width              = width;
		Desc.Height             = height;
		Desc.MipLevels          = 1;
		Desc.ArraySize          = 1;
		Desc.Format             = DXGI_FORMAT_B8G8R8A8_UNORM;
		Desc.SampleDesc.Count   = 1;
		Desc.SampleDesc.Quality = 0;
		Desc.Usage              = D3D11_USAGE_DEFAULT;
		Desc.BindFlags          = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		Desc.CPUAccessFlags     = 0;
		Desc.MiscFlags          = D3D11_RESOURCE_MISC_SHARED;

		HRESULT hr = device->CreateTexture2D(&Desc, NULL, &pBuffer);
		if (FAILED(hr))
		{
			log.alert("CreateTexture WPF: ", LOG_FAILED);
			log.alert("Description: ", DXGetErrorDescription(hr));
		}

		shared_surface = pBuffer;
	}
	else
	{
		hr = swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void **)&pBuffer);

		if (FAILED(hr))
		{
			log.alert("CreateBackBuffer: ", DXGetErrorDescription(hr));
		}
		else
		{
			log.notice("CreateBackBuffer: ", LOG_OK);
		}	
	}

	hr = device->CreateRenderTargetView(pBuffer, NULL, &rtv_screen);
	//pBuffer->Release();

	if (FAILED(hr))
	{
		log.alert("CreateBackBuffer RenderTarget: ", DXGetErrorDescription(hr));
	}
	else
	{
		log.notice("CreateBackBuffer RenderTarget: ", LOG_OK);
	}

	// setup stores
	texture_store.init(this);
	shader_store.init(this);
	mesh_store.init(this);
	font_store.init(this);

	model_import.init(this);

	particle_system.init(this);

	// default values

	//create viewport structure
	setViewport(0, 0, width, height);

	context->IASetInputLayout(NULL);
	context->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);



	// create depth texture
	depth_texture = texture_store.create(width, height, TEXTURE_DEPTH);

	// bind
	screen.bind(rtv_screen, depth_texture);
	framebuffer = &screen;

	
	// create offscreen texture

	// WAIT WITH TEXT UNTIL FONT OBJECTS WORK
	/*
	offscreen_texture = texture_store.create(512, 512, TEXTURE_BITMAP);

	offscreen_texture->getResource()->QueryInterface(&dxgi_surface);

	D2D1_RENDER_TARGET_PROPERTIES props =
        D2D1::RenderTargetProperties(
            D2D1_RENDER_TARGET_TYPE_DEFAULT,
            D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED),
            96,
            96
            );

	hr = d2_factory->CreateDxgiSurfaceRenderTarget(dxgi_surface, &props, &d2_rt);

	if (FAILED(hr))
	{
		log.alert("CreateDxgiSurfaceRenderTarget: ", LOG_FAILED);
		log.alert("Description: ", DXGetErrorDescription(hr));
	}
	*/
	


	context->OMSetRenderTargets(1, &rtv_screen, depth_texture->getDepthStencilView());

	context->VSSetShader(NULL, NULL, 0);
	context->PSSetShader(NULL, NULL, 0);
	context->GSSetShader(NULL, NULL, 0);


	log.notice("Join: ", LOG_OK);


	return true;
}

bool Renderer::switchWindowFullScreenMode()
{
	int fullscreen;
	IDXGIOutput * output;
	swap_chain->GetFullscreenState(&fullscreen, &output);
	
	HRESULT hr = swap_chain->SetFullscreenState(!fullscreen, NULL);
	return SUCCEEDED(hr);
}

bool Renderer::setFullscreen(bool state)
{
	int fullscreen;
	IDXGIOutput * output;
	swap_chain->GetFullscreenState(&fullscreen, &output);

	DXGI_OUTPUT_DESC desc;
	if (output)
		output->GetDesc(&desc);
	
	//DXGI_FRAME_STATISTICS stats;
	//output->GetFrameStatistics(&stats);

	//desc.DesktopCoordinates

	HRESULT hr = swap_chain->SetFullscreenState(state, NULL);

	std::cout << "Fullscreen: " << state << " " << hr << std::endl;
			return SUCCEEDED(hr);
}

const bool Renderer::getFullscreen() const
{
	int fullscreen;
	IDXGIOutput * output;
	swap_chain->GetFullscreenState(&fullscreen, &output);

	return fullscreen == 1;
}

void Renderer::render()
{
	// render layers
	for (StageMap::iterator i = stages.begin(); i != stages.end(); ++i)
	{
		i->second->apply();
	}
}

void Renderer::flush()
{
	context->Flush();
}

void Renderer::present(bool vertical_sync)
{
	// if we are wpf we dont present but just flush

	if (flags & RENDERER_ENABLE_WPF)
		context->Flush();
	else
		swap_chain->Present(vertical_sync, 0);
}

void Renderer::clear(Texture * target)
{
	if (target)
	{
		target->clear();
	}
	else
	{
		context->ClearRenderTargetView(rtv_screen, &clear_color.r);

		// to be made member function 'clear'
		context->ClearDepthStencilView(depth_texture->getDepthStencilView(), D3D10_CLEAR_DEPTH, 1.0f, 0);
	}
}

void Renderer::setClearColor(const Color & color)
{
	clear_color = color;
}

void Renderer::setRenderTarget(const Texture * target, int flags)
{
	beginRenderSequence();

	// can be a multitexture
	if (target == NULL)
	{
		ID3D11DepthStencilView * temp_dsv;
		ID3D11RenderTargetView * temp_rtv;

		// use screen
		framebuffer->renderTo(&temp_rtv, &temp_dsv);

		addRenderTarget(temp_rtv);
		addViewport(0, 0, framebuffer->getWidth(), framebuffer->getHeight());
		endRenderSequence(temp_dsv);
	

		// this simplifies the ability to
		// change the framebuffer to another texture
		//target = framebuffer;
	}
	else if (target->isMultiTexture())
	{
		// now we must set multiple viewports and multiple rendertargets

		ID3D11DepthStencilView * dsv = NULL;

		if (flags == VIEWPORT_SCREEN_AND_TEXTURE)
		{
			ID3D11DepthStencilView * temp_dsv;
			ID3D11RenderTargetView * temp_rtv;

			framebuffer->renderTo(&temp_rtv, &temp_dsv);

			/*
			// add our screen first and use its depth buffer
			addRenderTarget(rtv_screen);
			addViewport(0, 0, width, height);

			//dsv = depth_texture->getDepthStencilView();
			dsv = framebuffer->getDepthStencilView();
			*/

			addRenderTarget(temp_rtv);
			addViewport(0, 0, framebuffer->getWidth(), framebuffer->getHeight());

			dsv = temp_dsv;
		}
		
		unsigned int n = target->getTextureCount();
		for (unsigned int i = 0; i < n; ++i)
		{
			Texture * texture = target->getMultiTexture(i);

			ID3D11DepthStencilView * temp_dsv;
			ID3D11RenderTargetView * temp_rtv;
			texture->renderTo(&temp_rtv, &temp_dsv);

			// now use the first depth map we get
			if (temp_dsv != NULL && dsv == NULL)
				dsv = temp_dsv;
			
			addRenderTarget(temp_rtv);
			addViewport(0, 0, texture->getWidth(), texture->getHeight());
		}

		endRenderSequence(dsv);
	}
	else
	{
		ID3D11DepthStencilView * dsv;
		ID3D11RenderTargetView * temp_rtv;

		target->renderTo(&temp_rtv, &dsv);

		if (flags == VIEWPORT_SCREEN_AND_TEXTURE)
		{
			ID3D11DepthStencilView * temp_dsv;
			ID3D11RenderTargetView * temp_rtv;

			framebuffer->renderTo(&temp_rtv, &temp_dsv);

			// add our screen first and use its depth buffer
			addRenderTarget(temp_rtv);
			addViewport(0, 0, framebuffer->getWidth(), framebuffer->getHeight());

			if (temp_dsv != NULL)
				dsv = temp_dsv;
		}

		addRenderTarget(temp_rtv);
		addViewport(0, 0, target->getWidth(), target->getHeight());

		endRenderSequence(dsv);
	}
}

void Renderer::redirectFrameBuffer(const Texture * target)
{
	if (target == NULL)
	{
		framebuffer = &screen;
	}
	else
	{
		framebuffer = target;
	}
}

Layer * Renderer::createLayer()
{
	Layer * layer = new Layer(this);

	return layer;
}

void Renderer::insertStage(unsigned int priority, Stage * stage)
{
	stages.insert(std::pair < unsigned int, Stage* > (priority, stage));
}

void Renderer::insertLayer(unsigned int priority, Layer * layer)
{
	//if (stages.find(priority) != stages.end())
	//{
		stages.insert(std::pair < unsigned int, Stage* > (priority, layer));
	//}
}

void Renderer::insertStateChange(unsigned int priority, Texture * texture)
{
	StateChange * state = new StateChange(this);

	state->texture = texture;

	stages.insert(std::pair < unsigned int, Stage* > (priority, state));
}

void Renderer::insertOverlay(unsigned int priority, Texture * texture, BaseShader * ps)
{
	stages.insert(std::pair < unsigned int, RenderOverlay* > (priority, new RenderOverlay(this, texture, ps)));
}

void Renderer::clearStage(unsigned int priority)
{
	std::pair < StageMap::iterator, StageMap::iterator > ret = stages.equal_range(priority);
	for (StageMap::iterator i = ret.first; i != ret.second; ++i)
	{
		i->second->free();
	}

	stages.erase(priority);
}

void Renderer::clearLayers()
{
	for (StageMap::iterator i = stages.begin(); i != stages.end(); ++i)
	{
		i->second->free();
	}

	stages.clear();
}

unsigned int Renderer::getStageCount()
{
	return stages.size();
}

unsigned int Renderer::queryPerformance() const
{
	unsigned int performance = 0;
	for (StageMap::const_iterator i = stages.begin(); i != stages.end(); ++i)
	{
		performance += i->second->performance();
	}

	return performance;
}

ID3D11Device * Renderer::getDevice()
{
	return device;
}

ID3D11DeviceContext * Renderer::getContext()
{
	return context;
}

Texture * Renderer::getDepthTexture() const
{
	return depth_texture;
}

Texture * Renderer::getFrameBufferTexture()
{
	return &screen;
}

void * Renderer::getSharedWPFSurface() const
{
	return shared_surface;
}

const D3D_FEATURE_LEVEL Renderer::getFeatureLevel() const
{
	return featureLevel;
}

TextureStore * Renderer::texture()
{
	return &texture_store;
}

ShaderStore * Renderer::shader()
{
	return &shader_store;
}

MeshStore * Renderer::mesh()
{
	return &mesh_store;
}

FontStore * Renderer::font()
{
	return &font_store;
}

ModelImport * Renderer::modelImport()
{
	return &model_import;
}

ParticleSystem * Renderer::ps()
{
	return &particle_system;
}

void Renderer::onResizeWindow(const unsigned int width, const unsigned int height)
{

	this->width = width;
	this->height = height;

	//rtv_screen->Release();

	//context->OMSetRenderTargets(0, NULL, NULL);

	//rtv_screen = NULL;

	//depth_texture->free();

	//depth_texture->getDepthStencilView()->Release();

	//for(unsigned int i = 0; i < rendertargets.size(); i++)
	//{
	//	rendertargets[i]->Release();
	//}

	//swap_chain->ResizeBuffers(2, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH);



	setViewport(0,0,width,height);
}

void Renderer::getResolutions(std::vector<DXGI_MODE_DESC>& resolutions)
{
	IDXGIOutput* out = NULL;
	swap_chain->GetContainingOutput(&out);

	unsigned int nrModes = 0;
	out->GetDisplayModeList( DXGI_FORMAT_R8G8B8A8_UNORM, 0, &nrModes, 0);


	resolutions.resize(nrModes);

	out->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, 0, &nrModes, &resolutions.front());

}

void Renderer::setViewport(int x, int y, int width, int height)
{
	D3D11_VIEWPORT viewPort;
	viewPort.Width = static_cast < float > (width);
	viewPort.Height = static_cast < float > (height);
	viewPort.MinDepth = 0.0f;
	viewPort.MaxDepth = 1.0f;
	viewPort.TopLeftX = 0;
	viewPort.TopLeftY = 0;

	//set the viewport
	context->RSSetViewports(1, &viewPort);
}

void Renderer::beginRenderSequence()
{
	viewports.clear();
	rendertargets.clear();
}

void Renderer::endRenderSequence(ID3D11DepthStencilView * view)
{
	// set the viewports
	context->RSSetViewports(viewports.size(), &viewports.front());

	// set the rendertargets
	context->OMSetRenderTargets(rendertargets.size(), 
		&rendertargets.front(), view);
}

void Renderer::addViewport(int x, int y, int width, int height)
{
	D3D11_VIEWPORT viewport;
	viewport.Width = static_cast < float > (width);
	viewport.Height = static_cast < float > (height);
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;

	viewports.push_back(viewport);
}

void Renderer::addRenderTarget(ID3D11RenderTargetView * target)
{
	rendertargets.push_back(target);
}

bool Renderer::createDeviceAndSwapChain(HWND hwnd)
{
	HRESULT hr;


	DXGI_SWAP_CHAIN_DESC desc;
	ZeroMemory(&desc, sizeof(desc));
    desc.BufferCount = 2;
	desc.BufferDesc.Width = width;
	desc.BufferDesc.Height = height;
    desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    desc.BufferDesc.RefreshRate.Numerator = 60;
    desc.BufferDesc.RefreshRate.Denominator = 1;
	desc.BufferDesc.Scaling = DXGI_MODE_SCALING_STRETCHED;
	desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;

	
    desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

	if (flags & RENDERER_ENABLE_WPF)
	{
		//desc.BufferUsage |= DXGI_USAGE_SHARED;
	}

    desc.OutputWindow = hwnd;

    desc.SampleDesc.Count = 1;
    desc.SampleDesc.Quality = 0;

    desc.Windowed = true;
	desc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

	int flags = D3D11_CREATE_DEVICE_SINGLETHREADED;

	flags |= D3D11_CREATE_DEVICE_BGRA_SUPPORT;


#ifndef NDEBUG
	flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	D3D_FEATURE_LEVEL feature;
	D3D_FEATURE_LEVEL forced[1] = { D3D_FEATURE_LEVEL_11_0 };

	log.notice("CreatingDevice LatestVersion...");
	
	// directx 11
	hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags, forced, 1,
		D3D11_SDK_VERSION, &desc, &swap_chain, &device, &feature, &context);

	if (FAILED(hr))
	{
		log.alert("DeviceCreation: ", LOG_FAILED);
		log.alert("Description: ", DXGetErrorDescription(hr));
		log.notice("Trying DirectX 10_1...");

		D3D_FEATURE_LEVEL forced[1] = { D3D_FEATURE_LEVEL_10_1 };

		hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags, forced, 1,
			D3D11_SDK_VERSION, &desc, &swap_chain, &device, &feature, &context);

		if (FAILED(hr))
		{
			log.alert("DeviceCreation: ", LOG_FAILED);
			log.alert("Description: ", DXGetErrorDescription(hr));
			log.notice("Trying DirectX 10_0...");

			D3D_FEATURE_LEVEL forced[1] = { D3D_FEATURE_LEVEL_10_0 };

			hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags, forced, 1,
				D3D11_SDK_VERSION, &desc, &swap_chain, &device, &feature, &context);

			if (FAILED(hr))
			{
				log.alert("DeviceCreation: ", LOG_FAILED);
				log.alert("Description: ", DXGetErrorDescription(hr));
				log.notice("Trying DirectX 9_3...");

				D3D_FEATURE_LEVEL forced[1] = { D3D_FEATURE_LEVEL_9_3 };

				hr = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, flags, forced, 1,
					D3D11_SDK_VERSION, &desc, &swap_chain, &device, &feature, &context);

				if (FAILED(hr))
				{
					log.alert("DeviceCreation: ", LOG_FAILED);
					log.alert("Description: ", DXGetErrorDescription(hr));
					log.fatal("You need atleast DirectX 9_3 to run this");
					log.fatal("Shuting down..");
					return false;
				}
			}
		}
	}

	featureLevel = feature;
	
	switch ( feature )
	{
	case D3D_FEATURE_LEVEL_9_3:

		log.notice("Running DirectX 9.3");
		break;

	case D3D_FEATURE_LEVEL_10_0:

		log.notice("Running DirectX 10.0");
		break;

	case D3D_FEATURE_LEVEL_10_1:

		log.notice("Running DirectX 10.1");
		break;

	case D3D_FEATURE_LEVEL_11_0:

		log.notice("Running DirectX 11.0");
		break;

	default:

		log.alert("Unable to determine DirectX level");
		log.fatal("Shuting down..");
		return false;
	}

	return true;
}

bool Renderer::createDevice9Ex(HWND hwnd)
{
	/*
	HRESULT hr;
	IDirect3D9Ex * d3d = NULL;
    
	hr = Direct3DCreate9Ex(D3D_SDK_VERSION, &d3d);

	if (FAILED(hr))
	{
		log.alert("Direct3DCreate9Ex (WPF): ", DXGetErrorDescription(hr));
		return false;
	}

	D3DPRESENT_PARAMETERS d3dpp; 
	ZeroMemory( &d3dpp, sizeof(d3dpp) );
	d3dpp.Windowed = TRUE;
    d3dpp.BackBufferHeight = 1;
    d3dpp.BackBufferWidth = 1;
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
    d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8;

	hr = d3d->CreateDeviceEx(D3DADAPTER_DEFAULT, 
		D3DDEVTYPE_HAL, hwnd, 
		D3DCREATE_SOFTWARE_VERTEXPROCESSING, 
		&d3dpp, NULL, &device9ex);

	if (FAILED(hr))
	{
		log.alert("CreateDeviceEx (WPF): ", DXGetErrorDescription(hr));
		return false;
	}


	// the shared surface
	shared_handle = NULL;
	LPDIRECT3DTEXTURE9 texture9 = NULL;
	hr = device9ex->CreateTexture(width, height, 1, 
		D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &texture9, 
		&shared_handle);

	if (FAILED(hr))
	{
		log.alert("CreateTexture (WPF): ", DXGetErrorDescription(hr));
		return false;
	}
	
	hr = texture9->GetSurfaceLevel(0, &shared_surface);

	if (FAILED(hr))
	{
		log.alert("Obtaining SharedSurface (WPF): ", DXGetErrorDescription(hr));
		return false;
	}
	*/

	return true;
}