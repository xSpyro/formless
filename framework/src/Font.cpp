
#include "Font.h"

#include "Mesh.h"
#include "Renderer.h"

#include "Model.h"
#include "Renderable.h"

#include "Layer.h"

Font::Font()
{
	renderable.getMaterial().blend = Color(1, 1, 1, 1);
}

Mesh * Font::create(Renderer * renderer, BaseShader * vs)
{
	Mesh * out = renderer->mesh()->create();

	MeshStream * stream = out->createSingleStream("Glyph", sizeof(Vertex));

	stream->fill(512);

	out->submit(vs);

	out->setTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	mesh = out;

	renderable.setMesh(mesh);
	renderable.setTexture(texture);

	return out;
}

void Font::draw(Mesh * mesh, const std::string & text)
{
	MeshStream * stream = mesh->getStream("Glyph");

	stream->fill(512);
	Vector2 offset(0, 0);

	Vertex * buffer = reinterpret_cast < Vertex* > (stream->get(0));

	int n = 0;
	for (std::string::const_iterator i = text.begin(); i != text.end(); ++i)
	{

		// print the first char
		int c = *i;

		const FontGlyphMetric & gm = glyphs.at(c - 32);

		if (c == ' ')
		{
			offset += gm.advance;
			continue;
		}

		Rect rect = gm.coord;

	
		Vertex v;

		v.position = offset + gm.offset;
		v.position.floor();
		v.uv = Vector2(rect.x, rect.y);
		buffer[0 + n] = v;

		v.position = offset + gm.offset + Vector2(gm.dim.x, 0);
		v.position.floor();
		v.uv = Vector2(rect.x + rect.width, rect.y);
		buffer[1 + n] = v;

		v.position = offset + gm.offset + Vector2(0, gm.dim.y);
		v.position.floor();
		v.uv = Vector2(rect.x, rect.y + rect.height);
		buffer[2 + n] = v;
		buffer[3 + n] = v;

		v.position = offset + gm.offset + Vector2(gm.dim.x, 0);
		v.position.floor();
		v.uv = Vector2(rect.x + rect.width, rect.y);
		buffer[4 + n] = v;

	
		v.position = offset + gm.offset + gm.dim;
		v.position.floor();
		v.uv = Vector2(rect.x + rect.width, rect.y + rect.height);
		buffer[5 + n] = v;


		offset += Vector2(gm.dim.x + 2, 0);
		n += 6;
	}

	mesh->update();
}

void Font::draw(Layer * layer, const std::string & text, float x, float y)
{
	Model model(renderable);
	model.getTransform().translate(Vector3(x, y, 0));

	draw(mesh, text);
	layer->intermediate(&model);
	mesh->clear();
}

void Font::setBlendColor(const Color & color)
{
	renderable.getMaterial().blend = color;
}

float Font::width(const std::string & text)
{

	Vector2 size;
	Vector2 offset;

	for (std::string::const_iterator i = text.begin(); i != text.end(); ++i)
	{
		int c = *i;

		const FontGlyphMetric & gm = glyphs.at(c - 32);

		size += Vector2(gm.dim.x + 2, 0);
	}

	return size.x;
}