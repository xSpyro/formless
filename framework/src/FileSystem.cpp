
#include "FileSystem.h"

#include "Logger.h"

//#define ENABLE_PHYSFS

namespace FileSystem
{
	std::string base_dir = "../resources/";

	void init()
	{
#ifdef ENABLE_PHYSFS
		PHYSFS_init(NULL);
		
		//PHYSFS_setWriteDir("");
#endif
	}

	void addResource(const std::string & resource)
	{
#ifdef ENABLE_PHYSFS
		int res = PHYSFS_mount(resource.c_str(), "", false);
#endif
	}

	bool get(const std::string & src, File * file, bool useBase)
	{
#ifdef ENABLE_PHYSFS
		std::string path = src;

		if (PHYSFS_exists(path.c_str()))
		{
			PHYSFS_file * ph = PHYSFS_openRead(path.c_str());

			PHYSFS_uint32 size = static_cast < PHYSFS_uint32 > (PHYSFS_fileLength(ph));

			file->size = static_cast < unsigned int > (size);
			file->data = new char[file->size];

			PHYSFS_sint64 length_read = PHYSFS_read(ph, file->data, 1, size);

			PHYSFS_close(ph);

			//log()("File found: ", path);
			return true;
		}
		else
		{
			log().warn("File not found: ", path);
			return false;
		}
#else

		
		// automaticly add "/resources"
		std::string path = src;
		if (useBase)
			path = base_dir + src;


		std::ifstream stream(path, std::ios::binary);

		if (!stream.is_open())
		{
			log().warn("File not found: ", path);
			return false;
		}

		//log()("File found: ", path);

		stream.seekg(0, std::ios::end);
		std::streamoff off = stream.tellg();
		stream.seekg(0, std::ios::beg);

		// alloc
		file->size = static_cast < unsigned int > (off);
		file->data = new char[file->size];
		

		stream.read(file->data, off);
		stream.close();

		return true;
#endif
	}

	bool exists(const std::string & src, bool useBase)
	{
#ifdef ENABLE_PHYSFS
		std::string path = src;

		if (PHYSFS_exists(path.c_str()))
		{
			return true;
		}

		return false;
#else

		// automaticly add "/resources"
		std::string path = src;
		if (useBase)
			path = base_dir + src;


		std::ifstream stream(path);

		if (!stream.is_open())
			return false;

		return true;
#endif
	}

	bool write(const std::string & dst, const File * file, bool useBase)
	{
		// automaticly add "/resources"
		std::string path = dst;
		if (useBase)
			path = base_dir + dst;

		std::ofstream stream(path, std::ios::binary);

		if (!stream.is_open())
		{
			log().warn("Path not valid: ", path);
			return false;
		}

		stream.write(file->data, file->size);
		stream.close();

		return true;
	}

	Logger & log()
	{
		static Logger * logger = new Logger("FileSystem");

		return *logger;
	}

	void setBaseDirectory(const std::string & path)
	{
		base_dir = path;
	}

	void iterateUsingCallback(const std::string & folder, Callback callback, void * userdata)
	{
#ifdef ENABLE_PHYSFS
		char **rc = PHYSFS_enumerateFiles(folder.c_str());
		char **i;

		for (i = rc; *i != NULL; i++)
		{
			std::string name = *i;

			if (!name.empty())
			{
				if (name.front() == '.')
				{
					// svn file
				}
				else
				{
					callback(name, userdata);
				}
			}
		}

		PHYSFS_freeList(rc);
#endif
	}
}