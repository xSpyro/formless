
#ifndef MODEL_IMPORT_OBJECT_STREAM
#define MODEL_IMPORT_OBJECT_STREAM

#include "Common.h"

#include "Logger.h"

class ModelImportObjectFormatEnhanced;

struct ModelImportObjectStream
{
	std::stringstream str;
	ModelImportObjectFormatEnhanced * parent;

	Logger log;

	ModelImportObjectStream()
		: log("ModelImportObjectStreamEnhanced")
	{
	}
};

#endif