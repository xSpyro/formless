
#ifndef MATRIX
#define MATRIX

#include "Common.h"

struct Vector3;
struct Quaternion;

struct Matrix
{
	union{
		float m[16];
		float mm[4][4];
	};
	

	float operator [] (unsigned int index) const;

	Matrix operator * (const Matrix & rhs) const;

	void translate(const Vector3 & position);
	void scale(const Vector3 & scale);
	void rotate(const Vector3 & axis, float angle);
	void rotateQuaternion(const Quaternion & quat);

	void lookAt(const Vector3 & pos, const Vector3 & at, const Vector3 & up);

	void rotateUp(const Vector3 & up);

	Matrix inverseOf() const;
	void invert();
	void transpose();
	void identity();

	friend std::ostream & operator << (std::ostream & stream, const Matrix & self);
};

#endif