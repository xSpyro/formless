
#ifndef ANIMATION
#define ANIMATION

#include "Common.h"

struct Animation
{
	Animation();
	Animation(int src, int dst, float lerp);

	int src;
	int dst;
	float lerp;
};

#endif