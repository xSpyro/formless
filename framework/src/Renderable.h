
#ifndef RENDERABLE
#define RENDERABLE

#include "Common.h"

#include "Material.h"

class Mesh;
class Texture;
class Model;
class Layer;

struct Ray;
struct IntersectResult;

enum
{
	SHAPE_NOT_USED,
	SHAPE_POINT = 1,
	SHAPE_FILLED_RECT = 4,
	SHAPE_RECT = 5
};

class Renderable
{
public:

	Renderable();
	Renderable(Mesh * mesh, Texture * texture, Model * model);
	Renderable(unsigned int verts, Texture * texture);
	~Renderable();

	void setMesh(Mesh * mesh);
	void setTexture(Texture * texture);
	void setMaterial(const Material & material);
	void setMaterial(Material * material);

	void setVisible(bool state);
	inline bool isVisible() const		{ return visible; }

	Mesh * getMesh();
	const Mesh * getMesh() const;

	unsigned int getForcedVertices() const;

	Material & getMaterial();
	const Material & getMaterial() const;

	Texture * getTexture();
	const Texture * getTexture() const;

	void setParent(Model * model);
	const Model * getParent() const;

	bool pick(const Ray & ray, IntersectResult * result) const;


	void detach() const;

	void detach(Layer * layer) const;
	void attach(Layer * layer) const;

private:

	void init();

private:

	// if we have no mesh we can throw x amount of vertices in
	unsigned int forced_verts; 
	Material material;
	bool visible;


	Mesh * mesh;
	Texture * texture;
	Model * model; // parent


	// layer 
	// this is what layers
	// this renderable belongs to
	// max 4



	mutable Layer * holder[4];
};

#endif