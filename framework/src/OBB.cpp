#include "OBB.h"
#include "Matrix.h"

OBB::OBB(Vector3 dimensions /* = {0, 0, 0} */, Vector3 position /* = {0, 0, 0} */, Quaternion rotation /* {0, 0, 0, 1}*/) : dimensions(dimensions), pos(position), rotation(rotation)
{
	updateSlabs();
}

OBB::~OBB()
{}

Vector3 OBB::getDimensions() const
{
	return dimensions;
}

Vector3 OBB::getPosition() const
{
	return pos;
}

Vector3 OBB::getMax() const
{
	return pos + dimensions;
}

Vector3 OBB::getMin() const
{
	return pos - dimensions;
}

Quaternion OBB::getRotation() const
{
	return rotation;
}

void OBB::setPosition(Vector3 position)
{
	pos = position;
	updateSlabs();
}

void OBB::setDimensions(Vector3 dim)
{
	dimensions = dim;
	updateSlabs();
}

void OBB::setRotation(Quaternion rot)
{
	rotation = rot;
	updateSlabs();
}

void OBB::set(Vector3 dim, Vector3 position, Quaternion rot, bool setDim /* = true */, bool setPos /* = true */, bool setRot /* = true */)
{
	if (setDim)
		dimensions = dim;
	if (setPos)
		pos = position;
	if (setRot)
		rotation = rot;
	updateSlabs();
}


Matrix OBB::createMatrix()
{
	Matrix rtn;
	rtn.identity();
	rtn.scale(dimensions * 2);
	rtn.rotateQuaternion(rotation);
	rtn.translate(pos);

	return rtn;
}

void OBB::updateSlabs()
{
	Matrix m;
	m.identity();

	m.rotateQuaternion(rotation);
	// D3DXMatrixRotationYawPitchRoll(&m, mRotYPR.x, mRotYPR.y, mRotYPR.z);

	Vector3 v1(dimensions.x, 0, 0);
	Vector3 v2(0, dimensions.y, 0);
	Vector3 v3(0, 0, dimensions.z);

	v1.transform(m);
	v2.transform(m);
	v3.transform(m);

	slab[0].max.setPlane(pos - v1 - v2 - v3, pos - v1 - v2 + v3, pos + v1 - v2 - v3);
	slab[1].max.setPlane(pos - v1 - v2 - v3, pos - v1 - v2 + v3, pos - v1 + v2 - v3);
	slab[2].max.setPlane(pos - v1 - v2 - v3, pos - v1 + v2 - v3, pos + v1 - v2 - v3);

	slab[0].min.setPlane(pos + v1 + v2 + v3, pos + v1 + v2 - v3, pos - v1 + v2 + v3);
	slab[1].min.setPlane(pos + v1 + v2 + v3, pos + v1 + v2 - v3, pos + v1 - v2 + v3);
	slab[2].min.setPlane(pos + v1 + v2 + v3, pos + v1 - v2 + v3, pos - v1 + v2 + v3);

	for (int n = 0; n < 3; ++n)
	{
		slab[n].max.awayFrom(pos + v1 + v2 + v3, pos - v1 - v2 - v3);
		slab[n].min.awayFrom(pos - v1 - v2 - v3, pos + v1 + v2 + v3);
	}
}