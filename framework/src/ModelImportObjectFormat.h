
#ifndef MODEL_IMPORT_OBJECT_FORMAT
#define MODEL_IMPORT_OBJECT_FORMAT

#include "Common.h"

#include "ModelImportFormat.h"

#include "Vector3.h"
#include "Vector2.h"

class Renderer;
class Mesh;

class ModelImportObjectFormat : public ModelImportFormat
{
	struct Stream
	{
		std::stringstream str;
		Mesh * mesh;
		bool append;
	};

public:

	ModelImportObjectFormat(Renderer * renderer);

	bool import(File * file, ModelData * model);

private:

	bool parse(Stream & stream);

	void parseV(Stream & stream);
	void parseVN(Stream & stream);
	void parseVT(Stream & stream);
	void parseF(Stream & stream);

private:

	std::vector < Vector3 > positions;
	std::vector < Vector3 > normals;
	std::vector < Vector2 > texcoords;

	Renderer * renderer;
};

#endif