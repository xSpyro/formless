
#ifndef TIMER
#define TIMER

#include "Common.h"

class Timer
{

public:

	Timer();

	float now();
	bool accumulate(float peak);
	void restart();

	float getAccumulated();
private:

	__int64 start;
	__int64 end;
	__int64 freq;

	float accumulated;
	float delta;

};

#endif