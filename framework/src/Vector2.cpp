
#include "Vector2.h"

Vector2::Vector2()
	: x(0), y(0)
{
}

Vector2::Vector2(float x, float y)
	: x(x), y(y)
{
}

Vector2 Vector2::operator + (const Vector2 & rhs) const
{
	return Vector2(x + rhs.x, y + rhs.y);
}

Vector2 Vector2::operator - (const Vector2 & rhs) const
{
	return Vector2(x - rhs.x, y - rhs.y);
}

Vector2 Vector2::operator * (const Vector2 & rhs) const
{
	return Vector2(x * rhs.x, y * rhs.y);
}

Vector2 Vector2::operator * (float scalar) const
{
	return Vector2(x * scalar, y * scalar);
}

Vector2 & Vector2::operator += (const Vector2 & rhs)
{
	x += rhs.x;
	y += rhs.y;

	return *this;
}

void Vector2::floor()
{
	x = std::floor(x);
	y = std::floor(y);
}

float Vector2::length() const
{
	return sqrt(x * x + y * y);
}