
#ifndef PARTICLE_SYSTEM
#define PARTICLE_SYSTEM

#include "Common.h"

class Renderer;
class ParticleType;
class ParticleEmitter;
class ParticleBehavior;
class BaseShader;
class Mesh;

#include "ParticleGroup.h"

class ParticleSystem
{

	typedef std::map < std::string, ParticleType* > ParticleTypes;
	typedef std::map < std::string, const ParticleBehavior* > ParticleBehaviors;

	typedef std::map < std::string, ParticleGroup > Groups;

public:

	void init(Renderer * renderer);

	void update();

	bool createType(const std::string & name, BaseShader * shader);
	bool createBehavior(const std::string & name, const ParticleBehavior * behavior);

	void addToGroup(const std::string type_n, const std::string & name, const std::string & behavior, int life, float spawn_rate, float affect_rate);

	ParticleEmitter * spawnEmitter(const std::string & type);
	ParticleEmitter * spawnEmitter(const std::string & type, const std::string & behavior);

	void spawnGroup(const std::string & name, const Vector3 & position, const Vector3 & normal);

	Mesh * getParticleMesh(const std::string & type);

	bool kill();

	void killBehavior(const std::string & type, const std::string & behavior);

	Renderer * getRenderer();

private:

	ParticleTypes types;
	ParticleBehaviors behaviors;

	Groups groups;

	Renderer * renderer;
};

#endif