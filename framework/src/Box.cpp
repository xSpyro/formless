
#include "Box.h"

#include "Renderer.h"
#include "Mesh.h"

#include "MeshStream.h"

bool Box::generate(Renderer * renderer, BaseShader * shader, Desc * desc, Mesh ** out)
{
	Desc opt;
	opt.scale = 1.0f;
	opt.wireframe = true;

	if (desc)
	{
		opt = *desc;
	}

	int n_face;
	unsigned int indices;

	if (opt.wireframe)
	{
		n_face = 12;
		indices = 24;
	}
	else
	{
		n_face = 6;
		indices = 36;
	}

	unsigned int index = 0;
	unsigned int vertices = 8;
	

	*out = renderer->mesh()->create();
	Mesh * mesh = *out;

	MeshStream * stream = mesh->createEmptyStream("BoxStream");

	stream->addDesc("POSITION", sizeof(Vector3));
	stream->addDesc("NORMAL", sizeof(Vector3));
	stream->addDesc("TEXCOORD", sizeof(Vector2));

	stream->fill(vertices);

	Vertex * vertex = reinterpret_cast < Vertex* > (stream->get(index));

	vertex[0].position = Vector3(-0.5f, +0.5f, +0.5f) * opt.scale;
	vertex[1].position = Vector3(+0.5f, +0.5f, +0.5f) * opt.scale;
	vertex[2].position = Vector3(+0.5f, -0.5f, +0.5f) * opt.scale;
	vertex[3].position = Vector3(-0.5f, -0.5f, +0.5f) * opt.scale;

	vertex[4].position = Vector3(-0.5f, +0.5f, -0.5f) * opt.scale;
	vertex[5].position = Vector3(+0.5f, +0.5f, -0.5f) * opt.scale;
	vertex[6].position = Vector3(+0.5f, -0.5f, -0.5f) * opt.scale;
	vertex[7].position = Vector3(-0.5f, -0.5f, -0.5f) * opt.scale;

	if (!opt.wireframe)
	{



		// y+
		mesh->addIndex(0);
		mesh->addIndex(1);
		mesh->addIndex(2);

		mesh->addIndex(3);
		mesh->addIndex(0);
		mesh->addIndex(2);

		// y-
		mesh->addIndex(4);
		mesh->addIndex(5);
		mesh->addIndex(6);

		mesh->addIndex(7);
		mesh->addIndex(4);
		mesh->addIndex(6);

		// z+
		mesh->addIndex(0);
		mesh->addIndex(4);
		mesh->addIndex(1);

		mesh->addIndex(1);
		mesh->addIndex(4);
		mesh->addIndex(5);

		// z-
		mesh->addIndex(3);
		mesh->addIndex(7);
		mesh->addIndex(6);

		mesh->addIndex(6);
		mesh->addIndex(3);
		mesh->addIndex(2);

		// x-
		mesh->addIndex(3);
		mesh->addIndex(0);
		mesh->addIndex(4);

		mesh->addIndex(7);
		mesh->addIndex(3);
		mesh->addIndex(4);

		// x+
		mesh->addIndex(2);
		mesh->addIndex(1);
		mesh->addIndex(5);

		mesh->addIndex(6);
		mesh->addIndex(2);
		mesh->addIndex(5);

		mesh->setTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	}
	else
	{
		mesh->addIndex(0);
		mesh->addIndex(1);

		mesh->addIndex(1);
		mesh->addIndex(2);

		mesh->addIndex(2);
		mesh->addIndex(3);

		mesh->addIndex(3);
		mesh->addIndex(0);

		mesh->addIndex(4);
		mesh->addIndex(5);

		mesh->addIndex(5);
		mesh->addIndex(6);

		mesh->addIndex(6);
		mesh->addIndex(7);

		mesh->addIndex(7);
		mesh->addIndex(4);

		mesh->addIndex(0);
		mesh->addIndex(4);

		mesh->addIndex(1);
		mesh->addIndex(5);

		mesh->addIndex(2);
		mesh->addIndex(6);

		mesh->addIndex(3);
		mesh->addIndex(7);

		mesh->setTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	}

	
	mesh->submit(shader);
	return true;
}