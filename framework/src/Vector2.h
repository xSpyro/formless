
#ifndef VECTOR2
#define VECTOR2

#include "Common.h"

struct Vector2
{
	float x, y;

	Vector2();
	Vector2(float x, float y);

	Vector2 operator + (const Vector2 & rhs) const;
	Vector2 operator - (const Vector2 & rhs) const;
	Vector2 operator * (const Vector2 & rhs) const;
	Vector2 operator * (float scalar) const;

	Vector2 & operator += (const Vector2 & rhs);

	void floor();

	float length() const;
};

#endif