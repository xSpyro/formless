
#include "Texture.h"

#include "Renderer.h"
#include "Surface.h"
#include "Color.h"

#include "Mesh.h"
#include "MeshStream.h"

Texture::Texture()
{
	memset(mt, 0, 16 * sizeof(void *));
	depth = NULL;

	rtv = NULL;

	flags = TEXTURE_DEFAULT;

	use_depth = true;
}

bool Texture::load(Renderer * renderer, File * file, int flags)
{
	HRESULT hr = S_OK;

	this->renderer = renderer;

	switch ( flags )
	{

	case TEXTURE_DEFAULT:

		hr = D3DX11CreateShaderResourceViewFromMemory(renderer->getDevice(), file->data, file->size, NULL, NULL, &view, NULL);
		break;

	case TEXTURE_READ:

		break;
	}

	if (FAILED(hr))
	{
		return false;
	}


	if (flags == TEXTURE_READ)
	{
	
		
		D3DX11_IMAGE_LOAD_INFO info;
		ZeroMemory( &info, sizeof(D3DX11_IMAGE_LOAD_INFO) );
		info.BindFlags = 0;
		info.Usage = D3D11_USAGE_STAGING;
		info.CpuAccessFlags = D3D11_CPU_ACCESS_READ;
		info.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;

		hr = D3DX11CreateTextureFromMemory(renderer->getDevice(), file->data, file->size, &info, NULL, &resource, NULL);

		if (FAILED(hr))
		{
			return false;
		}

	}
	else
	{
		// get resource
		view->GetResource(&resource);
	}

	mt_count = 1;

	reinterpret_cast < ID3D11Texture2D* >(resource)->GetDesc(&desc);

	return true;
}

bool Texture::create(Renderer * renderer, int width, int height, int flags)
{
	HRESULT hr;

	this->renderer = renderer;
	this->flags = flags;

	if (flags == TEXTURE_MULTI)
	{
		mt_count = 0;
		return true;
	}

	// create texture RGBA
	ZeroMemory(&desc, sizeof(desc));
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	//desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;

	if (flags == TEXTURE_WRITE)
	{
		desc.Usage = D3D11_USAGE_DYNAMIC;
		desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else if (flags == TEXTURE_READ)
	{
		desc.Usage = D3D11_USAGE_STAGING;
		desc.BindFlags = 0;
		desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
	}
	else if (flags == TEXTURE_DEPTH)
	{
		desc.Format = DXGI_FORMAT_R32_TYPELESS;
		desc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	}
	else if (flags == TEXTURE_BITMAP)
	{
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	}
	else if (flags == TEXTURE_GRAYSCALE)
	{
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		desc.Format = DXGI_FORMAT_R8_UNORM;
	}
		else if (flags == TEXTURE_GRAYSCALE_WITH_DEPTH)
	{
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		desc.Format = DXGI_FORMAT_R32_FLOAT;
	}
	else if (flags == TEXTURE_RENDERTARGET)
	{
		//desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // maybe not
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	}
	else 
	{
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	}

	// we dont have initial data
	ID3D11Texture2D * tex2d = NULL;
	hr  = renderer->getDevice()->CreateTexture2D(&desc, NULL, &tex2d);

	if (FAILED(hr))
	{
		Logger::print().warn("CreateTexture: ", LOG_FAILED);
		Logger::print().info("Description: ", DXGetErrorDescription(hr));
		return false;
	}

	resource = tex2d;


	// special adds
	if (desc.BindFlags & D3D11_BIND_DEPTH_STENCIL)
	{
		// fill the dsv

		D3D11_DEPTH_STENCIL_VIEW_DESC ddesc;
		ZeroMemory(&ddesc, sizeof(ddesc));

		ddesc.Format = DXGI_FORMAT_D32_FLOAT;
		ddesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		ddesc.Texture2D.MipSlice = 0;
		renderer->getDevice()->CreateDepthStencilView(resource, &ddesc, &dsv);

		// set correct format for the shader resource view
		desc.Format = DXGI_FORMAT_R32_FLOAT;
	}

	if (desc.BindFlags & D3D11_BIND_RENDER_TARGET)
	{

		// fill the rtv
		
		D3D11_RENDER_TARGET_VIEW_DESC rt_desc;
		rt_desc.Format = desc.Format;
		rt_desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		rt_desc.Texture2D.MipSlice = 0;
		renderer->getDevice()->CreateRenderTargetView(tex2d, &rt_desc, &rtv);
	}

	if (desc.BindFlags & D3D11_BIND_SHADER_RESOURCE)
	{

		// create the shader-resource view
		D3D11_SHADER_RESOURCE_VIEW_DESC sr_desc;
		sr_desc.Format = desc.Format;
		sr_desc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2D;
		sr_desc.Texture2D.MostDetailedMip = 0;
		sr_desc.Texture2D.MipLevels = 1;

		hr = renderer->getDevice()->CreateShaderResourceView(resource, &sr_desc, &view);

		if (FAILED(hr))
		{
			Logger::print().alert("CreateShaderResourceView: ", LOG_FAILED);
			Logger::print().info("Description: ", DXGetErrorDescription(hr));
			return false;
		}

	}


	// if we are a rendertarget we attach a depth addon aswell
	if (flags == TEXTURE_RENDERTARGET || flags == TEXTURE_GRAYSCALE_WITH_DEPTH)
	{
		depth = new Texture;
		depth->create(renderer, width, height, TEXTURE_DEPTH);
	}



	mt_count = 1;

	reinterpret_cast < ID3D11Texture2D* >(resource)->GetDesc(&desc);

	return true;
}

bool Texture::bind(Renderer * renderer, ID3D11Resource * resource)
{
	this->resource = resource;
	this->renderer = renderer;

	flags = TEXTURE_DEFAULT;

	reinterpret_cast < ID3D11Texture2D* >(resource)->GetDesc(&desc);

	// create the shader-resource view
	D3D11_SHADER_RESOURCE_VIEW_DESC sr_desc;
	sr_desc.Format = desc.Format;
	sr_desc.ViewDimension = D3D10_SRV_DIMENSION_TEXTURE2D;
	sr_desc.Texture2D.MostDetailedMip = 0;
	sr_desc.Texture2D.MipLevels = 1;

	HRESULT hr = renderer->getDevice()->CreateShaderResourceView(resource, &sr_desc, &view);

	if (FAILED(hr))
	{
		Logger::print().alert("bind CreateShaderResourceView: ", LOG_FAILED);
		Logger::print().info("Description: ", DXGetErrorDescription(hr));
		return false;
	}

	return true;
}

bool Texture::bind(ID3D11RenderTargetView * rtv, Texture * depth)
{
	this->rtv = rtv;
	this->depth = depth;

	flags = TEXTURE_RENDERTARGET;

	rtv->GetResource(&resource);

	reinterpret_cast < ID3D11Texture2D* >(resource)->GetDesc(&desc);

	return true;
}

bool Texture::save(const std::string & fname) const
{
	HRESULT hr;

	if (flags == TEXTURE_DEPTH)
	{
		hr = D3DX11SaveTextureToFile(renderer->getContext(), resource, D3DX11_IFF_DDS, fname.c_str());
	}
	else
	{
		hr = D3DX11SaveTextureToFile(renderer->getContext(), resource, D3DX11_IFF_PNG, fname.c_str());

		if (FAILED(hr))
		{
			std::cout << "failed to save texture" << std::endl;
		}
	}

	return SUCCEEDED(hr);
}

void Texture::free()
{
	if (flags != TEXTURE_MULTI)
		resource->Release();
}

void Texture::render(const Surface & surface)
{
	ID3D11Texture2D * texture = (ID3D11Texture2D *)resource;

	texture->GetDesc(&desc);

	D3D11_MAPPED_SUBRESOURCE mapping;
	if (SUCCEEDED(renderer->getContext()->Map(texture, D3D10CalcSubresource(0, 0, 1), D3D11_MAP_WRITE_DISCARD, 0, &mapping)))
	{
		Color * pixels = (Color *)mapping.pData;

		surface.mapTo(pixels);

		renderer->getContext()->Unmap(texture, D3D10CalcSubresource(0, 0, 1));
	}
}

void Texture::read(Surface & dst)
{
	ID3D11Texture2D * texture = reinterpret_cast < ID3D11Texture2D* >(resource);

	texture->GetDesc(&desc);

	// refit surface
	dst.resize(desc.Width, desc.Height);

	D3D11_MAPPED_SUBRESOURCE mapping;
	if (SUCCEEDED(renderer->getContext()->Map(texture, D3D10CalcSubresource(0, 0, 1), D3D11_MAP_READ, 0, &mapping)))
	{
		Color * pixels = (Color *)mapping.pData;

		dst.write(pixels);

		renderer->getContext()->Unmap(texture, D3D10CalcSubresource(0, 0, 1));
	}
}

void Texture::clear()
{
	switch ( flags )
	{
	case TEXTURE_DEPTH:

		renderer->getContext()->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1, 0);
		break;

	case TEXTURE_DEFAULT:

		if (depth)
			depth->clear();

		// shall be no BREAK here

	case TEXTURE_GRAYSCALE:

		renderer->getContext()->ClearRenderTargetView(rtv, &clear_color.r);
		break;
	

	case TEXTURE_GRAYSCALE_WITH_DEPTH:
	case TEXTURE_RENDERTARGET:
		
		renderer->getContext()->ClearRenderTargetView(rtv, &clear_color.r);

		// always has depth addon
		depth->clear();

		break;
	}
}

void Texture::renderTo(ID3D11RenderTargetView ** out_rtv, ID3D11DepthStencilView ** out_dsv) const
{
	*out_rtv = rtv;
	*out_dsv = NULL;

	if (flags == TEXTURE_DEPTH)
	{
		*out_rtv = NULL;
		*out_dsv = dsv;
	}

	if (depth && use_depth)
	{
		*out_dsv = depth->dsv;
	}
}

ID3D11ShaderResourceView * Texture::getView()
{
	return view;
}

ID3D11ShaderResourceView * Texture::getView(unsigned int index)
{
	return mt[index]->view;
}

const ID3D11ShaderResourceView * Texture::getView() const
{
	return view;
}

const ID3D11ShaderResourceView * Texture::getView(unsigned int index) const
{
	return mt[index]->view;
}

ID3D11RenderTargetView * Texture::getRenderTargetView() const
{
	return rtv;
}

ID3D11DepthStencilView * Texture::getDepthStencilView() const
{
	return dsv;
}

ID3D11Resource * Texture::getResource()
{
	return resource;
}

int Texture::getWidth() const
{
	return desc.Width;
}

int Texture::getHeight() const
{
	return desc.Height;
}

int Texture::getFlags() const
{
	return flags;
}

bool Texture::isMultiTexture() const
{
	return flags == TEXTURE_MULTI;
}

unsigned int Texture::getTextureCount() const
{
	return mt_count; // will be 1 even if not a multitexture
}

void Texture::addMultiTexture(Texture * texture)
{
	if (texture == NULL || flags != TEXTURE_MULTI)
		return;

	// since we dont allow null textures, find the next null
	for (unsigned int i = 0; i < 16; ++i)
	{
		if (mt[i] == NULL)
		{
			mt[i] = texture;
			mt_count = i + 1;
			break;
		}
	}
}

void Texture::setMultiTexture(unsigned int index, Texture * texture)
{
	if (texture == NULL || flags != TEXTURE_MULTI)
		return;

	mt[index] = texture;

	if (index + 1 > mt_count)
	{
		mt_count = index + 1;
	}
}

Texture * Texture::getMultiTexture(unsigned int index) const
{
	return mt[index];
}

void Texture::setClearColor(const Color & color)
{
	clear_color = color;
}

void Texture::setDepthAddon(Texture * texture)
{
	depth = texture;
}

Texture * Texture::getDepthAddon() const
{
	return depth;
}

void Texture::useDepthAddon(bool state)
{
	use_depth = state;
}