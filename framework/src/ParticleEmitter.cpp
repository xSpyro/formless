
#include "ParticleEmitter.h"

#include "MeshStream.h"
#include "Particle.h"
#include "ParticleType.h"
#include "ParticleBehavior.h"

#include "Helpers.h"

ParticleEmitter::ParticleEmitter()
	: start(0), end(0), type(NULL)
{
	life = life_max = 0;
	rate = 1.0f;
	life_affect_rate = 0.0f;

	spawn = 1.0f; // we create one particle instantly

	ptr_position = &position;

	userdata = NULL;
	onDestroy = NULL;
}

ParticleEmitter::ParticleEmitter(int start, int end, ParticleType * type, const ParticleBehavior * behavior)
	: start(start), end(end), type(type), behavior(behavior)
{
	stream = type->getStream();

	life = life_max = 0;
	rate = 1.0f;
	life_affect_rate = 1.0f;

	spawn = 1.0f; // we create one particle instantly

	ptr_position = &position;

	userdata = NULL;
	onDestroy = NULL;
}

ParticleEmitter::~ParticleEmitter()
{
	if (onDestroy)
	{
		onDestroy(this, userdata);
	}
}

bool ParticleEmitter::update(MeshStream * stream)
{
	for (int i = start; i < end; ++i)
	{
		Particle * p = (Particle *)stream->get(i);

		for (Attractors::iterator a = attractors.begin(); a != attractors.end(); ++a)
		{
			a->apply(p);
		}

		behavior->process(*p);
	}

	

	
	while ( spawn >= 1.0f )
	{
		Particle * p = getNextDeadParticle();
		if (p)
		{
			behavior->setNormal(normalW);
			behavior->spawn(*p);
			p->position += *ptr_position;
			p->position += Vector3(
				random(-spawn_box.x, spawn_box.x),
				random(-spawn_box.y, spawn_box.y),
				random(-spawn_box.z, spawn_box.z));
		}

		spawn -= 1.0f;
	}

	if (life_max > 0)
	{
		float ratio = (life / static_cast < float > (life_max)) * life_affect_rate;

		if (life > 0)
		{
			spawn += rate * (1 - ratio);
		}
		else if (life < -8 * 60)
		{
			return false;
		}

		--life;
	}
	else
	{
		spawn += rate;
	}

	return true;
}

void ParticleEmitter::bind(callback cb, void * userdata)
{
	onDestroy = cb;
	this->userdata = userdata;
}

void ParticleEmitter::move(const Vector3 & pos)
{
	position = pos;
	ptr_position = &position;
}

void ParticleEmitter::attach(const Vector3 * pos)
{
	ptr_position = pos;
}

void ParticleEmitter::detach()
{
	position = *ptr_position;
	ptr_position = &position;
}

void ParticleEmitter::setSpawnRate(float rate)
{
	this->rate = rate;

}

void ParticleEmitter::setSpawnBox(const Vector3 & box)
{
	spawn_box = box;
}

void ParticleEmitter::setLife(int life)
{
	this->life = life_max = life;
}

void ParticleEmitter::setLifeAffectRate(float amount)
{
	life_affect_rate = amount; // 0 - 1
}
int ParticleEmitter::addAttractor(const Vector3 & rel, float range)
{
	attractors.push_back(ParticleAttractor(rel, range));

	return attractors.size() - 1;
}

void ParticleEmitter::attractorMove(int index, const Vector3 & rel)
{
	attractors[index].setPosition(rel);
}

void ParticleEmitter::attractorRange(int index, float range)
{
	attractors[index].setRange(range);
}
void ParticleEmitter::setNormal(const Vector3 & n)
{
	normalW = n;
}
Particle * ParticleEmitter::getNextDeadParticle()
{
	for (int i = start; i < end; ++i)
	{
		Particle * p = (Particle *)stream->get(i);
		if (p->life <= 0.0f)
			return p;
	}


	return NULL;
}
