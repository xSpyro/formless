
#ifndef TEXTURE
#define TEXTURE

#include "Common.h"

#include "File.h"
#include "Color.h"

class Surface;
class Renderer;
class Layer;
class Mesh;

enum
{
	TEXTURE_DEFAULT,
	TEXTURE_READ,
	TEXTURE_WRITE,
	TEXTURE_DEPTH,
	TEXTURE_BITMAP,
	TEXTURE_MULTI,
	TEXTURE_RENDERTARGET,
	TEXTURE_GRAYSCALE,
	TEXTURE_GRAYSCALE_WITH_DEPTH
};

class Texture
{
public:

	Texture();

	bool load(Renderer * renderer, File * file, int flags);
	bool create(Renderer * renderer, int width, int height, int flags);
	bool bind(Renderer * renderer, ID3D11Resource * resource);
	bool bind(ID3D11RenderTargetView * rtv, Texture * depth);
	bool save(const std::string & fname) const;
	void free();

	void render(const Surface & surface);
	void read(Surface & dst);

	void clear();
	void renderTo(ID3D11RenderTargetView ** out_rtv, ID3D11DepthStencilView ** out_dsv) const;

	ID3D11ShaderResourceView * getView();
	ID3D11ShaderResourceView * getView(unsigned int index);

	const ID3D11ShaderResourceView * getView() const;
	const ID3D11ShaderResourceView * getView(unsigned int index) const;

	ID3D11RenderTargetView * getRenderTargetView() const;
	ID3D11DepthStencilView * getDepthStencilView() const;

	ID3D11Resource * getResource();

	int getWidth() const;
	int getHeight() const;
	int getFlags() const;

	// multitexture
	bool isMultiTexture() const;

	unsigned int getTextureCount() const;

	void addMultiTexture(Texture * texture);
	void setMultiTexture(unsigned int index, Texture * texture);
	Texture * getMultiTexture(unsigned int index) const;

	void setClearColor(const Color & color);

	Texture * getDepthAddon() const;
	void setDepthAddon(Texture * texture);

	void useDepthAddon(bool state);

private:

	D3D11_TEXTURE2D_DESC desc;
	Color clear_color;

	Renderer * renderer;
	int flags;

	ID3D11Resource * resource;
	ID3D11ShaderResourceView * view;

	union
	{
		ID3D11RenderTargetView * rtv;
		ID3D11DepthStencilView * dsv;
	};

	// depth addon
	Texture * depth;
	bool use_depth;

	// multitexture
	Texture * mt[16];
	unsigned int mt_count;
};

#endif