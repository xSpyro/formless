
#ifndef APPMAIN
#define APPMAIN

#include "Common.h"

#include "Window.h"
#include "Renderer.h"
#include "Timer.h"

#include "AudioSystem.h"

#include "ParticleEmitter.h"
#include "Camera.h"


#include "Logger.h"

class AppMain : public Window
{
public:

	AppMain();

	int run(int argc, char ** argv);

	void onMouseMovement(int x, int y);
	void onInput(int key, int state);

private:

	Logger log;

	Renderer renderer;
	Timer timeframe;

	AudioSystem audio;

	
	Camera camera;
	Layer * objects;
	Layer * landscape;
	Texture * shadowmap;
	Camera sun;
	Texture * godraysmap;

	float camera_scroll;
	float camera_scroll_move;
	Vector3 camera_position[2];
	Vector3 camera_lookat[2];

	Vector3 hit_point;
};


#endif