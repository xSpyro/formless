
#include "Matrix.h"

#include "Vector3.h"

#include "Quaternion.h"

float Matrix::operator [] (unsigned int index) const
{
	return m[index];
}

Matrix Matrix::operator * (const Matrix & rhs) const
{
	static Matrix mat;
	D3DXMatrixMultiply((D3DXMATRIX*)&mat, (const D3DXMATRIX*)this, (const D3DXMATRIX*)&rhs);

	return mat;
}

void Matrix::translate(const Vector3 & position)
{
	static Matrix mat;
	
	D3DXMatrixTranslation((D3DXMATRIX*)&mat, position.x, position.y, position.z);
	D3DXMatrixMultiply((D3DXMATRIX*)this, (D3DXMATRIX*)this, (D3DXMATRIX*)&mat);
}

void Matrix::scale(const Vector3 & scale)
{
	static Matrix mat;
	
	D3DXMatrixScaling((D3DXMATRIX*)&mat, scale.x, scale.y, scale.z);
	D3DXMatrixMultiply((D3DXMATRIX*)this, (D3DXMATRIX*)this, (D3DXMATRIX*)&mat);
}

void Matrix::rotate(const Vector3 & axis, float angle)
{
	static Matrix mat;
	
	D3DXMatrixRotationAxis((D3DXMATRIX*)&mat, (const D3DXVECTOR3*)&axis, angle);
	D3DXMatrixMultiply((D3DXMATRIX*)this, (D3DXMATRIX*)this, (D3DXMATRIX*)&mat);
}

void Matrix::rotateQuaternion(const Quaternion & quat)
{
	static Matrix mat;
	
	D3DXMatrixRotationQuaternion((D3DXMATRIX*)&mat, (D3DXQUATERNION*)&quat);
	D3DXMatrixMultiply((D3DXMATRIX*)this, (D3DXMATRIX*)this, (D3DXMATRIX*)&mat);
}



void Matrix::lookAt(const Vector3 & pos, const Vector3 & at, const Vector3 & up)
{
	D3DXMatrixLookAtLH((D3DXMATRIX*)this, (D3DXVECTOR3*)&pos, (D3DXVECTOR3*)&at, (D3DXVECTOR3*)&up);
}

void Matrix::rotateUp(const Vector3 & up)
{
	static Matrix mat;
	mat.identity();

	// right is 1, 0, 0
	mat.m[0] = up.y;
	mat.m[1] = -up.x;

	// set the up
	mat.m[4] = up.x;
	mat.m[5] = up.y;
	mat.m[6] = up.z;

	// adjust the right
	Vector3 binormal = reinterpret_cast < Vector3* >(&mat.m[0])->cross(*reinterpret_cast < Vector3* >(&mat.m[4]));

	mat.m[8] = binormal.x;
	mat.m[9] = binormal.y;
	mat.m[10] = binormal.z;

	D3DXMatrixMultiply((D3DXMATRIX*)this, (D3DXMATRIX*)this, (D3DXMATRIX*)&mat);
}

Matrix Matrix::inverseOf() const
{
	Matrix out;
	D3DXMatrixInverse((D3DXMATRIX*)&out, NULL, (D3DXMATRIX*)this);

	return out;
}

void Matrix::invert()
{
	D3DXMatrixInverse((D3DXMATRIX*)this, NULL, (const D3DXMATRIX*)this);
}

void Matrix::transpose()
{
	D3DXMatrixTranspose((D3DXMATRIX*)this, (D3DXMATRIX*)this);
}

void Matrix::identity()
{
	D3DXMatrixIdentity((D3DXMATRIX*)this);
}

std::ostream & operator << (std::ostream & stream, const Matrix & self)
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			stream << self.m[i * 4 + j] << " ";
		}

		stream << std::endl;
	}

	return stream;
}