
#ifndef COMMON
#define COMMON

#include <physfs.h>

#include <windows.h>
#include <shellapi.h>

#include <d3d11.h>
#include <d3dx11.h>
#include <d3dx10math.h>

#include <d3d9.h>

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>

#include <dxerr.h>

#include <fmod.hpp>
#include <fmod_errors.h>

#include <EventLua.h>

#include <ft2build.h>
#include FT_FREETYPE_H 

#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
#include <stack>

#include <sstream>
#include <fstream>

#include <ctime>
#include <cmath>
#include <clocale>

#include <conio.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846f
#endif

#undef min
#undef max
#endif