
#ifndef ANIMATION_GROUP
#define ANIMATION_GROUP

#include "Common.h"
#include "Animation.h"

class AnimationGroup
{
	struct Frame
	{
		int mesh;
		float curve;
	};

	typedef std::map < unsigned int, Frame > Timeline;

public:

	void insertFrame(int pos, int mesh, float curve = 1.0f);


	unsigned int getLength() const;
	Animation getFrame(unsigned int pos) const;

	const std::string & getName() const;
	void setName(const std::string & name);

private:

	Timeline::const_iterator getLeftBound(unsigned int pos) const;

private:

	std::string name;
	unsigned int length;
	Timeline timeline;
};

#endif