#ifndef ALL_MODULES_ACHIEVEMENT__H
#define ALL_MODULES_ACHIEVEMENT__H

#include "Achievement.h"

class AllModulesAchievement : public Achievement
{
public:
	AllModulesAchievement();
	AllModulesAchievement( std::string name, std::string description );

	bool validate( const Achievement::InData & data );
};

#endif
