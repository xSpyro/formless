#ifndef ITEM_INFO_H
#define ITEM_INFO_H

#include <string>

class ItemInfo
{

public:

	ItemInfo();
	ItemInfo( unsigned int uid, const std::string & name );
	ItemInfo( unsigned int uid, unsigned int userid, unsigned int type, unsigned int slotX, unsigned int slotY, const std::string & name );

	const char * getItemName() const;

// public aswell

	unsigned int uid;
	unsigned int userid;
	unsigned int type;
	unsigned int slotX;
	unsigned int slotY;
	char name[64];

	unsigned int timeout;
};

#endif
