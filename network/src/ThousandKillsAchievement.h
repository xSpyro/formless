#ifndef THOUSAND_KILLS_ACHIEVEMENT__H
#define THOUSAND_KILLS_ACHIEVEMENT__H

#include "Achievement.h"

class ThousandKillsAchievement : public Achievement
{
public:
	ThousandKillsAchievement();
	ThousandKillsAchievement( std::string name, std::string description );

	bool validate( const Achievement::InData & data );
};

#endif
