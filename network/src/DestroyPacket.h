
#ifndef DESTROY_PACKET_H
#define DESTROY_PACKET_H

#include "RawPacket.h"

struct DestroyPacket : public NetPacket
{
	unsigned int uid;

	DestroyPacket() : NetPacket(DESTROY_PACKET, 0)
	{
	}
};

#endif