
#ifndef SERVER_LIST_PACKET_H
#define SERVER_LIST_PACKET_H

#include "RawPacket.h"

struct ServerListPacket : public NetPacket
{
	unsigned int results;

	ServerListPacket() : NetPacket(SERVER_LIST_PACKET, 0)
	{
	}
};

#endif