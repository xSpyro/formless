#ifndef PLAYTIME_ACHIEVEMENT__H
#define PLAYTIME_ACHIEVEMENT__H

#include "Achievement.h"

class PlaytimeAchievement : public Achievement
{
public:
	PlaytimeAchievement();
	PlaytimeAchievement( std::string name, std::string description );

	bool validate( const Achievement::InData & data );
};

#endif
