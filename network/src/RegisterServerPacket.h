
#ifndef REGISTER_SERVER_PACKET_H
#define REGISTER_SERVER_PACKET_H

#include "RawPacket.h"

struct RegisterServerPacket : public NetPacket
{
	char name[64];

	RegisterServerPacket() : NetPacket(REGISTER_SERVER_PACKET, 0)
	{
	}

	void setName( const std::string & str )
	{
		strcpy_s( name, 64, str.c_str() );
	}
};

#endif