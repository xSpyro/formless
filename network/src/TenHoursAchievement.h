#ifndef TEN_HOURS_ACHIEVEMENT__H
#define TEN_HOURS_ACHIEVEMENT__H

#include "Achievement.h"

class TenHoursAchievement : public Achievement
{
public:
	TenHoursAchievement();
	TenHoursAchievement( std::string name, std::string description );

	bool validate( const Achievement::InData & data );
};

#endif
