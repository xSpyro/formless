
#ifndef GAME_START_PACKET_H
#define GAME_START_PACKET_H

#include "RawPacket.h"

struct GameStartPacket : public NetPacket
{
	unsigned int user_id;

	GameStartPacket() : NetPacket(GAME_START_PACKET, 0)
	{
	}
};



#endif