
#ifndef ENTER_ARENA_PACKET_H
#define ENTER_ARENA_PACKET_H

#include "RawPacket.h"

struct EnterArenaPacket : public NetPacket
{
	EnterArenaPacket() : NetPacket(ENTER_ARENA_PACKET, 0)
	{
	}
};



#endif