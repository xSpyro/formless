
#ifndef FILE_TRANSFER_PACKET_H
#define FILE_TRANSFER_PACKET_H

#include "RawPacket.h"

struct FileTransferPacket : public NetPacket
{
	char cmd[128];

	FileTransferPacket() : NetPacket(FILE_TRANSFER_PACKET, 0)
	{
	}
};

#endif