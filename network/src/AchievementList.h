#ifndef ACHIEVEMENT_LIST__H
#define ACHIEVEMENT_LIST__H

#include "AchievementInfo.h"
#include <vector>

class AchievementList
{
	typedef std::vector < AchievementInfo > Achievements;

public:
	void registerItem( const AchievementInfo & achievement );
	void clear();

	char * getDataPointer();
	unsigned int getDataSize();

	unsigned int getNumberOfEntries() const;

private:
	Achievements achievements;
};

#endif