#include "TenHoursAchievement.h"

TenHoursAchievement::TenHoursAchievement()
	: Achievement()
{
	type			= Achievement::TYPE_10_HOURS;
	name			= "Got nothing better to do?";
	description		= "10 hours playtime";
}

TenHoursAchievement::TenHoursAchievement( std::string name, std::string description )
	: Achievement( name, description )
{
	type			= Achievement::TYPE_10_HOURS;
}

bool TenHoursAchievement::validate( const Achievement::InData & data )
{
	bool result = false;

	if( data.type == type && data.playtime >= 36000 )
	{
		result = true;
	}

	return result;
}
