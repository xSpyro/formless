
#include "ServerInfo.h"

ServerInfo::ServerInfo(const std::string & ip, const std::string & name)
{
	strcpy_s(this->ip, 32, ip.c_str());
	strcpy_s(this->name, 64, name.c_str());
	timeout = 1000;
}

const char * ServerInfo::getIp() const
{
	return ip;
}

const char * ServerInfo::getName() const
{
	return name;
}
