
#ifndef FILE_REQUEST_INTERFACE_H
#define  FILE_REQUEST_INTERFACE_H

#include <iostream>
#include <boost/asio.hpp>

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <vector>

using boost::asio::ip::tcp;



class FileRequestInterface// : public boost::enable_shared_from_this<FileRequestInterface>
{

public:

	FileRequestInterface(boost::asio::io_service& service, const std::string & host);
	~FileRequestInterface();

	bool request(const std::string & cmd);

	virtual void onStart() { }
	virtual void onRate(unsigned int bytes, unsigned int total) { }
	virtual void onDone(unsigned char * data, unsigned int bytes) { }

private:

	//typedef boost::shared_ptr<FileRequestInterface> pointer;
	//static pointer create(boost::asio::io_service& service, const std::string & host);

	bool connect(const std::string & host);
	bool disconnect();

	void handleConnect(const boost::system::error_code& error);
	void handleRecv(const boost::system::error_code& error, unsigned int n);

private:

	std::string host;
	std::string cmd;
	std::vector < unsigned char > file;

	tcp::socket socket;
	

	boost::array<char, 10240> buffer;

	unsigned int bytesRead;
	unsigned int totalSize;
};




#endif