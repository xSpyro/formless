#ifndef SERVER_JOIN_PACKET__H
#define SERVER_JOIN_PACKET__H

#include "RawPacket.h"

struct ServerJoinPacket : public NetPacket
{
	unsigned int user_id;
	char server[32];

	ServerJoinPacket() : NetPacket(SERVER_JOIN_PACKET, 0), user_id(0)
	{
	}

	void setServer( const std::string & str )
	{
		strcpy_s( server, 32, str.c_str() );
	}
};

#endif
