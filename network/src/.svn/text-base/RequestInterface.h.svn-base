
#ifndef REQUEST_INTERFACE_H
#define REQUEST_INTERFACE_H

#include <iostream>
#include <boost/asio.hpp>

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "RawPacket.h"
#include "Packer.h"

#include "Utils.h"

// might not be safe at all
static const std::string MASTER_HOST = queryMasterHostAddress();

using boost::asio::ip::tcp;

class MasterSendAsync;

static const size_t BUFFER_SIZE = 10000;

class RequestInterface : public Packer, public boost::enable_shared_from_this<RequestInterface>
{
	friend class MasterSendAsync;
	friend class MasterRequestAsync;

public:

	RequestInterface(boost::asio::io_service& service, const std::string & host);
	~RequestInterface();


	bool request(const NetPacket & packet, unsigned int size, boost::array<char, BUFFER_SIZE> & out);
	bool send(const NetPacket & packet, unsigned int size);
	bool send(const NetPacket & packet, const void * data, unsigned int bytes);

	void sendAsync(const NetPacket & packet, unsigned int size);

	typedef void (*ResultFunc)(RawPacket & result);

	void requestAsync(const NetPacket & packet, unsigned int size);
	static void setRequestAsyncData(boost::asio::io_service & service, ResultFunc callback);


private:

	typedef boost::shared_ptr<RequestInterface> pointer;
	static pointer create(boost::asio::io_service& service, const std::string & host);



	bool connect(const std::string & host);
	void connectAsync(const std::string & host);
	bool disconnect();

	void onPacket(RawPacket & packet);

	void handleSendAsync(const boost::system::error_code& error);

	void handleWrite(const boost::system::error_code& error, unsigned int n);

	void handleComplete(const boost::system::error_code& error);

	void handleRecvAsync(const boost::system::error_code & error, unsigned int n);

	void handleConnectAsync(const boost::system::error_code& error);
	void handleSendAfterConnectAsync(const boost::system::error_code & error, unsigned int n);

private:

	std::string host;
	tcp::socket socket;

	unsigned int size;
	bool stop;

	boost::array<char, BUFFER_SIZE> buffer;
	boost::array<char, BUFFER_SIZE> * result;

	struct RequestAsyncData
	{
		boost::asio::io_service * service;
		ResultFunc callback;
	};

	static RequestAsyncData rsd;

};

// wrapper
class MasterRequest
{

public:

	MasterRequest()
	{

	}

	~MasterRequest()
	{
		
	}

	bool operator () (const NetPacket & packet, unsigned int size, RawPacket & recv)
	{
		//RequestInterface req(service, "194.47.150.40");
		RequestInterface req(service, MASTER_HOST);
		bool result = req.request(packet, size, buffer);

		recv = *reinterpret_cast < RawPacket* > (buffer.data());
		recv.data = buffer.data();

		return result;
	}

private:

	boost::array<char, BUFFER_SIZE> buffer;
	boost::asio::io_service service;
};

// wrapper
class MasterSend
{

public:

	MasterSend()
	{

	}

	~MasterSend()
	{
		
	}

	bool operator () (const NetPacket & packet, unsigned int size)
	{
		//RequestInterface req(service, "194.47.150.40");
		RequestInterface req(service, MASTER_HOST);
		bool result = req.send(packet, size);

		return result;
	}

	bool operator () (const NetPacket & packet, const void * data, unsigned int bytes)
	{
		//RequestInterface req(service, "194.47.150.40");
		RequestInterface req(service, MASTER_HOST);
		bool result = req.send(packet, data, bytes);

		return result;
	}

private:

	boost::asio::io_service service;
};

class MasterSendAsync
{

public:

	MasterSendAsync()
	{

	}

	~MasterSendAsync()
	{
		
	}

	void operator () (const NetPacket & packet, unsigned int size)
	{
		RequestInterface::pointer req = RequestInterface::create(service, MASTER_HOST);
		
		req->sendAsync(packet, size);
	}

private:

	boost::asio::io_service service;
};

// wrapper
class MasterRequestAsync
{

public:

	MasterRequestAsync()
	{

	}

	~MasterRequestAsync()
	{
		
	}

	bool operator () (const NetPacket & packet, unsigned int size)
	{
		RequestInterface::pointer req = RequestInterface::create(*RequestInterface::rsd.service, MASTER_HOST);

		req->result = &buffer;

		req->requestAsync(packet, size);

		return true;
	}

private:

	boost::array<char, BUFFER_SIZE> buffer;
};

#endif