
#ifndef TIMER_PACKET_H
#define TIMER_PACKET_H

#include "RawPacket.h"

struct TimerPacket : public NetPacket
{
	float			time;
	int				kills[3];
	bool			gameStarted;
	bool			allReady;

	TimerPacket() : NetPacket(TIMER_PACKET, 0)
	{
	}
};

#endif