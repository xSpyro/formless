#include "PlayerInfo.h"

PlayerInfo::PlayerInfo()
{
	this->uid	= 0;
	this->team	= 0;
	this->time	= 0;
	timeout		= 1000;

	strcpy_s( this->name, 64, "" );
}

PlayerInfo::PlayerInfo( unsigned int uid, unsigned int team, const std::string& name )
{
	this->uid	= uid;
	this->team	= team;
	this->time	= 0;
	timeout		= 1000;

	strcpy_s( this->name, 64, name.c_str() );
}

unsigned int PlayerInfo::getUserID()
{
	return uid;
}

const char* PlayerInfo::getName() const
{
	return name;
}
