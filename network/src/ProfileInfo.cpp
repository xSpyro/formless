#include "ProfileInfo.h"

ProfileInfo::ProfileInfo( const std::string & slot, const std::string & name, unsigned int size, unsigned int uid )
{
	strcpy_s(this->slot, 32, slot.c_str());
	strcpy_s(this->name, 64, name.c_str());
	this->size = size;
	this->uid = uid;
	timeout = 1000;
}

const char * ProfileInfo::getSlot() const
{
	return slot;
}

const char * ProfileInfo::getName() const
{
	return name;
}

unsigned int ProfileInfo::getSize() const
{
	return size;
}
