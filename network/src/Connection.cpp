
#include "Connection.h"

#include "ServerInterface.h"

#include "../game/src/MemLeaks.h"

// Connection constructor is private use create function
Connection::Connection(boost::asio::io_service& io_service)
	: socket(io_service)
{

}

Connection::~Connection()
{

}

Connection::pointer Connection::create(boost::asio::io_service& io_service)
{
	return pointer(New Connection(io_service));
}

tcp::socket& Connection::getSocket()
{
	return socket;
}

boost::asio::ip::address& Connection::getAddress()
{
	return remote_address;
}

unsigned int Connection::getId() const
{
	return id;
}

void Connection::start(ServerInterface * server, unsigned int id)
{
	this->server = server;
	this->id = id;

	server->respond_to = this;
	server->registerConnection(id, this);

	socket.async_receive(boost::asio::buffer(buffer), boost::bind(&Connection::handleRecv, shared_from_this(),
		boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));


	// find endpoint

	boost::asio::ip::tcp::socket::endpoint_type e;
	e = socket.remote_endpoint();

	remote_address = e.address();
	//std::cout << " -- Client Connected: " << remote_address.to_string() << std::endl;
}

void Connection::send(const std::string & message)
{
	boost::asio::socket_base::message_flags flags = 0;
	
	/*
	socket.async_send(boost::asio::buffer(message), flags, boost::bind(&Connection::handleSend, shared_from_this(),
		boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
	*/
	if (socket.is_open())
	{
		size_t sent = socket.send(boost::asio::buffer(message), flags);

		server->bytes_out += sent;

		socket.get_io_service().poll();
	}
}

void Connection::send(const void * data, unsigned int bytes)
{
	boost::asio::socket_base::message_flags flags = 0;
	
	/*
	socket.async_send(boost::asio::buffer(data, bytes), flags, boost::bind(&Connection::handleSend, shared_from_this(),
		boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
	*/

	if (socket.is_open())
	{
		size_t sent = socket.send(boost::asio::buffer(data, bytes), flags);

		server->bytes_out += sent;

		socket.get_io_service().poll();
	}
}

void Connection::sendCombined(const void * header, unsigned int header_size, const void * data, unsigned int size)
{
	boost::asio::socket_base::message_flags flags = 0;
	boost::system::error_code error;

	size_t sent = 0;

	if (socket.is_open())
	{
		sent += socket.send(boost::asio::buffer(header, header_size), flags, error);
		sent += socket.send(boost::asio::buffer(data, size), flags, error);

		server->bytes_out += sent;

		//std::cout << "Sent " << sent << " bytes of combined data" << std::endl;

		if (sent != header_size + size)
			std::cout << error << std::endl;

		socket.get_io_service().poll();
	}
}

void Connection::send(const NetPacket & packet, unsigned int size)
{
	boost::asio::socket_base::message_flags flags = 0;

	packet.size = size - RawPacket::HeaderSize;
	
	//socket.async_send(boost::asio::buffer(&packet, size), flags, boost::bind(&Connection::handleSend, shared_from_this(),
	//	boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
	if (socket.is_open())
	{
		try
		{
			size_t sent = socket.send(boost::asio::buffer(&packet, size), flags);

			server->bytes_out += sent;

			if (sent - size != 0)
				std::cout << "WantedSize " << size << " Sent " << sent << std::endl;

			socket.get_io_service().poll();
		}
		catch ( std::exception e )
		{

		}
	}
}

void Connection::poll()
{
	socket.get_io_service().poll();
}

void Connection::onPacket(RawPacket & packet)
{
	server->respond_to = this;
	server->onPacket(packet);
}

void Connection::handleRecv(const boost::system::error_code& error, size_t n)
{	
	if (error)
	{
		if (error.value() == 10054)
		{
			//std::cout << " -- Client Disconnected: " << remote_address.to_string() << std::endl;
		}
		else
		{
			//std::cout << " -- " << error.message() << " error occured: " << remote_address.to_string() << std::endl;
		}

		server->handleDisconnect(id);
		socket.close();
	}
	else
	{
		server->bytes_in += n;

		// separate connection from server
		server->respond_to = this;
		server->onReceive(buffer.data(), n);
		packup(buffer.data(), n);

		try
		{
			socket.async_receive(boost::asio::buffer(buffer), boost::bind(&Connection::handleRecv, shared_from_this(),
				boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
		}
		catch ( std::exception e )
		{
		}


	}
}

void Connection::handleSend(const boost::system::error_code& error, size_t n)
{

}