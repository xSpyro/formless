#include "ProfileList.h"

void ProfileList::registerItem( const ProfileItem & item )
{
	items.push_back( item );
}

void ProfileList::clear()
{
	items.clear();
}

char * ProfileList::getDataPointer()
{
	if (items.empty())
		return NULL;

	return reinterpret_cast < char* > (&items.front());
}

unsigned int ProfileList::getDataSize()
{
	return items.size() * sizeof(ProfileItem);
}

unsigned int ProfileList::getNumberOfEntries() const
{
	return items.size();
}
