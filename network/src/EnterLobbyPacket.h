

#ifndef ENTER_LOBBY_PACKET_H
#define ENTER_LOBBY_PACKET_H

#include "RawPacket.h"

struct EnterLobbyPacket : public NetPacket
{
	unsigned int uid;
	int team;

	EnterLobbyPacket() : NetPacket(ENTER_LOBBY_PACKET, 0)
	{
	}
};



#endif