#ifndef PLAYTIME_UPDATE_PACKET__H
#define PLAYTIME_UPDATE_PACKET__H

#include "RawPacket.h"

struct PlaytimeUpdatePacket : public NetPacket
{
	unsigned int uid;
	int time;

	PlaytimeUpdatePacket() : NetPacket(PLAYTIME_UPDATE_PACKET, 0), uid(0), time(0)
	{
	}
};

#endif
