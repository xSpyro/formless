
#ifndef REQUEST_READY_PACKET_H
#define REQUEST_READY_PACKET_H

#include "RawPacket.h"

struct RequestReadyPacket : public NetPacket
{
	RequestReadyPacket() : NetPacket(REQUEST_READY_PACKET, 0)
	{
	}
};



#endif