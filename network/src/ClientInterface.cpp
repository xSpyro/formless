
#include "ClientInterface.h"

ClientInterface::ClientInterface(boost::asio::io_service& service)
	: socket(service)
{
}

ClientInterface::~ClientInterface()
{
	socket.close();
}

void ClientInterface::poll()
{
	try
	{
		socket.get_io_service().poll();
		//socket.get_io_service().reset();

		lowFreqService.poll_one();
	}
	catch ( std::exception e )
	{
	}
}

bool ClientInterface::connect(const char * address)
{
	std::cout << "client connecting... " << address << std::endl;


	tcp::resolver resolver(socket.get_io_service());
    tcp::resolver::query query(address, "1012");
    tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
    tcp::resolver::iterator end;

	unsigned int tries = 0;
	unsigned int max_tries = 2;

    boost::system::error_code error = boost::asio::error::host_not_found;
    while ((error && endpoint_iterator != end) && tries < max_tries)
    {
		socket.close();
		socket.connect(*endpoint_iterator++, error);
    }
    if (error || tries >= max_tries)
	{
		std::cout << "connect failed" << std::endl;
		return false;
	}
	else
	{
		boost::asio::ip::tcp::no_delay option(true);
		socket.set_option(option);

		socket.async_receive(boost::asio::buffer(buffer), boost::bind(&ClientInterface::handleRecv, this,
			boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

		std::cout << "connect successful" << std::endl;

		//new boost::thread(boost::bind(&ClientInterface::poll, this));

		return true;
	}

	return false;
}

void ClientInterface::disconnect()
{
	socket.close();

	restart();
}

bool ClientInterface::isConnected() const
{
	return socket.is_open();
}

std::string ClientInterface::getRemoteIp() const
{
	return socket.remote_endpoint().address().to_string();
}

boost::asio::io_service & ClientInterface::getService()
{
	return socket.get_io_service();
}

boost::asio::io_service & ClientInterface::getLowFreqService()
{
	return lowFreqService;
}

void ClientInterface::send(const std::string & message)
{
	boost::asio::socket_base::message_flags flags = 0;
	
	/*
	socket.async_send(boost::asio::buffer(message), flags, boost::bind(&ClientInterface::handleSend, this,
		boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
	*/

	try
	{
		socket.send(boost::asio::buffer(message), flags);
		//socket.get_io_service().poll();

		bytes_out += message.size();
	}
	catch ( std::exception e )
	{
	}
}

void ClientInterface::send(const void * data, unsigned int size)
{
	boost::asio::socket_base::message_flags flags = 0;

	try
	{
		socket.send(boost::asio::buffer(data, size), flags);
		//socket.get_io_service().poll();

		bytes_out += size;
	}
	catch ( std::exception e )
	{
	}
}

void ClientInterface::send(const NetPacket & packet, unsigned int size)
{
	boost::asio::socket_base::message_flags flags = 0;

	packet.size = size - RawPacket::HeaderSize;

	try
	{
		socket.send(boost::asio::buffer(&packet, size), flags);
		//socket.get_io_service().poll();

		bytes_out += size;
	}
	catch ( std::exception e )
	{
	}
}

unsigned int ClientInterface::pollReceivedBytes()
{
	unsigned int out = bytes_in;

	bytes_in = 0;

	return out;
}

unsigned int ClientInterface::pollSentBytes()
{
	unsigned int out = bytes_out;

	bytes_out = 0;

	return out;
}

void ClientInterface::handleSend(const boost::system::error_code& error, std::size_t bytes)
{
	// not used
	//onSend(buffer.data(), bytes); 
}

void ClientInterface::handleRecv(const boost::system::error_code& error, std::size_t bytes)
{
	onReceive(buffer.data(), bytes);

	
	if (error)
	{
		std::cout << error.message() << std::endl;
		restart();

		socket.close();
		onDisconnected();
	}
	else
	{
		// insert into packer
		packup(buffer.data(), bytes);

		socket.async_receive(boost::asio::buffer(buffer), boost::bind(&ClientInterface::handleRecv, this,
			boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
	}

	bytes_in += bytes;
}