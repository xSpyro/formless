
#ifndef KILL_PLAYER_PACKET_H
#define KILL_PLAYER_PACKET_H

#include "RawPacket.h"

struct KillPlayerPacket : public NetPacket
{
	unsigned int uid;
	unsigned int killerId;
	bool count;

	KillPlayerPacket() : NetPacket(KILL_PLAYER_PACKET, 0)
	{
	}
};

#endif