#include "Achievement.h"

Achievement::Achievement()
	: type( Achievement::TYPE_UNKNOWN ), name( "Unknown Achievement" ), description( "Whatever you've done, grats!" )
{
}

Achievement::Achievement( std::string name, std::string description )
	: type( Achievement::TYPE_UNKNOWN ), name( name ), description( description )
{
}

Achievement::~Achievement()
{
}

bool Achievement::validate( const Achievement::InData & data )
{
	std::cout << "Cannot validate unknown achievement." << std::endl;

	return false;
}
