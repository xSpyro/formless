#ifndef PROFILE_LIST__H
#define PROFILE_LIST__H

#include <vector>
#include "ProfileItem.h"

class ProfileList
{
	typedef std::vector < ProfileItem > Items;

public:
	void registerItem( const ProfileItem & item );
	void clear();

	char * getDataPointer();
	unsigned int getDataSize();

	unsigned int getNumberOfEntries() const;

private:
	Items items;
};

#endif