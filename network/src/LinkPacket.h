#ifndef LINK_PACKET_H
#define LINK_PACKET_H

#include "RawPacket.h"

struct LinkPacket : public NetPacket
{
	char link[32];
	unsigned int owner;
	unsigned int target;
	bool status;

	LinkPacket() : NetPacket(LINK_PACKET, 0)
	{
	}
};

#endif