#ifndef ACHIEVEMENT_RESPONSE_PACKET__H
#define ACHIEVEMENT_RESPONSE_PACKET__H

#include "RawPacket.h"

struct AchievementResponsePacket : public NetPacket
{
	unsigned int user_id;
	unsigned int type;
	bool unlocked;
	char name[64];

	AchievementResponsePacket() : NetPacket(ACHIEVEMENT_RESPONSE_PACKET, 0), user_id(0), type(0)
	{
	}

	void setName( const std::string & str )
	{
		strcpy_s( name, 64, str.c_str() );
	}
};

#endif
