
#ifndef COMMON_PACKETS_H
#define COMMON_PACKETS_H

#define PING_PACKET					10
#define PONG_PACKET					11

#define MASTER_AUTH_PACKET			20
#define AUTH_RESULT_PACKET			21

#define CREATE_USER_PACKET			30
#define DROP_USER_PACKET			31
#define QUERY_USERNAME_PACKET		32
#define USER_PACKET					33
#define USER_REQUEST_PACKET			34
#define ACCOUNT_AGE_PACKET			35

#define INVENTORY_REQUEST_PACKET	40
#define ITEM_LIST_PACKET			41
#define PROFILE_LOAD_PACKET			42
#define PROFILE_LIST_PACKET			43

#define QUERY_SERVER_LIST_PACKET	50
#define SERVER_LIST_PACKET			51

#define SERVER_UPDATE_PACKET		60

#define REGISTER_SERVER_PACKET		80
#define DROP_SERVER_PACKET			81
#define SERVER_ADDED_PACKET			82

#define CONNECT_PACKET				100
#define DISCONNECT_PACKET			110

#define ADD_ITEM_PACKET				200
#define MOVE_ITEM_PACKET			210

#define PLAYTIME_UPDATE_PACKET		300
#define PLAYTIME_PACKET				310

#define ACHIEVEMENT_RESPONSE_PACKET			400
#define ACHIEVEMENT_REQUEST_PACKET			401
#define ACHIEVEMENT_LIST_PACKET				410
#define ACHIEVEMENT_LIST_REQUEST_PACKET		411

#define PLAYER_LIST_REQUEST_PACKET	500
#define PLAYER_LIST_PACKET			510

#define SERVER_JOIN_PACKET			600
#define SERVER_LEAVE_PACKET			610

#define READY_PACKET				1000
#define GAME_START_PACKET			1001
#define GAME_END_PACKET				1002
#define REQUEST_READY_PACKET		1003
#define COMPANION_PACKET			1004
#define TIMER_PACKET				1010
#define RESPAWN_TIMER_PACKET		1011
#define SKILL_EFFECT_PACKET			1020
#define ENTER_LOBBY_PACKET			1100

#define CREATE_PACKET				2000
#define DESTROY_PACKET				2010
#define KILL_PLAYER_PACKET			2020
#define RESPAWN_PLAYER_PACKET		2030
#define LINK_PACKET					2040
#define UPDATE_PACKET				2100
#define REQUEST_PACKET				2200

#define TEXT_PACKET					3000

#define TEAM_SELECT_PACKET			4000

#define LEVEL_DATA_REQUEST_PACKET	5000
#define LEVEL_DATA_RESPONSE_PACKET	5010

#define CLIENT_STATE_PACKET			10000
#define AI_COMMAND_PACKET			10001
#define SERVER_STATE_PACKET			10100

#define CHANGE_MODULE_PACKET		20000
#define CORE_ATTR_PACKET			20010

#define FILE_TRANSFER_PACKET		30000

#include "PingPacket.h"
#include "PongPacket.h"

#include "MasterAuthPacket.h"
#include "AuthResultPacket.h"

#include "CreateUserPacket.h"
#include "DropUserPacket.h"
#include "QueryUsernamePacket.h"
#include "UserPacket.h"
#include "UserRequestPacket.h"
#include "AccountAgePacket.h"

#include "InventoryRequestPacket.h"
#include "ItemListPacket.h"
#include "ProfileLoadPacket.h"
#include "ProfileListPacket.h"

#include "QueryServerListPacket.h"
#include "ServerListPacket.h"

#include "ServerUpdatePacket.h"

#include "RegisterServerPacket.h"
#include "DropServerPacket.h"
#include "ServerAddedPacket.h"

#include "ConnectPacket.h"
#include "DisconnectPacket.h"

#include "AddItemPacket.h"
#include "MoveItemPacket.h"

#include "AchievementResponsePacket.h"
#include "AchievementRequestPacket.h"
#include "AchievementListPacket.h"
#include "AchievementListRequestPacket.h"

#include "PlayerListRequestPacket.h"
#include "PlayerListPacket.h"

#include "ServerJoinPacket.h"
#include "ServerLeavePacket.h"

#include "ReadyPacket.h"
#include "GameStartPacket.h"
#include "GameEndPacket.h"
#include "RequestReadyPacket.h"
#include "CompanionPacket.h"
#include "TimerPacket.h"
#include "RespawnTimerPacket.h"
#include "SkillEffectPacket.h"
#include "EnterLobbyPacket.h"

#include "CreatePacket.h"
#include "DestroyPacket.h"
#include "KillPlayerPacket.h"
#include "RespawnPlayerPacket.h"
#include "LinkPacket.h"
#include "UpdatePacket.h"
#include "RequestPacket.h"

#include "TextPacket.h"

#include "TeamSelectPacket.h"

#include "LevelDataRequestPacket.h"
#include "LevelDataResponsePacket.h"

#include "ClientStatePacket.h"
#include "AICommandPacket.h"
#include "ServerStatePacket.h"

#include "ChangeModulePacket.h"
#include "CoreAttrPacket.h"

#include "FileTransferPacket.h"

#endif