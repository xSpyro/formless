#ifndef PLAYER_INFO__H
#define PLAYER_INFO__H

#include <string>

class PlayerInfo
{
public:
	PlayerInfo();
	PlayerInfo( unsigned int uid, unsigned int team, const std::string& name );

	unsigned int getUserID();
	const char* getName() const;

	unsigned int nid;
	unsigned int uid;
	unsigned int team;
	char name[64];
	__int64 time;

	unsigned int timeout;
};

#endif
