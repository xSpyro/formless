
#ifndef PACKER_H
#define PACKER_H

#include "RawPacket.h"

#include <boost/array.hpp>

class Packer
{
public:

	Packer();

	void restart();

	void packup(const char * data, unsigned int bytes);

	virtual void onPacket(RawPacket & packet) { }

private:

	void packupProcess(const char * data, unsigned int bytes);

private:

	RawPacket packet;

	boost::array<char, 20000> buffer;
	unsigned int token;
};

#endif