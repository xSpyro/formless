
#ifndef RAW_PACKET_H
#define RAW_PACKET_H

#include <string>

struct RawPacket
{
	static const unsigned int HeaderSize = 16;
	static const unsigned int HeaderMagic = 4576654;

	struct Header
	{
		unsigned int id;
		mutable unsigned int size;
		const unsigned int magic;
		unsigned int flags;

		Header()
			: id(0), size(0), magic(HeaderMagic), flags(0)
		{
		}

		Header(unsigned int id, unsigned int flags)
			: id(id), size(0), magic(HeaderMagic), flags(flags)
		{
		}

		Header(const Header & header)
			: id(header.id), size(header.size), magic(HeaderMagic), flags(header.flags)
		{
		}

		Header & operator = (const Header & header)
		{
			id = header.id;
			size = header.size;
			flags = header.flags;

			return *this;
		}
	};

	Header header;
	void * data;

	RawPacket()
		: data(NULL)
	{
	}
};

typedef RawPacket::Header NetPacket;

#endif