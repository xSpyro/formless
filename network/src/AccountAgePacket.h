#ifndef ACCOUNT_AGE_PACKET_H
#define ACCOUNT_AGE_PACKET_H

#include "RawPacket.h"

struct AccountPacket : public NetPacket
{
	unsigned int	userID;
	unsigned int	age;
	char			itemname[32];

	unsigned int	prevItemAt;
	unsigned int	nextItemAt;
	
	unsigned int	nrUnlockedCores;
	unsigned int	nrUnlockedModules;
	unsigned int	nrValidCores;
	unsigned int	nrValidModules;
	bool			allUnlocked;

	char			achievementName[64];
	char			achievementDesc[256];

	AccountPacket() : NetPacket(ACCOUNT_AGE_PACKET, 0)
	{
	}
};

#endif
