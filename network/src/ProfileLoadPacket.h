#ifndef PROFILE_LOAD_PACKET__H
#define PROFILE_LOAD_PACKET__H

#include "RawPacket.h"

struct ProfileLoadPacket : public NetPacket
{
	unsigned int uid;
	unsigned int pid;

	ProfileLoadPacket() : NetPacket(PROFILE_LOAD_PACKET, 0)
	{
	}
};

#endif
