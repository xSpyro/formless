#include "FirstBloodAchievement.h"

FirstBloodAchievement::FirstBloodAchievement()
	: Achievement()
{
	type			= Achievement::TYPE_FIRST_BLOOD;
	name			= "First blood";
	description		= "Got the first kill in a match";
}

FirstBloodAchievement::FirstBloodAchievement( std::string name, std::string description )
	: Achievement( name, description )
{
	type			= Achievement::TYPE_FIRST_BLOOD;
}

bool FirstBloodAchievement::validate( const Achievement::InData & data )
{
	bool result = false;

	if( data.type == type && data.rambo )
	{
		result = true;
	}

	return result;
}
