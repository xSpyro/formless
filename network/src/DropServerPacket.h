
#ifndef DROP_SERVER_PACKET_H
#define DROP_SERVER_PACKET_H

#include "RawPacket.h"

struct DropServerPacket : public NetPacket
{
	DropServerPacket() : NetPacket(DROP_SERVER_PACKET, 0)
	{
	}
};

#endif