#include "ItemInfo.h"

ItemInfo::ItemInfo()
{
	this->uid		= 0;
	this->type		= 0;
	this->slotX		= 0;
	this->slotY		= 0;
	this->timeout	= 1000;

	strcpy_s(this->name, 64, "");
}

ItemInfo::ItemInfo( unsigned int uid, const std::string & name )
{
	this->uid		= uid;
	this->userid	= 0;
	this->type		= 0;
	this->slotX		= 0;
	this->slotY		= 0;
	this->timeout	= 1000;

	strcpy_s(this->name, 64, name.c_str());
}

//ItemInfo::ItemInfo( unsigned int uid, unsigned int type, unsigned int slot, const std::string & name )
//{
//	this->uid		= uid;
//	this->userid	= 0;
//	this->type		= type;
//	this->slot		= slot;
//	this->timeout	= 1000;
//
//	strcpy_s(this->name, 64, name.c_str());
//}

ItemInfo::ItemInfo( unsigned int uid, unsigned int userid, unsigned int type, unsigned int slotX, unsigned int slotY, const std::string & name )
{
	this->uid		= uid;
	this->userid	= userid;
	this->type		= type;
	this->slotX		= slotX;
	this->slotY		= slotY;
	this->timeout	= 1000;

	strcpy_s(this->name, 64, name.c_str());
}

const char * ItemInfo::getItemName() const
{
	return name;
}
