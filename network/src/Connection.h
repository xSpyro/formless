
#ifndef CONNECTION_H
#define CONNECTION_H

#include <iostream>
#include <boost/asio.hpp>


#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread.hpp>

#include "RawPacket.h"
#include "Packer.h"

class ServerInterface;

using boost::asio::ip::tcp;

class Connection : public Packer, public boost::enable_shared_from_this<Connection>
{
public:

	typedef boost::shared_ptr<Connection> pointer;

	~Connection();

	static pointer create(boost::asio::io_service& io_service);

	tcp::socket& getSocket();
	boost::asio::ip::address& getAddress();
	unsigned int getId() const;

	void start(ServerInterface * server, unsigned int id);

	void send(const std::string & message);
	void send(const void * data, unsigned int bytes);
	void sendCombined(const void * header, unsigned int header_size, const void * data, unsigned int size);

	void send(const NetPacket & packet, unsigned int size);

	void poll();

private:

	Connection(boost::asio::io_service& io_service);

	void onPacket(RawPacket & packet);

	void handleRecv(const boost::system::error_code& error, size_t n);
	void handleSend(const boost::system::error_code& error, size_t n);

	ServerInterface * server;

	boost::array<char, 8192> buffer;
	tcp::socket socket;
	std::string message;

	// ids
	unsigned int id;
	boost::asio::ip::address remote_address;
};

#endif