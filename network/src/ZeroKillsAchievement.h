#ifndef ZERO_KILLS_ACHIEVEMENT__H
#define ZERO_KILLS_ACHIEVEMENT__H

#include "Achievement.h"

class ZeroKillsAchievement : public Achievement
{
public:
	ZeroKillsAchievement();
	ZeroKillsAchievement( std::string name, std::string description );

	bool validate( const Achievement::InData & data );
};

#endif
