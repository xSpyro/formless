#include "FiveKillstreakAchievement.h"

FiveKillstreakAchievement::FiveKillstreakAchievement()
	: Achievement()
{
	type			= Achievement::TYPE_5_KILLSTREAK;
	name			= "Good for you";
	description		= "5 kill streak";
}

FiveKillstreakAchievement::FiveKillstreakAchievement( std::string name, std::string description )
	: Achievement( name, description )
{
	type			= Achievement::TYPE_5_KILLSTREAK;
}

bool FiveKillstreakAchievement::validate( const Achievement::InData & data )
{
	bool result = false;

	if( data.type == type && data.killstreak >= 5 )
	{
		result = true;
	}

	return result;
}
