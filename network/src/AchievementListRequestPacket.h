#ifndef ACHIEVEMENT_LIST_REQUEST_PACKET__H
#define ACHIEVEMENT_LIST_REQUEST_PACKET__H

#include "RawPacket.h"

struct AchievementListRequestPacket : public NetPacket
{
	unsigned int user_id;

	AchievementListRequestPacket() : NetPacket(ACHIEVEMENT_LIST_REQUEST_PACKET, 0)
	{
	}
};

#endif
