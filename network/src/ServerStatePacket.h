
#ifndef SERVER_STATE_PACKET_H
#define SERVER_STATE_PACKET_H

#include "RawPacket.h"

struct ServerStatePacket : public NetPacket
{

	unsigned int connections;
	unsigned int net_id[16];

	ServerStatePacket() : NetPacket(SERVER_STATE_PACKET, 0)
	{
		memset(net_id, 0, 16 * sizeof(unsigned int));
	}
};



#endif