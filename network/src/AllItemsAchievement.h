#ifndef ALL_ITEMS_ACHIEVEMENT__H
#define ALL_ITEMS_ACHIEVEMENT__H

#include "Achievement.h"

class AllItemsAchievement : public Achievement
{
public:
	AllItemsAchievement();
	AllItemsAchievement( std::string name, std::string description );

	bool validate( const Achievement::InData & data );
};

#endif
