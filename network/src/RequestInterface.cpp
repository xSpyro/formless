
#include "RequestInterface.h"

RequestInterface::RequestAsyncData RequestInterface::rsd;

RequestInterface::RequestInterface(boost::asio::io_service& service, const std::string & host)
	: socket(service), host(host)
{
}

RequestInterface::~RequestInterface()
{
}

bool RequestInterface::request(const NetPacket & packet, unsigned int size, boost::array<char, BUFFER_SIZE> & out)
{
	// this function will do three major things
	// 1: setup a connection
	// 2: send packet and wait for response
	// 3: close connection

	if (!connect(host))
		return false;

	boost::asio::socket_base::message_flags flags = 0;
	packet.size = size - RawPacket::HeaderSize;

	try
	{
		socket.send(boost::asio::buffer(&packet, size), flags);
		socket.get_io_service().poll();

		boost::system::error_code ec;

		restart();

		result = &out;

		stop = false;
		while ( !stop )
		{ 
			size_t n = socket.receive(boost::asio::buffer(buffer), flags, ec);
			packup(buffer.data(), n);
			socket.get_io_service().poll();
		}

		disconnect();

	}
	catch ( std::exception e )
	{
	}

	return true;
}

bool RequestInterface::send(const NetPacket & packet, unsigned int size)
{
	// this function will do three major things
	// 1: setup a connection
	// 2: send packet
	// 3: close connection

	if (!connect(host))
		return false;

	boost::asio::socket_base::message_flags flags = 0;
	packet.size = size - RawPacket::HeaderSize;

	try
	{
		socket.send(boost::asio::buffer(&packet, size), flags);
		socket.get_io_service().poll();

		disconnect();
	}
	catch ( std::exception e )
	{
	}

	return true;
}

bool RequestInterface::send(const NetPacket & packet, const void * data, unsigned int bytes)
{
	// this function will do three major things
	// 1: setup a connection
	// 2: send packet
	// 3: close connection

	if (!connect(host))
		return false;

	boost::asio::socket_base::message_flags flags = 0;

	unsigned int headersize = packet.size - bytes + RawPacket::HeaderSize;

	try
	{
		size_t sent;
		sent = socket.send(boost::asio::buffer(&packet, headersize), flags);
		//std::cout << "Sent a " << sent << " sized header. " << std::endl;
		sent = socket.send(boost::asio::buffer(data, bytes), flags);
		//std::cout << "Sent " << sent << " bytes of data" << std::endl;
		socket.get_io_service().poll();

		disconnect();

	}
	catch ( std::exception e )
	{
	}


	return true;
}

void RequestInterface::sendAsync(const NetPacket & packet, unsigned int size)
{
	try
	{


		// this function will do three major things
		// 1: setup a connection
		// 2: send packet
		// 3: close connection

		// copy packet data
		std::memcpy(buffer.data(), &packet, size);
		this->size = size;

		reinterpret_cast < NetPacket* > (buffer.data())->size = size - RawPacket::HeaderSize;

		connectAsync(host);

		socket.get_io_service().run_one();


	}
	catch ( std::exception e )
	{
	}
}

void RequestInterface::requestAsync(const NetPacket & packet, unsigned int size)
{

	try
	{

		// copy packet data
		std::memcpy(buffer.data(), &packet, size);
		this->size = size;

		reinterpret_cast < NetPacket* > (buffer.data())->size = size - RawPacket::HeaderSize;

		// connect
		tcp::resolver resolver(socket.get_io_service());
		tcp::resolver::query query(host, "1022");
		tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
		tcp::resolver::iterator end;

		socket.async_connect(*endpoint_iterator, boost::bind(&RequestInterface::handleConnectAsync, shared_from_this(), 
			boost::asio::placeholders::error));

	}
	catch ( std::exception e )
	{
	}
}

void RequestInterface::setRequestAsyncData(boost::asio::io_service & service, ResultFunc callback)
{
	rsd.callback = callback;
	rsd.service = &service;
}

RequestInterface::pointer RequestInterface::create(boost::asio::io_service& service, const std::string & host)
{
	return pointer(new RequestInterface(service, host));
}

bool RequestInterface::connect(const std::string & host)
{
	tcp::resolver resolver(socket.get_io_service());
    tcp::resolver::query query(host, "1022");
    tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
    tcp::resolver::iterator end;

	unsigned int tries = 0;
	unsigned int max_tries = 2;

    boost::system::error_code error = boost::asio::error::host_not_found;
    while ((error && endpoint_iterator != end) && tries < max_tries)
    {
		socket.close();
		socket.connect(*endpoint_iterator++, error);
    }
    if (error || tries >= max_tries)
	{
		return false;
	}
	else
	{
		return true;
	}

	return false;
}

void RequestInterface::connectAsync(const std::string & host)
{

	try
	{

		tcp::resolver resolver(socket.get_io_service());
		tcp::resolver::query query(host, "1022");
		tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
		tcp::resolver::iterator end;

		socket.async_connect(*endpoint_iterator, boost::bind(&RequestInterface::handleSendAsync, shared_from_this(), 
			boost::asio::placeholders::error));

		socket.get_io_service().run_one();


	}
	catch ( std::exception e )
	{
	}
}

bool RequestInterface::disconnect()
{
	socket.close();

	return true;
}

void RequestInterface::onPacket(RawPacket & packet)
{
	std::memcpy(result->data(), packet.data, RawPacket::HeaderSize + packet.header.size);

	stop = true;
}

void RequestInterface::handleSendAsync(const boost::system::error_code& error)
{
	// send

	try
	{

		if (!error)
		{
			socket.async_send(boost::asio::buffer(buffer, size), 0,  boost::bind(&RequestInterface::handleWrite, shared_from_this(), 
			boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

			socket.get_io_service().run_one();
		}
	}
	catch ( std::exception e )
	{
	}

}

void RequestInterface::handleWrite(const boost::system::error_code& error, unsigned int n)
{
	disconnect();
}

void RequestInterface::handleComplete(const boost::system::error_code& error)
{

}

void RequestInterface::handleRecvAsync(const boost::system::error_code & error, unsigned int n)
{
	try
	{

		// start packing data
		packup(buffer.data(), n);

		if (stop)
		{
			// stop and call the callback with the packet data

			RawPacket answer = *reinterpret_cast < RawPacket* > (buffer.data());
			answer.data = buffer.data();

			(*rsd.callback)(answer);
			disconnect();
		}
		else
		{
			socket.async_receive(boost::asio::buffer(buffer), 0, boost::bind(&RequestInterface::handleRecvAsync, shared_from_this(), 
				boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
		}

	
	}
	catch ( std::exception e )
	{
	}
}

void RequestInterface::handleConnectAsync(const boost::system::error_code& error)
{
	try
	{
		if (!error)
		{
			socket.async_send(boost::asio::buffer(buffer, size), 0,  boost::bind(&RequestInterface::handleSendAfterConnectAsync, shared_from_this(), 
			boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
		}
	}
	catch ( std::exception e )
	{
	}
}

void RequestInterface::handleSendAfterConnectAsync(const boost::system::error_code & error, unsigned int n)
{
	try
	{

		// start reading
		socket.async_receive(boost::asio::buffer(buffer), 0, boost::bind(&RequestInterface::handleRecvAsync, shared_from_this(), 
			boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));


	}
	catch ( std::exception e )
	{
	}
}