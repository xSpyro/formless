#ifndef PLAYTIME_PACKET__H
#define PLAYTIME_PACKET__H

#include "RawPacket.h"

struct PlaytimePacket : public NetPacket
{
	unsigned int uid;
	unsigned int time;

	PlaytimePacket() : NetPacket(PLAYTIME_PACKET, 0), uid(0), time(0)
	{
	}
};

#endif
