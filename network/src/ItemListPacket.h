#ifndef ITEM_LIST_PACKET__H
#define ITEM_LIST_PACKET__H

#include "RawPacket.h"

struct ItemListPacket : public NetPacket
{
	unsigned int results;

	ItemListPacket() : NetPacket(ITEM_LIST_PACKET, 0)
	{
	}
};

#endif
