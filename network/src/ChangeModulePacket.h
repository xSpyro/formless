
#ifndef CHANGE_MODULE_PACKET_H
#define CHANGE_MODULE_PACKET_H

#include "RawPacket.h"

struct ChangeModulePacket : public NetPacket
{
	unsigned int uid;
	bool companion;

	unsigned int moduleid;
	unsigned int slot;
	char module[32];

	bool add;

	ChangeModulePacket() : NetPacket(CHANGE_MODULE_PACKET, 0)
	{
	}
};

#endif