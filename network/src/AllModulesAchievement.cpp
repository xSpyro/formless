#include "AllModulesAchievement.h"

AllModulesAchievement::AllModulesAchievement()
	: Achievement()
{
	type			= Achievement::TYPE_ALL_MODULES;
	name			= "Modulemaster";
	description		= "Unlocked all modules";
}

AllModulesAchievement::AllModulesAchievement( std::string name, std::string description )
	: Achievement( name, description )
{
	type			= Achievement::TYPE_ALL_MODULES;
}

bool AllModulesAchievement::validate( const Achievement::InData & data )
{
	bool result = false;

	if( data.type == type && data.modules >= data.totalModules )
	{
		result = true;
	}

	return result;
}
