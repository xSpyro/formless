#ifndef USER_PACKET__H
#define USER_PACKET__H

#include "RawPacket.h"

struct UserPacket : public NetPacket
{
	unsigned int nid;
	unsigned int uid;
	unsigned int team;
	char name[32];

	UserPacket() : NetPacket(USER_PACKET, 0)
	{
	}

	void setName( const std::string & str )
	{
		strcpy_s( name, 32, str.c_str() );
	}
};

#endif
