
#ifndef CLIENT_STATE_PACKET_H
#define CLIENT_STATE_PACKET_H

#include "RawPacket.h"

struct ClientStatePacket : public NetPacket
{
	unsigned int input;

	float lx;
	float ly;
	float lz;

	ClientStatePacket() : NetPacket(CLIENT_STATE_PACKET, 0)
	{
	}
};

#endif