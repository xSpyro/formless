
#ifndef SERVER_ADDED_PACKET_H
#define SERVER_ADDED_PACKET_H

#include "RawPacket.h"

struct ServerAddedPacket : public NetPacket
{
	unsigned int error;

	ServerAddedPacket() : NetPacket(SERVER_ADDED_PACKET, 0)
	{
	}
};

#endif