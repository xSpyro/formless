#ifndef QUERY_USERNAME_PACKET__H
#define QUERY_USERNAME_PACKET__H

#include "RawPacket.h"

struct QueryUsernamePacket : public NetPacket
{
	int uid;
	unsigned int nid;

	QueryUsernamePacket() : NetPacket(QUERY_USERNAME_PACKET, 0)
	{
	}
};

#endif
