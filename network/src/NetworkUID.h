
#ifndef NETWORK_UID_H
#define NETWORK_UID_H

class NetworkUID
{
public:

	NetworkUID();

	static NetworkUID * getInstance();

	unsigned int generate();

private:

	unsigned int uid;
};

#define CreateUID() (NetworkUID::getInstance()->generate());

#endif