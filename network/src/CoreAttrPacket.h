
#ifndef CORE_ATTR_PACKET_H
#define CORE_ATTR_PACKET_H

#include "RawPacket.h"

struct CoreAttrPacket : public NetPacket
{
	unsigned int uid;
	bool companion;

	unsigned int nrSlots;
	unsigned int nrSkills;
	float life;
	float mana;
	float speed;
	float lifeRegen;
	float manaRegen;
	char name[32];

	CoreAttrPacket() : NetPacket(CORE_ATTR_PACKET, 0)
	{
	}
};

#endif