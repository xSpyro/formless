
#ifndef CREATE_PACKET_H
#define CREATE_PACKET_H

#include "RawPacket.h"

#include <windows.h>

struct CreatePacket : public NetPacket
{
	char object[32];
	unsigned int uid;
	unsigned int creator;

	float x;
	float y;
	float z;

	int	team;

	CreatePacket() : NetPacket(CREATE_PACKET, 0)
	{
	}

	void setObjectName(const std::string & str)
	{
		ZeroMemory(object, sizeof(object));
		strcpy_s(object, 32, str.c_str());
	}
};

#endif