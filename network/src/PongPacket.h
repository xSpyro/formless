#ifndef PONG_PACKET__H
#define PONG_PACKET__H

#include "RawPacket.h"

struct PongPacket : public NetPacket
{
	PongPacket() : NetPacket(PONG_PACKET, 0)
	{
	}
};

#endif
