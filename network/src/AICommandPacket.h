
#ifndef AI_COMMAND_PACKET_H
#define AI_COMMAND_PACKET_H

#include "RawPacket.h"

struct AICommandPacket : public NetPacket
{
	unsigned int uid;

	char command[32];
	int commandState;

	float targetX;
	float targetY;
	float targetZ;

	AICommandPacket() : NetPacket(AI_COMMAND_PACKET, 0)
	{
	}
};

#endif