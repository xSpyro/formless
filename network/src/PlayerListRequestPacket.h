#ifndef PLAYER_LIST_REQUEST_PACKET__H
#define PLAYER_LIST_REQUEST_PACKET__H

#include "RawPacket.h"

struct PlayerListRequestPacket : public NetPacket
{
	PlayerListRequestPacket() : NetPacket(PLAYER_LIST_REQUEST_PACKET, 0)
	{
	}
};

#endif
