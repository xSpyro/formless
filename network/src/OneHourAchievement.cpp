#include "OneHourAchievement.h"

OneHourAchievement::OneHourAchievement()
	: Achievement()
{
	type			= Achievement::TYPE_1_HOUR;
	name			= "Noob";
	description		= "1 hour playtime";
}

OneHourAchievement::OneHourAchievement( std::string name, std::string description )
	: Achievement( name, description )
{
	type			= Achievement::TYPE_1_HOUR;
}

bool OneHourAchievement::validate( const Achievement::InData & data )
{
	bool result = false;

	if( data.type == type && data.playtime >= 3600 )
	{
		result = true;
	}

	return result;
}
