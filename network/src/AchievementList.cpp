#include "AchievementList.h"

void AchievementList::registerItem( const AchievementInfo & achievement )
{
	achievements.push_back( achievement );
}

void AchievementList::clear()
{
	achievements.clear();
}

char * AchievementList::getDataPointer()
{
	if (achievements.empty())
		return NULL;

	return reinterpret_cast < char* > (&achievements.front());
}

unsigned int AchievementList::getDataSize()
{
	return achievements.size() * sizeof(AchievementInfo);
}

unsigned int AchievementList::getNumberOfEntries() const
{
	return achievements.size();
}
