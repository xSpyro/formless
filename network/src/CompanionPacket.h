
#ifndef COMPANION_PACKET_H
#define COMPANION_PACKET_H

#include "RawPacket.h"

struct CompanionPacket : public NetPacket
{
	unsigned int uid;
	bool add;
	int team;
	unsigned int masterUserID;

	CompanionPacket() : NetPacket(COMPANION_PACKET, 0)
	{
	}
};



#endif