#ifndef MOVE_ITEM_PACKET__H
#define MOVE_ITEM_PACKET__H

#include "RawPacket.h"

struct MoveItemPacket : public NetPacket
{
	unsigned int userid;
	unsigned int itemid;
	unsigned int slotX;
	unsigned int slotY;
	char holder[32];

	MoveItemPacket() : NetPacket(MOVE_ITEM_PACKET, 0)
	{
	}

	void setHolder( const std::string & str )
	{
		strcpy_s( holder, 32, str.c_str() );
	}
};

#endif
