#ifndef ONE_HOUR_ACHIEVEMENT__H
#define ONE_HOUR_ACHIEVEMENT__H

#include "Achievement.h"

class OneHourAchievement : public Achievement
{
public:
	OneHourAchievement();
	OneHourAchievement( std::string name, std::string description );

	bool validate( const Achievement::InData & data );
};

#endif
