#ifndef ADD_ITEM_PACKET__H
#define ADD_ITEM_PACKET__H

#include "RawPacket.h"

struct AddItemPacket : public NetPacket
{
	unsigned int uid;
	unsigned int type;
	char name[32];

	AddItemPacket() : NetPacket(ADD_ITEM_PACKET, 0)
	{
	}

	void setName( const std::string & str )
	{
		strcpy_s( name, 32, str.c_str() );
	}
};

#endif
