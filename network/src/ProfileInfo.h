#ifndef PROFILE_INFO__H
#define PROFILE_INFO__H

#include <string>

class ProfileInfo
{
public:

	ProfileInfo( const std::string & slot, const std::string & name, unsigned int size = 1, unsigned int uid = 0 );

	const char * getSlot() const;
	const char * getName() const;
	unsigned int getSize() const;

// public aswell

	char slot[32];
	char name[64];
	unsigned int size;
	unsigned int uid;

	unsigned int timeout;
};

#endif
