#include "AllItemsAchievement.h"

AllItemsAchievement::AllItemsAchievement()
	: Achievement()
{
	type			= Achievement::TYPE_ALL_ITEMS;
	name			= "Itemmaster";
	description		= "Unlocked all items";
}

AllItemsAchievement::AllItemsAchievement( std::string name, std::string description )
	: Achievement( name, description )
{
	type			= Achievement::TYPE_ALL_ITEMS;
}

bool AllItemsAchievement::validate( const Achievement::InData & data )
{
	bool result = false;

	if( data.type == type && data.items >= data.totalItems )
	{
		result = true;
	}

	return result;
}
