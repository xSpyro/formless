
#ifndef MASTER_AUTH_PACKET_H
#define MASTER_AUTH_PACKET_H

#include "RawPacket.h"

#include <string>

struct MasterAuthPacket : public NetPacket
{
	char login[32];
	char password[32];

	MasterAuthPacket() : NetPacket(MASTER_AUTH_PACKET, 0)
	{
	}

	void setLogin(const std::string & str)
	{
		strcpy_s(login, 32, str.c_str());
	}

	void setPassword(const std::string & str)
	{
		strcpy_s(password, 32, str.c_str());
	}
};

#endif