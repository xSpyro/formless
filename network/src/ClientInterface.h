
#ifndef CLIENT_INTERFACE_H
#define CLIENT_INTERFACE_H

#include <iostream>
#include <boost/asio.hpp>

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <boost/thread.hpp>

#include "RawPacket.h"
#include "Packer.h"

using boost::asio::ip::tcp;

class ClientInterface : public Packer
{
public:

	ClientInterface(boost::asio::io_service& service);
	~ClientInterface();

	void poll();

	bool connect(const char * address);
	void disconnect();

	bool isConnected() const;
	std::string getRemoteIp() const;
	boost::asio::io_service & getService();
	boost::asio::io_service & getLowFreqService();

	void send(const std::string & message);
	void send(const void * data, unsigned int size);
	void send(const NetPacket & packet, unsigned int size);


	unsigned int pollReceivedBytes();
	unsigned int pollSentBytes();

	template < class T > void send(const T & packet)
	{
		send(packet, sizeof(T));
	}
	

	// remember that its stream based so any number of bytes can be received
	virtual void onSend(const void * data, unsigned int size) { }
	virtual void onReceive(const void * data, unsigned int size) { }

	// will be called by packer
	virtual void onPacket(RawPacket & packet) { }

	virtual void onConnected(unsigned int id) { }

	virtual void onDisconnected() { }

private:

	void handleSend(const boost::system::error_code& error, std::size_t bytes_transferred); 

	void handleRecv(const boost::system::error_code& error, std::size_t bytes_transferred); 

	
	tcp::socket socket;

	boost::array<char, 10000> buffer;


	unsigned int bytes_in;
	unsigned int bytes_out;

	boost::asio::io_service lowFreqService;
};

#endif