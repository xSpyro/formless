#ifndef PROFILE_ITEM__H
#define PROFILE_ITEM__H

#include <string>

class ProfileItem
{
public:

	ProfileItem( const std::string & slot, unsigned int uid, unsigned int size, const std::string & name = "" );

	const char * getSlot() const;
	const char * getName() const;

// public aswell

	char slot[16];
	char name[32];
	unsigned int uid;
	unsigned int size;

	unsigned int timeout;
};

#endif
