
#ifndef QUERY_SERVER_LIST_PACKET_H
#define QUERY_SERVER_LIST_PACKET_H

#include "RawPacket.h"

struct QueryServerListPacket : public NetPacket
{
	QueryServerListPacket() : NetPacket(QUERY_SERVER_LIST_PACKET, 0)
	{
	}
};

#endif