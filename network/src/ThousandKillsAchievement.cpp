#include "ThousandKillsAchievement.h"

ThousandKillsAchievement::ThousandKillsAchievement()
	: Achievement()
{
	type			= Achievement::TYPE_1000_KILLS;
	name			= "Only a thousand?";
	description		= "1000 kills";
}

ThousandKillsAchievement::ThousandKillsAchievement( std::string name, std::string description )
	: Achievement( name, description )
{
	type			= Achievement::TYPE_1000_KILLS;
}

bool ThousandKillsAchievement::validate( const Achievement::InData & data )
{
	bool result = false;

	if( data.type == type && data.kills >= 1000 )
	{
		result = true;
	}

	return result;
}
