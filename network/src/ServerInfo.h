
#ifndef SERVER_INFO_H
#define SERVER_INFO_H

#include <string>

class ServerInfo
{

public:

	ServerInfo(const std::string & ip, const std::string & name);

	const char * getIp() const;
	const char * getName() const;

// public aswell

	char ip[32];
	char name[64];

	unsigned int timeout;
};

#endif