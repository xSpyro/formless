#ifndef ACHIEVEMENTS__H
#define ACHIEVEMENTS__H

#include "Achievement.h"

#include "OneHourAchievement.h"
#include "TenHoursAchievement.h"
#include "FirstBloodAchievement.h"
#include "ThousandKillsAchievement.h"
#include "FiveKillstreakAchievement.h"
#include "AllItemsAchievement.h"
#include "AllCoresAchievement.h"
#include "AllModulesAchievement.h"
#include "ZeroKillsAchievement.h"

#endif
