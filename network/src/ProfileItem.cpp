#include "ProfileItem.h"

ProfileItem::ProfileItem( const std::string & slot, unsigned int uid, unsigned int size, const std::string & name )
{
	strcpy_s(this->slot, 16, slot.c_str());
	this->uid = uid;
	this->size = size;
	strcpy_s(this->name, 32, name.c_str());
	timeout = 1000;
}

const char * ProfileItem::getSlot() const
{
	return slot;
}

const char * ProfileItem::getName() const
{
	return name;
}
