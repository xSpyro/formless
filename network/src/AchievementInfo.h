#ifndef ACHIEVEMENT_INFO__H
#define ACHIEVEMENT_INFO__H

class AchievementInfo
{
public:
	AchievementInfo( unsigned int type = 0, bool unlocked = false );

	const char * getName() const;
	const char * getDescription() const;

	unsigned int type;
	bool unlocked;
	char name[64];
	char description[256];
};

#endif
