#include "Utils.h"

#include <fstream>

std::string queryMasterHostAddress()
{
	std::string result;

	std::ifstream iFile;
	iFile.open("../resources/master.cfg");

	if( iFile.is_open() )
	{
		std::getline( iFile, result );
	}
	else
	{
		// Default stuff
		//result = "194.47.150.40";
		result = "192.168.1.2";

		std::ofstream oFile;
		oFile.open("../resources/master.cfg");
		oFile << result << std::endl;
		oFile.close();
	}

	iFile.close();

	return result;
}
