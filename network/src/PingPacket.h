#ifndef PING_PACKET__H
#define PING_PACKET__H

#include "RawPacket.h"

struct PingPacket : public NetPacket
{
	PingPacket() : NetPacket(PING_PACKET, 0)
	{
	}
};

#endif
