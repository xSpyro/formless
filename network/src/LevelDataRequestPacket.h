#ifndef LEVEL_DATA_REQUEST_PACKET__H
#define LEVEL_DATA_REQUEST_PACKET__H

#include "RawPacket.h"

struct LevelDataRequestPacket : public NetPacket
{
	LevelDataRequestPacket() : NetPacket(LEVEL_DATA_REQUEST_PACKET, 0)
	{
	}
};

#endif