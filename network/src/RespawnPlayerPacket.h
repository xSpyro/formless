
#ifndef RESPAWN_PLAYER_PACKET_H
#define RESPAWN_PLAYER_PACKET_H

#include "RawPacket.h"

struct RespawnPlayerPacket : public NetPacket
{
	unsigned int uid;
	float x;
	float y;
	float z;
	bool count;

	RespawnPlayerPacket() : NetPacket(RESPAWN_PLAYER_PACKET, 0)
	{
	}
};

#endif