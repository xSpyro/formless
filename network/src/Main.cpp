
#include <iostream>
#include <conio.h>

#include <boost/thread.hpp>

#include "ServerInterface.h"
#include "ClientInterface.h"

#include "../game/src/MemLeaks.h"


class MyServer : public ServerInterface
{
public:

	MyServer(boost::asio::io_service& service) : ServerInterface(service, 13)
	{

	}

	void onPacket(RawPacket & packet)
	{
		NetPacket msg;
		msg.id = 1;
		
		respond(msg);
	}

	void onConnect(unsigned int uid)
	{
		// send welcome message
		JoinMsg msg;
		msg.id = 0;
		msg.your_internal_id = 1337;

		respond(msg);
	}
};


void serverThread()
{
	boost::asio::io_service service;
	MyServer server(service);

	service.run();

	std::cout << "server end" << std::endl;
}

/*
void clientThread()
{
	boost::asio::io_service service;
	
	for (int i = 0; i < 5; ++i)
	{
		client[i] = new Client(service);
		client[i]->connect("localhost");
	}

	service.run();
}
*/


class MyClient : public ClientInterface
{

public:

	MyClient(boost::asio::io_service& service) : ClientInterface(service)
	{
	}

	void onPacket(RawPacket & packet)
	{
		switch (packet.header.id)
		{
		case 0:
			{
				JoinMsg * msg = reinterpret_cast < JoinMsg* > (packet.data);
				std::cout << "Your ID is: " << msg->your_internal_id << std::endl;
				break;
			}
		case 1:
			{
				std::cout << "ping" << std::endl;
			}
		}
	}
};


int main()
{
	boost::thread s(serverThread);

	//boost::thread c(clientThread);


	boost::asio::io_service service;
	MyClient * client = New MyClient(service);
	client->connect("localhost");


	client->send(NetPacket());

	
	while ( 1 )
	{
		/*
		while (_getch())
		{
			client->send(asd);
		}
		*/

		service.poll();
	}
	

	s.join();

	return 0;
}