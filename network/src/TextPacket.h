#ifndef TEXT_PACKET__H
#define TEXT_PACKET__H

#include "RawPacket.h"

struct TextPacket : public NetPacket
{
	//unsigned int uid;
	char text[256];
	char name[32];
	unsigned int textlen;
	unsigned int channel;
	unsigned int senderId;

	unsigned int team;

	TextPacket() : NetPacket(TEXT_PACKET, 0)
	{
	}

	void setName( const std::string & str )
	{
		strcpy_s( name, 32, str.c_str() );
	}
};

#endif
