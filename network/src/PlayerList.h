#ifndef PLAYER_LIST_H
#define PLAYER_LIST_H

#include <vector>
#include "PlayerInfo.h"

class PlayerList
{
	typedef std::vector < PlayerInfo > Players;

public:
	void registerItem( const PlayerInfo & player );
	void clear();

	char * getDataPointer();
	unsigned int getDataSize();

	unsigned int getNumberOfEntries() const;

private:
	Players players;
};

#endif