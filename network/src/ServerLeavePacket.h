#ifndef SERVER_LEAVE_PACKET__H
#define SERVER_LEAVE_PACKET__H

#include "RawPacket.h"

struct ServerLeavePacket : public NetPacket
{
	unsigned int user_id;

	ServerLeavePacket() : NetPacket(SERVER_LEAVE_PACKET, 0), user_id(0)
	{
	}
};

#endif
