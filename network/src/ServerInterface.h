
#ifndef SERVER_INTERFACE_H
#define SERVER_INTERFACE_H

#include <iostream>
#include <boost/asio.hpp>


#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread.hpp>

#include <map>

#include "Connection.h"
#include "Packer.h"

class Connection;

struct JoinMsg : public NetPacket
{
	unsigned int your_internal_id;
};

class ServerInterface
{
	friend class Connection;

	typedef std::map < unsigned int, Connection* > Connections;

public:

	ServerInterface(boost::asio::io_service& service, int port);

	void disableConnectResponse();
	void poll();

	void status();

	unsigned int getUID() const;

	unsigned int getResponseId() const;
	std::string getResponseIp() const;

	unsigned int getConnectionCount() const;
	unsigned int getConnectionID(unsigned int connection) const;

	unsigned int pollReceivedBytes();
	unsigned int pollSentBytes();

	void broadcast(const std::string & message);
	void broadcast(const void * data, unsigned int bytes);
	void broadcast(const NetPacket & packet, unsigned int size);

	template < class T > void broadcast(const T & packet)
	{
		broadcast(packet, sizeof(T));
	}

	void respond(const void * data, unsigned int bytes);
	void respond(const void * header, unsigned int header_size, const void * data, unsigned int size);
	void respond(const NetPacket & packet, unsigned int size);

	void responderDisconnect();
	void responderRunOne();
	
	template < class T > void respond(const T & packet)
	{
		respond(packet, sizeof(T));
	}

	void disconnect(unsigned int id);

	virtual void onConnect(unsigned int uid) { }
	virtual void onDisconnect(unsigned int uid) { }

	virtual void onReceive(const void * data, unsigned int bytes) { }

	virtual void onPacket(RawPacket & packet) { }

private:

	bool registerConnection(unsigned int id, Connection * connection);
	bool handleDisconnect(unsigned int id);

	void accept();

	void handleAccept(Connection::pointer new_connection, const boost::system::error_code& error);

private:

	bool do_connect_response;

	Connections connections;
	Connection * respond_to;

	tcp::acceptor acceptor;


	unsigned int bytes_in;
	unsigned int bytes_out;
};

#endif