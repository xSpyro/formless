#ifndef FIVE_KILLSTREAK_ACHIEVEMENT__H
#define FIVE_KILLSTREAK_ACHIEVEMENT__H

#include "Achievement.h"

class FiveKillstreakAchievement : public Achievement
{
public:
	FiveKillstreakAchievement();
	FiveKillstreakAchievement( std::string name, std::string description );

	bool validate( const Achievement::InData & data );
};

#endif
