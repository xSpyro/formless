
#ifndef DISCONNECT_PACKET_H
#define DISCONNECT_PACKET_H

#include "RawPacket.h"

struct DisconnectPacket : public NetPacket
{
	unsigned int client_id;



	DisconnectPacket(unsigned int uid) : NetPacket(DISCONNECT_PACKET, 0), client_id(uid)
	{
	}
};



#endif