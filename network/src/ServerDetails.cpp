#include "ServerDetails.h"

ServerDetails::ServerDetails(const std::string & playerlist)
{
	strcpy_s(this->players, 256, playerlist.c_str());
	timeout = 1000;
}

const char * ServerDetails::getPlayers() const
{
	return players;
}
