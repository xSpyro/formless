#include "ZeroKillsAchievement.h"

ZeroKillsAchievement::ZeroKillsAchievement()
	: Achievement()
{
	type			= Achievement::TYPE_TROLL;
	name			= "Trollin' or supportin'?";
	description		= "0 kills in a game";
}

ZeroKillsAchievement::ZeroKillsAchievement( std::string name, std::string description )
	: Achievement( name, description )
{
	type			= Achievement::TYPE_TROLL;
}

bool ZeroKillsAchievement::validate( const Achievement::InData & data )
{
	bool result = false;

	if( data.type == type && data.kills == 0 )
	{
		result = true;
	}

	return result;
}
