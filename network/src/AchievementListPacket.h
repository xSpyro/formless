#ifndef ACHIEVEMENT_LIST_PACKET__H
#define ACHIEVEMENT_LIST_PACKET__H

#include "RawPacket.h"

struct AchievementListPacket : public NetPacket
{
	unsigned int results;
	AchievementListPacket() : NetPacket(ACHIEVEMENT_LIST_PACKET, 0)
	{
	}
};

#endif
