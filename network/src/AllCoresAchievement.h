#ifndef ALL_CORES_ACHIEVEMENT__H
#define ALL_CORES_ACHIEVEMENT__H

#include "Achievement.h"

class AllCoresAchievement : public Achievement
{
public:
	AllCoresAchievement();
	AllCoresAchievement( std::string name, std::string description );

	bool validate( const Achievement::InData & data );
};

#endif
