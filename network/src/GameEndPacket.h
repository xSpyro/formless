
#ifndef GAME_END_PACKET_H
#define GAME_END_PACKET_H

#include "RawPacket.h"

struct GameEndPacket : public NetPacket
{
	unsigned int user_id;
	unsigned int kills;
	bool full_time;

	GameEndPacket() : NetPacket(GAME_END_PACKET, 0), user_id(0), kills(0), full_time(false)
	{
	}
};



#endif