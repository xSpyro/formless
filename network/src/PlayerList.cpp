#include "PlayerList.h"

void PlayerList::registerItem( const PlayerInfo & player )
{
	players.push_back( player );
}

void PlayerList::clear()
{
	players.clear();
}

char * PlayerList::getDataPointer()
{
	if (players.empty())
		return NULL;

	return reinterpret_cast < char* > (&players.front());
}

unsigned int PlayerList::getDataSize()
{
	return players.size() * sizeof(PlayerInfo);
}

unsigned int PlayerList::getNumberOfEntries() const
{
	return players.size();
}
