#include "AllCoresAchievement.h"

AllCoresAchievement::AllCoresAchievement()
	: Achievement()
{
	type			= Achievement::TYPE_ALL_CORES;
	name			= "Coremaster";
	description		= "Unlocked all cores";
}

AllCoresAchievement::AllCoresAchievement( std::string name, std::string description )
	: Achievement( name, description )
{
	type			= Achievement::TYPE_ALL_CORES;
}

bool AllCoresAchievement::validate( const Achievement::InData & data )
{
	bool result = false;

	if( data.type == type && data.cores >= data.totalCores )
	{
		result = true;
	}

	return result;
}
