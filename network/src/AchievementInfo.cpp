#include "AchievementInfo.h"

AchievementInfo::AchievementInfo( unsigned int type, bool unlocked )
	: type( type ), unlocked( unlocked )
{
}

const char * AchievementInfo::getName() const
{
	return name;
}

const char * AchievementInfo::getDescription() const
{
	return description;
}
