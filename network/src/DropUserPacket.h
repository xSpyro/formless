#ifndef DROP_USER_PACKET__H
#define DROP_USER_PACKET__H

#include "RawPacket.h"

struct DropUserPacket : public NetPacket
{
	unsigned int uid;

	DropUserPacket() : NetPacket(DROP_USER_PACKET, 0)
	{
	}
};

#endif
