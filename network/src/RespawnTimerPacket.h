
#ifndef RESPAWN_TIMER_PACKET_H
#define RESPAWN_TIMER_PACKET_H

#include "RawPacket.h"

struct RespawnTimerPacket : public NetPacket
{
	float			time;
	unsigned int	timer;
	unsigned int	playerIDs[16];

	RespawnTimerPacket() : NetPacket(RESPAWN_TIMER_PACKET, 0)
	{
		for(unsigned int i = 0; i < 16; i++)
			playerIDs[i] = 0;
	}
};

#endif