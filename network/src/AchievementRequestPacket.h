#ifndef ACHIEVEMENT_REQUEST_PACKET__H
#define ACHIEVEMENT_REQUEST_PACKET__H

#include "RawPacket.h"

struct AchievementRequestPacket : public NetPacket
{
	unsigned int user_id;
	unsigned int type;
	unsigned int kills;
	unsigned int killstreak;
	bool rambo;

	AchievementRequestPacket() : NetPacket(ACHIEVEMENT_REQUEST_PACKET, 0), user_id(0), type(0), kills(0), killstreak(0), rambo(false)
	{
	}
};

#endif
