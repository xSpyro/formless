#ifndef PLAYER_LIST_PACKET__H
#define PLAYER_LIST_PACKET__H

#include "RawPacket.h"

struct PlayerListPacket : public NetPacket
{
	unsigned int results;

	PlayerListPacket() : NetPacket(PLAYER_LIST_PACKET, 0)
	{
	}
};

#endif
