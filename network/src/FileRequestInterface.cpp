
#include "FileRequestInterface.h"

#include "CommonPackets.h"

FileRequestInterface::FileRequestInterface(boost::asio::io_service& service, const std::string & host)
	: socket(service), host(host)
{

	bytesRead = 0;
	totalSize = 0;
}

FileRequestInterface::~FileRequestInterface()
{
}

bool FileRequestInterface::request(const std::string & cmd)
{
	// start the connect only
	// since everything is async

	this->cmd = cmd;

	return connect(host);
}

bool FileRequestInterface::connect(const std::string & host)
{
	tcp::resolver resolver(socket.get_io_service());
    tcp::resolver::query query(host, "1032");
    tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
    tcp::resolver::iterator end;

	socket.async_connect(*endpoint_iterator, boost::bind(&FileRequestInterface::handleConnect, this, 
		boost::asio::placeholders::error));

	socket.get_io_service().run_one();

	return true;
}

bool FileRequestInterface::disconnect()
{
	socket.close();

	return true;
}

void FileRequestInterface::handleConnect(const boost::system::error_code& error)
{
	// send a message with the cmd as a endline terminated message

	//unsigned int sent = socket.send(cmd + "\n", 0);

	//std::cout << "Connected to file transfer" << std::endl;
	onStart();

	FileTransferPacket ft;
	socket.send(boost::asio::buffer(&ft, sizeof(ft)), 0);

	//socket.get_io_service().run_one();

	socket.async_receive(boost::asio::buffer(buffer), boost::bind(&FileRequestInterface::handleRecv, this,
		boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

	//socket.get_io_service().run_one();


}

void FileRequestInterface::handleRecv(const boost::system::error_code& error, unsigned int n)
{
	// the first recv will include 4 bytes of size

	if (file.empty())
	{
		if (n < 4)
		{
			// very bad
			std::cout << "VERY BAD : network" << std::endl;
		}
		else
		{
			// extract size
			totalSize = *reinterpret_cast < unsigned int* > (buffer.data());
			//std::cout << "size of incoming transfer " << totalSize << std::endl;
			//totalSize = 1000000;

			

			// reserve
			file.assign(totalSize, 0);

			// extract rest of data
			memcpy(&file.at(bytesRead), buffer.data() + 4, n - 4);

			bytesRead += n - 4;
		}
	}
	else
	{
		memcpy(&file.at(bytesRead), buffer.data(), n);

		bytesRead += n;
	}

	//std::cout << "Transferred " << bytesRead << " bytes" << std::endl;
	onRate(bytesRead, totalSize);

	if (bytesRead >= totalSize)
	{
		// disconnect and return

		//std::cout << "Transfer done " << bytesRead << " bytes" << std::endl;

		disconnect();
		onDone(file.data(), totalSize);
	}
	else
	{

		socket.async_receive(boost::asio::buffer(buffer), boost::bind(&FileRequestInterface::handleRecv, this,
			boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));

		//socket.get_io_service().run_one();
	}
}