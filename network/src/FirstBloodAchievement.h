#ifndef FIRST_BLOOD_ACHIEVEMENT__H
#define FIRST_BLOOD_ACHIEVEMENT__H

#include "Achievement.h"

class FirstBloodAchievement : public Achievement
{
public:
	FirstBloodAchievement();
	FirstBloodAchievement( std::string name, std::string description );

	bool validate( const Achievement::InData & data );
};

#endif
