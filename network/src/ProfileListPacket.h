#ifndef PROFILE_LIST_PACKET__H
#define PROFILE_LIST_PACKET__H

#include "RawPacket.h"

struct ProfileListPacket : public NetPacket
{
	unsigned int results;
	unsigned int uid;
	unsigned int pid;

	ProfileListPacket() : NetPacket(PROFILE_LIST_PACKET, 0)
	{
	}
};

#endif
