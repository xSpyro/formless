#ifndef LEVEL_DATA_RESPONSE_PACKET__H
#define LEVEL_DATA_RESPONSE_PACKET__H

#include "RawPacket.h"

struct LevelDataResponsePacket : public NetPacket
{
	unsigned int level_size;
	char level_name[128];

	LevelDataResponsePacket() : NetPacket(LEVEL_DATA_RESPONSE_PACKET, 0), level_size(0)
	{
	}

	void setName( const std::string& str )
	{
		strcpy_s( level_name, 128, str.c_str() );
	}
};

#endif