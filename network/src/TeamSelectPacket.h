#ifndef TEAM_SELECT_PACKET__H
#define TEAM_SELECT_PACKET__H

#include "RawPacket.h"

struct TeamSelectPacket : public NetPacket
{
	int team;
	unsigned int errorCode;

	TeamSelectPacket() : NetPacket(TEAM_SELECT_PACKET, 0), errorCode(0)
	{
	}
};

#endif
