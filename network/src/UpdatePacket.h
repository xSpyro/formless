
#ifndef UPDATE_PACKET_H
#define UPDATE_PACKET_H

#include "RawPacket.h"

struct UpdatePacket : public NetPacket
{
	unsigned int uid;
	float x;
	float y;
	float z;
	float angle;
	unsigned int input;

	float vx;
	float vy;
	float vz;

	float lx;
	float ly;
	float lz;

	float life;
	float mana;

	UpdatePacket() : NetPacket(UPDATE_PACKET, 0)
	{
	}
};

#endif