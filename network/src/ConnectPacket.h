
#ifndef CONNECT_PACKET_H
#define CONNECT_PACKET_H

#include "RawPacket.h"

struct ConnectPacket : public NetPacket
{
	unsigned int client_id;



	ConnectPacket(unsigned int uid) : NetPacket(CONNECT_PACKET, 0), client_id(uid)
	{
	}
};



#endif