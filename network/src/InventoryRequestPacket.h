#ifndef INVENTORY_REQUEST_PACKET__H
#define INVENTORY_REQUEST_PACKET__H

#include "RawPacket.h"

struct InventoryRequestPacket : public NetPacket
{
	unsigned int uid;
	bool companion;

	InventoryRequestPacket() : NetPacket(INVENTORY_REQUEST_PACKET, 0)
	{
	}
};

#endif
