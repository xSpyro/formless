
#include "ServerInterface.h"

#include "NetworkUID.h"
#include "CommonPackets.h"

ServerInterface::ServerInterface(boost::asio::io_service& service, int port)
	: acceptor(service, tcp::endpoint(tcp::v4(), port))
{
	do_connect_response = true;
	accept();
}

void ServerInterface::disableConnectResponse()
{
	do_connect_response = false;
}

void ServerInterface::poll()
{
	try
	{
		acceptor.get_io_service().poll();

		//for (Connections::iterator i = connections.begin(); i != connections.end(); ++i)
		//{
		//	i->second->poll();
		//}
	}
	catch ( std::exception )
	{
	}
}

void ServerInterface::status()
{
	// print all active connections

	std::cout << "Checking server status -----" << std::endl;

	for (Connections::iterator i = connections.begin(); i != connections.end(); ++i)
	{
		std::string addr = i->second->getAddress().to_string();
		
		bool open = i->second->getSocket().is_open();

		std::cout << " * " << addr << " > " << open << std::endl;
	}
}

unsigned int ServerInterface::getUID() const
{
	return CreateUID();
}

unsigned int ServerInterface::getResponseId() const
{
	return respond_to->getId();
}

std::string ServerInterface::getResponseIp() const
{
	return respond_to->getAddress().to_string();
}

unsigned int ServerInterface::getConnectionCount() const
{
	return connections.size();
}

unsigned int ServerInterface::getConnectionID(unsigned int c) const
{
	Connections::const_iterator i = connections.begin();

	// increment map iterator c times
	for (unsigned int n = 0; n < c; ++n, ++i);

	return i->first;
}

unsigned int ServerInterface::pollReceivedBytes()
{
	unsigned int temp = bytes_in;

	bytes_in = 0;

	return temp;
}

unsigned int ServerInterface::pollSentBytes()
{
	unsigned int temp = bytes_out;

	bytes_out = 0;

	return temp;
}

void ServerInterface::broadcast(const std::string & message)
{
	for (Connections::iterator i = connections.begin(); i != connections.end(); ++i)
	{
		i->second->send(message);
	}
}

void ServerInterface::broadcast(const void * data, unsigned int bytes)
{
	for (Connections::iterator i = connections.begin(); i != connections.end(); ++i)
	{
		i->second->send(data, bytes);
	}
}

void ServerInterface::broadcast(const NetPacket & packet, unsigned int size)
{
	for (Connections::iterator i = connections.begin(); i != connections.end(); ++i)
	{
		i->second->send(packet, size);
	}
}

void ServerInterface::respond(const void * data, unsigned int bytes)
{
	respond_to->send(data, bytes);
}

void ServerInterface::respond(const void * header, unsigned int header_size, const void * data, unsigned int size)
{
	respond_to->sendCombined(header, header_size, data, size);
}

void ServerInterface::respond(const NetPacket & packet, unsigned int size)
{
	respond_to->send(packet, size);
}

bool ServerInterface::registerConnection(unsigned int id, Connection * connection)
{
	connections[id] = connection;

	// before we go there we send the acc message

	
	if (do_connect_response)
	{
		ConnectPacket packet(id);
		respond(packet);
	}

	onConnect(id);

	return true;
}

bool ServerInterface::handleDisconnect(unsigned int id)
{
	connections.erase(id);

	if (do_connect_response)
	{
		DisconnectPacket packet(id);
		broadcast(packet);
	}
	
	onDisconnect(id);

	return true;
}

void ServerInterface::accept()
{
	Connection::pointer new_connection =
		Connection::create(acceptor.io_service());

	acceptor.async_accept(new_connection->getSocket(),
		boost::bind(&ServerInterface::handleAccept, this, new_connection,
			boost::asio::placeholders::error));
}

void ServerInterface::handleAccept(Connection::pointer new_connection, const boost::system::error_code& error)
{
	if (!error)
	{
		unsigned int uid = CreateUID();
		
		new_connection->start(this, uid);

		// continue to accept new clients
		accept();
	}
}

void ServerInterface::responderDisconnect()
{
	respond_to->getSocket().close();
}

void ServerInterface::responderRunOne()
{
	respond_to->getSocket().get_io_service().run_one();
}

void ServerInterface::disconnect(unsigned int id)
{
	handleDisconnect(id);
}