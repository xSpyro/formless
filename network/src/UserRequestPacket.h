#ifndef USER_REQUEST_PACKET__H
#define USER_REQUEST_PACKET__H

#include "RawPacket.h"

struct UserRequestPacket : public NetPacket
{
	unsigned int uid;

	UserRequestPacket() : NetPacket(USER_REQUEST_PACKET, 0)
	{
	}
};

#endif
