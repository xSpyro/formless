
#ifndef REQUEST_PACKET_H
#define REQUEST_PACKET_H

#include "RawPacket.h"

struct RequestPacket : public NetPacket
{
	unsigned int type;
	unsigned int uid;

	RequestPacket(unsigned int type, unsigned int uid) : NetPacket(REQUEST_PACKET, 0), type(type), uid(uid)
	{
	}
};



#endif