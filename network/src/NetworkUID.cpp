
#include "NetworkUID.h"

#include "../game/src/MemLeaks.h"

NetworkUID::NetworkUID()
{
	uid = 1000;
}

NetworkUID * NetworkUID::getInstance()
{
	static NetworkUID * instance = New NetworkUID;

	return instance;
}

unsigned int NetworkUID::generate()
{
	unsigned int temp = uid;
	++uid;

	return temp;
}