#ifndef ACHIEVEMENT__H
#define ACHIEVEMENT__H

#include <iostream>
#include <string>

class Achievement
{
public:
	struct InData
	{
		InData()
		{
			memset( this, 0, sizeof(InData) );
		}

		unsigned int type;
		unsigned int kills;
		unsigned int killstreak;
		bool rambo;

		// Filled in by master server automatically
		unsigned int playtime;
		unsigned int cores;
		unsigned int modules;
		unsigned int items;

		unsigned int totalCores;
		unsigned int totalModules;
		unsigned int totalItems;
	};

	enum Type
	{
		// WARNING: Do not change the order! If adding new, add to the end of the list
		TYPE_UNKNOWN = 0,
		TYPE_1_HOUR,
		TYPE_10_HOURS,
		TYPE_FIRST_BLOOD,
		TYPE_1000_KILLS,
		TYPE_5_KILLSTREAK,
		TYPE_ALL_ITEMS,
		TYPE_ALL_CORES,
		TYPE_ALL_MODULES,
		TYPE_TROLL
	};

	Achievement();
	Achievement( std::string name, std::string description );
	virtual ~Achievement();

	Achievement::Type	type;
	std::string			name;
	std::string			description;

	virtual bool		validate( const Achievement::InData & data );
};

#endif
