
#ifndef AUTH_RESULT_PACKET_H
#define AUTH_RESULT_PACKET_H

#include "RawPacket.h"

struct AuthResultPacket : public NetPacket
{
	int state;
	int uid;
	char name[32];

	AuthResultPacket() : NetPacket(AUTH_RESULT_PACKET, 0)
	{
	}

	void setName( const std::string & str )
	{
		strcpy_s( name, 32, str.c_str() );
	}
};

#endif