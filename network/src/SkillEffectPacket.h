#ifndef SKILL_EFFECT_PACKET_H
#define SKILL_EFFECT_PACKET_H

#include "RawPacket.h"

struct SkillEffectPacket : public NetPacket
{
	float damage;
	bool stun;
	bool link;
	bool outOfMana;
	int killStreak;
	float x;
	float y;
	float z;

	SkillEffectPacket() : NetPacket(SKILL_EFFECT_PACKET, 0)
	{
	}
};

#endif