#ifndef CREATE_USER_PACKET__H
#define CREATE_USER_PACKET__H

#include "RawPacket.h"

struct CreateUserPacket : public NetPacket
{
	char username[32];
	char password[32];
	bool success;

	CreateUserPacket() : NetPacket(CREATE_USER_PACKET, 0), success(false)
	{
	}

	void setUsername( const std::string & str )
	{
		strcpy_s( username, 32, str.c_str() );
	}

	void setPassword( const std::string & str )
	{
		strcpy_s( password, 32, str.c_str() );
	}
};

#endif
