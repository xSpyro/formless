
#include "Packer.h"

Packer::Packer()
{
	token = 0;
}

void Packer::restart()
{
	token = 0;
}

void Packer::packup(const char * data, unsigned int bytes)
{
	// insert data and maybe create a complete packet
	packupProcess(data, bytes);
}

void Packer::packupProcess(const char * data, unsigned int bytes)
{
	// amount of bytes that was read in this call to the function
	unsigned int amount = 0;
	bool full_header = false;

	if (token < RawPacket::HeaderSize)
	{
		// we might be able to build the head

		// how much to read max
		unsigned int max_read = RawPacket::HeaderSize - token;
		amount = std::min(max_read, bytes);

		// copy to buffer
		memcpy(&buffer[token], data, amount);
		token += amount;

		if (token == RawPacket::HeaderSize)
			full_header = true;
	}
	else
	{
		// otherwise we read until we fill our current packet
		RawPacket * current = reinterpret_cast < RawPacket* > (buffer.data());

		// now we know the size of the packet so we limit our read size to that
		unsigned int max_read = current->header.size - (token - RawPacket::HeaderSize);
		amount = std::min(max_read, bytes);

		// copy to buffer and then check if the packet is complete
		memcpy(&buffer[token], data, amount);
		token += amount;

		// see if we have a complete packet
		if (token == RawPacket::HeaderSize + current->header.size)
		{
			memcpy(&packet, &buffer.front(), RawPacket::HeaderSize);
			packet.data = buffer.data();

			// callback
			onPacket(packet);

			// reset tokens
			token = 0;

			// check if stream is empty
			if (amount == 0)
				return;
		}
	}

	// see if there is remaining data
	if (bytes - amount > 0 || full_header)
	{
		// call again with offset
		packupProcess(&data[amount], bytes - amount);
	}
}