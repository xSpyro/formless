

#ifndef FADE_TRANSITION_H
#define FADE_TRANSITION_H

#include "Common.h"
#include "SceneTransition.h"

class RenderModule;

class FadeTransition : public SceneTransition
{

public:

    FadeTransition(RenderModule * renderModule);
    ~FadeTransition();

    void onBegin(int frames);
	void onFadeChange();

    void onFadeIn();
	void onFadeOut();

private:

	int frame;
	int frames;

	float power;

	RenderModule * renderModule;
};


#endif