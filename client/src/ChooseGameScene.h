
#ifndef CHOOSEGAMESCENE_H
#define CHOOSEGAMESCENE_H

#include "Scene.h"
#include "Framework.h"

class Font2D;
class Graphics;
class ListBox;
class GameHandler;
class InputBox;

class ChooseGameScene : public Scene
{
public:

	ChooseGameScene();
	ChooseGameScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler* game, Font2D* font = NULL, Graphics* graphics = NULL );
	virtual ~ChooseGameScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	void onFocus();
	void onFocusLost();

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	bool onCreateObject(Object* object);	// Do not call this manually!
	bool onDestroyObject(Object* object);	// Do not call this manually!

	void onConnected(unsigned int id);
	void requestGameInfo();

	static void joinServer( const std::string & ip );

	void onRender2D(Font2D * font);
	struct Stage2D : public Stage
	{
		ChooseGameScene * me;
		Font2D * font;
		bool visible;

		Stage2D(ChooseGameScene * me, Font2D* font) : me(me), font(font)
		{
		}

		void apply()
		{
			if(visible)
				me->onRender2D(font);
		}
	};

private:

	void buildLayers();
	void clearLayers();
	void refreshList();

	//For lua
	static ChooseGameScene* me;

	static void refreshGameList();
	static void connect();
	static void createServer();

private:

	Stage2D * layer2d;

	Layer * layer;
	Camera camera;

	ListBox* list;
	GameHandler* game;
	InputBox* srvName;

	Timer	luaTimer;

	std::string warningText;

};

#endif