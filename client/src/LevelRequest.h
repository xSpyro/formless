#ifndef LEVEL_REQUEST__H
#define LEVEL_REQUEST__H

#include "FileRequestInterface.h"
#include "Logger.h"

class Client;

class LevelRequest : public FileRequestInterface
{
public:
	LevelRequest( boost::asio::io_service& service, const std::string& host, Client* client );
	~LevelRequest();

	void setClient( Client* client );

	void onStart();
	void onRate(unsigned int bytes, unsigned int total);
	void onDone(unsigned char * data, unsigned int bytes);

private:
	Client*		client;
	Logger		log;
};

#endif
