
#include "MainMenuScene.h"

#include "Common.h"
#include "Object.h"
#include "LogicHandler.h"
#include "RenderComponent.h"
#include "GameConnector.h"
#include "ScriptEnvironment.h"

#include "Framework.h"

#include "MemLeaks.h"

MainMenuScene::MainMenuScene()
	: Scene()
{
	
}

MainMenuScene::MainMenuScene(LogicHandler* handler, Renderer* rend, Window* wndw)
	: Scene(handler, rend, wndw)
{
	me		= this;
	layer	= renderer->createLayer();

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);
	camera.setProjection3D(45, 16 / 9.0f);

	// camera set

	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));


	layer2d = New Stage2D(this);

	// onCreate script
	File file;
	FileSystem::get("scripts/mainmenu.lua", &file);
	parent->getScript()->execute(file.data, file.size);
}

MainMenuScene::~MainMenuScene()
{
	delete layer2d;
}

void MainMenuScene::update()
{
	camera.update();
}

void MainMenuScene::render()
{

}

void MainMenuScene::onEnter()
{
	//renderer->insertLayer(0, layer);
	//renderer->insertStage(1000, layer2d);
	buildLayers();

	luaTimer.restart();

	parent->getScript()->getGlobals()["onMainMenuEnter"]();
}

void MainMenuScene::onLeave()
{
	//renderer->clearStage(0);
	//renderer->clearStage(1000);
	clearLayers();
}

void MainMenuScene::onFocus()
{
	//renderer->insertLayer(0, layer);
	//renderer->insertStage(1000, layer2d);
	buildLayers();
	layer2d->visible = true;
}

void MainMenuScene::onFocusLost()
{
	layer2d->visible = false;
}

bool MainMenuScene::onInput(int sym, int state)
{
	if(layer2d->visible)
		parent->getScript()->getGlobals()["onMainMenuInput"](sym, state);

	return true;
}

bool MainMenuScene::onMouseMovement(int x, int y)
{
	return true;
}

bool MainMenuScene::onCreateObject(Object* object)
{
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();

	if(rendcomp)
	{
		rendcomp->load(renderer);
		layer->add(&rendcomp->model);
	}

	return true;
}

bool MainMenuScene::onDestroyObject(Object* object)
{
	return true;
}

void MainMenuScene::onRender2D()
{
	if(layer2d->visible)
	{
		// call lua here
		if (luaTimer.accumulate(1.0f))
		{
			File file;
			FileSystem::get("scripts/mainmenu.lua", &file);
			if (!parent->getScript()->execute(file.data, file.size))
				std::cout << parent->getScript()->getLastError() << std::endl;
		}

		parent->getScript()->getGlobals()["onMainMenuRender"]();
	}
}

MainMenuScene* MainMenuScene::me = NULL;

void MainMenuScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer(0, layer);
	renderer->insertStage(1000, layer2d);
}

void MainMenuScene::clearLayers()
{
	renderer->clearStage(0);
	renderer->clearStage(1000);
}
