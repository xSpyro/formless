
#ifndef CREDITSSCENE_H
#define CREDITSSCENE_H

#include "Scene.h"

class CreditsScene : public Scene
{

public:
	CreditsScene();
	CreditsScene(LogicHandler* handler, Renderer* rend, Window* wndw);
	virtual ~CreditsScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	void onFocus();
	void onFocusLost();

	bool onInput(int sym, int state);
	bool onMouseMovement(int x, int y);

	void onRender2D();
	struct Stage2D : public Stage
	{
		CreditsScene * me;
		bool visible;

		Stage2D(CreditsScene * me) : me(me), visible(true)
		{
		}

		void apply()
		{
			if(visible)
				me->onRender2D();
		}
	};

	void onRender2DPost();
	struct Stage2DPost : public Stage
	{
		CreditsScene * me;
		bool visible;

		Stage2DPost(CreditsScene * me) : me(me)
		{
		}

		void apply()
		{
			if(visible)
				me->onRender2DPost();
		}
	};

private:
	void buildLayers();
	void clearLayers();

private:
	//For lua
	static CreditsScene*	me;


private:
	Stage2D *	layer2d;
	Stage2DPost * layerPost;

	Layer *		layer;
	Camera		camera;

	Timer		luaTimer;
};

#endif