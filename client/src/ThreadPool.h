
#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include "Common.h"

#include <boost/thread.hpp>

class ThreadPool
{
	typedef std::list < boost::thread* > Threads;

public:

	ThreadPool();

	void addWork(boost::thread * thread);

private:

	Threads threads;
};

#endif