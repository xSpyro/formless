#include "ClientInterface.h"
#include "CommonPackets.h"
#include "Client.h"

#include "TeamSelectionScene.h"

#include "Common.h"
#include "LogicHandler.h"
#include "GameHandler.h"

#include "NetworkSubsystem.h"
#include "NetworkSubsystemClient.h"
#include "TextBox.h"

#include "MemLeaks.h"

TeamSelectionScene* TeamSelectionScene::me = NULL;

TeamSelectionScene::TeamSelectionScene()
	: Scene()
{
}

TeamSelectionScene::TeamSelectionScene( LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game, Font2D* font )
	: Scene( handler, rend, wndw ), game( game )
{
	me					= this;
	layer				= renderer->createLayer();
	layer2d				= New Stage2D(this);
	team0				= New TextBox( font );
	team1				= New TextBox( font );

	team0->setPos( 335 + 12, 160 + 12, 300 - 24, 400 - 24 );
	team0->setVisible( true );

	team1->setPos( 645 + 12, 160 + 12, 300 - 24, 400 - 24 );
	team1->setVisible( true );

	File file;
	FileSystem::get("scripts/teamselection.lua", &file);
	parent->getScript()->execute(file.data, file.size);

	EventLua::Object g = parent->getScript()->getGlobals();
	//g.bind("getGameStarted", &getGameStarted);
}

TeamSelectionScene::~TeamSelectionScene()
{
	delete layer2d;
	delete team0;
	delete team1;
}

void TeamSelectionScene::update()
{
	getPlayers();
	team0->update();
	team1->update();
}

void TeamSelectionScene::render()
{
}

void TeamSelectionScene::onEnter()
{
	//renderer->insertLayer( 10, layer );
	//renderer->insertStage( 1000, layer2d );
	buildLayers();

	net = (NetworkSubsystem*)game->getSubsystem( "Network" );

	NetworkSubsystemClient* netClient = (NetworkSubsystemClient*)net;
	stats = netClient->getStats();

	PlayerListRequestPacket packet;
	game->getNetwork()->send( packet );

	getPlayers();

	layer2d->visible = true;

	luaTimer.restart();
}

void TeamSelectionScene::onLeave()
{
	clearLayers();

	layer2d->visible = false;
}

void TeamSelectionScene::onFocus()
{
	//renderer->insertLayer(0, layer);
	//renderer->insertStage(1000, layer2d);
	buildLayers();
	layer2d->visible = true;
}

void TeamSelectionScene::onFocusLost()
{
	layer2d->visible = false;
}

bool TeamSelectionScene::onInput( int sym, int state )
{
	switch( sym )
	{
	case VK_F2:
		getPlayers();
		break;
	}

	if(layer2d->visible)
		parent->getScript()->getGlobals()["onTeamSelectionInput"](sym, state);

	return true;
}

bool TeamSelectionScene::onMouseMovement( int x, int y )
{
	return true;
}

void TeamSelectionScene::onRender2D()
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/teamselection.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout<< parent->getScript()->getLastError()<<std::endl;
	}
	team0->render();
	team1->render();
	parent->getScript()->getGlobals()["onTeamSelectionRender"]();
}

void TeamSelectionScene::getPlayers()
{
	team0->clear();
	team1->clear();
	net->queryPlayers( players );

	for( Players::iterator it = players.begin(); it != players.end(); ++it )
	{
		if( it->team == 1 )
		{
			team0->addMessage( it->getName() );
		}
		else if( it->team == 2 )
		{
			team1->addMessage( it->getName() );
		}
	}
}

void TeamSelectionScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer( 10, layer );
	renderer->insertStage( 1000, layer2d );
}

void TeamSelectionScene::clearLayers()
{
	renderer->clearStage(10);
	renderer->clearStage(1000);
}
