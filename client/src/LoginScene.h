
#ifndef LOGINSCENE_H
#define LOGINSCENE_H

#include "Scene.h"
#include "Framework.h"

class LoginForm;
class Font2D;

class LoginScene : public Scene
{
public:

	LoginScene();
	LoginScene(LogicHandler* handler, Renderer* rend, Window* wndw, Font2D* font = NULL);
	virtual ~LoginScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	void onRender2D();
	struct Stage2D : public Stage
	{
		LoginScene * me;
		bool visible;

		Stage2D(LoginScene * me) : me(me)
		{
		}

		void apply()
		{
			if(visible)
				me->onRender2D();
		}
	};

public:
	static bool login(const char* username = NULL, const char* password = NULL);	// Returns success or failure
	static void createUser();
	static void loadUser();
	static void saveUser(const char* username, const char* password);

private:
	//For lua
	static LoginScene* me;

private:
	void buildLayers();
	void clearLayers();

	Stage2D *	layer2d;

	Layer *		layer;
	Camera		camera;

	LoginForm*	form;

	Timer		luaTimer;
};

#endif