
#include "DemoScene.h"

#include "Object.h"
#include "RenderComponent.h"

#include "GameHandler.h"

/*

DemoScene::DemoScene()
{
}

DemoScene::DemoScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game, Chat* chat)
	: Scene(handler, rend, wndw), game(game)
{

}

DemoScene::~DemoScene()
{
}

void DemoScene::update()
{
	float v = timer.now() * 0.1f;

	camera.setPosition(Vector3(300 * cos(v), 300, -300 * sin(v)));
	camera.setLookAt(Vector3(0, 10.0f, 0));

	renderModule.update();
}

void DemoScene::render()
{
	renderModule.preFrame();
}

void DemoScene::onEnter()
{
	renderModule.init(renderer, RM_TERRAIN | RM_OBJECTS);
	renderModule.getBlendTexture().load("textures/texture_blend.png");
	renderModule.build();
	game->setHeightMap(&renderModule.getTerrain());
	game->setBlendMap(&renderModule.getBlendTexture().begin());

	camera.setClipRange(10, 600);
	camera.setProjection3D(45, 16 / 9.0f);

	renderModule.setPlayerPosition(0);
	renderModule.setCursorPosition(0);
	renderModule.bind();

	game->createObjectExt("Ruin", Vector3(0, 0, 0), 0);
}

void DemoScene::onLeave()
{
}

void DemoScene::onLoad()
{
	renderModule.updateTerrain();
}

bool DemoScene::onInput(int sym, int state)
{
	return false;
}

bool DemoScene::onMouseMovement(int x, int y)
{
	return false;
}

void DemoScene::onCreateObject(Object* object)
{
	RenderComponent * rend = object->findComponent<RenderComponent>();

	if (rend)
	{
		renderModule.addObject(rend);
	}
}

void DemoScene::onDestroyObject(Object* object)
{
}

*/