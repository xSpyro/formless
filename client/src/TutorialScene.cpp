#include "Client.h"

#include "TutorialScene.h"

#include "Common.h"
#include "Object.h"
#include "LogicHandler.h"

#include "RenderComponent.h"

#include "MemLeaks.h"

#include "GameConnector.h"

#include "InventoryScene.h"

#include "CoreComponent.h"

TutorialScene::TutorialScene()
	:Scene()
{

}

TutorialScene::TutorialScene(LogicHandler* handler, Renderer* rend, Window* wndw, Font2D* font, Graphics* graphics, Scene* const inventory)
	: Scene(handler, rend, wndw)
{

	this->inventory = inventory;

	layer = renderer->createLayer();

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);


	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));

	layer2d = New Stage2D(this);

	EventLua::Object g = parent->getScript()->getGlobals();

	File file;
	FileSystem::get("script/tutorial.lua", &file, true);

	if ( !parent->getScript()->execute(file.data, file.size) ) 
		std::cout << "Script not executed" << std::endl;
}

TutorialScene::~TutorialScene()
{
	Delete layer2d;
}

void TutorialScene::update()
{
	InventoryScene* is = (InventoryScene*)inventory;

	
	CoreComponent* core = is->getCurrentPlayerCore();
	if (core)
	{
		if (core->name == "")
		{
			// no core
			parent->getScript()->getGlobals().set("showCoreTutorial", 1);
			parent->getScript()->getGlobals().set("showModuleTutorial", 0);
		}
		else
		{
			parent->getScript()->getGlobals().set("showCoreTutorial", 0);
			parent->getScript()->getGlobals().set("showModuleTutorial", 1);
		}
	}

}

void TutorialScene::render()
{
}

void TutorialScene::onEnter()
{
	buildLayers();

	luaTimer.restart();

	File file;
	FileSystem::get("scripts/tutorialmenu.lua", &file);
	if (!parent->getScript()->execute(file.data, file.size))
		std::cout << parent->getScript()->getLastError() << std::endl;

	parent->getScript()->getGlobals()["onTutorialEnter"]();

	layer2d->visible = true;

	parent->getScript()->getGlobals().set("showTutorial", 1);

	parent->getScript()->getGlobals()["onInventoryEnter"]();


}

void TutorialScene::onLeave()
{
	clearLayers();
	layer2d->visible = false;
	parent->getScript()->getGlobals().set("showTutorial", 0);

	parent->getScript()->getGlobals()["onInventoryLeave"]();
}

void TutorialScene::onLoad()
{

}

bool TutorialScene::onInput(int sym, int state)
{
	if( layer2d->visible )
		parent->getScript()->getGlobals()["onTutorialInput"](sym, state);
	return true;
}
bool TutorialScene::onMouseMovement(int x, int y)
{
	return true;
}

bool TutorialScene::onCreateObject(Object* object)
{
	return true;
}
bool TutorialScene::onDestroyObject(Object* object)
{
	return true;
}

void TutorialScene::onConnected(unsigned int id)
{

}

void TutorialScene::onFocus()
{
	parent->getScript()->getGlobals()["onTutorialEnter"]();
	parent->getScript()->getGlobals().set("showTutorial", 1);
	layer2d->visible = true;

	InventoryScene* is = (InventoryScene*)inventory;
	parent->getScript()->getGlobals()["onInventoryEnter"]();
	is->setLayer2DVisibility(true);


}
void TutorialScene::onFocusLost()
{
	parent->getScript()->getGlobals()["onTutorialLeave"]();
	parent->getScript()->getGlobals().set("showTutorial", 0);
	parent->getScript()->getGlobals()["onInventoryLeave"]();
	
	layer2d->visible = false;


}

void TutorialScene::onRender2D()
{
	// call lua here
	InventoryScene* is = (InventoryScene*)inventory;

	is->setLayer2DVisibility(true);
	//is->onRender2D();

	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/tutorial.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout << parent->getScript()->getLastError() << std::endl;
	}


	parent->getScript()->getGlobals()["onTutorialRender"]();
}

void TutorialScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer(1603, layer);
	renderer->insertStage(1604, layer2d);


}
void TutorialScene::clearLayers()
{
	renderer->clearStage(1603);
	renderer->clearStage(1604);
}