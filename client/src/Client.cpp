#include "Client.h"

#include "RequestInterface.h"

#include "LogicHandler.h"
#include "GameHandler.h"

#include "PhysicsSubsystem.h"
#include "RenderSubsystem.h"
#include "RenderComponent.h"
#include "ControllerSubsystem.h"
#include "TimerSubsystem.h"
#include "SkillSubsystem.h"
#include "CoreSubsystem.h"
#include "ScriptSubsystem.h"
#include "AnimationSubsystem.h"
#include "NetworkSubsystemClient.h"
#include "SoundSubsystem.h"
#include "EffectSubsystem.h"
#include "StatsSubsystem.h"
#include "EffectComponent.h"
#include "CoreComponent.h"
#include "AISubsystem.h"

#include "EventHandler.h"
#include "EventEnum.h"

#include "GameConnector.h"

#include "StartupScene.h"
#include "MainMenuScene.h"
#include "ArenaScene.h"
#include "InventoryScene.h"
#include "CompanionInventoryScene.h"
#include "PauseMenuScene.h"
#include "LobbyScene.h"
#include "Chat.h"
#include "Console.h"
#include "LoginScene.h"
#include "ChooseGameScene.h"
#include "TeamSelectionScene.h"
#include "EndGameScene.h"
#include "CreateAccountScene.h"
#include "ProfileScene.h"
#include "LoadingScene.h"
#include "IntroScene.h"
#include "TutorialScene.h"
#include "LaggTestScene.h"
#include "CreditsScene.h"
#include "TrainingScene.h"

#include "UmlScene.h"

#include "DemoScene.h"
#include "ParticleDemoScene.h"

#include "CommonPackets.h"

#include "MemLeaks.h"


#include "ParticleAnimationSequenceFormat.h"
#include "LevelFormat.h"
#include "EffectFormat.h"

#include "FadeTransition.h"
#include "BlurTransition.h"

#include "OptionsScene.h"

#include "LevelRequest.h"
#include "LevelFormat.h"

#include "AchievementInfo.h"

Client::Client(boost::asio::io_service & service, int show, HINSTANCE inst) : Program(), Window(), ClientInterface(service),
	updateGameProfile("Game", false),
	updateRestProfile("Rest", false),
	focus(true)
{
	game			= NULL;
	chat			= NULL;
	console			= NULL;
	arenaScene		= NULL;
	gameStats		= NULL;
	levelName = "Network.flf";

	poll_network	= true;

	// set request async data
	RequestInterface::setRequestAsyncData(service, &Client::onPacketStatic);

	Logger::print()("Window: cmdShow ", show);
	Logger::print()("HINSTANCE: ", inst, " - ", GetModuleHandle(NULL));
	setStartupData(show, inst);

	musicThread = NULL;
}

Client::~Client()
{
	
	delete game;
	delete direct2dtexture;
	delete chat;
	delete console;

	font.shutDown();

	delete renderModule;



	Delete musicThread;
}

bool Client::startup()
{
	tick = 0;

	width = 1280;
	height = 720;

	FileSystem::init();
	FileSystem::addResource("../resources");
	FileSystem::iterateUsingCallback("", &fileCallback, NULL);

	LogicHandler* logic	= New LogicHandler(this);
	game				= New GameHandler(logic);

	SoundSubsystem*			sound	= New SoundSubsystem(game);

	logic->registerSubsystem("Sound", sound);

	renderModule = new RenderModule;

	File settingsFile;

	if (FileSystem::get("settings.txt", &settingsFile, false))
	{
		game->getScript()->execute(settingsFile.data, settingsFile.size);
		width = logic->getScript()->get("Settings").get("Graphics").get("width").queryInteger(1280);
		height = logic->getScript()->get("Settings").get("Graphics").get("height").queryInteger(720);
	}

	init(width, height, "Formless Beta - AI Version 0.9");
	open();

	me = this;
	user_id = 0;
	user_name = "";


	game->setNetworkInterface(this);

	FileSystem::setBaseDirectory("../resources/");

	GameConnector::setGame(game);
	game->init();
	game->setFormatStore(&formatStore);

	// register some global functions to lua
	EventLua::Object g = logic->getScript()->getGlobals();
	g.bind("setFont", &setFont);
	g.bind("setBlendColor", &setBlendColor);
	g.bind("drawSprite", &drawSprite);
	g.bind("drawSpriteAdd", &drawSpriteAdd);
	g.bind("drawSpriteRot", &drawSpriteRot);
	g.bind("drawShinySprite", &drawShinySprite);
	g.bind("drawShine", &drawShine);
	g.bind("drawShineExt", &drawShineExt);
	g.bind("drawBorder", &drawBorder);
	g.bind("drawBorderEx", &drawBorderEx);
	g.bind("drawBar", &drawBar);
	g.bind("drawLine", &drawLine);
	g.bind("drawString", &drawString);
	g.bind("drawText", &drawStringArea);
	g.bind("getStringHeight", &getStringHeight);
	g.bind("gotoScene", &gotoScene);
	g.bind("gotoSceneUsingTransition", &gotoSceneUsingTransition);
	g.bind("activateScene", &activateScene);
	g.bind("deactivateScene", &deactivateScene);
	g.bind( "getUserName", &getUserName );
	g.bind( "getOnlineStatus", &getOnlineStatus );
	g.bind("isSpectator", &isSpectator);
	g.bind( "logoff", &logoff );
	g.bind( "quit", &exit );
	g.bind( "say", &say );

	g.bind("saveKeybinds", &saveKeybindsLua);
	g.bind("loadKeybinds", &loadKeybindsLua);

	g.bind( "spawnHudEffect", &spawnHudEffect );

	g.bind("getStats", &getStats);
	g.bind("getStringMetrics", &getStringMetrics);

	//g.bind( "awardItem", &awardItem );
	//g.bind("cprint", &cprint); 

	g.set("FONT_DEFAULT", "Trebuchet MS:20");
	renderer.join(this);

	// setup direct2d
	font.createDevices(1280, 720);
	font.createFont("Trebuchet MS:20");

	//bool result = font.createFont("Berlin Sans FB:20");
	//result = font.setFont("century.ttf:20");

	//result = result;

	// bind to shared texture
	initDirect2D();

	loadSettings();

	console = New Console( &font, logic->getScript() );

	renderer.insertOverlay(5000, direct2dtexture, NULL);
	 
	PhysicsSubsystem*		phys	= New PhysicsSubsystem(game);
	RenderSubsystem*		rend	= New RenderSubsystem();
	ControllerSubsystem*	cont	= New ControllerSubsystem(game);
	ScriptSubsystem *		script	= New ScriptSubsystem();
	TimerSubsystem*			timer	= New TimerSubsystem();
	SkillSubsystem*			skill	= New SkillSubsystem(game);
	CoreSubsystem*			core	= New CoreSubsystem(game);
							network	= New NetworkSubsystemClient(game, this);
	EffectSubsystem*		effect	= New EffectSubsystem(game);
	StatsSubsystem*			stats	= New StatsSubsystem(game);
	AISubsystem*			ai		= New AISubsystem(game);

	NetworkSubsystemClient*	netClient	= (NetworkSubsystemClient*)network;
	gameStats	= netClient->getStats();

	sound->setMusicVolume(0.0f);
	sound->getMusicPlayer()->tellCurrentPlaylist("Start");
	sound->getMusicPlayer()->randomizeSongFromCurrentContainer(false);
	
	animation = New AnimationSubsystem(game, 1000, &formatStore);

	particleSystem.init(&renderer);
	renderModule->setParticleSystem(&particleSystem);
	
	
	renderModule->init(&renderer, RM_ALL | RM_EFFECTS);
	renderModule->build();

	renderModule->bindHud();

	formatStore.registerFormat("paf", New EffectFormat(NULL, renderModule, &particleSystem)); // Added by meee....

	formatStore.importFromFile("effects/mongo_boll.paf");

	formatStore.importFromFile("effects/ice_hit3.paf");
	formatStore.importFromFile("effects/recharge.paf");
	formatStore.importFromFile("effects/electric_hit_textured.paf"); // nya texturer som typ
	formatStore.importFromFile("effects/ice_hit.paf");
	formatStore.importFromFile("effects/teleport.paf");
	formatStore.importFromFile("effects/hud_animation.paf");
	//formatStore.importFromFile("effects/ice_hit_shard.paf");         // nya texturer som typ
	formatStore.importFromFile("effects/ice_hit_bolt.paf");
	formatStore.importFromFile("effects/barrage.paf");
	formatStore.importFromFile("effects/laso.paf");
	formatStore.importFromFile("effects/soil_hit.paf");
	formatStore.importFromFile("effects/splitter_hit.paf");
	formatStore.importFromFile("effects/pilar_effect.paf");
	formatStore.importFromFile("effects/pilar_effect_heat.paf");
	formatStore.importFromFile("effects/jump.paf");
	formatStore.importFromFile("effects/spear.paf");
	formatStore.importFromFile("effects/link.paf");
	formatStore.importFromFile("effects/firebolt_hit.paf");
	formatStore.importFromFile("effects/shield.paf");
	formatStore.importFromFile("effects/teleport_v2.paf");
	formatStore.importFromFile("effects/hammer_hit.paf");
	formatStore.importFromFile("effects/hammer_hit2.paf");
	formatStore.importFromFile("effects/shout1.paf");
	formatStore.importFromFile("effects/heat_middle.paf");
	formatStore.importFromFile("effects/daniels_svett2.paf");
	
	
	

	// register extensions
	formatStore.registerFormat("pas", New ParticleAnimationSequenceFormat(animation));
	formatStore.registerFormat("flf", New LevelFormat(game));

	chat = New Chat( game, &font, network );

	logic->registerSubsystem("Physics", phys);
	logic->registerSubsystem("Render", rend);
	logic->registerSubsystem("Controller", cont);
	logic->registerSubsystem("Script", script);
	logic->registerSubsystem("Timers", timer);
	logic->registerSubsystem("Skill", skill);
	logic->registerSubsystem("Core", core); 
	logic->registerSubsystem("Animation", animation);
	logic->registerSubsystem("Network", network);
	logic->registerSubsystem("Effect", effect);  
	logic->registerSubsystem("Stats", stats);

	logic->registerSubsystem("AI", ai);

	logic->createScene("Startup", New StartupScene(logic, &renderer, this));

	InventoryScene* inventoryScene = New InventoryScene(logic, &renderer, this, game);
	logic->createScene("MainMenu", New MainMenuScene(logic, &renderer, this));
	logic->createScene("OptionsMenu", New OptionsScene(logic, &renderer, this, &font, &graphics));
	logic->createScene("Inventory", inventoryScene);
	logic->createScene("CompanionInventory", New CompanionInventoryScene(logic, &renderer, this, game));
	logic->createScene("PauseMenu", New PauseMenuScene(logic, &renderer, this));
	logic->createScene("Lobby", New LobbyScene(logic, &renderer, this, game, chat));
	logic->createScene("Login", New LoginScene(logic, &renderer, this, &font));
	logic->createScene("ChooseGame", New ChooseGameScene(logic, &renderer, this, game, &font, &graphics));
	logic->createScene("TeamSelection", New TeamSelectionScene(logic, &renderer, this, game, &font));
	logic->createScene("EndGame", New EndGameScene(logic, &renderer, this, game, &font));
	logic->createScene("CreateAccount", New CreateAccountScene(logic, &renderer, this, &font));
	logic->createScene("Training", New TrainingScene(&renderer, this, game, renderModule, &font));
	logic->createScene("Profile", New ProfileScene(logic, &renderer, this));
	logic->createScene("Intro", New IntroScene(game, &renderer, this));
	logic->createScene("Credits", New CreditsScene(logic, &renderer, this));

	logic->createScene("Loading", New LoadingScene(game, &renderer, this, &font, this));

	logic->createScene("Tutorial", New TutorialScene(logic, &renderer, this, &font, &graphics, inventoryScene));

	logic->createScene("Uml", New UmlScene(logic, &renderer, this, &font));

	ArenaScene::InitData data;

	data.window = this;
	data.rend = &renderer;
	data.game = game;
	data.chat = chat;
	data.ps = &particleSystem;
	data.font = &font;
	data.renderModule = renderModule;
	data.client = this;
	
	arenaScene = New ArenaScene(data);
	logic->createScene("Arena", arenaScene);


	// LAGG TEST SCENE
	{
		LaggTestScene::InitData data;

		data.window = this;
		data.rend = &renderer;
		data.game = game;
		data.chat = chat;
		data.ps = &particleSystem;
		data.font = &font;
		data.renderModule = renderModule;
		data.client = this;
	
		logic->createScene("LaggTest", New LaggTestScene(data));
	}
	///////////////////////

	loadKeybinds();

	game->registerObjectTemplate("Node");
	game->registerObjectTemplate("Player");
	game->registerObjectTemplate("Box");
	game->registerObjectTemplate("AIPlayer");
	game->registerSkillTemplate("Projectile");
	game->registerSkillTemplate("Explosion");
	game->registerSkillTemplate("Ball");
	game->registerSkillTemplate("SpeedBuff");
	game->registerSkillTemplate("DefaultProjectile");
	game->registerSkillTemplate("Splitter");
	game->registerSkillTemplate("SplitterProjectile");
	game->registerSkillTemplate("DefaultProjectileHit");
	game->registerSkillTemplate("ProjectileHit");
	game->registerSkillTemplate("Charge");
	game->registerSkillTemplate("LinkProjectile");
	game->registerSkillTemplate("DefaultLink");
	game->registerSkillTemplate("Tripwire");
	game->registerSkillTemplate("TripwireLink");
	game->registerSkillTemplate("FireBolt");
	game->registerSkillTemplate("FireBoltHit");
	game->registerSkillTemplate("IceBolt");
	game->registerSkillTemplate("IceBoltHit");
	game->registerSkillTemplate("LightningBolt");
	game->registerSkillTemplate("LightningBoltHit");
	game->registerSkillTemplate("SoilBolt");
	game->registerSkillTemplate("SoilBoltHit");
	game->registerSkillTemplate("Teleport");
	game->registerSkillTemplate("Barrage");
	game->registerSkillTemplate("Lasso");
	game->registerSkillTemplate("LassoHit");
	game->registerSkillTemplate("Jump");
	game->registerSkillTemplate("Razorwire");
	game->registerSkillTemplate("RazorwireLink");
	game->registerSkillTemplate("Wall");
	game->registerSkillTemplate("WallLink");
	game->registerSkillTemplate("Shield");
	game->registerSkillTemplate("Spear");
	game->registerSkillTemplate("SpearHit");
	game->registerSkillTemplate("BarrageProjectile");
	game->registerSkillTemplate("BarrageProjectileHit");
	game->registerSkillTemplate("LifeStream");
	game->registerSkillTemplate("Hammer");
	game->registerSkillTemplate("HammerHit");
	game->registerSkillTemplate("Shout");
	

	game->registerObjectTemplate("Ruin");
	game->registerObjectTemplate("Torus");
	game->registerObjectTemplate("Spawn");
	game->registerObjectTemplate("Spire");
	game->registerObjectTemplate("Plate");
	game->registerObjectTemplate("Stairs");

	game->registerObjectTemplate("TestLaggObject");


	EventLua::State state;

	if (state.execute("startup.lua"))
	{
		EventLua::Object startupScene = state.get("Scene");
		if (startupScene)
		{
			logic->activateScene(startupScene.queryString("Intro"));
		}
		else
		{
			logic->activateScene("Intro");
		}
	}
	else
	{
		logic->activateScene("Intro");
	}
	

	graphics.init(&renderer);
	g.bind("setTextAlign", &setTextAlignment);
	g.bind("setParaAlign", &setParagraphAlignment);
	g.bind("setFontColor", &setFontColor);


	updateSettings();

	return isOpen();
}

bool Client::shutdown()
{
	// always save settings before exiting, this to prevent functionality loss when editing settingsparameters
	this->saveSettings();

	arenaScene = NULL;
	if( online > 0 )
	{
		MasterSendAsync request;

		DropUserPacket packet;
		packet.uid = user_id;
		request(packet, sizeof(packet));
	}

	renderer.flush();

	return false;
}

void Client::update()
{
	updateGameProfile.begin();
	game->update();
	updateGameProfile.end();

	updateRestProfile.begin();


	console->update();
	
	renderModule->update();
	updateRestProfile.end();
}

void Client::run()
{
	Timer timer;
	Timer timeframe;

	int fps_temp = 0;
	fps = 0;
	bytes_in = 0;

	float templagg = 0;
	Timer netTimer;

	float temp_update = 0;
	float temp_render = 0;
	float temp_present = 0;
	float temp_winnet = 0;


	while(isOpen()) 
	{
		Timer wntimer;
		process();
		poll();
		temp_winnet += wntimer.now();

		/* Used for keeping track if our window lose/gain focus */
		if( !focus && getHandle() == GetFocus() )
		{
			focus = true;
		}
		else if( focus && getHandle() != GetFocus() )
		{
			game->getEventHandler()->refreshKeys();
			focus = false;
		}

		if (timeframe.accumulate(1.0f / 60.0f))
		{

			if (game->isInTransition())
			{
				renderModule->renderToScreen(false);

				renderer.clear(NULL);

				font.begin();
				game->render();
				renderer.render();
				font.end();

				renderer.flush();

				update();
			}
			else
			{
				
				Timer utimer;
				update();
				temp_update += utimer.now();
			}
		}

		
		if (!game->isInTransition())
		{
			
			Timer rtimer;
			renderModule->renderToScreen(true);

			renderer.clear(NULL);
			/*
			game->render();
			renderer.render();

			font.begin();
			font.end();
			*/

			if (!game->isInScene("Uml"))
				font.begin();
			game->render();
			renderer.render();
			game->getEventHandler()->update(); // So that the lua system gets to do it's thing before some special keys are cleared.

			if (!game->isInScene("Uml"))
				font.end();

			if( console->isActive() )
			{
				font.begin();
				graphics.drawSprite( "bg.png", 0, 0, 1280.0f, 440.0f );
				console->render();
				font.end();
			}
			temp_render += rtimer.now();



			Timer ptimer;
			renderer.present(false);
			temp_present += ptimer.now();
			
		}
		else
		{
			renderer.present();
		}
		

		fps_temp++;
		if (timer.accumulate(1.0f))
		{
			fps = fps_temp;

			
			

			bytes_in = pollReceivedBytes();
			bytes_out = pollSentBytes();

			profile_update = temp_update / fps_temp;
			profile_render = temp_render / fps_temp;
			profile_present = temp_present / fps_temp;
			profile_winnet = temp_winnet / fps_temp;


			net_lagg = temp_update + temp_render + temp_present;
			
			templagg = 0;
			temp_update = 0;
			temp_render = 0;
			temp_present = 0;
			temp_winnet = 0;
			fps_temp = 0;

			//std::cout << fps << std::endl;
		}
	}
}

void Client::onInput(int key, int state)
{
	if( key == VK_F1 && state == 1 )
	{
		console->setActive( !console->isActive() );
	}

	if( console->isActive() )
	{
		console->onInput( key, state );
		return;
	}

	if (key == VK_F3 && state == 1)
	{
		renderer.texture()->reload();
	}

	if (key == VK_F5 && state == 1)
	{
		renderer.shader()->reload();
	}

	if (key == VK_F4 && state == 1)
	{
		renderer.switchWindowFullScreenMode();
	}

	if (key == VK_F6 && state == 1)
	{
		game->reloadAllLuaFiles();
	}

	game->onInput(key, state);
}

void Client::onMouseMovement(int x, int y)
{
	// convert to virtual coordinates

	// this is our virtual window coordinates
	// we coinvert so mouse position always reflects
	// these

	float mx, my;
	this->getMouseVector(&mx, &my);

	mx = (mx + 1) * (1280 / 2);
	my = (1 - my) * (720 / 2);

	if( !console->isActive() )
		game->onMouseMovement(static_cast < int > (mx), static_cast < int > (my));
}

void Client::onResizeWindow(unsigned int width, unsigned int height)
{
	std::cout << "Client onResize" << std::endl;

	renderer.onResizeWindow(width, height);
}

void Client::onSceneChange()
{
	renderer.clearStage(5000);
	renderer.insertOverlay(5000, direct2dtexture, NULL);
}

void Client::onCreateObject(Object* object)
{
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();

	if (rendcomp)
	{
		renderModule->addObject(rendcomp);
	}	

	AnimationComponent* animcomp = object->findComponent<AnimationComponent>();

	if (animcomp)
	{
		renderModule->addLinkedParticle(animcomp);
	}

	EffectComponent* effcomp = object->findComponent<EffectComponent>();

	if (effcomp)
	{
		Object* creator = object->getCreator();
		PhysicsComponent * com = NULL;
		if (creator)
			com = creator->findComponent<PhysicsComponent>();
		if (com)
		{
			Vector3 normal = Vector3(0, 1, 0); // riktning p� terr�ngen/surface
			renderModule->spawnEffect(effcomp->getName(), com->position, normal);
		}
	}
}

void Client::onDestroyObject(Object* object)
{

}

void Client::onPacket(RawPacket & raw)
{
	switch( raw.header.id )
	{
	case LEVEL_DATA_RESPONSE_PACKET:

		processLevelDataResponsePacket( *reinterpret_cast < LevelDataResponsePacket* > (raw.data) );
		break;

	default:

		network->onPacket(raw);
		break;
	}
}

void Client::onConnected(unsigned int id)
{
	// loading goes to teamselection afterwards
	gotoSceneUsingTransition("Loading", 0, 20);
}

void Client::onDisconnected()
{
	network->onDisconnected();

	online = 0;
	user_id = 0;

	// go back
	if (!game->isInScene("MainMenu"))
	{
		gotoSceneUsingTransition("MainMenu", 0, 20);
	}
}

void Client::loadingPostServerSelect()
{
	game->getNetwork()->send( LevelDataRequestPacket() );
	
	game->setHeightMap(&renderModule->getTerrain());
	game->setBlendMap(&renderModule->getBlendTexture().begin());

	renderModule->updateTerrain();

	game->onConnected(network->getID());

	SoundSubsystem* sound = (SoundSubsystem*)game->getSubsystem("Sound");

	// load in another thread
	if (musicThread == NULL)
	{
		// only load once
		musicThread = New boost::thread(boost::bind(&MusicPlayer::queryAllSongs, sound->getMusicPlayer()));
	}
}

void Client::setUser( unsigned int id, std::string name )
{
	user_id = id;
	user_name = name;
}

unsigned int Client::getUserID()
{
	return user_id;
}

void Client::setOnlineStatus( int status )
{
	online = status;
}

void Client::loadSettings()
{
	File f;

	FileSystem::get("settings.txt", &f, false);
	
	
	if (game->getScript()->execute("settings.txt"))
	{
		EventLua::Object settings, graphics, soundTable, gameTable;

		settings = game->getScript()->get("Settings");

		graphics = settings.get("Graphics");

		soundTable = settings.get("Sound");

		gameTable = settings.get("Game");

		width = graphics.get("width").queryInteger(1280);
		height = graphics.get("height").queryInteger(720);

		renderModule->getCamera()->setProjection3D(70, (float)width / (float)height);

		int ssao = graphics.get("ssao").queryInteger(0);
		int shadows = graphics.get("shadows").queryInteger(0);
		int fog = graphics.get("fog").queryInteger(0);

		int showStats = graphics.get("showStats").queryInteger(0);

		float visualScale = graphics.get("visualScale").queryFloat(0.0f);
		int showBoundingVolumes = graphics.get("showBoundingVolumes").queryInteger(0);
		renderModule->setSSAO(ssao == 1);
		renderModule->setShadows(shadows == 1);
		renderModule->setFog(fog == 1);
		renderModule->setVisualScale(visualScale);	
		
		
		renderModule->setBoundingVolumesVisibility(showBoundingVolumes == 1);

		float masterVolume = soundTable.get("masterVolume").queryFloat(0.8f);
		float fxVolume = soundTable.get("fxVolume").queryFloat(1.0f);
		float guiVolume = soundTable.get("guiVolume").queryFloat(1.0f);
		float musicVolume = soundTable.get("musicVolume").queryFloat(0.33f);
		int muted = soundTable.get("muted").queryInteger(0);
		int musicMuted = soundTable.get("musicMuted").queryInteger(0);

		SoundSubsystem* sound = (SoundSubsystem*)game->getSubsystem("Sound");
		if (sound)
		{
			sound->setMasterVolume(masterVolume);
			sound->setFXVolume(fxVolume);
			sound->setGUIVolume(guiVolume);
		
		

			if (muted > 0)
				sound->mute();
			else
				sound->unmute();

			if (musicMuted == 0)
				sound->getMusicPlayer()->mute();
				//sound->setMusicVolume(0.0f);
			else
			{
				sound->getMusicPlayer()->unmute();
			}

			sound->getMusicPlayer()->setVolume(musicVolume);
			int floatingCombatText = gameTable.get("floatingCombatText").queryInteger(0);
			int displayPlayerName = gameTable.get("displayPlayerName").queryInteger(0);
			int displayEnemyNames = gameTable.get("displayEnemyNames").queryInteger(0);
			int displayTeamMemberNames = gameTable.get("displayTeamMemberNames").queryInteger(0);

			if (arenaScene)
			{
		
				ArenaScene* s = (ArenaScene*)arenaScene;

				s->setShowCombatText(floatingCombatText == 1);
				s->setShowPlayerName(displayPlayerName == 1);
				s->setShowEnemyNames(displayEnemyNames == 1);
				s->setShowTeamNames(displayTeamMemberNames == 1);
				s->setShowStats(showStats == 1);
			}

			saveSettings();
			game->getScript()->execute("settings.txt");
		}

		// else if the file don't exist, create one 
		else
		{
			saveSettings();
			game->getScript()->execute("settings.txt");
		}

	}

}

void Client::saveSettings()
{
	std::filebuf fb;
	fb.open("settings.txt", std::ios::out);

	std::ostream file(&fb);

	EventLua::State script;
	EventLua::Object settings, graphicsTable, sound, gameTable;

	settings = script.newTable("Settings");

	settings.newTable("Graphics");

	settings.newTable("Sound");

	settings.newTable("Game");

	graphicsTable = settings.get("Graphics");

	sound = settings.get("Sound");

	gameTable = settings.get("Game");

	SoundSubsystem* s = (SoundSubsystem*)game->getSubsystem("Sound");

	if (s)
	{
		int muted = 0;
		if ( s->getAudioSystem()->isMuted() )
			muted = 1;
		else
			muted = 0;

		sound.set("masterVolume", s->getMasterVolume());
		sound.set("fxVolume", s->getFXVolume());
		sound.set("guiVolume", s->getGUIVolume());
		sound.set("musicVolume", s->getMusicPlayer()->getVolume());
		sound.set( "muted", muted );
		int musicMuted = 0;
		if (s->getMusicPlayer()->isMuted())
			musicMuted = 1;
		sound.set("musicMuted",musicMuted);
	}


	graphicsTable.set("width", width);
	graphicsTable.set("height", height);

	int ssao = 0;
	
	if ( renderModule->getSSAO() )
		ssao = 1;
	else
		ssao = 0;

	int shadows = 0;
	if ( renderModule->getShadows() )
		shadows = 1;
	else
		shadows = 0;

	int fog = 0;
	if ( renderModule->getFog() )
		fog = 1;
	else
		fog = 0;
	int showBoundingVolumes = 0;
	if (renderModule->getBoundingVolumesVisibility())
		showBoundingVolumes = 1;




	graphicsTable.set("ssao", ssao);
	graphicsTable.set("shadows", shadows);
	graphicsTable.set("fog", fog);
	graphicsTable.set("visualRange", renderModule->getVisualScale());
	graphicsTable.set("showBoundingVolumes", showBoundingVolumes);

	if (renderer.getFullscreen())
		graphicsTable.set("fullscreen", 1);

	else
		graphicsTable.set("fullscreen", 0);


	EventLua::Object oldSettings = game->getScript()->get("Settings");
	EventLua::Object oldGameTable = oldSettings.get("Game");
	EventLua::Object oldGraphicsTable = oldSettings.get("Graphics");

	int floatingCombatText = oldGameTable.get("floatingCombatText").queryInteger(1);
	int displayPlayerName = oldGameTable.get("displayPlayerName").queryInteger(0);
	int displayEnemyNames = oldGameTable.get("displayEnemyNames").queryInteger(1);
	int displayTeamMemberNames = oldGameTable.get("displayTeamMemberNames").queryInteger(1);
	int vsync = oldGameTable.get("vsync").queryInteger(0);

	graphicsTable.set("showStats", oldGraphicsTable.get("showStats").queryInteger(0));
	graphicsTable.set("vsync", vsync);
	
	

	gameTable.set("floatingCombatText", floatingCombatText);
	gameTable.set("displayPlayerName", displayPlayerName);
	gameTable.set("displayEnemyNames", displayEnemyNames);
	gameTable.set("displayTeamMemberNames", displayTeamMemberNames);

	file << settings;

	fb.close();
}

void Client::updateSettings()
{

	EventLua::Object settings, graphics, soundTable, gameTable;

	settings = game->getScript()->get("Settings");

	graphics = settings.get("Graphics");

	soundTable = settings.get("Sound");

	gameTable = settings.get("Game");

	width = graphics.get("width").queryInteger(1280);
	height = graphics.get("height").queryInteger(720);

	renderModule->getCamera()->setProjection3D(70, (float)width / (float)height);

	int ssao = graphics.get("ssao").queryInteger(0);
	int shadows = graphics.get("shadows").queryInteger(0);
	int fog = graphics.get("fog").queryInteger(0);
	int fullscreen = graphics.get("fullscreen").queryInteger(0);
	int showStats = graphics.get("showStats").queryInteger(0);
	int showBoundingVolumes = graphics.get("showBoundingVolumes").queryInteger(0);
	float visualScale = graphics.get("visualScale").queryFloat(0.0f);
	int vsync = graphics.get("vsync").queryInteger(0);

	renderModule->setSSAO(ssao == 1);
	renderModule->setShadows(shadows == 1);
	renderModule->setFog(fog == 1);
	renderModule->setVisualScale(visualScale);
	renderModule->setBoundingVolumesVisibility(showBoundingVolumes == 1);
	if (renderer.getFullscreen() && fullscreen == 0)
		renderer.switchWindowFullScreenMode();
	

	else if (!renderer.getFullscreen() && fullscreen == 1)
		renderer.switchWindowFullScreenMode();

	int muted = 0;

	float masterVolume = soundTable.get("masterVolume").queryFloat(0.8f);
	float fxVolume = soundTable.get("fxVolume").queryFloat(1.0f);
	float guiVolume = soundTable.get("guiVolume").queryFloat(1.0f);
	float musicVolume = soundTable.get("musicVolume").queryFloat(0.33f);
	
	muted = soundTable.get("muted").queryInteger(0);

	int musicMuted = soundTable.get("musicMuted").queryInteger(0);

	SoundSubsystem* sound = (SoundSubsystem*)game->getSubsystem("Sound");

	sound->setMasterVolume(masterVolume);
	sound->setFXVolume(fxVolume);
	sound->setGUIVolume(guiVolume);
	
	
	if (musicMuted == 0 || muted > 0)
		sound->getMusicPlayer()->mute();
	else
	{
		sound->getMusicPlayer()->unmute();
	}

	sound->getMusicPlayer()->setVolume(musicVolume);

	if (muted > 0)
		sound->mute();
	else
	{
		sound->unmute();
	}

	int floatingCombatText = gameTable.get("floatingCombatText").queryInteger(0);
	int displayPlayerName = gameTable.get("displayPlayerName").queryInteger(0);
	int displayEnemyNames = gameTable.get("displayEnemyNames").queryInteger(0);
	int displayTeamMemberNames = gameTable.get("displayTeamMemberNames").queryInteger(0);

	
	ArenaScene* s = (ArenaScene*)arenaScene;

	s->setShowCombatText(floatingCombatText == 1);
	s->setShowPlayerName(displayPlayerName == 1);
	s->setShowEnemyNames(displayEnemyNames == 1);
	s->setShowTeamNames(displayTeamMemberNames == 1);
	s->setShowStats(showStats == 1);

}

void Client::saveLevelData( const unsigned char* data, unsigned int bytes )
{
	levelData.size = bytes;
	levelData.data = New char[bytes];
	memcpy( levelData.data, data, bytes );

	FileSystem::write( levelName, &levelData );

	LevelFormat* level = New LevelFormat( game );
	level->importFromFile( levelData, levelName );
	delete level;
}

const File* Client::getLevelData()
{
	return &levelData;
}

int Client::getFps() const
{
	return fps;
}

float Client::getNetLagg() const
{
	return net_lagg;
}

float Client::getUpdateTime() const
{
	return profile_update;
}

float Client::getRenderTime() const
{
	return profile_render;
}

float Client::getPresentTime() const
{
	return profile_present;
}

float Client::getWinNetTime() const
{
	return profile_winnet;
}

unsigned int Client::getBytesIn() const
{
	return bytes_in;
}

unsigned int Client::getBytesOut() const
{
	return bytes_out;
}

void Client::initDirect2D()
{
	HRESULT hr = S_OK;
	IDXGIResource * otherRes(NULL);
	hr = font.getSurface()->QueryInterface(__uuidof(IDXGIResource), (void**)&otherRes);

	HANDLE sharedHandle;
	hr = otherRes->GetSharedHandle(&sharedHandle);

	ID3D11Resource * res11 = NULL;

	renderer.getDevice()->OpenSharedResource(sharedHandle, __uuidof(ID3D11Resource), (void**)(&res11)); 

	mutex = NULL;
	if (SUCCEEDED(res11->QueryInterface(__uuidof(IDXGIKeyedMutex), (void**)&mutex)))
	{
		font.setSharedMutex(mutex);
	}
	
	direct2dtexture = New Texture();
	direct2dtexture->bind(&renderer, res11);

	me->fontColor = Color( 1.0f, 1.0f, 1.0f, 1.0f );
}

Client * Client::me = NULL;
Color Client::fontColor = Color( 1.0f, 1.0f, 1.0f, 1.0f );

void Client::setBlendColor(float r, float g, float b, float a)
{
	me->renderModule->setBlendColor(Color(r, g, b, a));
}

void Client::drawSprite(const char * texture, float x, float y, float width, float height)
{
	me->renderModule->setDrawTexture(texture);
	me->renderModule->drawSprite(x, y, width, height);
}

void Client::drawSpriteAdd(const char * texture, float x, float y, float width, float height)
{
	me->renderModule->setDrawTexture(texture);
	me->renderModule->drawSpriteAdd(x, y, width, height);
}

void Client::drawSpriteRot(const char * texture, float x, float y, float width, float height, float rot)
{
	me->renderModule->setDrawTexture(texture);
	me->renderModule->drawSpriteRot(x, y, width, height, rot);
}

void Client::drawShinySprite(const char * texture, float x, float y, float width, float height, float shininess)
{
	me->renderModule->setDrawTexture(texture);
	me->renderModule->drawShinySprite(x, y, width, height, shininess);
}

void Client::drawShine(float x, float y, float size)
{
	me->renderModule->drawShine(x, y, size);
}

void Client::drawShineExt(const char * texture, float x, float y, float size)
{
	me->renderModule->drawShineExt(texture, x, y, size);
}

// No, I'm not proud of this one.
void Client::drawBorder( const char * texture, float x, float y, float width, float height )
{
	drawBorderEx(texture, x, y, width, height);
}

void Client::drawBorderEx(const char * texture, float x, float y, float width, float height)
{

	x -= 2;
	y -= 2;
	width += 4;
	height += 4;

	float corner = 16;

	// shrink corner if width and height is too small
	if (corner > width / 2)
		corner = width / 2;

	if (corner > height / 2)
		corner = height / 2;

	me->renderModule->setDrawTexture(texture);

	// top left
	me->renderModule->drawSpritePart(x, y, corner, corner, 0, 0, -0.66f, -0.66f);

	// top
	me->renderModule->drawSpritePart(x + corner, y, width - corner * 2, corner, 0.33f, 0, -0.33f, -0.66f);

	// top right
	me->renderModule->drawSpritePart(x + width - corner, y, corner, corner, 0.66f, 0, 0, -0.66f);

	// middle left
	me->renderModule->drawSpritePart(x, y + corner, corner, height - corner * 2, 0, 0.33f, -0.66f, -0.33f);

	// center
	me->renderModule->drawSpritePart(x + corner, y + corner, width - corner * 2, height - corner * 2, 0.33f, 0.33f, -0.33f, -0.33f);


	// middle right
	me->renderModule->drawSpritePart(x + width - corner, y + corner, corner, height - corner * 2, 0.66f, 0.33f, 0, -0.33f);


	// bottom left
	me->renderModule->drawSpritePart(x, y + height - corner, corner, corner, 0, 0.66f, -0.66f, 0);

	// bottom
	me->renderModule->drawSpritePart(x + corner, y + height - corner, width - corner * 2, corner, 0.33f, 0.66f, -0.33f, 0);

	// bottom right
	me->renderModule->drawSpritePart(x + width - corner, y + height - corner, corner, corner, 0.66f, 0.66f, 0, 0);
}

void Client::drawBar(const char * texture, float x, float y, float width, float height, float val)
{
	// draw the progress
	x -= 2;
	y -= 2;
	width += 4;
	height += 4;

	val = std::min(std::max(val, 0.0f), 1.0f);

	float corner = 16;

	// shrink corner if width and height is too small
	if (corner > width / 2)
		corner = width / 2;

	if (corner > height / 2)
		corner = height / 2;

	me->renderModule->setDrawTexture(texture);


	float real_width = width;
	width = val * width;


	float left_corner = std::min(width, corner);
	float left_per = (1 - left_corner / corner) * 0.33f;

	// top left
	me->renderModule->drawSpritePart(x, y, left_corner, corner, 0, 0, -0.66f - left_per, -0.66f);

	// bottom left
	me->renderModule->drawSpritePart(x, y + height - corner, left_corner, corner, 0, 0.66f, -0.66f - left_per, 0);

	// middle left
	me->renderModule->drawSpritePart(x, y + corner, left_corner, height - corner * 2, 0, 0.33f, -0.66f - left_per, -0.33f);

	
	if (width > corner)
	{
		float middle_width = std::min(width - corner, real_width - corner * 2);

		// top
		me->renderModule->drawSpritePart(x + corner, y, middle_width, corner, 0.33f, 0, -0.33f, -0.66f);

		// center
		me->renderModule->drawSpritePart(x + corner, y + corner, middle_width, height - corner * 2, 0.33f, 0.33f, -0.33f, -0.33f);
	
		// bottom
		me->renderModule->drawSpritePart(x + corner, y + height - corner, middle_width, corner, 0.33f, 0.66f, -0.33f, 0);
	}
	

	float right_corner = std::max(width - (real_width - corner), 0.0f);
	float right_per = (1 - right_corner / corner) * 0.33f;

	
	if (width > real_width - corner)
	{

		// top right
		me->renderModule->drawSpritePart(x + real_width - corner, y, right_corner, corner, 0.66f, 0, 0 - right_per, -0.66f);

		// middle right
		me->renderModule->drawSpritePart(x + real_width - corner, y + corner, right_corner, height - corner * 2, 0.66f, 0.33f, 0 - right_per, -0.33f);

		// bottom right
		me->renderModule->drawSpritePart(x + real_width - corner, y + height - corner, right_corner, corner, 0.66f, 0.66f, 0 - right_per, 0);
	}
	

	// draw the top bar
	setBlendColor(1, 1, 1, 1);
	drawBorderEx("border_empty.png", x + 2, y + 2, real_width - 4, height - 4);
}

void Client::drawLine(float x1, float y1, float x2, float y2)
{
	me->renderModule->drawLine(x1, y1, x2, y2);
}

bool Client::setFont(const char * font)
{
	return me->font.createFont(font);
}

void Client::setTextAlignment( int align )
{
	me->font.setTextAlignment( align );
}

void Client::setParagraphAlignment( int align )
{
	me->font.setParagraphAlignment( align );
}

void Client::setFontColor( float r, float g, float b, float a )
{
	me->fontColor = Color( r, g, b, a );
}

void Client::drawString(const char * str, float x, float y)
{
	if (!str)
		return;

	D3DXCOLOR color;
	color.r = me->fontColor.r;
	color.g = me->fontColor.g;
	color.b = me->fontColor.b;
	color.a = me->fontColor.a;

	me->font.draw(str, x, y, 9000, 9000, color);
}

void Client::drawStringArea(const char * str, float x, float y, float width, float height)
{
	if (!str)
		return;

	D3DXCOLOR color;
	color.r = me->fontColor.r;
	color.g = me->fontColor.g;
	color.b = me->fontColor.b;
	color.a = me->fontColor.a;

	me->font.draw(str, x, y, width, height, color);
}

float Client::getStringHeight(const char * str, float width)
{
	float w;
	static float h;

	me->font.getSize( str, &w, &h, width );

	return h;
}

EventLua::Object Client::getStringMetrics( const char* str )
{
	std::string name = "TEMP_TABLE_GETSTRINGMETRICS";
	EventLua::Object rtn = me->game->getScript()->newTable( name.c_str() );

	float w;
	float h;

	me->font.getSize( str, &w, &h );

	rtn["w"] = w;
	rtn["h"] = h;

	return rtn;
}

void Client::gotoScene( const char* name )
{
	me->game->gotoSceneOnUpdate(name);
	//me->game->gotoSceneUsingTransition(name, New BlurTransition(&me->renderModule), 30);
}

void Client::gotoSceneUsingTransition(const char* name, int effect, int time)
{
	//me->game->gotoSceneOnUpdate(name);
	me->game->gotoSceneUsingTransition(name, New BlurTransition(me->renderModule), time);
}

void Client::activateScene( const char* name )
{
	me->game->activateSceneOnUpdate(name);
}

void Client::deactivateScene( const char* name )
{
	me->game->deactivateSceneOnUpdate(name);
}

const char* Client::getUserName()
{
	static std::string username = "";
	username = me->user_name;
	return username.c_str();
}

int Client::online = 0;
int Client::getOnlineStatus()
{
	return me->online;
}

bool Client::isSpectator()
{
	Object * player = me->game->getControlledObject();
	if (player)
	{
		CoreComponent * cc = player->findComponent<CoreComponent>();
		if (cc)
		{
			return cc->team == 0;
		}
	}

	return false;
}

void Client::logoff()
{
	if( me->online > 0 )
	{
		MasterSendAsync request;

		DropUserPacket packet;
		packet.uid = me->user_id;
		request(packet, sizeof(packet));

		me->online = 0;
		me->user_id = 0;
	}
}

void Client::exit()
{
	me->close();
}

void Client::say( const char * msg )
{
	std::stringstream buffer;

	buffer << "0:-1:" << me->user_name << ":" << msg;

	me->chat->addMessage( buffer.str() );
}

void Client::spawnHudEffect(float x, float y, int type)
{
	if (type == 0)
		me->renderModule->spawnEffect("effects/hud_animation.paf", Vector3(x, y, 0), Vector3(0, 1, 0));
	else
		me->renderModule->spawnEffect("effects/electric_hit3.paf", Vector3(x, y, 0), Vector3(0, 1, 0));
}

EventLua::Object Client::getStats()
{
	std::stringstream ss;
	ss << "TEMP_TABLE_GETSTATS";
	EventLua::Object rtn = me->game->getScript()->newTable(ss.str().c_str());

	if( me->gameStats )
	{
		rtn["time"] = me->gameStats->gameTimeStr.c_str();
		rtn["minutes"] = me->gameStats->gameMins;
		rtn["seconds"] = me->gameStats->gameSecs;
		rtn["kills1"] = me->gameStats->kills[1];
		rtn["kills2"] = me->gameStats->kills[2];
		rtn["gameStarted"] = me->gameStats->gameStarted;
		rtn["allReady"] = me->gameStats->allReady;
	}

	return rtn;
}

void Client::fileCallback(const std::string & file, void * userdata)
{
	std::cout << "resource * " << file << std::endl;
}


bool Client::saveKeybindsLua(EventLua::Object file)
{
	if (file)
	{
		std::string name = file.queryString("keybinds.lua");
		return saveKeybinds(name);
	}
	return saveKeybinds();
}

bool Client::loadKeybindsLua(EventLua::Object file)
{
	if (file)
	{
		std::string name = file.queryString("keybinds.lua");
		return loadKeybinds(name);
	}
	return loadKeybinds();
}

bool Client::saveKeybinds(std::string fname)
{
	File file;
	std::string buffer = me->game->getLogic()->getEventHandler()->generateKeybindsFile();

	file.size = buffer.size();
	file.data = new char[file.size];
	buffer._Copy_s( file.data, file.size, buffer.size() );
	
	FileSystem::write( fname, &file );

	return true;
}

bool Client::loadKeybinds(std::string fname)
{
	File file;
	if (!FileSystem::get(fname, &file))
	{
		if (fname == "defaultKeybinds.lua")
			return false;
		return loadKeybinds("defaultKeybinds.lua");
	}

	LogicHandler * logic = me->game->getLogic();
	logic->getScript()->execute(file.data, file.size);

	if( !logic->getScript()->execute(file.data, file.size) )
	{
		std::cout << logic->getScript()->getLastError() << ". In file: " << fname <<std::endl;
		return false;
	}
	return true;
}

void Client::processLevelDataResponsePacket(LevelDataResponsePacket & packet)
{
	levelName = packet.level_name;
	FileSystem::get( levelName, &levelData );

	if( levelData.size != packet.level_size )
	{
		ClientInterface * net = game->getNetwork();
		LevelRequest* level = New LevelRequest( net->getLowFreqService(), net->getRemoteIp(), this );
	}
	else
	{
		LevelFormat* level = New LevelFormat( game );
		level->importFromFile( levelData, levelName );
		Delete level;
	}

	std::cout << "level done" << std::endl;
}

void Client::processAuthResultPacket(AuthResultPacket & packet)
{
	if( packet.state == 1 )
	{
		setUser( packet.uid, packet.name );
		setOnlineStatus( 1 );
	}
}

void Client::processAchievementListPacket(RawPacket & packet)
{
}

void Client::onPacketStatic(RawPacket & packet)
{
	switch( packet.header.id )
	{
	case AUTH_RESULT_PACKET:

		me->processAuthResultPacket(*reinterpret_cast < AuthResultPacket* > (packet.data));
		break;

	case ACHIEVEMENT_LIST_PACKET:

		me->processAchievementListPacket(packet);
		break;

	default:

		me->onPacket(packet);
		break;
	}
}