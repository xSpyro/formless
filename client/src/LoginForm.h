#ifndef LOGINFORM__H
#define LOGINFORM__H

#include <string>

class Font2D;
class InputBox;

class LoginForm
{
public:
	LoginForm( Font2D* font = NULL );
	~LoginForm();

	void onInput( int key, int state );
	void onClick( float x, float y );
	void render();
	void clear();

	void getData( std::string& user, std::string& pass );

public:
	static void setUserPos( float x, float y, float w, float h );
	static void setPassPos( float x, float y, float w, float h );
	//static void select( float mouseX, float mouseY );

private:
	static LoginForm* me;

private:
	InputBox* username;
	InputBox* password;
};

#endif
