#ifndef LISTBOX__H
#define LISTBOX__H

#include "TextBox.h"

class Graphics;

class ListBox : public TextBox
{
	typedef std::map< std::string, std::string > ListData;

public:
	ListBox( Font2D* font, Graphics* graphics, void (*callback)(const std::string &) = NULL );
	virtual ~ListBox();

	void			onInput( int key, int state );
	void			onClick( float x, float y );
	void			bindCallback( void (*callback)(const std::string &) );

	void			addItem( const std::string & item, const std::string & data );
	void			setSelectedItem( const std::string & item, const std::string & data );
	std::string		getSelectedItem();
	unsigned int	getSize();

	bool			selectItem( const std::string & str );

	void			update();
	void			render();

private:
	void			updateBuffer();

private:
	unsigned int	selectedLine;
	unsigned int	timer;
	ListData		listData;
	Graphics*		graphics;

	void			(*callback)(const std::string &);
};

#endif
