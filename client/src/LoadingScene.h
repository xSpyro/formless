
#ifndef LOADING_SCENE_H
#define LOADING_SCENE_H

#include "Scene.h"
#include "Framework.h"

class Font2D;

class LoadingScene : public Scene
{
public:

	LoadingScene();
	LoadingScene(GameHandler * game, Renderer* rend, Window* wndw, Font2D* font = NULL, Client * client = NULL);
	virtual ~LoadingScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	void onRender2D();
	struct Stage2D : public Stage
	{
		LoadingScene * me;
		bool visible;

		Stage2D(LoadingScene * me) : me(me)
		{
		}

		void apply()
		{
			if(visible)
				me->onRender2D();
		}
	};

public:

private:
	//For lua
	static LoadingScene* me;

private:
	void buildLayers();
	void clearLayers();

	unsigned int loadOffset;

	Stage2D *	layer2d;

	Layer *		layer;
	Camera		camera;

	Timer		luaTimer;

	GameHandler * game;
	Client * client;
};

#endif
