
#ifndef INTRO_SCENE_H
#define INTRO_SCENE_H

#include "Scene.h"
#include "Framework.h"

class IntroScene : public Scene
{
public:

	IntroScene();
	IntroScene(GameHandler * game, Renderer* rend, Window* wndw);
	virtual ~IntroScene();

	void update();
	void render();

	void onEnter();
	void onLeave();


	void onRender2D();
	struct Stage2D : public Stage
	{
		IntroScene * me;

		Stage2D(IntroScene * me) : me(me)
		{
		}

		void apply()
		{
			me->onRender2D();
		}
	};

private:
	//For lua
	static IntroScene* me;

private:
	void buildLayers();
	void clearLayers();

	unsigned int next;

	GameHandler * game;

	Stage2D *	stage;
	Camera		camera;
		
	Timer		luaTimer;
};

#endif