
#ifndef STARTUP_SCENE_H
#define STARTUP_SCENE_H

#include "Scene.h"

//#include "Framework.h"

#include "ParticleStream.h"



class Graphics;

class StartupScene : public Scene
{
public:

	StartupScene();
	StartupScene(LogicHandler* handler, Renderer* rend, Window* wndw);
	virtual ~StartupScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	bool onCreateObject(Object* object);	//Do not call this manually!
	bool onDestroyObject(Object* object);	//Do not call this manually!

	void onOverlay();

	void onTest();

private:
	void buildLayers();
	void clearLayers();

	struct Overlay : public Stage
	{
		StartupScene * me;

		Overlay(StartupScene * me) : me(me)
		{
		}

		void apply()
		{
			me->onOverlay();
		}
	};

	struct Test : public Stage
	{
		StartupScene * me;

		Test(StartupScene * me) : me(me)
		{
			
		}

		void apply()
		{
			me->onTest();
		}
	};

	Viewport * viewport;

	Overlay * layer;
	Test * test;

	Layer * debug;
	Layer * particles;

	Camera camera;

	Graphics * graphics;

	ParticleStream stream;

	Timer timer;
};

#endif