
#include "FadeTransition.h"

#include "RenderModule.h"

FadeTransition::FadeTransition(RenderModule * renderModule)
	: renderModule(renderModule)
{
	power = 1.0f;
}

FadeTransition::~FadeTransition()
{

}

void FadeTransition::onBegin(int frames)
{
	this->frames = frames;

	frame = 0;
}

void FadeTransition::onFadeChange()
{
	// render a single iteration of the pipeline

	frame = 0;
}

void FadeTransition::onFadeIn()
{
	++frame;

	float alpha = std::pow(frame / static_cast < float > (frames), power);

	renderModule->setBlendColor(Color(1, 1, 1, 1 - alpha));
	renderModule->setDrawTexture(renderModule->getOffscreenTexture());
	renderModule->drawSpriteForceScreen(0, 0, 1280, 720);
}

void FadeTransition::onFadeOut()
{
	++frame;

	float alpha = -std::pow((frame - frames) / static_cast < float > (frames), power);

	renderModule->setBlendColor(Color(1, 1, 1, 1 - alpha));
	renderModule->setDrawTexture(renderModule->getOffscreenTexture());
	renderModule->drawSpriteForceScreen(0, 0, 1280, 720);
}
