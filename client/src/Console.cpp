#include "Console.h"

#include "Font2D.h"
#include "TextBox.h"
#include "InputBox.h"

#include "MemLeaks.h"

Console::Console( Font2D* font, EventLua::State* lua )
	: font( font ), lua( lua )
{
	me		= this;
	active	= false;
	log		= New TextBox( font );
	input	= New InputBox( font );

	log->setPos( 0, 0, 1280, 400 );
	//log->setColor( 1, 1, 0, 1 );
	log->setActive( active );
	input->setPos( 0, 400, 1280, 40 );
	//input->setColor( 1, 1, 0, 1 );
	input->setLabel( "Lua>" );
	input->setActive( active );

	input->bindCallback( execute );

	EventLua::Object g = lua->getGlobals();
	g.bind("cprint", &cprint);
	g.bind("setConsoleColor", &setColor);
}

Console::~Console()
{
	if( log )
		delete log;
	if( input )
		delete input;
}

void Console::update()
{
	log->update();
}

void Console::render()
{
	if( active )
	{
		input->render();
		log->render();
	}
}

void Console::onInput( int key, int state )
{
	if( active )
	{
		log->onInput( key, state );
		input->onInput( key, state );
	}
}

void Console::setActive( bool status )
{
	active = status;

	log->setVisible( status );
	log->setActive( status );
	input->setVisible( status );
	input->setActive( status );
}

bool Console::isActive()
{
	return active;
}

Console* Console::me = NULL;

void Console::execute( const std::string & cmd )
{
	std::string tmp = cmd;
	if (cmd.size() > 0 && cmd.at(0) == '=')
	{
		tmp = "print( "+ cmd.substr(1) + ")";
		if( !me->lua->execute( tmp.c_str(), tmp.size() ) )
		{
			std::string error = me->lua->getLastError();
			std::cout << error << std::endl;
			me->log->addMessage( error );
		}
	}
	else if( !me->lua->execute( tmp.c_str(), tmp.size() ) )
	{
		std::string error = me->lua->getLastError();
		std::cout << error << std::endl;
		me->log->addMessage( error );
	}
	me->input->clear();
}

void Console::cprint( const char * str )
{
	me->log->addMessage( str );
	//std::cout << "cprint: " << str << std::endl;
}

void Console::setColor( float r, float g, float b, float a )
{
	me->log->setColor( r, g, b, a );
	me->input->setColor( r, g, b, a );
}
