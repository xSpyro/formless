#include "ClientInterface.h"
#include <iostream>
#include "Chat.h"
#include "TextBox.h"
#include "InputBox.h"
#include "GameHandler.h"
#include "CommonPackets.h"
#include "NetworkSubsystem.h"

Chat* Chat::me = NULL;
Chat::Chat( GameHandler* gh, Font2D* f, NetworkSubsystem* n )
{
	me				= this;
	game			= gh;
	font			= f;
	net				= n;
	active			= true;
	team			= 0;
	input			= new InputBox( f );
	log				= new TextBox( f );

	channels[CHAT_CHANNEL_NONE] = "";
	channels[CHAT_CHANNEL_ALL]	= "[To All]";
	channels[CHAT_CHANNEL_TEAM] = "[To Team]";
	currentChannel				= CHAT_CHANNEL_ALL;

	input->setLabel( channels[currentChannel] + ": " );

	EventLua::Object g = game->getScript()->getGlobals();
	g.bind( "setChatLog", &setLogPos );
	g.bind( "setChatInput", &setInputPos );
	g.bind( "chatActive", &getActive );
	g.bind( "activateChat", &setActive );
}

Chat::~Chat()
{
}

void Chat::update()
{
	if( net )
	{
		std::vector<std::string> messages;
		net->queryTextBuffer( messages );
		for( std::vector<std::string>::iterator it = messages.begin(); it != messages.end(); ++it )
		{
			addMessage( *it );
		}
	}

	log->update();
}

void Chat::render()
{
//	if( active )
//	{
		log->render();
		input->render();
//	}
}

void Chat::onInput( int key, int state )
{
	if( active && (state != 0) )
	{
		switch( key )
		{
		case VK_TAB:
			setChannel( (ChatEnum)((currentChannel + 1) % CHAT_CHANNELS) );
			break;
		case VK_RETURN:
			{
				std::string buffer = input->getText();
				if( buffer.size() > 0 )
				{
					TextPacket packet;

					packet.textlen = 255;
					if( buffer.size() < 255 )
						packet.textlen = buffer.size();
					memcpy( packet.text, buffer.c_str(), packet.textlen );
					packet.text[packet.textlen] = '\0';

					packet.channel = currentChannel;

					if( currentChannel == CHAT_CHANNEL_TEAM )
						packet.team = team;
					else
						packet.team = 0;

					if( game )
					{
						game->getNetwork()->send(packet);
					}

					clear();
				}
			}
			break;
		case MB_LEFT:
			break;
		default:
			break;
		}
	}

	log->onInput( key, state );
	input->onInput( key, state );
}

void Chat::addMessage( std::string msg )
{
	std::stringstream buf(msg);

	// Extract channel
	int c;
	buf >> c;
	buf.ignore();

	// Extract team
	int t;
	buf >> t;
	buf.ignore();

	// TODO: Fix this. Opposing team's messages shouldn't be sent this far.
	if( c == CHAT_CHANNEL_TEAM && t != team )
		return;

	// Clear the message
	int pos = msg.find_first_of( ':', 0 );
	pos = msg.find_first_of( ':', pos + 1 );
	msg.erase( 0, pos + 1 );
	
	// Extract the name (in case we want to process it) and clear message
	pos = msg.find_first_of( ':', 0 );
	std::string n = msg.substr( 0, pos );
	msg.erase( 0, pos + 1 );

	// Put back name and channel, but as a string
	msg.insert( 0, n + ": " );
	msg.insert( 0, channels[c] + " " );

	log->addMessage( msg );

	if( !log->isVisible() )
	{
		log->setVisible( true );
		log->fadeOut( 3 );
	}
}

void Chat::clear()
{
	input->clear();
}
void Chat::clearLog()
{
	log->clear();
	input->clear();
}

void Chat::setTeam( unsigned int id )
{
	team = id;
}

bool Chat::active = true;
void Chat::setActive( bool status )
{
	me->active = status;

	me->log->setVisible( status );
	me->log->setActive( status );

	//if( status == true )
	//{
	//	//me->log->setVisible( status );
	//	me->log->fadeIn();
	//	me->log->setActive( status );
	//}
	//else
	//{
	//	//me->log->setVisible( status );
	//	me->log->fadeOut();
	//	me->log->setActive( status );
	//}
	me->input->setVisible( status );
	me->input->setActive( status );
}

bool Chat::getActive()
{
	return active;
}

void Chat::setChannel( ChatEnum id )
{
	Channels::iterator it = channels.find( id );
	if( it != channels.end() )
	{
		currentChannel = id;
		input->setLabel( channels[currentChannel] + ": " );
	}
}

void Chat::setLogPos( float x, float y, float w, float h )
{
	me->log->setPos( x, y, w, h );
}

void Chat::setInputPos( float x, float y, float w, float h )
{
	me->input->setPos( x, y, w, h );
}
