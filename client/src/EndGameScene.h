
#ifndef ENDGAMESCENE_H
#define ENDGAMESCENE_H

#include "Scene.h"
#include "Framework.h"

class Font2D;
class NetworkSubsystemClient;
class GameHandler;
struct Statistics;

class EndGameScene : public Scene
{
public:

	EndGameScene();
	EndGameScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler* game = NULL, Font2D * font = NULL);
	virtual ~EndGameScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	void onRender2D(Font2D * font);
	struct Stage2D : public Stage
	{
		EndGameScene * me;
		Font2D * font;
		bool visible;

		Stage2D(EndGameScene * me, Font2D* font) : me(me), font(font)
		{
		}

		void apply()
		{
			if(visible)
				me->onRender2D(font);
		}
	};

private:
	//For lua
	static EndGameScene* me;

private:
	void buildLayers();
	void clearLayers();

	Stage2D *	layer2d;

	Layer *		layer;
	Camera		camera;

	Timer		luaTimer;

	NetworkSubsystemClient*	network;
	GameHandler*			game;
	Statistics*				stats;

	Object*		player;
};

#endif