#include "ClientInterface.h"
#include "Client.h"

#include "RequestInterface.h"
#include "CommonPackets.h"

#include "ChooseGameScene.h"
#include "LogicHandler.h"
#include "GameHandler.h"

#include "ListBox.h"
#include "InputBox.h"

#include "MemLeaks.h"

#include "ServerInfo.h"
#include "RenderComponent.h"
#include "AnimationComponent.h"
#include "Object.h"

ChooseGameScene::ChooseGameScene()
	: Scene()
{

}

ChooseGameScene::ChooseGameScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler* game, Font2D* font, Graphics* graphics)
	: Scene(handler, rend, wndw), game(game)
{
	me = this;
	layer = renderer->createLayer();

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);
	camera.setProjection3D(45, 16 / 9.0f);

	// camera set

	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));

	layer2d = New Stage2D(this, font);
	layer2d->visible = true;

	list = New ListBox( font, graphics );
	list->setPos( 10 + 5, 10 + 12, 500 - 10, 700 - 24 );
	list->setVisible( true );
	list->setActive( true );
	list->bindCallback( joinServer );

	srvName = New InputBox( font );
	srvName->setPos( 530 + 8, 365, 400-16, 40 );
	srvName->setVisible( true );
	//srvName->setActive( false );
	srvName->setMaxChars( 32 );

	File srvFile;
	FileSystem::get("server.cfg", &srvFile);

	if( parent->getScript()->execute(srvFile.data, srvFile.size) )
	{
		EventLua::Object srvcfg = parent->getScript()->get("server");
		srvName->setText( srvcfg.get("name").queryString("Formless server") );
	}
	else
	{
		srvName->setText( "Formless server" );
	}

	// register some global functions to lua
	EventLua::Object g = parent->getScript()->getGlobals();
	g.bind( "refreshGameList", &refreshGameList );
	g.bind( "connectToServer", &connect );
	g.bind( "createServer", &createServer );

	File file;
	FileSystem::get("scripts/choosegame.lua", &file);
	parent->getScript()->execute(file.data, file.size);

	warningText = "";
}

ChooseGameScene::~ChooseGameScene()
{
	delete layer2d;
}

void ChooseGameScene::update()
{
	camera.update();
	list->update();
}

void ChooseGameScene::render()
{

}

void ChooseGameScene::onEnter()
{
	//renderer->insertLayer(10, layer);
	//renderer->insertStage(1000, layer2d);
	buildLayers();

	refreshGameList();

	parent->getScript()->getGlobals()["onChooseGameEnter"]();

	warningText = "";

	luaTimer.restart();
}

void ChooseGameScene::onLeave()
{
	//renderer->clearStage(10);
	//renderer->clearStage(1000);
	clearLayers();

	parent->getScript()->getGlobals()["onChooseGameLeave"]();
}

void ChooseGameScene::onFocus()
{
	//renderer->insertLayer(10, layer);
	//renderer->insertStage(1000, layer2d);
	buildLayers();
	layer2d->visible = true;
	/*File file;
	FileSystem::get("scripts/choosegame.lua", &file);
	if (!parent->getScript()->execute(file.data, file.size))
		std::cout<< parent->getScript()->getLastError()<<std::endl;

	me->parent->getScript()->getGlobals()["onChooseGameFocus"]();*/
}

void ChooseGameScene::onFocusLost()
{
	layer2d->visible = false;
	//me->parent->getScript()->getGlobals()["onChooseGameFocusLost"]();
}

bool ChooseGameScene::onInput(int sym, int state)
{
	if(state != 0)
	{
		if( sym == VK_RETURN )
		{
			if( list->isActive() )
			{
				connect();
			}
		}

		if( sym == MB_LEFT )
		{
			list->onClick( (float)mx, (float)my );
			srvName->onClick( (float)mx, (float)my );
		}
	}

	list->onInput( sym, state );
	srvName->onInput( sym, state );

	if( layer2d->visible )
		parent->getScript()->getGlobals()["onChooseGameInput"](sym, state);

	return true;
}

bool ChooseGameScene::onMouseMovement(int x, int y)
{
	Scene::onMouseMovement( x, y );
	return true;
}

bool ChooseGameScene::onCreateObject(Object* object)
{
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();

	if(rendcomp)
	{
		rendcomp->load(renderer);
		layer->add(&rendcomp->model);
	}

	AnimationComponent* animcomp = object->findComponent<AnimationComponent>();

	if (animcomp)
	{
		animcomp->load(renderer, &camera);
	}

	return true;
}

bool ChooseGameScene::onDestroyObject(Object* object)
{
	return true;
}

void ChooseGameScene::onConnected(unsigned int id)
{
	game->gotoSceneUsingLua( "TeamSelection" );
}

void ChooseGameScene::requestGameInfo()
{
	std::string server = list->getSelectedItem();
	// Request server info here...
}

void ChooseGameScene::joinServer( const std::string & ip )
{
	std::cout << "Joining server: " << ip << std::endl;
	if( !me->game->getNetwork()->isConnected() )
	{
		if( !me->game->getNetwork()->connect( ip.c_str() ) )
		{
			me->game->getNetwork()->disconnect();
		}
		else
		{
			MasterSendAsync async;
			ServerJoinPacket packet;
			packet.user_id = ((Client*)me->window)->getUserID();
			packet.setServer( ip );
			async( packet, sizeof(packet) );
		}
	}
}

void ChooseGameScene::onRender2D(Font2D* font)
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/choosegame.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout<< parent->getScript()->getLastError();
	}
	parent->getScript()->getGlobals()["onChooseGameRender"]();
	list->render();
	srvName->render();

	/*
	if( !warningText.empty() )
	{
		float fontW;
		float fontH;

		font->createFont("Centuary:30");

		font->getSize(warningText, &fontW, &fontH);

		font->draw(warningText.c_str(), 710, 580, fontW + 1.0f, fontH + 1.0f, D3DXCOLOR(1, 1, 1, 1));

		font->createFont("Centuary:20");
	}
	*/
}

void ChooseGameScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer(10, layer);
	renderer->insertStage(1000, layer2d);
}

void ChooseGameScene::clearLayers()
{
	renderer->clearStage(10);
	renderer->clearStage(1000);
}

void ChooseGameScene::refreshList()
{
	MasterRequest request;

	QueryServerListPacket packet;
	RawPacket result;

	try
	{
		if (request(packet, sizeof(packet), result))
		{
			list->clear();

			char * offset = &reinterpret_cast < char* > (result.data)[RawPacket::HeaderSize];

			unsigned int results = *reinterpret_cast < unsigned int* > (offset);

			offset += sizeof(unsigned int); // skip the int

			for (unsigned int i = 0; i < results; ++i)
			{
				ServerInfo * srv = reinterpret_cast < ServerInfo* > (offset);

				std::string name = srv->getName();

				if( !name.empty() )
					list->addItem( name, srv->getIp() );
				else
					list->addItem( srv->getIp(), srv->getIp() );

				offset += sizeof(ServerInfo);
			}
		}

	}
	catch ( std::exception e )
	{
	}
}

ChooseGameScene * ChooseGameScene::me = NULL;

void ChooseGameScene::refreshGameList()
{
	me->refreshList();
}

void ChooseGameScene::connect()
{
	std::string server = me->list->getSelectedItem();
	me->joinServer( server );
}

void ChooseGameScene::createServer()
{
	std::string name = me->srvName->getText();

	if( !name.empty() )
		name = "\"" + name + "\"";

	 //Open up a local server.
	int res = (int)ShellExecute(NULL, "open", "server.exe", name.c_str(), "", SW_SHOWNORMAL);
	if (res == 2)
	{
		//If it's not found, try to explicitly open a release server
		res = (int)ShellExecute(NULL, "open", "..\\Release\\server.exe", name.c_str(), "", SW_SHOWNORMAL);
		if (res == 2)
		{
			//If a release server couldn't be found, open a debug server
			res = (int)ShellExecute(NULL, "open", "..\\Debug\\server.exe", name.c_str(), "", SW_SHOWNORMAL);
		}
	}
	if( res > 32 )
	{
		me->joinServer("127.0.0.1");
	}
}
