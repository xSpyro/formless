

#include "ParticleDemoScene.h"

#include "Object.h"
#include "AnimationComponent.h"
#include "AnimationSubsystem.h"

#include "GameHandler.h"

/*

ParticleDemoScene::ParticleDemoScene()
{
}

ParticleDemoScene::ParticleDemoScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game, Chat* chat)
	: Scene(handler, rend, wndw), game(game)
{

}

ParticleDemoScene::~ParticleDemoScene()
{
}

void ParticleDemoScene::update()
{
	camera.setPosition(Vector3(0, 20.0f, -50));
	camera.setLookAt(Vector3(0, 10.0f, 0));

	renderModule.update();
}

void ParticleDemoScene::render()
{
	renderModule.preFrame();
}

void ParticleDemoScene::onEnter()
{

	renderModule.init(renderer, &camera, RM_PARTICLES | RM_SKY);
	renderModule.build();

	camera.setClipRange(10, 600);
	camera.setProjection3D(45, 16 / 9.0f);

	renderModule.setPlayerPosition(0);
	renderModule.setCursorPosition(0);
	renderModule.bind();

	game->createObjectExt("Player", Vector3(0, 0, 0), 0);
}

void ParticleDemoScene::onLeave()
{
}

bool ParticleDemoScene::onInput(int sym, int state)
{
	return false;
}

bool ParticleDemoScene::onMouseMovement(int x, int y)
{
	return false;
}

void ParticleDemoScene::onCreateObject(Object* object)
{
	AnimationComponent* animcomp = object->findComponent<AnimationComponent>();

	if (animcomp)
	{
		renderModule.addLinkedParticle(animcomp);
	}
}

void ParticleDemoScene::onDestroyObject(Object* object)
{
}

*/