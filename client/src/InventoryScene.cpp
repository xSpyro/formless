#include "Client.h"
#include "ClientInterface.h"
#include "CommonPackets.h"

#include "RequestInterface.h"
#include "CommonPackets.h"

#include "ProfileItem.h"
#include "ProfileList.h"

#include "InventoryScene.h"
#include "LobbyScene.h"

#include "Common.h"
#include "Object.h"
#include "LogicHandler.h"
#include "RenderComponent.h"
#include "GameConnector.h"
#include "ScriptEnvironment.h"
#include "PhysicsComponent.h"
#include "CoreComponent.h"
#include "ControllerComponent.h"
#include "AnimationComponent.h"
#include "NetworkComponent.h"
#include "GameHandler.h"
#include "Module.h"

#include "CoreSubsystem.h"
#include "NetworkSubsystem.h"
#include "SoundSubsystem.h"

#include "Framework.h"

#include "MemLeaks.h"

InventoryScene::InventoryScene()
	: Scene()
{
	
}

InventoryScene::InventoryScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game)
	: Scene(handler, rend, wndw), game(game), sound0(-1), sound1(-1)
{
	me = this;
	layer = renderer->createLayer();

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);
	camera.setProjection3D(45, 16 / 9.0f);

	// camera set
	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));

	layer2d = New Stage2D(this);

	net = NULL;

	// register some global functions to lua
	EventLua::Object g = parent->getScript()->getGlobals();
	g.bind("addItem", &addItem);
	g.bind("removeItem", &removeItem);
	g.bind("sendAttrPacket", &sendAttrPacket);
	g.bind("resetCore", &resetCore);
	g.bind("setCoreAttributes", &setCoreAttributes);
	g.bind("getCoreAttributes", &getCoreAttributes);
	g.bind("loadProfile", &loadProfile);
	g.bind("saveProfile", &saveProfile);

	g.bind("giveItem", &dbgItem);
	g.bind("refresh", &refresh);

	g.bind("isInventoryConnected", &isConnected);
	g.bind("getInventoryClientID", &getClientID);
	g.bind("getInventoryPlayerTeam", &getPlayerTeam);

	File file;
	FileSystem::get("scripts/inventory.lua", &file);
	parent->getScript()->execute(file.data, file.size);
	//EventLua::Object addItems = parent->getScript()->getGlobals().get("addItems");
	//addItems.call("items/items.lua");

	player = NULL;
	playerCore = NULL;
}

InventoryScene::~InventoryScene()
{
	delete layer2d;
}

void InventoryScene::update()
{
	camera.update();


	if (game->getNetwork()->isConnected())
	{
		lobby->proxyUpdate();
	}
	// Now to make sure inventory switch is back
	EventLua::Object inv = parent->getScript()->getGlobals().get("switchToInventory");
	
	if (inv)
		inv();


	/*
	if( net != NULL && player != NULL )
	{
		EventLua::Object teamUpdate = parent->getScript()->getGlobals().get("teamPlayerUpdate");
		EventLua::Object oppositeUpdate = parent->getScript()->getGlobals().get("otherPlayerUpdate");
		EventLua::Object resetplayers = parent->getScript()->getGlobals().get("resetPlayers");

		resetplayers.call();

		std::vector<Object*> objects;
		net->queryClientList(objects);

		for(std::vector<Object*>::iterator ob = objects.begin(); ob != objects.end(); ++ob)
		{
			NetworkComponent*	netcomp		= (*ob)->findComponent<NetworkComponent>();
			CoreComponent*		corecomp	= (*ob)->findComponent<CoreComponent>();

			unsigned int netId = netcomp->getID();
			std::string username = netcomp->getName(netId);

			if(corecomp->team == playerCore->team)
			{
				teamUpdate.call(username.c_str(), netId, netcomp->isReady());
			}
			else
			{
				oppositeUpdate.call(username.c_str(), netId, netcomp->isReady());
			}


		}
	}
	*/
}

void InventoryScene::render()
{
}

void InventoryScene::onEnter()
{
	//renderer->insertLayer(10, layer);
	//renderer->insertStage(1100, layer2d);
	buildLayers();

	File file;
	FileSystem::get("scripts/inventory.lua", &file);
	if (!parent->getScript()->execute(file.data, file.size))
		std::cout<< parent->getScript()->getLastError()<<std::endl;

	player = game->getControlledObject();

	// If we have no player, create a temporary CoreComponent
	if( player )
	{
		playerCore	= player->findComponent<CoreComponent>();
		playerNet	= player->findComponent<NetworkComponent>();
	}
	else
		playerCore = New CoreComponent( game->getSubsystem("Core") );
	
	net = (NetworkSubsystem*)game->getSubsystem("Network");


	// Fetch items from master server
	refreshItems();

	luaTimer.restart();

	layer2d->visible = true;

	parent->getScript()->getGlobals()["onInventoryEnter"]();



	// get proxy scene
	lobby = reinterpret_cast < LobbyScene* > (game->getLogic()->getScene("Lobby"));
}
	
void InventoryScene::onLeave()
{
	parent->getScript()->getGlobals()["onInventoryLeave"]();

	// If [core] is temporary, clear the memory
	if( player == NULL )
	{
		delete playerCore;
		playerCore = NULL;
	}

	//renderer->clearStage(10);
	//renderer->clearStage(1100);

	layer2d->visible = false;

	clearLayers();
}

void InventoryScene::onFocus()
{
	//renderer->insertLayer(10, layer);
	//renderer->insertStage(1100, layer2d);
	buildLayers();

	layer2d->visible = true;

	parent->getScript()->getGlobals()["onInventoryEnter"]();

}

void InventoryScene::onFocusLost()
{
	layer2d->visible = false;
	parent->getScript()->getGlobals()["onInventoryLeave"]();
}

bool InventoryScene::onInput(int sym, int state)
{
	if(state != 0)
	{
		if (sym == 'U')
		{
			EventLua::Object reset = parent->getScript()->getGlobals().get("reset");
			reset.call();
		}
	}

	if(layer2d->visible)
		parent->getScript()->getGlobals()["onInventoryInput"](sym, state);

	return true;
}

bool InventoryScene::onMouseMovement(int x, int y)
{
	return true;
}

bool InventoryScene::onCreateObject(Object* object)
{
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();

	if(rendcomp)
	{
		rendcomp->load(renderer);
		layer->add(&rendcomp->model);
	}

	AnimationComponent* animcomp = object->findComponent<AnimationComponent>();

	if (animcomp)
	{
		animcomp->load(renderer, &camera);
	}

	return true;
}

bool InventoryScene::onDestroyObject(Object* object)
{
	return true;
}

void InventoryScene::onRender2D()
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/inventory.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout<< parent->getScript()->getLastError()<<std::endl;
	}
	parent->getScript()->getGlobals()["onInventoryRender"]();
}

CoreComponent* const InventoryScene::getCurrentPlayerCore()
{
	return playerCore;
}

void InventoryScene::setLayer2DVisibility(const bool state)
{
	if (layer2d)
	{
		layer2d->visible = state;
	}
}

InventoryScene * InventoryScene::me = NULL;

void InventoryScene::addItem( const char* holder, unsigned int holderWidth, unsigned int itemid, int x, int y, EventLua::Object data )
{
	std::string tempHolder = holder;

	if(tempHolder == "moduleBar_passive" || tempHolder == "moduleBar_active")
	{
		if(tempHolder == "moduleBar_passive")
			x += me->playerCore->getSkillSlots();

		me->playerCore->addModule(x, itemid, me->items[itemid].getItemName());

		// network sync
		ChangeModulePacket packet;
		packet.slot = x;
		packet.moduleid = itemid;
		strcpy_s(packet.module, 32, me->items[itemid].getItemName());
		packet.add = true;
		packet.companion = false;
		me->game->getNetwork()->send(packet);

	}
	else if( tempHolder == "coreSlot" )
	{
		//CoreComponent* core = me->player->findComponent<CoreComponent>();

		CoreSubsystem* subsys = (CoreSubsystem*)(me->parent->getSubsystem("Core"));
		subsys->load(data, me->playerCore);

		me->playerCore->id = itemid;

		sendAttrPacket();
	}

	/* Sync with master server */
	MasterSendAsync asend;
	MoveItemPacket packet;
	packet.setHolder( holder );
	packet.itemid = itemid;
	packet.userid = ((Client*)me->window)->getUserID();
	packet.slotX = x;
	packet.slotY = y;

	asend( packet, sizeof(MoveItemPacket) );
}

void InventoryScene::removeItem( const char* holder, unsigned int itemid )
{
	std::string tempHolder = holder;
	if(tempHolder == "moduleBar_passive" || tempHolder == "moduleBar_active")
	{
		//CoreComponent* core = me->player->findComponent<CoreComponent>();

		int slot = me->playerCore->removeModule(me->items[itemid].getItemName());

		if( slot >= 0 )
		{
			// network sync
			ChangeModulePacket packet;
			packet.slot = slot;
			packet.moduleid = itemid;
			strcpy_s(packet.module, 32, "");
			packet.add = false;
			me->game->getNetwork()->send(packet);
		}
	}
	else if( tempHolder == "coreSlot" )
	{
		resetCore();
		sendAttrPacket();
	}
}


void InventoryScene::sendAttrPacket()
{
	//CoreComponent* core = me->player->findComponent<CoreComponent>();

	if(me->playerCore)
	{
		if( me->playerCore->getTotalSlots() == 0 )
			int ARGH = 0;

		// network sync
		CoreAttrPacket packet;
		packet = me->playerCore->getAttrPacket();
		packet.companion = false;

		me->game->getNetwork()->send(packet);
	}
}

void InventoryScene::resetCore()
{
	//CoreComponent* core = me->player->findComponent<CoreComponent>();

	if(me->playerCore)
	{
		me->playerCore->setSlots(0, 0);
		me->playerCore->life.setBase(0);
		me->playerCore->lifeRegen.setBase(0);
		me->playerCore->mana.setBase(0);
		me->playerCore->manaRegen.setBase(0);
		me->playerCore->speed.setBase(0);
		me->playerCore->name.clear();
		me->playerCore->id = 0;
	}

	if( me->player )
	{
		ControllerComponent* cont = me->player->findComponent<ControllerComponent>();

		if(cont)
		{
			cont->setSpeed(0);
		}
	}
}

void InventoryScene::setCoreAttributes( EventLua::Object attributes )
{
	//CoreComponent* core = me->player->findComponent<CoreComponent>();

	me->playerCore->name = attributes["name"];
	me->playerCore->setSlots(attributes["nrSlots"], attributes["nrSkills"]);
	me->playerCore->life.setBase(attributes["life"]);
	me->playerCore->lifeRegen.setBase(attributes["lifeRegen"]);
	me->playerCore->mana.setBase(attributes["mana"]);
	me->playerCore->manaRegen.setBase(attributes["manaRegen"]);
	me->playerCore->speed.setBase(attributes["speed"]);
	me->playerCore->damageDealt.setOffset(attributes["damageOffset"]);
	me->playerCore->damageDealt.setModifier(attributes["damageModifier"]);
	me->playerCore->damageRecieved.setOffset(attributes["resistOffset"]);
	me->playerCore->damageRecieved.setModifier(attributes["resistModifier"]);
}

EventLua::Object InventoryScene::getCoreAttributes()
{
	std::string str("TEMP_TABLE_COREATTR");
	EventLua::Object rtn = me->game->getScript()->newTable(str.c_str());

	//CoreComponent* core = me->player->findComponent<CoreComponent>();

	if(me->playerCore == NULL)
		return rtn;
	else
	{
		rtn["name"]				= me->playerCore->name.c_str();
		rtn["nrSlots"]			= me->playerCore->getTotalSlots();
		rtn["nrSkills"]			= me->playerCore->getSkillSlots();
		rtn["life"]				= me->playerCore->life.getMax();
		rtn["lifeRegen"]		= me->playerCore->lifeRegen.getMax();
		rtn["mana"]				= me->playerCore->mana.getMax();
		rtn["manaRegen"]		= me->playerCore->manaRegen.getMax();
		rtn["speed"]			= me->playerCore->speed.getMax();
		rtn["damageOffset"]		= me->playerCore->damageDealt.getOffset();
		rtn["damageModifier"]	= me->playerCore->damageDealt.getModifier();
		rtn["resistOffset"]		= me->playerCore->damageRecieved.getOffset();
		rtn["resistModifier"]	= me->playerCore->damageRecieved.getModifier();

		return rtn;
	}

}

void InventoryScene::loadProfile( unsigned int pid, const char* scene )
{
	std::string fromScene = scene;
	bool companion = false;
	if(pid == 3)
		companion = true;

	MasterRequest request;
	ProfileLoadPacket packet;
	packet.uid = ((Client*)me->window)->getUserID();
	packet.pid = pid;
	RawPacket result;

	std::vector< ProfileItem > profile;

	if( me->items.empty() )
	{
		me->refreshItems();
	}

	try
	{	// Get profile from master server
		if (request(packet, sizeof(packet), result))
		{
			profile.clear();
			char * offset = &reinterpret_cast < char* > (result.data)[RawPacket::HeaderSize];	// This will be a ProfileListPacket
			unsigned int results = *reinterpret_cast < unsigned int* > (offset);
			offset += sizeof(unsigned int); // skip the results int
			offset += sizeof(unsigned int); // skip the uid int
			offset += sizeof(unsigned int); // skip the pid int

			for (unsigned int i = 0; i < results; ++i)
			{
				ProfileItem * item = reinterpret_cast < ProfileItem* > (offset);
				profile.push_back( *item );
				offset += sizeof(ProfileItem);
			}
		}
	}
	catch ( std::exception e )
	{
	}

	// First find the core (else the modules can't be placed) ...
	for( unsigned int i = 0; i < profile.size(); ++i )
	{
		//std::string name = me->items[ profile[i].uid ].getItemName();
		std::string name = profile[i].getName();
		std::string slot = profile[i].getSlot();
		if( slot.find( "Core" ) != slot.npos )
		{
			if( fromScene == "Inventory" )
				me->parent->getScript()->getGlobals()["onInventoryLoadCore"](profile[i].uid);
			else if( fromScene == "CompanionInventory")
				me->parent->getScript()->getGlobals()["onCompanionInventoryLoadCore"](profile[i].uid);
			else if( fromScene == "Lobby" )
				me->parent->getScript()->getGlobals()["onLobbyLoadCore"](profile[i].uid, name.c_str(), companion);
			break;
		}
	}

	// ... then place the modules
	for( unsigned int i = 0; i < profile.size(); ++i )
	{
		//std::string name = me->items[ profile[i].uid ].getItemName();
		std::string name = profile[i].getName();
		std::string slot = profile[i].getSlot();
		unsigned int slotNr = 0;
		if( slot.find( "Slot" ) != slot.npos )
		{
			slot.erase( 0, 4 );
			std::stringstream buffer(slot);
			buffer >> slotNr;

			if( fromScene == "Inventory" )
				me->parent->getScript()->getGlobals()["onInventoryLoadModule"](profile[i].uid, slotNr);
			else if( fromScene == "CompanionInventory")
				me->parent->getScript()->getGlobals()["onCompanionInventoryLoadCore"](profile[i].uid);
			else if( fromScene == "Lobby" )
				me->parent->getScript()->getGlobals()["onLobbyLoadModule"](profile[i].uid, slotNr, name.c_str(), companion);
		}
	}
}

void InventoryScene::saveProfile( unsigned int pid )
{
	//CoreComponent* core = me->player->findComponent<CoreComponent>();

	if( me->playerCore == NULL )
		return;

	int nrMods = me->playerCore->getTotalSlots();

	ProfileList items;

	items.registerItem( ProfileItem( "Core", me->playerCore->id, nrMods ) );
	for( int i = 0; i < nrMods; ++i )
	{
		std::stringstream slot;
		slot << "Slot" << i;
		Module* mod = me->playerCore->getModule(i);
		if( mod )
			items.registerItem( ProfileItem( slot.str(), me->playerCore->getModule(i)->id, 1 ) );
		else
			items.registerItem( ProfileItem( slot.str(), 0, 0 ) );
	}

	char* data = items.getDataPointer();
	unsigned int size = items.getDataSize();

	ProfileListPacket response;

	response.results = items.getNumberOfEntries();
	response.uid = ((Client*)me->window)->getUserID();
	response.pid = pid;
	response.size = sizeof(ProfileListPacket) - RawPacket::HeaderSize + size;

	MasterSend send;

	if( response.results > 0 )
	{
		send( response, data, size );
	}
}

void InventoryScene::dbgItem( const char* item )
{
	if( strlen(item) > 32 )
	{
		return;
	}

	MasterSend send;
	AddItemPacket packet;
	packet.setName( item );
	packet.type = 0;
	packet.uid = ((Client*)me->window)->getUserID();

	try
	{
		send( packet, sizeof(AddItemPacket) );
	}
	catch( std::exception e )
	{
	}

	me->refresh();
}

void InventoryScene::refresh()
{
	me->saveProfile(0);
	me->refreshItems();
	me->loadProfile(0, "Inventory");
}

bool InventoryScene::isConnected()
{
	return (me->player != NULL);
}

int InventoryScene::getClientID()
{
	if( me->player )
	{
		if( me->playerNet )
			return me->playerNet->getID();
		else
			return -1;
	}
	else
		return -1;
}

int InventoryScene::getPlayerTeam()
{
	if( me->player )
		return me->playerCore->team;
	else
		return -1;
}


void InventoryScene::refreshItems()
{
	MasterRequest request;

	InventoryRequestPacket packet;
	packet.uid = ((Client*)window)->getUserID();
	packet.companion = false;
	RawPacket result;

	/* This part is for using master server's inventory */
	//File file;
	//FileSystem::get("scripts/inventory.lua", &file);
	//parent->getScript()->execute(file.data, file.size);
	parent->getScript()->getGlobals().get("clearItems")();
	EventLua::Object addItem = parent->getScript()->getGlobals().get("addSingleItem");

	try
	{
		if (request(packet, sizeof(packet), result))
		{
			items.clear();
			char * offset = &reinterpret_cast < char* > (result.data)[RawPacket::HeaderSize];
			unsigned int results = *reinterpret_cast < unsigned int* > (offset);
			offset += sizeof(unsigned int); // skip the int

			for (unsigned int i = 0; i < results; ++i)
			{
				ItemInfo * item = reinterpret_cast < ItemInfo* > (offset);
				items[ item->uid ] = *item;

				//itemID[ item->getItemName() ] = item->uid;

				offset += sizeof(ItemInfo);
			}
		}
	}
	catch ( std::exception e )
	{
	}

	/* This part is for using master server's inventory */
	//for( unsigned int i = 0; i < items.size(); i++ )
	for( Items::iterator it = items.begin(); it != items.end(); ++it )
	{
		addItem.call("items/items.lua", it->second.getItemName(), it->second.uid, it->second.slotX, it->second.slotY );
	}
}

void InventoryScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer(1398, layer);
	renderer->insertStage(1399, layer2d);
}

void InventoryScene::clearLayers()
{
	renderer->clearStage(1398);
	renderer->clearStage(1399);
}
