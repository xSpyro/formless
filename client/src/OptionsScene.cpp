#include "Client.h"

#include "OptionsScene.h"

#include "Common.h"
#include "Object.h"
#include "LogicHandler.h"

#include "RenderComponent.h"

#include "MemLeaks.h"

#include "GameConnector.h"

OptionsScene::OptionsScene()
	: Scene(),resolutionBox(NULL), firstWidth(0), firstHeight(0), keyBindingBox(NULL)
{

}

OptionsScene::OptionsScene(LogicHandler* handler, Renderer* rend, Window* wndw, Font2D* font, Graphics* graphics)
	: Scene(handler, rend, wndw), resolutionBox(NULL), keyBindingBox(NULL)
{
	File file;


	FileSystem::get("script/OptionsMenu.lua", &file, true);

	if ( !parent->getScript()->execute(file.data, file.size) ) 
		std::cout << "Script not executed" << std::endl;

	layer = renderer->createLayer();

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);
	//camera.setProjection3D(45, 16 / 9.0f);

	// camera set

	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));

	layer2d = New Stage2D(this);

	EventLua::Object g = parent->getScript()->getGlobals();
	g.bind("applySettings", &applySettings);
	g.bind("resetKeybinds", &resetKeybinds	);
	

	resolutionBox = new ListBox(font, graphics);

	//1024x576, 1152x648, 1280x720, 1600x900, and 1920x1080
	
	std::vector<DXGI_MODE_DESC> resolutions;
	std::vector<DXGI_MODE_DESC> existingResolutions;

	renderer->getResolutions(resolutions);


	for(unsigned int i = 0; i < resolutions.size(); i++)
	{
		std::stringstream ss, ss2;

		ss << resolutions[i].Width << "x" << resolutions[i].Height;
		ss2 << resolutions[i].Width << " " << resolutions[i].Height;

		bool exists = false;
		for(unsigned int j = 0; j < existingResolutions.size(); j++)
		{
			if (resolutions[i].Width == existingResolutions[j].Width && 
				resolutions[i].Height == existingResolutions[j].Height)
			{
				exists = true;
				break;
			}
		}

		if (!exists)
		{
			existingResolutions.push_back(resolutions[i]);

			resolutionBox->addItem( ss.str(), ss2.str() );
		}

	}		

	if ( resolutions.empty() )
	{
		resolutionBox->addItem("1280x720", "1280 720");
	}

	int width = 0, height = 0;
	EventLua::Object graph = parent->getScript()->get("Settings").get("Graphics");

	width = graph.get("width").queryInteger(1280);
	height = graph.get("height").queryInteger(720);

	firstWidth = width;
	firstHeight = height;

	std::stringstream ss;

	ss << width << "x" << height;

	resolutionBox->selectItem( ss.str() );

	resolutionBox->setPos( 200, 95, 110, 300 );


	keyBindingBox = New ListBox(font, graphics);

	refreshKeybinds();

	keyBindingBox->setPos( 425, 95, 200, 350 );

	selected = "";

	me = this;
}

OptionsScene::~OptionsScene()
{
	Delete layer2d;

	if (resolutionBox)
	{
		delete resolutionBox;
		resolutionBox = NULL;
	}

	if (keyBindingBox)
	{
		delete keyBindingBox;
		keyBindingBox = NULL;
	}
}

void OptionsScene::update()
{

	camera.update();

	Client* client = (Client*)window;

	client->updateSettings();
	

	resolutionBox->update();
	
	keyBindingBox->update();

	resolutionBox->setActive(true);
	keyBindingBox->setActive(true);
}

void OptionsScene::render()
{
	//parent->getScript()->getGlobals()["onOptionsMenuRender"]();
}

void OptionsScene::onEnter()
{
	//renderer->insertLayer(1499, layer);
	//renderer->insertStage(1500, layer2d);
	buildLayers();

	resolutionBox->setVisible(true);

	resolutionBox->setActive(true);

	keyBindingBox->setVisible(true);

	resolutionBox->setActive(true);


	File file;
	FileSystem::get("scripts/OptionsMenu.lua", &file);
	if (!parent->getScript()->execute(file.data, file.size))
		std::cout << parent->getScript()->getLastError() << std::endl;
}

void OptionsScene::onLeave()
{
	//renderer->clearStage(1499);
	//renderer->clearStage(1500);
	clearLayers();

	resolutionBox->setVisible(false);
	keyBindingBox->setVisible(false);

	Client* client = (Client*)window;	

	client->updateSettings();
	client->saveSettings();

	client->saveKeybinds();

}

void OptionsScene::onLoad()
{
}

bool OptionsScene::onInput(int sym, int state)
{
	if( layer2d->visible )
		parent->getScript()->getGlobals()["onOptionsMenuInput"](sym, state);

	if ( sym == 600 )
	{
		resolutionBox->onClick( (float)mx, (float)my );
		keyBindingBox->onClick((float)mx, (float)my);
	}
	if (sym == 612 && selected == "")
	{
		if ( resolutionBox->hover((float)mx, (float)my) )
			resolutionBox->onInput(38,1);
		else if (keyBindingBox->hover((float)mx, (float)my))
			keyBindingBox->onInput(38,1);
	}

	else if (sym == 611 && selected == "")
	{
		if ( resolutionBox->hover((float)mx, (float)my) )
			resolutionBox->onInput(40,1);
		else if (keyBindingBox->hover((float)mx, (float)my))
			keyBindingBox->onInput(40,1);
	}

	else if (sym == 8 && state == 1 && selected != "")
	{
		std::string action, key;

		std::stringstream ss;
		ss << selected;
		ss >> action;
		ss >> key;

		keyBindingBox->setSelectedItem(action + "	- " + key, action + " " + key);

		selected = "";

	}
	else if (sym == 13 && state == 1 && selected == "")
	{
		if (selected == "")
		{ 
			std::string action, key;

			selected = keyBindingBox->getSelectedItem();
			std::stringstream ss;
			ss << selected;
			ss >> action;
			ss >> key;
			keyBindingBox->setSelectedItem(action + "	- ???", action + " " + key);
			
		}

	}
	else if (state == 1)
	{
		if (selected != "")
		{
			selected = keyBindingBox->getSelectedItem();
			std::stringstream ss, keyConvert;
			ss << selected;

			std::string action, keyBind;

			ss >> action;
			ss >> keyBind;

			std::vector<int> keys;
			keys.push_back(sym);
			keyConvert << sym;

			


			parent->getEventHandler()->setKeyBind(keys, action);

			keyBind = (std::string)GameConnector::getKeyBind(action.c_str());//keyConvert.str();
			keyBindingBox->setSelectedItem(action + "	- " + keyBind, action + " " + keyBind);



			selected = "";

		}
	}

	else
	{
		resolutionBox->onInput(sym, state);
		keyBindingBox->onInput(sym, state);
	}



	std::string item = resolutionBox->getSelectedItem();

	int width = 0;
	int height = 0;


	std::stringstream ss;
	ss << item;

	ss >> width;
	ss >> height;


	EventLua::Object graphics = parent->getScript()->get("Settings").get("Graphics");

	graphics.set("width", width);
	graphics.set("height", height);

	if(state == 1 && sym == VK_ESCAPE)
		parent->deactivateSceneOnUpdate("OptionsMenu");

	return true;
}

bool OptionsScene::onMouseMovement(int x, int y)
{
	Scene::onMouseMovement( x, y );
	return true;
}

bool OptionsScene::onCreateObject(Object* object)
{
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();

	if(rendcomp)
	{
		rendcomp->load(renderer);
		layer->add(&rendcomp->model);
	}

	return true;
}

bool OptionsScene::onDestroyObject(Object* object)
{
	return true;
}

void OptionsScene::onConnected(unsigned int uid)
{

}

void OptionsScene::onFocus()
{
	//renderer->insertLayer(1499, layer);
	//renderer->insertStage(1500, layer2d);
	buildLayers();

	layer2d->visible = true;
}

void OptionsScene::onFocusLost()
{
	layer2d->visible = false;
}

void OptionsScene::onRender2D()
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/OptionsMenu.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout << parent->getScript()->getLastError() << std::endl;
	}

	if ( isRestartRequiredToApplySettings() )
	{
		parent->getScript()->getGlobals().set("restartText", 1);
	}

	else
	{
		parent->getScript()->getGlobals().set("restartText", 0);
	}

	parent->getScript()->getGlobals()["onOptionsMenuRender"]();

	resolutionBox->render();
	keyBindingBox->render();
}

void OptionsScene::applySettings(int ssao, int fog, int shadows)
{
	std::filebuf fb;
	fb.open("settings.txt", std::ios::out);

	std::ostream file(&fb);

	EventLua::State script;
	EventLua::Object settings, graphicsTable, sound;

	settings = script.newTable("Settings");

	settings.newTable("Graphics");

	settings.newTable("Sound");

	graphicsTable = settings.get("Graphics");

	sound = settings.get("Sound");


	sound.set("masterVolume",1.0f);
	sound.set("fxVolume", 1.0f);
	sound.set("guiVolume", 1.0f);
	sound.set("musicVolume", 1.0f);

	graphicsTable.set("width", 1280);
	graphicsTable.set("height", 720);

	graphicsTable.set("ssao", ssao);
	graphicsTable.set("shadows", shadows);
	graphicsTable.set("fog", fog);
	graphicsTable.set("visualRange", 0);


	file << settings;	

	fb.close();
}

void OptionsScene::resetKeybinds()
{
	Client* client = me->getClient();

	client->loadKeybinds("defaultKeybinds.lua");

	client->saveKeybinds();

	me->refreshKeybinds();


}

bool OptionsScene::isRestartRequiredToApplySettings()
{
	std::stringstream ss;

	ss << resolutionBox->getSelectedItem();
	int width = 0;
	int height = 0;

	ss >> width;
	ss >> height;

	EventLua::Object graph = parent->getScript()->get("Settings").get("Graphics");

	int currentWidth = graph.get("width").queryInteger(1280);
	int currentHeight = graph.get("height").queryInteger(720);


	if (firstWidth != currentWidth || firstHeight != currentHeight)
	{
		return true;
	}



	return false;
}

void OptionsScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer(1499, layer);
	renderer->insertStage(1500, layer2d);
}

void OptionsScene::clearLayers()
{
	renderer->clearStage(1499);
	renderer->clearStage(1500);
}

void OptionsScene::refreshKeybinds()
{
	keyBindingBox->clear();

	std::map <std::string, std::string> keyBindings;

	keyBindingBox->selectItem("null");

	keyBindings["up"] = "up W";
	keyBindings["down"] = "down S";
	keyBindings["left"] = "left A";
	keyBindings["right"] = "right D";
	keyBindings["attack"] = "attack 600";
	keyBindings["nextSlot"] = "nextSlot 612";
	keyBindings["prevSlot"] = "prevSlot 611";
	keyBindings["slot0"] = "slot0 1";
	keyBindings["slot1"] = "slot1 2";
	keyBindings["slot2"] = "slot2 3";
	keyBindings["slot3"] = "slot3 4";
	keyBindings["slot4"] = "slot4 5";
	keyBindings["slot5"] = "slot5 6";
	keyBindings["slot6"] = "slot6 7";
	keyBindings["slot7"] = "slot7 8";
	keyBindings["slot8"] = "slot8 9";
	keyBindings["slot9"] = "slot9 0";

	for(std::map <std::string, std::string>::iterator it = keyBindings.begin(); it != keyBindings.end(); it++)
	{
		std::string keyBind, action;
		std::stringstream ss;
		ss << it->second;

		ss >> action;
		ss >> keyBind;

		Client* client = (Client*)window;

		client->loadKeybinds();

		std::string newKeyBind = (std::string)GameConnector::getKeyBind( action.c_str() );

		keyBindingBox->addItem(action + "	- " + newKeyBind, action + " " + newKeyBind);

	}
}


Client* const OptionsScene::getClient()
{
	return (Client*)window;
}

OptionsScene * OptionsScene::me = NULL;