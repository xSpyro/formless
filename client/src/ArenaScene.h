
#ifndef ARENASCENE_H
#define ARENASCENE_H

#include "Scene.h"
#include "Framework.h"

//#include "PlayerVisual.h"
//#include "SceneShaderState.h"

#include "RenderModule.h"
#include "EventList.h"

class Object;
class PhysicsComponent;
class ControllerComponent;
class CoreComponent;
class GameHandler;
class Chat;
class NetworkSubsystem;
class NetworkSubsystemClient;
struct Statistics;
struct RespawnTimer;

class ArenaScene : public Scene
{


public:

	struct InitData
	{
		Renderer * rend;
		Window * window;
		GameHandler * game;
		Chat * chat;
		ParticleSystem * ps;
		Font2D * font;
		RenderModule * renderModule;
		Client * client;
	};

	struct EffectText
	{
		std::string		text;
		int				timeticks;
		int				age;
		Vector3			position;
		D3DXCOLOR		color;
		bool			inWorld;
		unsigned int	textSize;
	};

	struct AccountInfo
	{
		std::string		itemName;
		unsigned int	playerAge;
		unsigned int	prevItemAt;
		unsigned int	nextItemAt;
		unsigned int	timeTicks;
		bool			allUnlocked;
	};

	struct NewAchievement
	{
		std::string		name;
		std::string		description;
		unsigned int	timeTicks;
	};

	typedef std::vector<EffectText> EffectTexts;

public:

	ArenaScene();
	ArenaScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game, Chat* chat = NULL, ParticleSystem * ps = NULL, Font2D * font = NULL);
	ArenaScene(const InitData & in);
	virtual ~ArenaScene();

	void update();
	void render();

	void onEnter();
	void onLeave();
	void onLoad();

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	bool onCreateObject(Object* object);	// Do not call this manually!
	bool onDestroyObject(Object* object);	// Do not call this manually!
	void onKillObject(Object* killed, Object* killer); // Do not call this manually!

	void onFocus();
	void onFocusLost();

	void onConnected(unsigned int id);

	void onRender2D(Font2D * font);

	bool getShowCombatText();
	bool getShowTeamNames();
	bool getShowPlayerName();
	bool getShowEnemyNames();
	bool getShowStats();

	void setShowCombatText(const bool ct);
	void setShowTeamNames(const bool tn);
	void setShowPlayerName(const bool pn);
	void setShowEnemyNames(const bool en);
	void setShowStats(const bool s);
	struct Stage2D : public Stage
	{
		ArenaScene * me;
		Font2D * font;
		bool visible;
		
		Stage2D(ArenaScene * me, Font2D* font) : me(me), font(font)
		{
			visible = true;
		}

		void apply()
		{
			if(visible)
				me->onRender2D(font);
		}
	};

	void onSkillEffect(float damage, bool stun, bool link, bool outOfMana, int killStreak, float x, float y, float z);
	void onAccountUpdate(AccountPacket* packet);

private:
	//For lua
	static ArenaScene*		me;
	static int				getSkillSlots();
	static const char*		getSkillAt( int slot );
	static int				getSkillID( int slot );
	static void				setActiveSlot( int slot );
	static int				getClientID();
	static int				getClientTeam();
	static void				disconnect();
	static EventLua::Object getItemUpdateInfo();
	static EventLua::Object	getRespawnTimer();

private:
	void buildLayers();
	void clearLayers();

	void preparePlayer();
	void createHeightMap();
	void createSkySphere();
	bool updateMouseWorldPosition();		//Returns true if the mouse hovered over the height map, false if outside
	void updateCamera();
	void updateSound();
	void updateLights();

	void renderPlayerNames(Font2D* font);
	void renderPlayerStats();
	void renderSkillEffects(Font2D* font);
	void renderItemUnlock();
	void renderNewAchievement();
	void renderEventList(Font2D* font);

	Vector2 worldToScreen(Vector3 position);

private:
	Stage2D*	layer2d;
	Timer		luaTimer;

	Vector3		mouseWorldPosition;	

	RenderModule * renderModule;
	
	Camera*		camera;
	Vector3		cameraDest;
	Vector3		cameraLookDest;
	Vector3		cameraLookAt;
	int			cameraType;

	Object*					player;
	PhysicsComponent*		playerPhys;
	ControllerComponent*	playerCont;
	CoreComponent*			playerCore;


	NetworkSubsystemClient* network;
	Chat* chat;

	GameHandler * game;

	Timer		timer;
	Statistics* stats;
	RespawnTimer*	respawnTimer;

	int sound;

	// hud graphics
	Camera hudCamera;
	Layer * hudLayer;

	AnimationComponent * hudLife;
	AnimationComponent * hudCore;
	ShapeAnimation * hudLifeShape;

	Vector2 mouse;

	int		tabState;

	Client * client;

	bool showStats;

	EffectTexts		effectTexts;
	AccountInfo		accountInfo;
	NewAchievement	newAchievement;

	EventList eventList;

	bool showCombatText;
	bool showTeamNames;
	bool showPlayerName;
	bool showEnemyNames;


	std::vector < Object* > sceneryObjects;

};

#endif