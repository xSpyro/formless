
#ifndef LAGG_TEST_SCENE_H
#define LAGG_TEST_SCENE_H


#include "Scene.h"
#include "Framework.h"

//#include "PlayerVisual.h"
//#include "SceneShaderState.h"

#include "RenderModule.h"
#include "EventList.h"

class Object;
class PhysicsComponent;
class ControllerComponent;
class CoreComponent;
class GameHandler;
class Chat;
class NetworkSubsystem;
class NetworkSubsystemClient;
struct Statistics;

class Font2D;
class Client;

class LaggTestScene : public Scene
{
public:

	struct InitData
	{
		Renderer * rend;
		Window * window;
		GameHandler * game;
		Chat * chat;
		ParticleSystem * ps;
		Font2D * font;
		RenderModule * renderModule;
		Client * client;
	};

public:

	LaggTestScene(const InitData & in);

	void update();
	void render();

	void onEnter();
	void onLeave();
	void onLoad();

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	bool onCreateObject(Object* object);	// Do not call this manually!
	bool onDestroyObject(Object* object);	// Do not call this manually!
	void onKillObject(Object* killed, Object* killer); // Do not call this manually!

	void onFocus();
	void onFocusLost();

private:

	Timer restartTimer;
	Timer fireTimer;

	Client * client;

	Camera*		camera;
	RenderModule * renderModule;

	GameHandler * game;

	std::vector < Object* > sceneryObjects;
};

#endif