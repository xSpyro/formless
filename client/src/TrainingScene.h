
#ifndef TRAININGSCENE_H
#define TRAININGSCENE_H

#include "Scene.h"
#include "Framework.h"
#include "RenderModule.h"
class Font2D;
class NetworkSubsystemClient;
class Chat;
class CoreComponent;
class AIComponent;

class TrainingScene : public Scene
{
public:

	TrainingScene(Renderer* rend, Window* wndw, GameHandler* game, RenderModule* module, Font2D* font);
	virtual ~TrainingScene();

	void update();
	void render();

	void onEnter();
	void onLeave();
	
	bool onInput(int sym, int state);
	bool onMouseMovement(int x, int y);

	bool onCreateObject(Object* object);
	bool onDestroyObject(Object* object);

	void onRender2D(Font2D* font);
	struct Stage2D : public Stage
	{
		TrainingScene * me;
		Font2D * font;
		bool visible;

		Stage2D(TrainingScene * me, Font2D* font) : me(me), font(font)
		{
			visible = true;
		}

		void apply()
		{
			if(visible)
				me->onRender2D(font);
		}
	};

private:
	bool updateMouseWorldPosition();
	void preparePlayer();
	void prepareAI();
	void updateCamera();

private:
	//For lua
	static TrainingScene* me;
	static int				getPlayerSkillSlots();
	static int				getAISkillSlots();
	static EventLua::Object	getPlayerLife();
	static EventLua::Object	getPlayerMana();
	static EventLua::Object getAILife();
	static EventLua::Object getAIMana();
	static const char*		getPlayerSkillAt( int slot );
	static const char*		getAISkillAt( int slot );

private:
	Stage2D*	layer2d;
	Timer		luaTimer;

	Vector3		mouseWorldPosition;	

	RenderModule * renderModule;

	Camera*		camera;
	Vector3		cameraDest;
	Vector3		cameraLookDest;
	Vector3		cameraLookAt;
	int			cameraType;

	Object*					player;
	PhysicsComponent*		playerPhys;
	ControllerComponent*	playerCont;
	CoreComponent*			playerCore;

	Object*					AI;
	PhysicsComponent*		AIPhys;
	AIComponent*			AICont;
	CoreComponent*			AICore;

	NetworkSubsystemClient* network;
	Chat*					chat;

	GameHandler * game;

	Timer		tempTimer;
};

#endif