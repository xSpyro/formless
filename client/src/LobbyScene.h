
#ifndef LOBBYSCENE_H
#define LOBBYSCENE_H

#include "Scene.h"
#include "Framework.h"

class GameHandler;
class NetworkSubsystem;
class Chat;
class CoreComponent;
class NetworkComponent;
struct Statistics;

class LobbyScene : public Scene
{
public:

	LobbyScene();
	LobbyScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game, Chat* chat = NULL);
	virtual ~LobbyScene();

	void update();
	void proxyUpdate();

	void render();

	void onEnter();
	void onLeave();

	bool onInput(int sym, int state);				// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);				// If return false give input to eventHandler

	bool onCreateObject(Object* object);	// Do not call this manually!
	bool onDestroyObject(Object* object);	// Do not call this manually!

	void onRender2D();

	void onConnected(unsigned int id);

	void onFocus();
	void onFocusLost();
	

	struct Stage2D : public Stage
	{
		LobbyScene * me;
		bool visible;

		Stage2D(LobbyScene * me) : me(me)
		{
			visible = true;
		}

		void apply()
		{
			if(visible)
				me->onRender2D();
		}
	};

private:
	void updatePlayers();

	void buildLayers();
	void clearLayers();

private:

	//For lua
	static LobbyScene* me;

	static const char*	getChatText();
	static const char*	getChatLog();
	static int			getClientID();
	static const char*	getPlayerCore();
	static const char*	getCompanionCore();
	static int			getPlayerTeam();

	static void			loadCore( const char* name, EventLua::Object data, bool companion = false );
	static void			loadModule( unsigned int id, const char* name, int slot, EventLua::Object data, bool companion = false );

	static void			setReady( bool ready );
	static bool			getReady();
	static void			createCompanion();
	static void			removeCompanion();
	static bool			hasCompanion();

	static void			disconnect();

	static bool			joinTeam( int team );

private:

	Chat* chat;

	GameHandler *		game;
	Stage2D *			layer2d;

	Layer *				layer;
	Camera				camera;

	NetworkSubsystem*	net;

	Statistics*			stats;

	Object*				player;
	CoreComponent*		playerCore;
	NetworkComponent*	playerNet;

	// TODO:: Fix this better...
	int					join_team;
	unsigned int		teams[3];

	Timer				luaTimer;
	Timer				updatePlayerTimer;

	Timer				reloadProfileTimer;
	bool				profileReloaded;
};

#endif