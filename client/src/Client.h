#ifndef CLIENT_H
#define CLIENT_H

#include "ClientInterface.h"

#include "CommonPackets.h"

#include "Program.h"
#include "Framework.h"
class LogicHandler;
class GameHandler;
class Scene;
class Chat;
class Console;

#include "Graphics.h"
#include "SharedFormatStore.h"
#include "LuaFileManager.h"

class AnimationSubsystem;
class NetworkSubsystem;
struct Statistics;

// TESTING
#include "Object.h"
#include "Font2D.h"
// END TESTING

#include "RenderModule.h"

#include "Profile.h"

class Client : public Program, public Window, public ClientInterface
{
public:
	Client(boost::asio::io_service & service, int show, HINSTANCE inst);
	~Client();

	bool startup();
	bool shutdown();

	void update();
	void run();

	void onInput(int key, int state);
	void onMouseMovement(int x, int y);

	void onResizeWindow(unsigned int width, unsigned int height);

	void onSceneChange();
	void onCreateObject(Object* object);
	void onDestroyObject(Object* object);

	void onPacket(RawPacket & packet);

	void onConnected(unsigned int id);
	void onDisconnected();

	void loadingPostServerSelect();

	void setUser( unsigned int id, std::string name );
	unsigned int getUserID();
	void setOnlineStatus( int status );

	void loadSettings();
	void saveSettings();
	void updateSettings();

	void saveLevelData( const unsigned char* data, unsigned int bytes );
	const File* getLevelData();


	int getFps() const;
	float getNetLagg() const;
	float getUpdateTime() const;
	float getRenderTime() const;
	float getPresentTime() const;
	float getWinNetTime() const;
	unsigned int getBytesIn() const;
	unsigned int getBytesOut() const;

public:
	void initDirect2D();
	// lua register
	static Client*	me;
	static Color	fontColor;

	//static void cprint( const char * str );

	static void setBlendColor(float r, float g, float b, float a);
	static void drawSprite(const char * texture, float x, float y, float width, float height);
	static void drawSpriteAdd(const char * texture, float x, float y, float width, float height);
	static void drawSpriteRot(const char * texture, float x, float y, float width, float height, float rot);
	static void drawShinySprite(const char * texture, float x, float y, float width, float height, float shininess);
	static void drawShine(float x, float y, float size);
	static void drawShineExt(const char * texture, float x, float y, float size);
	static void drawBorder( const char * texture, float x, float y, float width, float height );	// Draws a border along coordinates and fills with [texture]
	static void drawBorderEx(const char * texture, float x, float y, float width, float height);
	static void drawBar(const char * texture, float x, float y, float width, float height, float val);

	static void drawLine(float x1, float y1, float x2, float y2);

	static bool setFont(const char * font);
	static void setTextAlignment( int align );
	static void setParagraphAlignment( int align );
	static void setFontColor( float r, float g, float b, float a );

	static void drawString(const char * str, float x, float y);
	static void drawStringArea(const char * str, float x, float y, float width, float height);
	static float getStringHeight(const char * str, float width);
	static EventLua::Object getStringMetrics( const char* str );

	static void gotoScene( const char* name );
	static void gotoSceneUsingTransition(const char* name, int effect, int time);
	static void activateScene( const char* name );
	static void deactivateScene( const char* name );

	static const char* getUserName();
	static int getOnlineStatus();
	static bool isSpectator();
	static void logoff();

	static void exit();

	static void say( const char * msg );

	static void spawnHudEffect(float x, float y, int type);

	static EventLua::Object getStats();
	
	// filesystem
	static void fileCallback(const std::string & file, void * userdata);

	static bool saveKeybindsLua(EventLua::Object file);
	static bool loadKeybindsLua(EventLua::Object file);

	static bool saveKeybinds(std::string fname = "keybinds.lua");
	static bool loadKeybinds(std::string fname = "keybinds.lua");

private:
	void processLevelDataResponsePacket(LevelDataResponsePacket & packet);
	void processAuthResultPacket(AuthResultPacket & packet);
	void processAchievementListPacket(RawPacket & packet);

	static void onPacketStatic(RawPacket & packet);
private:

	int width, height;
	bool focus;

	RenderModule*	renderModule;

	GameHandler*	game;
	Renderer		renderer;
	Graphics		graphics;
	Chat*			chat;
	Console*		console;

	std::string		levelName;
	File			levelData;

	Object*			player;

	static int		online;
	unsigned int	user_id;
	std::string		user_name;

	// TODO: Fis this better...
	std::string		join_ip;
	int				join_team;

	//Temporary
	
	Font2D font;

	Texture * direct2dtexture;
	IDXGIKeyedMutex * mutex;

	AnimationSubsystem * animation;
	NetworkSubsystem * network;
	ParticleSystem particleSystem; // temporary

	SharedFormatStore formatStore;

	int tick;
	float net_lagg;
	int fps;

	bool poll_network;

	float profile_update;
	float profile_render;
	float profile_present;
	float profile_winnet;

	unsigned int bytes_in;
	unsigned int bytes_out;


	Profile updateGameProfile;
	Profile updateRestProfile;


	Scene* arenaScene;

	Statistics* gameStats;


	boost::thread * musicThread;
};

#endif