
#ifndef MAINMENU_SCENE_H
#define MAINMENU_SCENE_H

#include "Scene.h"

#include "Framework.h"

class MainMenuScene : public Scene
{
public:

	MainMenuScene();
	MainMenuScene(LogicHandler* handler, Renderer* rend, Window* wndw);
	virtual ~MainMenuScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	void onFocus();
	void onFocusLost();

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	bool onCreateObject(Object* object);	//Do not call this manually!
	bool onDestroyObject(Object* object);	//Do not call this manually!

	void onRender2D();

	struct Stage2D : public Stage
	{
		MainMenuScene * me;
		bool visible;

		Stage2D(MainMenuScene * me) : me(me), visible(true)
		{
		}

		void apply()
		{
			if( visible )
				me->onRender2D();
		}
	};

private:
	//For lua
	static MainMenuScene* me;

private:
	void buildLayers();
	void clearLayers();

	Stage2D *	layer2d;
	Layer *		layer;
	Camera		camera;
	Timer		luaTimer;
};

#endif