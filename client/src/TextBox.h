#ifndef TEXTBOX__H
#define TEXTBOX__H

#include "Font2D.h"
#include <ctime>

class TextBox
{
	typedef std::vector< std::string > Data;

public:
	TextBox( Font2D* font );
	virtual ~TextBox();

	virtual void			onInput( int key, int state );
	virtual void			onClick( float x, float y );

	virtual void			addMessage( const std::string & msg );
	virtual void			getData( std::vector< std::string > & data );	// Copies stored data into vector passed as argument
	virtual std::string		getData( unsigned int index );					// Returns stored data at index (or empty string if index is out of bounds)

	virtual void			update();
	virtual void			render();
	virtual void			clear();

	virtual void			setActive( bool status );
	virtual bool			isActive();

	virtual void			setVisible( bool status );
	virtual bool			isVisible();

	virtual bool			hover( float x, float y );

	virtual void			setSize( unsigned int max );					// Limits the size of the data vector (default is 50)

	virtual void			setPos( float x, float y, float w, float h );
	virtual D2D1_RECT_F		getPos();

	virtual void			setColor( float r, float g, float b, float a );

	virtual void			fadeOut( int delay = 0 );
	virtual void			fadeIn();

protected:
	virtual void			updateBuffer();
	virtual void			determineLineheight();

protected:
	Data					data;

	D2D1_RECT_F				pos;
	D3DXCOLOR				color;
	
	FLOAT					fadeTarget;
	std::clock_t			fadeDelay;
	bool					fade;

	int						atLine;
	float					lineHeight;
	int						nrLines;
	unsigned int			visibleLines;
	unsigned int			maxSize;

	bool					active;
	bool					visible;

	// Handlers
	Font2D*					font;
	bool					rebuildBuffer;

private:
	std::string				outBuffer;
};

#endif
