

#ifndef BLUR_TRANSITION_H
#define BLUR_TRANSITION_H

#include "Common.h"
#include "SceneTransition.h"

class RenderModule;

class BlurTransition : public SceneTransition
{

public:

    BlurTransition(RenderModule * renderModule);
    ~BlurTransition();

    void onBegin(int frames);
	void onFadeChange();

    void onFadeIn();
	void onFadeOut();

private:

	int frame;
	int frames;

	float power;

	RenderModule * renderModule;
};


#endif