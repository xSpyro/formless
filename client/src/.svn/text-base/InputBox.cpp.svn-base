#include "InputBox.h"

InputBox::InputBox( Font2D* font )
	: font(font)
{
	active			= false;
	allowSpecial	= true;
	maxChars		= 255;
	password		= false;
	visible			= false;

	callback		= NULL;

	setColor( 1.0f, 1.0f, 1.0f, 1.0f );
	setPos( 0, 0, 0, 0 );

	clear();
}

InputBox::~InputBox()
{
}

void InputBox::onInput( int key, int state )
{
	if( visible && active && state != 0 )
	{
		switch( key )
		{
		case VK_DELETE:
			{
				if( cursorPos < text.size() )
				{
					text.erase( cursorPos + 1, 1 );
				}
			}
			break;
		case VK_BACK:
			{
				if( cursorPos > 0 )
				{
					text.erase( --cursorPos, 1 );
				}
			}
			break;
		case VK_LEFT:
			{
				if( cursorPos > 0 )
				{
					text.erase( cursorPos, 1 );
					text.insert( --cursorPos, 1, '|' );
				}
			}
			break;
		case VK_RIGHT:
			{
				if( cursorPos < text.size() - 1 )
				{
					text.erase( cursorPos, 1 );
					text.insert( ++cursorPos, 1, '|' );
				}
			}
			break;
		case VK_RETURN:
			{
				if( callback )
				{
					callback( getText() );
				}
			}
			break;
		default:
			{
				if( key >= 0 && key <= 255 && text.size() < maxChars )
				{
					unsigned char buf[3];

					GetKeyboardState( kbBuf );

					int res = ToAscii( key, MapVirtualKey( key, 0 ), kbBuf, (LPWORD)buf, 0 );

					if( res == 1 )
					{
						// Control characters not allowed
						if( buf[0] >= 0 && buf[0] <= 31 )
							break;

						if( allowSpecial )
						{
							text.insert( cursorPos++, 1, buf[0] );
						}
						else if( buf[0] >= 0 && buf[0] <= 255 && isalnum( buf[0] ) )
						{
							text.insert( cursorPos++, 1, buf[0] );
						}
					}
				}
			}
			break;
		}
	}
}

void InputBox::onClick( float x, float y )
{
	if( x > pos.left && x < pos.right &&
		y > pos.top && y < pos.bottom )
	{
		setActive( true );
	}
	else
	{
		setActive( false );
	}
}

void InputBox::bindCallback( void (*callback)(const std::string &) )
{
	this->callback = callback;
}

void InputBox::setMaxChars( unsigned int max )
{
	maxChars = max + 1; // +1 for the cursor
}

void InputBox::setLabel( const std::string & l )
{
	label = l;
}

void InputBox::setText( const std::string & t )
{
	text = t;

	if( active )
	{
		text.insert( text.size(), 1, '|' );
		cursorPos = text.find('|');
	}
}

std::string InputBox::getText()
{
	std::string result = text;

	if( active )
	{
		result.erase( cursorPos, 1 );
	}

	return result;
}

void InputBox::clear()
{
	text.clear();

	if( active )
	{
		text = "|";
		cursorPos = text.find('|');
	}
	else
	{
		cursorPos = 0;
	}
}

void InputBox::render()
{
	if( font != NULL && visible )
	{
		float x = pos.left;
		float y = pos.top;
		float w = pos.right - pos.left;
		float h = pos.bottom - pos.top;

		int oldAlign = font->getParagraphAlignment();
		font->setParagraphAlignment( 2 );

		if( !password )
		{
			font->draw( label + text, x, y, w, h, color );
		}
		else
		{
			// Could probably be done better...
			std::string buffer;
			buffer = text;

			if( active )
			{
				buffer.erase( cursorPos, 1 );
				buffer.replace( 0, buffer.size(), buffer.size(), '*' );
				buffer.insert( cursorPos, 1, '|' );
			}
			else
			{
				buffer.replace( 0, buffer.size(), buffer.size(), '*' );
			}

			font->draw( buffer, x, y, w, h, color );
		}

		font->setParagraphAlignment( oldAlign );
	}
}

void InputBox::setPos( float x, float y, float w, float h )
{
	pos.left	= x;
	pos.top		= y;
	pos.right	= x + w;
	pos.bottom	= y + h;
}

D2D1_RECT_F InputBox::getPos()
{
	return pos;
}

void InputBox::setColor( float r, float g, float b, float a )
{
	color = D3DXCOLOR( r, g, b, a );
}

void InputBox::setVisible( bool status )
{
	visible = status;
}

bool InputBox::isVisible()
{
	return visible;
}

void InputBox::setActive( bool status )
{
	if( active && !status )
	{
		text.erase( cursorPos, 1 );
	}
	else if( !active && status )
	{
		text.insert( text.size(), 1, '|' );
		cursorPos = text.find('|');
	}

	active = status;
}

bool InputBox::isActive()
{
	return active;
}

void InputBox::allowSpecialChars( bool status )
{
	allowSpecial = status;
}

void InputBox::setPassword( bool status )
{
	password = status;
}
