
#ifndef PROFILESCENE_H
#define PROFILESCENE_H

#include "Scene.h"
#include "Framework.h"
#include "ItemInfo.h"
class NetworkSubsystemClient;
class AchievementInfo;

class ProfileScene : public Scene
{
	struct AccountInfo
	{
		unsigned int	playerAge;
		unsigned int	prevItemAt;
		unsigned int	nextItemAt;
		unsigned int	nrUnlockedCores;
		unsigned int	nrUnlockedModules;
		unsigned int	nrValidCores;
		unsigned int	nrValidModules;
	};

	typedef std::map< unsigned int, ItemInfo > Items;
	typedef std::vector< AchievementInfo > Achievements;

public:
	ProfileScene();
	ProfileScene(LogicHandler* handler, Renderer* rend, Window* wndw);
	virtual ~ProfileScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	void onFocus();
	void onFocusLost();

	bool onInput(int sym, int state);
	bool onMouseMovement(int x, int y);

	void onRender2D();
	struct Stage2D : public Stage
	{
		ProfileScene * me;
		bool visible;

		Stage2D(ProfileScene * me) : me(me), visible(true)
		{
		}

		void apply()
		{
			if(visible)
				me->onRender2D();
		}
	};

	void onRender2DPost();
	struct Stage2DPost : public Stage
	{
		ProfileScene * me;
		bool visible;

		Stage2DPost(ProfileScene * me) : me(me)
		{
		}

		void apply()
		{
			if(visible)
				me->onRender2DPost();
		}
	};

	void onAccountUpdate(AccountPacket* packet);

private:
	void fetchItems();

	void buildLayers();
	void clearLayers();

private:
	//For lua
	static ProfileScene*	me;
	static EventLua::Object	getAccountInfo();
	static EventLua::Object getAccountCores();
	static EventLua::Object getAccountModules();
	static EventLua::Object getAchievement(unsigned int index);
	static int				getNrOfAchievements();


private:
	Stage2D *	layer2d;
	Stage2DPost * layerPost;

	Layer *		layer;
	Camera		camera;

	Timer		luaTimer;

	NetworkSubsystemClient* network;

	AccountInfo	accountInfo;
	Items		items;

	std::vector<std::string>	newItems;
	std::vector<std::string>	newAchievements;

	Achievements achievements;
};

#endif