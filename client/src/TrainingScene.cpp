
#include "TrainingScene.h"

#include "LogicHandler.h"
#include "GameHandler.h"
#include "MemLeaks.h"

#include "Object.h"
#include "PhysicsComponent.h"
#include "RenderComponent.h"
#include "AnimationComponent.h"
#include "EffectComponent.h"
#include "CoreComponent.h"
#include "ControllerComponent.h"
#include "AIComponent.h"

#include "QueryEvent.h"

#include "Module.h"


TrainingScene::TrainingScene(Renderer* rend, Window* wndw, GameHandler* game, RenderModule* module, Font2D* font)
	: Scene(game->getLogic(), rend, wndw), game(game), renderModule(module)
{
	network = NULL;

	camera = renderModule->getCamera();

	camera->setPosition(Vector3(0,200,-80));
	camera->setLookAt(Vector3(0, 10.0f, 0));
	camera->setClipRange(40, 1000);

	camera->setProjection3D(70, 16 / 9);

	cameraDest = camera->getPosition();

	layer2d = New Stage2D(this, font);

	me = this;

	player = NULL;

	// register some global functions to lua
	EventLua::Object g = game->getScript()->getGlobals();
	g.bind("getPlayerSkillSlots", &getPlayerSkillSlots);
	g.bind("getAISkillSlots", &getAISkillSlots);
	g.bind("getPlayerLife", &getPlayerLife);
	g.bind("getPlayerMana", &getPlayerMana);
	g.bind("getAILife", &getAILife);
	g.bind("getAIMana", &getAIMana);
	g.bind("getPlayerSkillAt", &getPlayerSkillAt);
	g.bind("getAISkillAt", &getAISkillAt);
}

TrainingScene::~TrainingScene()
{
	Delete layer2d;
}

void TrainingScene::update()
{
	updateMouseWorldPosition();

	playerCont->setMousePosition(mouseWorldPosition);
}

void TrainingScene::render()
{
	updateCamera();

	renderModule->preFrame();
}

void TrainingScene::onEnter()
{
	buildLayers();

	camera->setPosition(Vector3(0, 200, -80));

	preparePlayer();
	prepareAI();

	renderModule->updateTerrain();
	renderModule->bind();

	renderer->insertStage(1000, layer2d);

	// onEnter-script
	File file;
	FileSystem::get("scripts/training.lua", &file);
	parent->getScript()->execute(file.data, file.size);

	parent->getScript()->getGlobals()["onTrainingEnter"]();

	luaTimer.restart();
	tempTimer.restart();
}

void TrainingScene::onLeave()
{
	game->destroyObject(player);
	game->destroyObject(AI);

	player	= NULL;
	AI		= NULL;

	clearLayers();
}

bool TrainingScene::onInput(int sym, int state)
{
	if(state == 1)
	{
		if( sym == VK_ESCAPE )
			game->gotoSceneOnUpdate("MainMenu");
	}

	if (layer2d->visible == true)
		parent->getScript()->getGlobals()["onTrainingInput"]( sym, state );

	return false;
}

bool TrainingScene::onMouseMovement(int x, int y)
{
	return false;
}

bool TrainingScene::onCreateObject(Object* object)
{
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();

	if (rendcomp)
	{
		renderModule->addObject(rendcomp);
	}	

	AnimationComponent* animcomp = object->findComponent<AnimationComponent>();

	if (animcomp)
	{
		renderModule->addLinkedParticle(animcomp);
	}

	EffectComponent* effcomp = object->findComponent<EffectComponent>();

	if (effcomp)
	{

		PhysicsComponent * com = object->findComponent<PhysicsComponent>();
		if (com)
		{
			//std::cout << "spawn effect " << com->getPosition() << std::endl;

			Vector3 normal = Vector3(0, 1, 0); // riktning p� terr�ngen/surface
			normal.normalize();

			renderModule->spawnEffect(effcomp->getName(), com->getPosition() + Vector3(0, 10, 0), normal);
		}
	}

	return true;
}

bool TrainingScene::onDestroyObject(Object* object)
{
	return true;
}

void TrainingScene::onRender2D(Font2D* font)
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/training.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout << parent->getScript()->getLastError() << std::endl;
	}
	parent->getScript()->getGlobals()["onTrainingRender"]();
}


bool TrainingScene::updateMouseWorldPosition()
{
	Ray ray;

	Vector2 mouseVector;
	window->getMouseVector(&mouseVector.x, &mouseVector.y);

	camera->pick(mouseVector, &ray);

	IntersectResult res;
	Renderable* rend = (Renderable*)renderModule->getTerrainLayer()->pick(ray, &res);
	if(rend)
	{
		//Mouse hits height map

		Vector3 resultpos = ray.origin + ray.direction * res.dist;

		mouseWorldPosition = resultpos;

		return true;
	}
	else
	{
		//Mouse is outside of height map

		camera->pick(mouseVector, &ray);

		Plane plane;
		plane.a = 0;
		plane.b = 1;
		plane.c = 0;
		plane.d = 0;

		if(Intersect::intersect(ray, plane, &res))
		{
			Vector3 resultpos = ray.origin + ray.direction * res.dist;

			mouseWorldPosition = resultpos;
		}

		return false;
	}
}

void TrainingScene::preparePlayer()
{
	player = game->getObject("Player");

	playerPhys = player->findComponent<PhysicsComponent>();
	playerPhys->position.x = -100;

	playerCont = player->findComponent<ControllerComponent>();
	playerCont->setSpeed(10.0f);
	playerCont->subscribe();

	playerCore = player->findComponent<CoreComponent>();
	playerCore->revive();
	playerCore->lifeRegen.setBase(10);
	playerCore->manaRegen.setBase(30);
	playerCore->setSlots(4, 4);
	playerCore->addModule(0, 0, "Fire Bolt");
	playerCore->team = 1;
}

void TrainingScene::prepareAI()
{
	AI = game->getObject("AIPlayer");

	AIPhys = AI->findComponent<PhysicsComponent>();
	AIPhys->position.x = 100;

	AICont = AI->findComponent<AIComponent>();
	AICont->setSpeed(3.0f);

	AICore = AI->findComponent<CoreComponent>();
	AICore->revive();
	AICore->lifeRegen.setBase(10);
	AICore->manaRegen.setBase(30);
	AICore->setSlots(4, 4);
	AICore->addModule(0, 1, "Fire Bolt");
	AICore->team = 2;

	AICont->addGamePlayer(player);
	AICont->giveCommand("slot0", 1);
}

void TrainingScene::updateCamera()
{
	float multiplier = 0.1f;

	// read from script if found
	float len = 180;
	float limit = 60;
	float height = 230;
	int type = 0; // camera same look at
	float fov = 75;
	float down = 1.8f;

	static EventLua::State state;
	if (state.execute("../CameraSettings.lua"))
	{
		len = state.get("length").queryFloat();
		limit = state.get("limit").queryFloat();
		height = state.get("height").queryFloat();
		type = state.get("type").queryInteger();
		fov = state.get("fov").queryFloat();
		down = state.get("down").queryFloat();
	}

	//if(playerCore->isPlayerAlive())
	//{
	//	//Calculate the camera destination point from the player position and mouse world position
	//	//Vector2 mouseToPlayer = Vector2(mouseWorldPosition.x, mouseWorldPosition.z - 110) - Vector2(playerPhys->position.x, playerPhys->position.z);

	//	//Vector2 mouseToPlayer = playerPhys->position * 0.6f;

	//	if (type == 0)
	//	{
	//		cameraDest.x = playerPhys->position.x;
	//		cameraDest.y = playerPhys->position.y + height - playerPhys->offset.y;
	//		cameraDest.z = playerPhys->position.z - len;
	//	}
	//	else
	//	{

	//		Vector3 cn = playerPhys->position;
	//		cn.normalize();

	//		cameraDest.x = playerPhys->position.x + cn.x * len;
	//		cameraDest.y = playerPhys->position.y + height;
	//		cameraDest.z = playerPhys->position.z + cn.z * len;

	//		cameraLookDest.x = playerPhys->position.x - cn.x * len;
	//		cameraLookDest.y = playerPhys->position.y;
	//		cameraLookDest.z = playerPhys->position.z - cn.z * len;

	//		Vector3 cameraDest2 = cameraDest;
	//		cameraDest2.y = 0;

	//		if (cameraDest2.length() < limit)
	//		{
	//			cameraDest2.normalize();
	//			cameraDest = cameraDest2 * limit;

	//			cameraDest.y = height;
	//		}

	//		multiplier = 0.1f;
	//	}
	//}
	//else
	//{
	//Calculate the camera destination point from the mouse world position only
	camera->setPosition(Vector3(0, 460, -350));
	camera->setLookAt(Vector3(0, 0, 0));
	multiplier = 0.0f;


	//}



	//Move the camera towards the destination point
	Vector3 destination = cameraDest - camera->getPosition();
	camera->setPosition(camera->getPosition() + (destination * multiplier));

	cameraLookAt += (cameraLookDest - cameraLookAt) * multiplier;

	if (type == 1)
	{
		camera->setLookAt(cameraLookAt);
	}
	else
	{
		camera->setOrientation(Vector3(0, -down, 1.0f));
	}

	camera->setProjection3D(fov, 16 / 9.0f);
	camera->update();
}


TrainingScene* TrainingScene::me = NULL;

int TrainingScene::getPlayerSkillSlots()
{
	return me->playerCore->getSkillSlots();
}

int TrainingScene::getAISkillSlots()
{
	return me->AICore->getSkillSlots();
}

EventLua::Object TrainingScene::getPlayerLife()
{
	std::stringstream ss;
	ss << "TEMP_TABLE_PLAYER_LIFE";
	EventLua::Object rtn = me->parent->getScript()->newTable(ss.str().c_str());

	QueryEvent<Vector3> q(CORE_HEALTH);
	me->player->sendInternal(&q);

	if (!q.isSet())
		return rtn;

	rtn["current"] = q.getData().x;
	rtn["max"] = q.getData().y;

	return rtn;
}

EventLua::Object TrainingScene::getPlayerMana()
{
	std::stringstream ss;
	ss << "TEMP_TABLE_PLAYER_MANA";
	EventLua::Object rtn = me->parent->getScript()->newTable(ss.str().c_str());

	QueryEvent<Vector3> q(CORE_MANA);
	me->player->sendInternal(&q);

	if (!q.isSet())
		return rtn;

	rtn["current"] = q.getData().x;
	rtn["max"] = q.getData().y;

	return rtn;
}

EventLua::Object TrainingScene::getAILife()
{
	std::stringstream ss;
	ss << "TEMP_TABLE_PLAYER_LIFE";
	EventLua::Object rtn = me->parent->getScript()->newTable(ss.str().c_str());

	QueryEvent<Vector3> q(CORE_HEALTH);
	me->AI->sendInternal(&q);

	if (!q.isSet())
		return rtn;

	rtn["current"] = q.getData().x;
	rtn["max"] = q.getData().y;

	return rtn;
}

EventLua::Object TrainingScene::getAIMana()
{
	std::stringstream ss;
	ss << "TEMP_TABLE_PLAYER_MANA";
	EventLua::Object rtn = me->parent->getScript()->newTable(ss.str().c_str());

	QueryEvent<Vector3> q(CORE_MANA);
	me->AI->sendInternal(&q);

	if (!q.isSet())
		return rtn;

	rtn["current"] = q.getData().x;
	rtn["max"] = q.getData().y;

	return rtn;
}

const char* TrainingScene::getPlayerSkillAt( int slot )
{
	Module* mod = me->playerCore->getModule( slot );

	if( mod )
		return mod->name.c_str();

	return NULL;
}

const char* TrainingScene::getAISkillAt( int slot )
{
	Module* mod = me->AICore->getModule( slot );

	if( mod )
		return mod->name.c_str();

	return NULL;
}