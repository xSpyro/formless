
#include "Client.h"
#include "LogicHandler.h"

#include "UmlScene.h"



UmlScene::UmlScene()
	: Scene()
{

}

UmlScene::UmlScene(LogicHandler* handler, Renderer* rend, Window* wndw, Font2D* font)
	: Scene(handler, rend, wndw), font(font)
{
	me = this;


	layer2d = new Stage2D(this);
	layer2dBase = new Stage2DBase(this);
	layer2dPost = new Stage2DPost(this);

	print = renderer->texture()->create(1280, 720, TEXTURE_DEFAULT);
}

UmlScene::~UmlScene()
{
	delete layer2d;
}

void UmlScene::update()
{

}

void UmlScene::render()
{
	if (printTimer.accumulate(1.0f))
	{
		print->save("uml.png");
	}

	print->setClearColor(Color(0, 0, 0, 0));
	print->clear();
}

void UmlScene::onEnter()
{
	//renderer->insertLayer(10, layer);
	//renderer->insertStage(2000, layer2d);
	buildLayers();

	luaTimer.restart();
}

void UmlScene::onLeave()
{

	clearLayers();
	//renderer->clearStage(10);
	//renderer->clearStage(2000);
}

bool UmlScene::onInput(int sym, int state)
{

	return true;
}

bool UmlScene::onMouseMovement(int x, int y)
{
	Scene::onMouseMovement( x, y );
	return true;
}

void UmlScene::onRender2D()
{
	// call lua here

	
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/uml.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout<< parent->getScript()->getLastError();
	}

	font->begin();
	parent->getScript()->getGlobals()["onUmlRender"]();
	
}

void UmlScene::onRender2DBase()
{
	parent->getScript()->getGlobals()["onUmlRenderBase"]();
}

void UmlScene::onRender2DPost()
{
	font->end();
}


UmlScene * UmlScene::me = NULL;



void UmlScene::buildLayers()
{
	clearLayers();

	renderer->insertStage(1200, layer2dBase);

	renderer->insertStateChange(1500, print);
	renderer->insertStage(2000, layer2d);


	renderer->insertStage(6000, layer2dPost);

	renderer->insertStateChange(7000, NULL);
	renderer->insertOverlay(8000, print, NULL);
}

void UmlScene::clearLayers()
{
	renderer->clearStage(2000);
}
