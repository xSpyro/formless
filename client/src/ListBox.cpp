#include <iostream>
#include <sstream>
#include "ListBox.h"
#include "Graphics.h"

ListBox::ListBox( Font2D* font, Graphics* graphics, void (*c)(const std::string &) )
	: TextBox( font ), graphics( graphics )
{
	selectedLine	= 0;
	timer			= 0;
	callback		= c;
}

ListBox::~ListBox()
{
}

void ListBox::onInput( int key, int state )
{
	if( visible && active && state != 0 )
	{
		switch( key )
		{
		case VK_UP:
			{
				if( selectedLine > 0 )
				{
					selectedLine--;

					if( (int)selectedLine < atLine )
					{
						atLine--;
						rebuildBuffer = true;
					}
				}
			}
			break;

		case VK_DOWN:
			{
				if( selectedLine < data.size() - 1 )
				{
					selectedLine++;

					if( (int)selectedLine > atLine + visibleLines - 1 )
					{
						atLine++;
						rebuildBuffer = true;
					}
				}
			}
			break;

		default:
			break;
		}
	}
}

void ListBox::onClick( float x, float y )
{
	if( visible )
	{
		if( x > pos.left && x < pos.right &&
			y > pos.top && y < pos.bottom )
		{
			unsigned int clickedLine = (unsigned int)((y - pos.top - 2) / lineHeight + atLine);

			if( clickedLine < data.size() )
			{
				selectedLine = clickedLine;

				if( timer > 0 )
				{
					timer = 0;

					if( callback != NULL )
					{
						callback( listData[ data[selectedLine] ] );
					}
				}
				else
				{
					timer = 20;
				}
			}

			setActive( true );
		}
		else
		{
			timer = 0;
			setActive( false );
		}
	}
}

void ListBox::bindCallback( void (*callback)(const std::string &) )
{
	this->callback = callback;
}

void ListBox::addItem( const std::string & item, const std::string & data )
{
	this->data.push_back( item );
	listData[ item ] = data;
	rebuildBuffer = true;
}

void ListBox::setSelectedItem( const std::string & item, const std::string & data )
{
	ListData::iterator it = listData.find(this->data[selectedLine]);


	if ( it != listData.end() )
	{
		listData.erase(item);

		this->data[selectedLine] = item;

		listData[item] = data;

		rebuildBuffer = true;
	}

}

std::string ListBox::getSelectedItem()
{
	std::string result;

	if( selectedLine >= 0 && selectedLine < data.size() )
	{
		result = data[ selectedLine ];
		result = listData[ result ];
	}

	return result;
}

unsigned int ListBox::getSize()
{
	return data.size();
}

bool ListBox::selectItem( const std::string & str )
{
	bool result = false;

	unsigned int i = 0;
	while( i < data.size() && data.at(i) != str )
	{
		i++;
	}

	if( i < data.size() )
	{
		selectedLine = i;

		if( selectedLine > atLine + visibleLines - 1 )
		{
			atLine = selectedLine - visibleLines + 1;
		}
		
		if( (int)selectedLine < atLine )
		{
			atLine = selectedLine;
		}

		rebuildBuffer = true;
		result = true;
	}

	return result;
}

void ListBox::update()
{
	TextBox::update();

	if( timer > 0 )
	{
		timer--;
	}
}

void ListBox::render()
{
	TextBox::render();

	if( font != NULL && visible && !data.empty() )
	{
		if( (int)selectedLine >= atLine && (int)selectedLine < atLine + visibleLines )
		{
			int selection = selectedLine - atLine;

			float x = pos.left;
			float y = pos.top;
			float w = pos.right - pos.left;
			float h = pos.bottom - pos.top;

			graphics->drawSprite( "bg.png", x, y + lineHeight * selection + 2, w, lineHeight );
		}
	}
}

void ListBox::updateBuffer()
{
	TextBox::updateBuffer();

	if( data.size() == 0 )
	{
		selectedLine = 0;
	}
}
