
#include "RequestInterface.h"

#include "Client.h"
#include "CreateAccountScene.h"
#include "LogicHandler.h"

#include "LoginForm.h"

#include "MemLeaks.h"

#include "CommonPackets.h"

CreateAccountScene::CreateAccountScene()
	: Scene()
{

}

CreateAccountScene::CreateAccountScene(LogicHandler* handler, Renderer* rend, Window* wndw, Font2D* font)
	: Scene(handler, rend, wndw)
{
	me = this;
	layer = renderer->createLayer();

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);
	camera.setProjection3D(45, 16 / 9.0f);

	// camera set

	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));


	layer2d = New Stage2D(this);
	layer2d->visible = true;

	form = New LoginForm( font );
	form->setUserPos( 450 + 8, 250, 380 - 16, 40 );
	form->setPassPos( 450 + 8, 340, 380 - 16, 40 );

	// register some global functions to lua
	EventLua::Object g = parent->getScript()->getGlobals();
	//g.bind( "setUserPos", &form->setUserPos );
	//g.bind( "setPassPos", &form->setPassPos );
	//g.bind( "selectLoginForm", &form->select );
	//g.bind( "login", &login );
	g.bind( "createAccount", &createUser );
	//g.bind("addMod", &addMod);
	//g.bind( "loadUser", &loadUser );
	//g.bind( "saveUser", &saveUser );

	File file;
	FileSystem::get("scripts/createaccount.lua", &file);
	parent->getScript()->execute(file.data, file.size);
}

CreateAccountScene::~CreateAccountScene()
{
	delete layer2d;
}

void CreateAccountScene::update()
{
	camera.update();
}

void CreateAccountScene::render()
{

}

void CreateAccountScene::onEnter()
{
	//renderer->insertLayer(10, layer);
	//renderer->insertStage(2000, layer2d);
	buildLayers();

	luaTimer.restart();
}

void CreateAccountScene::onLeave()
{
	clearLayers();
	//renderer->clearStage(10);
	//renderer->clearStage(2000);

	form->clear();

	parent->getScript()->getGlobals()["onCreateAccountLeave"]();
}

bool CreateAccountScene::onInput(int sym, int state)
{
	if(state != 0)
	{
		if (sym == 'U')
		{
			EventLua::Object reset = parent->getScript()->getGlobals().get("reset");
			reset.call();
		}

		if( sym == VK_RETURN )
		{
			//createUser();
			//if( login() )
			//	parent->deactivateSceneOnUpdate("Login");
		}

		if( sym == VK_ESCAPE )
		{
			form->clear();
			parent->deactivateSceneOnUpdate("CreateAccount");
		}

		if( sym == MB_LEFT )
		{
			form->onClick( (float)mx, (float)my );
		}
	}

	form->onInput( sym, state );
	parent->getScript()->getGlobals()["onCreateAccountInput"](sym, state);

	return true;
}

bool CreateAccountScene::onMouseMovement(int x, int y)
{
	Scene::onMouseMovement( x, y );
	return true;
}

void CreateAccountScene::onRender2D()
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/createaccount.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout<< parent->getScript()->getLastError();
	}

	form->render();
	parent->getScript()->getGlobals()["onCreateAccountRender"]();
}

CreateAccountScene * CreateAccountScene::me = NULL;

EventLua::Object CreateAccountScene::createUser()
{
	std::string str = "TEMP_TABLE_CREATEUSER";
	EventLua::Object rtn = me->parent->getScript()->newTable(str.c_str());

	std::string user;
	std::string pass;
	me->form->getData( user, pass );

	MasterRequest request;
	CreateUserPacket packet;
	packet.setUsername(user);
	packet.setPassword(pass);

	bool success = false;

	RawPacket result;
	if( request( packet, sizeof(packet), result ) )
	{
		CreateUserPacket usr = *reinterpret_cast<CreateUserPacket*>(result.data);
		rtn["success"]	= usr.success;
		rtn["master"]	= true;

		success			= usr.success;
	}
	else
	{
		rtn["success"]	= false;
		rtn["master"]	= false;
	}

	if( success )
	{
		login();
	}

	return rtn;
}

bool CreateAccountScene::login()
{
	bool result = false;
	std::string user;
	std::string pass;

	me->form->getData( user, pass );

	MasterRequestAsync request;
	MasterAuthPacket packet;
	packet.setLogin( user );
	packet.setPassword( pass );

	result = request( packet, sizeof(packet) );
	if( result )
	{
		saveUser( user.c_str(), pass.c_str() );
	}

	return result;
}

void CreateAccountScene::saveUser(const char* username, const char* password)
{
	std::ofstream out;

	out.open( "../resources/CurrentUser.txt", std::ios_base::trunc | std::ios_base::out );

	if( out.is_open() )
	{
		out << username << std::endl;
		out << password << std::endl;
	}

	out.close();
}

void CreateAccountScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer(10, layer);
	renderer->insertStage(2000, layer2d);
}

void CreateAccountScene::clearLayers()
{
	renderer->clearStage(10);
	renderer->clearStage(2000);
}
