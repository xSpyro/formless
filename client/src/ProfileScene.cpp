
#include "Client.h"

#include "ProfileScene.h"
#include "LogicHandler.h"
#include "MemLeaks.h"

#include "NetworkSubsystemClient.h"

#include "RequestInterface.h"

#include "AchievementInfo.h"

ProfileScene::ProfileScene()
{

}

ProfileScene::ProfileScene(LogicHandler* handler, Renderer* rend, Window* wndw)
	: Scene(handler, rend, wndw)
{
	me	= this;
	layer = renderer->createLayer();

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);
	camera.setProjection3D(45, 16 / 9.0f);

	// camera set

	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));

	layer2d = New Stage2D(this);
	layer2d->visible = true;

	layerPost = New Stage2DPost(this);


	File file;
	FileSystem::get("scripts/profile.lua", &file);
	parent->getScript()->execute(file.data, file.size);

	EventLua::Object g = parent->getScript()->getGlobals();
	g.bind("getAccountInfo", &getAccountInfo);
	g.bind("getAccountCores", &getAccountCores);
	g.bind("getAccountModules", &getAccountModules);
	g.bind("getAchievement", &getAchievement);
	g.bind("getNrOfAchievements", &getNrOfAchievements);

	network		= NULL;

	accountInfo.playerAge			= 0;
	accountInfo.nextItemAt			= 0;
	accountInfo.prevItemAt			= 0;
	accountInfo.nrUnlockedCores		= 0;
	accountInfo.nrUnlockedModules	= 0;
	accountInfo.nrValidCores		= 0;
	accountInfo.nrValidModules		= 0;
}

ProfileScene::~ProfileScene()
{
	delete layer2d;
}

void ProfileScene::update()
{
	camera.update();
}

void ProfileScene::render()
{

}

void ProfileScene::onEnter()
{
	//renderer->insertLayer(10, layer);
	//renderer->insertStage(1000, layer2d);
	//renderer->insertStage(2000, layerPost);
	buildLayers();

	fetchItems();

	// will only ever be client network subsystem
	network = reinterpret_cast < NetworkSubsystemClient* > (parent->getSubsystem("Network"));
	Client* client = (Client*)window;
	unsigned int userID = client->getUserID();
	network->sendUserInfoRequest(userID);

	File file;
	FileSystem::get("scripts/profile.lua", &file);
	parent->getScript()->execute(file.data, file.size);

	parent->getScript()->getGlobals()["onProfileEnter"]();

	layer2d->visible = true;

	luaTimer.restart();
}

void ProfileScene::onLeave()
{
	clearLayers();

	newItems.clear();
	achievements.clear();
	newAchievements.clear();

	layer2d->visible = false;
}

void ProfileScene::onFocus()
{
	buildLayers();

	layer2d->visible = true;
}

void ProfileScene::onFocusLost()
{
	layer2d->visible = false;
}

bool ProfileScene::onInput(int sym, int state)
{
	parent->getScript()->getGlobals()["onProfileInput"](sym, state);
	return true;
}

bool ProfileScene::onMouseMovement(int x, int y)
{
	return true;
}

void ProfileScene::onRender2D()
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/profile.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout<< parent->getScript()->getLastError();
	}

	parent->getScript()->getGlobals()["onProfileRender"]();
}

void ProfileScene::onRender2DPost()
{
	parent->getScript()->getGlobals()["onProfileRenderPost"]();
}

void ProfileScene::onAccountUpdate(AccountPacket* packet)
{
	accountInfo.playerAge			= packet->age;
	accountInfo.nextItemAt			= packet->nextItemAt;
	accountInfo.prevItemAt			= packet->prevItemAt;
	accountInfo.nrUnlockedCores		= packet->nrUnlockedCores;
	accountInfo.nrUnlockedModules	= packet->nrUnlockedModules;
	accountInfo.nrValidCores		= packet->nrValidCores;
	accountInfo.nrValidModules		= packet->nrValidModules;

	std::string itemname = packet->itemname;
	if(itemname != "")
	{
		newItems.push_back(itemname);
	}

	std::string achievementName = packet->achievementName;
	if(achievementName != "")
	{ 
		newAchievements.push_back(achievementName);
	}
}


void ProfileScene::fetchItems()
{
	MasterRequest request;
	unsigned int userID = ((Client*)window)->getUserID();

	InventoryRequestPacket packet;
	packet.uid = userID;
	packet.companion = false;
	RawPacket result;

	try
	{
		if (request(packet, sizeof(packet), result))
		{	
			items.clear();
			char * offset = &reinterpret_cast < char* > (result.data)[RawPacket::HeaderSize];
			unsigned int results = *reinterpret_cast < unsigned int* > (offset);
			offset += sizeof(unsigned int); // skip the int

			for (unsigned int i = 0; i < results; ++i)
			{
				ItemInfo * item = reinterpret_cast < ItemInfo* > (offset);
				items[ item->uid ] = *item;
				offset += sizeof(ItemInfo);
			}
		}
	}
	catch ( std::exception e )
	{
	}


	AchievementListRequestPacket achPacket;
	achPacket.user_id = userID;
	RawPacket achResult;
	try
	{
		if( request(achPacket, sizeof(achPacket), achResult) )
		{
			char * offset = &reinterpret_cast < char* > (achResult.data)[RawPacket::HeaderSize];
			unsigned int results = *reinterpret_cast < unsigned int* > (offset);
			offset += sizeof(unsigned int); // skip the int

			for (unsigned int i = 0; i < results; ++i)
			{
				AchievementInfo* achStureVadDuAerTarvlig = reinterpret_cast < AchievementInfo* > (offset);
				achievements.push_back(*achStureVadDuAerTarvlig);
				offset += sizeof(AchievementInfo);
			}
		}
	}
	catch (std::exception e)
	{
	}

	////Replace all the spaces in items name with underscores, to be able to use them for lookup in lua tables
	//for(Items::iterator it = items.begin(); it != items.end(); ++it)
	//{
	//	std::string str = it->second.name;
	//	size_t position = str.find(" ");
	//	while( position != std::string::npos )
	//	{
	//		str.replace(position, 1, "_");
	//		position = str.find(" ");
	//	}
	//	strcpy_s(it->second.name, 64, str.c_str());
	//}
}

void ProfileScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer(10, layer);
	renderer->insertStage(1000, layer2d);
	renderer->insertStage(2000, layerPost);
}

void ProfileScene::clearLayers()
{
	renderer->clearStage(10);
	renderer->clearStage(1000);
	renderer->clearStage(2000);
}

ProfileScene* ProfileScene::me = NULL;

EventLua::Object ProfileScene::getAccountInfo()
{
	std::stringstream ss;
	ss << "TEMP_TABLE_ACCOUNT_INFO";
	EventLua::Object rtn = me->parent->getScript()->newTable(ss.str().c_str());

	rtn["prevItemAt"]		= (int)me->accountInfo.prevItemAt;
	rtn["nextItemAt"]		= (int)me->accountInfo.nextItemAt;
	rtn["age"]				= (int)me->accountInfo.playerAge;
	rtn["unlockedCores"]	= (int)me->accountInfo.nrUnlockedCores;
	rtn["unlockedModules"]	= (int)me->accountInfo.nrUnlockedModules;
	rtn["validCores"]		= (int)me->accountInfo.nrValidCores;
	rtn["validModules"]		= (int)me->accountInfo.nrValidModules;

	return rtn;
}

EventLua::Object ProfileScene::getAccountCores()
{
	std::stringstream ss;
	ss << "TEMP_TABLE_ACCOUNT_CORES";
	EventLua::Object rtn = me->parent->getScript()->newTable(ss.str().c_str());

	for(Items::iterator it = me->items.begin(); it != me->items.end(); ++it)
	{
		if(it->second.type == 1)	//Type 1 is a cores according to the lua system
		{
			bool newItem = false;
			for(unsigned int i = 0; i < me->newItems.size() && newItem == false; i++)
			{
				if(me->newItems[i] == it->second.name)
					newItem = true;
			}

			if(newItem)
				rtn[it->second.name]	= 1;
			else
				rtn[it->second.name]	= 2;
		}
	}

	return rtn;
}

EventLua::Object ProfileScene::getAccountModules()
{
	std::stringstream ss;
	ss << "TEMP_TABLE_ACCOUNT_MODULES";
	EventLua::Object rtn = me->parent->getScript()->newTable(ss.str().c_str());

	for(Items::iterator it = me->items.begin(); it != me->items.end(); ++it)
	{
		if(it->second.type == 2 || it->second.type == 3)	//Type 2 is active modules and type 3 is passive modules according to the lua system
		{
			bool newItem = false;
			for(unsigned int i = 0; i < me->newItems.size() && newItem == false; i++)
			{
				if(me->newItems[i] == it->second.name)
					newItem = true;
			}

			if(newItem)
				rtn[it->second.name]	= 1;
			else
				rtn[it->second.name]	= 2;
		}
	}

	return rtn;
}

EventLua::Object ProfileScene::getAchievement(unsigned int index)
{
	std::stringstream ss;
	ss << "TEMP_TABLE_ACHIEVEMENT";
	EventLua::Object rtn = me->parent->getScript()->newTable(ss.str().c_str());

	if(index < me->achievements.size())
	{
		rtn["name"]			= me->achievements[index].getName();
		rtn["description"]	= me->achievements[index].getDescription();
		
		if(me->achievements[index].unlocked)
		{
			bool newAch = false;
			for(unsigned int i = 0; i < me->newAchievements.size() && newAch == false; i++)
			{
				if(me->newAchievements[i] == me->achievements[index].getName())
					newAch = true;
			}

			if(!newAch)
				rtn["unlocked"]		= 1;
			else
				rtn["unlocked"]		= 2;
		}
		else
			rtn["unlocked"]		= 0;
	}

	return rtn;
}

int ProfileScene::getNrOfAchievements()
{
	return (int)me->achievements.size();
}