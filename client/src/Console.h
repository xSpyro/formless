#ifndef CONSOLE__H
#define CONSOLE__H

#include <string>
#include <EventLua.h>

class Font2D;
class TextBox;
class InputBox;

class Console
{
public:
	Console( Font2D* font, EventLua::State* lua );
	~Console();

	void update();
	void render();
	void onInput( int key, int state );
	void setActive( bool status );
	bool isActive();

private:
	static Console* me;
	static void execute( const std::string & cmd );
	static void cprint( const char * str );
	static void setColor( float r, float g, float b, float a );

private:
	bool active;

	TextBox* log;
	InputBox* input;
	EventLua::State* lua;
	Font2D* font;
};

#endif
