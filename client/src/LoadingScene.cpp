
#include "Client.h"
#include "LoadingScene.h"
#include "GameHandler.h"

#include "MemLeaks.h"


LoadingScene::LoadingScene()
	: Scene()
{

}

LoadingScene::LoadingScene(GameHandler * game, Renderer* rend, Window* wndw, Font2D* font, Client * client)
	: Scene(game->getLogic(), rend, wndw), game(game), client(client)
{

	me = this;
	layer = renderer->createLayer();

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);
	camera.setProjection3D(45, 16 / 9.0f);

	// camera set

	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));


	layer2d = New Stage2D(this);
	layer2d->visible = true;



	// register some global functions to lua
	EventLua::Object g = parent->getScript()->getGlobals();

}

LoadingScene::~LoadingScene()
{
	delete layer2d;
}

void LoadingScene::update()
{
	camera.update();

	if (++loadOffset > 70)
	{
		std::cout << "start to load" << std::endl;
		client->loadingPostServerSelect();
		loadOffset = -999999;
	}


}

void LoadingScene::render()
{

}

void LoadingScene::onEnter()
{
	//std::cout << "has layer2d stage" << std::endl;
	//renderer->insertLayer(10, layer);
	//renderer->insertStage(1000, layer2d);
	buildLayers();

	luaTimer.restart();

	//LevelRequest* fr = New LevelRequest( game->getNetwork()->getService(), game->getNetwork()->getRemoteIp(), this );

	loadOffset = 0;
}

void LoadingScene::onLeave()
{
	clearLayers();
	//renderer->clearStage(10);
	//renderer->clearStage(1000);
}

bool LoadingScene::onInput(int sym, int state)
{

	return true;
}

bool LoadingScene::onMouseMovement(int x, int y)
{
	Scene::onMouseMovement( x, y );
	return true;
}

void LoadingScene::onRender2D()
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/loading.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout<< parent->getScript()->getLastError();
	}

	parent->getScript()->getGlobals()["onLoadingRender"]();
}

LoadingScene * LoadingScene::me = NULL;

void LoadingScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer(10, layer);
	renderer->insertStage(1000, layer2d);
}

void LoadingScene::clearLayers()
{
	renderer->clearStage(10);
	renderer->clearStage(1000);
}
