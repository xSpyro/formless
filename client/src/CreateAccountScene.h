
#ifndef CREATEACCOUNTSCENE_H
#define CREATEACCOUNTSCENE_H

#include "Scene.h"
#include "Framework.h"

class LoginForm;
class Font2D;

class CreateAccountScene : public Scene
{
public:

	CreateAccountScene();
	CreateAccountScene(LogicHandler* handler, Renderer* rend, Window* wndw, Font2D* font = NULL);
	virtual ~CreateAccountScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	void onRender2D();
	struct Stage2D : public Stage
	{
		CreateAccountScene * me;
		bool visible;

		Stage2D(CreateAccountScene * me) : me(me)
		{
		}

		void apply()
		{
			if(visible)
				me->onRender2D();
		}
	};

public:
	static EventLua::Object createUser();
	static bool login();
	static void saveUser(const char* username, const char* password);

private:
	//For lua
	static CreateAccountScene* me;

private:
	void buildLayers();
	void clearLayers();

	Stage2D *	layer2d;

	Layer *		layer;
	Camera		camera;

	LoginForm*	form;

	Timer		luaTimer;
};

#endif
