
#ifndef UML_SCENE_H
#define UML_SCENE_H

#include "Scene.h"
#include "Framework.h"

class LoginForm;
class Font2D;

class UmlScene : public Scene
{
public:

	UmlScene();
	UmlScene(LogicHandler* handler, Renderer* rend, Window* wndw, Font2D* font = NULL);
	virtual ~UmlScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	void onRender2DBase();
	struct Stage2DBase : public Stage
	{
		UmlScene * me;

		Stage2DBase(UmlScene * me) : me(me)
		{
		}

		void apply()
		{
			me->onRender2DBase();
		}
	};

	void onRender2D();
	struct Stage2D : public Stage
	{
		UmlScene * me;

		Stage2D(UmlScene * me) : me(me)
		{
		}

		void apply()
		{
			me->onRender2D();
		}
	};

	void onRender2DPost();
	struct Stage2DPost : public Stage
	{
		UmlScene * me;

		Stage2DPost(UmlScene * me) : me(me)
		{
		}

		void apply()
		{
			me->onRender2DPost();
		}
	};

private:
	//For lua
	static UmlScene* me;

private:

	void buildLayers();
	void clearLayers();

	Stage2D *	layer2d;
	Stage2DBase * layer2dBase;
	Stage2DPost * layer2dPost;

	Timer luaTimer;
	Timer printTimer;

	Texture * print;


public:
	Font2D* font;
};

#endif