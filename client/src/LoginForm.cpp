#include "LoginForm.h"
#include "InputBox.h"
#include "GameHandler.h"

LoginForm* LoginForm::me = 0;

LoginForm::LoginForm( Font2D* font )
{
	me = this;

	username = new InputBox( font );
	username->allowSpecialChars( false );
	username->setVisible( true );
	username->setActive( true );
	username->setMaxChars( 16 );
	username->setPos( 450+12, 250, 380-24, 40 );

	password = new InputBox( font );
	password->allowSpecialChars( false );
	password->setVisible( true );
	password->setActive( false );
	password->setMaxChars( 16 );
	password->setPassword ( true );
	password->setPos( 450+12, 340, 380-24, 40 );
}

LoginForm::~LoginForm()
{
	if( username )	delete username;
	if( password )	delete password;
}

void LoginForm::onInput( int key, int state )
{
	if( state != 0 )
	{
		switch( key )
		{
		case VK_TAB:
			if( username->isActive() )
			{
				username->setActive( false );
				password->setActive( true );
			}
			else
			{
				username->setActive( true );
				password->setActive( false );
			}
			break;
		default:
			break;
		}
	}

	username->onInput( key, state );
	password->onInput( key, state );
}

void LoginForm::onClick( float x, float y )
{
	username->onClick( x, y );
	password->onClick( x, y );
}

void LoginForm::render()
{
	username->render();
	password->render();
}

void LoginForm::clear()
{
	username->clear();
	password->clear();
}

void LoginForm::getData( std::string& user, std::string& pass )
{
	user = username->getText();
	pass = password->getText();
}

void LoginForm::setUserPos( float x, float y, float w, float h )
{
	me->username->setPos( x, y, w, h );
}

void LoginForm::setPassPos( float x, float y, float w, float h )
{
	me->password->setPos( x, y, w, h );
}