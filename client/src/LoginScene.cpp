
#include "RequestInterface.h"

#include "Client.h"
#include "LoginScene.h"
#include "LogicHandler.h"

#include "LoginForm.h"

#include "MemLeaks.h"

#include "CommonPackets.h"

LoginScene::LoginScene()
	: Scene()
{

}

LoginScene::LoginScene(LogicHandler* handler, Renderer* rend, Window* wndw, Font2D* font)
	: Scene(handler, rend, wndw)
{
	me = this;
	layer = renderer->createLayer();

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);
	camera.setProjection3D(45, 16 / 9.0f);

	// camera set

	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));


	layer2d = New Stage2D(this);
	layer2d->visible = true;

	form = New LoginForm( font );

	// register some global functions to lua
	EventLua::Object g = parent->getScript()->getGlobals();
	g.bind( "setUserPos", &form->setUserPos );
	g.bind( "setPassPos", &form->setPassPos );
	//g.bind( "selectLoginForm", &form->select );
	g.bind( "login", &login );
	g.bind( "createTestUser", &createUser );
	//g.bind("addMod", &addMod);
	g.bind( "loadUser", &loadUser );
	g.bind( "saveUser", &saveUser );

	File file;
	FileSystem::get("scripts/login.lua", &file);
	parent->getScript()->execute(file.data, file.size);
}

LoginScene::~LoginScene()
{
	delete layer2d;
}

void LoginScene::update()
{
	camera.update();
}

void LoginScene::render()
{

}

void LoginScene::onEnter()
{
	//renderer->insertLayer(10, layer);
	//renderer->insertStage(2000, layer2d);
	buildLayers();

	luaTimer.restart();
}

void LoginScene::onLeave()
{
	form->clear();

	clearLayers();
	//renderer->clearStage(10);
	//renderer->clearStage(2000);
}

bool LoginScene::onInput(int sym, int state)
{
	if(state != 0)
	{
		if (sym == 'U')
		{
			EventLua::Object reset = parent->getScript()->getGlobals().get("reset");
			reset.call();
		}

		if( sym == VK_RETURN )
		{
			if( login() )
				parent->deactivateSceneOnUpdate("Login");
		}

		if( sym == VK_ESCAPE )
		{
			form->clear();
			parent->deactivateSceneOnUpdate("Login");
		}

		if( sym == MB_LEFT )
		{
			form->onClick( (float)mx, (float)my );
		}
	}

	form->onInput( sym, state );
	parent->getScript()->getGlobals()["onLoginInput"](sym, state);

	return true;
}

bool LoginScene::onMouseMovement(int x, int y)
{
	Scene::onMouseMovement( x, y );
	return true;
}

void LoginScene::onRender2D()
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/login.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout<< parent->getScript()->getLastError();
	}

	form->render();
	parent->getScript()->getGlobals()["onLoginRender"]();
}

LoginScene * LoginScene::me = NULL;

bool LoginScene::login(const char* username, const char* password)
{
	bool result = false;
	std::string user;
	std::string pass;

	if( username == NULL || password == NULL )
		me->form->getData( user, pass );
	else
	{
		user = username;
		pass = password;
	}

	MasterRequestAsync request;
	MasterAuthPacket packet;
	packet.setLogin( user );
	packet.setPassword( pass );

	result = request( packet, sizeof(packet) );
	if( result )
	{
		saveUser( user.c_str(), pass.c_str() );
	}

	return result;
}

void LoginScene::createUser()
{
	std::string user;
	std::string pass;
	me->form->getData( user, pass );

	MasterRequest request;
	CreateUserPacket packet;
	packet.setUsername(user);
	packet.setPassword(pass);

	RawPacket result;
	request( packet, sizeof(packet), result );
}

void LoginScene::loadUser()
{
	std::ifstream in;

	in.open( "../resources/CurrentUser.txt" );

	if( in.is_open() )
	{
		std::string username;
		std::string password;

		in >> username;
		in >> password;

		if( !in.eof() && !username.empty() && !password.empty() )
			login(username.c_str(), password.c_str());
	}

	in.close();
}

void LoginScene::saveUser(const char* username, const char* password)
{
	std::ofstream out;

	out.open( "../resources/CurrentUser.txt", std::ios_base::trunc | std::ios_base::out );

	if( out.is_open() )
	{
		out << username << std::endl;
		out << password << std::endl;
	}

	out.close();
}

void LoginScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer(10, layer);
	renderer->insertStage(2000, layer2d);
}

void LoginScene::clearLayers()
{
	renderer->clearStage(10);
	renderer->clearStage(2000);
}
