#ifndef TUTORIAL_SCENE_H

#define TUTORIAL_SCENE_H

#include "Scene.h"

#include "Framework.h"

#include "ListBox.h"

class TutorialScene : public Scene
{

public:
	TutorialScene();
	TutorialScene(LogicHandler* handler, Renderer* rend, Window* wndw, Font2D* font, Graphics* graphics, Scene* const inventory);
	~TutorialScene();

	void update();
	void render();

	void onEnter();							// Defines what should be done upon entering the scene
	void onLeave();							// Defines what should be done upon leaving the scene
	void onLoad();							// Defines what happens after a load

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	bool onCreateObject(Object* object);
	bool onDestroyObject(Object* object);

	void onConnected(unsigned int id);

	void onFocus();
	void onFocusLost();

	void onRender2D();

	struct Stage2D : public Stage
	{
		TutorialScene * me;
		bool visible;

		Stage2D(TutorialScene * me) : me(me), visible(true)
		{
		}

		void apply()
		{
			if( visible )
				me->onRender2D();
		}
	};

private:
	void buildLayers();
	void clearLayers();

	Stage2D *	layer2d;
	Layer *		layer;
	Camera		camera;
	Timer		luaTimer;
	Scene*		inventory;
};


#endif