
#include "Client.h"

#include "IntroScene.h"

#include "Common.h"
#include "Object.h"
#include "LogicHandler.h"

#include "RenderComponent.h"
#include "GameHandler.h"

#include "MemLeaks.h"




IntroScene::IntroScene()
	: Scene()
{

}

IntroScene::IntroScene(GameHandler* game, Renderer* rend, Window* wndw)
	: Scene(game->getLogic(), rend, wndw), game(game)
{
	stage = new Stage2D(this);
	next = 0;

}

IntroScene::~IntroScene()
{
}

void IntroScene::update()
{
	if (++next > 200)
	{
		game->gotoSceneUsingLua("MainMenu");
		next = -9999;
	}
}

void IntroScene::render()
{
	//parent->getScript()->getGlobals()["onOptionsMenuRender"]();
}

void IntroScene::onEnter()
{
	//renderer->insertStage(1000, stage);
	buildLayers();
}

void IntroScene::onLeave()
{
	//renderer->clearStage(1000);
	clearLayers();
}

void IntroScene::onRender2D()
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/intro.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout << parent->getScript()->getLastError() << std::endl;
	}

	parent->getScript()->getGlobals()["onIntroRender"]();
}

void IntroScene::buildLayers()
{
	clearLayers();
	renderer->insertStage(1000, stage);
}

void IntroScene::clearLayers()
{
	renderer->clearStage(1000);
}
