

#ifndef FRAMEWORK_H
#define FRAMEWORK_H

#include "../../framework/src/Layer.h"
#include "../../framework/src/Renderable.h"

#include "../../framework/src/Renderer.h"
#include "../../framework/src/Window.h"

#include "../../framework/src/FileSystem.h"
#include "../../framework/src/Effect.h"

#include "../../framework/src/Viewport.h"

#include "../../framework/src/BaseShader.h"

#include "../../framework/src/Mesh.h"
#include "../../framework/src/MeshStream.h"
#include "../../framework/src/Vector3.h"
#include "../../framework/src/Vector2.h"
#include "../../framework/src/Color.h"
#include "../../framework/src/Matrix.h"
#include "../../framework/src/Plane.h"
#include "../../framework/src/Intersect.h"

#include "../../framework/src/Model.h"
#include "../../framework/src/ModelData.h"

#include "../../framework/src/Texture.h"
#include "../../framework/src/Surface.h"

#include "../../framework/src/ParticleSystem.h"

#include "../../framework/src/Input.h"

#include "../../framework/src/AnimationGroup.h"

#include "../../framework/src/HeightMap.h"
#include "../../framework/src/SkySphere.h"
#include "../../framework/src/Box.h"

#include "../../framework/src/IntersectResult.h"

#include "../../framework/src/ConstantBuffer.h"

#include "../../framework/src/Logger.h"
#include "../../framework/src/Timer.h"

#include "../../framework/src/AudioSystem.h"

#include "../../framework/src/ParticleEmitter.h"
#include "../../framework/src/Camera.h"

#include "../../framework/src/Font.h"

#include "../../framework/src/Particle.h"
#include "../../framework/src/ParticleBehavior.h"

#include "../../framework/src/Color.h"
#include "../../framework/src/Helpers.h"

#endif