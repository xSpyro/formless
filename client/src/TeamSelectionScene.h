#ifndef CHOOSE_TEAM_SCENE__H
#define CHOOSE_TEAM_SCENE__H

#include "Scene.h"
#include "Framework.h"

#include "PlayerInfo.h"

class GameHandler;
class NetworkSubsystem;
class TextBox;
class Font2D;
struct Statistics;

class TeamSelectionScene : public Scene
{
	typedef std::vector< PlayerInfo > Players;

public:

	TeamSelectionScene();
	TeamSelectionScene( LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game, Font2D* font = NULL );
	virtual ~TeamSelectionScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	void onFocus();
	void onFocusLost();

	bool onInput(int sym, int state);				// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);				// If return false give input to eventHandler

	void onRender2D();

	struct Stage2D : public Stage
	{
		TeamSelectionScene * me;
		bool visible;

		Stage2D(TeamSelectionScene * me) : me(me), visible(true)
		{
		}

		void apply()
		{
			if(visible)
			me->onRender2D();
		}
	};

private:
	void getPlayers();

	void buildLayers();
	void clearLayers();

private:

	//For lua
	static TeamSelectionScene* me;

private:

	GameHandler * game;
	Stage2D *	layer2d;

	Layer *		layer;

	NetworkSubsystem* net;

	Players		players;
	TextBox*	team0;
	TextBox*	team1;

	Timer		luaTimer;

	Statistics* stats;
};

#endif