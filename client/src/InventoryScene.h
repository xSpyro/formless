
#ifndef INVENTORYSCENE_H
#define INVENTORYSCENE_H

#include "Scene.h"
#include "Framework.h"

#include "ItemInfo.h"

class Object;
class GameHandler;
class CoreComponent;
class NetworkComponent;

class LobbyScene;

class InventoryScene : public Scene
{
	//typedef std::map< std::string, unsigned int > ItemID;
	typedef std::map< unsigned int, ItemInfo > Items;

public:

	InventoryScene();
	InventoryScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game);
	virtual ~InventoryScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	void onFocus();
	void onFocusLost();

	bool onInput(int sym, int state);				// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);				// If return false give input to eventHandler

	bool onCreateObject(Object* object);	// Do not call this manually!
	bool onDestroyObject(Object* object);	// Do not call this manually!

	void onRender2D();
	
	CoreComponent* const getCurrentPlayerCore();
	
	void setLayer2DVisibility(const bool state);
	
	struct Stage2D : public Stage
	{
		InventoryScene * me;
		bool visible;

		Stage2D(InventoryScene * me) : me(me), visible(true)
		{
		}

		void apply()
		{
			if(visible)
				me->onRender2D();
		}
	};
	
private:

	//For lua
	static InventoryScene* me;

	static void		addItem( const char* holder, unsigned int holderWidth, unsigned int itemid, int x, int y, EventLua::Object data );
	static void		removeItem( const char* holder, unsigned int itemid );
	static void		setCoreSlotsName( int nrSlots, int nrSkills, const char* name );
	static void		setCoreLifeMana( float life, float lifeRegen, float mana, float manaRegen );
	static void		setCoreSpeedWeight( float speed, float weight );
	static void		sendAttrPacket();
	static void		resetCore();
	static void		setCoreAttributes( EventLua::Object attributes );
	static EventLua::Object	getCoreAttributes();

	static void		loadProfile( unsigned int pid, const char* scene );
	static void		saveProfile( unsigned int pid );

	static void		dbgItem( const char* item );
	static void		refresh();

	static bool		isConnected();
	static int		getClientID();
	static int		getPlayerTeam();

private:
	void refreshItems();

	void buildLayers();
	void clearLayers();

private:

	GameHandler *	game;
	Stage2D *		layer2d;

	Layer *			layer;
	Camera			camera;

	Object*				player;
	CoreComponent*		playerCore;
	NetworkComponent*	playerNet;

	NetworkSubsystem* net;

	//ItemID itemID;
	Items		items;

	Timer		luaTimer;

	int			sound0;
	int			sound1;


	// proxy lobby scene
	LobbyScene * lobby;
};

#endif