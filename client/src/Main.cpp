#include "Client.h"




#ifdef _DEBUG
int main()
{
	boost::asio::io_service service;

	Client client(service, 1, NULL);
	if (client.startup())
	{
		client.run();
		client.shutdown();
	}
	
	return 0;
}
#else
#include <windows.h>

int WINAPI WinMain(HINSTANCE hinstance, HINSTANCE, LPSTR, int show)
{
	boost::asio::io_service service;

	Client client(service, show, hinstance);
	if (client.startup())
	{
		client.run();
		client.shutdown();
	}

	return 0;
}
#endif
