#include "TextBox.h"
#include <sstream>
#include <iostream>

TextBox::TextBox( Font2D* font )
	: font( font )
{
	atLine			= 0;
	lineHeight		= 1;
	visibleLines	= 0;
	nrLines			= 0;
	maxSize			= 50;
	active			= false;
	visible			= false;

	pos.left		= 0.0f;
	pos.top			= 0.0f;
	pos.right		= 0.0f;
	pos.bottom		= 0.0f;

	fade			= false;

	color			= D3DXCOLOR( 1.0f, 1.0f, 1.0f, 1.0f );

	determineLineheight();
	updateBuffer();
}

TextBox::~TextBox()
{
}

void TextBox::onInput( int key, int state )
{
	if( visible && active && state != 0 )
	{
		switch( key )
		{
		case VK_PRIOR:
			{
				if( atLine > 0 )
				{
					atLine -= visibleLines;

					if( atLine < 0 )
					{
						atLine = 0;
					}

					rebuildBuffer = true;
				}
			}
			break;

		case VK_NEXT:
			{
				if( nrLines > (int)visibleLines && (int)(atLine + visibleLines) < nrLines )
				{
					atLine += visibleLines;

					if( atLine > (int)(nrLines - visibleLines) )
					{
						atLine = (int)(nrLines - visibleLines);
					}

					rebuildBuffer = true;
				}
			}
			break;

		case VK_UP:
			{
				if( atLine > 0 )
				{
					atLine--;
					rebuildBuffer = true;
				}
			}
			break;

		case VK_DOWN:
			{
				if( nrLines > (int)visibleLines && (int)(atLine + visibleLines) < nrLines )
				{
					atLine++;
					rebuildBuffer = true;
				}
			}
			break;

		default:
			break;
		}
	}
}

void TextBox::onClick( float x, float y )
{
	if( visible )
	{
		if( x > pos.left && x < pos.right &&
			y > pos.top && y < pos.bottom )
		{
			setActive( true );
		}
		else
		{
			setActive( false );
		}
	}
}

void TextBox::addMessage( const std::string & msg )
{
	DWRITE_TEXT_METRICS metrics;
	metrics = font->getTextMetrics( msg, pos.right - pos.left );

	data.push_back( msg );

	nrLines += metrics.lineCount;

	// Limit the log
	if( data.size() > maxSize )
	{
		metrics = font->getTextMetrics( data.front(), pos.right - pos.left );
		data.erase( data.begin() );
		nrLines -= metrics.lineCount;
		//atLine -= metrics.lineCount;
	}

	determineLineheight();

	// Auto-scrolls to bottom when new message is added
	if( nrLines > (int)visibleLines )
	{
		atLine = nrLines - visibleLines;
	}
	else
	{
		atLine = 0;
	}

	rebuildBuffer = true;
}

void TextBox::getData( std::vector< std::string > & data )
{
	data = this->data;
}

std::string TextBox::getData( unsigned int index )
{
	std::string result;

	if( index < data.size() )
	{
		result = data[index];
	}

	return result;
}

void TextBox::update()
{
	determineLineheight();

	if( rebuildBuffer )
	{
		updateBuffer();
	}

	if( fade && clock() >= fadeDelay )
	{
		if( fadeTarget <= 0.4f && color.a <= 0.4f )
		{
			color.a		= 0.0f;
			visible		= false;
			active		= false;
			fade		= false;
		}
		else if( fadeTarget >= 0.8f && color.a >= 0.8f )
		{
			color.a		= 1.0f;
			fade		= false;
		}
		else
		{
			color.a		= color.a + (fadeTarget - color.a) * 0.07f;
		}
	}
}

void TextBox::clear()
{
	data.clear();
	outBuffer.clear();

	nrLines		= 0;
	atLine		= 0;

	determineLineheight();
	updateBuffer();
}

void TextBox::render()
{
	if( font != NULL && visible && !outBuffer.empty() )
	{
		float x = pos.left;
		float y = pos.top;
		float w = pos.right - pos.left;
		float h = pos.bottom - pos.top;

		font->draw( outBuffer, x, y, w, h, color );
	}
}

void TextBox::setActive( bool status )
{
	active = status;
}

bool TextBox::isActive()
{
	return active;
}

void TextBox::setVisible( bool status )
{
	color.a		= 1.0f;
	fade		= false;
	visible		= status;
}

bool TextBox::isVisible()
{
	return visible;
}

bool TextBox::hover( float x, float y )
{
	bool result = false;

	if( visible )
	{
		if( x > pos.left && x < pos.right &&
			y > pos.top && y < pos.bottom )
		{
			result = true;
		}
	}

	return result;
}

void TextBox::setSize( unsigned int max )
{
	maxSize = max;
}


void TextBox::setPos( float x, float y, float w, float h )
{
	bool resize = true;
	determineLineheight();

	if( (unsigned int)(h / lineHeight) == visibleLines )
	{
		resize = false;
	}

	pos.left	= x;
	pos.top		= y;
	pos.right	= x + w;
	pos.bottom	= y + h;

	if( resize )
	{
		visibleLines = (unsigned int)(h / lineHeight);
		
		if( (int)(atLine + visibleLines) >= nrLines )
		{
			atLine = nrLines - visibleLines;
		}

		if( atLine < 0 )
		{
			atLine = 0;
		}

		rebuildBuffer = true;
	}
}

D2D1_RECT_F TextBox::getPos()
{
	return pos;
}

void TextBox::setColor( float r, float g, float b, float a )
{
	color = D3DXCOLOR( r, g, b, a );
}

void TextBox::fadeOut( int delay )
{
	color.a		= 1.0f;
	fadeDelay	= std::clock() + delay * CLOCKS_PER_SEC;
	fadeTarget	= 0.0f;
	fade		= true;
}

void TextBox::fadeIn()
{
	fadeDelay	= 0;
	fadeTarget	= 1.0f;
	fade		= true;
	visible		= true;
}

void TextBox::updateBuffer()
{
	std::stringstream outstream;

	outBuffer.clear();

	if( data.empty() )
	{
		atLine = 0;
		rebuildBuffer = false;
		return;
	}

	for( unsigned int i = 0; i < data.size(); i++ )
	{
		//outBuffer += data[i];
		outstream << data[i];
		if( i < data.size() - 1 )
		{
			//outBuffer += '\n';
			outstream << std::endl;
		}
	}

	DWRITE_TEXT_METRICS metrics = font->getTextMetrics( outstream.str(), pos.right - pos.left );
	DWRITE_LINE_METRICS * lineMetrics = new DWRITE_LINE_METRICS[ metrics.lineCount ];
	UINT32 lineCount;
	font->getLineMetrics( outstream.str(), lineMetrics, metrics.lineCount, &lineCount, pos.right - pos.left );

	if( nrLines != lineCount )
	{
		std::cout << "nrLines = " << nrLines << "; lineCount = " << lineCount << std::endl;
		nrLines = lineCount;

		// Auto-scrolls to bottom when new message is added
		if( nrLines > (int)visibleLines )
		{
			atLine = nrLines - visibleLines;
		}
		else
		{
			atLine = 0;
		}
	}

	for( int i = 0; i < atLine && i < (int)lineCount; i++ )
	{
		//outBuffer.erase( 0, lineMetrics[i].length );
		outstream.ignore( lineMetrics[i].length );
	}

	delete lineMetrics;

	outBuffer = outstream.str().substr( (unsigned int)outstream.tellg() );

	rebuildBuffer = false;
}

void TextBox::determineLineheight()
{
	if( font != NULL )
	{
		lineHeight = font->getTextMetrics( std::string(), pos.right - pos.left ).height;

		unsigned int vl = (int)((pos.bottom - pos.top) / lineHeight);

		if( vl != visibleLines )
		{
			visibleLines	= vl;
			rebuildBuffer	= true;
		}
	}
}
