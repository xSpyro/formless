
#include "BlurTransition.h"

#include "RenderModule.h"

BlurTransition::BlurTransition(RenderModule * renderModule)
	: renderModule(renderModule)
{
	power = 1.0f;
}

BlurTransition::~BlurTransition()
{

}

void BlurTransition::onBegin(int frames)
{
	this->frames = frames;

	frame = 0;
}

void BlurTransition::onFadeChange()
{
	// render a single iteration of the pipeline
	//renderModule->getOffscreenTexture()->clear();


	frame = 0;
}

void BlurTransition::onFadeIn()
{
	++frame;

	float strength = std::pow(frame / static_cast < float > (frames), power);
	float alpha = std::pow(frame / static_cast < float > (frames), power);

	renderModule->setBlendColor(Color(1, 1, strength, alpha));
	renderModule->setDrawTexture(renderModule->getOffscreenTexture());
	renderModule->drawSpriteForceScreen(0, 0, 1280, 720);

	renderModule->setBlendColor(Color(1, 1, 1, 1));


	renderModule->getOffscreenTexture()->clear();
}

void BlurTransition::onFadeOut()
{
	++frame;

	float strength = -std::pow((frame - frames) / static_cast < float > (frames), power);
	float alpha = 1 + std::pow(frame / static_cast < float > (frames), power);

	renderModule->setBlendColor(Color(1, 1, strength, alpha));
	renderModule->setDrawTexture(renderModule->getOffscreenTexture());
	renderModule->drawSpriteForceScreen(0, 0, 1280, 720);

	renderModule->setBlendColor(Color(1, 1, 1, 1));

	renderModule->getOffscreenTexture()->clear();
}
