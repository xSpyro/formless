#include "Client.h"
#include "StartupScene.h"


#include "Object.h"
#include "LogicHandler.h"
#include "RenderComponent.h"
//#include "GameConnector.h"
#include "ScriptEnvironment.h"


//#include "Graphics.h"

#include "MemLeaks.h"



StartupScene::StartupScene()
	: Scene()
{
	
}

StartupScene::StartupScene(LogicHandler* handler, Renderer* rend, Window* wndw)
	: Scene(handler, rend, wndw)
{
	/*
	graphics = New Graphics();

	graphics->init(rend);

	layer = New Overlay(this);

	test = New Test(this);

	debug = renderer->createLayer();
	Viewport * vp = debug->addViewport(&camera);
	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));



	stream.init(renderer, &camera);
	
	camera.setPosition(Vector3(0, 0, -20));
	camera.setLookAt(0);

	File file;
	FileSystem::get("scripts/startup.lua", &file);
	parent->getScript()->execute(file.data, file.size);


	// bind particle layer
	{
		BaseShader * vs = renderer->shader()->get("shaders/particle.vs");
		BaseShader * gs = renderer->shader()->get("shaders/particle.gs");
		BaseShader * ps = renderer->shader()->get("shaders/particle.ps");

		particles = renderer->createLayer();
		Viewport * vp = particles->addViewport(&camera);

		vp->getEffect()->blendModeDepthWithAlpha();
		vp->setShaders(vs, gs, ps);
	}

	Texture * mt = renderer->texture()->create(0, 0, TEXTURE_MULTI);

	Texture * texture1 = renderer->texture()->get("textures/ps_parts.png");
	Texture * texture2 = renderer->texture()->get("textures/ps_hori_stream.png");

	mt->addMultiTexture(texture1);
	mt->addMultiTexture(texture2);

	particles->add(New Renderable(stream.getRenderMesh(), mt, NULL));
	*/
	
}

StartupScene::~StartupScene()
{
	/*
	delete graphics;
	delete test;
	delete layer;
	*/
}

void StartupScene::update()
{

}

void StartupScene::render()
{

}

void StartupScene::onEnter()
{
	renderer->insertStage(1000, layer);
	renderer->insertStage(10, test);
	//renderer->insertLayer(5, debug);
	buildLayers();


	//renderer->insertLayer(4, particles);

	//renderer->switchWindowFullScreenMode();

	ModelData * data = NULL;
	renderer->modelImport()->import("models/ruin.obj2", &data);

	//renderer->modelImport()->import("models/ruin2.obj2", &data);


}

void StartupScene::onLeave()
{
	clearLayers();
}

bool StartupScene::onInput(int sym, int state)
{
	if (sym == VK_SPACE && state == 1)
	{
		parent->gotoScene("MainMenu"); 
	}

	if (sym == VK_F5 && state == 1)
	{
		renderer->shader()->reload();
		std::cout << "shaders reloaded" << std::endl;
	}

	if (sym == VK_F4 && state == 1)
	{
		renderer->switchWindowFullScreenMode();
	}

	if (sym == MB_MOUSEWHEEL)
	{
		graphics->temp(state * 0.1f);
	}

	if (sym == VK_F2 && state == 1)
	{
	}

	return false;
}

bool StartupScene::onMouseMovement(int x, int y)
{
	/*
	for (int i = 0; i < 1; ++i)
	{

		stream.insert(Vector3(0, 0, 0));
	}
	*/

	return false;
}

bool StartupScene::onCreateObject(Object* object)
{
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();

	if(rendcomp)
	{
		rendcomp->load(renderer);
		debug->add(&rendcomp->model);
	}	

	return true;
}

bool StartupScene::onDestroyObject(Object* object)
{
	return true;
}

void StartupScene::onOverlay()
{
	parent->getScript()->getGlobals()["onStartupRender"]();
}

void StartupScene::onTest()
{

	stream.render();
}

void StartupScene::buildLayers()
{
	clearLayers();
	renderer->insertStage(1000, layer);
	renderer->insertStage(10, test);
}

void StartupScene::clearLayers()
{
	renderer->clearStage(1000);
	renderer->clearStage(10);
}
