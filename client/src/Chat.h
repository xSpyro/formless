#ifndef CHAT__H
#define CHAT__H

#include "Font2D.h"

class TextBox;
class InputBox;
class NetworkSubsystem;
class GameHandler;

enum ChatEnum
{
	CHAT_CHANNEL_NONE	= -1,
	CHAT_CHANNEL_ALL	= 0,
	CHAT_CHANNEL_TEAM,

	CHAT_CHANNELS
};

class Chat
{
	typedef std::map< int, std::string > Channels;

public:
	Chat( GameHandler* game, Font2D* font, NetworkSubsystem* net );
	~Chat();

	void update();
	void render();
	void onInput( int key, int state );
	void addMessage( std::string msg );
	void clear();
	void clearLog();
	void setChannel( ChatEnum id );
	void setTeam( unsigned int id );

public:
	static void setLogPos( float x, float y, float w, float h );
	static void setInputPos( float x, float y, float w, float h );
	static void setActive( bool status );
	static bool getActive();

private:
	static Chat*		me;
	static bool			active;

private:
	unsigned int		team;

	InputBox*			input;

	// Channel stuff
	Channels			channels;
	int					currentChannel;

	// Chatlog stuff
	TextBox*			log;

	// Handlers
	GameHandler*		game;
	Font2D*				font;
	NetworkSubsystem*	net;
};

#endif
