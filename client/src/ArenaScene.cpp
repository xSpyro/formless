
#include "Client.h"

#include "ClientInterface.h"

#include "RequestInterface.h"
#include "CommonPackets.h"

#include "ArenaScene.h"
#include "Common.h"
#include "Object.h"
#include "LogicHandler.h"
#include "RenderComponent.h"
#include "GameConnector.h"
#include "ScriptEnvironment.h"
#include "PhysicsComponent.h"
#include "CoreComponent.h"
#include "ControllerComponent.h"
#include "AnimationComponent.h"
#include "NetworkComponent.h"
#include "SkillComponent.h"
#include "GameHandler.h"
#include "CommonPackets.h"
#include "SoundSubsystem.h"
#include "EffectComponent.h"
#include "StatsComponent.h"

#include "Framework.h"

#include "MemLeaks.h"

#include "PlayerVisual.h"

#include "Module.h"

#include "Chat.h"
#include "NetworkSubsystem.h"
#include "NetworkSubsystemClient.h"

#include "Events.h"

ArenaScene::ArenaScene()
	: Scene(), sound(-1)
{
	
}

ArenaScene::ArenaScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game, Chat* chat, ParticleSystem * ps, Font2D * font)
	: Scene(handler, rend, wndw), game(game), chat(chat), sound(-1), showCombatText(true), showTeamNames(true), showPlayerName(true), showEnemyNames(true)
{	

}

ArenaScene::ArenaScene(const InitData & in)
	: Scene(in.game->getLogic(), in.rend, in.window), game(in.game), chat(in.chat), client(in.client), sound(-1), 
	showCombatText(true), showTeamNames(true), showPlayerName(true), showEnemyNames(true)
{
	network = NULL;
	stats	= NULL;
	respawnTimer = NULL;

	renderModule = in.renderModule;
	camera = renderModule->getCamera();

	camera->setPosition(Vector3(0,100,-80));
	camera->setLookAt(Vector3(0, 10.0f, 0));
	camera->setClipRange(40, 1000);

	camera->setProjection3D(70, 16 / 9);

	cameraDest = camera->getPosition();

	layer2d = New Stage2D(this, in.font);

	// register some global functions to lua
	EventLua::Object g = game->getScript()->getGlobals();
	g.bind("getSkillSlots", &getSkillSlots);
	g.bind("getSkillAt", &getSkillAt);
	g.bind("getSkillID", &getSkillID);
	g.bind("setActiveSlot", &setActiveSlot);
	g.bind("getArenaClientID", &getClientID);
	g.bind("getClientTeam", &getClientTeam);
	g.bind("arenaDisconnect", &disconnect);
	g.bind("getItemUpdateInfo", &getItemUpdateInfo);
	g.bind("getRespawnTimer", &getRespawnTimer);

	me = this;

	player = NULL;
	playerPhys = NULL;
	playerCont = NULL;
	playerCore = NULL;

	tabState = 0;

	accountInfo.itemName	= "";
	accountInfo.playerAge	= 0;
	accountInfo.prevItemAt	= 0;
	accountInfo.nextItemAt	= 0;
	accountInfo.timeTicks	= 0;

	newAchievement.name			= "";
	newAchievement.description	= "";
	newAchievement.timeTicks	= 0;
}

ArenaScene::~ArenaScene()
{
	Delete layer2d;
}

void ArenaScene::update()
{

	// hide players
	std::vector<Object*> objvector;
	parent->getObjects("Player", objvector);
	for(std::vector<Object*>::iterator it = objvector.begin(); it != objvector.end(); ++it)
	{
		Object * obj = *it;

		CoreComponent * core = obj->findComponent<CoreComponent>();

		if (core->team <= 0 || !core->isPlayerAlive())
		{
			AnimationComponent * ac = obj->findComponent<AnimationComponent>();
			ac->getModel()->setVisible(false);
		}
	}

	// hide ai-players
	objvector.clear();
	parent->getObjects("AIPlayer", objvector);
	for(std::vector<Object*>::iterator it = objvector.begin(); it != objvector.end(); ++it)
	{
		Object * obj = *it;

		CoreComponent * core = obj->findComponent<CoreComponent>();

		if (core->team <= 0 || !core->isPlayerAlive())
		{
			AnimationComponent * ac = obj->findComponent<AnimationComponent>();
			ac->getModel()->setVisible(false);
		}
	}


	Vector3 mouseToCenter = Vector3(mouseWorldPosition.x, 0, mouseWorldPosition.z) * 0.2f;
	updateMouseWorldPosition();

	if(playerCore->isPlayerAlive())
		renderModule->setPlayerPosition(playerPhys->position);
	else
	{
		Vector3 v(0, renderModule->getHeightFromTerrain(Vector2(0, 0)), 0);
		renderModule->setPlayerPosition(v);
	}

	renderModule->setCursorPosition(mouseWorldPosition);
	playerCont->setMousePosition(mouseWorldPosition);
	playerCont->subscribe();

	updateSound();

	updateLights();

	// client is doing this
	//renderModule->update();

	if( chat )
		chat->update();

	for(EffectTexts::iterator it = effectTexts.begin(); it != effectTexts.end();)
	{
		it->timeticks--;
		it->age++;

		if( it->timeticks <= 0 )
			it = effectTexts.erase(it);
		else
			++it;
	}

	if(accountInfo.itemName != "")
	{
		accountInfo.timeTicks--;

		if(accountInfo.timeTicks <= 0)
		{
			accountInfo.itemName	= "";
			accountInfo.timeTicks	= 0;
		}
	}	

	if(newAchievement.name != "")
	{
		newAchievement.timeTicks--;

		if(newAchievement.timeTicks <= 0)
		{
			newAchievement.name			= "";
			newAchievement.description	= "";
			newAchievement.timeTicks	= 0;
		}
	}

	static Timer effectTimer;

	if (effectTimer.accumulate(2))
	{
		for (unsigned int i = 0; i < renderModule->getLinks(); ++i)
		{
			Vector3 p = renderModule->getLinkPosition(i);

			renderModule->spawnEffect("effects/pilar_effect.paf", p, Vector3(0, 1, 0));
		}

		renderModule->spawnEffect("effects/heat_middle.paf", Vector3(0, 50, 0), Vector3(0, 1, 0));
	}


	// event list handling
	eventList.update();
}

void ArenaScene::render()
{
	updateCamera();

	renderModule->preFrame();
}

void ArenaScene::onEnter()
{
	tabState = 0;

	camera->setPosition(Vector3(0, 100, -80));

	renderModule->clean();


	renderModule->updateTerrain();

	renderModule->bind();

	
	renderModule->addCircleOfLinks();
	renderModule->addCenterPowerLink();

	
	sceneryObjects.push_back(game->createObjectExt("Plate", Vector3(0, 0, 0), Vector3(0, 0, 0), NULL, NULL));

	for (unsigned int i = 0; i < renderModule->getLinks(); ++i)
	{
		Vector3 p = renderModule->getLinkPosition(i);

		sceneryObjects.push_back(game->createObjectExt("Spire", p - Vector3(0, 60, 0), p, NULL, NULL));
	}
	


	//renderer->insertStage(1000, layer2d);
	//onFocus();

	buildLayers();
	layer2d->visible = true;

	//Here we suppose that the player is already created in another scene
	player = game->getControlledObject();
	preparePlayer();

	// set animation to invisible
	if (player && client->isSpectator())
	{
		AnimationComponent * ac = player->findComponent<AnimationComponent>();
		ac->getModel()->setVisible(false);
	}
	
	std::vector<Object*> objvector;
	parent->getObjects("Player", objvector);
	for(std::vector<Object*>::iterator it = objvector.begin(); it != objvector.end(); ++it)
	{
		onCreateObject((*it));
	
		CoreComponent * core = (*it)->findComponent<CoreComponent>();
		if (core)
			core->setCoreAttributes();
	}

	parent->getObjects("AIPlayer", objvector);
	for(std::vector<Object*>::iterator it = objvector.begin(); it != objvector.end(); ++it)
	{
		onCreateObject((*it));

		CoreComponent * core = (*it)->findComponent<CoreComponent>();
		if (core)
			core->setCoreAttributes();
	}


	// will only ever be client network subsystem
	network = reinterpret_cast < NetworkSubsystemClient* > (game->getSubsystem("Network"));
	stats	= network->getStats();
	respawnTimer = network->getRespawnTimer();


	if( chat )
		chat->setActive( false );


	// onEnter-script
	File file;
	FileSystem::get("scripts/hud.lua", &file);
	parent->getScript()->execute(file.data, file.size);

	parent->getScript()->getGlobals()["onHudEnter"]();

	EventLua::Object hp = parent->getScript()->get("setMaxHealth");
	hp( playerCore->life.getMax() );

	EventLua::Object mp = parent->getScript()->get("setMaxMana");
	mp( playerCore->mana.getMax() );

	luaTimer.restart();


	SoundSubsystem* sound = (SoundSubsystem*)game->getSubsystem("Sound");

	sound->getMusicPlayer()->stop();
	sound->getMusicPlayer()->tellCurrentPlaylist("Arena");
	sound->getMusicPlayer()->randomizeSongFromCurrentContainer(true);


	//sound->getMusicPlayer()->play("402567_Jump_Into_A_Puddle_and_Rel.mp3");

	sound->setCamera(camera);

	sound->setListener(player);
}

void ArenaScene::onLeave()
{
	tabState = 0;

	renderModule->clearAutoScenery();

	// clear scenery objects
	for (std::vector < Object* > ::iterator i = sceneryObjects.begin(); i != sceneryObjects.end(); ++i)
	{
		game->destroyObject(*i);
	}

	sceneryObjects.clear();



	parent->getScript()->getGlobals()["onHudLeave"]();

	clearLayers();


	SoundSubsystem* sound = (SoundSubsystem*)game->getSubsystem("Sound");

	sound->getMusicPlayer()->stop();
	sound->getMusicPlayer()->tellCurrentPlaylist("Start");
	//sound->getMusicPlayer()->next();
	
	sound->getMusicPlayer()->play("start1.mp3");

	sound->setCamera(NULL);

	if(stats)
	{
		stats->gameStarted = false;
		stats = NULL;
	}


	renderModule->clean();
}

void ArenaScene::onLoad()
{

}

bool ArenaScene::onInput(int sym, int state)
{
	if(state != 0)
	{
		if( (chat == NULL) || !chat->getActive() )
		{
			if( sym == 'Y' )
			{
				chat->setChannel( CHAT_CHANNEL_ALL );
				chat->setActive( true );
			}

			if( sym == 'T' )
			{
				chat->setChannel( CHAT_CHANNEL_TEAM );
				chat->setActive( true );
			}

			if (sym == VK_ESCAPE)
			{
				parent->activateScene("PauseMenu");
			}

			if (sym == VK_F8)
			{
				SoundSubsystem* sound = (SoundSubsystem*)game->getSubsystem("Sound");
				sound->getMusicPlayer()->next();
			}

			if (sym == VK_F6)
			{
				SoundSubsystem* sound = (SoundSubsystem*)game->getSubsystem("Sound");
				sound->getMusicPlayer()->previous();
			}

		}
		else if( chat )
		{
			chat->onInput( sym, state );
			if( sym == VK_ESCAPE || sym == VK_RETURN )
			{
				chat->setActive( false );
			}
		}
	}

	if(sym == VK_TAB)
		tabState = state;

	if (layer2d->visible == true)
		parent->getScript()->getGlobals()["onHudInput"]( sym, state );

	if( chat )
		return ( chat->getActive() && state ? true : false );
	else
		return false;
}

bool ArenaScene::onMouseMovement(int x, int y)
{
	mouse.x = (float)x;
	mouse.y = (float)y;

	return false;
}

bool ArenaScene::onCreateObject(Object* object)
{
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();

	if (rendcomp)
	{
		renderModule->addObject(rendcomp);
	}	

	AnimationComponent* animcomp = object->findComponent<AnimationComponent>();

	if (animcomp)
	{
		renderModule->addLinkedParticle(animcomp);
	}

	EffectComponent* effcomp = object->findComponent<EffectComponent>();

	if (effcomp)
	{
		
		PhysicsComponent * com = object->findComponent<PhysicsComponent>();
		if (com)
		{
			//std::cout << "spawn effect " << com->getPosition() << std::endl;

			Vector3 normal = Vector3(0, 1, 0); // riktning p� terr�ngen/surface
			normal.normalize();

			renderModule->spawnEffect(effcomp->getName(), com->getPosition() + Vector3(0, 15, 0), normal);
		}
	}
	
	return true;
}

bool ArenaScene::onDestroyObject(Object* object)
{
	return true;
}

void ArenaScene::onKillObject(Object* killed, Object* killer)
{
	if(killed && killer)
	{
		NetworkComponent* netComp1 = killed->findComponent<NetworkComponent>();
		NetworkComponent* netComp2 = killer->findComponent<NetworkComponent>();

		if(netComp1 && netComp2)
		{
			std::string killedName	= netComp1->getName(netComp1->getID());
			std::string killerName	= netComp2->getName(netComp2->getID());

			CoreComponent* core1 = killer->findComponent<CoreComponent>();
			CoreComponent* core2 = killed->findComponent<CoreComponent>();

			/*
			EffectText text;

			text.text		= str.str();
			text.age		= 0;
			text.color		= D3DXCOLOR(0, 0, 0, 1);
			text.position	= Vector3(900, 200, -1);
			text.timeticks	= 90;
			text.inWorld	= false;
			text.textSize	= 20;

			effectTexts.push_back(text);
			*/

			EventList::Event ev;
			ev.name[0] = killerName;
			ev.name[1] = killedName;
			ev.level = 0;
			ev.duration = 300;
			ev.team[0] = core1->team;
			ev.team[1] = core2->team;
			ev.cause = "";

			if (killer == player || killed == player)
			{
				ev.level = 2;
			}

			eventList.add(ev);
		}
	}
}

void ArenaScene::onFocus()
{
	buildLayers();
	layer2d->visible = true;
}

void ArenaScene::onFocusLost()
{
	layer2d->visible = false;
}

void ArenaScene::onConnected(unsigned int id)
{

}

void ArenaScene::onRender2D(Font2D * font)
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/hud.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout << parent->getScript()->getLastError() << std::endl;
	}
	parent->getScript()->getGlobals()["onHudRender"]();

	renderPlayerStats();


	if( chat /*&& chat->getActive()*/ )
		chat->render();

	renderPlayerNames(font);

	if (showCombatText)
		renderSkillEffects(font);

	if (showStats == true)
	{
		// render stats
		std::stringstream stream;
		stream << "Fps: " << client->getFps() << " Total: " << (int)(client->getNetLagg() * 1000) << " ms" << std::endl;
		stream << "Traffic In: " << client->getBytesIn() << " b/s" << std::endl;
		stream << "Traffic Out: " << client->getBytesOut() <<  " b/s" << std::endl;
		stream << "Update Time: " << (int)(client->getUpdateTime() * 1000) << " ms" << std::endl;
		stream << "Render Time: " << (int)(client->getRenderTime() * 1000) << " ms" << std::endl;
		stream << "Present Time: " << (int)(client->getPresentTime() * 1000) << " ms" << std::endl;
		stream << "WinNet Time: " << (int)(client->getWinNetTime() * 1000) << " ms" << std::endl;

		stream << renderModule->getDebugText();

		stream << game->getDebugString();

		font->draw(stream.str().c_str(), 900, 10, 360, 700, D3DXCOLOR(0, 0, 0, 1));

	}

	if(!accountInfo.allUnlocked && accountInfo.itemName != "")
		renderItemUnlock();

	if(newAchievement.name != "")
		renderNewAchievement();

	renderEventList(font);
}

bool ArenaScene::getShowCombatText()
{
	return showCombatText;
}

bool ArenaScene::getShowTeamNames()
{
	return showTeamNames;
}

bool ArenaScene::getShowPlayerName()
{
	return showPlayerName;
}

bool ArenaScene::getShowEnemyNames()
{
	return showEnemyNames;
}

bool ArenaScene::getShowStats()
{
	return showStats;
}

void ArenaScene::setShowCombatText(const bool ct)
{
	showCombatText = ct;
}

void ArenaScene::setShowTeamNames(const bool tn)
{
	showTeamNames = tn;
}

void ArenaScene::setShowPlayerName(const bool pn)
{
	showPlayerName = pn;
}

void ArenaScene::setShowEnemyNames(const bool en)
{
	showEnemyNames = en;
}

void ArenaScene::setShowStats(const bool s)
{
	showStats = s;
}

void ArenaScene::onSkillEffect(float damage, bool stun, bool link, bool outOfMana, int killStreak, float x, float y, float z)
{
	EffectText effect;

	effect.timeticks	= 60;
	effect.age			= 0;
	effect.position		= Vector3(x, y, z);
	effect.inWorld		= true;
	effect.textSize		= 16;

	if(damage > 0)
	{
		std::stringstream str;
		str << (int)damage;

		effect.text		= str.str();
		effect.color	= D3DXCOLOR(1, 1, 0, 1);

		effectTexts.push_back(effect);
	}

	if( stun == true )
	{
		effect.text		= "STUN";
		effect.color	= D3DXCOLOR(1, 1, 1, 1);

		effectTexts.push_back(effect);
	}
	
	if( link == true )
	{
		effect.text		= "LINK";
		effect.color	= D3DXCOLOR(0.5f, 0, 1, 1);

		effectTexts.push_back(effect);
	}

	if( outOfMana == true )
	{
		effect.text		= "NOT ENOUGH MANA";
		effect.color	= D3DXCOLOR(0.5f, 0, 0, 1);

		effectTexts.push_back(effect);
	}

	if( killStreak > 1 )
	{
		std::stringstream str;
		str << killStreak << " STREAK";

		effect.text		= str.str();
		effect.color	= D3DXCOLOR(0, 1, 0, 1);

		effectTexts.push_back(effect);
	}
}

void ArenaScene::onAccountUpdate(AccountPacket* packet)
{
	if(accountInfo.itemName == "")
	{
		accountInfo.itemName	= packet->itemname;
		accountInfo.playerAge	= packet->age;
		accountInfo.prevItemAt	= packet->prevItemAt;
		accountInfo.nextItemAt	= packet->nextItemAt;
		accountInfo.allUnlocked	= packet->allUnlocked;

		if(accountInfo.itemName != "")
		{
			accountInfo.timeTicks	= 360;
		}
	}

	if(packet->achievementName != "")
	{
		newAchievement.name			= packet->achievementName;
		newAchievement.description	= packet->achievementDesc;
		newAchievement.timeTicks	= 360;
	}
}



ArenaScene* ArenaScene::me = NULL;

int ArenaScene::getSkillSlots()
{
	return me->playerCore->getSkillSlots();
}

const char* ArenaScene::getSkillAt( int slot )
{
	Module* mod = me->playerCore->getModule( slot );

	if( mod )
		return mod->name.c_str();

	return NULL;
}

int ArenaScene::getSkillID( int slot )
{
	Module* mod = me->playerCore->getModule( slot );

	if( mod )
		return mod->id;

	return NULL;
}

void ArenaScene::setActiveSlot( int slot )
{
	me->playerCont->setActiveSlot( slot );
}

int ArenaScene::getClientID()
{
	return me->player->findComponent<NetworkComponent>()->getID();
}

int ArenaScene::getClientTeam()
{
	return me->playerCore->team;
}

void ArenaScene::disconnect()
{
	MasterSendAsync async;
	ServerLeavePacket packet;
	packet.user_id = ((Client*)me->window)->getUserID();
	async( packet, sizeof(packet) );

	me->game->getNetwork()->disconnect();
	me->game->disconnect();
	me->chat->clearLog();
}

EventLua::Object ArenaScene::getItemUpdateInfo()
{
	std::stringstream ss;
	ss << "TEMP_TABLE_ITEM_UPDATE" << env->getEnvId();
	EventLua::Object rtn = me->game->getScript()->newTable(ss.str().c_str());

	rtn["prevItemAt"]	= (int)me->accountInfo.prevItemAt;
	rtn["nextItemAt"]	= (int)me->accountInfo.nextItemAt;
	rtn["age"]			= (int)me->accountInfo.playerAge;
	rtn["allUnlocked"]	= me->accountInfo.allUnlocked;

	return rtn;
}

EventLua::Object ArenaScene::getRespawnTimer()
{
	std::stringstream ss;
	ss << "TEMP_TABLE_RESPAWN_TIMER" << env->getEnvId();
	EventLua::Object rtn = me->game->getScript()->newTable(ss.str().c_str());

	rtn["respawning"]	= me->respawnTimer->respawning;
	rtn["time"]			= me->respawnTimer->time;

	return rtn;
}

void ArenaScene::buildLayers()
{
	clearLayers();

	renderer->insertStage(1000, layer2d);
}

void ArenaScene::clearLayers()
{
	renderer->clearStage(1000);
}

void ArenaScene::preparePlayer()
{
	playerPhys = player->findComponent<PhysicsComponent>();

	playerCont = player->findComponent<ControllerComponent>();
	if(!playerCont->subscribe())
	{
		std::cout << "FAILED TO SUBSCRIBE LOL" << playerCont->getObject() << std::endl;
	}

	playerCore = player->findComponent<CoreComponent>();
	playerCore->revive();
}

bool ArenaScene::updateMouseWorldPosition()
{
	Ray ray;

	Vector2 mouseVector;
	window->getMouseVector(&mouseVector.x, &mouseVector.y);

	camera->pick(mouseVector, &ray);

	IntersectResult res;
	Renderable* rend = (Renderable*)renderModule->getTerrainLayer()->pick(ray, &res);
	if(rend)
	{
		//Mouse hits height map

		Vector3 resultpos = ray.origin + ray.direction * res.dist;

		mouseWorldPosition = resultpos;

		return true;
	}
	else
	{
		//Mouse is outside of height map

		camera->pick(mouseVector, &ray);

		Plane plane;
		plane.a = 0;
		plane.b = 1;
		plane.c = 0;
		plane.d = 0;

		if(Intersect::intersect(ray, plane, &res))
		{
			Vector3 resultpos = ray.origin + ray.direction * res.dist;

			mouseWorldPosition = resultpos;
		}

		return false;
	}
}

void ArenaScene::updateCamera()
{
	float multiplier = 0.1f;

	// read from script if found
	float len = 180;
	float limit = 60;
	float height = 230;
	int type = 0; // camera same look at
	float fov = 75;
	float down = 1.8f;

	static EventLua::State state;
	if (state.execute("../CameraSettings.lua"))
	{
		len = state.get("length").queryFloat();
		limit = state.get("limit").queryFloat();
		height = state.get("height").queryFloat();
		type = state.get("type").queryInteger();
		fov = state.get("fov").queryFloat();
		down = state.get("down").queryFloat();
	}

	if(playerCore->isPlayerAlive())
	{
		//Calculate the camera destination point from the player position and mouse world position
		//Vector2 mouseToPlayer = Vector2(mouseWorldPosition.x, mouseWorldPosition.z - 110) - Vector2(playerPhys->position.x, playerPhys->position.z);

		//Vector2 mouseToPlayer = playerPhys->position * 0.6f;

		if (type == 0)
		{
			cameraDest.x = playerPhys->position.x;
			cameraDest.y = playerPhys->position.y + height - playerPhys->offset.y;
			cameraDest.z = playerPhys->position.z - len;
		}
		else
		{

			Vector3 cn = playerPhys->position;
			cn.normalize();

			cameraDest.x = playerPhys->position.x + cn.x * len;
			cameraDest.y = playerPhys->position.y + height;
			cameraDest.z = playerPhys->position.z + cn.z * len;

			cameraLookDest.x = playerPhys->position.x - cn.x * len;
			cameraLookDest.y = playerPhys->position.y;
			cameraLookDest.z = playerPhys->position.z - cn.z * len;

			Vector3 cameraDest2 = cameraDest;
			cameraDest2.y = 0;

			if (cameraDest2.length() < limit)
			{
				cameraDest2.normalize();
				cameraDest = cameraDest2 * limit;

				cameraDest.y = height;
			}

			multiplier = 0.1f;
		}
	}
	else
	{
		//Calculate the camera destination point from the mouse world position only
		camera->setPosition(Vector3(0, 460, -350));
		camera->setLookAt(Vector3(0, 0, 0));
		multiplier = 0.0f;


	}

	

	//Move the camera towards the destination point
	Vector3 destination = cameraDest- camera->getPosition();
	camera->setPosition(camera->getPosition() + (destination * multiplier));

	cameraLookAt += (cameraLookDest - cameraLookAt) * multiplier;

	if (type == 1)
	{
		camera->setLookAt(cameraLookAt);
	}
	else
	{
		camera->setOrientation(Vector3(0, -down, 1.0f));
	}

	camera->setProjection3D(fov, 16 / 9.0f);
	camera->update();
}

void ArenaScene::updateSound()
{
	SoundSubsystem* sound = (SoundSubsystem*)game->getSubsystem("Sound");

	if( playerCore->isPlayerAlive() )
	{
		renderModule->setGhostMode(false);

		if (sound->getMusicPlayer()->getNameOfCurrentPlaylist() != "Arena")
		{
			sound->getMusicPlayer()->stop();
			sound->getMusicPlayer()->tellCurrentPlaylist("Arena");
			sound->getMusicPlayer()->randomizeSongFromCurrentContainer(true);
		}
	}
	else
	{
		renderModule->setGhostMode(true);

		if (sound->getMusicPlayer()->getNameOfCurrentPlaylist() != "Ghost")
		{
			sound->getMusicPlayer()->stop();
			sound->getMusicPlayer()->tellCurrentPlaylist("Ghost");
			sound->getMusicPlayer()->randomizeSongFromCurrentContainer(false);
		}
	}
}

void ArenaScene::updateLights()
{
	std::vector < Object* > * objects;
	objects = game->getCoreObjects();

	if (player)
	{
		CoreComponent * core = player->findComponent<CoreComponent>();

		if (core->isPlayerAlive())
		{

			EventLua::State state;
			if (state.execute("../resources/render.lua"))
			{
				EventLua::Object t = state.get("mouse");

				float radius = t.get("range").queryFloat(20);
				Vector3 c;

				float offset = t.get("offset").queryFloat(5);

				c.x = t.get("light")[1].queryFloat(1);
				c.y = t.get("light")[2].queryFloat(1);
				c.z = t.get("light")[3].queryFloat(1);

				SceneLightBuffer::Entity entity;
				entity.position = mouseWorldPosition + Vector3(0, offset, 0);
				entity.color = c;
				entity.radius = 1 / radius;

				renderModule->addLight(entity);
			}
		}
	}

	for (std::vector < Object* >::iterator i = objects->begin(); i != objects->end(); ++i)
	{
		CoreComponent * core = (*i)->findComponent<CoreComponent>();

		if (core->isPlayerAlive())
		{
			SceneLightBuffer::Entity entity;

			PhysicsComponent * com = (*i)->findComponent<PhysicsComponent>();
			AnimationComponent * an = (*i)->findComponent<AnimationComponent>();

			float life = core->life.getCurrent() / core->life.getMax();

			entity.position = com->position + Vector3(0, 5, 0);
			entity.radius = 1;

			if (core->team == 1)
			{
				entity.color = Vector3(2, 0.2f, 0.2f);

				if (an)
					an->setBaseColor(Color(1, 0.7f, 0.7f, 1));
			}
			else if (core->team == 2)
			{
				entity.color = Vector3(0.2f, 0.2f, 2);

				if (an)
					an->setBaseColor(Color(0.7f, 0.7f, 1.0f, 1));
			}
			renderModule->addLight(entity);
		}

		if (core->isPlayerAlive())
		{
			ScenePlayerBuffer::Entity entity;

			PhysicsComponent * com = (*i)->findComponent<PhysicsComponent>();



			entity.position = com->position;
			entity.position.y = 0;
			if (core->team == 1)
			{
				entity.team = 0;
			}
			else if (core->team == 2)
			{
				entity.team = 1;
			}

			renderModule->addPlayer(entity);
		}
	}

	
	std::vector < Object* > * skills;
	skills = game->getSkillObjects();

	for (std::vector < Object* >::iterator i = skills->begin(); i != skills->end(); ++i)
	{
		SceneLightBuffer::Entity entity;

		PhysicsComponent * com = (*i)->findComponent<PhysicsComponent>();
		AnimationComponent * an = (*i)->findComponent<AnimationComponent>();

		if( com )
		{
			entity.position = com->position;
			entity.position.y = 0; // first shape
			entity.radius = 1.5f;

			if( an )
				entity.color = an->getLightColor();
			else
				entity.color = Vector3(1, 1, 0);

			renderModule->addLight(entity);
		}
	}
	
}



void ArenaScene::renderPlayerNames(Font2D* font)
{
	if(tabState == 0)
	{
		font->setTextAlignment(1);

		EventLua::Object renderNames = parent->getScript()->getGlobals().get("renderPlayerNames");
		EventLua::Object renderHP = parent->getScript()->getGlobals().get("renderPlayerHealthBar");

		std::vector < Object* > * objects;
		objects = game->getCoreObjects();

		// Draw all the player names above their heads
		for (std::vector < Object* >::iterator i = objects->begin(); i != objects->end(); ++i)
		{
			CoreComponent * core = (*i)->findComponent<CoreComponent>();

			if(core->isPlayerAlive())
			{
				PhysicsComponent * physics = (*i)->findComponent<PhysicsComponent>();

				Vector2 position = worldToScreen(physics->position);

				//Always render the health bar
				game->getScript()->getGlobals().set("renderPlayerLifeCurrent", core->life.getCurrent());
				game->getScript()->getGlobals().set("renderPlayerLifeMax", core->life.getMax());

				renderHP.call(position.x, position.y);


				// check if we want to show the player name, if we don't we just continue with the rest names
				if ((*i) == player && !showPlayerName)
					continue;
				// check if we want to render the other team-member names
				if (core->team == playerCore->team && !showTeamNames && (*i) != player)
					continue;

				// if we want to render the enemy-names
				if (core->team != playerCore->team && !showEnemyNames)
					continue;


				NetworkComponent * net = (*i)->findComponent<NetworkComponent>();

				std::string name = network->getName(net->getID());

				std::stringstream format;
				format << name << std::endl;

			
				float fontW = 0.0f;
				float fontH = 0.0f;
				font->getSize(format.str(), &fontW, &fontH);

				renderNames.call(format.str().c_str(), position.x, position.y, fontW, fontH, core->team);


			}
		}

		font->setTextAlignment(0);
	}
}

void ArenaScene::renderPlayerStats()
{
	EventLua::Object renderStats = parent->getScript()->getGlobals().get("renderPlayerStats");
	std::vector<Object*> netObjects;
	network->queryClientList(netObjects);
	int teamCounter[3];
	teamCounter[1] = 0;
	teamCounter[2] = 0;

	//Sort on most kills
	std::vector<Object*> sortedNetObjects;
	while(!netObjects.empty())
	{
		int mostKills = 0;
		std::vector<Object*>::iterator highest = netObjects.begin();

		for(std::vector<Object*>::iterator it = netObjects.begin(); it != netObjects.end(); ++it)
		{
			StatsComponent*		statscomp	= (*it)->findComponent<StatsComponent>();

			if(statscomp->kills > mostKills)
			{
				highest = it;
				mostKills = statscomp->kills;
			}
		}

		sortedNetObjects.push_back((*highest));
		netObjects.erase(highest);
	}

	//Call the Lua render for each player
	for(std::vector<Object*>::iterator obj = sortedNetObjects.begin(); obj != sortedNetObjects.end(); ++obj)
	{
		NetworkComponent*	netcomp		= (*obj)->findComponent<NetworkComponent>();
		CoreComponent*		corecomp	= (*obj)->findComponent<CoreComponent>();
		StatsComponent*		statscomp	= (*obj)->findComponent<StatsComponent>();

		unsigned int netId = netcomp->getID();
		std::string username = netcomp->getName(netId);
		int team = corecomp->team;

		// std::stringstream ss;
		// ss << "TEMP_TABLE_ITEM_UPDATE";
		EventLua::Object rtn = me->game->getScript()->newTable();

		rtn["counter"]		= teamCounter[team];
		rtn["username"]		= username.c_str();
		rtn["team"]			= team;
		rtn["kills"]		= statscomp->kills;
		rtn["deaths"]		= statscomp->deaths;
		rtn["killstreak"]	= statscomp->killStreak;
		rtn["damage"]		= statscomp->damage;
		rtn["me"]			= player == (*obj);

		renderStats.call(rtn);		

		teamCounter[team]++;
	}
}

void ArenaScene::renderSkillEffects(Font2D* font)
{
	Vector2 prevpos;

	for(EffectTexts::iterator it = effectTexts.begin(); it != effectTexts.end(); ++it)
	{
		std::stringstream fontStr;
		fontStr << "Trebuchet MS:" << it->textSize;

		font->createFont(fontStr.str());

		D3DXCOLOR fontColor = it->color;

		float fontW = 0.0f;
		float fontH = 0.0f;
		font->getSize(it->text, &fontW, &fontH);

		Vector2 textPosition;

		if(it->inWorld)
			textPosition = worldToScreen(it->position);
		else
			textPosition = Vector2(it->position.x, it->position.y);

		textPosition.y -= it->age;

		if(abs(textPosition.y - prevpos.y) < fontH)
		{
			textPosition.y = prevpos.y + fontH + 1.0f;
		}

		it->color.a =  0 + (it->timeticks*0.05f);

		font->draw(it->text, textPosition.x, textPosition.y, fontW + 1.0f, fontH + 1.0f, it->color);

		prevpos = textPosition;
	}

	font->createFont("Trebuchet MS:20");
}

void ArenaScene::renderItemUnlock()
{
	EventLua::Object renderItem = parent->getScript()->getGlobals().get("renderItemUnlock");
	if(!renderItem.call(accountInfo.itemName.c_str()))
		accountInfo.itemName = "";
}

void ArenaScene::renderNewAchievement()
{
	EventLua::Object renderAchievement = parent->getScript()->getGlobals().get("renderNewAchievement");
	renderAchievement.call(newAchievement.name.c_str());
}

void ArenaScene::renderEventList(Font2D* font)
{
	const EventList::Events & events = eventList.getEvents();

	float y = 0;
	for (EventList::Events::const_iterator i = events.begin(); i != events.end(); ++i, y += 40)
	{
		const EventList::Event & ev = *i;

		D3DXCOLOR color;
		color.r = 1;
		color.g = 1;
		color.b = 1;
		color.a = 1;

		float alpha = 1;


		float m = 60.0f / (1 + ev.level);
		if (ev.duration <= m)
		{
			alpha = ev.duration / m;
		}


		client->setBlendColor(1, 1, 1, alpha);

		if (ev.level > 0)
			client->setBlendColor(2, 2, 2, alpha);

		client->drawBorder("border2.png", 1280 - 450, 55 + y, 430, 30);
		


		if (ev.team[0] == 1)
			client->drawSprite("redteam_small.png", 1280 - 440, 60 + y, 20, 20);
		else if (ev.team[0] == 2)
			client->drawSprite("blueteam_small.png", 1280 - 440, 60 + y, 20, 20);
			

		color = D3DXCOLOR(1, 1, 1, alpha);

		font->setTextAlignment(0);
		font->draw(ev.name[0], 1280 - 440 + 20, 60 + y, 260, 30, color);

		
		if (ev.team[1] == 1)
			client->drawSprite("redteam_small.png", 1280 - 290 + 260 - 20, 60 + y, 20, 20);
		else if (ev.team[1] == 2)
			client->drawSprite("blueteam_small.png", 1280 - 290 + 260 - 20, 60 + y, 20, 20);

		font->setTextAlignment(2);
		font->draw(ev.name[1], 1280 - 290, 60 + y, 260 - 20, 30, color);

		//client->drawSprite("skill_hammer.png", 1280 - 290 / 2 - 25, 60 + y - 5, 30, 30);

		client->setBlendColor(1, 1, 1, 1);
		font->setTextAlignment(0);
	}
}

Vector2 ArenaScene::worldToScreen(Vector3 position)
{
	// project position
	Vector2 pos = camera->project(position);

	pos.x = (pos.x + 1) * 0.5f;
	pos.y = (pos.y + 1) * 0.5f;
	pos.y = 1 - pos.y;

	pos.x *= 1280.0f;
	pos.y *= 720.0f;

	return pos;
}