
#include "EndGameScene.h"
#include "LogicHandler.h"

#include "MemLeaks.h"

#include "Font2D.h"
#include "NetworkSubsystemClient.h"

#include "GameHandler.h"

#include "Object.h"
#include "NetworkComponent.h"
#include "CoreComponent.h"
#include "StatsComponent.h"

#include "SoundSubsystem.h"

EndGameScene::EndGameScene()
	: Scene()
{
	network = NULL;
}

EndGameScene::EndGameScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler* game, Font2D* font)
	: Scene(handler, rend, wndw), game(game)
{
	me = this;
	layer = renderer->createLayer();

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);
	camera.setProjection3D(45, 16 / 9.0f);

	// camera set

	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));


	layer2d = New Stage2D(this, font);
	layer2d->visible = true;


	// register some global functions to lua
	EventLua::Object g = parent->getScript()->getGlobals();
	//g.bind("addMod", &addMod);

	File file;
	FileSystem::get("scripts/endgame.lua", &file);
	parent->getScript()->execute(file.data, file.size);

	player = NULL;
}

EndGameScene::~EndGameScene()
{
	delete layer2d;
}

void EndGameScene::update()
{
	camera.update();
}

void EndGameScene::render()
{

}

void EndGameScene::onEnter()
{
	//renderer->insertLayer(50, layer);
	//renderer->insertStage(2000, layer2d);
	buildLayers();

	SoundSubsystem* sound = (SoundSubsystem*)game->getSubsystem("Sound");

	sound->getMusicPlayer()->lowerVolume();

	network = reinterpret_cast < NetworkSubsystemClient* > (game->getSubsystem("Network"));
	stats	= network->getStats();

	player = game->getControlledObject();
	
	CoreComponent* playerCore = player->findComponent<CoreComponent>();

	if (playerCore)
	{
		parent->getScript()->getGlobals().set("playerTeam", (int)playerCore->team);
	}
	
	parent->getScript()->getGlobals().set("soundPlayed", 0);

	luaTimer.restart();
}

void EndGameScene::onLeave()
{
	clearLayers();

	SoundSubsystem* sound = (SoundSubsystem*)game->getSubsystem("Sound");

	sound->getMusicPlayer()->resetVolume();

	//parent->getScript()->getGlobals().set("playerTeam", -1);
	//renderer->clearStage(50);
	//renderer->clearStage(5000);
}

bool EndGameScene::onInput(int sym, int state)
{
	parent->getScript()->getGlobals()["onEndGameInput"](sym, state);

	return true;
}

bool EndGameScene::onMouseMovement(int x, int y)
{
	return true;
}

void EndGameScene::onRender2D(Font2D* font)
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/endgame.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout<< parent->getScript()->getLastError();
	}
	parent->getScript()->getGlobals()["onEndGameRender"]();

	EventLua::Object renderStats = parent->getScript()->getGlobals().get("renderPlayerStats");
	std::vector<Object*> netObjects;
	network->queryClientList(netObjects);
	int teamCounter[3];
	teamCounter[1] = 0;
	teamCounter[2] = 0;

	//Sort on most kills
	std::vector<Object*> sortedNetObjects;
	while(!netObjects.empty())
	{
		int mostKills = 0;
		std::vector<Object*>::iterator highest = netObjects.begin();

		for(std::vector<Object*>::iterator it = netObjects.begin(); it != netObjects.end(); ++it)
		{
			StatsComponent*		statscomp	= (*it)->findComponent<StatsComponent>();

			if(statscomp->kills > mostKills)
			{
				highest = it;
				mostKills = statscomp->kills;
			}
		}

		sortedNetObjects.push_back((*highest));
		netObjects.erase(highest);
	}

	//Call the Lua render for each player
	for(std::vector<Object*>::iterator obj = sortedNetObjects.begin(); obj != sortedNetObjects.end(); ++obj)
	{
		NetworkComponent*	netcomp		= (*obj)->findComponent<NetworkComponent>();
		CoreComponent*		corecomp	= (*obj)->findComponent<CoreComponent>();
		StatsComponent*		statscomp	= (*obj)->findComponent<StatsComponent>();

		unsigned int netId = netcomp->getID();
		std::string username = netcomp->getName(netId);
		int team = corecomp->team;

		std::stringstream ss;
		ss << "TEMP_TABLE_ITEM_UPDATE";
		EventLua::Object rtn = me->game->getScript()->newTable(ss.str().c_str());

		rtn["counter"]		= teamCounter[team];
		rtn["username"]		= username.c_str();
		rtn["team"]			= team;
		rtn["kills"]		= statscomp->kills;
		rtn["deaths"]		= statscomp->deaths;
		rtn["killstreak"]	= statscomp->highestKillStreak;
		rtn["damage"]		= statscomp->highestDamage;
		rtn["me"]			= player == (*obj);

		renderStats.call(rtn);

		teamCounter[team]++;
	}

	//player = game->getControlledObject();

	CoreComponent* playerCore = player->findComponent<CoreComponent>();

	if (playerCore)
	{
		parent->getScript()->getGlobals().set("playerTeam", (int)playerCore->team);
	}
}

EndGameScene * EndGameScene::me = NULL;

void EndGameScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer(50, layer);
	renderer->insertStage(2000, layer2d);
}

void EndGameScene::clearLayers()
{
	renderer->clearStage(50);
	renderer->clearStage(2000);
}

