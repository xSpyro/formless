
#ifndef PAUSEMENUSCENE_H
#define PAUSEMENUSCENE_H

#include "Scene.h"
#include "Framework.h"

class PauseMenuScene : public Scene
{
public:

	PauseMenuScene();
	PauseMenuScene(LogicHandler* handler, Renderer* rend, Window* wndw);
	virtual ~PauseMenuScene();

	void update();
	void render();

	void onEnter();
	void onLeave();

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	void onFocus();
	void onFocusLost();

	void onRender2D();
	struct Stage2D : public Stage
	{
		PauseMenuScene * me;
		bool visible;

		Stage2D(PauseMenuScene * me) : me(me)
		{
		}

		void apply()
		{
			if(visible)
				me->onRender2D();
		}
	};

private:
	//For lua
	static PauseMenuScene* me;

private:
	void buildLayers();
	void clearLayers();

	Stage2D *	layer2d;
	Layer *		layer;
	Camera		camera;
		
	Timer		luaTimer;
};

#endif