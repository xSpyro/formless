
#include "ClientInterface.h"
#include "CommonPackets.h"

#include "RequestInterface.h"
#include "ProfileInfo.h"
#include "ProfileList.h"

#include "LobbyScene.h"

#include "Common.h"
#include "Object.h"
#include "Module.h"
#include "LogicHandler.h"
#include "GameHandler.h"

#include "RenderComponent.h"
#include "AnimationComponent.h"

#include "NetworkSubsystem.h"
#include "NetworkSubsystemClient.h"
#include "NetworkComponent.h"
#include "CoreComponent.h"
#include "CoreSubsystem.h"

#include "Chat.h"
#include "Client.h"

#include "MemLeaks.h"

LobbyScene::LobbyScene()
	: Scene()
{

}

LobbyScene::LobbyScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game, Chat* c)
	: Scene(handler, rend, wndw), game(game)
{
	me		= this;
	layer	= renderer->createLayer();
	stats	= NULL;

	chat	= c;
	join_team = 0;
	teams[0] = 0;
	teams[1] = 0;
	teams[2] = 0;

	net		= NULL;
	player	= NULL;

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);
	camera.setProjection3D(45, 16 / 9.0f);

	// camera set

	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));


	layer2d = New Stage2D(this);


	File file;
	FileSystem::get("scripts/lobby.lua", &file);
	parent->getScript()->execute(file.data, file.size);

	// register some global functions to lua
	EventLua::Object g = parent->getScript()->getGlobals();
	g.bind("getLobbyClientID", &getClientID);
	g.bind("getPlayerCore", &getPlayerCore);
	g.bind("getCompanionCore", &getCompanionCore);
	g.bind("getPlayerTeam", &getPlayerTeam);
	g.bind("lobbyLoadCore", &loadCore);
	g.bind("lobbyLoadModule", &loadModule);
	g.bind("lobbyDisconnect", &disconnect);
	g.bind("setPlayerReady", &setReady);
	g.bind("getPlayerReady", &getReady);
	g.bind("createPlayerCompanion", &createCompanion);
	g.bind("removePlayerCompanion", &removeCompanion);
	g.bind("playerHasCompanion", &hasCompanion);
	g.bind("joinTeam", &joinTeam);

	reloadProfileTimer.restart();
	profileReloaded = false;
}

LobbyScene::~LobbyScene()
{
	delete layer2d;
}

void LobbyScene::update()
{
	camera.update();

	//Update players twice per second
	if(updatePlayerTimer.accumulate(0.5f))
		updatePlayers();

	if( chat )
		chat->update();

	if(!profileReloaded && reloadProfileTimer.now() >= 0.5f)
	{
		parent->getScript()->getGlobals()["reloadLobbyProfile"]();
		profileReloaded = true;
	}
}

void LobbyScene::proxyUpdate()
{
	if(updatePlayerTimer.accumulate(0.5f))
		updatePlayers();
}

void LobbyScene::render()
{

}

void LobbyScene::onEnter()
{
	//renderer->insertLayer(0, layer);
	//renderer->insertStage(1000, layer2d);
	buildLayers();

	File file;
	FileSystem::get("scripts/lobby.lua", &file);
	if (!parent->getScript()->execute(file.data, file.size))
		std::cout<< parent->getScript()->getLastError()<<std::endl;

	// network
	EnterLobbyPacket packet;
	packet.uid = ((Client*)window)->getUserID();
	packet.team = join_team;
	game->getNetwork()->send(packet);

	Timer timeout;
	do
	{
		player = game->getControlledObject();
	} while( player == NULL && !timeout.accumulate(10));

	if( player == NULL )
	{
		std::cout << "ERROR! player is NULL!" << std::endl;
	}

	playerCore	= player->findComponent<CoreComponent>();
	playerNet	= player->findComponent<NetworkComponent>();

	net = (NetworkSubsystem*)game->getSubsystem("Network");

	if( chat )
		chat->setActive( true );

	// will only ever be client network subsystem
	NetworkSubsystemClient* netClient = reinterpret_cast < NetworkSubsystemClient* > (net);
	stats	= netClient->getStats();

	RequestReadyPacket request;
	me->game->getNetwork()->send(request);

	luaTimer.restart();

	profileReloaded = false;
	reloadProfileTimer.restart();

	parent->getScript()->getGlobals()["onLobbyEnter"]();
}

void LobbyScene::onLeave()
{
	parent->getScript()->getGlobals()["onLobbyLeave"]();
	clearLayers();
}

bool LobbyScene::onInput(int sym, int state)
{
	if(layer2d->visible)
	{
		if( chat )
			chat->onInput( sym, state );

		parent->getScript()->getGlobals()["onLobbyInput"](sym, state);
	}

	return true;
}

bool LobbyScene::onMouseMovement(int x, int y)
{
	return true;
}

bool LobbyScene::onCreateObject(Object* object)
{
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();

	if(rendcomp)
	{
		rendcomp->load(renderer);
		layer->add(&rendcomp->model);
	}

	AnimationComponent* animcomp = object->findComponent<AnimationComponent>();

	if (animcomp)
	{
		animcomp->load(renderer, &camera);
	}

	return true;
}

bool LobbyScene::onDestroyObject(Object* object)
{
	return true;
}

void LobbyScene::onRender2D()
{
	// call lua here
	if (luaTimer.accumulate(1.0f))
	{
		File file;
		FileSystem::get("scripts/lobby.lua", &file);
		if (!parent->getScript()->execute(file.data, file.size))
			std::cout<< parent->getScript()->getLastError()<<std::endl;
	}
	parent->getScript()->getGlobals()["onLobbyRender"]();
	if( chat )
		chat->render();
}

void LobbyScene::onConnected(unsigned int id)
{
	player = game->getControlledObject();

	parent->getScript()->getGlobals()["onLobbyConnected"]();
}

void LobbyScene::onFocus()
{
	//renderer->insertLayer(0, layer);
	//renderer->insertStage(1000, layer2d);
	buildLayers();

	layer2d->visible = true;
	File file;
	FileSystem::get("scripts/lobby.lua", &file);
	if (!parent->getScript()->execute(file.data, file.size))
		std::cout<< parent->getScript()->getLastError()<<std::endl;

	me->parent->getScript()->getGlobals()["onLobbyFocus"]();
}

void LobbyScene::onFocusLost()
{
	layer2d->visible = false;
	me->parent->getScript()->getGlobals()["onLobbyFocusLost"]();
}

void LobbyScene::updatePlayers()
{
	if(net && player)
	{
		EventLua::Object resetPlayers = parent->getScript()->getGlobals().get("resetPlayers");
		EventLua::Object teamPlayerUpdate = parent->getScript()->getGlobals().get("teamPlayerUpdate");
		EventLua::Object setModule = parent->getScript()->getGlobals().get("setModule");
		EventLua::Object otherPlayerUpdate = parent->getScript()->getGlobals().get("otherPlayerUpdate");

		resetPlayers.call();

		if( chat )
			chat->setTeam( playerCore->team );

		std::vector<Object*> objects;
		net->queryClientList(objects);

		
		for(std::vector<Object*>::iterator ob = objects.begin(); ob != objects.end(); ++ob)
		{
			NetworkComponent*	netcomp		= (*ob)->findComponent<NetworkComponent>();
			CoreComponent*		corecomp	= (*ob)->findComponent<CoreComponent>();

			unsigned int netId = netcomp->getID();
			std::string username = "";
			
			//if(netId >= 10000)
			//	username = "Companion (" + netcomp->getName((int)(netId*0.1f)) + ")";
			//else
				username = netcomp->getName(netId);
			

			if(corecomp->team == playerCore->team)
			{
				teamPlayerUpdate.call(username.c_str(), netId, corecomp->name.c_str(), netcomp->isReady());

				std::vector<Module*> modules;
				corecomp->getModuleSlots(modules);

				for(unsigned int i = 0; i < modules.size(); ++i)
				{
					if(modules[i] != NULL)
					{
						setModule.call(modules[i]->id, modules[i]->name.c_str(), i);
					}
				}
			}
			else if (corecomp->team != 0) // no spectators here
			{
				otherPlayerUpdate.call(username.c_str(), netId, netcomp->isReady());
			}

		}
	}
}

void LobbyScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer(0, layer);
	renderer->insertStage(1000, layer2d);
}

void LobbyScene::clearLayers()
{
	renderer->clearStage(0);
	renderer->clearStage(1000);
}

LobbyScene* LobbyScene::me = NULL;


int LobbyScene::getClientID()
{
	return me->playerNet->getID();
}

const char* LobbyScene::getPlayerCore()
{
	return me->playerCore->name.c_str();
}

const char* LobbyScene::getCompanionCore()
{
	Object* companion = me->player->getCompanion();
	
	if(companion)
	{
		return companion->findComponent<CoreComponent>()->name.c_str();
	}

	return NULL;
}

int LobbyScene::getPlayerTeam()
{
	return me->playerCore->team;
}

void LobbyScene::loadCore( const char* name, EventLua::Object data, bool companion )
{
	CoreComponent* coreToLoad = me->playerCore;

	if(companion == true)
	{
		Object* compObj = me->player->getCompanion();

		if(compObj)
			coreToLoad = compObj->findComponent<CoreComponent>();
	}

	if(coreToLoad)
	{
		CoreSubsystem* subsys = (CoreSubsystem*)(me->parent->getSubsystem("Core"));
		subsys->load(data, coreToLoad);

		// network sync
		CoreAttrPacket packet;
		packet = coreToLoad->getAttrPacket();
		packet.companion = companion;
		me->game->getNetwork()->send(packet);
	}
}

void LobbyScene::loadModule( unsigned int id, const char* name, int slot, EventLua::Object data, bool companion )
{
	CoreComponent* coreToLoad = me->playerCore;

	if(companion == true)
	{
		Object* compObj = me->player->getCompanion();

		if(compObj)
			coreToLoad = compObj->findComponent<CoreComponent>();
	}

	if(coreToLoad)
	{
		coreToLoad->addModule(slot, id, name);

		// network sync
		ChangeModulePacket packet;
		packet.slot = slot;
		packet.moduleid = id;
		strcpy_s(packet.module, 32, name);
		packet.add = true;
		packet.companion = companion;
		me->game->getNetwork()->send(packet);
	}
	
}

void LobbyScene::setReady(bool ready)
{
	// NETWORK
	ReadyPacket packet;
	packet.clientReady	= ready;
	packet.uid			= me->playerNet->getID();
	me->game->getNetwork()->send(packet);

	me->playerNet->setReady(ready);
}

bool LobbyScene::getReady()
{
	return me->playerNet->isReady();
}

void LobbyScene::createCompanion()
{
	CompanionPacket packet;
	packet.uid		= me->playerNet->getID();
	packet.add		= true;
	packet.team		= me->playerCore->team;
	packet.masterUserID	= ((Client*)me->window)->getUserID();
	me->game->getNetwork()->send(packet);
}

void LobbyScene::removeCompanion()
{
	CompanionPacket packet;
	packet.uid		= me->playerNet->getID();
	packet.add		= false;
	packet.team		= me->playerCore->team;
	packet.masterUserID	= ((Client*)me->window)->getUserID();
	me->game->getNetwork()->send(packet);
}

bool LobbyScene::hasCompanion()
{
	return (me->player->getCompanion() != NULL);
}

void LobbyScene::disconnect()
{
	MasterSendAsync async;
	ServerLeavePacket packet;
	packet.user_id = ((Client*)me->window)->getUserID();
	async( packet, sizeof(packet) );

	me->game->getNetwork()->disconnect();
	me->game->disconnect();
	me->chat->clearLog();
}

bool LobbyScene::joinTeam( int team )
{
	bool result = false;

	me->join_team = team;
	TeamSelectPacket packet;
	packet.team = team;
	me->game->getNetwork()->send( packet );
	result = true;

	return result;
}