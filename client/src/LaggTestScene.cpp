

#include "Client.h"

#include "LaggTestScene.h"

#include "Common.h"
#include "Object.h"
#include "LogicHandler.h"
#include "RenderComponent.h"
#include "GameConnector.h"
#include "ScriptEnvironment.h"
#include "PhysicsComponent.h"
#include "CoreComponent.h"
#include "ControllerComponent.h"
#include "AnimationComponent.h"
#include "NetworkComponent.h"
#include "SkillComponent.h"
#include "GameHandler.h"
#include "CommonPackets.h"
#include "SoundSubsystem.h"
#include "EffectComponent.h"
#include "StatsComponent.h"

#include "Framework.h"

#include "MemLeaks.h"

#include "PlayerVisual.h"

#include "Module.h"

#include "Chat.h"
#include "NetworkSubsystem.h"
#include "NetworkSubsystemClient.h"

#include "Events.h"

LaggTestScene::LaggTestScene(const InitData & in)
	: Scene(in.game->getLogic(), in.rend, in.window), game(in.game), client(in.client)
{
	renderModule = in.renderModule;
	camera = renderModule->getCamera();

	camera->setPosition(Vector3(0,100 * 6,-80 * 5));
	camera->setLookAt(Vector3(0, 10.0f, -100));
	camera->setClipRange(40, 2000);

	camera->setProjection3D(70, 16 / 9);
}

void LaggTestScene::update()
{
	renderModule->preFrame();


	std::stringstream format;
	format << "Fps: " << client->getFps();
	SetWindowText(client->getHandle(), format.str().c_str()); 
	
	
	if (fireTimer.accumulate(0.1f))
	{
		game->createObjectExt("FireBolt", Vector3(random(-300, 30), 200, random(-30, 30)), 0);
	}

	
	
	/*
	if (restartTimer.accumulate(8))
	{
		client->gotoSceneUsingTransition("LaggTest", 0, 40);
	}
	*/
	
}

void LaggTestScene::render()
{
}

void LaggTestScene::onEnter()
{
	//renderModule->getRenderer()->clearLayers();
	renderModule->clean();


	renderModule->updateTerrain();

	renderModule->bind();

	/*
	static bool first = 0;
	if (first == 0)
	{
		for (int i = 0; i < 10; ++i)
			sceneryObjects.push_back(game->createObjectExt("Player", Vector3(-100 + i * 30, 10, 10), Vector3(0, 0, 0)));

		//first = 1;
	}
	*/
	

	/*
	renderModule->addCircleOfLinks();
	renderModule->addCenterPowerLink();

	sceneryObjects.push_back(game->createObjectExt("Plate", Vector3(0, 0, 0), Vector3(0, 0, 0), NULL, NULL));

	for (unsigned int i = 0; i < renderModule->getLinks(); ++i)
	{
		Vector3 p = renderModule->getLinkPosition(i);

		sceneryObjects.push_back(game->createObjectExt("Spire", p - Vector3(0, 60, 0), p, NULL, NULL));

		sceneryObjects.push_back(game->createObjectExt("Player", p * 0.5, p * 0.5));
	}
	*/

	
	
}

void LaggTestScene::onLeave()
{
	renderModule->clearAutoScenery();

	// clear scenery objects
	for (std::vector < Object* > ::iterator i = sceneryObjects.begin(); i != sceneryObjects.end(); ++i)
	{
		game->destroyObject(*i);
	}

	sceneryObjects.clear();
}

void LaggTestScene::onLoad()
{
}

bool LaggTestScene::onInput(int sym, int state)
{
	return true;
}

bool LaggTestScene::onMouseMovement(int x, int y)
{
	return true;
}

bool LaggTestScene::onCreateObject(Object* object)
{
	
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();
	
	if (rendcomp)
	{
		renderModule->addObject(rendcomp);
	}	
	
	
	AnimationComponent* animcomp = object->findComponent<AnimationComponent>();

	if (animcomp)
	{
		renderModule->addLinkedParticle(animcomp);
	}
	
	
	EffectComponent* effcomp = object->findComponent<EffectComponent>();

	if (effcomp)
	{
		
		PhysicsComponent * com = object->findComponent<PhysicsComponent>();
		if (com)
		{
			//std::cout << "spawn effect " << com->getPosition() << std::endl;

			Vector3 normal = Vector3(0, 1, 0); // riktning p� terr�ngen/surface
			normal.normalize();

			renderModule->spawnEffect(effcomp->getName(), com->getPosition() + Vector3(0, 15, 0), normal);
		}
	}
	
	return true;
}

bool LaggTestScene::onDestroyObject(Object* object)
{
	return true;
}

void LaggTestScene::onKillObject(Object* killed, Object* killer)
{
}

void LaggTestScene::onFocus()
{
}

void LaggTestScene::onFocusLost()
{
}