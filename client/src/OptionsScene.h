#ifndef OPTIONS_SCENE_H

#define OPTIONS_SCENE_H

#include "Scene.h"

#include "Framework.h"

#include "ListBox.h"

class OptionsScene : public Scene
{

public:
	OptionsScene();
	OptionsScene(LogicHandler* handler, Renderer* rend, Window* wndw, Font2D* font, Graphics* graphics);
	~OptionsScene();

	void update();
	void render();

	void onEnter();							// Defines what should be done upon entering the scene
	void onLeave();							// Defines what should be done upon leaving the scene
	void onLoad();							// Defines what happens after a load

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	bool onCreateObject(Object* object);
	bool onDestroyObject(Object* object);

	void onConnected(unsigned int id);

	void onFocus();
	void onFocusLost();

	void onRender2D();

	static void applySettings(int ssao, int fog, int shadows);

	static void resetKeybinds();

	bool isRestartRequiredToApplySettings();

	struct Stage2D : public Stage
	{
		OptionsScene * me;
		bool visible;

		Stage2D(OptionsScene * me) : me(me), visible(true)
		{
		}

		void apply()
		{
			if( visible )
				me->onRender2D();
		}
	};

	void refreshKeybinds();

	Client* const getClient();
private:
	void buildLayers();
	void clearLayers();

	static OptionsScene* me;

private:

	Stage2D *	layer2d;
	Layer *		layer;
	Camera		camera;
	Timer		luaTimer;

	ListBox*	resolutionBox;
	ListBox*	keyBindingBox;

	unsigned int firstWidth;
	unsigned int firstHeight;

	std::string selected;
};


#endif