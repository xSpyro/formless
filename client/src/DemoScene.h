
#ifndef DEMOSCENE_H
#define DEMOSCENE_H

#include "Scene.h"
#include "Framework.h"

#include "RenderModule.h"

class Object;
class PhysicsComponent;
class ControllerComponent;
class GameHandler;
class Chat;
class NetworkSubsystem;

class DemoScene : public Scene
{

public:

	DemoScene();
	DemoScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game, Chat* chat = NULL);
	virtual ~DemoScene();

	void update();
	void render();

	void onEnter();
	void onLeave();
	void onLoad();

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

private:

	RenderModule renderModule;
	Camera		camera;

	GameHandler * game;

	Timer timer;
};

#endif