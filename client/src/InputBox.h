#ifndef INPUTBOX__H
#define INPUTBOX__H

#include "Font2D.h"

class InputBox
{
public:
	InputBox( Font2D* font = NULL );
	~InputBox();

	void			onInput( int key, int state );
	void			onClick( float x, float y );
	void			bindCallback( void (*callback)(const std::string &) );

	void			setMaxChars( unsigned int max );	// Default is 255

	void			setLabel( const std::string & label );
	void			setText( const std::string & text );
	std::string		getText();

	void			clear();
	void			render();

	void			setPos( float x, float y, float w, float h );
	D2D1_RECT_F		getPos();

	void			setColor( float r, float g, float b, float a );

	void			setVisible( bool status );
	bool			isVisible();

	void			setActive( bool status );
	bool			isActive();

	void			allowSpecialChars( bool status );
	void			setPassword( bool status );

private:
	std::string		label;
	std::string		text;
	BYTE			kbBuf[256];
	D2D1_RECT_F		pos;
	D3DXCOLOR		color;
	unsigned int	cursorPos;
	unsigned int	maxChars;
	bool			visible;
	bool			active;
	bool			allowSpecial;
	bool			password;

	void			(*callback)(const std::string &);

	// Handlers
	Font2D*			font;
};

#endif
