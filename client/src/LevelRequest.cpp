#include "Client.h"

#include "LevelRequest.h"
#include "CommonPackets.h"


LevelRequest::LevelRequest( boost::asio::io_service& service, const std::string& host, Client* client )
	: FileRequestInterface( service, host ), log( std::cout ), client(client)
{
	this->request( "LEVEL" );
}

LevelRequest::~LevelRequest()
{
}

void LevelRequest::onStart()
{
	log( "Level transfer started" );
}

void LevelRequest::onRate( unsigned int bytes, unsigned int total )
{
	log( "Transferred ", bytes, " of ", total, " bytes" );
}

void LevelRequest::onDone( unsigned char * data, unsigned int bytes )
{
	client->saveLevelData( data, bytes );
	log( "Level transfer done" );

	delete this;
}
