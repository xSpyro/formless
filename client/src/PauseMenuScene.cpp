
#include "PauseMenuScene.h"
#include "LogicHandler.h"

#include "MemLeaks.h"

PauseMenuScene::PauseMenuScene()
	: Scene()
{

}

PauseMenuScene::PauseMenuScene(LogicHandler* handler, Renderer* rend, Window* wndw)
	: Scene(handler, rend, wndw)
{
	me = this;
	layer = renderer->createLayer();

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);
	camera.setProjection3D(45, 16 / 9.0f);

	// camera set

	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));


	layer2d = New Stage2D(this);
	layer2d->visible = true;


	// register some global functions to lua
	EventLua::Object g = parent->getScript()->getGlobals();
	//g.bind("addMod", &addMod);

	File file;
	FileSystem::get("scripts/pausemenu.lua", &file);
	parent->getScript()->execute(file.data, file.size);
}

PauseMenuScene::~PauseMenuScene()
{
	delete layer2d;
}

void PauseMenuScene::update()
{
	camera.update();
}

void PauseMenuScene::render()
{

}

void PauseMenuScene::onEnter()
{
	//renderer->insertLayer(10, layer);
	//renderer->insertStage(2000, layer2d);
	buildLayers();

	luaTimer.restart();
}

void PauseMenuScene::onLeave()
{
	clearLayers();
	//renderer->clearStage(10);
	//renderer->clearStage(2000);
}

bool PauseMenuScene::onInput(int sym, int state)
{
	if (layer2d->visible)
	{
		if(state != 0)
		{
			if (sym == 'U')
			{
				EventLua::Object reset = parent->getScript()->getGlobals().get("reset");
				reset.call();
			}
			else if(sym == VK_ESCAPE)
			{
				parent->deactivateSceneOnUpdate("PauseMenu");
			}
		}

		parent->getScript()->getGlobals()["onPauseMenuInput"](sym, state);
	}
	

	return true;
}

void PauseMenuScene::onFocus()
{
	//renderer->insertLayer(10, layer);
	//renderer->insertStage(2000, layer2d);
	buildLayers();

	layer2d->visible = true;
}

void PauseMenuScene::onFocusLost()
{
	layer2d->visible = false;
}

bool PauseMenuScene::onMouseMovement(int x, int y)
{
	return true;
}

void PauseMenuScene::onRender2D()
{
	if(layer2d->visible)
	{
		// call lua here
		if (luaTimer.accumulate(1.0f))
		{
			File file;
			FileSystem::get("scripts/pausemenu.lua", &file);
			if (!parent->getScript()->execute(file.data, file.size))
				std::cout<< parent->getScript()->getLastError();
		}
		parent->getScript()->getGlobals()["onPauseMenuRender"]();
	}
}

PauseMenuScene * PauseMenuScene::me = NULL;

void PauseMenuScene::buildLayers()
{
	clearLayers();
	renderer->insertLayer(10, layer);
	renderer->insertStage(2000, layer2d);
}

void PauseMenuScene::clearLayers()
{
	renderer->clearStage(10);
	renderer->clearStage(2000);
}
