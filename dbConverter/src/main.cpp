#include "CSVManager.h"

int main()
{
	CSVManager db;

	if( db.openDatabase( "accounts.csv" ) )
	{
		db.addFieldLabel( "Playtime" );
		db.addFieldLabel( "NextItemAt" );

		for( unsigned int i = 0; i < db.getNrEntries(); i++ )
		{
			db.getEntry( i )->addField( 0 );
			db.getEntry( i )->addField( 600 );
		}
	}

	db.closeDatabase();

	return 0;
}
