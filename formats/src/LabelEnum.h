#ifndef LABEL_ENUM
#define LABEL_ENUM

enum LabelEnum
{
	LABEL_END = 0,
	LABEL_INVALID,
	LABEL_FORMAT_VERSION,
	LABEL_USE_MOD,

	LABEL_INFO,
		LABEL_AUTHOR,
		LABEL_LEVEL_VERSION,
		LABEL_LEVEL_NAME,

		LABEL_MOD_VERSION,
		LABEL_MOD_NAME,

		LABEL_DESCRIPTION,
	LABEL_LUA_FILES,
		LABEL_NAME,
		//LABEL_TYPE,
		LABEL_DATA,
	LABEL_HEIGHTMAP,
	LABEL_BLENDMAP,
	LABEL_OBJECT_TEMPLATE,
		//LABEL_NAME,
	LABEL_OBJECT,
		//LABEL_NAME,
			LABEL_SCALE,
			LABEL_POSITION,
			LABEL_ROTATION,
		LABEL_RELATIONS, // After all objects have been created
			LABEL_OBJECT_CREATOR, // No necessary order, e.g. creator, child, creator is valid in a file
			LABEL_OBJECT_CHILD,

	// Enums added later... Must be in the order off added or old files will not be valid anymore
	LABEL_TYPE,
};

#endif