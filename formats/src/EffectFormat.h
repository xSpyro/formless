#ifndef PAF_EFFECT_FORMAT
#define PAF_EFFECT_FORMAT

#include "SharedFormatInterface.h"
//#include "Particle.h"
#include "ParticleAnimation.h"
#include "ParticleEffect.h"

class ParticleSystem;
class RenderModule;

class EffectFormat : public SharedFormatInterface
{
public:
	EffectFormat(ParticleEffect * p, RenderModule * RM, ParticleSystem * system = NULL);

	bool importFromFile(const File & file, const std::string& fileName);
	bool exportToFile(File & file);

	void shutDown();

	void initBehaviorByPackage(ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * p, ParticleCustomBehavior * b);

	void initEmitter(ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * p);
	void initPackageByBehavior(ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * p, ParticleCustomBehavior * b);

private:

	void importVersionOne(std::stringstream & str, const std::string& fileName);
	void importVersionTwo(std::stringstream & str, const std::string& fileName);

	typedef std::vector< ParticleAnimation > ParticleAnimationVector;

	ParticleAnimationVector animations;

	int version;

	ParticleEffect * pe;
	ParticleSystem * system;
	RenderModule * renderMod;
};


#endif



