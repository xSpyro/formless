
#ifndef SHARED_FORMAT_STORE
#define SHARED_FORMAT_STORE

#include "Common.h"

#include "SharedFormatInterface.h"

class SharedFormatStore
{

	typedef std::map < std::string, SharedFormatInterface* > Formats;

public:

	SharedFormatStore();

	bool registerFormat(const std::string & ext, SharedFormatInterface * format);

	bool importFromFile(const std::string & fname);
	bool exportToFile(const std::string & fname);

private:

	SharedFormatInterface * queryFormat(const std::string & fname);

private:

	Formats formats;

	Logger log;
};

#endif