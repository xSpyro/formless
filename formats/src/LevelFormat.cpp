#include "LevelFormat.h"
#include "LabelEnum.h"
#include "LuaFileManager.h"

#define FAIL_CHECK_LOG(x, msg) if (!x) {log.alert(msg); return false;}

LevelFormat::LevelFormat(GameHandler * game) : SharedFormatInterface(), game(game)
{}


bool LevelFormat::importFromFile(const File & file, const std::string& fileName)
{
	log.info("Importing from file: ", fileName);
	std::stringstream f(std::stringstream::in | std::stringstream::out | std::stringstream::binary);
	f.write((char *)file.data, file.size);
	/*while (true)
	{
		f.read((char *)c, sizeof(int));
		memcpy(&data, c, sizeof(int));
		c0 = c[0];
		c1 = c[1];
		c2 = c[2];
		c3 = c[3];
	}*/
	
	int version = readVersion(f);
	log.info("Version: ", version);
	// TODO: check version number
	if (version == -1)
		return false;
	//f.read();
	while (!f.eof())
	{
		LabelEnum label;
		f.read((char *)&label, sizeof(LabelEnum));

		switch (label)
		{
		case LABEL_INFO:
			break;
		case LABEL_HEIGHTMAP:
			FAIL_CHECK_LOG(game->loadHeightMap(f), "Failed to load heightmap.");
			log.info("Heightmap loaded(or skipped if none game have none).");
			break;
		case LABEL_BLENDMAP: 
			FAIL_CHECK_LOG(game->loadBlendMap(f), "Failed to load blendmap.");
			log.info("Blendmap loaded(or skipped if none game have none).");
			break;
		case LABEL_LUA_FILES:
			FAIL_CHECK_LOG(game->getLuaFileManager()->load(f), "Failed to load lua files.");
			log.info("Lua files loaded.");
			break;
		case LABEL_OBJECT_TEMPLATE:
			FAIL_CHECK_LOG(game->loadObjectTemplates(f), "Failed to load object templates.");
			log.info("Object templates loaded.");
			break;
		case LABEL_OBJECT:
			FAIL_CHECK_LOG(game->loadObjects(f), "Failed to load objects.");
			log.info("Objects loaded.");
			break;
		case LABEL_END:
			game->onLoad();
			log.info("File loaded successfully.");
			return true;
		default:
			log.alert("Undefined label.");
			// TODO: Log something...
			{
				char c[4];
				int i = label;
				int counter = 0;
				memcpy(c, &i, sizeof(int));

				while (!f.eof())
				{
					for (int n = 3; n > 0; --n)
						c[n] = c[n - 1];
					f.read(c, sizeof(char));
					memcpy(&i, c, sizeof(int));
					counter++;
				}
			}

			return false;
		}

	}
	game->onLoad();
	return false;
}

int LevelFormat::readVersion(std::istream &f)
{
	LabelEnum label;

	f.read((char *)&label, sizeof(LabelEnum));
	if (label != LABEL_FORMAT_VERSION)
		return -1;
	int rtn = -1;
	f.read((char *)&rtn, sizeof(int));
	return rtn;
}

bool LevelFormat::exportToFile(File & file)
{
	std::stringstream f(std::stringstream::in | std::stringstream::out | std::stringstream::binary);
	//std::ofstream f("SAVEFILE2.flf", std::ios::out | std::ios::binary);
	/*LuaFileManager * lfm = game->getLuaFileManager();
	std::fstream f("SAVEFILE.flf", std::fstream::binary | std::fstream::out);
	lfm->save(f);
	f.close();*/
	log.info("Exporting to a .flf file");

	LabelEnum label = LABEL_FORMAT_VERSION;
	int version = 0; // TODO: in another way?
	f.write((char*)&label, sizeof(LabelEnum));
	f.write((char*)&version, sizeof(int));

	FAIL_CHECK_LOG(game->saveHeightMap(f), "Failed to export heightmap.");
	FAIL_CHECK_LOG(game->saveBlendMap(f), "Failed to export blendmap.");
	FAIL_CHECK_LOG(game->getLuaFileManager()->save(f), "Failed to export lua files.");
	FAIL_CHECK_LOG(game->saveObjectTemplates(f), "Failed to export object templates.");
	FAIL_CHECK_LOG(game->saveObjects(f), "Failed to export objects.");


	label = LABEL_END;
	f.write((char *)&label, sizeof(LabelEnum));

	//f.close();

	f.seekg (0, std::stringstream::end);
	int size = (int)f.tellg();
	f.seekg (0, std::stringstream::beg);

	file.data = new char[size];
	f.read((char *)file.data, sizeof(char) * size);
	file.size = size;

	log.info("Exporting .flf done, file size: ", size);
	return true;
}
