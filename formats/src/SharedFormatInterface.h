
#ifndef SHARED_FORMAT_INTERFACE
#define SHARED_FORMAT_INTERFACE

#include "Common.h"
#include "../framework/src/Logger.h"

class SharedFormatInterface
{
public:
	virtual bool importFromFile(const File & file, const std::string& fileName);
	virtual bool exportToFile(File & file);

	static Logger log;
};
#endif