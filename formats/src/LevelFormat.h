#ifndef FLF_LEVEL_FORMAT
#define FLF_LEVEL_FORMAT
#include "SharedFormatInterface.h"

class LevelFormat : public SharedFormatInterface
{
public:
	LevelFormat(GameHandler * game);

	bool importFromFile(const File & file, const std::string& fileName);
	bool exportToFile(File & file);
private:
	int readVersion(std::istream &stream);

	GameHandler * game;
};

#endif