
#include "SharedFormatStore.h"

SharedFormatStore::SharedFormatStore()
	: log("SharedFormatStore")
{}

bool SharedFormatStore::registerFormat(const std::string & ext, SharedFormatInterface * format)
{
	Formats::iterator i = formats.find(ext);

	if (i == formats.end())
	{
		log("Registered ", ext);

		formats[ext] = format;
		return true;
	}

	return false;
}

bool SharedFormatStore::importFromFile(const std::string & fname)
{
	SharedFormatInterface * format = queryFormat(fname);

	bool useBase = true;
	if (std::string::npos != fname.find(":\\"))
		useBase = false;

	if (format)
	{
		// then see if file exists
		File file;
		if (FileSystem::get(fname, &file, useBase))
		{
			return format->importFromFile(file, fname);
		}
	}

	return false;
}

bool SharedFormatStore::exportToFile(const std::string & fname)
{
	SharedFormatInterface * format = queryFormat(fname);

	bool useBase = true;
	if (std::string::npos != fname.find(":\\"))
		useBase = false;

	if (format)
	{
		// then see if path is valid
		File file;
		if (format->exportToFile(file))
		{
			return FileSystem::write(fname, &file, useBase);
		}
	}

	return false;
}

SharedFormatInterface * SharedFormatStore::queryFormat(const std::string & fname)
{
	// search for extension in format database
	
	unsigned int dot = fname.rfind(".");
	if (dot == std::string::npos)
	{
		// format unknown
		return NULL;
	}

	std::string ext = fname.substr(dot + 1);

	// lookup extension

	if (!ext.empty())
	{
		Formats::iterator i = formats.find(ext);

		if (i != formats.end())
		{
			log("QueryFormat ", LOG_OK);

			// we found the format
			return i->second;
		}
	}

	return NULL;
}