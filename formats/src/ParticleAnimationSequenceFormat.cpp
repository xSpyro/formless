
#include "ParticleAnimationSequenceFormat.h"

#include "AnimationComponent.h"

#include "Framework.h"

#include "ShapeAnimation.h"
#include "AnimationSubsystem.h"

#include "MemLeaks.h"

// This is the header, 64bytes 
// an integer of 4 bytes, and 60bytes unused data
struct ShapeHeader
{
	int nr;
	char args[60];
};


ParticleAnimationSequenceFormat::ParticleAnimationSequenceFormat(AnimationSubsystem * as)
	: as(as)
{
	
}

bool ParticleAnimationSequenceFormat::importFromFile(const File & file, const std::string& fileName)
{
	// check if file is larger than header
	if(file.size > 64)
	{
		
		ShapeAnimation* animation = New ShapeAnimation();
		as->insertAnimation(fileName, animation);

		// storing our data
		std::vector<unsigned int> frames;
		std::vector<ShapeResult> shape;


		ShapeHeader* header;

		//read the header
		header = (ShapeHeader*)&file.data[0];

		// reserve memory
		frames.reserve(header->nr);
		shape.reserve(header->nr);

		// start will be at the byte after the header
		int start = sizeof(ShapeHeader);
		// size of each node in bytes (integer)
		int nodeSize = sizeof(unsigned int);
		// read the data
		for(int i = 0; i < header->nr; i++)
		{
			frames.push_back(0);

			std::memcpy(&frames.back(), &file.data[start + (i*nodeSize)], nodeSize);
		}

		// start will be after all frames
		start += (sizeof(unsigned int)*header->nr);
		// nodesize is as big as shaperesult
		nodeSize = sizeof(ShapeResult);
		// read the data
		for(int i = 0; i < header->nr; i++)
		{
			// reading a ShapeResult directly from the file-data

			ShapeResult res;
			std::memcpy(&res, &file.data[start + (i*nodeSize)], nodeSize);
			shape.push_back(res);
		}

		// recreate all the frames with our frame-data and shaperesult-data
		for(unsigned int i = 0; i < frames.size(); i++)
		{
			if(shape[i].count > 0)
				animation->recreateFrame(frames[i], &shape[i]);
		}

		return true;
	}
	return false;
}

bool ParticleAnimationSequenceFormat::exportToFile(File & file)
{
	
	return false;
}