#include "EffectFormat.h"

#include "LabelEnum.h"
#include "LuaFileManager.h"

#include "Logger.h"

#include "RenderModule.h"

EffectFormat::EffectFormat(ParticleEffect * p, RenderModule * RM, ParticleSystem * system) : pe(p), system(system), SharedFormatInterface(), version(2)
{
	//system = NULL;
	if( pe == NULL )
	{
		pe = new ParticleEffect();
		pe->created = true;
		//exit(0);
	}
	renderMod = RM;
}


bool EffectFormat::importFromFile(const File & file, const std::string& fileName)
{
	//exit(0);
	Logger log("ParticleFormatImportFormat");

	pe ? log.notice("success to pass") : log.notice("Failed to pass");

	std::stringstream f(std::stringstream::in | std::stringstream::out | std::stringstream::binary);
	
	f.write(file.data, file.size);

	int v    = -1,
		type = -1,
		nr_e = -1,
		size = -1;

	ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE b;

	f.read((char *)&v, sizeof(int));
	log.notice("Version: ", v);

	switch(v)
	{
	case 1:
		importVersionOne(f, fileName);
		break;

	case 2:
		importVersionTwo(f, fileName);
		//exit(0);
		break;

	default:
		exit(0);
		return false;
		break;
	}


	return true;

	/*
	if( version != v )
	{
		return false;
	}

	f.read((char *)&size, sizeof(int));
	log.notice("Size: ", size);
	char name[64];
	for(int i = 0; i < size; i++)
	{
		f.read((char *)&type, sizeof(int));
		if( type == 0 )
		{
			log.notice("Successfully found animation, starting read", i);
			ParticleAnimation a;
			pe->animations.push_back(a);
			int size_e = -1;
			f.read((char *)&size_e, sizeof(int));
			for(int j = 0; j < size_e; j++)
			{
				ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE b;
				pe->animations[i].effects.push_back(b);

				f.read((char *)&type, sizeof(int));

				f.read( (char *)&pe->animations[i].effects[j], sizeof(struct ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE) - sizeof(char)*128);

				memset(name, 0, 64);
				strcpy_s(name, 64, pe->animations[i].effects[j].behave_name);
	
				pe->animations[i].effects[j].behavior = new ParticleCustomBehavior();
				if(pe->animations[i].effects[j].life == 0)
				{
					pe->animations[i].effects[j].life = 1;
				}
				initBehaviorByPackage(&pe->animations[i].effects[j], pe->animations[i].effects[j].behavior);
				//pe->animations[i].effects[j].behavior->dir = Vector3(0, 0, 0);
				///pe->animations[i].effects[j].behavior->dirW = Vector3(0, 0, 0);
				initEmitter(&pe->animations[i].effects[j]);
				
				//pe->animations[i].effects[j].affectRate = 1.5f;
				if( system != 0 )
				{
					system->createBehavior(name, pe->animations[i].effects[j].behavior);
					system->addToGroup(fileName, name, pe->animations[i].effects[j].life, pe->animations[i].effects[j].spawnRate, pe->animations[i].effects[j].lifeAffectRate);
					log.notice("In system added: ", j);
				}
				pe->animations[i].effects[j].behavior = NULL;
				log.notice("In system added: ", j);





				log.notice("lifeAffectRate   ", pe->animations[i].effects[j].lifeAffectRate           );
				log.notice("AffectRate       ", pe->animations[i].effects[j].affectRate           );
				log.notice("displacementPo   ", pe->animations[i].effects[j].displacementPos 			);
				log.notice("colorPath		 ", pe->animations[i].effects[j].colorPath			    );    
				log.notice("speedPath        ", pe->animations[i].effects[j].speedPath       			);
				log.notice("endColor	     ", pe->animations[i].effects[j].endColor		   			);
				log.notice("middleColor      ", pe->animations[i].effects[j].middleColor     			);
				log.notice("startColor       ", pe->animations[i].effects[j].startColor      			);
				log.notice("endSpeed         ", pe->animations[i].effects[j].endSpeed        			);
				log.notice("middleSpeed      ", pe->animations[i].effects[j].middleSpeed     			);
				log.notice("startSpeed	     ", pe->animations[i].effects[j].startSpeed	   			);
				log.notice("endScale	     ", pe->animations[i].effects[j].endScale		   			);
				log.notice("startScale	     ", pe->animations[i].effects[j].startScale	   			);
				log.notice("spinMax		     ", pe->animations[i].effects[j].spinMax		   			);
				log.notice("spinMin		     ", pe->animations[i].effects[j].spinMin		   			);
				log.notice("distorsion       ", pe->animations[i].effects[j].distorsion      			);
				log.notice("gravity		     ", pe->animations[i].effects[j].gravity		   			);
				log.notice("randAngle        ", pe->animations[i].effects[j].randAngle     			);
				log.notice("intensityDir     ", pe->animations[i].effects[j].intensityDir    			);
				log.notice("spawnRate        ", pe->animations[i].effects[j].spawnRate                  );
				log.notice("life             ", pe->animations[i].effects[j].life                      );






















			}
		}
	}*/

	return false;
}
void EffectFormat::importVersionOne(std::stringstream & str, const std::string& fileName)
{
	Logger logger("ParticleFormatImportVersionOne");

	logger.notice("filename: ", fileName);
	int	type = -1,
		nr_e = -1,
		size = -1;

	str.read((char *)&size, sizeof(int));

	char name[64];
	for(int i = 0; i < size; i++)
	{
		str.read((char *)&type, sizeof(int));
		if( type == 0 )
		{
			ParticleAnimation a;
			pe->animations.push_back(a);
			int size_e = -1;
			str.read((char *)&size_e, sizeof(int));
			logger.notice("Added animation");
			for(int j = 0; j < size_e; j++)
			{
				ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE b;
				pe->animations[i].effects.push_back(b);

				str.read((char *)&type, sizeof(int));

				str.read( (char *)&pe->animations[i].effects[j], sizeof(struct ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE)-128*sizeof(char));

				memset(name, 0, 64);
				strcpy_s(name, 64, pe->animations[i].effects[j].behave_name);
	
				pe->animations[i].effects[j].behavior = new ParticleCustomBehavior();
				if(pe->animations[i].effects[j].life == 0)
				{
					pe->animations[i].effects[j].life = 1;
				}

				initEmitter(&pe->animations[i].effects[j]);
				logger.notice("Added emitter");
				
				if( system != 0 )
				{
					if( !system->createBehavior(name, pe->animations[i].effects[j].behavior) )
						return;
					
					system->addToGroup("basic", fileName, name, pe->animations[i].effects[j].life, pe->animations[i].effects[j].spawnRate, pe->animations[i].effects[j].lifeAffectRate);

				}
				#if defined(DEBUG) || defined(_DEBUG)
					logger.notice("Added to group");
					//strcpy_s(pe->animations[i].effects[j].behavior->type_name, 64, "hej");
					logger.notice("----------------[BEHAVIOR STATS]---------------");

					logger.notice("pi                ", pe->animations[i].effects[j].behavior->pi                );
					logger.notice("radx				 ", pe->animations[i].effects[j].behavior->radx				 );
					logger.notice("dir				 ", pe->animations[i].effects[j].behavior->dir				 );
					logger.notice("startColor		 ", pe->animations[i].effects[j].behavior->startColor		 );
					logger.notice("middleColor		 ", pe->animations[i].effects[j].behavior->middleColor		 );
					logger.notice("endColor			 ", pe->animations[i].effects[j].behavior->endColor			 );
					logger.notice("randAngleX		 ", pe->animations[i].effects[j].behavior->randAngleX		 );
					logger.notice("randAngleY		 ", pe->animations[i].effects[j].behavior->randAngleY		 );
					logger.notice("randAngleZ		 ", pe->animations[i].effects[j].behavior->randAngleZ		 );
					logger.notice("intensityDir		 ", pe->animations[i].effects[j].behavior->intensityDir		 );
					logger.notice("spinMax			 ", pe->animations[i].effects[j].behavior->spinMax			 );
					logger.notice("spinMin			 ", pe->animations[i].effects[j].behavior->spinMin			 );
					logger.notice("startScale		 ", pe->animations[i].effects[j].behavior->startScale		 );
					logger.notice("endScale			 ", pe->animations[i].effects[j].behavior->endScale			 );
					logger.notice("startSpeed		 ", pe->animations[i].effects[j].behavior->startSpeed		 );
					logger.notice("middleSpeed		 ", pe->animations[i].effects[j].behavior->middleSpeed		 );
					logger.notice("endSpeed			 ", pe->animations[i].effects[j].behavior->endSpeed			 );
					logger.notice("speedPath		 ", pe->animations[i].effects[j].behavior->speedPath		 );
					logger.notice("colorPath		 ", pe->animations[i].effects[j].behavior->colorPath		 );
					logger.notice("gravity			 ", pe->animations[i].effects[j].behavior->gravity			 );
					logger.notice("distorsion		 ", pe->animations[i].effects[j].behavior->distorsion		 );
					logger.notice("displacementPos	 ", pe->animations[i].effects[j].behavior->displacementPos	 );
					logger.notice("lifeAffectRate	 ", pe->animations[i].effects[j].behavior->lifeAffectRate	 );			 
					//logger.notice("type_name		 ", pe->animations[i].effects[j].behavior->type_name		 );
					logger.notice("life              ",	pe->animations[i].effects[j].life                        );	
					logger.notice("spawnRate         ", pe->animations[i].effects[j].spawnRate                   );
					logger.notice("lifeAffectRate    ", pe->animations[i].effects[j].lifeAffectRate              );
					logger.notice("name              ", name                                                     );

					logger.notice("----------------[BEHAVIOR END]----------------");

					logger.notice("----------------[PACKAGE STATS]----------------");


					logger.notice("startColor		 ", pe->animations[i].effects[j].startColor			 );
					logger.notice("middleColor		 ", pe->animations[i].effects[j].middleColor		 );
					logger.notice("endColor			 ", pe->animations[i].effects[j].endColor			 );
					logger.notice("randAngleX		 ", pe->animations[i].effects[j].randAngle.x		 );
					logger.notice("randAngleY		 ", pe->animations[i].effects[j].randAngle.y		 );
					logger.notice("randAngleZ		 ", pe->animations[i].effects[j].randAngle.z		 );
					logger.notice("intensityDir		 ", pe->animations[i].effects[j].intensityDir		 );
					logger.notice("spinMax			 ", pe->animations[i].effects[j].spinMax			 );
					logger.notice("spinMin			 ", pe->animations[i].effects[j].spinMin			 );
					logger.notice("startScale		 ", pe->animations[i].effects[j].startScale			 );
					logger.notice("endScale			 ", pe->animations[i].effects[j].endScale			 );
					logger.notice("startSpeed		 ", pe->animations[i].effects[j].startSpeed			 );
					logger.notice("middleSpeed		 ", pe->animations[i].effects[j].middleSpeed		 );
					logger.notice("endSpeed			 ", pe->animations[i].effects[j].endSpeed			 );
					logger.notice("speedPath		 ", pe->animations[i].effects[j].speedPath			 );
					logger.notice("colorPath		 ", pe->animations[i].effects[j].colorPath			 );
					logger.notice("gravity			 ", pe->animations[i].effects[j].gravity			 );
					logger.notice("distorsion		 ", pe->animations[i].effects[j].distorsion			 );
					logger.notice("displacementPos	 ", pe->animations[i].effects[j].displacementPos	 );
					logger.notice("lifeAffectRate	 ", pe->animations[i].effects[j].lifeAffectRate		 );			 
					logger.notice("type_name		 ", pe->animations[i].effects[j].type_name			 );
					logger.notice("life              ",	pe->animations[i].effects[j].life                );	
					logger.notice("spawnRate         ", pe->animations[i].effects[j].spawnRate           );
					logger.notice("lifeAffectRate    ", pe->animations[i].effects[j].lifeAffectRate      );
					logger.notice("name              ", name                                             );

					logger.notice("----------------[PACKAGE END]----------------");


				#endif

				pe->animations[i].effects[j].behavior = NULL;


			}
		}
	}
	logger.close();
}
void EffectFormat::importVersionTwo(std::stringstream & str, const std::string& fileName)
{
	Logger logger("ParticleFormatImportVersionTwo");

	if( renderMod == NULL )
		return;
	logger.notice("filename: ", fileName);
	int	type = -1,
		nr_e = -1,
		size = -1;

	str.read((char *)&size, sizeof(int));

	char name[64];
	for(int i = 0; i < size; i++)
	{
		str.read((char *)&type, sizeof(int));
		if( type == 0 )
		{
			ParticleAnimation a;
			pe->animations.push_back(a);
			int size_e = -1;
			str.read((char *)&size_e, sizeof(int));
			#if defined(DEBUG) || defined(_DEBUG)
				logger.notice("Added animation");
			#endif
			for(int j = 0; j < size_e; j++)
			{
				ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE b;
				pe->animations[i].effects.push_back(b);

				str.read((char *)&type, sizeof(int));
				//memset(pe->animations[i].effects[j].type_name, 0, 64);
				str.read( (char *)&pe->animations[i].effects[j], sizeof(struct ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE));

				memset(name, 0, 64);
				strcpy_s(name, 64, pe->animations[i].effects[j].behave_name);
				//strcpy_s(name, 64, "Hejsan000");
				pe->animations[i].effects[j].behavior = new ParticleCustomBehavior();
				if(pe->animations[i].effects[j].life == 0)
				{
					//pe->animations[i].effects[j].life = 1;
				}

				initEmitter(&pe->animations[i].effects[j]);
				#if defined(DEBUG) || defined(_DEBUG)
					logger.notice("Added emitter");
				#endif
				//strcpy_s(pe->animations[i].effects[j].type_name, 64, "heat");
				if( system != 0 )
				{
					if( !system->createBehavior(name, pe->animations[i].effects[j].behavior) )
						return;
					std::stringstream os;
					os << fileName << pe->animations[i].effects[j].type_name;
					std::string type_ = os.str();

					std::string type_name = pe->animations[i].effects[j].type_name;
					std::string group_name = fileName;
					std::string behavior_name = pe->animations[i].effects[j].behave_name;

					//system->createType( type_name, system->getRenderer()->shader()->get("shaders/cpu_particle.vs") );
					bool success = renderMod->addParticleType(type_name, type_name);
					//if( !success )
					//	type_name = "basic";

					system->addToGroup(
						type_name,
						group_name, 
						behavior_name, 
						pe->animations[i].effects[j].life, 
						pe->animations[i].effects[j].spawnRate, 
						pe->animations[i].effects[j].lifeAffectRate);
					}

					#if defined(DEBUG) || defined(_DEBUG)
						logger.notice("Added to group");
						//strcpy_s(pe->animations[i].effects[j].behavior->type_name, 64, "hej");
						logger.notice("----------------[BEHAVIOR STATS]---------------");

						logger.notice("pi                ", pe->animations[i].effects[j].behavior->pi                );
						logger.notice("radx				 ", pe->animations[i].effects[j].behavior->radx				 );
						logger.notice("dir				 ", pe->animations[i].effects[j].behavior->dir				 );
						logger.notice("startColor		 ", pe->animations[i].effects[j].behavior->startColor		 );
						logger.notice("middleColor		 ", pe->animations[i].effects[j].behavior->middleColor		 );
						logger.notice("endColor			 ", pe->animations[i].effects[j].behavior->endColor			 );
						logger.notice("randAngleX		 ", pe->animations[i].effects[j].behavior->randAngleX		 );
						logger.notice("randAngleY		 ", pe->animations[i].effects[j].behavior->randAngleY		 );
						logger.notice("randAngleZ		 ", pe->animations[i].effects[j].behavior->randAngleZ		 );
						logger.notice("intensityDir		 ", pe->animations[i].effects[j].behavior->intensityDir		 );
						logger.notice("spinMax			 ", pe->animations[i].effects[j].behavior->spinMax			 );
						logger.notice("spinMin			 ", pe->animations[i].effects[j].behavior->spinMin			 );
						logger.notice("startScale		 ", pe->animations[i].effects[j].behavior->startScale		 );
						logger.notice("endScale			 ", pe->animations[i].effects[j].behavior->endScale			 );
						logger.notice("startSpeed		 ", pe->animations[i].effects[j].behavior->startSpeed		 );
						logger.notice("middleSpeed		 ", pe->animations[i].effects[j].behavior->middleSpeed		 );
						logger.notice("endSpeed			 ", pe->animations[i].effects[j].behavior->endSpeed			 );
						logger.notice("speedPath		 ", pe->animations[i].effects[j].behavior->speedPath		 );
						logger.notice("colorPath		 ", pe->animations[i].effects[j].behavior->colorPath		 );
						logger.notice("gravity			 ", pe->animations[i].effects[j].behavior->gravity			 );
						logger.notice("distorsion		 ", pe->animations[i].effects[j].behavior->distorsion		 );
						logger.notice("displacementPos	 ", pe->animations[i].effects[j].behavior->displacementPos	 );
						logger.notice("lifeAffectRate	 ", pe->animations[i].effects[j].behavior->lifeAffectRate	 );			 
						logger.notice("type_name		 ", pe->animations[i].effects[j].behavior->type_name		 );
						logger.notice("life              ",	pe->animations[i].effects[j].life                        );	
						logger.notice("spawnRate         ", pe->animations[i].effects[j].spawnRate                   );
						logger.notice("lifeAffectRate    ", pe->animations[i].effects[j].lifeAffectRate              );
						logger.notice("name              ", name                                                     );

						logger.notice("----------------[BEHAVIOR END]----------------");

						logger.notice("----------------[PACKAGE STATS]----------------");


						logger.notice("startColor		 ", pe->animations[i].effects[j].startColor		 );
						logger.notice("middleColor		 ", pe->animations[i].effects[j].middleColor		 );
						logger.notice("endColor			 ", pe->animations[i].effects[j].endColor			 );
						logger.notice("randAngleX		 ", pe->animations[i].effects[j].randAngle.x		 );
						logger.notice("randAngleY		 ", pe->animations[i].effects[j].randAngle.y		 );
						logger.notice("randAngleZ		 ", pe->animations[i].effects[j].randAngle.z		 );
						logger.notice("intensityDir		 ", pe->animations[i].effects[j].intensityDir		 );
						logger.notice("spinMax			 ", pe->animations[i].effects[j].spinMax			 );
						logger.notice("spinMin			 ", pe->animations[i].effects[j].spinMin			 );
						logger.notice("startScale		 ", pe->animations[i].effects[j].startScale		 );
						logger.notice("endScale			 ", pe->animations[i].effects[j].endScale			 );
						logger.notice("startSpeed		 ", pe->animations[i].effects[j].startSpeed		 );
						logger.notice("middleSpeed		 ", pe->animations[i].effects[j].middleSpeed		 );
						logger.notice("endSpeed			 ", pe->animations[i].effects[j].endSpeed			 );
						logger.notice("speedPath		 ", pe->animations[i].effects[j].speedPath		 );
						logger.notice("colorPath		 ", pe->animations[i].effects[j].colorPath		 );
						logger.notice("gravity			 ", pe->animations[i].effects[j].gravity			 );
						logger.notice("distorsion		 ", pe->animations[i].effects[j].distorsion		 );
						logger.notice("displacementPos	 ", pe->animations[i].effects[j].displacementPos	 );
						logger.notice("lifeAffectRate	 ", pe->animations[i].effects[j].lifeAffectRate	 );			 
						logger.notice("type_name		 ", pe->animations[i].effects[j].type_name		 );
						logger.notice("life              ",	pe->animations[i].effects[j].life                        );	
						logger.notice("spawnRate         ", pe->animations[i].effects[j].spawnRate                   );
						logger.notice("lifeAffectRate    ", pe->animations[i].effects[j].lifeAffectRate              );
						logger.notice("name              ", name                                                     );

						logger.notice("----------------[PACKAGE END]----------------");
			
				#endif

				pe->animations[i].effects[j].behavior = NULL;



			}
		}
	}


















	logger.close();
}

bool EffectFormat::exportToFile(File & file)
{
	//exit(0);
	std::stringstream f(std::stringstream::in | std::stringstream::out | std::stringstream::binary);

	if( pe == NULL )
		return false;

	Logger log("ParticleFormat");

	int type = -1;
	int nr_e = 0;

	int size = (int)pe->animations.size();
	//std::ofstream out(file, std::ios::out | std::ios::binary);

	f.write((char *)&version, sizeof(int));
	f.write((char *)&size, sizeof(int));

	//l.notice("Version: ", version);

	int debug_a = sizeof(class ParticleAnimation);
	int debug_e = sizeof(struct ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE);

	#if defined(DEBUG) || defined(_DEBUG)
		log.notice("Animation size saved: ", size);
		log.notice("Size of ParticleAnimation", debug_a);
		log.notice("Size of BehaviorPackage", debug_e);
	#endif

	for(int i = size-1; i > -1; i--)
	{
		type = 0;
		int size_e = (int)pe->animations[i].effects.size();
		f.write((char *)&type, sizeof(int));
		log.notice("Type saved: ", type);
		log.notice("Emitter size saved: ", size);
		f.write((char *)&size_e, sizeof(int));
		for(int j = size_e-1; j > -1; j--)
		{
			type = 1;
			f.write((char *)&type, sizeof(int));
			f.write(
				(char *)&pe->animations[i].effects[j],
				sizeof(struct ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE));
			log.notice("Type name: ", pe->animations[i].effects[j].type_name);
		}
	}

	f.seekg (0, std::stringstream::end);
	int f_size = (int)f.tellg();
	f.seekg (0, std::stringstream::beg);

	file.data = new char[f_size];
	f.read((char *)file.data, sizeof(char) * f_size);
	file.size = f_size;

	return true;

}
void EffectFormat::shutDown()
{
	if( pe->created )
	{
		for(int i = 0; i < (int)pe->animations.size(); i++)
		{
			pe->animations[i].effects.clear();
		}
		pe->animations.clear();
		delete pe;
		pe = NULL;
	}
}
void EffectFormat::initBehaviorByPackage(ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * p, ParticleCustomBehavior * b)
{
	b->lifeAffectRate  = p->lifeAffectRate;
	b->displacementPos = p->displacementPos;
	b->colorPath       = p->colorPath;
	b->speedPath       = p->speedPath;
	b->endColor		   = p->endColor;
	b->middleColor     = p->middleColor;
	b->startColor      = p->startColor;
	b->endSpeed        = p->endSpeed;
	b->middleSpeed     = p->middleSpeed;
	b->startSpeed	   = p->startSpeed;
	b->endScale		   = p->endScale;
	b->startScale	   = p->startScale;
	b->spinMax		   = p->spinMax;
	b->spinMin		   = p->spinMin;
	b->distorsion      = p->distorsion;
	b->gravity		   = p->gravity;
	b->randAngleX      = p->randAngle.x;
	b->randAngleY      = p->randAngle.y;
	b->randAngleZ      = p->randAngle.z;
	b->intensityDir    = p->intensityDir;

	#if defined(DEBUG) || defined(_DEBUG)
		Logger log("InitBehaviorByPackage");
		log.notice("lifeAffectRate   ", b->lifeAffectRate            );
		log.notice("displacementPo   ", b->displacementPos 			);
		log.notice("colorPath		    ", b->colorPath				    );    
		log.notice("speedPath        ", b->speedPath       			);
		log.notice("endColor		     ", b->endColor		   			);
		log.notice("middleColor      ", b->middleColor     			);
		log.notice("startColor       ", b->startColor      			);
		log.notice("endSpeed         ", b->endSpeed        			);
		log.notice("middleSpeed      ", b->middleSpeed     			);
		log.notice("startSpeed	   ", b->startSpeed	   			);
		log.notice("endScale		     ", b->endScale		   			);
		log.notice("startScale	   ", b->startScale	   			);
		log.notice("spinMax		   ", b->spinMax		   			);
		log.notice("spinMin		   ", b->spinMin		   			);
		log.notice("distorsion       ", b->distorsion      			);
		log.notice("gravity		   ", b->gravity		   			);
		log.notice("randAngleX       ", b->randAngleX      			);
		log.notice("randAngleY       ", b->randAngleY      			);
		log.notice("randAngleZ       ", b->randAngleZ      			);
		log.notice("intensityDir     ", b->intensityDir    			);

	#endif

	log.close();











}
void EffectFormat::initEmitter(ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * p)
{
	//p->emitter->setLife( 2 );
	//ps_log.notice("life ", p->life);

	//p->emitter->setLifeAffectRate( 1.2f );
	//ps_log.notice("life_affect_rate ", p->lifeAffectRate);

	//p->emitter->setSpawnBox( p->spawnBox );

	//p->emitter->setSpawnRate( p->spawnRate );
	
	p->behavior->endColor    = p->endColor;
	p->behavior->startColor  = p->startColor;
	p->behavior->middleColor = p->middleColor;
	
	p->behavior->endScale   = p->endScale;
	p->behavior->startScale = p->startScale;
	//ps_log.notice("s_scale ", p->startScale);
	
	p->behavior->randAngleX   = p->randAngle.x;
	p->behavior->randAngleY   = p->randAngle.y;
	p->behavior->randAngleZ   = p->randAngle.z;
	p->behavior->intensityDir = p->intensityDir;
	p->behavior->colorPath    = p->colorPath;
	p->behavior->speedPath    = p->speedPath;

	p->behavior->spinMin = p->spinMin;
	p->behavior->spinMax = p->spinMax;

	p->behavior->gravity      = p->gravity;

	p->behavior->distorsion   = p->distorsion;

	p->behavior->startSpeed  = p->startSpeed;	
	p->behavior->middleSpeed = p->middleSpeed;	
	p->behavior->endSpeed    = p->endSpeed;

	p->behavior->displacementPos = p->displacementPos;
	p->behavior->lifeAffectRate = p->affectRate;
	//ps_log.notice("life_affect_rate ", p->affectRate);
	

}
void initPackageByBehavior(ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * p, ParticleCustomBehavior * b)
{
	p->lifeAffectRate  = b->lifeAffectRate;
	p->displacementPos = b->displacementPos;
	p->colorPath       = b->colorPath;
	p->speedPath       = b->speedPath;
	p->endColor		   = b->endColor;
	p->middleColor     = b->middleColor;
	p->startColor      = b->startColor;
	p->endSpeed        = b->endSpeed;
	p->middleSpeed     = b->middleSpeed;
	p->startSpeed	   = b->startSpeed;
	p->endScale		   = b->endScale;
	p->startScale	   = b->startScale;
	p->spinMax		   = b->spinMax;
	p->spinMin		   = b->spinMin;
	p->distorsion      = b->distorsion;
	p->gravity		   = b->gravity;
	p->randAngle.x     = b->randAngleX;
	p->randAngle.y     = b->randAngleY;
	p->randAngle.z     = b->randAngleZ;
	p->intensityDir    = b->intensityDir;
}