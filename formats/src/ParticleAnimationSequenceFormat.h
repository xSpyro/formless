
#ifndef PARTICLE_ANIMATION_SEQUENCE_FORMAT
#define PARTICLE_ANIMATION_SEQUENCE_FORMAT

#include "SharedFormatInterface.h"

class AnimationSubsystem;

class ParticleAnimationSequenceFormat : public SharedFormatInterface
{
public:

	ParticleAnimationSequenceFormat(AnimationSubsystem * as);

	bool importFromFile(const File & file, const std::string& fileName);
	bool exportToFile(File & file);

private:

	AnimationSubsystem * as;
};

#endif