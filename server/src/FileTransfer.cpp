
#include "FileTransfer.h"

#include "CommonPackets.h"

#include "Server.h"

FileTransfer::FileTransfer(boost::asio::io_service & service)
	: ServerInterface(service, 1032), log(std::cout)
{
	disableConnectResponse();
}

FileTransfer::~FileTransfer()
{
}

void FileTransfer::setServer(Server * server)
{
	this->server = server;
}

void FileTransfer::onPacket(RawPacket & packet)
{
	if (packet.header.id != FILE_TRANSFER_PACKET)
		return;

	// use server to get something done
	// cmd is in the data part of the packet

	log("FileTransfer started from " + getResponseIp());

	unsigned int size = 1000000;

	size = server->getLevelData()->size;

	char * data = server->getLevelData()->data;
	//data = new char[size];

	//unsigned int asd = 12345;

	respond(&size, 4);
	respond(data, size);

	//delete[] data;
}