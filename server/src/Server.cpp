// #define  _SERVERRENDER
#include "RequestInterface.h"

#include "Server.h"

#include "LogicHandler.h"
#include "GameHandler.h"

#include "PhysicsSubsystem.h"
#include "RenderSubsystem.h"
#include "ControllerSubsystem.h"
#include "TimerSubsystem.h"
#include "SkillSubsystem.h"
#include "CoreSubsystem.h"
#include "ScriptSubsystem.h"
#include "NetworkSubsystemServer.h"
#include "StatsSubsystem.h"
#include "AISubsystem.h"


#ifdef _SERVERRENDER
#include "AnimationSubsystem.h"
#include "ParticleAnimationSequenceFormat.h"
#include "EffectSubsystem.h"
#include "EffectFormat.h"
#endif

#include "LevelFormat.h"

#include "EventHandler.h"
#include "EventEnum.h"

#include "GameConnector.h"

#include "ArenaScene.h"

#include "CommonPackets.h"

#include "MemLeaks.h"

BOOL WINAPI Routine(DWORD t)
{
	if (t == CTRL_CLOSE_EVENT)
	{
		MasterSendAsync request;

		DropServerPacket packet;
		request(packet, sizeof(packet));
	}

	return true;
}

Server::Server(boost::asio::io_service & service)
	: ServerInterface(service, 1012), log(std::cout), fileTransfer(service)
{
	SetConsoleCtrlHandler(&Routine, true);

	pulse = 50;
	registered = false;

	// set request async data
	RequestInterface::setRequestAsyncData(service, &Server::onPacketStatic);
}

Server::~Server()
{
	if(registered)
		Routine(CTRL_CLOSE_EVENT);
}

bool Server::startup( std::string name )
{

	ParticleSystem * psPointer = NULL;

#ifdef _SERVERRENDER
	init(1280, 720, "Server - v0.9");
	open();

	renderer = New Renderer();

	renderer->join(this);

	psPointer = &particleSystem;
#endif

	me = this;

	fileTransfer.setServer(this);

	FileSystem::setBaseDirectory("../resources/");

	LogicHandler* logic	= New LogicHandler(this);
	game				= New GameHandler(logic);

	GameConnector::setGame(game);
	game->setFormatStore(&formatStore);

	EventLua::Object g = logic->getScript()->getGlobals();
	g.bind("include", &loadScript);



	PhysicsSubsystem*		phys	= New PhysicsSubsystem(game);
	
	ControllerSubsystem*	cont	= New ControllerSubsystem(game);
	ScriptSubsystem *		script	= New ScriptSubsystem();
	TimerSubsystem*			timer	= New TimerSubsystem();
	SkillSubsystem*			skill	= New SkillSubsystem(game);
	CoreSubsystem*			core	= New CoreSubsystem(game);
							network	= New NetworkSubsystemServer(game, this);
	StatsSubsystem*			stats	= New StatsSubsystem(game);
	AISubsystem*			ai		= New AISubsystem(game);

#ifdef _SERVERRENDER
	RenderSubsystem*		rend	= New RenderSubsystem();
	AnimationSubsystem * animation  = New AnimationSubsystem(game, 1000, &formatStore);
	EffectSubsystem *	effect		= New EffectSubsystem(game);

	logic->registerSubsystem("Animation", animation);
	logic->registerSubsystem("Render", rend);
	logic->registerSubsystem("Effect", effect);

	// register extensions
	formatStore.registerFormat("pas", New ParticleAnimationSequenceFormat(animation));
	
	// effects
	particleSystem.init(renderer);
	formatStore.registerFormat("paf", New EffectFormat(NULL, NULL, &particleSystem));

	// temporary
	formatStore.importFromFile("effects/explosion_v2.paf");


	// fonts
	font.createDevices(1280, 720);
	font.createFont("Centuary:13");

	HRESULT hr = S_OK;
	IDXGIResource * otherRes(NULL);
	hr = font.getSurface()->QueryInterface(__uuidof(IDXGIResource), (void**)&otherRes);

	HANDLE sharedHandle;
	hr = otherRes->GetSharedHandle(&sharedHandle);

	ID3D11Resource * res11 = NULL;

	renderer->getDevice()->OpenSharedResource(sharedHandle, __uuidof(ID3D11Resource), (void**)(&res11)); 

	mutex = NULL;
	if (SUCCEEDED(res11->QueryInterface(__uuidof(IDXGIKeyedMutex), (void**)&mutex)))
	{
		font.setSharedMutex(mutex);
	}
	
	direct2dtexture = New Texture();
	direct2dtexture->bind(renderer, res11);

#endif

	formatStore.registerFormat("flf", New LevelFormat(game));
	
	logic->registerSubsystem("Physics", phys);
	logic->registerSubsystem("Controller", cont);
	logic->registerSubsystem("Script", script);
	logic->registerSubsystem("Timers", timer);
	logic->registerSubsystem("Skill", skill);
	logic->registerSubsystem("Core", core);
	logic->registerSubsystem("Network", network);
	logic->registerSubsystem("Stats", stats);
	logic->registerSubsystem("AI", ai);

	game->registerObjectTemplate("Node");
	game->registerObjectTemplate("Player");
	game->registerObjectTemplate("AIPlayer");
	game->registerSkillTemplate("Projectile");
	game->registerSkillTemplate("Explosion");
	game->registerSkillTemplate("Ball");
	game->registerSkillTemplate("SpeedBuff");
	game->registerSkillTemplate("DefaultProjectile");
	game->registerSkillTemplate("FireBolt");
	game->registerSkillTemplate("FireBoltHit");
	game->registerSkillTemplate("IceBolt");
	game->registerSkillTemplate("IceBoltHit");
	game->registerSkillTemplate("LightningBolt");
	game->registerSkillTemplate("LightningBoltHit");
	game->registerSkillTemplate("SoilBolt");
	game->registerSkillTemplate("SoilBoltHit");
	game->registerSkillTemplate("Splitter");
	game->registerSkillTemplate("SplitterProjectile");
	game->registerSkillTemplate("DefaultProjectileHit");
	game->registerSkillTemplate("ProjectileHit");
	game->registerSkillTemplate("Charge");
	game->registerSkillTemplate("LinkProjectile");
	game->registerSkillTemplate("DefaultLink");
	game->registerSkillTemplate("Tripwire");
	game->registerSkillTemplate("TripwireLink");
	game->registerSkillTemplate("Teleport");
	game->registerSkillTemplate("Barrage");
	game->registerSkillTemplate("Lasso");
	game->registerSkillTemplate("Jump");
	game->registerSkillTemplate("Razorwire");
	game->registerSkillTemplate("RazorwireLink");
	game->registerSkillTemplate("Wall");
	game->registerSkillTemplate("WallLink");
	game->registerSkillTemplate("Shield");
	game->registerSkillTemplate("Spear");
	game->registerSkillTemplate("LifeStream");
	game->registerSkillTemplate("Hammer");
	game->registerSkillTemplate("HammerHit");
	game->registerSkillTemplate("Shout");

	game->registerSkillTemplate("BarrageProjectile");
	game->registerSkillTemplate("BarrageProjectileHit");

	game->registerObjectTemplate("Ruin");
	game->registerObjectTemplate("Torus");
	game->registerObjectTemplate("Spawn");

#ifdef _SERVERRENDER
	logic->createScene("Arena", New ArenaScene(logic, renderer, (Window*)this, game, this, psPointer, &font));
#else
	logic->createScene("Arena", New ArenaScene(logic, renderer, (Window*)this, game, this));
#endif
	logic->activateScene("Arena");


	// everything seems to have worked
	// send register message

	EventLua::Object config;
	bool saveConfig = false;
	File file; // TODO: Now in gameConnector?
	if( FileSystem::get( "server.cfg", &file ) )
	{
		if( !game->getScript()->execute( file.data, file.size ) )
		{
			std::cout << game->getScript()->getLastError() << std::endl;
		}
		else
		{
			config = game->getScript()->get( "server" );
			if( name.empty() )
			{
				name = config.get("name").queryString("Formless server");
			}
			else
			{
				config.set( "name", name.c_str() );
				saveConfig = true;
			}
		}
	}
	else
	{
		if( name.empty() )
		{
			name = "Formless Server";
		}

		config = game->getScript()->newTable( "server" );
		config.set( "name", name.c_str() );
		saveConfig = true;
	}

	if( saveConfig )
	{
		File oFile;
		std::string buffer = config.print();
		oFile.size = buffer.size();
		oFile.data = new char[oFile.size];
		buffer._Copy_s( oFile.data, oFile.size, buffer.size() );
		//buffer.copy( oFile.data, oFile.size );

		FileSystem::write( "server.cfg", &oFile );
	}

	log("Server name is: ", name);

	MasterRequest request;
	RegisterServerPacket packet;
	packet.setName( name );
	RawPacket result;
	if (request(packet, sizeof(packet), result))
	{
		ServerAddedPacket* added = reinterpret_cast< ServerAddedPacket* > (result.data);
		switch( added->error )
		{
		case 0:
			registered = true;
			log("Server Registered @ Master");
			break;
		case 1:
			registered = true;
			log("Server is feeling healthy again");
			break;
		default:
			log.warn("Server registration FAILED (Error Code: ", added->error, ")");
			break;
		}
	}

	return registered;
}

bool Server::shutdown()
{
	return true;
}

void Server::update()
{
	game->update();
}

void Server::run()
{
	Timer timeframe;
	Timer keepAlive;
	Timer statTimer;

#ifdef _SERVERRENDER
	while(isOpen())
	{
		process();
		poll();

		if (timeframe.accumulate(1 / 60.0f))
		{
			update();
		}

		if( keepAlive.accumulate( pulse ) )
		{
			MasterSendAsync ping;
			PingPacket packet;
			ping(packet, sizeof(packet));
			//log("I'm not dead!");
		}

		if ( statTimer.accumulate(1) )
		{
			//std::cout << "bytes in " << pollReceivedBytes() << std::endl;
			//std::cout << "bytes out " << pollSentBytes() << std::endl;
		}

		renderer->clear(NULL);

		font.begin();
		game->render();
		renderer->render();
		font.end();

		renderer->present();

	}
#else

	while (true)
	{
		poll();

		if (timeframe.accumulate(1 / 60.0f))
		{
			update();
		}

		if( keepAlive.accumulate( pulse ) )
		{
			MasterSendAsync ping;
			PingPacket packet;
			ping(packet, sizeof(packet));
			//log("I'm not dead!");
		}

		
		if ( statTimer.accumulate(1) )
		{
			//std::cout << "bytes in " << pollReceivedBytes() << std::endl;
			//std::cout << "bytes out " << pollSentBytes() << std::endl;
		}
	}
#endif
		
}

void Server::onSceneChange()
{
#ifdef _SERVERRENDER
	renderer->insertOverlay(5000, direct2dtexture, NULL);
#endif
}

void Server::onInput(int sym, int state)
{
	game->onInput(sym, state);
#ifdef _SERVERRENDER
	if (sym == VK_F5 && state == 1)
	{
		renderer->shader()->reload();
	}
#endif
}

void Server::onPacket(RawPacket & raw)
{
	switch( raw.header.id )
	{
	case LEVEL_DATA_REQUEST_PACKET:

		processLevelDataRequestPacket( *reinterpret_cast < LevelDataRequestPacket* > (raw.data) );
		break;

	case FILE_TRANSFER_PACKET:

		fileTransfer.onPacket( raw );
		break;

	default:

		network->onPacket(raw);
		break;
	}
}

void Server::onConnect(unsigned int uid)
{
	network->onConnect(uid);
}

void Server::onDisconnect(unsigned int uid)
{
	network->onDisconnect(uid);
}

void Server::onCreateObject(Object * object)
{
	network->onCreateObject(object);
}

void Server::onDestroyObject(Object * object)
{
	network->onDestroyObject(object);
}

bool Server::loadLevel( std::string name )
{
	levelName = name;
	//if( !formatStore.importFromFile( name ) )
	//	return false;
	formatStore.importFromFile( levelName );
	return FileSystem::get( levelName, &levelData );
}

const File* Server::getLevelData()
{
	return &levelData;
}

Server * Server::me = NULL;
void Server::loadScript( const char* name )
{
	File file; // TODO: Now in gameConnector?
	FileSystem::get( name, &file );
	if( !me->game->getScript()->execute( file.data, file.size ) )
	{
		std::cout << me->game->getScript()->getLastError() << std::endl;
	}
}

void Server::onPacketStatic(RawPacket & raw)
{
	//switch( raw.header.id )
	//{
	//default:

		me->network->onPacket(raw);
	//	break;
	//}
}

void Server::processLevelDataRequestPacket( LevelDataRequestPacket & packet )
{
	LevelDataResponsePacket response;
	response.level_size = levelData.size;
	response.setName( levelName );

	respond( response );
}
