
#include "Server.h"
#include "ArenaScene.h"



#include "Common.h"
#include "Object.h"
#include "LogicHandler.h"
#include "GameConnector.h"
#include "ScriptEnvironment.h"
#include "PhysicsComponent.h"
#include "CoreComponent.h"
#include "ControllerComponent.h"

#include "GameHandler.h"
#include "PhysicsSubsystem.h"



#ifdef _SERVERRENDER
#include "RenderComponent.h"
#include "AnimationComponent.h"
#include "EffectComponent.h"
#endif

#include "NetworkSubsystemServer.h"
#include "NetworkComponent.h"

#include "GameHandler.h"
#include "CommonPackets.h"

#include "MemLeaks.h"


ArenaScene::ArenaScene()
	: Scene()
{
	
}

ArenaScene::ArenaScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game, Server * server, ParticleSystem * ps, Font2D * font)
	: Scene(handler, rend, wndw), server(server), game(game)
{
	
#ifdef _SERVERRENDER

	renderModule.setParticleSystem(ps);
	
	renderModule.init(rend, RM_ALL);
	//renderModule.getBlendTexture().load("textures/texture_blend.png");
	renderModule.build();

	renderModule.setPlayerPosition(0);

	camera = renderModule.getCamera();

	camera->setPosition(Vector3(0, 540, -480));
	camera->setOrientation(Vector3(0, -1.8f, 1));
	camera->setClipRange(200, 2000);
	camera->setProjection3D(75, 16 / 9.0f);

	cameraDest = camera->getPosition();

	stage2D = New Stage2D(this, font);
#endif

	// will only ever be server network subsystem
	network = reinterpret_cast < NetworkSubsystemServer* > (game->getSubsystem("Network"));
}

ArenaScene::~ArenaScene()
{
	Delete stage2D;
}

void ArenaScene::update()
{

#ifdef _SERVERRENDER

	std::vector < Object* > * objects;
	objects = game->getCoreObjects();

	for (std::vector < Object* >::iterator i = objects->begin(); i != objects->end(); ++i)
	{
		CoreComponent * core = (*i)->findComponent<CoreComponent>();

		{
			SceneLightBuffer::Entity entity;

			PhysicsComponent * com = (*i)->findComponent<PhysicsComponent>();

			float life = core->life.getCurrent() / core->life.getMax();

			entity.position = com->position + Vector3(0, 5, 0);
			entity.radius = 1 / 0.5f;

			if (core->team == 1)
			{
				entity.color = Vector3(0.5, -1, -1);
			}
			else if (core->team == 2)
			{
				entity.color = Vector3(-1, -1, 0.5);
			}
			renderModule.addLight(entity);
		}

		if (core->isAlive())
		{
			ScenePlayerBuffer::Entity entity;

			PhysicsComponent * com = (*i)->findComponent<PhysicsComponent>();



			entity.position = com->position;
			entity.position.y = 0;
			if (core->team == 1)
			{
				entity.team = 0;
			}
			else if (core->team == 2)
			{
				entity.team = 1;
			}

			renderModule.addPlayer(entity);
		}
	}


	std::vector < Object* > * skills;
	skills = game->getSkillObjects();

	for (std::vector < Object* >::iterator i = skills->begin(); i != skills->end(); ++i)
	{
		SceneLightBuffer::Entity entity;

		PhysicsComponent * com = (*i)->findComponent<PhysicsComponent>();
		AnimationComponent * an = (*i)->findComponent<AnimationComponent>();

		if( com )
		{
			entity.position = com->position;
			entity.position.y = 0; // first shape
			entity.radius = 1.5f;

			if( an )
				entity.color = an->getLightColor();
			else
				entity.color = Vector3(1, 1, 0);

			renderModule.addLight(entity);
		}
	}


	// fix camera
	static EventLua::State state;
	if (state.execute("../ServerCamera.lua"))
	{
		float y = state.get("y").queryFloat();
		float z = state.get("z").queryFloat();
		float down = state.get("down").queryFloat();
		float znear = state.get("znear").queryFloat();
		float zfar = state.get("zfar").queryFloat();
		float fov = state.get("fov").queryFloat();

		camera->setPosition(Vector3(0, y, z));
		camera->setOrientation(Vector3(0, -down, 1));
		camera->setClipRange(znear, zfar);
		camera->setProjection3D(fov, 16 / 9.0f);
		camera->update();
	}

	renderModule.update();
#endif
}

void ArenaScene::render()
{
#ifdef _SERVERRENDER
	renderModule.preFrame();
#endif
}

void ArenaScene::onEnter()
{
#ifdef _SERVERRENDER
	renderModule.bind();
	renderer->insertStage(1000, stage2D);
	game->setHeightMap(&renderModule.getTerrain());
	game->setBlendMap(&renderModule.getBlendTexture().begin());
#else
	game->setHeightMap(&heightmap);
#endif

	//game->createObjectExt("Ruin", Vector3(0, 0, 0), 0);
	//game->createObjectExt("Torus", Vector3(0, 0, 0), 0);
	//game->createObjectExt("Spawn", Vector3(0, 0, 0), 0);

	
	
	//server->formatStore.importFromFile("Arena41.flf");

	File file;
	FileSystem::get("server.cfg", &file);

	EventLua::State state;
	if (state.execute(file.data, file.size))
	{
		server->loadLevel(state.get("server")["map"].queryString("Arena46.flf"));
		network->setGameTime(state.get("server")["gametime"].queryFloat(4.0f));
	}

#ifdef _SERVERRENDER
	renderModule.addCircleOfLinks();

	for (unsigned int i = 0; i < renderModule.getLinks(); ++i)
	{
	}

	renderModule.updateTerrain();
#endif
}

void ArenaScene::onLeave()
{
#ifdef _SERVERRENDER
	renderer->clearLayers();
#endif
}

void ArenaScene::onLoad()
{
#ifdef _SERVERRENDER
	renderModule.updateTerrain();
#endif
}

bool ArenaScene::onInput(int sym, int state)
{
#ifdef _SERVERRENDER
	if (sym == VK_F4 && state == 1)
	{
		renderer->switchWindowFullScreenMode();
	}
#endif

	return false;
}

bool ArenaScene::onCreateObject(Object* object)
{

#ifdef _SERVERRENDER
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();

	if (rendcomp)
	{
		renderModule.addObject(rendcomp);
	}	

	AnimationComponent* animcomp = object->findComponent<AnimationComponent>();

	if (animcomp)
	{
		renderModule.addLinkedParticle(animcomp);
	}

	/*
	EffectComponent* effcomp = object->findComponent<EffectComponent>();

	if (effcomp)
	{
		PhysicsComponent * com = object->getCreator()->findComponent<PhysicsComponent>();
		if (com)
		{
			Vector3 normal = Vector3(0, 1, 0); // riktning p� terr�ngen/surface
			renderModule.spawnEffect(effcomp->getName(), com->position, normal);
		}
	}
	*/
#endif
	server->onCreateObject(object);

	return true;
}

bool ArenaScene::onDestroyObject(Object* object)
{
	server->onDestroyObject(object);

	return true;
}

void ArenaScene::onRender2D(Font2D * font)
{
#ifdef _SERVERRENDER
	std::vector < Object* > * objects;
	objects = game->getCoreObjects();

	for (std::vector < Object* >::iterator i = objects->begin(); i != objects->end(); ++i)
	{
		CoreComponent * core = (*i)->findComponent<CoreComponent>();
		PhysicsComponent * physics = (*i)->findComponent<PhysicsComponent>();
		NetworkComponent * net = (*i)->findComponent<NetworkComponent>();
		
		// project position
		Vector2 position = camera->project(physics->position);

		const std::string & name = network->getPlayerName(net->getID());

		std::stringstream format;
		format << "#" << name << std::endl;
		format << "life: " << core->life.getCurrent() << std::endl;
		format << "mana: " << core->mana.getCurrent() << std::endl;
		format << "speed: " << core->speed.getCurrent();

		
		position.x = (position.x + 1) * 0.5f;
		position.y = (position.y + 1) * 0.5f;
		position.y = 1 - position.y;

		position.x *= 1280.0f;
		position.y *= 720.0f;

		font->draw(format.str().c_str(), position.x - 30, position.y - 120, 9000, 9000, D3DXCOLOR(0, 0, 0, 1));
	}

#endif
}
