#include <iostream>
#include <string>
#include <map>
#include "Server.h"

int main( int argc, char** argv )
{
	std::string name;

	if( argc > 1 )
	{
		name = argv[1];
	}

	boost::asio::io_service service;

	Server server(service);
	if (server.startup(name))
	{
		server.run();
		server.shutdown();
	}
	
	return 0;
}