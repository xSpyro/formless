
#ifndef ARENASCENE_H
#define ARENASCENE_H

#include "Scene.h"

#ifdef _SERVERRENDER
#include "Framework.h"
#include "RenderModule.h"
#else
class ParticleSystem;
class Font2D;
#endif
#include "../../game/src/Terrain.h"

class Object;
class PhysicsComponent;
class ControllerComponent;
class GameHandler;
class Server;

class NetworkSubsystemServer;

class ArenaScene : public Scene
{

public:

	ArenaScene();
	ArenaScene(LogicHandler* handler, Renderer* rend, Window* wndw, GameHandler * game, Server * server, ParticleSystem * ps = NULL, Font2D * font = NULL);
	virtual ~ArenaScene();

	void update();
	void render();

	void onEnter();
	void onLeave();
	void onLoad();

	bool onInput(int sym, int state);
	bool onMouseMovement(int x, int y) { return false; }

	bool onCreateObject(Object* object);	// Do not call this manually!
	bool onDestroyObject(Object* object);	// Do not call this manually!

	void onRender2D(Font2D * font);

	// text rendering
#ifdef _SERVERRENDER
	struct Stage2D : public Stage
	{
		ArenaScene * me;
		Font2D * font;
		
		Stage2D(ArenaScene * me, Font2D * font) : me(me), font(font)
		{
		}

		void apply()
		{
			me->onRender2D(font);
		}
	};
#else
	struct Stage2D
	{
		Stage2D(void * empty)
		{
		}
	};
#endif

private:

#ifdef _SERVERRENDER

	Camera*		camera;
	Vector3		cameraDest;

	RenderModule renderModule;
#else
	Terrain heightmap;
#endif

	NetworkSubsystemServer * network;

	GameHandler * game;
	Server * server;

	Stage2D * stage2D;

};

#endif