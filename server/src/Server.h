
#ifndef SERVER_H
#define SERVER_H

#include "ServerInterface.h"

class GameHandler;

#include <string>

#include "Program.h"
#include "SharedFormatStore.h"
#include "LuaFileManager.h"
#include "Logger.h"

#include "NetworkSubsystem.h"
#include "FileTransfer.h"


#include <boost/asio.hpp>

//#define _SERVERRENDER

#ifdef _SERVERRENDER
#include "Window.h"
#include "Font2D.h"
#include "Texture.h"
#include "Renderer.h"
#include "Timer.h"

#include "Graphics.h"




class Object;

class Server : public Program, public Window, public ServerInterface
#else

class Renderer;
class Window;

class Server : public Program, public ServerInterface
#endif

{
public:
	Server(boost::asio::io_service & service);
	~Server();

	bool startup( std::string name = "" );
	bool shutdown();

	void update();
	void run();

	void onSceneChange();

	void onInput(int sym, int state);

	void onPacket(RawPacket & packet);
	void onConnect(unsigned int uid);
	void onDisconnect(unsigned int uid);


	void onCreateObject(Object * object);
	void onDestroyObject(Object * object);

	bool loadLevel( std::string name );
	const File* getLevelData();


	SharedFormatStore formatStore; // TODO: Private later

private:

	static Server *	me;
	static void loadScript( const char* name );

private:

	static void onPacketStatic(RawPacket & raw);

	void processLevelDataRequestPacket(LevelDataRequestPacket & packet);

private:

	FileTransfer fileTransfer;
	File levelData;
	std::string levelName;

	GameHandler*	game;
	NetworkSubsystem * network;
	float pulse;
	bool registered;
	Logger log;

	// rendering on the server is only for debug

	Renderer * renderer;

#ifdef _SERVERRENDER
	Font2D font;
	Texture * direct2dtexture;
	IDXGIKeyedMutex * mutex;
	ParticleSystem particleSystem;
#endif

};

#endif