
#ifndef FILE_TRANSFER_H
#define FILE_TRANSFER_H

#include "ServerInterface.h"

#include "Logger.h"

class Server;

class FileTransfer : public ServerInterface
{

public:

	FileTransfer(boost::asio::io_service & service);
	~FileTransfer();

	void setServer(Server * server);

	void onPacket(RawPacket & packet);

private:

	Server * server;
	Logger log;
};

#endif