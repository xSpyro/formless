#include "Font2D.h"

#include <sstream>
#include <iostream>

#define ReleaseCOM(x) { if(x){ x->Release();x = 0; } }

Font2D::Font2D()
{

	device	   = NULL;
	off_screen = NULL;
	textureRV  = NULL;

	d2_factory = NULL;
	dw_factory = NULL;

	render_target = NULL;
	clear   = NULL;

	redraw = false;

	use_mutexes = false;

}
Font2D::~Font2D()
{

}
bool Font2D::createDevices(int w, int h)
{
	HRESULT hr = S_OK;
	
	width  = w;
	height = h;

	/**
	 * Device Independent.
	 */
	hr = D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &d2_factory);
	if( FAILED(hr) )
		return false;

    hr = DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(dw_factory), reinterpret_cast<IUnknown **>(&dw_factory ) );
	if( FAILED(hr) )
		return false;


	IDXGIFactory * dxgi10_1 = NULL;
	hr = CreateDXGIFactory1(__uuidof(IDXGIFactory1), (void**)(&dxgi10_1)); 
	if (FAILED(hr))
	{
		std::cout << "Font2D Failed DXGI Factory" << std::endl;
	}

	IDXGIAdapter * adapter = NULL;
	dxgi10_1->EnumAdapters(0, &adapter);




	/**
	 * Create Device10.1 and use QueryInterface for Device10
	 */
	ID3D10Device1 * pDevice = NULL;
    UINT nDeviceFlags = D3D10_CREATE_DEVICE_BGRA_SUPPORT;

    hr = CreateD3DDevice(
        adapter,
        D3D10_DRIVER_TYPE_HARDWARE,
        nDeviceFlags,
        &pDevice
        );
	pDevice->QueryInterface(&device);


    device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    /**
	 * Texture used as render target
	 */
    D3D10_TEXTURE2D_DESC texDesc;
	ZeroMemory(&texDesc, sizeof(texDesc));

    texDesc.ArraySize = 1;
    texDesc.BindFlags = D3D10_BIND_RENDER_TARGET | D3D10_BIND_SHADER_RESOURCE;
    texDesc.CPUAccessFlags = 0;
    texDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	//texDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    texDesc.Height = h;                          // HARDCODED RES //
    texDesc.Width = w;
    texDesc.MipLevels = 1;
    //texDesc.MiscFlags = D3D10_RESOURCE_MISC_SHARED;
	texDesc.MiscFlags = D3D10_RESOURCE_MISC_SHARED_KEYEDMUTEX;
    texDesc.SampleDesc.Count = 1;
    texDesc.SampleDesc.Quality = 0;
    texDesc.Usage = D3D10_USAGE_DEFAULT;

	off_screen = NULL;

    hr = device->CreateTexture2D(&texDesc, NULL, &off_screen);
	if (FAILED(hr))
	{
		std::cout << "Font2D Failed CreateTexture2D " << std::endl;

		// okay skip the mutex build go all on flickering texts!!

		texDesc.MiscFlags = D3D10_RESOURCE_MISC_SHARED;
		hr = device->CreateTexture2D(&texDesc, NULL, &off_screen);

		if (FAILED(hr))
		{
			std::cout << "Font2D Failed CreateTexture2D without mutexes!!" << std::endl;
		}
	}
	else
	{
		std::cout << "Font2D Using Mutexes" << std::endl;
		use_mutexes = true;
	}

	hr = device->CreateShaderResourceView(off_screen, NULL, &textureRV);
	if (FAILED(hr))
	{
		std::cout << "Font2D Failed CreateShaderResourceView " << std::endl;
	}

	if (SUCCEEDED(off_screen->QueryInterface(__uuidof(IDXGIKeyedMutex), (void**)&mutex)))
	{
		std::cout << "Font2D QueryMutex Success" << std::endl;
	}

	FLOAT dpiX = 96;
	FLOAT dpiY = 96;
	//d2_factory->GetDesktopDpi(&dpiX, &dpiY);
	off_screen->QueryInterface(&dxgi_surface);
    
    D2D1_RENDER_TARGET_PROPERTIES props =
        D2D1::RenderTargetProperties(
            D2D1_RENDER_TARGET_TYPE_DEFAULT,
            D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_PREMULTIPLIED),
            dpiX,
            dpiY
            );
	

    hr = d2_factory->CreateDxgiSurfaceRenderTarget(
        dxgi_surface,
        &props,
        &render_target
        );

	D2D1_COLOR_F color;
	color.a = 0.0f;
	color.r = 0.0f;
	color.g = 0.0f;
	color.b = 0.0f;
	hr = render_target->CreateSolidColorBrush(
		color,
		&clear );

	if( FAILED(hr) )
	{
		return false;
	}

	render_target->SetAntialiasMode(D2D1_ANTIALIAS_MODE_ALIASED);


	return true;

}

bool Font2D::createFont(std::string font)
{


	map_text::iterator lb = text_formats.find(font); 
 
	if(lb != text_formats.end()) 
	{ 
		current_text = lb->second;

		// key already exists 
		// update lb->second if you care to 
		return true;
	} 
	else 
	{ 		
		HRESULT hr = S_OK;
		//std::string str("Hello, can you find Ben?");
		std::string::size_type position = font.find(":");

		std::string font_cleared  = font.substr(0, position); 
		std::string font_size     = font.substr(position+1);

		std::istringstream stream(font_size);
		int size = -1;
		stream >> size;
  

		const size_t origsize = strlen(font_cleared.c_str()) + 1;
		static const size_t newsize = 10000;
		size_t convertedChars = 0;
		static wchar_t wcstring[newsize];
		///wchar_t * wcstring
		UINT size_clear = font_cleared.length();
		mbstowcs_s(&convertedChars, wcstring, origsize, font_cleared.c_str(), _TRUNCATE);


		IDWriteTextFormat * text_format;

		IDWriteFontCollection * font_collection = NULL;
		IDWriteFontFile *       font_files = NULL;
		
		// H�mta alla fonts som �r installerade p� systemet, R�R EJ
		dw_factory->GetSystemFontCollection(&font_collection);
		
		DWRITE_FONT_WEIGHT weight;

		if(font_cleared == "Trebuchet MS")
			weight = DWRITE_FONT_WEIGHT_BOLD;
		else
			weight = DWRITE_FONT_WEIGHT_NORMAL;

		hr = dw_factory->CreateTextFormat(wcstring, font_collection, DWRITE_FONT_WEIGHT_BOLD,
										  DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 
										  (FLOAT)size, L"", &text_format );

		

		// Ladda in fr�n fil, R�R EJ
		/*std::string _temp_font = "../resources/fonts/" + font_cleared + ".ttf";
		const size_t origsize2 = strlen(_temp_font.c_str()) + 1;
		size_t convertedChars2 = 0;
		UINT size_clear2 = font_cleared.length();
		mbstowcs_s(&convertedChars2, wcstring, origsize2, _temp_font.c_str(), _TRUNCATE);

		hr = dw_factory->CreateFontFileReference(
					wcstring,
					NULL,
					&font_files);*/

		if( FAILED(hr) )
		{



			//std::string _temp_font = "../resources/fonts/" + font_cleared;


			//const size_t origsize = strlen(_temp_font.c_str()) + 1;
			//static const size_t newsize = 10000;
			//size_t convertedChars = 0;
			//static wchar_t wcstring[newsize];
			/////wchar_t * wcstring
			//UINT size_clear = font_cleared.length();
			//mbstowcs_s(&convertedChars, wcstring, origsize, _temp_font.c_str(), _TRUNCATE);


			//hr = dw_factory->CreateTextFormat(wcstring, NULL, DWRITE_FONT_WEIGHT_NORMAL,
			//							  DWRITE_FONT_STYLE_NORMAL, DWRITE_FONT_STRETCH_NORMAL, 
			//							  (FLOAT)size, L"", &text_format );

			//if( FAILED(hr) )
			//	return false;
			return false;
		}

		text_format->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
		text_format->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);

		text_formats.insert(lb, map_text::value_type(font, text_format)); 

		current_text = text_format;
	}

	return true;
}
bool Font2D::setFont(std::string font)
{
	map_text::iterator lb = text_formats.lower_bound(font); 
 
	if(lb != text_formats.end() && !(text_formats.key_comp()(font, lb->first))) 
	{
		current_text = lb->second;
		// key already exists 
		// update lb->second if you care to 
		return true;
	} 
	else 
	{ 
		return false;
	}
}

void Font2D::setTextAlignment(int id)
{
	if(id == 0)
	{
		current_text->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
	}
	else if(id == 1)
	{
		current_text->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
	}
	else if(id == 2)
	{
		current_text->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_TRAILING);
	}
}

int Font2D::getTextAlignment()
{
	return current_text->GetTextAlignment();
}

void Font2D::setParagraphAlignment(int id)
{
	if(id == 0)
	{
		current_text->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
	}
	else if(id == 1)
	{
		current_text->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_FAR);
	}
	else if(id == 2)
	{
		current_text->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
	}
}

int Font2D::getParagraphAlignment()
{
	return current_text->GetParagraphAlignment();
}

void Font2D::draw(std::string msg, float x, float y, float w, float h, D3DXCOLOR col)
{
	D2D1_COLOR_F color;
	color.r = col.r;
	color.g = col.g;
	color.b = col.b;
	color.a = col.a;

	ID2D1SolidColorBrush * text;
	render_target->CreateSolidColorBrush(
		color,
		&text );

	const size_t origsize = strlen(msg.c_str()) + 1;
    static const size_t newsize = 10000;
    size_t convertedChars = 0;
    static wchar_t wcstring[newsize];
	UINT size = msg.length();
	mbstowcs_s(&convertedChars, wcstring, origsize, msg.c_str(), _TRUNCATE);

	D2D1_SIZE_F targetSize = render_target->GetSize();
	//render_target->BeginDraw();
	render_target->SetTransform(D2D1::Matrix3x2F::Identity());


	render_target->DrawText(
		  wcstring,
		  size,
		  current_text,
		  D2D1::RectF(x, y, x + w, y + h),
		  text,
		  D2D1_DRAW_TEXT_OPTIONS_CLIP
		  );

	text->Release();

	//render_target->EndDraw();

	
}



void Font2D::begin()
{
	//mutex_shared->ReleaseSync(0);
	if (use_mutexes)
		mutex->AcquireSync(0, 2);

	render_target->BeginDraw();

	render_target->Clear();

	if (use_mutexes)
		mutex->ReleaseSync(1);

	//mutex_shared->AcquireSync(1, 20);

	if (use_mutexes)
		mutex_shared->AcquireSync(1, 2);
	
}
void Font2D::end()
{
	if (use_mutexes)
		mutex_shared->ReleaseSync(0);
    //render_target->EndDraw();

	//mutex->AcquireSync(0, INFINITE);
	
	if (use_mutexes)
	{
		mutex->ReleaseSync(0);
		mutex->AcquireSync(0, 2);
	}

	render_target->EndDraw();


	device->Flush();

	

	//device->

	//render_target->BeginDraw();

	
	if (use_mutexes)
		mutex->ReleaseSync(1);

	//mutex_shared->AcquireSync(1, 20);
	
}
void Font2D::recreateDevice()
{
	ReleaseCOM(device);
	HRESULT hr = S_OK;
	ID3D10Device1 * pDevice = NULL;
    UINT nDeviceFlags = D3D10_CREATE_DEVICE_BGRA_SUPPORT;

    hr = CreateD3DDevice(
        NULL,
        D3D10_DRIVER_TYPE_HARDWARE,
        nDeviceFlags,
        &pDevice
        );
	pDevice->QueryInterface(&device);


    device->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}
void Font2D::shutDown()
{
	ReleaseCOM(device);
	ReleaseCOM(off_screen);
	ReleaseCOM(textureRV);
	ReleaseCOM(d2_factory);
	ReleaseCOM(dw_factory);
	ReleaseCOM(render_target);
	ReleaseCOM(clear);


	 for ( map_text::const_iterator iter = text_formats.begin();
		  iter != text_formats.end(); ++iter )
	 {
		// ReleaseCOM(iter->second);
		  iter->second->Release();
		  //cout << iter->first << '\t' << iter->second << '\n';
	 }
}
void Font2D::getSize(std::string msg, float * w, float * h, float wMax, float hMax)
{
	const size_t origsize = strlen(msg.c_str()) + 1;
    static const size_t newsize = 10000;
    size_t convertedChars = 0;
    static wchar_t wcstring[newsize];
	UINT size = msg.length();
	mbstowcs_s(&convertedChars, wcstring, origsize, msg.c_str(), _TRUNCATE);

	IDWriteTextLayout * text_layout;

	HRESULT hr = S_OK;

	hr = dw_factory->CreateTextLayout(
		wcstring,      // The string to be laid out and formatted.
		size,		   // The length of the string.
		current_text,  // The text format to apply to the string (contains font information, etc).
		min((FLOAT)width, wMax),         // The width of the layout box.
		min((FLOAT)height, hMax),        // The height of the layout box.
		&text_layout  // The IDWriteTextLayout interface pointer.
		);


	if( FAILED(hr) )
	{
		return;
	}
	DWRITE_TEXT_METRICS tex;
	text_layout->GetMetrics(&tex);

	*h = tex.height;
	*w = tex.width;

	ReleaseCOM(text_layout);
}

DWRITE_TEXT_METRICS Font2D::getTextMetrics( std::string msg, float wMax, float hMax)
{
	DWRITE_TEXT_METRICS tex;

	const size_t origsize = strlen(msg.c_str()) + 1;
    static const size_t newsize = 10000;
    size_t convertedChars = 0;
    static wchar_t wcstring[newsize];
	UINT size = msg.length();
	mbstowcs_s(&convertedChars, wcstring, origsize, msg.c_str(), _TRUNCATE);

	IDWriteTextLayout * text_layout;

	HRESULT hr = S_OK;

	hr = dw_factory->CreateTextLayout(
		wcstring,      // The string to be laid out and formatted.
		size,		   // The length of the string.
		current_text,  // The text format to apply to the string (contains font information, etc).
		min((FLOAT)width, wMax),         // The width of the layout box.
		min((FLOAT)height, hMax),        // The height of the layout box.
		&text_layout  // The IDWriteTextLayout interface pointer.
		);


	if( FAILED(hr) )
	{
		ZeroMemory( &tex, sizeof(tex) );
		return tex;
	}

	text_layout->GetMetrics(&tex);

	ReleaseCOM(text_layout);

	return tex;
}

void Font2D::getLineMetrics( const std::string & msg, DWRITE_LINE_METRICS* lineMetrics, UINT32 maxLineCount, UINT32 * actualLineCount, float wMax, float hMax )
{
	DWRITE_LINE_METRICS line;
	ZeroMemory( &line, sizeof(line) );

	const size_t origsize = strlen(msg.c_str()) + 1;
    static const size_t newsize = 10000;
    size_t convertedChars = 0;
    static wchar_t wcstring[newsize];
	UINT size = msg.length();
	mbstowcs_s(&convertedChars, wcstring, origsize, msg.c_str(), _TRUNCATE);

	IDWriteTextLayout * text_layout;

	HRESULT hr = S_OK;

	hr = dw_factory->CreateTextLayout(
		wcstring,      // The string to be laid out and formatted.
		size,		   // The length of the string.
		current_text,  // The text format to apply to the string (contains font information, etc).
		min((FLOAT)width, wMax),         // The width of the layout box.
		min((FLOAT)height, hMax),        // The height of the layout box.
		&text_layout  // The IDWriteTextLayout interface pointer.
		);


	if( FAILED(hr) )
	{
		return;
	}

	text_layout->GetLineMetrics( lineMetrics, maxLineCount, actualLineCount );

	ReleaseCOM(text_layout);
}

HRESULT Font2D::CreateD3DDevice(IDXGIAdapter *pAdapter, D3D10_DRIVER_TYPE driverType,
								 UINT flags, ID3D10Device1 **ppDevice)
{
	HRESULT hr = S_OK;

	// | D3D10_CREATE_DEVICE_DEBUG

    static const D3D10_FEATURE_LEVEL1 levelAttempts[] =
    {
        D3D10_FEATURE_LEVEL_10_0,
        D3D10_FEATURE_LEVEL_9_3,
        D3D10_FEATURE_LEVEL_9_2,
        D3D10_FEATURE_LEVEL_9_1,
    };

    for (UINT level = 0; level < ARRAYSIZE(levelAttempts); level++)
    {
        ID3D10Device1 *pDevice = NULL;
        hr = D3D10CreateDevice1(
            pAdapter,
            driverType,
            NULL,
            flags,
            levelAttempts[level],
            D3D10_1_SDK_VERSION,
            &pDevice
            );

        if (SUCCEEDED(hr))
        {
			std::cout << "Font2D Succeeds Device" << std::endl;

            // transfer reference
            *ppDevice = pDevice;
            pDevice = NULL;
            break;
        }
		else
		{
			std::cout << "Font2D Fails Device" << std::endl;
		}
    }

	return hr;
}
