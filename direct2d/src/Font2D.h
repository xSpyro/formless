#ifndef FONT2D_H
#define FONT2D_H

#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <wchar.h>

#include <string>
//#include <dxerr.h>
#include <cstdlib>

#include <d3d10_1.h>
#include <d3dx10math.h>
#include <dxgi.h>

#include <string>
#include <vector>

#include <map>

struct Vertex
{
    D3DXVECTOR3 Pos;
    D3DXVECTOR2 Tex;
};

class Font2D
{

public:
	Font2D();
	~Font2D();

	bool createDevices(int w, int h);

	int createFont(std::string font, D3DXCOLOR col, float size);

	bool createFont(std::string font);

	bool setFont(std::string font);

	void setTextAlignment(int id);
	int getTextAlignment();

	void setParagraphAlignment(int id);
	int getParagraphAlignment();

	void draw(std::string msg, float x, float y, float w = 9000.0f, float h = 9000.0f, D3DXCOLOR col = D3DXCOLOR(1, 1, 1, 1));

	void begin();
	void end();

	void recreateDevice();

	void shutDown();

	void getSize(std::string msg, float * w, float * h, float wMax = 9000.0f, float hMax = 9000.0f);

	DWRITE_TEXT_METRICS getTextMetrics( std::string msg, float wMax = 9000.0f, float hMax = 9000.0f);
	void getLineMetrics( const std::string & msg, DWRITE_LINE_METRICS* lineMetrics, UINT32 maxLineCount, UINT32 * actualLineCount, float wMax = 9000.0f, float hMax = 9000.0f );

	ID3D10ShaderResourceView * getSRV()
	{
		return textureRV;
	}
	IDXGISurface * getSurface()
	{
		return dxgi_surface;
	}

	void setSharedMutex(IDXGIKeyedMutex * mutex)
	{
		mutex_shared = mutex;
	}





private:

	HRESULT CreateD3DDevice(IDXGIAdapter *pAdapter, D3D10_DRIVER_TYPE driverType,
									 UINT flags, ID3D10Device1 **ppDevice);


private:
	typedef std::map< std::string , IDWriteTextFormat * > map_text;

	ID3D10Device			 * device;
	ID3D10Texture2D			 * off_screen;
	ID3D10ShaderResourceView * textureRV;

	IDXGISurface * dxgi_surface;

	int width;
	int height;

	ID2D1Factory     * d2_factory;
	IDWriteFactory     * dw_factory;
	ID2D1RenderTarget    * render_target;

	ID2D1SolidColorBrush * clear;
	IDWriteTextFormat    * current_text;

	map_text text_formats;

	bool redraw;
	IDXGIKeyedMutex * mutex;
	IDXGIKeyedMutex * mutex_shared;

	bool use_mutexes;

	//std::vector< ID2D1SolidColorBrush * > text_brushes;
	//std::vector< IDWriteTextFormat * >    text_formats;
};

#endif