
#include "Profile.h"

Profile::Profile(const std::string & name, bool auto_log)
	: log(std::cout), name(name), auto_log(auto_log)
{
	reset();

	min_ms[1] = 0;
	max_ms[1] = 0;
	avg_ms[1] = 0;
}

Profile::~Profile()
{

}

void Profile::reset()
{
	min_ms[0] = 9000;
	max_ms[0] = -9000;
	avg_ms[0] = 0;

	times = 0;
	acc = 0;
}

void Profile::begin()
{
	timer.restart();
}

void Profile::end()
{
	++times;

	float run = timer.now();

	if (run < min_ms[0])
		min_ms[0] = run;

	if (run > max_ms[0])
		max_ms[0] = run;

	acc += run;

	if (second.accumulate(1.0f))
	{
		avg_ms[0] = (acc / times);

		update();
		reset();
	}
}

void Profile::update()
{
	avg_ms[1] = avg_ms[0] * 1000.0f;
	min_ms[1] = min_ms[0] * 1000.0f;
	max_ms[1] = max_ms[0] * 1000.0f;

	if (auto_log)
	{
		output();
	}
}

void Profile::output()
{
	log(name, " -- ", (int)avg_ms[1], " < ", (int)min_ms[1], " > ", (int)max_ms[1]);
}