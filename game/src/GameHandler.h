
#ifndef GAMEHANDLER_H
#define GAMEHANDLER_H

#include "Common.h"
#include "Framework.h"

class ClientInterface;

class ModuleFactory;
class LogicHandler;
class Module;
class Object;
class Scene;
class Subsystem;
class EventHandler;
class LuaFileManager;
class Terrain;
class SharedFormatStore;
struct AccountPacket;

#include "SceneTransition.h"

/*
 *	GameHandler wraps the LogicHandler, and is the communication with the client and the logic.
 *	It takes care of deleting the logic handler when we're done.
 *	It communicates with LogicHandler, and needs a pointer to a LogicHandler to function properly.
 *	It holds all modules that are present in the game, which is very game specific.
 *	It currently passes on all input to the logic handler.
 *
 *	The game handler works on a more concrete level than the logic handler.
 */

class GameHandler
{
public:
	GameHandler();								//If called, a logic handler needs to be handed as soon as possible
	GameHandler(LogicHandler* logicHandler);
	~GameHandler();

	void					init();
	void					update();
	void					clear(); // frees all components and clears memory
	void					render();

	void					activateScene(const std::string name);
	bool					deactivateScene(const std::string name);
	void					gotoScene( const std::string name );		// Clears the active scenes and activates [name] instead.
	void					activateSceneOnUpdate(const std::string name);
	void					deactivateSceneOnUpdate(const std::string name);
	void					gotoSceneOnUpdate(const std::string name);				// Clears the active scenes and activates [name] instead, the next time LogicHandler is updated.
	void					gotoSceneUsingTransition(const std::string & name, SceneTransition * transition, int time = 16);	// same as gotosceneonupdate but uses a transition effect
	void					gotoSceneUsingLua(const std::string & name);
	bool					isInScene(const std::string & name);


	bool					isInTransition() const;

	Module*					getModule(unsigned int id, std::string name);

	Object*					createObject(const std::string name, Object* creator = NULL, Object* parent = NULL);
	Object*					createObjectExt(const std::string name, const Vector3 position, const Vector3 destination, Object* creator = NULL, Object* parent = NULL);
	void					destroyObject(Object* object);
	Object*					getObject(const std::string name);
	void					getObjects(const std::string name, std::vector<Object*>& vec);
	Object*					getControlledObject();										// Returns the object that the client controls
	std::vector<Object*> *	getCoreObjects();
	std::vector<Object*> *	getSkillObjects();
	bool					registerObjectTemplate(const std::string name, LuaType type = TYPE_OBJECT);
	bool					registerSkillTemplate(const std::string name, LuaType type = TYPE_SKILL);
	bool					registerTriggerTemplate(const std::string name, LuaType type = TYPE_TRIGGER);
	bool					registerSceneryTemplate(const std::string name, LuaType type = TYPE_OBJECT);
	bool					registerTemplate(const std::string name, EventLua::Object table);
	bool					loadObjectTemplates(std::istream & stream);
	bool					saveObjectTemplates(std::ostream & stream);

	bool					loadObjects(std::istream & stream);
	bool					saveObjects(std::ostream & stream);

	bool					loadHeightMap(std::istream & stream);
	bool					saveHeightMap(std::ostream & stream);
	bool					loadBlendMap(std::istream & stream);
	bool					saveBlendMap(std::ostream & stream);

	void					onInput(int sym, int state);
	void					onMouseMovement(int x, int y);

	void					onLoad();

	LuaFileManager *		getLuaFileManager();

	EventHandler *			getEventHandler();
	void					onConnected(unsigned int id);

	ClientInterface*		getNetwork();
	void					disconnect(); // disconnect from current game and clear all objects and game data

	void					setNetworkInterface(ClientInterface * network);
	void					setLogicHandler(LogicHandler* logicHandler);
	void					setHeightMap(Terrain * terrain);
	void					setBlendMap(Surface * blend);
	Surface *				getBlendMap();
	Terrain *				getTerrainMap();

	void					setFormatStore(SharedFormatStore * store);
	SharedFormatStore *		getFormatStore();

	EventLua::State*		getScript();
	int						getKey(int sym);

	Subsystem *				getSubsystem(std::string name);
	LogicHandler *			getLogic() const;

	void					loadItems();

	void					onSkillEffect(float damage, bool stun, bool link, bool outOfMana, int killStreak, float x, float y, float z, bool fromServer);
	void					onAccountUpdate(AccountPacket* packet);
	void					onKillObject(Object* killed, Object* killer);

	std::string				getDebugString() const;

	void					reloadAllLuaFiles(LuaType type = TYPE_UNKOWN);
private:
	bool loadString(std::istream & f, std::string & string);
	bool loadRelations(std::istream & f, std::vector<Object *> & obj);

private:
	SharedFormatStore * formatStore;

	ModuleFactory*			moduleFactory;
	LogicHandler*			logic;
	Terrain *				heightMap;
	Surface *				blendMap;

	EventLua::State*		lua;

	ClientInterface *		network;
	Logger					log;


	// transition
	
	std::string				currentSceneTransition;
	SceneTransition*		currentTransition;

	std::string				nextSceneTransition;
    SceneTransition*		nextTransition;
	
    int						stepTransition;
	int						frameTransition;
};

#endif