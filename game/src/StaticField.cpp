
#include "StaticField.h"

StaticField::StaticField()
	: PotentialField()
{
	//Only generated once, therefore calcPotential does not calculate.

	//The closer to the middle or outskirts of the map, the worse.
	//We want a range within the AI can move, and negative fields that grows
	//stronger closer to the middle and the edges.
	//The function below gives a maximum y at the distance 300.
	//Potential function: y = -0.005x^2 + 3x - 450

	//For each row and col in the field, generate a value depending on the distance to the opponent
	for(int row = 0; row < NR_ROWS; ++row)
	{
		for(int col = 0; col < NR_COLS; ++col)
		{
			float distance = (Vector2(NR_ROWS*0.5f, NR_COLS*0.5f) - Vector2((float)row, (float)col)).length();

			field[row][col] = ((-0.005f) * pow(distance, 2)) + (3 * distance) - 450;

			if(field[row][col] > 0.0f)
				field[row][col] = 0.0f;
		}
	}
}

StaticField::~StaticField()
{
}

float StaticField::calcPotential(Vector2 position)
{
	float returnValue = 0.0f;

	Vector2 transformed = transformToField(position);

	returnValue = field [ (int)transformed.x ] [ (int)transformed.y ];

	return returnValue;
}