
#ifndef COMMON_FIELDS_H
#define COMMON_FIELDS_H

#include "PotentialField.h"
#include "AttackField.h"
#include "HealingField.h"
#include "StaticField.h"
#include "TrailField.h"
#include "ExplorationField.h"

#endif