#ifndef RENDERSUBSYSTEM_H
#define RENDERSUBSYSTEM_H

#include "Subsystem.h"
class RenderComponent;

/*
 * The specific tasks are described in the corresponding component header file.
 */

class RenderSubsystem : public Subsystem
{
	typedef std::vector<RenderComponent> Components;

public:

	RenderSubsystem( GameHandler* gameHandler = NULL, int size = 1000 );
	virtual ~RenderSubsystem();

	Component*	createComponent(EventLua::Object script);
	Component*	getComponent();
	void		update();
	void		clear();

private:
	Components components;
};

#endif