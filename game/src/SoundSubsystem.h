#ifndef SOUND_SUB_SYSTEM_H

#define SOUND_SUB_SYSTEM_H

#include "Subsystem.h"
#include "SoundComponent.h"
#include "MovementSoundSubComponent.h"
#include "EffectSoundSubComponent.h"
#include "ListenerSoundSubComponent.h"

#include "../framework/src/AudioSystem.h"

#include "MusicPlayer.h"

#include "../framework/src/Camera.h"

class MusicPlayer;



class SoundSubsystem : public Subsystem
{
	typedef std::vector<SoundComponent> Sounds; 
	typedef std::vector<MovementSoundSubComponent> MoveSubComponents;
	typedef std::vector<EffectSoundSubComponent> EffectSubComponents;
	typedef std::vector<ListenerSoundSubComponent> ListenerSubComponents;

public:
	SoundSubsystem();
	SoundSubsystem(GameHandler* gameHandler);
	SoundSubsystem(GameHandler* gameHandler, const std::string loggerName);
	~SoundSubsystem();

	Component*					createComponent(EventLua::Object script);
	int							createComponent(const std::string soundFile, float volume, bool loop);
	int							playSound(const std::string soundFile, float volume, bool loop, bool gui);


	Component*					getComponent();

	MovementSoundSubComponent*	createMovementSubComponent(SoundComponent* parent);
	EffectSoundSubComponent*	createEffectSoundSubComponent(SoundComponent* parent, float spread, float doppler, float directOcclusion, float reverbOcclusion, bool useCreatorPosition);
	ListenerSoundSubComponent*	createListenerSoundSubComponent(SoundComponent* parent);

	MovementSoundSubComponent*	getMovementSubComponent();
	EffectSoundSubComponent*	getEffectSubComponent();
	ListenerSoundSubComponent*	getListenerSoundSubComponent();

	
	void						update();
	void						clear();

	void						stopSoundcomponent(const int id);
	void						stopSoundcomponent(const std::string& soundFile);

	void						stopSound(const int id);
	void						stopSound(const std::string& soundFile);

	
	void						mute();
	void						unmute();

	MusicPlayer* const			getMusicPlayer() const;
	AudioSystem* const			getAudioSystem();

	void						setCamera(Camera* const cam);


	void						setFXVolume(const float vol);
	void						setGUIVolume(const float vol);
	void						setMasterVolume(const float vol);
	void						setMusicVolume(const float vol);

	const float					getFXVolume() const;
	const float					getGUIVolume() const;
	const float					getMasterVolume() const;
	const float					getMusicVolume() const;

	void						setListener(Object* const player);

private:
	AudioSystem audio;
	Sounds sounds;
	MusicPlayer* music;
	MoveSubComponents moveSubComponents;
	EffectSubComponents effectSubComponents;
	ListenerSubComponents listenerSubComponents;

	Camera* camera;

	float fxVolume;
	float guiVolume;
	float masterVolume;

	SoundComponent nullComponent;

	ListenerSoundSubComponent* listener;

};

#endif