#ifndef MOVE_EVENT_H
#define MOVE_EVENT_H
#include "Events.h"
#include "Framework.h"

class MoveEvent : public Event
{
public:
	MoveEvent();
	MoveEvent(Vector3 dir, float speed, bool instant = false);
	virtual ~MoveEvent();

	virtual void sendTo(Object * to);

	Vector3 getMovement();
	float	getSpeed();
	bool	isInstant(); // If the speed is maxed instantly. Also resets velocity.

	void	setMovement(Vector3 dir);
	void	setSpeed(float speed);
	void	setInstant(bool instant);

protected:
	Vector3	dir;
	float speed;
	bool instant; // If the speed is maxed instantly. Also resets velocity.
};

#endif