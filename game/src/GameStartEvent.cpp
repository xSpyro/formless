#include "GameStartEvent.h"
#include "EventEnum.h"
#include "Object.h"

GameStartEvent::GameStartEvent() : Event(EVENT_GAME_START, sender)
{}

GameStartEvent::~GameStartEvent()
{}


void GameStartEvent::sendTo(Object * o)
{
	o->receive<GameStartEvent>(this);
}