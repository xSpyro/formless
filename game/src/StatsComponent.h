
#ifndef STATSCOMPONENT_H
#define STATSCOMPONENT_H

#include "Component.h"
class PhysicsComponent;

class StatsComponent : public Component
{
public:
	StatsComponent( Subsystem* subsystem = NULL );
	virtual ~StatsComponent();

	void receive(KillPlayerEvent* kill);
	void receive(GameStartEvent* start);
	void receive(GameEndEvent* end);

	void update();

	void bindToComponents();

	void addDamage(int damage);

	void reset();

public:
	int kills;
	int deaths;
	int killStreak;
	int highestKillStreak;
	int damage;
	int highestDamage;

private:
	PhysicsComponent* phys;
};

#endif