
#include "StatsComponent.h"
#include "KillPlayerEvent.h"
#include "PhysicsComponent.h"
#include "Object.h"
#include "GameHandler.h"
#include "Subsystem.h"

StatsComponent::StatsComponent(  Subsystem* subsystem /* = NULL */  )
	: Component( subsystem )
{
	reset();

	phys = NULL;
}

StatsComponent::~StatsComponent()
{

}

void StatsComponent::update()
{
}

void StatsComponent::receive(KillPlayerEvent * kill)
{
	if(kill->getCount())
	{
		if (kill->getKilled() == object)
		{
			deaths++;
			killStreak = 0;
			damage = 0;
		}
		else if (kill->getKiller() == object)
		{
			kills++;
			killStreak++;

			if( killStreak > 1 && phys != NULL)
			{
				Vector3 pos = phys->position;
				subsystem->getGameHandler()->onSkillEffect(0, false, false, false, killStreak, pos.x, pos.y, pos.z, false);
			}
			
			if( killStreak > highestKillStreak )
				highestKillStreak = killStreak;
		}
	}
}

void StatsComponent::receive(GameStartEvent* start)
{
	reset();
}

void StatsComponent::receive(GameEndEvent* end)
{

}

void StatsComponent::addDamage(int damage)
{
	this->damage += damage;
	if(this->damage > highestDamage)
		highestDamage = this->damage;
}

void StatsComponent::reset()
{
	kills = 0;
	deaths = 0;
	killStreak = 0;
	highestKillStreak = 0;
	damage = 0;
	highestDamage = 0;
}

void StatsComponent::bindToComponents()
{
	phys = object->findComponent<PhysicsComponent>();
}