#include "ActivateSkillEvent.h"
#include "EventEnum.h"
#include "Object.h"

ActivateSkillEvent::ActivateSkillEvent(int skill, bool wait, Object * sender, Object ** result, bool fromServer) : Event(EVENT_ACTIVATE_SKILL, sender), skill(skill), wait(wait), result(result), fromServer(fromServer), succsess(false)
{}

ActivateSkillEvent::~ActivateSkillEvent()
{}

int ActivateSkillEvent::getSkill()
{
	return skill;
}

void ActivateSkillEvent::setFromServer(bool server)
{
	fromServer = server;
}

bool ActivateSkillEvent::isFromServer()
{
	return fromServer;
}

bool ActivateSkillEvent::waitForConfirm()
{
	return wait;
}

void ActivateSkillEvent::sendTo(Object * o)
{
	o->receive<ActivateSkillEvent>(this);
}

void ActivateSkillEvent::setResult(Object * obj)
{
	if (result)
	{
		*result = obj;
	}
}

Object * ActivateSkillEvent::getResult()
{
	return *result;
}

void ActivateSkillEvent::setSuccsess(bool good)
{
	succsess = good;
}

bool ActivateSkillEvent::isSuccsess()
{
	return (succsess || result);
}