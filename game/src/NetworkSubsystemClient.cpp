
#include "ClientInterface.h"

#include "NetworkSubsystemClient.h"

#include "CommonPackets.h"
#include "GameHandler.h"
#include "Object.h"

#include "NetworkComponent.h"
#include "Events.h"

#include "ControllerComponent.h"
#include "AIComponent.h"
#include "CoreComponent.h"

#include "RequestInterface.h"

#include "Achievement.h"

#define FAIL_CHECK_LOG_RTN(o, msg, rtn) if (o == 0) {log.alert(msg);return rtn;}
#define FAIL_CHECK_LOG(o, msg) if (o == 0) {log.alert(msg);return;}
#define FAIL_CHECK_LOG_CONT(o, msg) if (o == 0) {log.alert(msg);continue;}

NetworkSubsystemClient::NetworkSubsystemClient(GameHandler * game, ClientInterface * network)
	: NetworkSubsystem(game), network(network), log(std::cout)
{
	for(int i = 0; i < NR_TEAMS; i++)
	{
		stats.kills[i] = 0;
	}
	stats.gameStarted = false;
	stats.gameTimeStr = "";
	stats.gameMins = 0;
	stats.gameSecs = 0;
	stats.allReady = false;

	respawnTimer.respawning = false;
	respawnTimer.time		= 0;

	requestTimer.restart();
}

void NetworkSubsystemClient::update()
{
	NetworkSubsystem::update();

	if(stats.gameStarted && requestTimer.accumulate(10.0f))
	{
		sendUserInfoRequest(players[client_id].getUserID());
	}
}

void NetworkSubsystemClient::clear()
{
	connections.clear();
	textBuffer.clear();

	NetworkSubsystem::clear();

	requestTimer.restart();
}

ClientInterface * NetworkSubsystemClient::getNetwork()
{
	return network;
}

void NetworkSubsystemClient::onPacket(RawPacket & raw)
{
	// when you get the connect packet set client it and
	// mount a component to the player object

	// this is also where the server processes the packets
	// but certain packets can only arrive to the clients and same for server

	switch (raw.header.id)
	{

	case DISCONNECT_PACKET:

		processDisconnectPacket(*reinterpret_cast < DisconnectPacket* > (raw.data));
		break;

	case CONNECT_PACKET:

		processConnectPacket(*reinterpret_cast < ConnectPacket* > (raw.data));
		break;

	//case LEVEL_DATA_RESPONSE_PACKET:
	//	
	//	processLevelDataResponsePacket(*reinterpret_cast < LevelDataResponsePacket* > (raw.data));
	//	break;

	case READY_PACKET:

		processReadyPacket(*reinterpret_cast < ReadyPacket* > (raw.data));
		break;

	case GAME_START_PACKET:
		
		processGameStartPacket(*reinterpret_cast < GameStartPacket* > (raw.data));
		break;

	case GAME_END_PACKET:

		processGameEndPacket(*reinterpret_cast < GameEndPacket* > (raw.data));
		break;

	case TIMER_PACKET:

		processTimerPacket(*reinterpret_cast < TimerPacket* > (raw.data));
		break;

	case RESPAWN_TIMER_PACKET:

		processRespawnTimerPacket(*reinterpret_cast < RespawnTimerPacket* > (raw.data));
		break;

	case SERVER_STATE_PACKET:

		processServerStatePacket(*reinterpret_cast < ServerStatePacket* > (raw.data));
		break;

	case CREATE_PACKET:

		processCreatePacket(*reinterpret_cast < CreatePacket* > (raw.data));
		break;

	case DESTROY_PACKET:

		processDestroyPacket(*reinterpret_cast < DestroyPacket* > (raw.data));
		break;

	case KILL_PLAYER_PACKET:

		processKillPlayerPacket(*reinterpret_cast < KillPlayerPacket* > (raw.data));
		break;

	case RESPAWN_PLAYER_PACKET:

		processRespawnPlayerPacket(*reinterpret_cast < RespawnPlayerPacket* > (raw.data));
		break;

	case LINK_PACKET:
		processLinkPacket(*reinterpret_cast< LinkPacket*> (raw.data));
		break;

	case TEAM_SELECT_PACKET:

		processTeamSelectPacket(*reinterpret_cast < TeamSelectPacket* > (raw.data));
		break;

	case UPDATE_PACKET:

		processUpdatePacket(raw);
		break;

	case TEXT_PACKET:
		
		processTextPacket(*reinterpret_cast < TextPacket* > (raw.data));
		break;

	case PLAYER_LIST_PACKET:

		processPlayerListPacket(reinterpret_cast < PlayerListPacket* > (raw.data));
		break;

	case USER_PACKET:

		processUserPacket(*reinterpret_cast < UserPacket* > (raw.data));
		break;

	case CHANGE_MODULE_PACKET:

		processChangeModulePacket(*reinterpret_cast < ChangeModulePacket* > (raw.data));
		break;

	case CORE_ATTR_PACKET:

		processCoreAttrPacket(*reinterpret_cast < CoreAttrPacket* > (raw.data));
		break;

	case ACCOUNT_AGE_PACKET:

		processAccountAgePacket(*reinterpret_cast < AccountPacket* > (raw.data));
		break;

	case SKILL_EFFECT_PACKET:

		processSkillEffectPacket(*reinterpret_cast < SkillEffectPacket* > (raw.data));
		break;

	case ACHIEVEMENT_RESPONSE_PACKET:

		processAchievementResponsePacket(*reinterpret_cast < AchievementResponsePacket * > (raw.data));
		break;

	case AI_COMMAND_PACKET:

		processAICommandPacket(*reinterpret_cast < AICommandPacket * > (raw.data));
		break;
	}
}

void NetworkSubsystemClient::onDisconnected()
{

}

std::string NetworkSubsystemClient::getName( unsigned int netId )
{
	std::string result = "Unknown";
	
	if( players.find( netId ) != players.end() )
	{
		result = players[ netId ].name;
	}

	return result;
}

void NetworkSubsystemClient::queryClientList(std::vector < Object* > & list)
{
	list = connections;
}

void NetworkSubsystemClient::queryClientList(std::vector< Object* > & list, const int team)
{
	for(Connections::iterator it = connections.begin(); it != connections.end(); ++it)
	{
		CoreComponent* core = (*it)->findComponent<CoreComponent>();
		if(core && core->team == team)
		{
			list.push_back((*it));
		}
	}
}

void NetworkSubsystemClient::queryTextBuffer(std::vector < std::string > & list)
{
	list = textBuffer;
	textBuffer.clear();
}

void NetworkSubsystemClient::queryPlayers( std::vector< PlayerInfo > & list )
{
	list.clear();
	for( PlayerList::const_iterator it = players.cbegin(); it != players.cend(); ++it )
	{
		list.push_back( it->second );
	}
}

Object * NetworkSubsystemClient::queryControlledObject() const
{

	//std::cout << "QueryControlledObject " << client_id << std::endl;

	// search for object with 'client_id' in the networked objects
	Components::const_iterator i = components.find(client_id);

	if(i != components.end())
	{
		return i->second->getObject();
	}

	return NULL;
}

bool NetworkSubsystemClient::isServer()
{
	return false;
}

void NetworkSubsystemClient::timeInMinsSecs(float time, int& mins, int& secs)
{
	int inttime = (int)time;
	mins = inttime / 60;
	secs = inttime % 60;
}

std::string NetworkSubsystemClient::timeToText(float time)
{
	int minute = 0;
	int second = 0;

	timeInMinsSecs(time, minute, second);

	std::stringstream minuteStr;
	if( minute < 10 )
		minuteStr << "0";
	minuteStr << minute;

	std::stringstream secondStr;
	if( second < 10 )
		secondStr << "0";
	secondStr << second;

	return minuteStr.str() + ":" + secondStr.str();
}

Statistics* NetworkSubsystemClient::getStats()
{
	return &stats;
}

RespawnTimer* NetworkSubsystemClient::getRespawnTimer()
{
	return &respawnTimer;
}

void NetworkSubsystemClient::sendUserInfoRequest(unsigned int userID)
{
	MasterRequestAsync master;

	AccountPacket packet;
	packet.userID = userID;
	master(packet, sizeof(packet));
}

void NetworkSubsystemClient::processDisconnectPacket(DisconnectPacket & packet)
{
	log("Player Disconnected ", packet.client_id);

	Components::iterator i = components.find(packet.client_id);

	if (i != components.end())
	{
		Object* obj = i->second->getObject();
		Object* companion = i->second->getObject()->getCompanion();

		if(companion != NULL)
		{
			for (Connections::iterator it = connections.begin(); it != connections.end(); ++it)
			{
				if ((*it) == companion)
				{
					connections.erase(it);
					break;
				}
			}

			Components::iterator compIt = components.find(packet.client_id*10);
			if(compIt != components.end())
				components.erase(compIt);
		}

		for (Connections::iterator it = connections.begin(); it != connections.end(); ++it)
		{
			if ((*it) == obj)
			{
				connections.erase(it);
				break;
			}
		}
		getGameHandler()->destroyObject(obj);
		components.erase(i);
	}

	players.erase( packet.client_id );
	players.erase( packet.client_id * 10 );	//Possible companion

	if(packet.client_id == client_id)
	{
		game->gotoSceneUsingLua("MainMenu");
	}
}

void NetworkSubsystemClient::processConnectPacket(ConnectPacket & packet)
{
	client_id = packet.client_id;

	// create self
	Object * player = getGameHandler()->getObject("Player");
	if(player)
	{
		player->addComponent(getClientComponent(client_id));
	}

	players.clear();

	// call the virtual callback function
	network->onConnected(client_id);
}

//void NetworkSubsystemClient::processLevelDataResponsePacket(LevelDataResponsePacket & packet)
//{
//	std::cout << "Recieved " << packet.size << " bytes of level data" << std::endl;
//}

void NetworkSubsystemClient::processReadyPacket(ReadyPacket & packet)
{
	Components::iterator it = components.find(packet.uid);

	if(it != components.end())
	{
		it->second->setReady(packet.clientReady);
	}
}

void NetworkSubsystemClient::processGameStartPacket(GameStartPacket & packet)
{
	int myTeam = 0;
	Object* me = NULL;
	for(Components::iterator it = components.begin(); it != components.end(); ++it)
	{
		CoreComponent* core = it->second->getObject()->findComponent<CoreComponent>();
		if(core->team > 0)
		{
			it->second->getObject()->sendInternal(&GameStartEvent());
		}
		if(it->second->getID() == client_id)
		{
			myTeam = core->team;
			me = it->second->getObject();
		}
	}

	if(myTeam > 0)
	{
		//Team 0 means spectate mode

		requestTimer.restart();

		game->gotoSceneUsingLua("Arena");
	}
	else
	{
		//Just in case the player respawned anyway
		me->sendInternal(&KillPlayerEvent());
	}
}

void NetworkSubsystemClient::processGameEndPacket(GameEndPacket & packet)
{
	for(Components::iterator it = components.begin(); it != components.end(); ++it)
	{
		it->second->setReady(false);
		it->second->getObject()->sendInternal(&GameEndEvent());
	}

	//unsigned int tempID = players[client_id].getUserID();
	//MasterSendAsync sendasync;
	//GameEndPacket end;
	//end.user_id = tempID;
	//sendasync(end, sizeof(GameEndPacket));

	requestTimer.restart();

	game->activateSceneOnUpdate("EndGame");
}

void NetworkSubsystemClient::processTimerPacket(TimerPacket & packet)
{
	int minute = 0;
	int second = 0;
	timeInMinsSecs(packet.time, minute, second);
	stats.gameMins = minute;
	stats.gameSecs = second;
	stats.gameTimeStr = timeToText(packet.time);
	for(unsigned int i = 0; i < NR_TEAMS; i++)
	{
		stats.kills[i] = packet.kills[i];
	}
	stats.gameStarted = packet.gameStarted;
	stats.allReady = packet.allReady;
}

void NetworkSubsystemClient::processRespawnTimerPacket(RespawnTimerPacket & packet)
{
	bool foundMe = false;

	for(unsigned int i = 0; i < 16 && foundMe == false; i++)
	{
		if(packet.playerIDs[i] == getID())
		{
			foundMe = true;
			
			respawnTimer.respawning = true;
			respawnTimer.time		= (int)packet.time;
		}
	}
}

void NetworkSubsystemClient::processServerStatePacket(ServerStatePacket & packet)
{
	connections.clear();
	log("ServerStatePacket ", packet.connections);

	for (unsigned int i = 0; i < packet.connections; ++i)
	{
		unsigned int uid = packet.net_id[i];
		Components::iterator j = components.find(uid);

		if( players.find( uid ) == players.end() || std::string(players[ uid ].getName()) == "Unknown" )
		{
			log("Requesting name of player with NetID: ", uid);
			UserRequestPacket request;
			request.uid = uid;
			network->send( request );
		}

		log("  * Query id ", uid);

		if(j != components.end())
		{
			Object * obj = j->second->getObject();
			if (obj)
			{
				connections.push_back(obj);

				log("  * Connection (", uid, ")"); 
			}
		}
	}
}

NetworkComponent* NetworkSubsystemClient::processCreatePacket(CreatePacket & packet)
{
	std::cout << "Create Packet Received " << packet.uid << std::endl;
	std::cout << " - x y z " << Vector3(packet.x, packet.y, packet.z) << std::endl;

	return NetworkSubsystem::processCreatePacket(packet);
}

void NetworkSubsystemClient::processDestroyPacket(DestroyPacket & packet)
{
	Components::iterator i = components.find(packet.uid);

	if (i != components.end())
	{
		log("Object Destroyed ", packet.uid);

		getGameHandler()->destroyObject(i->second->getObject());
	}
}

void NetworkSubsystemClient::processKillPlayerPacket(KillPlayerPacket & packet)
{
	//If I loop through all components, will that be ALL network components, or just the NetworkComponentClient?
	Components::iterator i = components.find(packet.uid);
	Components::iterator k = components.find(packet.killerId);
	Object * killer = 0;

	if (k != components.end())
		killer = k->second->getObject();

	if (i != components.end())
	{
		Object* obj = i->second->getObject();

		if (obj)
		{
			log("Player killed ", packet.uid);

			obj->sendInternal(&KillPlayerEvent(killer, obj, packet.count));
			if (killer)
				killer->sendInternal(&KillPlayerEvent(killer, obj, packet.count));

			game->onKillObject(obj, killer);
		}
		else
		{
			log("Killed object does not exist on the client, ID ", packet.uid);
		}
	}
	else
	{
		log("Killed networkcomponent does not exist on client, ID", packet.uid);
	}
}

void NetworkSubsystemClient::processRespawnPlayerPacket(RespawnPlayerPacket & packet)
{
	//If I loop through all components, will that be ALL network components, or just the NetworkComponentClient?
	Components::iterator i = components.find(packet.uid);

	if (i != components.end())
	{
		Object* obj = i->second->getObject();

		if (obj)
		{
			log("Player respawned ", packet.uid);

			obj->sendInternal(&RespawnPlayerEvent(packet.x, packet.y, packet.z, packet.count));

			if(packet.uid == getID())
			{
				respawnTimer.respawning = false;
				respawnTimer.time		= 0;
			}
		}
		else
		{
			log("Respawned object does not exist on the client, ID ", packet.uid);
		}
	}
	else
	{
		log("Respawned networkcomponent does not exist on client, ID", packet.uid);
	}
}

void NetworkSubsystemClient::processLinkPacket(LinkPacket & packet)
{
	Components::iterator owner = components.find(packet.owner);
	Components::iterator target = components.find(packet.target);
	Object * own = 0;
	Object * tar = 0;
	Object * link = 0;

	if (owner != components.end())
		own = owner->second->getObject();
	else
	{
		log.alert("Process Link Packet failed. Could not find owner with id: ", packet.owner, ".");
		return;
	}
	if (target != components.end())
		tar = target->second->getObject();
	else
	{
		log.alert("Process Link Packet failed. Could not find target with id: ", packet.target, ".");
		return;
	}

	if (packet.status)
	{
		link = game->createObject(packet.link, own);
		FAIL_CHECK_LOG(link, "Process Link Packet failed. Link could not be created(link).");

		link->sendInternal(&LinkEvent(link, own, tar, true));
		own->sendInternal(&LinkEvent(link, own, tar, true));
		tar->sendInternal(&LinkEvent(link, own, tar, true));
	}
	else
	{
		CoreComponent * core = own->findComponent<CoreComponent>();
		FAIL_CHECK_LOG(core, "Process Link Packet failed. Owners core could not be found(unlink).");
		link = core->getLink();
		FAIL_CHECK_LOG(link, "Process Link Packet failed. Link could not be found(unlink).");

		link->sendInternal(&LinkEvent(link, own, tar, false));
		own->sendInternal(&LinkEvent(link, own, tar, false));
		tar->sendInternal(&LinkEvent(link, own, tar, false));
	}
}

void NetworkSubsystemClient::processTeamSelectPacket(TeamSelectPacket & packet)
{
	if( packet.errorCode == 0 )
	{
		if(packet.team > 0)
		{
			//game->gotoSceneOnUpdate("Lobby");
			game->gotoSceneUsingLua("Lobby");
		}
		else if (packet.team == 0)
		{
			game->gotoSceneUsingLua("Arena");
		}
		else
		{
			// -1 new team
			game->gotoSceneUsingLua("TeamSelection");
		}
	}
	else
	{
		std::cout << "Cannot join team. Error Code: " << packet.errorCode << std::endl;
	}
}

void NetworkSubsystemClient::processUpdatePacket(RawPacket & raw)
{
	UpdatePacket * packet = reinterpret_cast < UpdatePacket * > (raw.data);
	Components::iterator i = components.find(packet->uid);

	//std::cout << "update " << update->uid << " me " << client_id << std::endl;

	if (i != components.end())
	{
		// forward

		//std::cout << "updates on other objects" << std::endl;
		i->second->onPacket(raw);

	}
	else if (packet->uid != client_id)
	{
		// we request what we dont have
		//RequestPacket packet(CREATE_PACKET, update->uid);
		//network->send(packet);

		//std::cout << "Got Unknown UID " << update->uid << std::endl;
	}
}

void NetworkSubsystemClient::processTextPacket(TextPacket & packet)
{
	std::stringstream streambuf;
	streambuf << packet.channel << ":" << packet.team << ":" << packet.name << ":" << packet.text;
	//std::string buf = (std::string)text->name + ": " + text->text;
	textBuffer.push_back( streambuf.str() );

	//std::cout << "Recieved TextPacket: " << textBuffer.back() << std::endl;
}

void NetworkSubsystemClient::processPlayerListPacket(PlayerListPacket * packet)
{
	char * offset = &reinterpret_cast < char* > (packet)[RawPacket::HeaderSize];
	unsigned int results = *reinterpret_cast < unsigned int* > (offset);
	offset += sizeof(unsigned int); // skip the results int

	players.clear();
	for (unsigned int i = 0; i < results; ++i)
	{
		PlayerInfo * player = reinterpret_cast < PlayerInfo* > (offset);
		players[ player->nid ] = *player;
		offset += sizeof(PlayerInfo);
	}
}

void NetworkSubsystemClient::processUserPacket(UserPacket & packet)
{
	log("User with NetID ", packet.nid, " have UserID ", packet.uid, " and is named \"", packet.name, "\"");
	players[ packet.nid ] = PlayerInfo( packet.uid, packet.team, packet.name );
	//players[ user->nid ].first = user->uid;
	//players[ user->nid ].second = user->name;
}

void NetworkSubsystemClient::processChangeModulePacket(ChangeModulePacket & packet)
{
	Components::iterator i = components.find(packet.uid);

	if (i != components.end())
	{
		// forward
		CoreComponent * core = i->second->getObject()->findComponent<CoreComponent>();

		if( packet.add )
			core->addModule((int)packet.slot, packet.moduleid, packet.module);
		else
			core->removeModule((int)packet.slot);
	}
}

void NetworkSubsystemClient::processCoreAttrPacket(CoreAttrPacket & packet)
{
	Components::iterator i = components.find(packet.uid);

	if(i != components.end())
	{
		CoreComponent * core = i->second->getObject()->findComponent<CoreComponent>();

		core->setSlots(packet.nrSlots, packet.nrSkills);
		core->life.setBase(packet.life);
		core->mana.setBase(packet.mana);
		core->lifeRegen.setBase(packet.lifeRegen);
		core->manaRegen.setBase(packet.manaRegen);
		core->speed.setBase(packet.speed);
		core->name = packet.name;

		ControllerComponent * cont = i->second->getObject()->findComponent<ControllerComponent>();
		if(cont)
			cont->setSpeed(core->speed.getMax());
		else
		{
			AIComponent * ai = i->second->getObject()->findComponent<AIComponent>();
			ai->setSpeed(core->speed.getMax());
		}
	}
}

void NetworkSubsystemClient::processAccountAgePacket(AccountPacket & packet)
{
	//std::cout << "Current client age is " << packet.age << std::endl;
	//std::cout << "Got previous item " << packet.age - packet.prevItemAt << " seconds ago, and receives next in " << packet.nextItemAt - packet.age << " seconds." << std::endl;

	//std::string itemname = packet.itemname;

	//if(itemname != "")
	//	std::cout << "The client has been awarded with item " << packet.itemname << std::endl;

	game->onAccountUpdate(&packet);
}

void NetworkSubsystemClient::processSkillEffectPacket(SkillEffectPacket & packet)
{
	game->onSkillEffect(packet.damage, packet.stun, packet.link, packet.outOfMana, packet.killStreak, packet.x, packet.y, packet.z, true);
}

void NetworkSubsystemClient::processAchievementResponsePacket(AchievementResponsePacket & packet)
{
	if( packet.unlocked )
		log( "Player ", packet.user_id, " unlocked an achievement: (", packet.type, ") ", packet.name );
}

void NetworkSubsystemClient::processAICommandPacket(AICommandPacket & packet)
{
	Components::iterator i = components.find(packet.uid);

	if(i != components.end())
	{
		AIComponent* ai = i->second->getObject()->findComponent<AIComponent>();
		if(ai)
		{
			ai->setTargetPosition(Vector3(packet.targetX, packet.targetY, packet.targetZ));
			ai->giveCommand(packet.command, packet.commandState);
		}
	}
}
