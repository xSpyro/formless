#ifndef MODULE_H
#define MODULE_H

#include "Common.h"
#include "Trait.h"

class Module
{
public:
	Module();
	~Module();

public:
	std::string name;
	std::string skill;
	unsigned int id;

	Trait		life;
	Trait		mana;
	Trait		lifeRegen;
	Trait		manaRegen;
	Trait		speed;
	Trait		damageDealt;
	Trait		damageRecieved;

	float		manaCost;
	float		lifeCost;

	float		cooldown;
};

#endif
