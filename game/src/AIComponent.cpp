
#include "AIComponent.h"
#include "CoreComponent.h"
#include "Module.h"
#include "Object.h"
#include "Events.h"
#include "EventEnum.h"
#include "PhysicsComponent.h"
#include "AISubsystem.h"
#include "GameHandler.h"
#include "NetworkSubsystem.h"
#include "NetworkSubsystemServer.h"
#include "NetworkComponent.h"
#include "NetworkComponentDummy.h"

#include "MemLeaks.h"

#include "CommonFields.h"

AIComponent::AIComponent( Subsystem* subsystem )
	: Component( subsystem ), activeSlot(-1), blockSkills(0)
{
	physcomponent	= NULL;
	corecomponent	= NULL;
	netcomponent	= NULL;

	master.object	= NULL;
	master.phys		= NULL;
	master.core		= NULL;
	master.xzDistance	= Vector2(0,0);
	master.distance	= 0;

	velocity		= Vector3(0.0f, 0.0f, 0.0f);
	speed			= 0.8f;

	targetPosition	= Vector3(0.0f, 0.0f, 0.0f);
	cooldown = 0;

	stunTicker = 0;

	pFields.reserve(100);

	attackTimer.restart();
	healTimer.restart();
	gameCountdown.restart();

	boltSlot	= -1;
	healSlot	= -1;
	chargeSlot	= -1;

	idle = true;
	linked = false;
	linkedWith = NULL;
}

AIComponent::~AIComponent()
{
	clearPotentialFields();
}

void AIComponent::receive(KeyEvent* key)
{
	//std::cout << "ControllerComponent received a KeyEvent" << std::endl;
}

void AIComponent::receive(KeyBindEvent* keyBind)
{
	//keyStates[keyBind->getName()] = keyBind->getState();
	//
	///*
	//if( keyStates["attack"] ) // Good for stress testing skills and triggers
	//{
	//	CoreComponent * comp = object->findComponent<CoreComponent>();
 //		comp->activateModule(activeSlot);
	//}*/

	//if (keyBind->getState())
	//{
	//	std::string name = keyBind->getName();
	//	int newSlot = -1;
	//	CoreComponent * comp = object->findComponent<CoreComponent>();

	

	//updateMovement();
}

void AIComponent::setSpeed(float newSpeed)
{
	speed = newSpeed;
	updateMovement();
}

void AIComponent::updateMovement()
{
	if( !activated )
		return;

	Vector3 newVel( 0.0f, 0.0f, 0.0f );
	if (stunTicker <= 0)
	{
		if( commandStates["up"] )
			newVel.z += 1.0f;

		if( commandStates["down"] )
			newVel.z -= 1.0f;

		if( commandStates["left"] )
			newVel.x -= 1.0f;

		if( commandStates["right"] )
			newVel.x += 1.0f;
	}

	if (newVel != velocity)
	{
		velocity = newVel;
		Vector3 temp = velocity;
		if(temp.lengthNonSqrt() > 0)
		{
			temp.normalize();
			//temp = temp * speed;
		}
		MoveEvent move(temp, speed);
		object->sendInternal(&move);
	}
}

void AIComponent::receive(MouseEvent* mouse)
{
	//std::cout << "ControllerComponent received a MouseEvent" << std::endl;
}

void AIComponent::receive(EffectEvent * ev)
{
	if (ev->isEnd())
	{
		speed -= ev->getEffect()->speedChange;
		if (ev->getEffect()->blockSkills)
		{
			blockSkills--;
		}

		Vector3 temp = velocity;
		if(temp.lengthNonSqrt() > 0)
		{
			temp.normalize();
			//temp = temp * speed;
		}
		physcomponent->receive(&MoveEvent(temp, speed));

		return;
	}
	else if (ev->isFirst())
	{
		speed += ev->getEffect()->speedChange;
		if (ev->getEffect()->blockSkills)
		{
			blockSkills++;
		}
	}

	if (stunTicker < ev->getEffect()->stuntime)
	{
		stunTicker = ev->getEffect()->stuntime;
		setSpeed(speed); // So that it updates the speed
		if( ev->isRemarkable() )
		{
			Vector3 physpos = physcomponent->position;
			subsystem->getGameHandler()->onSkillEffect(0, true, false, false, 0, physpos.x, physpos.y, physpos.z, false);
		}
	}

	Vector3 temp = velocity;
	if(temp.lengthNonSqrt() > 0)
	{
		temp.normalize();
		//temp = temp * speed;
	}
	physcomponent->receive(&MoveEvent(temp, speed));
	// TODO: Send new speed.
}

void AIComponent::receive(QueryEvent<Vector3> * qv)
{
	switch(qv->getType())
	{
	case LOOK_AT:
		qv->setData(targetPosition + physcomponent->offset, 1);
		break;
	default:
		break;
	}
}

void AIComponent::receive( KillPlayerEvent* ev )
{
	Component::receive(ev);

	clearStates();
}

void AIComponent::receive( RespawnPlayerEvent* ev )
{
	Component::receive(ev);

	clearStates();

	attackTimer.restart();
	healTimer.restart();
}

void AIComponent::receive( DestroyEvent* ev )
{
	object->unsubscribe(EVENT_CREATE);
	object->unsubscribe(EVENT_DESTROY);

	if(ev->getSender() == object)
	{
		if(master.object)
		{
			master.object->removeCompanion();
			master.object	= NULL;
			master.phys		= NULL;
			master.core		= NULL;
		}
	}
	else
	{
		for(Players::iterator it = allPlayers.begin(); it != allPlayers.end(); ++it)
		{
			if((*it).object == ev->getSender())
			{
				allPlayers.erase(it);
				break;
			}
		}
	}
}

void AIComponent::receive(CreatedEvent * ev)
{
	if(netSub->isServer())
	{
		Object* created = ev->getSender();

		if(created)
		{
			if( created->getName() == "Player" || created->getName() == "AIPlayer" )
				addGamePlayer(created);
		}
	}
}

void AIComponent::receive(GameStartEvent* ev)
{
	CoreComponent::Modules mods;
	corecomponent->getModuleSlots(mods);

	boltSlot	= -1;
	healSlot	= -1;
	chargeSlot	= -1;

	for(unsigned int i = 0; i < mods.size(); i++)
	{
		if(mods[i] != NULL)
		{
			if(mods[i]->name == "Ice Bolt")
				boltSlot = i;
			else if(mods[i]->name == "Life Stream")
				healSlot = i;
			else if(mods[i]->name == "Charge")
				chargeSlot = i;
		}
	}

	buildPotentialFields();

	idle = true;
	linked = false;
	linkedWith = NULL;

	attackTimer.restart();
	healTimer.restart();
	gameCountdown.restart();
}

void AIComponent::receive(LinkEvent* ev)
{
	linked = ev->getState();

	if(linked)
	{
		if(ev->getOwner() == object)
			linkedWith = ev->getTarget();
		else if(ev->getTarget() == object)
			linkedWith = ev->getOwner();
		else
			linkedWith = NULL;
	}
	else
		linkedWith = NULL;
}

void AIComponent::update()
{
	cooldown--;
	if (cooldown < 0)
		cooldown = 0;

	if (stunTicker == 1)
	{
		stunTicker = 0;
		updateMovement();
	}

	stunTicker--;
	if (stunTicker < 0)
		stunTicker = 0;

	if(physcomponent)
	{
		
		toMiddle = physcomponent->position;
		toMiddle.normalize();
		//physcomponent->receive(&PushEvent(temp));
	}

	if(netSub->isServer())
	{
		//Wait 2 seconds at the start of a game to make sure everyone has loaded properly
		if(gameCountdown.now() >= 2.0f)
		{
			if(physcomponent && corecomponent)
			{
				myTeam.clear();
				otherTeam.clear();
				myTeamInRange.clear();
				otherTeamInRange.clear();

				//Fill the two vectors with the visible players of each team
				for(Players::iterator it = allPlayers.begin(); it != allPlayers.end(); ++it)
				{
					if((*it).core->isPlayerAlive())
					{
						Vector3 dist = (*it).phys->position - physcomponent->position;
						it->xzDistance = Vector2(dist.x, dist.z);

						float length = dist.length();
						it->distance = length;

						if(length < VIEW_DIST)
						{
							if((*it).core->team == corecomponent->team)
								myTeam.push_back((*it));
							else
								otherTeam.push_back((*it));

							if(length < SKILL_RANGE)
							{
								if((*it).core->team == corecomponent->team)
									myTeamInRange.push_back((*it));
								else
									otherTeamInRange.push_back((*it));
							}
						}
					}
				}

				handlePotentialFields();
				handleAttack();
			}
		}
	}
}

void AIComponent::clearStates()
{
	commandStates.clear();
	updateMovement();
}

void AIComponent::setActiveSlot( int slot )
{
	activeSlot = slot;
}

void AIComponent::bindObject(Object* obj)
{
	Component::bindObject(obj);
	/*
	for(unsigned int i = 0; i < keybinds.size(); ++i)
	{
		object->subscribe(keybinds[i]);
	}
	*/
}

void AIComponent::unbindObject()
{
	Component::unbindObject();

	physcomponent = NULL;
	corecomponent = NULL;
}

void AIComponent::bindToComponents()
{
	physcomponent	= object->findComponent<PhysicsComponent>();
	corecomponent	= object->findComponent<CoreComponent>();
	netcomponent	= object->findComponent<NetworkComponent>();

	me.object		= object;
	me.core			= corecomponent;
	me.phys			= physcomponent;
	me.xzDistance	= Vector2(0,0);
	me.distance		= 0;

	netSub = (NetworkSubsystem*)subsystem->getGameHandler()->getSubsystem("Network");

	if(netSub->isServer())
	{
		object->subscribe(EVENT_CREATE);
		object->subscribe(EVENT_DESTROY);

		std::vector<Object*> objvector;
		subsystem->getGameHandler()->getObjects("Player", objvector);

		for(std::vector<Object*>::iterator it = objvector.begin(); it != objvector.end(); ++it)
			addGamePlayer((*it));

		objvector.clear();
		subsystem->getGameHandler()->getObjects("AIPlayer", objvector);

		for(std::vector<Object*>::iterator it = objvector.begin(); it != objvector.end(); ++it)
			addGamePlayer((*it));
	}
}

void AIComponent::setTargetPosition(const Vector3& position)
{
	if (stunTicker > 0)
		return;

	targetPosition = position;

	float deltax = targetPosition.x - physcomponent->position.x;
	float deltay = targetPosition.z - physcomponent->position.z;
	float angle	 = atan2(-deltay, deltax);

	// For some reasons, quaternions behave differently when the angle to rotate with
	// is negative than an ordinary matrix rotation. Instead of continuing rotation
	// on the other half of the unit circle, it backs. Therefore, if the angle becomes negative,
	// we have to rotate a whole lap on the unit circle, 2PI, and THEN back the amount.
	if( angle < 0.0f )
	{
		angle = 2 * (float)D3DX_PI + angle;
	}

	physcomponent->rotation.rotationAxis(Vector3(0,1,0), angle);
}

const Vector3 & AIComponent::getTargetPosition() const
{
	return targetPosition;
}

const Vector3 AIComponent::getDirection() const
{
	return toMiddle;
}

void AIComponent::giveCommand(std::string command, int state)
{
	commandStates[command] = state;

	if(state == 1)
	{
		int newSlot = -1;

		if( command.find( "slot" ) != std::string::npos && command.size() == 5 )
		{
			newSlot = command[4] - 48;	// sym for '0' is 48
		}

		if (command == "nextSlot")
		{
			newSlot = activeSlot + 1;
			while (!corecomponent->getModule( newSlot ))
			{
				newSlot++;
				if (newSlot > 10)
				{
					newSlot = -1;
					break;
				}
			}
		}
		else if (command == "prevSlot")
		{
			newSlot = activeSlot - 1;
			while (!corecomponent->getModule( newSlot ))
			{
				newSlot--;
				if (newSlot < 0)
					break;
			}
		}


		if( newSlot >= 0  && newSlot < corecomponent->getSkillSlots() )
		{
			if( corecomponent->getModule( newSlot ) )
			{
				//EventLua::Object selectSlot = subsystem->getGameHandler()->getScript()->getGlobals().get("selectSlot");
				activeSlot = newSlot;
				//selectSlot( activeSlot );
			}
		}

		if (! subsystem->getGameHandler()->getKey(17) && stunTicker <= 0)
		{
			if (command.find("act") != std::string::npos && command.size() == 4)
			{
				int slot = command[3] - 48;

				if (corecomponent->getModule(slot)!= 0 && cooldown == 0 && blockSkills <= 0)
				{
					object->send(&ActivateSkillEvent(slot, true, object));
					cooldown = 15; // 0.25 s
				}
			}

			if (command == "attack")
			{
				if (corecomponent->getModule(activeSlot) != 0 && cooldown == 0 && blockSkills <= 0)
				{
					object->send(&ActivateSkillEvent(activeSlot, true, object));
					cooldown = 15; // 0.25 s
				}
			}
		}
	}

	updateMovement();

	if(netSub->isServer())
	{
		if(netcomponent == NULL)
			netcomponent = object->findComponent<NetworkComponent>();

		NetworkSubsystemServer* netServer = (NetworkSubsystemServer*)netSub;
		netServer->broadcastAICommand(netcomponent->getID(), command, state, targetPosition);
	}

	NetworkComponentDummy* dum = dynamic_cast< NetworkComponentDummy* >(netcomponent);
	if(dum)
	{
		dum->makeDirty();
	}
}

void AIComponent::addGamePlayer(Object* newPlayer)
{
	//We never want to add ourselves to the list
	if(newPlayer != object)
	{
		//We don't want duplicates
		for(unsigned int i = 0; i < allPlayers.size(); i++)
		{
			if(allPlayers[i].object == newPlayer)
				return;
		}

		Player pl;
		pl.object	= newPlayer;
		pl.phys		= newPlayer->findComponent<PhysicsComponent>();
		pl.core		= newPlayer->findComponent<CoreComponent>();
		pl.xzDistance = Vector2(0,0);
		pl.distance	= 0;

		allPlayers.push_back(pl);
	}
}

void AIComponent::setMaster(Object* player)
{
	master.object = player;

	if(player)
	{
		master.phys = player->findComponent<PhysicsComponent>();
		master.core	= player->findComponent<CoreComponent>();
	}
	else
	{
		master.phys	= NULL;
		master.core = NULL;
	}
}

Object* AIComponent::getMaster()
{
	return master.object;
}

void AIComponent::getMyTeam(std::vector<Player>& inVector)
{
	inVector.clear();

	for(Players::iterator it = myTeam.begin(); it != myTeam.end(); ++it)
	{
		inVector.push_back((*it));
	}
}

void AIComponent::getOtherTeam(std::vector<Player>& inVector)
{
	inVector.clear();

	for(Players::iterator it = otherTeam.begin(); it != otherTeam.end(); ++it)
	{
		inVector.push_back((*it));
	}
}

AIComponent::Player& AIComponent::getStructMe()
{
	return me;
}

bool AIComponent::isIdle()
{
	return idle;
}

void AIComponent::handlePotentialFields()
{
	//values is an array with the HIGHEST values around me in the potential field
	float values[9];
	for(int i = 0; i < 9; i++)
		values[i] = 0;
	/*
		+z
		|	7	0	1
		|
		|	6	8	2
		|
		|	5	4	3
		|--------------- +x
	*/

	Vector2 myPos = Vector2(physcomponent->position.x, physcomponent->position.z);

	for(Fields::iterator it = pFields.begin(); it != pFields.end(); ++it)
	{
		(*it)->update(myPos);

		//tempValues is an array with the values around me in the current potential field
		float tempValues[9];

		tempValues[0]	= (*it)->calcPotential(Vector2(myPos.x,			myPos.y + 10	));
		tempValues[1]	= (*it)->calcPotential(Vector2(myPos.x + 10,	myPos.y + 10	));
		tempValues[2]	= (*it)->calcPotential(Vector2(myPos.x + 10,	myPos.y			));
		tempValues[3]	= (*it)->calcPotential(Vector2(myPos.x + 10,	myPos.y - 10	));
		tempValues[4]	= (*it)->calcPotential(Vector2(myPos.x,			myPos.y - 10	));
		tempValues[5]	= (*it)->calcPotential(Vector2(myPos.x - 10,	myPos.y - 10	));
		tempValues[6]	= (*it)->calcPotential(Vector2(myPos.x - 10,	myPos.y			));
		tempValues[7]	= (*it)->calcPotential(Vector2(myPos.x - 10,	myPos.y + 10	));
		tempValues[8]	= (*it)->calcPotential(Vector2(myPos.x,			myPos.y			));

		//If we've found a value higher than that we already had, change
		//This calculation assumes that we want the highest value, not the total
		//for(int i = 0; i < 8; i++)
		//{
			//if(values[i] < tempValues[i])
				//values[i] = tempValues[i];
		//}
		//This calculation assumes that we want the total value, not the highest
		for(int i = 0; i < 9; i++)
			values[i] += tempValues[i];
	}

	//Now, find which one of the 8 directions had the highest value to determine which way to go
	int highest = 0;
	int nrOfZeroes = 0;
	for(int i = 0; i < 9; i++)
	{
		if(values[i] > values[highest])
			highest = i;

		if(values[i] < 1.0f && values[i] > -1.0f)
			nrOfZeroes++;
	}

	//If we found nothing aside from zero, nothing of interest is close and we stand still.
	if(nrOfZeroes == 9)
	{
		highest = -1;
		idle = true;
	}
	else
		idle = false;

	//Reset all commands
	giveCommand("right", 0);
	giveCommand("left", 0);
	giveCommand("up", 0);
	giveCommand("down", 0);

	//Give new command depending on direction
	switch(highest)
	{
	case 0:
		giveCommand("up", 1);
		break;
	case 1:
		giveCommand("up", 1);
		giveCommand("right", 1);
		break;
	case 2:
		giveCommand("right", 1);
		break;
	case 3:
		giveCommand("right", 1);
		giveCommand("down", 1);
		break;
	case 4:
		giveCommand("down", 1);
		break;
	case 5:
		giveCommand("down", 1);
		giveCommand("left", 1);
		break;
	case 6:
		giveCommand("left", 1);
		break;
	case 7:
		giveCommand("left", 1);
		giveCommand("up", 1);
		break;
	case 8:
		//If the highest value is where we stand, then keep still
		break;
	}
}

void AIComponent::handleAttack()
{
	//Only try to activate skills twice per second, more human-like
	if(attackTimer.accumulate(0.5f))
	{
		//If we have brought a lightning bolt
		if(boltSlot >= 0)
		{
			//If someone in the other team is in skill range
			if(otherTeamInRange.size() > 0)
			{
				//Activate the bolt slot
				std::stringstream str;
				str << "slot" << boltSlot;
				giveCommand(str.str(), 1);

				//Find the opponent with lowest health and attack it
				unsigned int lowest = 0;

				for(unsigned int i = 1; i < otherTeamInRange.size(); i++)
				{
					if(otherTeamInRange[i].core->life.getCurrent() < otherTeamInRange[lowest].core->life.getCurrent())
						lowest = i;
				}

				targetPosition = otherTeamInRange[lowest].phys->position;

				giveCommand("attack", 1);
			}
		}
	}

	if(healTimer.accumulate(1.0f))
	{
		//If we have brought the life stream
		if(healSlot >= 0)
		{
			//If someone in my team is in skill range, and I'm not already healing someone
			if(myTeamInRange.size() > 0 && !linked)
			{
				//Activate the life stream slot
				std::stringstream str;
				str << "slot" << healSlot;
				giveCommand(str.str(), 1);

				//Find the teammate with lowest health, check if it needs healing and then heal
				unsigned int lowest = 0;

				for(unsigned int i = 1; i < myTeamInRange.size(); i++)
				{
					if(myTeamInRange[i].core->life.getCurrent() < myTeamInRange[lowest].core->life.getCurrent())
						lowest = i;
				}

				//If the teammate with lowest health has max health, healing is not necessary
				if(myTeamInRange[lowest].core->life.getCurrent() < myTeamInRange[lowest].core->life.getMax())
				{
					targetPosition = myTeamInRange[lowest].phys->position;

					giveCommand("attack", 1);
				}	
			}

			//If we are healing someone, check if the healing is complete
			//For now, we assume that the only link skill we're using is the Life Stream
			//First check if we are linked.
			if(linked)
			{
				bool inMyTeam = false;
				//Loop through my team to find the link target
				for(unsigned int i = 0; i < myTeam.size() && inMyTeam == false; i++)
				{
					//If the linkTarget is a team member
					if(myTeam[i].object == linkedWith)
					{
						//If that team member has full health
						if(myTeam[i].core->life.getCurrent() >= myTeam[i].core->life.getMax())
						{
							std::stringstream str;
							str << "slot" << healSlot;
							giveCommand(str.str(), 1);

							giveCommand("attack", 1);
						}

						inMyTeam = true;
					}
				}
			}
		}


		////If we have brought charge
		//if(chargeSlot >= 0)
		//{
		//	//If someone in the opposite team is in my view range, but I have nothing in my skill range
		//	if(otherTeam.size() > 0 && otherTeamInRange.empty())
		//	{
		//		//Activate the charge slot
		//		std::stringstream str;
		//		str << "slot" << chargeSlot;
		//		giveCommand(str.str(), 1);

		//		//Find the opponent closest to us and charge towards it
		//		unsigned int closest = 0;

		//		for(unsigned int i = 1; i < otherTeam.size(); i++)
		//		{
		//			if(otherTeam[i].distance < otherTeam[closest].distance)
		//				closest = i;
		//		}

		//		targetPosition = otherTeam[closest].phys->position;

		//		giveCommand("attack", 1);
		//	}
		//}
	}
	

}

void AIComponent::buildPotentialFields()
{
	pFields.push_back(New StaticField());
	pFields.push_back(New TrailField(this));

	if(boltSlot >= 0)
	{
		pFields.push_back(New AttackField(this));
		pFields.back()->useWeights(true);
	}
	if(healSlot >= 0)
	{
		pFields.push_back(New HealingField(this));
		pFields.back()->useWeights(true);
	}

	//pFields.push_back(New ExplorationField(this));
}

void AIComponent::clearPotentialFields()
{
	for(Fields::iterator it = pFields.begin(); it != pFields.end();)
	{
		Delete (*it);
		it = pFields.erase(it);
	}
	pFields.clear();
}