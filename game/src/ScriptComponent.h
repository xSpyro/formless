
#ifndef SCRIPTCOMPONENT_H
#define SCRIPTCOMPONENT_H

#include "Component.h"
#include "Common.h"
#include "EventEnum.h"
#include "FunctionTable.h"

/*	
 *	The script component handles the scripting of an object.
 *	If an object has no script component, it cannot use scripted functions,
 *	and cannot be changed dynamically in lua.
 */

class ScriptComponent : public Component
{
public:

	ScriptComponent( Subsystem* subsystem = NULL );
	virtual ~ScriptComponent();


	void receive(KeyEvent * key);
	void receive(KeyBindEvent * keyBind);
	void receive(DestroyEvent * de); // onOtherDestroyed is also called in KillPLayerEvent with an additional argument
	void receive(CreatedEvent * de);
	void receive(MouseEvent * mouse);
	void receive(TimerEvent * timer);
	void receive(TriggerEvent * trigger);
	void receive(LinkEvent * link);		
	void receive(DamageEvent * damage);
	void receive(EffectEvent * effect);
	void receive(KillPlayerEvent * kill); // Calls onOtherDestroyed with a second argument that is true 

	void update();
	void bindObject(Object* obj);

public:
	FunctionTable callbacks;
};

#endif