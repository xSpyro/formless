
#include "PhysicsSubsystem.h"
#include "PhysicsComponent.h"
#include "Framework.h"
#include "Object.h"
#include "TriggerEvent.h"
#include "Terrain.h"
#include "../framework/src/Layer.h"

PhysicsSubsystem::PhysicsSubsystem( GameHandler* gameHandler, int size )
	: Subsystem( gameHandler, "PhysicsSubsystem" ), heightMap(0)
{
	components.reserve(size);
}

PhysicsSubsystem::~PhysicsSubsystem()
{
}

Component* PhysicsSubsystem::createComponent(EventLua::Object script)
{
	EventLua::Object scriptscale	= script.get("scale");
	EventLua::Object scriptrotation = script.get("rotation");
	EventLua::Object scriptposition = script.get("position");
	EventLua::Object scriptoffset	= script.get("offset");
	EventLua::Object scriptgravity	= script.get("gravity");
	EventLua::Object data			= script.get("sphere");
	EventLua::Object box			= script.get("box");

	PhysicsComponent* comp = (PhysicsComponent*)this->getComponent();

	if (comp == 0)
		return 0;

	if(scriptposition.isTable())
	{
		comp->position.x = scriptposition.get(1).queryFloat();
		comp->position.y = scriptposition.get(2).queryFloat();
		comp->position.z = scriptposition.get(3).queryFloat();
	}
	if(scriptrotation.isTable())
	{
		comp->rotation.x = scriptrotation.get(1).queryFloat();
		comp->rotation.y = scriptrotation.get(2).queryFloat();
		comp->rotation.z = scriptrotation.get(3).queryFloat();
		comp->rotation.w = scriptrotation.get(4).queryFloat();
	}
	if(scriptscale.isTable())
	{
		comp->scale.x = scriptscale.get(1).queryFloat(1.0f);
		comp->scale.y = scriptscale.get(2).queryFloat(1.0f);
		comp->scale.z = scriptscale.get(3).queryFloat(1.0f);
	}
	if(scriptoffset.isTable())
	{
		comp->offset.x = scriptoffset.get(1).queryFloat();
		comp->offset.y = scriptoffset.get(2).queryFloat();
		comp->offset.z = scriptoffset.get(3).queryFloat();
	}
	if (scriptgravity.isTable())
	{
		comp->gravity.x = scriptgravity.get(1).queryFloat();
		comp->gravity.y = scriptgravity.get(2).queryFloat();
		comp->gravity.z = scriptgravity.get(3).queryFloat();
	}
	if (data.isTable())
	{
		comp->sphere.radius = data.get(1).queryFloat();
		comp->sphereOffset.x = data.get(2).queryFloat();
		comp->sphereOffset.y = data.get(3).queryFloat();
		comp->sphereOffset.z = data.get(4).queryFloat();
	}
	else
	{
		Sphere s = {Vector3(0, 0, 0), 0};
		comp->sphere = s;
	}
	if (box.isTable())
	{
		data = box.get("dimension");
		Vector3 dim;
		if (data.isTable())
		{
			dim.x = data.get(1).queryFloat();
			dim.y = data.get(2).queryFloat();
			dim.z = data.get(3).queryFloat();
		}
		data = box.get("offset");
		if (data.isTable())
		{
			comp->boxOffset.x = data.get(1).queryFloat() / comp->scale.x; // As the offset will be scaled with the components scale afterwards.
			comp->boxOffset.y = data.get(2).queryFloat() / comp->scale.y;
			comp->boxOffset.z = data.get(3).queryFloat() / comp->scale.z;
		}
		data = box.get("rotation");
		Quaternion rotation;
		if (data.isTable())
		{
			comp->boxRotOffset.x = data.get(1).queryFloat();
			comp->boxRotOffset.y = data.get(2).queryFloat();
			comp->boxRotOffset.z = data.get(3).queryFloat();
			comp->boxRotOffset.w = data.get(4).queryFloat();
		}

		comp->box.set(dim, comp->boxOffset, comp->boxRotOffset);
	}


	comp->trigger = script["trigger"].queryBoolean(false);
	comp->hoverPower = script["hoverPower"].queryFloat(0.0f);
	comp->unmovable = script["static"].queryBoolean(false);
	comp->useProjectileRotation = script["useProjectileRotation"].queryBoolean(false);
	comp->repulsion = script["repulsion"].queryFloat(0.0f);

	comp->applyOffset();

	return comp;
}

Component* PhysicsSubsystem::getComponent()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if( !(*it).isAlive() )
		{
			(*it) = PhysicsComponent( this );
			return &(*it);
		}
	}
	if (components.size() == components.capacity())
		return 0;

	components.push_back( PhysicsComponent( this ) );
	return &components.back();
}

void PhysicsSubsystem::update()
{
	for( Triggers::iterator it = triggers.begin(); it != triggers.end(); ++it)
	{
		it->second.alive = false;
	}

	for (TriggerTerrain::iterator it = terrain.begin(); it != terrain.end(); it++)
	{
		it->second = false;
	}

	collisions.clear();

	for( Components::iterator it = components.begin(); it != components.end(); it++ )
	{
		if(it->isBound() && it->activated)
		{
			if (!it->isStatic() && it->collideWithArena())
			{
				Vector3 pos = it->getPosition();
				Vector3 posxz = Vector3(pos.x, 0, pos.z);

				// limit boundaries!
				if (posxz.length() > 440)
				{
					Vector3 n = posxz;
					n.normalize();

					it->position.x = pos.x + n.x * 440 - it->position.x;
					it->position.z = pos.z + n.z * 440 - it->position.z;
				}

				// limit center
				if (pos.length() < 150 && pos.length() != 0)
				{
					Vector3 n = pos;
					n.normalize();

					it->position = pos + n * 150  - it->position;
				}
			}
			

			for( Components::iterator it2 = it + 1; it2 != components.end();  it2++)
			{
				if(it2->isBound() && it->activated)
				{
					IntersectResult res;
					if (it2->collision(it._Ptr, &res))
					{
						Collision c = {it._Ptr, it2._Ptr, res};
						collisions.push_back(c);
					}
				}
			}
		}
	}


	for( Collisions::iterator it = collisions.begin(); it != collisions.end(); it++ )
	{
		bool related = it->lhs->getObject()->isRelated(it->rhs->getObject());

		IntersectResult r2 = it->res;
		r2.dist = -r2.dist;
		if (!related)
		{
			it->lhs->handleRepulsion(it->rhs, &it->res);
			it->rhs->handleRepulsion(it->lhs, &r2);
		}

		if (it->lhs->trigger || it->rhs->trigger)
		{
			if (!(it->lhs->trigger && it->rhs->trigger))
				checkTrigger((*it));
		}
		else
		{
			it->lhs->handleCollision(it->rhs, &it->res);
			it->rhs->handleCollision(it->lhs, &r2);
		}
	}


	for( Components::iterator it = components.begin(); it != components.end(); it++ )
	{
		if(it->isAlive() && it->activated)
		{
			it->update();
			if (it->updateYPosition(heightMap))
			{
				Object * obj = it->getObject();
				if (obj)
				{
					terrain[it._Ptr] = true;
					obj->send(&TriggerEvent(TRIGGER_TERRAIN, true));
				}
			}
			else if (it->isLinked() && it->getTarget())
			{
				float d = heightMap->findDistance(it->getPosition(), it->getTarget()->getPosition());
				Object * obj = it->getObject();
				if (d < -10.0f && obj) // TODO: Maybe shouldn't be hard coded... <-------HARDCODED!!!-------HARDCODED!!!-------HARDCODED!!!-------HARDCODED!!!-------
				{
					terrain[it._Ptr] = true;
					obj->send(&TriggerEvent(TRIGGER_TERRAIN, true));
				}
			}
		}
	}

	for (TriggerTerrain::iterator it = terrain.begin(); it != terrain.end();)
	{
		if(!it->second)
		{
			if (it->first->trigger)
				int hje = 0;

			Object * obj = it->first->getObject();
			if (obj)
				obj->send(&TriggerEvent(TRIGGER_TERRAIN, false));

			it = terrain.erase(it);
		}
		else
			it++;
	}

	for( Triggers::iterator it = triggers.begin(); it != triggers.end();)
	{
		if(!it->second.alive)
		{
			Object * f = it->first.first->getObject(), * s = it->first.second->getObject();
			if (f)
				f->send(&TriggerEvent(s, false, it->second.res));
			if (s)
				s->send(&TriggerEvent(f, false, it->second.res));

			it = triggers.erase(it);
		}
		else
			it++;
	}
}

void PhysicsSubsystem::clear()
{
	components.clear();
	collisions.clear();
	triggers.clear();
	terrain.clear();
}

std::string PhysicsSubsystem::getDebugString() const
{
	unsigned int out = 0;
	for( Components::const_iterator it = components.begin(); it != components.end(); ++it )
	{
		if(it->isBound() && it->activated)
			++out;
	}

	std::stringstream format;
	format << "instances: " << out;
	return format.str();
}

void PhysicsSubsystem::forgetTrigger(PhysicsComponent * phys, PhysicsComponent * phys2)
{
	for( Triggers::iterator it = triggers.begin(); it != triggers.end();)
	{
		if((it->first.first == phys && it->first.second == phys2) || (it->first.first == phys2 && it->first.second == phys))
		{
			it = triggers.erase(it);
		}
		else
			it++;
	}
}

void PhysicsSubsystem::forgetAllTriggers(PhysicsComponent * phys)
{
	for( Triggers::iterator it = triggers.begin(); it != triggers.end();)
	{
		if(it->first.first == phys ||it->first.second == phys)
		{
			it = triggers.erase(it);
		}
		else
			it++;
	}
}

void PhysicsSubsystem::checkTrigger(Collision & col)
{
	PhysicsPair p(col.rhs, col.lhs);
	Triggers::iterator it = triggers.find(p);

	if (it != triggers.end())
	{
		it->second.alive = true;
		it->second.res = col.res;
	}
	else
	{
		Trigger t = {col.res, true};

		IntersectResult res1 = col.res, res2 = col.res;
		res2.dist = - res2.dist;

		triggers[p] = t;
		p.first->getObject()->send(&TriggerEvent(p.second->getObject(), true, res1));
		p.second->getObject()->send(&TriggerEvent(p.first->getObject(), true, res2));
	}
}

void PhysicsSubsystem::setTerrain(Terrain * terrain)
{
	heightMap = terrain;
}