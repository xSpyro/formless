#ifndef QUERY_EVENT_H
#define QUERY_EVENT_H
#include "Event.h"
#include "EventEnum.h"
#include "QueryEnum.h"
#include "Object.h"

template <typename T>
class QueryEvent : public Event
{
public:
	QueryEvent(QueryEnum type);
	virtual ~QueryEvent();
	virtual void sendTo(Object * to);

	QueryEnum getType();

	int getPriority();

	T getData();
	bool isSet();
	bool setData(T val, int priority = 0);
private:
	QueryEnum type;
	int prio;
	bool set;
	T data;
};

template <typename T> 
QueryEvent<T>::QueryEvent(QueryEnum type) : Event(EVENT_QUERY), type(type), set(false), prio(-1)
{}

template <typename T> 
QueryEvent<T>::~QueryEvent()
{}

template <typename T> 
int QueryEvent<T>::getPriority()
{
	return prio;
}

template <typename T> 
QueryEnum QueryEvent<T>::getType()
{
	return type;
}

template <typename T> 
void QueryEvent<T>::sendTo(Object * to)
{
	to->receive<QueryEvent>(this);
}

template <typename T> 
bool QueryEvent<T>::setData(T val, int priority = 0)
{
	if (priority >= prio)
	{
		prio = priority;
		data = val;
		set = true;
		return true;
	}
	return false;
}

template <typename T>
T QueryEvent<T>::getData()
{
	return data;
}

template <typename T> 
bool QueryEvent<T>::isSet()
{
	return set;
}

#endif