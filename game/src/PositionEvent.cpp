#include "PositionEvent.h"
#include "EventEnum.h"
#include "Object.h"


PositionEvent::PositionEvent(float x, float y, float z ) : Event(EVENT_POSITION, 0,  true), pos(x, y, z)
{}
PositionEvent::PositionEvent(Vector3 p) : Event(EVENT_POSITION, 0,  true), pos(p)
{}

PositionEvent::~PositionEvent()
{}

Vector3 PositionEvent::getPosition()
{
	return pos;
}

void PositionEvent::setPosition(Vector3 p)
{
	pos = p;
}

void PositionEvent::sendTo(Object * to)
{
	to->receive<PositionEvent>(this);
}

