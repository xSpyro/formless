#include "SkillSubsystem.h"
#include "SkillComponent.h"

SkillSubsystem::SkillSubsystem( GameHandler* gameHandler, int size )
	: Subsystem( gameHandler, "SkillSubsystem" )
{
	components.reserve(size);
}

SkillSubsystem::~SkillSubsystem()
{
}

Component* SkillSubsystem::createComponent( EventLua::Object script )
{
	SkillComponent* comp = (SkillComponent*)this->getComponent();
	*comp = SkillComponent( &script, this );

	return comp;
}

Component* SkillSubsystem::getComponent()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if( !(*it).isBound() )
		{
			(*it) = SkillComponent( this );
			return &(*it);
		}
	}

	components.push_back( SkillComponent( this ) );
	return &components.back();
}

void SkillSubsystem::update()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if(it->isBound() && it->activated)
			it->update();
	}
}

void SkillSubsystem::clear()
{
	components.clear();
}

std::string SkillSubsystem::getDebugString() const
{

	unsigned int out = 0;
	for( Components::const_iterator it = components.begin(); it != components.end(); ++it )
	{
		if(it->isBound() && it->activated)
			++out;
	}

	std::stringstream format;
	format << "instances: " << out;
	return format.str();

}