#include "PhysicsComponent.h"
#include "RenderComponent.h"
#include "Events.h"
#include "../../formats/src/LabelEnum.h"

RenderComponent::RenderComponent( Subsystem* subsystem )
	: Component( subsystem )
{
	modelName	= "test.obj";
	shader		= "shaders/obj.vs";
}

RenderComponent::~RenderComponent()
{
}

void RenderComponent::load(Renderer* renderer)
{
	// MODELDATA
	ModelData * data = NULL;

	BaseShader * vs = renderer->shader()->get(shader);

	if(data = renderer->modelImport()->importSafe(modelName, vs))
	{
		//data->submit(vs); // importSafe will do it if needed

		model.create(data);
	}
	else
	{

		static Mesh * sphereMesh = NULL;

		if (sphereMesh == NULL)
		{
			SkySphere::Desc desc;
			desc.scale = 1;
			desc.wireframe = false;

			SkySphere::generate(renderer, vs, &desc, &sphereMesh);
		}
		
		// If no model was found with modelName, generate a SkySphere

		
		Renderable rend(sphereMesh, NULL, NULL);
		rend.setTexture(renderer->texture()->get(texture));

		model.clear();
		model.add(rend);
	}

	
	static Mesh * meshDebug = NULL;

	if (meshDebug == NULL)
	{
		// get identity cube for obb
		Box::generate(renderer, vs, NULL, &meshDebug);
	}
	
	modelDebug.getTransform().identity();

	modelDebug.clear();
	modelDebug.add(Renderable(meshDebug, NULL, NULL));
}

void RenderComponent::update()
{
}

void RenderComponent::receive(PositionEvent * pos)
{
	position = pos->getPosition();
	updateMatrix();
}

void RenderComponent::receive(RotationEvent * rot)
{
	if(rot->getApply())
		rotation.rotationAxisApply(rot->getAxis(), rot->getAngle());
	else
		rotation.setAxisAngle(rot->getAxis(), rot->getAngle());

	updateMatrix();
}

void RenderComponent::receive(ScaleEvent * se)
{
	if(se->getApply())
		scale = scale * se->getScale();
	else
		scale = se->getScale();

	updateMatrix();
}

void RenderComponent::receive(KillPlayerEvent * kill)
{
	Component::receive(kill);
	if (kill->getKilled() == object)
		model.setVisible(0);
}

void RenderComponent::receive(RespawnPlayerEvent * res)
{
	Component::receive(res);
	model.setVisible(1);
}

void RenderComponent::receive(QueryEvent<Vector3> * qv)
{
	switch (qv->getType())
	{
	case QUERY_POSITION:
		qv->setData(position, 0);
		break;
	case QUERY_SCALE:
		qv->setData(scale, 0);
		break;
	default:
		return;
	}
}

void RenderComponent::receive(QueryEvent<Quaternion> * qq)
{
	switch (qq->getType())
	{
	case QUERY_ROTATION:
		qq->setData(rotation, 0);
		break;
	default:
		return;
	}
}

void RenderComponent::unbindObject()
{
	Component::unbindObject();
	model.detach();
}

bool RenderComponent::load(std::istream &f, int label)
{
	if (object->findComponent<PhysicsComponent>() == 0)
	{
		switch (label)
		{
		case LABEL_POSITION:
			f.read((char *)&position, sizeof(Vector3));
			updateMatrix();
			break;
		case LABEL_SCALE:
			f.read((char *)&scale, sizeof(Vector3));
			updateMatrix();
			break;
		case LABEL_ROTATION:
			f.read((char*)&rotation, sizeof(Quaternion));
			updateMatrix();
			break;
		default:
			break;
		}
	}
	return true;
}

bool RenderComponent::save(std::ostream &f)
{
	if (object->findComponent<PhysicsComponent>() == 0)
	{
		LabelEnum label;
		label = LABEL_POSITION;
		f.write((char*)&label, sizeof(LabelEnum));
		f.write((char*)&position, sizeof(Vector3));

		label = LABEL_SCALE;
		f.write((char*)&label, sizeof(LabelEnum));
		f.write((char*)&scale, sizeof(Vector3));

		label = LABEL_ROTATION;
		f.write((char*)&label, sizeof(LabelEnum));
		f.write((char*)&rotation, sizeof(Quaternion));
	}

	return true;
}

void RenderComponent::updateMatrix()
{
	model.getNativeMatrix().identity();

	model.getNativeMatrix().scale(scale);
	model.getNativeMatrix().rotateQuaternion(rotation);
	model.getNativeMatrix().translate(position);
}

void RenderComponent::setDebugMatrix(const Matrix & matrix)
{
	modelDebug.getTransform() = matrix;
}