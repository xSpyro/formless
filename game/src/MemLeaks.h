#ifndef MEM_LEAKS_H

#define MEM_LEAKS_H

#include <stdlib.h>
#include <crtdbg.h>


#ifdef _DEBUG
	#define Malloc(s)       _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
	#define Calloc(c, s)    _calloc_dbg(c, s, _NORMAL_BLOCK, __FILE__, __LINE__)
	#define Realloc(p, s)   _realloc_dbg(p, s, _NORMAL_BLOCK, __FILE__, __LINE__)
	#define Expand(p, s)    _expand_dbg(p, s, _NORMAL_BLOCK, __FILE__, __LINE__)
	#define Free(p)         _free_dbg(p, _NORMAL_BLOCK)
	#define MemSize(p)      _msize_dbg(p, _NORMAL_BLOCK)
	#define New new(_NORMAL_BLOCK, __FILE__, __LINE__)
	#define Delete delete 
	#define InitMemoryCheck() \
    _CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF)

#else

	#define Malloc malloc
	#define Calloc calloc
	#define Realloc realloc
	#define Expand _expand
	#define Free free
	#define MemSize _msize
	#define New new
	#define Delete delete
	#define InitMemoryCheck()

#endif



#endif