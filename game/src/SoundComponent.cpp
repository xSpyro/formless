
#include "SoundComponent.h"

#include "KeyEvent.h"
#include "KillPlayerEvent.h"
#include "GameHandler.h"
#include "Subsystem.h"

SoundComponent::SoundComponent()
	:Component(NULL),
	channelID(-1), sound(""), channel(NULL), playing(false), volume(0), startPlaying(false), stopPlaying(false)
{

}
SoundComponent::SoundComponent(Subsystem* subSystem, int id, std::string file, FMOD::Channel* const chan, int prop, const float vol, AudioSystem* const audio)
	: Component(subSystem),
	channelID(id), sound(file), channel(chan), playing(false), volume(vol), startPlaying(true), stopPlaying(false)
	, is3D(false), isPlayer(false)
{
	this->audio = audio;
	if (prop == 1 && sound == "")
		hasSound = false;
	else
		hasSound = true;
}

SoundComponent::SoundComponent(Component& rhs, int id, std::string file, FMOD::Channel* const chan, int prop, const float vol, AudioSystem* const audio)
	: Component(rhs),
	channelID(id), sound(file), channel(chan), playing(false), volume(vol), startPlaying(true), stopPlaying(false)
	, is3D(false), isPlayer(false)
{
	this->audio = audio;
	if (prop == 1 && sound == "")
		hasSound = false;
	else
		hasSound = true;
}


SoundComponent::~SoundComponent()
{
}

void SoundComponent::update()
{

	if (hasSound == false)
	{
		playing = true;
	}

	else if (channel)
	{
		channel->isPlaying(&playing);

		
		if (!playing)
		{
			makeChildsDirty();
		}
		else
		{
			channel->setVolume(volume);
			channel->setPaused(false);
		}
	}

	else
	{
		playing = true;
	}
}

void SoundComponent::addSoundSubComponent(SoundSubComponent* const subComp)
{
	subComponents.push_back(subComp);
}

void SoundComponent::receive(KeyEvent * key)
{	
}

void SoundComponent::receive(MoveEvent * move)
{
}

void SoundComponent::receive(RespawnPlayerEvent * res)
{

	//audio->play("spawn0.mp3", 1.0f, false);
	
	if(this->isAlive())
	{
		for(unsigned int i = 0; i < subComponents.size(); i++)
		{
			subComponents[i]->receive(res);
		}
	}

}

void SoundComponent::receive(EffectEvent * effect)
{
	if( this->isAlive() )
	{
		for(unsigned int i = 0; i < subComponents.size(); i++)
		{
			subComponents[i]->receive(effect);
		}
	}
}

void SoundComponent::receive(KillPlayerEvent * kill)
{
	if( this->isAlive() && object == kill->getKilled())
	{
		for(unsigned int i = 0; i < subComponents.size(); i++)
		{
			subComponents[i]->receive(kill);
		}
	}
}

const std::string SoundComponent::getSoundName() const
{
	return sound;
}

const int SoundComponent::getChannelID() const
{
	return channelID;
}

FMOD::Channel* SoundComponent::getChannel() const
{
	return channel;
}

const bool SoundComponent::getLooping() const
{
	return loop;
}

const bool SoundComponent::getIs3D() const
{
	return is3D;
}

const bool SoundComponent::isPlaying() const
{
	return playing;
}

const bool SoundComponent::isStartPlaying() const
{
	return startPlaying;
}

const bool SoundComponent::isStopPlaying() const
{
	return stopPlaying;
}

void SoundComponent::setStartPlaying(const bool p)
{
	startPlaying = p;
}

void SoundComponent::setStopPlaying(const bool p)
{
	stopPlaying = p;
}

void SoundComponent::setIsplaying(const bool p)
{
	playing = p;
}

void SoundComponent::setChannel(FMOD::Channel* const chan)
{
	this->channel = chan;
}

void SoundComponent::setID(const int id)
{
	this->channelID = id;
}

void SoundComponent::setLooping(const bool l)
{
	loop = l;
}

void SoundComponent::set3D(const bool d)
{
	is3D = d;
}

const float SoundComponent::getVolume() const
{
	return volume;
}

void SoundComponent::makeChildsDirty()
{
	for(unsigned int i = 0; i < subComponents.size(); i++)
	{
		subComponents[i]->setDirty(true);
	}
}

AudioSystem* const SoundComponent::getAudio() const
{
	return audio;
}

const bool SoundComponent::getHasSound() const
{
	return hasSound;
}

void SoundComponent::bindToComponents()
{
	Object* player = getSubsystem()->getGameHandler()->getControlledObject();

	if (this->getObject()->getCreator() == player)
		isPlayer = true;
}