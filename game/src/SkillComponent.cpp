#include "SkillComponent.h"
#include "TimerEvent.h"
#include "Object.h"
#include "EffectEvent.h"
#include "DamageEvent.h"
#include "CoreComponent.h"
#include "Subsystem.h"
#include "PhysicsComponent.h"
#include "GameHandler.h"
#include "DestroyEvent.h"
#include "StatsComponent.h"

#include "../../logic/src/Subsystem.h"
#include "../../logic/src/EventHandler.h"
#include "../../logic/src/EventEnum.h"

SkillComponent::SkillComponent( Subsystem* subsystem )
	: Component( subsystem )
{
}

SkillComponent::SkillComponent( EventLua::Object* script, Subsystem* subsystem )
	: Component( subsystem )
{
	effect.damage			= script->get("damage").query(0.0f);
	effect.particleDamage	= script->get("particleDamage").query(0.0f);
	effect.knockback		= script->get("knockback").query(0.0f);
	effect.stuntime			= script->get("stun").queryInteger(0);
	effect.speedChange		= script->get("speedChange").query(0.0f);
	effect.range			= script->get("range").query(0.0f);
	effect.blockSkills		= script->get("blockSkills").query(false);
	effect.knockbackFromOwner = script->get("fromPlayer").query(false);
	effect.noDamageOffset	= script->get("noDamageOffset").query(false);

	casterAnimation			= script->get("casterAnimation").queryString();

	sound					= script->get("sound").queryString();
}

SkillComponent::~SkillComponent()
{
}

void SkillComponent::receive( TimerEvent* timer )
{
	//std::cout << "Timer event received, id: " << timer->getID() << std::endl;
}

void SkillComponent::receive(DestroyEvent * de)
{
	if (de->getSender() != object)
		return;

	Objects tempEffected = effected;
	for (Objects::iterator it = tempEffected.begin(); it != tempEffected.end(); ++it)
	{
		if((*it))
		{
			(*it)->send(&EffectEvent(&effect, false, getObject(), (*it), false, true));
		}
	}

	// for (int n = 0; n < effected.size(); ++n)
	// {
		// effected[n]->send(&EffectEvent(&effect, false, getObject(), effected[n], false, true));
	// }
	effected.clear();

	for(std::vector<Object*>::iterator it = subscriptionTargets.begin(); it != subscriptionTargets.end(); ++it)
	{
		subsystem->getGameHandler()->getEventHandler()->unsubscribe(object, EVENT_DESTROY, (*it));
	}

	subscriptionTargets.clear();
}

void SkillComponent::update()
{
}

void SkillComponent::addEffect(Object * target, Object * caster, bool remarkable)
{
	if (!target)
		return;

	DamageEvent dam(Attribute(effect.damage, 0, 1, true, true), target, getObject());
	if (effect.damage > 0)
	{
		CoreComponent * core = target->findFamilyComponent<CoreComponent>();
		if (core)
		{
			dam.damage.setModifier(core->damageRecieved.getModifier());
			dam.damage.setOffset(core->damageRecieved.getOffset());
		}
		target->send(&dam);
	}
	SkillEffect t = effect;
	t.damage = dam.damage.getMax();
	for (Objects::iterator it = effected.begin(); it != effected.end(); ++it)
	{
		if((*it) == target)
		{
			repeatEffect(target, remarkable);
			return;
		}
	}

	//Let the caster count the damage done
	if(caster)
	{
		StatsComponent* stats = caster->findComponent<StatsComponent>();
		if(stats)
		{
			stats->addDamage((int)t.damage);
		}
	}

	    
	target->send(&EffectEvent(&t, true, getObject(), target, remarkable));
	effected.push_back(target);
	subsystem->getGameHandler()->getEventHandler()->subscribe(object, EVENT_DESTROY, target);

	subscriptionTargets.push_back(target);
}

void SkillComponent::repeatEffect(Object * o, bool remarkable) // If o != object then repeate an effect at target. If o == object then repeate for all effected
{
	if (!o)
		return;

	if (o == object)
	{
		Objects tempEff = effected;
		for (Objects::iterator it = tempEff.begin(); it != tempEff.end(); ++it)
		{
			if ((*it)->isDestroying())
				continue;

			(*it)->send(&EffectEvent(&effect, false, getObject(), (*it), remarkable));
		}
		return;
	}

	o->send(&EffectEvent(&effect, false, getObject(), o, remarkable));
}

void SkillComponent::removeEffect(Object * o)
{
	if (!o)
		return;

	Objects tempEff = effected;
	for (Objects::iterator it = tempEff.begin(); it != tempEff.end(); ++it)
	{
		if((*it) == o)
		{
			o->send(&EffectEvent(&effect, false, getObject(), o, false, true));
			subsystem->getGameHandler()->getEventHandler()->unsubscribe(object, EVENT_DESTROY, o);
			(*it) = 0;
		}
	}
}

bool SkillComponent::isEffecting(Object * o)
{
	for (Objects::iterator it = effected.begin(); it != effected.end(); ++it)
	{
		if((*it) == o)
		{
			return true;
		}
	}
	return false;
}

void SkillComponent::bindObject(Object* obj)
{
	Component::bindObject(obj);
	if (obj)
	{
		CoreComponent * core = obj->findFamilyComponent<CoreComponent>();
		if (core && effect.damage > 0)
		{
			core->damageDealt.setBase(effect.damage);
			if (effect.noDamageOffset)
			{
				float temp = core->damageDealt.getOffset();
				core->damageDealt.setOffset(0.0f);
				effect.damage = core->damageDealt.getMax();
				core->damageDealt.setOffset(temp);
			}
			else
			{
				effect.damage = core->damageDealt.getMax();
			}
		}
	}
}

void SkillComponent::unbindObject()
{
	for (Objects::iterator it = effected.begin(); it != effected.end(); ++it)
	{
		if((*it))
		{
			(*it)->send(&EffectEvent(&effect, false, getObject(), (*it), false, true));
		}
	}
	effected.clear();
	object = 0;
}

std::string SkillComponent::getCasterAnimation()
{
	return casterAnimation;
}

std::string SkillComponent::getSound()
{
	return sound;
}

void SkillComponent::bindToComponents()
{
	stats = object->findComponent<StatsComponent>();
}