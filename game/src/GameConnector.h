#ifndef LUA_CONNECTOR_H
#define LUA_CONNECTOR_H

#include "Common.h"

class GameHandler;

namespace GameConnector
{
	void setGame(GameHandler * game);
	void move(int id, float x, float y, float z, int dir = -1, bool instant = false);	// Moves according to dir's look at if dir >= 0
	void push(int id, float x, float y, float z, int dir = -1);	// Moves according to dir's look at if dir >= 0
	void fire(int id, int dir, bool high, float offsetX = 0, float offsetY = 0, float offsetZ = 0);	// Moves to dir's mouse position + offset. If it doesnt have one it fails.
	void link(const char * linkName, int ownerID, int targetID);	// Assigns a link to another object
	void unlink(int linkId, int ownerId, int targetId); // Only needs one of the inputs. If linkId the unlink that link with that id. If ownerId then unlink all links from that owner. If targetId then unlink all links to that target. If owner and target then unlink the link between them.
	int create(const char * name, EventLua::Object parent); // Bool says if the caller is parent or not. Int says who is the parent

	void registerTemplate(const char * name, EventLua::Object table);

	void restartTimer(int timerId, int objId = 0);

	bool destroy(int id);
	void stopMovement(int id);

	void pauseAnimation(int id);
	void resumeAnimation(int id);
	void setAnimation(int id, const char * anim);
	void setParent(int target, int parent);
	int getRoot(int id = 0);
	int getParent(int id = 0);
	int getCreator(int id = 0);
	int getOwner(int id = 0);
	int getLinkTarget(int id = 0);

	int getTeam(int id);
	float getRand(float from, float to);

	EventLua::Object getLife(int id); // If in an object it returns the object defined by ids life. Else returns the controlled objects life
	EventLua::Object getMana(int id); // If in an object it returns the object defined by ids mana. Else returns the controlled objects mana

	float getMouseX(int id);
	float getMouseY(int id);
	float getMouseZ(int id);
	EventLua::Object getDirToMouse(int id, float multitude = 1.0f);
	EventLua::Object getDir(int from, int to, float multitude = 1.0f);

	void loadScript(const char * name, int type = TYPE_UNKOWN);

	bool setPosition(int target, int from, float x = 0, float y = 0, float z = 0);
	bool setRotation(int target, int from, float x = 0, float y = 0, float z = 0, float w = 0);
	bool setScale(int target, float x = 1.0f, float y = 1.0f, float z = 1.0f);

	bool setKeyBind(const char  * name, EventLua::Object info);
	void setAssignable(const char * assignable, const char * target);
	EventLua::Object getAssignablesFor(const char * target);
	EventLua::Object getActiveAssignables();

	const char* getKeyBind( const char* name );

	bool subscribeKeyBind(const char * c);
	bool subscribeEvent(int id, int type);
	bool subscribeKeyEvents();

	bool isMonster(int id);
	bool isEffecting(int taget, int effecter = 0);
	bool effect(int id, bool unremarkable);
	bool repeatEffect(int id, bool unremarkable); // If id == 0 then repeate for all effected otherwise repeate for only that object
	bool removeEffect(int id);

	int getKey(int sym);

	void attach(int id, int idTo);
	void detach(int id, int idFrom);
	void disableSlowdown(int id);
	void enableSlowdown(int id);

	int playSound(const char* file, float volume, int loop, int gui);
	void stopSound(int id);

	void forgetTrigger(int id, int id2); // Forgets the collision between id and id2, will make it so that if the object still is inside trigger enter will be called again.
	void forgetAllTriggers(int id); // Forgets the collision between phys and everything, will make it so that if the object still is inside trigger enter will be called again.

	bool checkTimer(int id, int timer); // Returns true if timer have been triggered.

	void reloadLuaFiles();


	void sendAICommand(int id, const char* command, int state);
}
#endif