
#include "ModuleFactory.h"
#include "Module.h"

#include "MemLeaks.h"

ModuleFactory::ModuleFactory()
{

}

ModuleFactory::~ModuleFactory()
{
	modules.clear();
}

void ModuleFactory::loadModules( EventLua::Object script )
{
	EventLua::Object key, val;

	while( script.next( key, val ) )
	{
		Module module;
		module.name						= val.get("name").queryString();

		module.skill					= val.get("skill").queryString();

		module.id						= 0;

		module.life.offset				= val.get("lifeOffset").queryFloat();
		module.life.modifier			= val.get("lifeModifier").queryFloat();
		module.mana.offset				= val.get("manaOffset").queryFloat();
		module.mana.modifier			= val.get("manaModifier").queryFloat();

		module.lifeRegen.offset			= val.get("lifeRegenOffset").queryFloat();
		module.lifeRegen.modifier		= val.get("lifeRegenModifier").queryFloat();
		module.manaRegen.offset			= val.get("manaRegenOffset").queryFloat();
		module.manaRegen.modifier		= val.get("manaRegenModifier").queryFloat();

		module.damageDealt.offset		= val.get("damageOffset").queryFloat();
		module.damageDealt.modifier		= val.get("damageModifier").queryFloat();
		module.damageRecieved.offset	= val.get("resistOffset").queryFloat();
		module.damageRecieved.modifier	= val.get("resistModifier").queryFloat(1.0f);

		module.lifeCost					= val.get("lifeCost").queryFloat();
		module.manaCost					= val.get("manaCost").queryFloat();
		module.speed.offset				= val.get("speedOffset").queryFloat();
		module.speed.modifier			= val.get("speedModifier").queryFloat();

		module.cooldown					= val.get("cooldown").queryFloat();

		moduleTemplates[module.name]	= module;
	}
}

Module* ModuleFactory::getModule( unsigned int id, std::string name )
{
	Modules::iterator i = modules.find( id );

	if( i != modules.end() )
	{
		return i->second;
	}
	else if( moduleTemplates.find( name ) != moduleTemplates.end() )
	{
		modules[ id ] = New Module( moduleTemplates[ name ] );
		modules[ id ]->id = id;
		return modules[ id ];
	}
	else
	{
		return NULL;
	}
}