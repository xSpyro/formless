#include "MusicPlayer.h"

MusicPlayer::MusicPlayer(SoundSubsystem* s)
	: sound(s), currentContainer(NULL), id(-1), muted(false), oldVolume(0.0f)
{
	setVolume(0.3f);
}

MusicPlayer::~MusicPlayer()
{

}

void MusicPlayer::init()
{
	Playlist temp;

	temp.push_back("402567_Jump_Into_A_Puddle_and_Rel.mp3");
	temp.push_back("arena1.mp3");
	temp.push_back("arena3.mp3");
	// temp.push_back("arena4.mp3");
	temp.push_back("arena5.mp3");

	//temp.push_back("Formless songha.mp3");

	songs["Arena"] = temp;

	temp.clear();


	temp.push_back("start1.mp3");
	temp.push_back("start3.mp3");

	songs["Start"] = temp;


	temp.clear();

	temp.push_back("ghost1.mp3");

	songs["Ghost"] = temp;

	temp.clear();



}

void MusicPlayer::queryAllSongs()
{
	for( Songs::iterator it = songs.begin(); it != songs.end(); it++)
	{
		for(Playlist::iterator jt = it->second.begin(); jt != it->second.end(); jt++)
		{
			sound->getAudioSystem()->query(*(jt));
		}
	}

	std::cout << "music is loaded" << std::endl;
}

void MusicPlayer::update()
{
	int time = 0;
	int maxTime = 0;


	maxTime = sound->getAudioSystem()->getSoundLengthMS(id);

	time = sound->getAudioSystem()->getPlayingSoundMS(id);

	if (time > -1)
	{
		if ( maxTime - time <= 1000) 
		{
			randomizeSongFromCurrentContainer(false);
		}
	}
	else 
		play(*(currentSong));

	
	if (muted)
		sound->getAudioSystem()->mute(this->id);
	else
		sound->getAudioSystem()->unmute(this->id);
}

void MusicPlayer::play(const std::string& song)
{
	if(currentContainer)
	{
		for(unsigned int i = 0; i < currentContainer->size(); i++)
		{
			if (song == currentContainer->at(i))
			{
				id = sound->playSound(currentContainer->at(i), 0.0f, true, false);

				if (muted)
				{
					sound->getAudioSystem()->mute(id);
				}

				int time = 0;
				int maxTime = 0;


				maxTime = sound->getAudioSystem()->getSoundLengthMS(id);

				time = sound->getAudioSystem()->getPlayingSoundMS(id);

				sound->getAudioSystem()->fadeIn(id, volume * sound->getMasterVolume());
				currentSong = currentContainer->begin() + i;
				return;
			}
		}
	}
}

void MusicPlayer::playWithRandomPosition(const std::string& song)
{
	if(currentContainer)
	{
		for(unsigned int i = 0; i < currentContainer->size(); i++)
		{
			if (song == currentContainer->at(i))
			{
				id = sound->playSound(currentContainer->at(i), 0.0f, true, false);

				if (muted)
				{
					sound->getAudioSystem()->mute(id);
				}

				int time = 0;
				int maxTime = 0;
				unsigned int pos = 0;
				maxTime = sound->getAudioSystem()->getSoundLengthMS(id);

				time = sound->getAudioSystem()->getPlayingSoundMS(id);

				pos = (unsigned int)random((float)time, (float)maxTime);

				sound->getAudioSystem()->setPositionForChannel(id, pos);

				sound->getAudioSystem()->fadeIn(id, volume * sound->getMasterVolume());
				
				currentSong = currentContainer->begin() + i;
				
				return;
			}
		}
	}
}

void MusicPlayer::stop()
{
	sound->getAudioSystem()->fadeOut(id, 0.5f);
}

void MusicPlayer::randomizeSongFromCurrentContainer(bool randomPosition)
{
	if (currentContainer && currentContainer->size() > 0)
	{

		int song = (int)random(0.0f, (float)currentContainer->size());

		Playlist::iterator it = currentContainer->begin() + song;

		sound->getAudioSystem()->fadeOut(id, 0.0f);

		if (randomPosition)
			playWithRandomPosition((*it));
		else
			play((*it));
	}
}

void MusicPlayer::next()
{

	if ( currentContainer->size() > 0 )
	{
		sound->getAudioSystem()->fadeOut(id, 0.0f);

		currentSong++;
		if ( currentSong == currentContainer->end() )
			currentSong = currentContainer->begin();
		play((*currentSong));

	}
}

void MusicPlayer::previous()
{
	if ( currentContainer->size() > 0 )
	{	

		sound->getAudioSystem()->fadeOut(id, 0.0f);
		if ( currentSong == currentContainer->begin() )
			currentSong = currentContainer->end();
		currentSong--;
		play((*currentSong));
	}
}

void MusicPlayer::tellCurrentPlaylist(const std::string& c)
{
	Songs::iterator it = songs.find(c);
	if ( it != songs.end() )
	{
		stop();

		currentContainer = &it->second;
		nameOfCurrentContainer = it->first;

		currentSong = it->second.begin();
	}
}


void MusicPlayer::setVolume(const float volume)
{
	this->volume = volume * 0.3f;

	sound->getAudioSystem()->setVolume(id, this->volume * sound->getMasterVolume());
}

const float MusicPlayer::getVolume() const
{
	return volume / 0.3f;
}

const std::string MusicPlayer::getNameOfCurrentPlaylist() const
{
	return nameOfCurrentContainer;
}


void MusicPlayer::mute()
{
	muted = true;
	sound->getAudioSystem()->mute(this->id);
}

void MusicPlayer::unmute()
{
	muted = false;
	sound->getAudioSystem()->unmute(this->id);
}

const bool MusicPlayer::isMuted() const
{
	return muted;
}

void MusicPlayer::lowerVolume()
{
	oldVolume = getVolume();

	setVolume(getVolume() *0.2f);

}

void MusicPlayer::resetVolume()
{
	setVolume(oldVolume);
}