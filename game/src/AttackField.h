
#ifndef ATTACK_FIELD_H
#define ATTACK_FIELD_H

#include "PotentialField.h"
class AIComponent;

class AttackField : public PotentialField
{

public:
	AttackField(AIComponent* component = 0);
	virtual ~AttackField();

	float calcPotential(Vector2 position);

private:
	AIComponent* component;
};

#endif