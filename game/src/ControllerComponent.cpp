
#include "ControllerComponent.h"
#include "CoreComponent.h"
#include "Object.h"
#include "Events.h"
#include "PhysicsComponent.h"
#include "ControllerSubsystem.h"
#include "GameHandler.h"

ControllerComponent::ControllerComponent( Subsystem* subsystem )
	: Component( subsystem ), activeSlot(-1), blockSkills(0)
{
	physcomponent	= NULL;
	velocity		= Vector3(0.0f, 0.0f, 0.0f);
	speed			= 0.8f;

	mousePosition	= Vector3(0.0f, 0.0f, 0.0f);
	cooldown = 0;

	stunTicker = 0;
}

ControllerComponent::~ControllerComponent()
{
}

void ControllerComponent::receive(KeyEvent* key)
{
	//std::cout << "ControllerComponent received a KeyEvent" << std::endl;
}

void ControllerComponent::receive(KeyBindEvent* keyBind)
{
	keyStates[keyBind->getName()] = keyBind->getState();
	
	/*
	if( keyStates["attack"] ) // Good for stress testing skills and triggers
	{
		CoreComponent * comp = object->findComponent<CoreComponent>();
 		comp->activateModule(activeSlot);
	}*/

	if (keyBind->getState())
	{
		std::string name = keyBind->getName();
		int newSlot = -1;
		CoreComponent * comp = object->findComponent<CoreComponent>();

		if( name.find( "slot" ) != std::string::npos && name.size() == 5 )
		{
			newSlot = name[4] - 48;	// sym for '0' is 48
		}

		if (name == "nextSlot")
		{
			newSlot = activeSlot + 1;
			while (!comp->getModule( newSlot ))
			{
				newSlot++;
				if (newSlot > 10)
				{
					newSlot = -1;
					break;
				}
			}
		}
		else if (name == "prevSlot")
		{
			newSlot = activeSlot - 1;
			while (!comp->getModule( newSlot ))
			{
				newSlot--;
				if (newSlot < 0)
					break;
			}
		}


		if( newSlot >= 0  && newSlot < comp->getSkillSlots() )
		{
			if( comp->getModule( newSlot ) )
			{
				EventLua::Object selectSlot = subsystem->getGameHandler()->getScript()->getGlobals().get("selectSlot");
				activeSlot = newSlot;
				selectSlot( activeSlot );
			}
		}
		
		if (! subsystem->getGameHandler()->getKey(17) && stunTicker <= 0)
		{
			if (name.find("act") != std::string::npos && name.size() == 4)
			{
				int slot = name[3] - 48;

				if (comp->getModule(slot)!= 0 && cooldown == 0 && blockSkills <= 0)
				{
					object->send(&ActivateSkillEvent(slot, true, object));
					cooldown = 15; // 0.25 s
				}
			}

			if (keyBind->getName() == "attack")
			{
				if (comp->getModule(activeSlot) != 0 && cooldown == 0 && blockSkills <= 0)
				{
					object->send(&ActivateSkillEvent(activeSlot, true, object));
					cooldown = 15; // 0.25 s
				}
			}
		}
	}
	/*
	Vector3 right = toMiddle.cross(Vector3(0, 1, 0));

	if( keyStates["up"] )
		newVel -= toMiddle;

	if( keyStates["down"] )
		newVel += toMiddle;

	if( keyStates["left"] )
		newVel -= right;

	if( keyStates["right"] )
		newVel += right;
	*/

	updateMovement();
}

void ControllerComponent::setSpeed(float newSpeed)
{
	speed = newSpeed;
	updateMovement();
}

void ControllerComponent::updateMovement()
{
	if( !activated )
		return;

	Vector3 newVel( 0.0f, 0.0f, 0.0f );
	if (stunTicker <= 0)
	{
		if( keyStates["up"] )
			newVel.z += 1.0f;

		if( keyStates["down"] )
			newVel.z -= 1.0f;

		if( keyStates["left"] )
			newVel.x -= 1.0f;

		if( keyStates["right"] )
			newVel.x += 1.0f;
	}

	if (newVel != velocity)
	{
		velocity = newVel;
		Vector3 temp = velocity;
		if(temp.lengthNonSqrt() > 0)
		{
			temp.normalize();
			//temp = temp * speed;
		}
		MoveEvent move(temp, speed);
		object->sendInternal(&move);
	}
}

void ControllerComponent::receive(MouseEvent* mouse)
{
	//std::cout << "ControllerComponent received a MouseEvent" << std::endl;
}

void ControllerComponent::receive(EffectEvent * ev)
{
	if (ev->isEnd())
	{
		speed -= ev->getEffect()->speedChange;
		if (ev->getEffect()->blockSkills)
		{
			blockSkills--;
		}

		Vector3 temp = velocity;
		if(temp.lengthNonSqrt() > 0)
		{
			temp.normalize();
			//temp = temp * speed;
		}
		physcomponent->receive(&MoveEvent(temp, speed));

		return;
	}
	else if (ev->isFirst())
	{
		speed += ev->getEffect()->speedChange;
		if (ev->getEffect()->blockSkills)
		{
			blockSkills++;
		}
	}

	if (stunTicker < ev->getEffect()->stuntime)
	{
		stunTicker = ev->getEffect()->stuntime;
		
		setSpeed(speed); // So that it updates the speed
		if( ev->isRemarkable() )
		{
			Vector3 physpos = physcomponent->position;
			subsystem->getGameHandler()->onSkillEffect(0, true, false, false, 0, physpos.x, physpos.y, physpos.z, false);
		}
	}

	Vector3 temp = velocity;
	if(temp.lengthNonSqrt() > 0)
	{
		temp.normalize();
		//temp = temp * speed;
	}
	physcomponent->receive(&MoveEvent(temp, speed));
	// TODO: Send new speed.
}

void ControllerComponent::receive(QueryEvent<Vector3> * qv)
{
	switch(qv->getType())
	{
	case LOOK_AT:
		qv->setData(mousePosition + physcomponent->offset, 1);
		break;
	default:
		break;
	}
}

void ControllerComponent::receive( KillPlayerEvent* ev )
{
	Component::receive(ev);

	clearStates();
}

void ControllerComponent::receive( RespawnPlayerEvent* ev )
{
	Component::receive(ev);

	clearStates();
}

void ControllerComponent::update()
{
	cooldown--;
	if (cooldown < 0)
		cooldown = 0;

	if (stunTicker == 1)
	{
		stunTicker = 0;
		updateMovement();
	}

	stunTicker--;
	if (stunTicker < 0)
		stunTicker = 0;

	if(physcomponent)
	{
		
		toMiddle = physcomponent->position;
		toMiddle.normalize();
		//physcomponent->receive(&PushEvent(temp));
	}
}

void ControllerComponent::clearStates()
{
	keyStates.clear();
	updateMovement();
}

void ControllerComponent::setActiveSlot( int slot )
{
	activeSlot = slot;
}

void ControllerComponent::bindObject(Object* obj)
{
	Component::bindObject(obj);
	/*
	for(unsigned int i = 0; i < keybinds.size(); ++i)
	{
		object->subscribe(keybinds[i]);
	}
	*/
}

void ControllerComponent::unbindObject()
{
	Component::unbindObject();

	physcomponent = NULL;
}

bool ControllerComponent::subscribe()
{
	if(!object)
		return false;

	for(unsigned int i = 0; i < keybinds.size(); ++i)
	{
		if(!object->subscribe(keybinds[i]))
			return false;
	}

	return true;
}

void ControllerComponent::bindToComponents()
{
	physcomponent = object->findComponent<PhysicsComponent>();
}

void ControllerComponent::setMousePosition(const Vector3& position)
{
	if (stunTicker > 0)
		return;

	mousePosition = position;

	float deltax = mousePosition.x - physcomponent->position.x;
	float deltay = mousePosition.z - physcomponent->position.z;
	float angle	 = atan2(-deltay, deltax);

	// For some reasons, quaternions behave differently when the angle to rotate with
	// is negative than an ordinary matrix rotation. Instead of continuing rotation
	// on the other half of the unit circle, it backs. Therefore, if the angle becomes negative,
	// we have to rotate a whole lap on the unit circle, 2PI, and THEN back the amount.
	if( angle < 0.0f )
	{
		angle = 2 * (float)D3DX_PI + angle;
	}

	physcomponent->rotation.rotationAxis(Vector3(0,1,0), angle);
}

const Vector3 & ControllerComponent::getMousePosition() const
{
	return mousePosition;
}

const Vector3 ControllerComponent::getDirection() const
{
	return toMiddle;
}