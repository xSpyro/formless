
#ifndef AISUBSYSTEM_H
#define AISUBSYSTEM_H

#include "Subsystem.h"
class AIComponent;

/*
 * The specific tasks are described in the corresponding component header file.
 */

class AISubsystem : public Subsystem
{
	typedef std::vector<AIComponent> Components;

public:
	AISubsystem( GameHandler* gameHandler = NULL, int size = 1000);
	virtual ~AISubsystem();

	Component*	createComponent(EventLua::Object script);
	Component*	getComponent();
	void		update();

	void clear();

private:
	Components components;
};

#endif