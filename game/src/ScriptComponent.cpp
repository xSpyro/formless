
#include "ScriptComponent.h"
#include "Events.h"
#include "Object.h"
#include "ScriptEnvironment.h"

#include "PhysicsComponent.h" // TODO: REMOVE THIS FUCCKER BITCHES!!!!

ScriptComponent::ScriptComponent( Subsystem* subsystem )
	: Component( subsystem )
{
}

ScriptComponent::~ScriptComponent()
{
}

void ScriptComponent::update()
{
}

void ScriptComponent::receive(CreatedEvent * ev)
{
	if (object == ev->getSender())
	{
		EventLua::Object * f = callbacks.getFunction("onCreated", object);
		if (f != 0)
		{
			f->call();
			callbacks.functionDone();
		}
	}
	else if (isAlive() && activated)
	{
		EventLua::Object * f = callbacks.getFunction("onOtherCreated", object);
		if (f != 0)
		{
			int id = env->addData(ev->getSender());
			f->call(id);
			callbacks.functionDone();
		}
	}
}

void ScriptComponent::receive(KeyEvent * ev)
{
	if (!isAlive() && !activated)
		return;

	EventLua::Object * f = callbacks.getFunction("onKey", object);
	if (f != 0)
	{
		f->call(ev->getState());
		callbacks.functionDone();
	}
}

void ScriptComponent::receive(KeyBindEvent * keyBind)
{
	if (!isAlive() && !activated)
		return;

	EventLua::Object * f = callbacks.getFunction("onKeyBind", object);
	if (f != 0)
	{
		f->call(keyBind->getName().c_str(), keyBind->getState());
		callbacks.functionDone();
	}
}

void ScriptComponent::receive(DestroyEvent * ev)
{
	if (object == ev->getSender())
	{
		EventLua::Object * f = callbacks.getFunction("onDestroy", object);
		if (f != 0)
		{
			f->call();
			callbacks.functionDone();
		}
	}
	else if (isAlive())
	{
		EventLua::Object * f = callbacks.getFunction("onOtherDestroy", object);
		if (f != 0)
		{
			int id = env->addData(ev->getSender());
			f->call(id);
			callbacks.functionDone();
		}
	}
}

void ScriptComponent::receive(MouseEvent * ev)
{
	if (!isAlive() && !activated)
		return;

	EventLua::Object * f = callbacks.getFunction("onMouse", object);
	if (f != 0)
	{
		f->call(ev->getDx(), ev->getDy()); // TODO: Add for normalized and vector
		callbacks.functionDone();
	}
}

void ScriptComponent::receive(TimerEvent * ev)
{
	if (!isAlive() && !activated)
		return;

	EventLua::Object * f = callbacks.getFunction("onTimer", object);
	if (f != 0)
	{
		f->call(ev->getID());
		callbacks.functionDone();
	}
}

void ScriptComponent::receive(TriggerEvent * ev)
{
	if (!isAlive() && !activated)
		return;

	EventLua::Object * f = 0;

	if (ev->entering())
	{
		if (ev->getException() != TRIGGER_NONE)
			f = callbacks.getFunction("onTriggerExceptionEnter", object);
		else
			f = callbacks.getFunction("onTriggerEnter", object);
	}
	else
	{
		if (ev->getException() != TRIGGER_NONE)
			f = callbacks.getFunction("onTriggerExceptionExit", object);
		else
		f = callbacks.getFunction("onTriggerExit", object);
	}
	if (f != 0)
	{
		if (ev->getException() != TRIGGER_NONE)
		{
			f->call(-1, 1); // TODO: Right now 1 is always terrain... Should be changed
		}
		else
		{
			Vector3 dir = ev->getResult().normal;
			if(ev->getOther())
				f->call(env->addData(ev->getOther()), ev->getResult().getDist(), dir.x, dir.y, dir.z);
			else
				f->call(-1, ev->getResult().getDist(), dir.x, dir.y, dir.z);

		}
		
		callbacks.functionDone();
	}
}

void ScriptComponent::receive(LinkEvent * link)
{
	if (!isAlive() && !activated)
		return;

	EventLua::Object * f = callbacks.getFunction("onLink", object);

	if (f)
	{
		f->call(env->addData(link->getOwner()), env->addData(link->getTarget()));
	}

	callbacks.functionDone();
}

void ScriptComponent::receive(DamageEvent * damage)
{
	if (!isAlive() && !activated)
		return;

	EventLua::Object rtn;
	if (damage->getDefender() == object)
	{
		EventLua::Object * f = callbacks.getFunction("onDamage", object);
		int attacker = env->addData(damage->getAttacker());

		if (f)
		{
			rtn = f->call(damage->damage.getBase(), damage->damage.getModifier(), damage->damage.getBase(), damage->damage.getModifier(), attacker);
			callbacks.functionDone();
		}
	}
	else
	{
		EventLua::Object * f = callbacks.getFunction("onOtherDamage", object);
		int attacker = env->addData(damage->getAttacker());
		int defender = env->addData(damage->getDefender());

		if (f)
		{
			rtn = f->call(defender, damage->damage.getBase(), damage->damage.getModifier(), damage->damage.getBase(), damage->damage.getModifier(), attacker);
			callbacks.functionDone();
		}
	}

	if (rtn)
	{
		damage->damage.changeModifier(rtn.get("modifier").queryFloat(1.0f), true);
		damage->damage.changeOffset(rtn.get("offset").queryFloat(0.0f));
	}
}

void ScriptComponent::bindObject(Object* obj)
{
	object = obj;
}


void ScriptComponent::receive(EffectEvent * effect)
{
	if (!isAlive() && !activated)
		return;

	if (object == effect->getSender())
	{
		EventLua::Object * f = callbacks.getFunction("onEffect", object);
		if (f != 0)
		{
			f->call();
			f->call(effect->getEffect()->knockback, effect->getEffect()->stuntime, effect->getEffect()->damage, effect->getEffect()->particleDamage);
			callbacks.functionDone();
		}
	}
	else
	{
		EventLua::Object * f = callbacks.getFunction("onOtherEffect", object);
		if (f != 0)
		{
			f->call();
			f->call(env->addData(effect->getSender()), effect->getEffect()->knockback, effect->getEffect()->stuntime, effect->getEffect()->damage, effect->getEffect()->particleDamage);
			callbacks.functionDone();
		}
	}
}

void ScriptComponent::receive(KillPlayerEvent * kill)
{
	if (object == kill->getSender())
	{
		EventLua::Object * f = callbacks.getFunction("onKilled", object);
		if (f != 0)
		{
			f->call();
			callbacks.functionDone();
		}
	}
	else if (isAlive())
	{
		EventLua::Object * f = callbacks.getFunction("onOtherDestroy", object);
		if (f != 0)
		{
			int id = env->addData(kill->getSender());
			f->call(id, true);
			callbacks.functionDone();
		}

		f = 0;
		f = callbacks.getFunction("onOtherKilled", object);
		if (f != 0)
		{
			int id = env->addData(kill->getSender());
			f->call(id);
			callbacks.functionDone();
		}
	}

	Component::receive(kill);
}