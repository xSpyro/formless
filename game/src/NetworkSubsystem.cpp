
#include "NetworkSubsystem.h"
#include "NetworkComponent.h"
#include "MemLeaks.h"

#include "GameHandler.h"
#include "Object.h"

#include "RawPacket.h"
#include "CommonPackets.h"

#include "NetworkComponentClient.h"
#include "NetworkComponentDummy.h"

#include "CoreComponent.h"
#include "AIComponent.h"

#include "Events.h"

NetworkSubsystem::NetworkSubsystem( GameHandler* gameHandler, int size )
	: Subsystem( gameHandler, "NetworkSubsystem" )
{
}

NetworkSubsystem::~NetworkSubsystem()
{
	clear();
}

Component* NetworkSubsystem::createComponent(EventLua::Object script)
{
	return NULL;
}
//
//Component* NetworkSubsystem::getComponent()
//{
//	for( Components::iterator it = components.begin(); it != components.end(); ++it )
//	{
//		if( !it->second->isBound() )
//		{
//			it->reset();
//			return it->second;
//		}
//	}
//	/*if (components.size() == components.capacity())
//		return 0;*/
//
//	components.push_back( NetworkComponent( this ) );
//	return &components.back();
//}

Component* NetworkSubsystem::getComponent(unsigned int id)
{
	Components::iterator it = components.find(id);

	if(it != components.end())
	{
		return it->second;
	}
	else
	{
		NetworkComponent* comp = New NetworkComponent(this);
		components[id] = comp;
		return comp;
	}
}

NetworkComponent* NetworkSubsystem::getClientComponent(unsigned int id)
{
	Components::iterator it = components.find(id);

	if(it != components.end())
	{
		return it->second;
	}
	else
	{
		NetworkComponent* comp = New NetworkComponentClient(this, id);
		components[id] = comp;
		return comp;
	}
}

NetworkComponent* NetworkSubsystem::getDummyComponent(unsigned int id)
{
	Components::iterator it = components.find(id);

	if(it != components.end())
	{
		return it->second;
	}
	else
	{
		NetworkComponent* comp = New NetworkComponentDummy(this, id);
		components[id] = comp;
		return comp;
	}
}

unsigned int NetworkSubsystem::getID()
{
	return client_id;
}

std::string NetworkSubsystem::getName( unsigned int netId )
{
	return "Unknown";
}

void NetworkSubsystem::update()
{
	for( Components::iterator it = components.begin(); it != components.end(); it++ )
	{
		if(it->second->isBound() && it->second->activated)
			it->second->update();
	}
}

void NetworkSubsystem::clear()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it)
	{
		delete it->second;
		it->second = NULL;
	}

	components.clear();
}

NetworkComponent * NetworkSubsystem::processCreatePacket(CreatePacket & packet)
{
	NetworkComponent * out = NULL;

	std::string object = packet.object;

	if(object == "AIPlayer")
	{
		out = processAICreatePacket(packet);
	}
	else
	{
		out = processDefaultCreatePacket(packet);
	}

	return out;
}

NetworkComponent * NetworkSubsystem::processDefaultCreatePacket(CreatePacket & packet)
{
	// first see if the id is already registered
	// this means that this object most likely is ourself (client player character)

	Vector3 position = Vector3(packet.x, packet.y, packet.z);
	NetworkComponent * out = NULL;

	Components::iterator i = components.find(packet.uid);
	if (i != components.end())
	{
		// only change position
		PositionEvent ev(position);
		i->second->getObject()->sendInternal(&ev);

		CoreComponent* core = i->second->getObject()->findComponent<CoreComponent>();
		if (core)
		{
			core->team = packet.team;
		}

		out = i->second;

		i->second->getObject()->removeCompanion();
	}
	else
	{
		Object * creator = NULL;

		// find creator
		Components::iterator j = components.find(packet.creator);
		if (j != components.end())
		{
			creator = j->second->getObject();
		}

		// we create the object and mount a dummy network component
		Object * object = getGameHandler()->createObjectExt(packet.object, position, position, creator, NULL);

		if (object)
		{
			out = getDummyComponent(packet.uid);
			object->addComponent(out);

			CoreComponent* core = object->findComponent<CoreComponent>();
			if (core)
			{
				core->team = packet.team;

				if( core->team == 0 )
				{
					KillPlayerEvent killPlayer;
					killPlayer.setCount(false);

					object->sendInternal(&killPlayer);
				}
			}

			object->removeCompanion();
		}
	}

	return out;
}

NetworkComponent* NetworkSubsystem::processAICreatePacket(CreatePacket & packet)
{
	Vector3 position = Vector3(packet.x, packet.y, packet.z);
	NetworkComponent * out = NULL;

	Components::iterator i = components.find(packet.uid);
	if (i != components.end())
	{
		// only change position
		PositionEvent ev(position);
		i->second->getObject()->sendInternal(&ev);

		CoreComponent* core = i->second->getObject()->findComponent<CoreComponent>();
		if (core)
		{
			core->team = packet.team;
		}

		out = i->second;

		Components::iterator master = components.find((int)(packet.uid * 0.1f));
		if(master != components.end())
		{
			master->second->getObject()->giveCompanion(i->second->getObject());
			AIComponent* ai = i->second->getObject()->findComponent<AIComponent>();
			if(ai)
			{
				ai->setMaster(master->second->getObject());
			}
		}
	}
	else
	{
		Object * creator = NULL;

		// find creator
		Components::iterator j = components.find(packet.creator);
		if (j != components.end())
		{
			creator = j->second->getObject();
		}

		// we create the object and mount a dummy network component
		Object * object = getGameHandler()->createObjectExt(packet.object, position, position, creator, NULL);

		if (object)
		{
			out = getDummyComponent(packet.uid);
			object->addComponent(out);

			CoreComponent* core = object->findComponent<CoreComponent>();
			if (core)
			{
				core->team = packet.team;

				if( core->team == 0 )
				{
					KillPlayerEvent killPlayer;
					killPlayer.setCount(false);

					object->sendInternal(&killPlayer);
				}
			}

			Components::iterator master = components.find((int)(packet.uid * 0.1f));
			if(master != components.end())
			{
				master->second->getObject()->giveCompanion(object);
				AIComponent* ai = object->findComponent<AIComponent>();
				if(ai)
				{
					ai->setMaster(master->second->getObject());
				}
			}
		}
	}

	return out;
}