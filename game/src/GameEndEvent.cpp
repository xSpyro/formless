#include "GameEndEvent.h"
#include "EventEnum.h"
#include "Object.h"

GameEndEvent::GameEndEvent() : Event(EVENT_GAME_END, sender)
{}

GameEndEvent::~GameEndEvent()
{}


void GameEndEvent::sendTo(Object * o)
{
	o->receive<GameEndEvent>(this);
}