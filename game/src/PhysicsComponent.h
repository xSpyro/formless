
#ifndef PHYSICSCOMPONENT_H
#define PHYSICSCOMPONENT_H

#include "Framework.h"
#include "Component.h"
class Terrain;
class CoreComponent;

/*	
 *	The physics component handles positioning and movement in the game world.
 *	If an object has no physics component, it cannot move and/or collide.
 */

class PhysicsComponent : public Component
{
public:

	PhysicsComponent( Subsystem* subsystem = NULL );
	virtual ~PhysicsComponent();

	void	receive(PushEvent * move);
	void	receive(CreatedEvent * create);
	void	receive(PositionEvent * pos);
	void	receive(RotationEvent * rot);
	void	receive(ScaleEvent * scale);
	void	receive(EffectEvent * effect);
	void	receive(MoveEvent * move);
	void	receive(FireEvent * fire);
	void	receive(KillPlayerEvent * kill);
	void	receive(RespawnPlayerEvent * res);
	void	receive(QueryEvent<Vector3> * qv);
	void	receive(QueryEvent<Quaternion> * qq);
	void	receive(LinkEvent * link);

	void	applyOffset();
	bool	collision(PhysicsComponent * comp, IntersectResult * res = 0);
	bool	collisionBox(PhysicsComponent * comp, IntersectResult * res = 0);
	void 	handleCollision(PhysicsComponent * comp, IntersectResult * res);
	void	handleRepulsion(PhysicsComponent * comp, IntersectResult * res);
	void	update();
	void	updateMatrixAndVolumes();

	bool	updateYPosition(Terrain* heightmap);	//Returns true if over the height map, false if outside (or below)

	void	bindToComponents();

	void	setPosition(Vector3 pos);
	void	setRotation(Quaternion rot);
	void	setScale(Vector3 scale);

	Vector3	getPosition();
	Quaternion getRotation();
	Vector3 getScale();

	void	translateWith(Vector3 translation);
	void	rotateWith(Vector3 axis, float angle);
	void	scaleWith(Vector3 rate);

	bool	load(std::istream &stream, int label);
	bool	save(std::ostream &stream);

	Vector3 getActualVelocity(); // Returns velocity + movement * speed

	bool	useBox();

	void	enableSlowdown();
	void	disableSlowdown();

	bool isLinked();
	bool isStatic();
	void setStatic(bool stat = true);

	void disableArenaCollision();
	bool collideWithArena() const;
	
	void setParentComponent(Component * parent);

	PhysicsComponent * getTarget();

public:

	Vector3	position;
	Vector3 destination;	//The actual destination position
	Vector3	velocity;		//The vector marking direction
	
	bool useGravity;
	bool useDest;

	Vector3	scale;

	Quaternion rotation;
	Quaternion boxRotOffset;
	//Vector3	rotation;
	//float	angle;			//The lookat angle around the axis (0,1,0) in radians

	Vector3 offset;
	Vector3 sphereOffset;
	Vector3 boxOffset;

	Matrix	matrix;
	Sphere	sphere;
	OBB box;
	bool	trigger;

	float 	speed;
	float 	hoverPower;
	float 	repulsion;

	Vector3 gravity;
	Vector3 movement;

	bool unmovable;
	bool useProjectileRotation;

	bool arenaCollisions;

private:
	void reset();
	void respawn(Vector3 pos);
	void updateLinkBox();

private:
	float	realSlowdown;
	float	currentSlowdown;
	bool	useRealSlowdown;


	
	float	maxSpeed;

	bool				linked;
	PhysicsComponent*	ownerPhys;
	PhysicsComponent*	targetPhys;
	CoreComponent*		ownerCore;
	CoreComponent*		targetCore;
};

#endif