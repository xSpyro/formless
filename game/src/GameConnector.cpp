
#include "NetworkSubsystem.h"

#include "GameConnector.h"
#include "GameHandler.h"
#include "EventHandler.h"
#include "ScriptEnvironment.h"
#include "EventEnum.h"
#include "Events.h"
#include "Object.h"
#include "PhysicsComponent.h"
#include "ControllerComponent.h"
#include "AnimationComponent.h"
#include "TimerComponent.h"
#include "SkillComponent.h"
#include "CoreComponent.h"
#include "LuaFileManager.h"
#include "SoundSubsystem.h"
#include "PhysicsSubsystem.h"
#include "NetworkComponent.h"
#include "AIComponent.h"

#include "MemLeaks.h"

using namespace GameConnector;


#define FAIL_CHECK_LOG_RTN(o, msg, rtn) if (o == 0) {luaLogger.alert(msg);return rtn;}
#define FAIL_CHECK_LOG(o, msg) if (o == 0) {luaLogger.alert(msg);return;}
#define FAIL_CHECK_LOG_CONT(o, msg) if (o == 0) {luaLogger.alert(msg);continue;}

GameHandler * gameHandler;
Logger luaLogger("Lua-Game"); 

void GameConnector::setGame(GameHandler * game)
{
	env = New ScriptEnvironment(50);
	gameHandler = game;

	luaLogger.info("GameHandler set");

	EventLua::Object g = game->getScript()->getGlobals();

	EventLua::Object luaTypes = game->getScript()->newTable("TYPE");
	luaTypes["UNKOWN"] = TYPE_UNKOWN;
	luaTypes["SCRIPT"] = TYPE_SCRIPT;
	luaTypes["OBJECT"] = TYPE_OBJECT;
	luaTypes["TRIGGER"] = TYPE_TRIGGER;
	luaTypes["SKILL"] = TYPE_SKILL;

	g.bind("create", &create);
	g.bind("destroy", &destroy);
	g.bind("root", &getRoot);
	g.bind("parent", &getParent);
	g.bind("creator", &getCreator);
	g.bind("owner", &getOwner);
	g.bind("bind", &setKeyBind);
	g.bind("assign", &setAssignable);
	g.bind("getActiveAssigns", &getActiveAssignables);
	g.bind("getAssignsFor", &getAssignablesFor);
	g.bind("setPos", &setPosition);
	g.bind("setRot", &setRotation);
	g.bind("setScale", &setScale);
	g.bind("getLinkTarget", &getLinkTarget);
	g.bind("subscribeKeyBind", &subscribeKeyBind);
	g.bind("subscribeEvent", &subscribeEvent);
	g.bind("subscribeKeyEvents", &subscribeKeyEvents);
	g.bind("move", &move);
	g.bind("push", &push);
	g.bind("fire", &fire);
	g.bind("link", &link);
	g.bind("isMonster", &isMonster);
	g.bind("isEffecting", &isEffecting);
	g.bind("effect", &effect);
	g.bind("repeatEffect", &repeatEffect);
	g.bind("removeEffect", &removeEffect);
	g.bind("setParent", &setParent);
	g.bind("getKey", &getKey);
	g.bind("getKeyBind", &getKeyBind);
	g.bind("getTeam", &getTeam);
	g.bind("getLife", &getLife);
	g.bind("getMana", &getMana);
	g.bind("worldMouseX", &getMouseX);
	g.bind("worldMouseY", &getMouseY);
	g.bind("worldMouseZ", &getMouseZ);
	g.bind("getDirToMouse", &getDirToMouse);    
	g.bind("getDir", &getDir);
	g.bind("random", &getRand);
	g.bind("include", &loadScript);
	g.bind("attach", &attach);
	g.bind("detach", &detach);
	g.bind("enableSlowdown", &enableSlowdown);
	g.bind("disableSlowdown", &disableSlowdown);
	g.bind("playSound", &playSound);
	g.bind("stopSound", &stopSound);
	g.bind("StopMovement", &stopMovement);
	g.bind("forgetTrigger", &forgetTrigger);
	g.bind("forgetAllTriggers", &forgetAllTriggers);
	g.bind("restartTimer", &restartTimer);
	g.bind("checkTimer", &checkTimer);
	g.bind("reloadLuaFiles", &reloadLuaFiles);
	g.bind("registerTemplate", &registerTemplate);
	g.bind("pauseAnimation", &pauseAnimation);
	g.bind("resumeAnimation", &resumeAnimation);

	g.bind("sendAICommand", &sendAICommand);

	File file;
	if (!FileSystem::get( "scripts/utils.lua", &file ))
	{
		luaLogger.alert("Failed to load file: ", "scripts/utils.lua", ".");
		return;
	}
	if( !gameHandler->getScript()->execute( file.data, file.size ) )
	{
		std::cout << gameHandler->getScript()->getLastError() << ". In file: " << "scripts/utils.lua" <<std::endl;
	}

	game->loadItems();
}

// Moves according to dir's look at if dir >= 0
void GameConnector::move(int id, float x, float y, float z, int dir, bool instant)
{
	Object * o = env->getData(id);
	Object * f = env->getData(dir);
	QueryEvent<Vector3> q(LOOK_AT);
	FAIL_CHECK_LOG(o, "Move failed, object null");
	if (f != 0)
	{
		f->sendInternal(&q);
		PhysicsComponent* fp = f->findComponent<PhysicsComponent>();

		if(q.isSet() && fp != 0)
		{
			Vector3 look = q.getData();
			Vector3 dir = look - fp->position;
			dir.normalize();
			Vector3 move = dir * x;
			move += dir.cross(Vector3(0,1,0)) * z;
			move.y += y;

			float speed = move.length();
			move = move / speed;

			o->send(&MoveEvent(move, speed, instant));

			PhysicsComponent* op = o->findComponent<PhysicsComponent>();

			if(op)
			{
				float deltax = move.x;
				float deltay = move.z;
				float angle	 = atan2(-deltay, deltax);

				// For some reasons, quaternions behave differently when the angle to rotate with
				// is negative than an ordinary matrix rotation. Instead of continuing rotation
				// on the other half of the unit circle, it backs. Therefore, if the angle becomes negative,
				// we have to rotate a whole lap on the unit circle, 2PI, and THEN back the amount.
				if( angle < 0.0f )
				{
					angle = 2 * (float)D3DX_PI + angle;
				}

				op->rotation.rotationAxis(Vector3(0,1,0), angle);
			}

			return;
		}
		luaLogger.notice("Move could not find PhysicsComponent of origin object, switches to non-origin version");
	}
	else if (dir >= 0)
	{
		luaLogger.notice("Move could not find origin object, switches to non-origin version");
	}
	Vector3 move(x, y, z);
	float speed = move.length();
	move = move / speed;
	o->send(&MoveEvent(move, speed, instant));
}

void GameConnector::push(int id, float x, float y, float z, int dir)
{
	Object * o = env->getData(id);
	Object * f = env->getData(dir);
	QueryEvent<Vector3> q(LOOK_AT);
	FAIL_CHECK_LOG(o, "Push failed, object null");
	if (f != 0)
	{
		f->sendInternal(&q);
		PhysicsComponent* fp = f->findComponent<PhysicsComponent>();

		if(q.isSet() && fp != 0)
		{
			Vector3 look = q.getData();
			Vector3 dir = look - fp->position;
			dir.normalize();
			Vector3 move = dir * x;
			move += dir.cross(Vector3(0,1,0)) * z;
			move.y += y;

			o->send(&PushEvent(move));

			PhysicsComponent* op = o->findComponent<PhysicsComponent>();

			return;
		}
		luaLogger.notice("Push could not find PhysicsComponent of origin object, switches to non-origin version");
	}
	else if (dir >= 0)
	{
		luaLogger.notice("Push could not find origin object, switches to non-origin version");
	}
	o->send(&PushEvent(Vector3(x, y, z)));
}

void GameConnector::fire(int id, int dir, bool high, float offsetX, float offsetY, float offsetZ)
{
	Object * caller = env->getCaller();
	FAIL_CHECK_LOG(caller, "Fire failed, object null");

	SkillComponent * sc = caller->findComponent<SkillComponent>();
	FAIL_CHECK_LOG(sc, "Fire failed, object null");

	float range = sc->effect.range;

	Object * o = env->getData(id);
	Object * f = env->getData(dir);
	FAIL_CHECK_LOG(o, "Fire failed, object null");
	FAIL_CHECK_LOG(f, "Fire failed, origin null");

	Vector3 destination;

	ControllerComponent * cc = f->findComponent<ControllerComponent>();
	if( cc )
	{
		destination = cc->getMousePosition();
	}
	else
	{
		AIComponent * ai = f->findComponent<AIComponent>();
		FAIL_CHECK_LOG(ai, "Fire failed, object has neither a ControllerComponent, nor an AIComponent.");

		destination = ai->getTargetPosition();
	}

	
	destination.x += offsetX;
	destination.y += offsetY;
	destination.z += offsetZ;

	o->send(&FireEvent(destination, range, high));
}

void GameConnector::link(const char * linkName, int ownerID, int targetID)
{
	// TODO: change so that we have a prepare link event instead...
	NetworkSubsystem * net = (NetworkSubsystem *)gameHandler->getSubsystem("Network");
	if (net)
	{
		if (!net->isServer())
			return;
	}
	FAIL_CHECK_LOG(linkName, "Link failed, no name on the link object.");

	Object * owner = env->getData(ownerID);
	Object * target = env->getData(targetID);

	FAIL_CHECK_LOG(owner, "Link failed, the owner could not be found.");
	FAIL_CHECK_LOG(target, "Link failed, the target could not be found.");

	Object * link = gameHandler->createObject(linkName, owner);
	

	FAIL_CHECK_LOG(link, "Link failed, the link object failed to be created.");
	
	link->sendInternal(&LinkEvent(link, owner, target, true));
	owner->sendInternal(&LinkEvent(link, owner, target, true));
	target->sendInternal(&LinkEvent(link, owner, target, true));

	NetworkComponent * own = owner->findComponent<NetworkComponent>();
	NetworkComponent * tar = target->findComponent<NetworkComponent>();

	if (!own || !tar)
	{
		luaLogger.alert("Trying to link, but one or both of the items doesn't have a network component.");
		return;
	}

	net->onLink(linkName, own->getID(), tar->getID(), true);
}

void GameConnector::unlink(int linkId, int ownerId, int targetId)
{
	NetworkSubsystem * net = (NetworkSubsystem *)gameHandler->getSubsystem("Network");
	if (net)
	{
		if (!net->isServer())
			return;
	}

	Object * link = env->getData(linkId);
	Object * owner = env->getData(ownerId);
	Object * target = env->getData(targetId);

	if (linkId == -1 && ownerId == -1 && targetId != -1) // Unlink all links to target
	{
		FAIL_CHECK_LOG(target, "Failed to unlink all objects to a target, could not find target.");
		CoreComponent * core = target->findComponent<CoreComponent>();
		FAIL_CHECK_LOG(target, "Failed to unlink all objects to a target, could not find targets core.");

		std::map<Object *, Object *> links = core->getLinks();

		for (std::map<Object *, Object *>::iterator it = links.begin(); it != links.end(); ++it)
		{
			link = it->first;
			owner = it->second;

			FAIL_CHECK_LOG_CONT(link, "Unlink failed, a specific link could not find the link. Continuing through links.");
			FAIL_CHECK_LOG_CONT(owner, "Unlink failed, a specific link could not find the owner. Continuing through links.");
			
			link->sendInternal(&LinkEvent(link, owner, target, false));
			owner->sendInternal(&LinkEvent(link, owner, target, false));
			target->sendInternal(&LinkEvent(link, owner, target, false));

			NetworkComponent * own = owner->findComponent<NetworkComponent>();
			NetworkComponent * tar = target->findComponent<NetworkComponent>();

			if (!own || !tar)
			{
				luaLogger.alert("Trying to link, but one or both of the items doesn't have a network component.");
				continue;
			}

			net->onLink("", own->getID(), tar->getID(), false);
		}
		gameHandler->destroyObject(link);

		return;
	}

	if (owner && !link)
	{
		CoreComponent * core = owner->findComponent<CoreComponent>();
		FAIL_CHECK_LOG(core, "Failed to unlink, could not find the owners core when trying to fetch a it's link object(Link object was not inputted/valid).");
		link = core->getLink();
	}

	if (link)
	{
		if (!owner)
		{
			CoreComponent * core = link->findFamilyComponent<CoreComponent>();
			if (core)
				owner = core->getObject();
		}
		if (!target)
		{
			PhysicsComponent * temp = link->findComponent<PhysicsComponent>();
			FAIL_CHECK_LOG(temp, "Unlink failed, could not find links physics component");
			temp = temp->getTarget();
			target = temp->getObject();
		}
	}

	FAIL_CHECK_LOG(link, "Unlink failed, a specific link could not find the link.");
	FAIL_CHECK_LOG(owner, "Unlink failed, a specific link could not find the owner.");
	FAIL_CHECK_LOG(target, "Unlink failed, a specific link could not find the target.");

	link->sendInternal(&LinkEvent(link, owner, target, false));
	owner->sendInternal(&LinkEvent(link, owner, target, false));
	target->sendInternal(&LinkEvent(link, owner, target, false));

	gameHandler->destroyObject(link);

	NetworkComponent * own = owner->findComponent<NetworkComponent>();
	NetworkComponent * tar = target->findComponent<NetworkComponent>();

	if (!own || !tar)
	{
		luaLogger.alert("Trying to link, but one or both of the items doesn't have a network component.");
		return;
	}

	net->onLink("", own->getID(), tar->getID(), false);
}

int GameConnector::create(const char * name, EventLua::Object parent)
{
	if (env->isFull())
	{
		luaLogger.alert("Create failed. Environment is full");
		return -1;
	}
	else
	{
		Object * p = 0;
		if (parent.isBoolean())
		{
			if (parent.queryBoolean(false))
				p = env->getCaller();
		}
		else if (parent.isNumber())
		{
			p = env->getData(parent.queryInteger(-1));
		}
		
		Object * o = gameHandler->createObject(name, env->getCaller(), p);

		if (o == 0)
		{
			luaLogger.alert("Create failed. Unknown reason. (Obj: " , name, ")");
			return -1;
		}

		return env->addData(o);
	}
}

void GameConnector::registerTemplate(const char * name, EventLua::Object table)
{
	gameHandler->registerTemplate(name, table);
}

void GameConnector::restartTimer(int timerId, int objId /* = 0 */)
{
	Object * o = env->getData(objId);
	FAIL_CHECK_LOG(o, "Restart timer failed. Could not find object");

	TimerComponent * tc = o->findComponent<TimerComponent>();
	FAIL_CHECK_LOG(tc, "Restart timer failed. Could not find timer component");
	tc->restart(timerId);
}

bool GameConnector::destroy(int id)
{
	Object * o = env->getData(id);
	FAIL_CHECK_LOG_RTN(o, "Destroy failed. Could not find target", false);

	PhysicsComponent * phys = o->findComponent<PhysicsComponent>();

	if (phys && phys->isLinked())
	{
		unlink(id, -1, -1);
		return true;
	}

	gameHandler->destroyObject(o);
	return true;
}

void GameConnector::stopMovement(int id)
{
	Object * o = env->getData(id);
	FAIL_CHECK_LOG(o, "StopMovement failed. Could not find target");

	PhysicsComponent * phys = o->findComponent<PhysicsComponent>();
	FAIL_CHECK_LOG(o, "StopMovement failed. Could not find PhysicsComponent");
	phys->velocity = Vector3(0, 0, 0);
	phys->speed = 0;
}

void GameConnector::pauseAnimation(int id)
{
	Object * o = env->getData(id);
	FAIL_CHECK_LOG(o, "Pause animation failed. Could not find target");

	AnimationComponent * a = o->findComponent<AnimationComponent>();
	FAIL_CHECK_LOG(a, "Pause animation failed. Could not find AnimationComponent");

	a->pause();
}

void GameConnector::resumeAnimation(int id)
{
	Object * o = env->getData(id);
	FAIL_CHECK_LOG(o, "Resume animation failed. Could not find target");

	AnimationComponent * a = o->findComponent<AnimationComponent>();
	FAIL_CHECK_LOG(a, "Resume animation failed. Could not find AnimationComponent");

	a->resume();
}

void GameConnector::setAnimation(int id, const char * anim)
{
	Object * o = env->getData(id);
	FAIL_CHECK_LOG(o, "SetAnimation failed. Could not find target");

	AnimationEvent ae = AnimationEvent(anim);

	o->send(&ae);
}

void GameConnector::setParent(int target, int parent)
{
	Object * o = env->getData(target);
	Object * p = env->getData(parent);
	FAIL_CHECK_LOG(o, "Set parent failed. Could not find target");
	FAIL_CHECK_LOG(o, "Set parent failed. Could not find parent");

	p->attachObject(o);
}

int GameConnector::getRoot(int id)
{
	if (!env->getData(id))
		return -1;
	return env->addData(env->getData(id)->getRoot());
}

int GameConnector::getParent(int id)
{
	if (!env->getData(id))
		return -1;
	return env->addData(env->getData(id)->getParent());
}

int GameConnector::getCreator(int id)
{
	if (!env->getData(id))
		return -1;
	return env->addData(env->getData(id)->getCreator());
}

int GameConnector::getOwner(int id)
{
	Object * o = env->getCaller();
	if (o == 0)
		return -1;
	CoreComponent * core = o->findFamilyComponent<CoreComponent>();
	FAIL_CHECK_LOG_RTN(core, "Could not find a core in family(getOwner).", -1);
	
	return env->addData(core->getObject());
}

int GameConnector::getLinkTarget(int id)
{
	Object * o = env->getData(id);
	FAIL_CHECK_LOG_RTN(o, "Failed getLinkTarget, could not find caller.", -1);
	PhysicsComponent * phys = o->findComponent<PhysicsComponent>();
	FAIL_CHECK_LOG_RTN(phys, "Failed getLinkTarget, could not find PhysicsComponent.", -1);
	phys = phys->getTarget();
	FAIL_CHECK_LOG_RTN(phys, "Failed getLinkTarget, could not find target in PhysicsComponent.", -1);
	o = phys->getObject();
	FAIL_CHECK_LOG_RTN(phys, "Failed getLinkTarget, could not find object in target PhysicsComponent.", -1);

	return env->addData(o);
}

int GameConnector::getTeam(int id)
{
	Object * o = env->getData(id);

	if(o == NULL)
	{
		luaLogger.notice("Object at team request does not exist.");
	}
	else
	{
		CoreComponent * core = o->findComponent<CoreComponent>();
		if(core == NULL)
		{
			luaLogger.notice("Object at team request does not have a core component.");
			return -1;
		}

		return core->team;
	}

	return -1;
}

float GameConnector::getRand(float from, float to)
{
	return random(from, to);
}

EventLua::Object GameConnector::getLife(int id)
{
	Object * o = 0;
	if (env->isEmpty())
		o = gameHandler->getControlledObject();
	else
		o = env->getData(id);

	std::stringstream ss;
	ss << "TEMP_TABLE_GETLIFE" << env->getEnvId();
	EventLua::Object rtn = gameHandler->getScript()->newTable(ss.str().c_str());

	if (o == 0)
		return rtn;
	QueryEvent<Vector3> q(CORE_HEALTH);
	o->sendInternal(&q);

	if (!q.isSet())
		return rtn;

	rtn["current"] = q.getData().x;
	rtn["max"] = q.getData().y;

	return rtn;
}

EventLua::Object GameConnector::getMana(int id)
{
	Object * o = 0;
	if (env->isEmpty())
		o = gameHandler->getControlledObject();
	else
		o = env->getData(id);

	std::stringstream ss;
	ss << "TEMP_TABLE_GETMANA" << env->getEnvId() << "_" << id;
	EventLua::Object rtn = gameHandler->getScript()->newTable(ss.str().c_str());

	if (o == 0)
		return rtn;
	QueryEvent<Vector3> q(CORE_MANA);
	o->sendInternal(&q);

	if (!q.isSet())
		return rtn;

	rtn["current"] = q.getData().x;
	rtn["max"] = q.getData().y;

	return rtn;
}

float GameConnector::getMouseX(int id)
{
	Object * o = env->getData(id);
	if(o)
	{
		ControllerComponent* cont = o->findComponent<ControllerComponent>();

		if(cont)
		{
			return cont->getMousePosition().x;
		}
		else
		{
			luaLogger.alert("Trying to fetch mouse position from an object that has no controller component.");
			return -1;
		}
	}
	else
	{
		luaLogger.alert("Trying to fetch mouse position from an object that does not exist.");
		return -1;
	}
}

float GameConnector::getMouseY(int id)
{
	Object * o = env->getData(id);
	if(o)
	{
		ControllerComponent* cont = o->findComponent<ControllerComponent>();

		if(cont)
		{
			return cont->getMousePosition().y;
		}
		else
		{
			luaLogger.alert("Trying to fetch mouse position from an object that has no controller component.h");
			return -1;
		}
	}
	else
	{
		luaLogger.alert("Trying to fetch mouse position from an object that does not exist.");
		return -1;
	}
}

float GameConnector::getMouseZ(int id)
{
	Object * o = env->getData(id);
	if(o)
	{
		ControllerComponent* cont = o->findComponent<ControllerComponent>();

		if(cont)
		{
			return cont->getMousePosition().z;
		}
		else
		{
			luaLogger.alert("Trying to fetch mouse position from an object that has no controller component.h");
			return -1;
		}
	}
	else
	{
		luaLogger.alert("Trying to fetch mouse position from an object that does not exist.");
		return -1;
	}
}


EventLua::Object GameConnector::getDirToMouse(int id, float multitude /* = 1.0f */)
{
	if(multitude == 0.0f)
		multitude = 1.0f;

	Object * o = env->getData(id);
	std::stringstream ss;
	ss << "TEMP_TABLE_MOUSEDIR" << env->getEnvId();
	EventLua::Object rtn = gameHandler->getScript()->newTable(ss.str().c_str());

	if (o == 0)
	{
		luaLogger.alert("Trying to fetch mouse position from an object that does not exist.");
		return rtn;
	}

	ControllerComponent * cont = o->findComponent<ControllerComponent>();
	PhysicsComponent * phys = o->findComponent<PhysicsComponent>();

	if (cont == 0 || phys == 0)
		return rtn;

	Vector3 mouse = cont->getMousePosition();
	Vector3 dir = mouse - phys->position;

	dir.normalize();

	dir = dir * multitude;

	rtn["x"] = dir.x;
	rtn["y"] = dir.y;
	rtn["z"] = dir.z;

	return rtn;
}

EventLua::Object GameConnector::getDir(int from, int to, float multitude /* = 1.0f */)
{
	if(multitude == 0.0f)
		multitude = 1.0f;

	Object * fobj = env->getData(from);
	Object * tobj = env->getData(to);
	std::stringstream ss;
	ss << "TEMP_TABLE_OBJDIR" << env->getEnvId();
	EventLua::Object rtn = gameHandler->getScript()->newTable(ss.str().c_str());

	if(fobj == 0)
	{
		luaLogger.alert("Trying to fetch direction from an object that does not exist.");
		return rtn;
	}
	if(tobj == 0)
	{
		luaLogger.alert("Trying to fetch direction to an object that does not exist.");
		return rtn;
	}

	PhysicsComponent* fromPhys = fobj->findComponent<PhysicsComponent>();
	PhysicsComponent* toPhys = tobj->findComponent<PhysicsComponent>();

	if(fromPhys == 0)
	{
		luaLogger.alert("Trying to fetch direction from an object that has no PhysicsComponent.");
		return rtn;
	}
	if(toPhys == 0)
	{
		luaLogger.alert("Trying to fetch direction to an object that has no PhysicsComponent.");
		return rtn;
	}

	Vector3 dir = toPhys->position - fromPhys->position;

	dir.normalize();

	dir = dir * multitude;

	rtn["x"] = dir.x;
	rtn["y"] = dir.y;
	rtn["z"] = dir.z;

	return rtn;
}

bool GameConnector::setPosition(int target, int from, float x, float y, float z)
{
	Object * t = env->getData(target);
	Object * f = env->getData(from);
	if (t == 0)
	{
		luaLogger.alert("SetPos failed. Could not find target");
		return false;
	}
	Vector3 pos(0.0f, 0.0f, 0.0f);
	if (from > 0)
	{
		if (f == 0)
		{
			luaLogger.alert("SetPos failed. Could not find origin");
			return false;
		}
		PhysicsComponent* fp = f->findComponent<PhysicsComponent>();
		if (fp == 0)
		{
			luaLogger.alert("SetPos failed. Could not find PhysicsComponent of origin object");
			return false;
		}
		pos = fp->position;
	}
	pos += Vector3(x, y, z);
	
	t->send(&PositionEvent(pos));
	return true;
}

bool GameConnector::setRotation(int target, int from, float x, float y, float z, float w)
{

	Object * t = env->getData(target);
	Object * f = env->getData(from);
	if (t == 0)
	{
		luaLogger.alert("SetRot failed. Could not find target");
		return false;
	}
	Quaternion rot(0.0f, 0.0f, 0.0f, 0.0f);
	if (from > 0)
	{
		if (f == 0)
		{
			luaLogger.alert("SetRot failed. Could not find origin");
			return false;
		}
		PhysicsComponent* fp = f->findComponent<PhysicsComponent>();
		if (fp == 0)
		{
			luaLogger.alert("SetRot failed. Could not find PhysicsComponent of origin object");
			return false;
		}
		rot = fp->rotation;
	}
	Quaternion quat = Quaternion(x, y, z, w);
	quat.normalize();
	rot = quat * rot;

	Vector3 tempAxis;
	float tempAngle;
	rot.getAxisAngle(tempAxis, tempAngle);

	t->send(&RotationEvent(tempAxis, tempAngle, false));
	return true;
}

bool GameConnector::setScale(int target, float x, float y, float z)
{
	Object * t = env->getData(target);

	if (t == 0)
	{
		luaLogger.alert("SetScale failed. Could not find target");
		return false;
	}

	t->send(&ScaleEvent(x, y, z, false));

	return true;
}


bool GameConnector::setKeyBind(const char * n, EventLua::Object info)
{
	EventLua::Object key, val;
	std::string name = n;

	std::vector<int> keys;

	while (info.next(key, val))
	{
		if (val.isNumber())
			keys.push_back(val.queryInteger());
	}
	gameHandler->getEventHandler()->setKeyBind(keys, name);
	return true;
}

void GameConnector::setAssignable(const char * ass, const char * tar)
{
	if (ass)
	{
		std::string assignable = ass;
		if (tar)
		{
			std::string target = tar;
			gameHandler->getEventHandler()->setAssignable(assignable, target);
		}
		else
			gameHandler->getEventHandler()->setAssignable(assignable, "");
	}
}

EventLua::Object GameConnector::getAssignablesFor(const char * target)
{
	std::stringstream ss;
	ss << "TEMP_TABLE_ASSIGNABLES_FOR" << env->getEnvId();
	EventLua::Object rtn = gameHandler->getScript()->newTable(ss.str().c_str());

	if (target)
	{
		std::vector<std::string> ass = gameHandler->getEventHandler()->getAssignablesFor(target);
		for (unsigned int n = 0; n < ass.size(); ++n)
		{
			rtn[n + 1] = ass[n].c_str();
		}
	}
	return rtn;
}

EventLua::Object GameConnector::getActiveAssignables()
{
	std::stringstream ss;
	ss << "TEMP_TABLE_ACTIVE_ASSIGNABLES" << env->getEnvId();
	EventLua::Object rtn = gameHandler->getScript()->newTable(ss.str().c_str());

	std::vector<std::string> ass = gameHandler->getEventHandler()->getActiveAssignables();
	for (unsigned int n = 0; n < ass.size(); ++n)
	{
		rtn[n + 1] = ass[n].c_str();
	}
	return rtn;
}

const char* GameConnector::getKeyBind( const char* name )
{
	static std::string result;
	std::stringstream resstream;
	std::vector<int> keybind = gameHandler->getEventHandler()->getKeyBind( name );

	for( std::vector<int>::iterator it = keybind.begin(); it != keybind.end(); ++it )
	{
		if( !resstream.str().empty() )
		{
			resstream << "+";
		}



		if ((*it) == MB_RIGHT)
			resstream<< "RMB";
		else if ((*it) == MB_LEFT)
			resstream<< "LMB";
		else if ((*it) == MB_MIDDLE)
			resstream<< "MMB";
		else if ((*it) == MB_EXTRA)
			resstream<< "MB4";
		else if ((*it) == MB_EXTRA2)
			resstream<< "MB5";
		else if ((*it) == MB_WHEEL_UP)
			resstream << "MWU";
		else if ((*it) == MB_WHEEL_DOWN)
			resstream << "MWD";
		else if( isprint( (unsigned char)(*it) ) )
			resstream << (unsigned char)(*it);
		else if( (*it) == 16 )
			resstream << "Shift";
		else if( (*it) == 17 )
			resstream << "Ctrl";
		else if( (*it) == 18 )
			resstream << "Alt";
		
	}

	result = resstream.str();

	return result.c_str();
}

bool GameConnector::subscribeKeyBind(const char * c)
{
	std::string name = c;
	return gameHandler->getEventHandler()->subscribe(env->getCaller(), name);
}

bool GameConnector::subscribeEvent(int id, int type)
{
	return gameHandler->getEventHandler()->subscribe(env->getCaller(), (EventEnum)type, env->getData(id));
}

bool GameConnector::subscribeKeyEvents()
{
	return gameHandler->getEventHandler()->subscribe(env->getCaller(), EVENT_KEY);
}

void GameConnector::loadScript(const char * name, int type)
{
	File file;
	//LuaFileManager * lfm = gameHandler->getLuaFileManager();
	//if (!lfm->getFile(name, file))
	{
		if (!FileSystem::get( name, &file ))
		{
			luaLogger.alert("Failed to load file: ", name, ".");
			return;
		}
		//lfm->addFile(name, file);
	}
	if( !gameHandler->getScript()->execute( file.data, file.size ) )
	{
		std::cout << gameHandler->getScript()->getLastError() << ". In file: " << name <<std::endl;
	}
}

bool GameConnector::isMonster(int id)
{
	Object * o = env->getData(id);
	if (o == 0)
		return false;

	PhysicsComponent * comp = o->findComponent<PhysicsComponent>();
	if (comp)
	{
		return !comp->trigger;
	}
	return false;
}

bool GameConnector::isEffecting(int taget, int effecter /* = 0 */)
{
	Object * o = env->getData(taget);
	Object * c = env->getData(effecter);

	FAIL_CHECK_LOG_RTN(o, "isEffecting failed. Could not find target", false);
	FAIL_CHECK_LOG_RTN(o, "isEffecting failed. Could not find effecter", false);

	SkillComponent * skill = c->findComponent<SkillComponent>();
	FAIL_CHECK_LOG_RTN(o, "isEffecting failed. Could not find effector's skill component", false);
	return skill->isEffecting(o);
}

bool GameConnector::effect(int id, bool unremarkable)
{
	//std::cout<< "Effect " << id << '\n';
	Object * o = env->getData(id);
	Object * c = env->getCaller();
	if (!isMonster(id)) // TODO: Fix prorperly... (the bug is effecting trigger items as well)
		return false;
	if (o == 0 || c == 0)
		return false;

	SkillComponent * comp = c->findComponent<SkillComponent>();
	if (comp == 0)
		return false;

	comp->addEffect(o, c->getCreator(), !unremarkable);

	return true;
}

bool GameConnector::repeatEffect(int id, bool unremarkable)
{
	Object * o = env->getData(id);
	FAIL_CHECK_LOG_RTN(o, "RepeatEffect failed. Could not find object", false);

	SkillComponent * comp = o->findComponent<SkillComponent>();
	FAIL_CHECK_LOG_RTN(comp, "RepeatEffect failed. Could not find SkillComponent in object", false);

	comp->repeatEffect(o, !unremarkable);
	return true;
}

bool GameConnector::removeEffect(int id)
{
	Object * o = env->getData(id);
	Object * c = env->getCaller();
	if (o == 0 || c == 0)
		return false;

	SkillComponent * comp = c->findComponent<SkillComponent>();
	if (comp == 0)
		return false;
	comp->removeEffect(o);
	return true;
}

int GameConnector::getKey(int sym)
{
	return gameHandler->getKey(sym);
}

void GameConnector::attach(int id, int idTo)
{
	Object * obj = env->getData(id);
	Object * to = env->getData(idTo);

	if(obj)
	{
		if(to)
		{
			to->attachObject(obj);
		}
		else
			luaLogger.alert("Object to attach to does not exist");
	}
	else
	{
		luaLogger.alert("Object to attach does not exist");
	}
}

void GameConnector::detach(int id, int idFrom)
{
	Object * obj = env->getData(id);
	Object * from = env->getData(idFrom);

	if(obj)
	{
		if(from)
			from->detachObject(obj);
		else
			luaLogger.alert("Object to detach from does not exist");
	}
	else
	{
		luaLogger.alert("Object to detach does not exist");
	}
}

void GameConnector::enableSlowdown(int id)
{
	Object * obj = env->getData(id);

	if(obj)
	{
		PhysicsComponent* phys = obj->findComponent<PhysicsComponent>();

		if(phys)
		{
			phys->enableSlowdown();
		}
		else
		{
			luaLogger.alert("Trying to enable slowdown to an object without PhysicsComponent.");
		}
	}
	else
	{
		luaLogger.alert("Trying to enable slowdown to an object that doesn't exist.");
	}
}

void GameConnector::disableSlowdown(int id)
{
	Object * obj = env->getData(id);

	if(obj)
	{
		PhysicsComponent* phys = obj->findComponent<PhysicsComponent>();

		if(phys)
		{
			phys->disableSlowdown();
		}
		else
		{
			luaLogger.alert("Trying to disable slowdown to an object without PhysicsComponent.");
		}
	}
	else
	{
		luaLogger.alert("Trying to disable slowdown to an object that doesn't exist.");
	}
}

int GameConnector::playSound(const char* file, float volume, int loop, int gui)
{
	SoundSubsystem* sound = (SoundSubsystem*)gameHandler->getSubsystem("Sound");

	std::string filename = file;

	if (sound)
	{
		return sound->playSound(filename, volume, loop == 1, gui == 1);
	}

	return -1;
}

void GameConnector::stopSound(int id)
{
	SoundSubsystem* sound = (SoundSubsystem*)gameHandler->getSubsystem("Sound");

	if (sound)
	{
		sound->stopSoundcomponent(id);
	}
}

void GameConnector::forgetTrigger(int id, int id2)
{
	Object * o = env->getData(id);
	Object * o2 = env->getData(id2);
	FAIL_CHECK_LOG(o, "Forget collision failed. Could not find object one.");
	FAIL_CHECK_LOG(o2, "Forget collision failed. Could not find object two.");

	PhysicsComponent * phys = o->findComponent<PhysicsComponent>();
	PhysicsComponent * phys2 = o2->findComponent<PhysicsComponent>();

	FAIL_CHECK_LOG(phys, "Forget collision failed. Could not find PhysicsComponent one.");
	FAIL_CHECK_LOG(phys2, "Forget collision failed. Could not find PhysicsComponent two.");

	PhysicsSubsystem * sub = (PhysicsSubsystem *)gameHandler->getSubsystem("Physics");
	FAIL_CHECK_LOG(sub, "Forget collision failed. Could not find PhysicsSubsystem.");

	sub->forgetTrigger(phys, phys2);
}

void GameConnector::forgetAllTriggers(int id)
{
	Object * o = env->getData(id);
	FAIL_CHECK_LOG(o, "Forget all collisions failed. Could not find object.");

	PhysicsComponent * phys = o->findComponent<PhysicsComponent>();
	FAIL_CHECK_LOG(phys, "Forget all collisions failed. Could not find PhysicsComponent.");

	PhysicsSubsystem * sub = (PhysicsSubsystem *)gameHandler->getSubsystem("Physics");    
	FAIL_CHECK_LOG(sub, "Forget all collisions failed. Could not find PhysicsSubsystem.");

	sub->forgetAllTriggers(phys);
}

bool GameConnector::checkTimer(int id, int timer)
{
	Object * o = env->getData(id);
	FAIL_CHECK_LOG_RTN(o, "Check timer failed. Could not find object.", false);
	TimerComponent * time = o->findComponent<TimerComponent>();
	FAIL_CHECK_LOG_RTN(time, "Check timer failed. Could not find timer component.", false);

	return time->checkTimer(timer);
}

void GameConnector::reloadLuaFiles()
{
	gameHandler->reloadAllLuaFiles();
}


void GameConnector::sendAICommand(int id, const char* command, int state)
{
	Object * o = env->getData(id);
	FAIL_CHECK_LOG(o, "Send AI command failed. Could not find object.");
	AIComponent* ai = o->findComponent<AIComponent>();
	FAIL_CHECK_LOG(o, "Send AI command failed. Could not find AI component");

	ai->giveCommand(command, state);
}