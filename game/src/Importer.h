#ifndef IMPORTER_H

#define IMPORTER_H

#include "AnimationComponent.h"

#include "Framework.h"

#include "Common.h"

#include "ShapeAnimation.h"

class Importer
{

	// This is the header, 64bytes 
	// an integer of 4 bytes, and 60bytes unused data
	struct ShapeHeader
	{
		int nr;
		char args[60];
	};

public:

	static bool readShapeFromFile(const File& file, ShapeAnimation* const animation);
	

};


#endif