
#ifndef CONTROLLERSUBSYSTEM_H
#define CONTROLLERSUBSYSTEM_H

#include "Subsystem.h"
class ControllerComponent;

/*
 * The specific tasks are described in the corresponding component header file.
 */

class ControllerSubsystem : public Subsystem
{
	typedef std::vector<ControllerComponent> Components;

public:
	ControllerSubsystem( GameHandler* gameHandler = NULL, int size = 1000);
	virtual ~ControllerSubsystem();

	Component*	createComponent(EventLua::Object script);
	Component*	getComponent();
	void		update();

	void clear();

private:
	Components components;
};

#endif