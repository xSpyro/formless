#ifndef PREPARE_RESPAWN_EVENT_H
#define PREPARE_RESPAWN_EVENT_H
#include "Event.h"

class PrepareRespawnEvent : public Event
{
public:
	PrepareRespawnEvent(Object * sender = 0);
	virtual ~PrepareRespawnEvent();

	virtual void sendTo(Object* to);
private:
};

#endif