#ifndef SOUND_SUB_COMPONENT_H

#define SOUND_SUB_COMPONENT_H

#include "SoundComponent.h"

class SoundComponent;

class SoundSubComponent
{
public:
	SoundSubComponent(SoundComponent* p);
	~SoundSubComponent();

	void update();

	virtual void updateSubComponent() = 0;

	void setDirty(const bool d);

	const bool getDirty() const;

	SoundComponent* const getParent();


	virtual void receive(KeyEvent * key){};

	virtual void receive(MoveEvent * move){};

	virtual void receive(RespawnPlayerEvent * res){};

	virtual void receive(EffectEvent * effect){};

	virtual void receive(KillPlayerEvent * kill ){};

	
private:
	bool dirty;

protected:
	SoundComponent* parent;
};


#endif