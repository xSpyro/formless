
#include "ClientInterface.h"

#include "NetworkComponentClient.h"
#include "NetworkSubsystemClient.h"

#include "CommonPackets.h"

#include "Events.h"
#include "RespawnPlayerEvent.h"

#include "Object.h"

#include "PhysicsComponent.h"
#include "ControllerComponent.h"
#include "CoreComponent.h"

NetworkComponentClient::NetworkComponentClient( Subsystem* subsys, unsigned int id )
	: NetworkComponent(subsys, id)
{
	input.assign(32, false);

	sub = dynamic_cast < NetworkSubsystemClient* > (subsys);

	outOfSync = 0;
	syncPos = 0;
}

void NetworkComponentClient::receive(KeyBindEvent* keyBind)
{
	bool val = keyBind->getState() == 1;

	if (keyBind->getName() == "up")
		input[0] = val;

	if (keyBind->getName() == "down")
		input[1] = val;

	if (keyBind->getName() == "left")
		input[2] = val;

	if (keyBind->getName() == "right")
		input[3] = val;
}

void NetworkComponentClient::receive(ActivateSkillEvent * skill)
{
	if (skill->waitForConfirm())
	{
		input[4 + skill->getSkill()] = true;
	}

	sendUpdateState();
}

void NetworkComponentClient::update()
{
	static unsigned int counter = 0;
	

	if (++counter >= 3)
	{
		sendUpdateState();

		counter = 0;
	}

	PhysicsComponent * physics = object->findComponent<PhysicsComponent>();

	Vector3 d = (syncPos - physics->position);
	float len = d.length();


	float correctSpeed = 10;
	unsigned int desyncFrames = 100;
	float desyncDist = 100;

	static EventLua::State state;
	if (state.execute("../ClientSync.lua"))
	{
		desyncFrames = (unsigned int)state.get("desyncFrames").queryInteger(desyncFrames);
		correctSpeed = state.get("correct").queryFloat(correctSpeed);
		desyncDist = state.get("desyncDist").queryFloat(desyncDist);
	}

	// if distance to high
	if (len > desyncDist)
	{
		// lets hope we get a correct packet then a couple of frames
		if (++outOfSync > desyncFrames)
		{
			// force correct
			physics->position = syncPos;

			outOfSync = 0;
		}
		else if (outOfSync > desyncFrames / 2)
		{
			physics->position += d / (correctSpeed * 4);
		}
	}
	else
	{
		physics->position += d / correctSpeed;
		outOfSync = 0;
	}
}

void NetworkComponentClient::onPacket(RawPacket & raw)
{
	switch (raw.header.id)
	{
	case UPDATE_PACKET:
		{
			UpdatePacket * update = reinterpret_cast < UpdatePacket* > (raw.data);

			compensatePosition(*update);

			PhysicsComponent * phys = object->findComponent<PhysicsComponent>();
			if(phys)
			{
				phys->velocity = Vector3(update->vx, update->vy, update->vz);
			}

			CoreComponent * core = object->findComponent<CoreComponent>();
			if (core)
			{
				// set life & mana
				core->life.setCurrent(update->life);
				core->mana.setCurrent(update->mana);
			}
		}
		break;
	}
}

void NetworkComponentClient::receive(RespawnPlayerEvent * res)
{
	Component::receive(res);
	outOfSync = 0;
	syncPos = res->getPosition();
	input.assign(32, false);

	//std::cout << "respawn event " << res->getPosition() << std::endl;
}

void NetworkComponentClient::sendUpdateState()
{
	ClientStatePacket packet;
		
	packet.input = 0;
	int adder = 1;
	for (int i = 0; i < 32; ++i)
	{
		packet.input += adder * input[i];
		adder <<= 1;
	}

	ControllerComponent * controller = object->findComponent<ControllerComponent>();

	if (controller)
	{
		Vector3 p = controller->getMousePosition();
		packet.lx = p.x;
		packet.ly = p.y;
		packet.lz = p.z;
	}

	sub->getNetwork()->send(packet);
	
	for (int n = 4; n < 32; ++n)
	{
		input[n] = false;
	}
}

void NetworkComponentClient::compensatePosition(UpdatePacket & packet)
{
	Vector3 target = Vector3(packet.x, packet.y, packet.z);


	CoreComponent* core = object->findComponent<CoreComponent>();
	if (core)
	{
		// set life & mana
		core->life.setCurrent(packet.life);
		core->mana.setCurrent(packet.mana);
	}

	PhysicsComponent * physics = object->findComponent<PhysicsComponent>();

	if (physics)
	{
		syncPos = target;

		/*
		physics->position += (target - physics->position) / 30.0f;

		float len = (target - physics->position).length();

		if (len > 60 + outOfSync * 10)
		{
			outOfSync += 1.0f;
		}
		else
		{
			if (len > 40)
			{
				physics->position += (target - physics->position) / 8.0f;
				outOfSync = 0.0f;
			}
		}

		if (len != len)
			physics->position = target; // TODO: remove

		if (outOfSync > 5)
		{
			physics->position = target;
			outOfSync = 0;
		}
		*/
	}
}