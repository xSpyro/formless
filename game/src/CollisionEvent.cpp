#include "CollisionEvent.h"
#include "EventEnum.h"
#include "Object.h"

CollisionEvent::CollisionEvent() : Event(EVENT_COLLISION), rhs(0), lhs(0)
{}

CollisionEvent::CollisionEvent(Object * rhs, Object * lhs, IntersectResult res) : Event(EVENT_COLLISION), rhs(rhs), lhs(lhs), res(res)
{}

CollisionEvent::~CollisionEvent()
{}

void CollisionEvent::set(Object * r, Object * l, IntersectResult result)
{
	rhs = r;
	lhs = l;
	res = result;
}

Object * CollisionEvent::getRight()
{
	return rhs;
}

Object * CollisionEvent::getLeft()
{
	return lhs;
}

IntersectResult CollisionEvent::getResult()
{
	return res;
}

void CollisionEvent::sendTo(Object *o)
{
	o->receive<CollisionEvent>(this);
}