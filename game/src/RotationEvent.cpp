#include "RotationEvent.h"
#include "EventEnum.h"
#include "Object.h"


RotationEvent::RotationEvent(float x, float y, float z, float a, bool apply ) : Event(EVENT_ROTATION, 0,  true), axis(x, y, z), angle(a), apply(apply)
{}
RotationEvent::RotationEvent(Vector3 axis, float angle, bool apply) : Event(EVENT_ROTATION, 0,  true), axis(axis), angle(angle), apply(apply)
{}

RotationEvent::~RotationEvent()
{}

Vector3 RotationEvent::getAxis()
{
	return axis;
}

float RotationEvent::getAngle()
{
	return angle;
}

bool RotationEvent::getApply()
{
	return apply;
}

void RotationEvent::setAxis(Vector3 rotationAxis)
{
	axis = rotationAxis;
}

void RotationEvent::setAngle(float rotationAngle)
{
	angle = rotationAngle;
}

void RotationEvent::setApply(bool rotationApply)
{
	apply = rotationApply;
}

void RotationEvent::sendTo(Object * to)
{
	to->receive<RotationEvent>(this);
}

