
#ifndef ANIMATIONCOMPONENT_H
#define ANIMATIONCOMPONENT_H

#include "Common.h"

#include "Component.h"
#include "Framework.h"

#include "ParticleStream.h"
#include "ShapeAnimationInst.h"

class ShapeAnimation;

class Object;
class AnimationSubsystem;

class PhysicsComponent;

class KillPlayerEvent;
class RespawnPlayerEvent;

class LinkEvent;

class AnimationComponent : public Component
{
public:
	AnimationComponent( Subsystem* subsystem = NULL );
	virtual ~AnimationComponent();

	void reset();
	void load(Renderer * renderer, Camera * camera);

	void update();
	void render();

	void receive(DestroyEvent * ev);
	void receive(KillPlayerEvent * ev);
	void receive(RespawnPlayerEvent * ev);

	void receive(AnimationEvent * ev);
	void receive(EffectEvent * ev);
	void receive(SkillEvent * ev);

	void receive(LinkEvent * ev);

	//void bindObject(Object* obj);
	//void unbindObject();

	void bindToComponents();


	void overrideAnimation(const ShapeAnimationInst * link);
	void setBaseColor(const Color & color);
	void setLightColor(const Vector3 & color);

	Color	getBaseColor() const;
	Vector3	getLightColor() const;

	bool dying();
	bool dead();

	const Renderable * getRenderable() const;
	Model * getModel();

	void pause();
	void resume();

	void startShape(const std::string & shape);
	void startShape(const ShapeAnimation * shape);
	void resetShape();

	void forceLink(const Vector3 & from, const Vector3 & to);
	const Vector3 & getForceLinkPosition() const;

	void kill();

private:

	void drawLink(const Vector3 & from, const Vector3 & to);

private: // old
public:

	Timer timer;

	AnimationSubsystem * as;
	PhysicsComponent * physics;

	ParticleStream stream;

	ShapeAnimationInst animation;
	const ShapeAnimationInst * link;

	unsigned int delta;

	bool has_animation;
	bool spawn;

	ShapeResult result;

	Renderable * renderable;
	Model model;
	Texture * mt;

	Color	baseColor;
	Vector3	lightColor;
	float	lossFactor;

	float gravityFactor;

	Vector3 linkTarget;
	bool linked;

	Vector3 forceLinks[2];
	bool forceLinked;

	PhysicsComponent * linkPlayer;
	PhysicsComponent * linkOther;
};

#endif
