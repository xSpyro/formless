
#ifndef CONTROLLERCOMPONENT_H
#define CONTROLLERCOMPONENT_H

#include "Component.h"
#include "Framework.h"
class Object;
class PhysicsComponent;

/*	
 *	The controller component handles user interaction with an object.
 *	If an object has no controller component, it cannot be controlled by a player.
 */

class ControllerComponent : public Component
{
public:
	ControllerComponent( Subsystem* subsystem = NULL );
	virtual ~ControllerComponent();

	void receive(KeyEvent* key);
	void receive(KeyBindEvent* keyBind);
	void receive(MouseEvent* mouse);
	void receive(EffectEvent * ev);
	void receive(QueryEvent<Vector3> * qv);
	void receive( KillPlayerEvent* ev );
	void receive( RespawnPlayerEvent* ev );
	
	void update();

	void clearStates();

	void setSpeed( float speed );
	void setActiveSlot( int slot );
	void updateMovement();

	void bindObject(Object* obj);
	void unbindObject();

	bool subscribe();

	void bindToComponents();

	void setMousePosition(const Vector3& position);
	const Vector3 & getMousePosition() const;

	const Vector3 getDirection() const;

public:
	std::vector<std::string> keybinds;

private:
	PhysicsComponent*				physcomponent;
	Vector3							velocity;
	std::map< std::string, int >	keyStates;
	float							speed;

	int								activeSlot;

	int								cooldown;
	int								stunTicker;
	int blockSkills;

	Vector3							mousePosition;

	Vector3							toMiddle;
};

#endif