#include "ScriptEnvironment.h"

ScriptEnvironment * env = NULL;

ScriptEnvironment::ScriptEnvironment() : maxSize(50), base(0, 100)
{
}

ScriptEnvironment::ScriptEnvironment(unsigned int max) : maxSize(max), base(0, 100)
{
}

ScriptEnvironment::~ScriptEnvironment()
{
	if(env)
		delete env;
}
	
bool ScriptEnvironment::push(Object * caller)
{
	if (envStack.size() < maxSize)
	{
		envStack.push(caller);
		return true;
	}
	return false;
}

void ScriptEnvironment::pop()
{
	if (envStack.empty())
		base.clearData();
	else
		envStack.pop();
}

Object * ScriptEnvironment::getCaller()
{
	if (envStack.empty())
		return base.getCaller();
	else
		return envStack.top().getCaller();
}

Object * ScriptEnvironment::getData(int id)
{
	if (envStack.empty())
		return base.getData(id);
	else
		return envStack.top().getData(id);
}

int ScriptEnvironment::addData(Object * o)
{
	if (envStack.empty())
		return base.addData(o);
	else
		return envStack.top().addData(o);
}

void ScriptEnvironment::clearData()
{
	if (envStack.empty())
		base.clearData();
	else
		envStack.top().clearData();
}

int ScriptEnvironment::getEnvId()
{
	return envStack.size();
}

bool ScriptEnvironment::isEmpty()
{
	return envStack.empty();
}

bool ScriptEnvironment::isFull()
{
	if (envStack.empty())
		return base.isFull();
	else
		return envStack.top().isFull();
}