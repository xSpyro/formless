#ifndef SKILL_EVENT_H
#define SKILL_EVENT_H

#include "Event.h"
#include "Framework.h"

class SkillEvent : public Event // Skill have been activated
{
public:
	SkillEvent(Object * skill = 0, std::string name = "");
	virtual ~SkillEvent();

	virtual void	sendTo( Object* to );

	std::string getName();
	Object * getSkill();
private:

	std::string name;
	Object * skill;
};

#endif