#include "Environment.h"

Environment::Environment(Object * c, unsigned int ms) : caller(c), maxSize(ms)
{}

Environment::~Environment()
{}

Object * Environment::getCaller()
{
	return caller;
}

Object * Environment::getData(int pos)
{
	if (pos == 0)
		return caller;
	if ((unsigned)pos <= data.size() && pos >= 1)
		return data[pos - 1];
	return 0;
}

int Environment::addData(Object * o)
{
	if(o == 0)
		return -1;
	if (data.size() < maxSize)
	{
		if (o == caller)
			return 0;
		for (int n = 0; (unsigned)n < data.size(); ++n)
		{
			if (o == data[n])
				return n + 1;
		}
		data.push_back(o);
		return data.size();
	}
	return -1;
}

void Environment::clearData()
{
	data.clear();
}

bool Environment::isFull()
{
	return (data.size() >= maxSize);
}