#include "Terrain.h"

Terrain::Terrain() : height(.0f), scale(1.0f), offset(0.0f, 0.0f, 0.0f), surface()
{}

bool Terrain::save(std::ostream & f)
{
	f.write((char *)&height, sizeof(float));
	f.write((char *)&scale, sizeof(float));
	f.write((char *)&offset, sizeof(Vector3));
	return surface.save(f);
}

bool Terrain::load(std::istream & f)
{
	f.read((char *)&height, sizeof(float));
	f.read((char *)&scale, sizeof(float));
	f.read((char *)&offset, sizeof(Vector3));
	return surface.load(f);
}

float Terrain::sample(float x, float z)
{
	if (!inside(x, z))
		return 0.0f;

	x = x * (surface.getWidth()/scale) + surface.getWidth() * 0.5f;
	z = z * (surface.getHeight()/scale) + surface.getHeight() * 0.5f;

	float xVal = x - ((int)x);
	float zVal = z - ((int)z);

	float tot = at((int)x,(int) z) * (1-xVal) * (1-zVal); // TODO: Properly check if this is correct
	tot += at((int)x,(int) z + 1) * (1-xVal) * (zVal);
	tot += at((int)x + 1,(int) z) * (xVal) * (1-zVal);
	tot += at((int)x + 1, (int)z + 1) * (xVal) * (zVal);
	return tot * scale * height + offset.y;
}

bool Terrain::inside(float x, float z)
{
	float sd2 = scale * 0.5f;
	return (x <= sd2 && x >= -sd2 && z <= sd2 && z >= -sd2);
}

float Terrain::findDistance(Vector3 from, Vector3 to)
{
	// Convert to surface positions
	Vector3 f(from.x * (surface.getWidth()/scale) + surface.getWidth() * 0.5f, (from.y - offset.y)/(scale * height), from.z * (surface.getHeight()/scale) + surface.getHeight() * 0.5f);
	Vector3 t(to.x * (surface.getWidth()/scale) + surface.getWidth() * 0.5f, (to.y - offset.y)/(scale * height), to.z * (surface.getHeight()/scale) + surface.getHeight() * 0.5f);
	
	Vector3 dir = t - f;
	float lenght = Vector3(dir.x, 0, dir.z).length();
	float delta = dir.length() / lenght;

	dir.normalize();

	float min = std::min(f.y - at((int)(f.x + 0.5f), (int)(f.z + 0.5f)), t.y - at((int)(t.x + 0.5f), (int)(t.z + 0.5f)));
	
	int lim = (int)(lenght + 0.5f);
	for (int n = 0; n <= lim; ++n)
	{
		Vector3 spot = f + dir * delta * (float)n;
		float temp = spot.y - at((int)(spot.x + 0.5f), (int)(spot.z + 0.5f));
		min = std::min(min, temp);
	}

	return min * scale * height;
}

bool Terrain::valid(int x, int z)
{
	return (x >= 0 && x < surface.getWidth() && z >= 0 && z < surface.getHeight());
}

float Terrain::at(int x, int z)
{
	if (!valid(x, z))
		return 0.0f;

	return surface.at(x, z).r;
}