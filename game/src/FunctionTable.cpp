#include "FunctionTable.h"
#include "ScriptEnvironment.h"

FunctionTable::FunctionTable()
{}

FunctionTable::~FunctionTable()
{}

EventLua::Object * FunctionTable::getFunction(std::string name, Object * caller)
{
	Table::iterator it = table.find(name);
	if (it == table.end())
		return 0;

	if (it->second.isFunction())
	{
		env->push(caller);
		return &it->second;
	}
	return 0;
}

void FunctionTable::load(const EventLua::Object & script)
{
	EventLua::Object key, val;
	while (script.next(key, val))
	{
		if (val.isFunction())
		{
			table[key.queryString()] = val;
		}
	}
}


void FunctionTable::functionDone()
{
	env->pop();
}