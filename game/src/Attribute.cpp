#include "Attribute.h"

Attribute::Attribute(float b, float off, float mod, bool allowZero, bool multiplicative) : base(b), offset(off), modifier(mod), allowZero(allowZero), regen(0), multiplicativeMod(multiplicative)
{
	updateValue();
}

void Attribute::update()
{
	if (current <= 0)
	{
		current = 0;
		return;
	}

	current += getRegen() * (1.0f/60.f); // Seconds per tick

	if (current > max)
		current = max;
	if (current < 0)
		current = 0;
}

void Attribute::updateValue()
{
	max = (base + offset) * modifier; // TODO: Ask about if this is good
	//if (current > max)
		current = max;
	//if (current < 0)
	//	current = 0;
}


bool Attribute::damage(float dam)
{
	current -= dam;
	if (current <= 0)
	{
		current = 0;
		return false;
	}
	return true;
}

bool Attribute::pay(float cost)
{
	if (cost >= current && !allowZero)
		return false;
	if (cost > current)
		return false;

	current -= cost;
	if (current > max)
		current = max;
	return true;
}

void Attribute::add(Trait t)
{
	offset += t.offset;
	if (multiplicativeMod)
		modifier *= t.modifier;
	else
		modifier += t.modifier;

	updateValue();
}

void Attribute::remove(Trait t)
{
	offset -= t.offset;
	if (multiplicativeMod)
		modifier /= t.modifier;
	else
		modifier -= t.modifier;

	updateValue();
}

float Attribute::getBase()
{
	return base;
}

float Attribute::getMax()
{
	return max;
}

float Attribute::getModifier()
{
	return modifier;
}

float Attribute::getOffset()
{
	return offset;
}

float Attribute::getCurrent()
{
	return current;
}

float Attribute::getPercentage()
{
	return current / max;
}

float Attribute::getRegen()
{
	if (regen)
		return regen->getCurrent();
	return 0.0f;
}

Attribute * Attribute::getRegenAttribute()
{
	return regen;
}

void Attribute::setCurrent(float c)
{
	current = c;
}

void Attribute::setBase(float b)
{
	base = b;
	updateValue();
}

void Attribute::setRegen(Attribute * reg)
{
	regen = reg;
	updateValue();
}

void Attribute::setModifier(float mod)
{
	modifier = mod;
	updateValue();
}

void Attribute::setOffset(float off)
{
	offset = off;
	updateValue();
}

void Attribute::changeModifier(float m, bool add)
{
	if(multiplicativeMod)
		if (add)
			modifier *= m;
		else
			modifier /= m;
	else
		modifier += m;
	updateValue();
}

bool Attribute::isMulticative()
{
	return multiplicativeMod;
}

void Attribute::changeOffset(float off)
{
	offset += off;
	updateValue();
}

void Attribute::revive()
{
	current = max;
}

void Attribute::drain()
{
	current = 0;
}

void Attribute::reset()
{
	offset = 0;
	modifier = 0;
	updateValue();
}