#ifndef GAME_EVENTS_H
#define GAME_EVENTS_H

#include "../logic/src/Events.h"
#include "PushEvent.h"
#include "PositionEvent.h"
#include "RotationEvent.h"
#include "ScaleEvent.h"
#include "TriggerEvent.h"
#include "CollisionEvent.h"
#include "EffectEvent.h"
#include "SkillEvent.h"
#include "QueryEvent.h"
#include "MoveEvent.h"
#include "ActivateSkillEvent.h"
#include "ConfirmSkill.h"
#include "FireEvent.h"
#include "AnimationEvent.h"
#include "LinkEvent.h"
#include "KillPlayerEvent.h"
#include "RespawnPlayerEvent.h"
#include "GameStartEvent.h"
#include "GameEndEvent.h"
#include "DamageEvent.h"

#endif