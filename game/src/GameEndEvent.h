#ifndef GAME_END_EVENT_H
#define GAME_END_EVENT_H
#include "Event.h"

class GameEndEvent : public Event // Try to activate a skill
{
public:
	GameEndEvent();
	~GameEndEvent();


	void sendTo(Object * o);

private:

};

#endif