#include "SoundSubSystem.h"

#include "GameHandler.h"

SoundSubsystem::SoundSubsystem()
	: camera(NULL), fxVolume(1.0f), masterVolume(1.0f), guiVolume(1.0f)
{
	sounds.reserve(1000);
	moveSubComponents.reserve(1000);
	effectSubComponents.reserve(1000);
	listenerSubComponents.reserve(1000);

	music = new MusicPlayer(this);

	music->init();

	listener = NULL;
	nullComponent = SoundComponent((Subsystem*)this, -1, "", NULL, 0, 0, NULL);
}

SoundSubsystem::SoundSubsystem(GameHandler* gameHandler)
	: Subsystem(gameHandler), camera(NULL),fxVolume(1.0f), masterVolume(1.0f), guiVolume(1.0f)

{
	sounds.reserve(1000);
	moveSubComponents.reserve(1000);
	effectSubComponents.reserve(1000);
	listenerSubComponents.reserve(1000);
	music = new MusicPlayer(this);

	music->init();

	listener = NULL;

	nullComponent = SoundComponent((Subsystem*)this, -1, "", NULL, 0, 0, NULL);
}


SoundSubsystem::SoundSubsystem(GameHandler* gameHandler, const std::string loggerName)
	: Subsystem(gameHandler, loggerName), camera(NULL), fxVolume(1.0f), masterVolume(1.0f), guiVolume(1.0f)
{
	sounds.reserve(1000);
	moveSubComponents.reserve(1000);
	effectSubComponents.reserve(1000);
	listenerSubComponents.reserve(1000);
	music = new MusicPlayer(this);

	music->init();

	listener = NULL;

	nullComponent = SoundComponent((Subsystem*)this, -1, "", NULL, 0, 0, NULL);
}


SoundSubsystem::~SoundSubsystem()
{
	delete music;
}

Component* SoundSubsystem::createComponent(EventLua::Object script)
{
	std::string name = script.get("sound").query("");
	int loop = script.get("loop").query(0);
	float volume = script.get("volume").query(0.4f);
	int property = script.get("property").query(0);
	int move3DEffect = script.get("move3DEffect").query(0);
	int listener = script.get("listener").query(0);

	float spread = script.get("spread").query(0.0f);
	float doppler = script.get("doppler").query(0.0f);
	float directOcclusion = script.get("directOcclusion").query(0.0f);
	float reverbOcclusion = script.get("reverbOcclusion").query(0.0f);

	int useCreatorPosition = script.get("useCreatorPosition").query(0);
	

	if (name == "" && listener == 0 && property == 0)
	{
		nullComponent.setStartPlaying(false);
		nullComponent.setStopPlaying(false);

		return &nullComponent;
	}

	SoundComponent* comp = (SoundComponent*)getComponent();

	volume *= fxVolume * masterVolume;

	int id = -1;

	if(comp)
	{
		*comp = SoundComponent(this, id, name, NULL, property, volume, &audio);
	}

	else 
	{
		sounds.push_back(SoundComponent(this, id, name, NULL, property, volume, &audio));
		comp = &sounds.back();
	}

	comp->setLooping(loop == 1);

	if (property == 1)
	{
		MovementSoundSubComponent* movement = this->createMovementSubComponent(comp);
		comp->addSoundSubComponent(movement);
	}

	if (move3DEffect == 1)     
	{
		comp->addSoundSubComponent(this->createEffectSoundSubComponent(
			comp, 
			spread, 
			doppler, 
			directOcclusion, 
			reverbOcclusion,
			useCreatorPosition == 1));

		comp->set3D(true);
	}

	return comp;
}

int SoundSubsystem::createComponent(const std::string soundFile, float volume, bool loop)
{
	std::cout << "is being where I'm not supposed to be"<< std::endl;
	return 0;
}

int	SoundSubsystem::playSound(const std::string soundFile, float volume, bool loop, bool gui)
{
	if (gui)
		return audio.play(soundFile, volume * guiVolume * masterVolume, loop);

	// else it's an effect-sound
	return audio.play(soundFile, volume * fxVolume * masterVolume, loop);
}

Component* SoundSubsystem::getComponent()
{

	for(Sounds::iterator it = sounds.begin(); it != sounds.end(); it++)
	{
		if((*it).isPlaying() == false && !(*it).isStartPlaying() )
		{
			return &(*it);
		}
	}

	return NULL;
}

MovementSoundSubComponent* SoundSubsystem::createMovementSubComponent(SoundComponent* parent)
{
	// check if subcomponent exists to be replaced
	MovementSoundSubComponent* sComp = getMovementSubComponent();

	
	if (sComp)
	{
		// if the subcomponent exists, replace it
		*sComp = MovementSoundSubComponent(parent);
		return sComp;
	}
	// or else create a new one
	moveSubComponents.push_back(MovementSoundSubComponent(parent));
	return &moveSubComponents.back();
}

EffectSoundSubComponent* SoundSubsystem::createEffectSoundSubComponent(SoundComponent* parent, float spread, float doppler, float directOcclusion, float reverbOcclusion, bool useCreatorPosition)
{
	// check if subcomponent exists to be replaced
	EffectSoundSubComponent* sComp = getEffectSubComponent();

	if (sComp)
	{
		// if the subcomponent exists, replace it
		*sComp = EffectSoundSubComponent(parent, spread, doppler, directOcclusion, reverbOcclusion, useCreatorPosition);
		return sComp;
	}
	// or else create a new one
	effectSubComponents.push_back(EffectSoundSubComponent(parent, spread, doppler, directOcclusion, reverbOcclusion, useCreatorPosition));
	return &effectSubComponents.back();
}

ListenerSoundSubComponent*	SoundSubsystem::createListenerSoundSubComponent(SoundComponent* parent)
{
	ListenerSoundSubComponent* sComp = getListenerSoundSubComponent();

	
	if (sComp)
	{
		// we always want one listener so return the one existing
		*sComp = ListenerSoundSubComponent(parent);
		return sComp;
	}
	// if no listener exists
	listenerSubComponents.push_back(ListenerSoundSubComponent(parent));
	return &listenerSubComponents.back();
}

MovementSoundSubComponent* SoundSubsystem::getMovementSubComponent()
{
	for(MoveSubComponents::iterator it = moveSubComponents.begin(); it != moveSubComponents.end(); it++)
	{
		if (it->getDirty())
			return &(*it);
	}

	return NULL;
}


EffectSoundSubComponent* SoundSubsystem::getEffectSubComponent()
{
	for(EffectSubComponents::iterator it = effectSubComponents.begin(); it != effectSubComponents.end(); it++)
	{
		if (it->getDirty())
		{
			return &(*it);
		}
	}

	return NULL;
}

ListenerSoundSubComponent* SoundSubsystem::getListenerSoundSubComponent()
{
	for(ListenerSubComponents::iterator it = listenerSubComponents.begin(); it != listenerSubComponents.end(); it++)
	{
		return &(*it);
	}

	return NULL;
}

void SoundSubsystem::update()
{

	audio.update();
	music->update();

	int i = 0;
	if (listener)
	{
		listener->update();
		audio.getSystem()->set3DListenerAttributes(
			i, 
			(FMOD_VECTOR*)&listener->getPosition(),
			(FMOD_VECTOR*)&listener->getVelocity(), 
			(FMOD_VECTOR*)&Vector3(0,0,1), 
			(FMOD_VECTOR*)&Vector3(0,1,0)
			);

		++i;

	}

	
	for(Sounds::iterator it = sounds.begin(); it != sounds.end(); it++)
	{
		// checks if the soundcomponent wants to start playing a sound
		// give it a channel and start playing that sound
		if ( it->isStartPlaying() )
		{
			int id = -1;

			if(it->getIs3D())
			{
				Object* obj = it->getObject();
				Vector3 pos(0,0,0);
				if(obj)
				{
					PhysicsComponent* comp = obj->findComponent<PhysicsComponent>();

					if ( ! comp && obj->getCreator())
					{
						comp = obj->getCreator()->findComponent<PhysicsComponent>();

						
					}
					
					if (comp)
						pos = comp->getPosition();
				
					// play the sound in 3D 
					id = audio.play3D(it->getSoundName(), pos, 0.0f, it->getLooping());

					if (id == -1)
					{
						std::cout << "playing 3dsound: " << it->getSoundName() << "id: " << id << std::endl;
					}
	
				}
	
			}
			else
			{
				// else if the sound is meant to be in 2D, play it in 2D
				id = audio.play(it->getSoundName(), it->getVolume() * fxVolume * masterVolume, it->getLooping());
			}

			// set parameters for the channel
			it->setChannel(audio.getChannel(id));
			it->setIsplaying(true);
			it->setStartPlaying(false);
			it->setID(id);
			
		}
		if ( it->isPlaying() )
			it->update();
	}

	for (MoveSubComponents::iterator it = moveSubComponents.begin(); it != moveSubComponents.end(); it++)
	{
		it->update();
	}

	for (EffectSubComponents::iterator it = effectSubComponents.begin(); it != effectSubComponents.end(); it++)
	{
		it->update();
	}


}

void SoundSubsystem::clear()
{
	moveSubComponents.clear();
	effectSubComponents.clear();
	sounds.clear();

}


void SoundSubsystem::stopSoundcomponent(const int id)
{
	audio.stop(id);

	for(Sounds::iterator it = sounds.begin(); it != sounds.end(); it++)
	{
		if((*it).getChannelID() == id)
			(*it).setIsplaying(false);
	}
}

void SoundSubsystem::stopSound(const int id)
{
	audio.stop(id);
}

void SoundSubsystem::stopSoundcomponent(const std::string& soundFile)
{
	audio.stop(soundFile);
	for(Sounds::iterator it = sounds.begin(); it != sounds.end(); it++)
	{
		if((*it).getSoundName() == soundFile)
			(*it).setIsplaying(false);
	}
}

void SoundSubsystem::stopSound(const std::string& soundFile)
{
	audio.stop(soundFile);
}


void SoundSubsystem::mute()
{
	audio.muteAllSounds();
}


void SoundSubsystem::unmute()
{
	audio.unmuteAllSounds();
}


MusicPlayer* const SoundSubsystem::getMusicPlayer() const
{
	return music;
}


AudioSystem* const SoundSubsystem::getAudioSystem()
{
	return &audio;
}


void SoundSubsystem::setCamera(Camera* const cam)
{
	camera = cam;
}


void SoundSubsystem::setFXVolume(const float vol)
{
	fxVolume = vol;
}

void SoundSubsystem::setGUIVolume(const float vol)
{
	guiVolume = vol;
}

void SoundSubsystem::setMasterVolume(const float vol)
{
	masterVolume = vol;
}

void SoundSubsystem::setMusicVolume(const float vol)
{
	music->setVolume(vol);
}

const float SoundSubsystem::getFXVolume() const
{
	return fxVolume;
}

const float SoundSubsystem::getGUIVolume() const
{
	return guiVolume;
}

const float SoundSubsystem::getMasterVolume() const
{
	return masterVolume;
}

const float SoundSubsystem::getMusicVolume() const
{
	return music->getVolume();
}

void SoundSubsystem::setListener(Object* const player)
{
	SoundComponent* comp = player->findComponent<SoundComponent>();

	if (comp)
	{
		listener = createListenerSoundSubComponent(comp);
		
		comp->addSoundSubComponent(listener);
		return;
	}

	listener = NULL;
}