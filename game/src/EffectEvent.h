#ifndef EFFECT_EVENT_H
#define EFFECT_EVENT_H

#include "Event.h"
#include "SkillEffect.h"
#include "../framework/src/IntersectResult.h"

class EffectEvent : public Event
{
public:
	EffectEvent(SkillEffect * efffect = 0, bool first = false, Object * from = 0, Object * to = 0, bool remarkable = true, bool end = false);
	virtual ~EffectEvent();
	virtual void sendTo(Object * to);

	SkillEffect * getEffect();
	IntersectResult & getResult();
	Object * getFrom();
	bool isFirst();
	bool isEnd();
	bool isRemarkable();
	bool isUnrearkable();

	void setEffect(SkillEffect * effect);
	void setFirst(bool first);
	void setEnd(bool end);
	void setResult(IntersectResult result);
	void setRemarkable(bool remarkable);
	void setFrom(Object * o);

private:
	SkillEffect * effect;
	bool first;
	bool end;
	IntersectResult result;

	Object * from;

	bool remarkable;
};

#endif