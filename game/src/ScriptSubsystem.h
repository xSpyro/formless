
#ifndef SCRIPTSUBSYSTEM_H
#define SCRIPTSUBSYSTEM_H

#include "Subsystem.h"
class ScriptComponent;

/*
 * The specific tasks are described in the corresponding component header file.
 */

class ScriptSubsystem : public Subsystem
{
	typedef std::vector<ScriptComponent> Components;

public:
	ScriptSubsystem( GameHandler* gameHandler = NULL, int size = 1000 );
	virtual ~ScriptSubsystem();

	Component*	createComponent(EventLua::Object script);
	Component*	getComponent();
	void		update();
	void		clear();

	std::string getDebugString() const;

private:
	Components components;
};

#endif