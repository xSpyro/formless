
#ifndef NETWORKCOMPONENT_H
#define NETWORKCOMPONENT_H

#include "Component.h"
#include "Framework.h"

#include "CommonPackets.h"

/*	
 *	The network component handles the control of an object that is not ours,
 *	that we got over the network. It does not replace a controller component!
 */

class NetworkComponent : public Component
{
public:
	NetworkComponent( Subsystem* subsystem = NULL, unsigned int id = 0);
	virtual ~NetworkComponent();
	
	virtual void update() { }
	virtual void onPacket(RawPacket & packet);
	virtual void onDisconnect(unsigned int uid);

	virtual bool update(UpdatePacket & packet) { return true; }

	void bindObject(Object* obj);
	void unbindObject();

	virtual void receive(PrepareKillEvent * kill);
	virtual void receive(PrepareRespawnEvent * res);
	virtual void receive(LinkEvent * link);
	//virtual void receive(RespawnPlayerEvent * res) { }

	void bindToComponents();

	unsigned int getID() const;
	std::string getName( unsigned int netId ) const;

	void setReady(bool state);
	bool isReady() const;

	virtual bool isOnServer() const;

	bool killPlayer(Object * killer = 0);
	bool respawnPlayer();

protected:

	bool submit;
	unsigned int id;
};

#endif