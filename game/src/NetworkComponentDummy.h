
#ifndef NETWORKCOMPONENT_DUMMY_H
#define NETWORKCOMPONENT_DUMMY_H

#include "NetworkComponent.h"

#include "CommonPackets.h"

#include "Events.h"
class EffectEvent;

class NetworkSubsystem;

class NetworkComponentDummy : public NetworkComponent
{

public:

	NetworkComponentDummy( Subsystem* subsystem = NULL, unsigned int id = 0 );

	void update();
	void onPacket(RawPacket & packet);

	bool update(UpdatePacket & packet);

	void receive(EffectEvent * ev);
	void receive(RespawnPlayerEvent * ev);

	void makeDirty();

private:

	void processClientState(ClientStatePacket & packet);
	void processUpdate(UpdatePacket & packet);

private:

	unsigned int input;
	bool dirty;

	NetworkSubsystem * net;

	Vector3 syncPos;
	Vector3 syncPosPrev;

	unsigned int lastSync;

	bool hasSynced;
};

#endif