
#include "Helpers.h"

float lerp(float a, float b, float l)
{
	return a + (b - a) * l;
}

float lerp3(float a, float b, float c, float t, float n)
{

	float delta = t;

	//float n = 0.5f;

	float ab = lerp(a, b, std::min(delta, n) / n);
	float result = lerp(ab, c, std::max(delta - n, 0.0f) / (1 - n));

	return result;
}

float quadraticBezier(float a, float b, float c, float t)
{ 
	float va = a*(1.0f-t)*(1.0f-t);
	float vb = b*2*(1-t)*t;
	float vc = c*t*t;

	return va + vb + vc;
}

float random(float from, float to)
{
	return lerp(from, to, static_cast < float > (rand()) / RAND_MAX);
}