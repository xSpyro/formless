#ifndef CORESUBSYSTEM_H
#define CORESUBSYSTEM_H

#include "Subsystem.h"
class CoreComponent;

/*
 * The specific tasks are described in the corresponding component header file.
 */

class CoreSubsystem : public Subsystem
{
	typedef std::vector< CoreComponent > Components;

public:
	CoreSubsystem( GameHandler* gameHandler = NULL, int size = 1000 );
	virtual ~CoreSubsystem();

	Component*	createComponent( EventLua::Object script );
	Component*	getComponent();
	void		update();
	
	void clear();
	void load( EventLua::Object script, CoreComponent* component );

private:
	Components components;
};

#endif
