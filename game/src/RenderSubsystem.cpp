#include "RenderSubsystem.h"
#include "RenderComponent.h"

RenderSubsystem::RenderSubsystem( GameHandler* gameHandler, int size )
	: Subsystem( gameHandler, "RenderSubsystem" )
{
	components.reserve(size);
}

RenderSubsystem::~RenderSubsystem()
{
}

Component* RenderSubsystem::createComponent(EventLua::Object script)
{
	RenderComponent* comp = (RenderComponent*)this->getComponent();

	comp->modelName	= script.get("model").queryString("N/A");
	comp->shader	= script.get("shader").queryString("shaders/obj.vs");
	comp->texture	= script.get("texture").queryString("textures/gray.png");

	if (comp->modelName == "N/A")
	{
		comp->modelName = "test.obj";
		comp->model.setVisible(false);
	}

	EventLua::Object scriptscale	= script.get("scale");
	EventLua::Object scriptrotation = script.get("rotation");
	EventLua::Object scriptposition = script.get("position");

	if(scriptposition.isTable())
	{
		comp->position.x = scriptposition.get(1).queryFloat(0.0f);
		comp->position.y = scriptposition.get(2).queryFloat(0.0f);
		comp->position.z = scriptposition.get(3).queryFloat(0.0f);
	}
	if(scriptrotation.isTable())
	{
		comp->rotation.x = scriptrotation.get(1).queryFloat(0.0f);
		comp->rotation.y = scriptrotation.get(2).queryFloat(0.0f);
		comp->rotation.z = scriptrotation.get(3).queryFloat(0.0f);
		comp->rotation.w = scriptrotation.get(4).queryFloat(1.0f);
	}
	if(scriptscale.isTable())
	{
		comp->scale.x = scriptscale.get(1).queryFloat(1.0f);
		comp->scale.y = scriptscale.get(2).queryFloat(1.0f);
		comp->scale.z = scriptscale.get(3).queryFloat(1.0f);
	}

	comp->updateMatrix();
	comp->model.setSharedMatrix(0); // Of some reason the matrix doesn't point at the native matrix... Odd...
	comp->modelDebug.setSharedMatrix(0);

	return comp;
}

Component* RenderSubsystem::getComponent()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if( !(*it).isBound() )
		{
			(*it) = RenderComponent( this );
			it->model.setSharedMatrix(0);
			return &(*it);
		}
	}
	if (components.size() == components.capacity())
		return 0;

	components.push_back( RenderComponent( this ) );
	return &components.back();
}

void RenderSubsystem::update()
{
	// no update needed
	/*
	for( Components::iterator it = components.begin(); it < components.end(); it++ )
	{
		if(it->isBound() && it->activated)
			it->update();
	}
	*/
}

void RenderSubsystem::clear()
{
	// for( Components::iterator it = components.begin(); it != components.end(); ++it )
	// {
		//it->~RenderComponent();
	// }

	components.clear();
}