#ifndef EFFECT_SOUND_SUB_COMPONENT_H

#define EFFECT_SOUND_SUB_COMPONENT_H

#include "SoundSubComponent.h"

class PhysicsComponent;

class EffectSoundSubComponent : public SoundSubComponent
{
public:
	EffectSoundSubComponent(SoundComponent* p, float spread, float doppler, float directOcclusion, float reverbOcclusion, bool useCreatorPosition);
	~EffectSoundSubComponent();
	
	void updateSubComponent();

private:
	float spread;
	float doppler;
	float directOcclusion;
	float reverbOcclusion;

	PhysicsComponent* parentPhys;

	bool useCreatorPosition;
};


#endif