#include "SkillEvent.h"
#include "EventEnum.h"
#include "Object.h"

SkillEvent::SkillEvent(Object * skill, std::string name)
	: Event( EVENT_SKILL ), skill(skill), name(name)
{}

SkillEvent::~SkillEvent()
{
}

void SkillEvent::sendTo( Object* to )
{
	to->receive<SkillEvent>( this );
}
