#ifndef MOVEMENT_SOUND_SUB_COMPONENT_H

#define MOVEMENT_SOUND_SUB_COMPONENT_H

#include "SoundSubComponent.h"
#include "PhysicsComponent.h"

class PhysicsComponent;

class MovementSoundSubComponent : public SoundSubComponent
{
public:
	MovementSoundSubComponent(SoundComponent* p);
	~MovementSoundSubComponent();

	void updateSubComponent();

	void receive(EffectEvent* effect);

	void receive(KillPlayerEvent * kill);

private:
	Vector3 lastPos;

	PhysicsComponent* parentPhys;
};


#endif