#ifndef SKILLCOMPONENT_H
#define SKILLCOMPONENT_H

#include "Component.h"
#include "SkillEffect.h"
class StatsComponent;

/*	
 *	The skill component handles the activation of skills and skill effects.
 *	If an object has no skill component, it cannot use skills or be affected by them.
 */

class PhysicsComponent;

class SkillComponent : public Component
{

public:

	SkillComponent( Subsystem* subsystem = NULL );
	SkillComponent( EventLua::Object* script, Subsystem* subsystem = NULL);
	virtual ~SkillComponent();

	void receive(TimerEvent* timer);
	void receive(DestroyEvent * de);

	void update();

	void addEffect(Object * target, Object * caster, bool remarkable = true);
	void repeatEffect(Object * o, bool remarkable = true);
	void removeEffect(Object * o);

	bool isEffecting(Object * o);
	std::string getCasterAnimation();
	std::string getSound();

	void bindObject(Object* obj);
	void unbindObject();

	void bindToComponents();
public:
	SkillEffect effect;
private:
	typedef std::vector<Object *> Objects;
	Objects effected;
	std::string casterAnimation;
	std::string sound;

	StatsComponent* stats;

	std::vector<Object*> subscriptionTargets;
};

#endif
