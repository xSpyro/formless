#include "LinkEvent.h"
#include "EventEnum.h"
#include "Object.h"


LinkEvent::LinkEvent(Object * linkObject, Object * owner, Object * target, bool state)
	: Event(EVENT_LINK, 0,  true), link(linkObject), owner(owner), target(target), state(state)
{}

LinkEvent::~LinkEvent()
{}

Object* LinkEvent::getLink()
{
	return link;
}

Object* LinkEvent::getOwner()
{
	return owner;
}

Object* LinkEvent::getTarget()
{
	return target;
}

bool LinkEvent::getState()
{
	return state;
}

void LinkEvent::setLink(Object* obj)
{
	link = obj;
}

void LinkEvent::setOwner(Object* obj)
{
	owner = obj;
}

void LinkEvent::setTarget(Object* obj)
{
	target = obj;
}

void LinkEvent::setState(bool linkState)
{
	state = linkState;
}

void LinkEvent::sendTo(Object * to)
{
	to->receive<LinkEvent>(this);
}

