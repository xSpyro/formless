
#ifndef STATSSUBSYSTEM_H
#define STATSSUBSYSTEM_H

#include "Subsystem.h"

class StatsComponent;

class StatsSubsystem : public Subsystem
{
	typedef std::vector<StatsComponent> Components;

public:
	StatsSubsystem( GameHandler* gameHandler = NULL );
	virtual ~StatsSubsystem();

	Component* createComponent(EventLua::Object script);
	Component* getComponent();

	void update();
	void clear();

	
	std::string getDebugString() const;

private:
	Components components;
};

#endif