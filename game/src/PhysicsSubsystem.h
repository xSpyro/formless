
#ifndef PHYSICSSUBSYSTEM_H
#define PHYSICSSUBSYSTEM_H

#include "Subsystem.h"
#include "CollisionEvent.h"

class Layer;
class PhysicsComponent;
class Surface;
class Terrain;

/*
 * The specific tasks are described in the corresponding component header file.
 */

class PhysicsSubsystem : public Subsystem
{
	struct Collision
	{
		PhysicsComponent * rhs;
		PhysicsComponent * lhs;
		IntersectResult res;
	};
	struct Trigger
	{
		IntersectResult res;
		bool alive;
	};
	typedef std::vector<PhysicsComponent *> ComponentPointers;
	typedef std::vector<PhysicsComponent> Components;
	typedef std::vector<Collision> Collisions;
	typedef std::pair<PhysicsComponent *, PhysicsComponent *> PhysicsPair;
	typedef std::map<PhysicsPair, Trigger> Triggers;
	typedef std::map<PhysicsComponent *, bool> TriggerTerrain;

public:
	PhysicsSubsystem( GameHandler* gameHandler = NULL, int size = 1000 );
	virtual ~PhysicsSubsystem();

	Component*	createComponent(EventLua::Object script);
	Component*	getComponent();
	void		update();
	void		clear();

	std::string getDebugString() const;

	void		setTerrain(Terrain * terrain);
	void		forgetTrigger(PhysicsComponent * phys, PhysicsComponent * phys2); // Forgets the collision between phys and phys2, will make it so that if the object still is inside trigger enter will be called again.
	void		forgetAllTriggers(PhysicsComponent * phys); // Forgets the collision between phys and everything, will make it so that if the object still is inside trigger enter will be called again.

	ComponentPointers getInside(PhysicsComponent * phys);
private:
	void checkTrigger(Collision & collision);

	Components components;
	Collisions collisions;
	Triggers triggers;
	TriggerTerrain terrain;


	Terrain * heightMap;
};

#endif