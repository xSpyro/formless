
#include "TrailField.h"

#include "AIComponent.h"
#include "PhysicsComponent.h"

TrailField::TrailField(AIComponent* comp /* = NULL */)
	: PotentialField(), component(comp)
{
	for(unsigned int i = 0; i < TRAIL_SIZE; i++)
	{
		trail[i] = Vector2(0,0);
	}
}

TrailField::~TrailField()
{
}

float TrailField::calcPotential(Vector2 position)
{
	float returnValue = 0.0f;

	if(component)
	{
		Vector2 transformed = transformToField(position);
		returnValue = field[ (int)transformed.x ] [ (int)transformed.y ];
	}

	return returnValue;
}

void TrailField::update(Vector2 AIposition)
{
	//Move all the positions back
	for(unsigned int i = TRAIL_SIZE-1; i > 0; i--)
		trail[i] = trail[i-1];

	//Then put the current position in front
	trail[0] = transformToField(AIposition);


	//Give negative values to the trail
	//Making the lowest negative value to TRAIL_SIZE - 1, causes the
	//last trail position to get the value 0, therefore resetting it
	int potentialValue = (TRAIL_SIZE - 1) * -2;
	for(int i = 0; i < TRAIL_SIZE; i++)
	{
		int x = (int)trail[i].x;
		int y = (int)trail[i].y;

		//We want to spread the effect of the trail to all positions up to
		//5 units away from the trail point
		int xmin = x - 5;
		int ymin = y - 5;
		int xmax = x + 5;
		int ymax = y + 5;

		//But we don't want to be outside the array
		if(xmin < 0)
			xmin = 0;
		if(ymin < 0)
			ymin = 0;
		if(xmax > NR_COLS)
			xmax = NR_COLS - 1;
		if(ymax > NR_ROWS)
			ymax = NR_ROWS - 1;

		for(int row = ymin; row < ymax; row++)
		{
			for(int col = xmin; col < xmax; col++)
			{
				field[col][row] = (float)potentialValue;
			}
		}

		potentialValue += 2;
	}
}