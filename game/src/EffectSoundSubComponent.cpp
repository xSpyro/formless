#include "EffectSoundSubComponent.h"


EffectSoundSubComponent::EffectSoundSubComponent(SoundComponent* p, float spread, float doppler, float directOcclusion, float reverbOcclusion, bool useCreatorPosition)
	: SoundSubComponent(p)
{
	this->spread = spread;
	this->doppler = doppler;
	this->directOcclusion = directOcclusion;
	this->reverbOcclusion = reverbOcclusion;

	parentPhys = NULL;

	this->useCreatorPosition = useCreatorPosition;
}

EffectSoundSubComponent::~EffectSoundSubComponent()
{

}


void EffectSoundSubComponent::updateSubComponent()
{
	if ( parent->isPlaying() && parent->getObject() )
	{
		if( parentPhys == NULL && !useCreatorPosition)
			parentPhys = parent->getObject()->findComponent<PhysicsComponent>();
		if (parentPhys == NULL)
			parentPhys = parent->getObject()->getCreator()->findComponent<PhysicsComponent>();

		if ( parentPhys )
		{

			FMOD::Channel* channel = parent->getChannel();

			channel->set3DAttributes((FMOD_VECTOR*)&parentPhys->getPosition(), (FMOD_VECTOR*)&parentPhys->getActualVelocity());

			channel->set3DSpread(spread);

			channel->set3DOcclusion(directOcclusion, reverbOcclusion);

			channel->set3DDopplerLevel(doppler);

		}

	}
}