#ifndef TIMERCOMPONENT_H
#define TIMERCOMPONENT_H

#include "Component.h"

class TimerComponent : public Component
{
private:
	struct Timer
	{
		unsigned int	max;
		unsigned int	tick;
		int				id;
	};

public:
	TimerComponent( Subsystem* subsystem = NULL );
	TimerComponent( EventLua::Object* script, Subsystem* subsystem = NULL );
	virtual ~TimerComponent();

	void update();
	void restart(int id);

	bool checkTimer(int timer); // Returns true if timer have been triggered(after last restart) false otherwise.

private:
	std::vector<Timer>	timers;
};

#endif
