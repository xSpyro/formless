
#include "NetworkComponentDummy.h"
#include "NetworkSubsystem.h"
#include "NetworkSubsystemServer.h"
#include "GameHandler.h"

#include "CommonPackets.h"

#include "Events.h"

#include "PhysicsComponent.h"
#include "ControllerComponent.h"
#include "CoreComponent.h"

NetworkComponentDummy::NetworkComponentDummy( Subsystem* subsystem, unsigned int id )
	: NetworkComponent(subsystem, id)
{
	net = reinterpret_cast < NetworkSubsystem* > (subsystem);
	dirty = true;

	syncPos = 0;
	syncPosPrev = 0;

	lastSync = 0;
}

void NetworkComponentDummy::update()
{
	PhysicsComponent* physics = object->findComponent<PhysicsComponent>();


	float slowness = 4.0f;
	float forward = 5.0f;

	static EventLua::State state;
	if (state.execute("../ClientPrediction.lua"))
	{
		EventLua::Object g = state.getGlobals();
		slowness = g["slowness"].queryFloat(30.0f);
		forward = g["forward"].queryFloat(60.0f);
	}

	// take prev pos
	Vector3 dir = (syncPos - syncPosPrev);

	float len = dir.length();
	if (len == 0)
	{
		physics->position += (syncPos - physics->position) / slowness;
	}
	else
	{
		Vector3 n = dir / len;
		n.y = 0;

		physics->position += ((syncPos + n * forward) - physics->position) / slowness;
	}
}

void NetworkComponentDummy::receive(EffectEvent * ev)
{
	dirty = true;
}


void NetworkComponentDummy::receive(RespawnPlayerEvent * ev)
{
	Component::receive(ev);
	syncPos = ev->getPosition();
	syncPosPrev = syncPos;
}

void NetworkComponentDummy::makeDirty()
{
	dirty = true;
}


void NetworkComponentDummy::onPacket(RawPacket & raw)
{
	if (!object)
		return;

	switch (raw.header.id)
	{
	case CLIENT_STATE_PACKET:
		{
			ClientStatePacket * packet = reinterpret_cast < ClientStatePacket* > (raw.data);

			processClientState(*packet);
		}
		break;

	case UPDATE_PACKET:
		{
			UpdatePacket * update = reinterpret_cast < UpdatePacket* > (raw.data);

			processUpdate(*update);
		}
		break;
	}
}

bool NetworkComponentDummy::update(UpdatePacket & packet)
{
	// happens server side

	if (!dirty && lastSync < 5)
	{
		lastSync ++;
		return false;
	}

	packet.input = input;

	input = 0;

	dirty = false;
	lastSync = 0;


	return true;
}

void NetworkComponentDummy::processClientState(ClientStatePacket & packet)
{
	ControllerComponent* controller = object->findComponent<ControllerComponent>();

	if (controller)
	{
		controller->setMousePosition(Vector3(packet.lx, packet.ly, packet.lz));
	}

	
	{
		KeyBindEvent ev(packet.input & 1, "up");
		object->sendInternal(&ev);
	}
	{
		KeyBindEvent ev(packet.input & 2, "down");
		object->sendInternal(&ev);
	}
	{
		KeyBindEvent ev(packet.input & 4, "left");
		object->sendInternal(&ev);
	}
	{
		KeyBindEvent ev(packet.input & 8, "right");
		object->sendInternal(&ev);
	}
	

	//std::cout << "in state: " << (packet.input & 15) << std::endl;
	//std::cout << "old state: " << (input & 15) << std::endl;

	if ((input & 15) != (packet.input & 15))
	{
		dirty = true;
	}


	bool activated_skill = false;

	input = packet.input & 7;

	for (int n = 0, i = 16; n < 32; i *= 2, ++n)
	{
		if (packet.input & i)
		{
			Object * skill = NULL;
			ActivateSkillEvent act(n, false, NULL, &skill, false);
			object->sendInternal(&act);


			// only if we activated a skill
			// we set the input for that
			
			// so that the next time we update we reset
			if (act.isSuccsess())
			{
				activated_skill = true;
				input = input | i;
			}
			else
				activated_skill = false;
		}
	}

	// input = packet.input;

	if (activated_skill)
	{
		dirty = true;
		net->updateComponent(this);
	}
}

void NetworkComponentDummy::processUpdate(UpdatePacket & packet)
{
	if (object)
	{
		/*
		{
			KeyBindEvent ev(packet.input & 1, "up");
			object->sendInternal(&ev);
		}
		{
			KeyBindEvent ev(packet.input & 2, "down");
			object->sendInternal(&ev);
		}
		{
			KeyBindEvent ev(packet.input & 4, "left");
			object->sendInternal(&ev);
		}
		{
			KeyBindEvent ev(packet.input & 8, "right");
			object->sendInternal(&ev);
		}
		*/



		CoreComponent* core = object->findComponent<CoreComponent>();
		if (core)
		{
			// set life & mana
			core->life.setCurrent(packet.life);
			core->mana.setCurrent(packet.mana);
		}
		
		ControllerComponent* controller = object->findComponent<ControllerComponent>();
		if (controller) 
		{
			controller->setMousePosition(Vector3(packet.lx, packet.ly, packet.lz));
		}
		
		//std::cout << "input on dummy: " << packet.input << std::endl;
		
		for (int n = 0, i = 16; n < 32; i *= 2, ++n)
		{
			if (packet.input & i)
			{
				object->sendInternal(&ActivateSkillEvent(n, false, 0, 0, true));
			}
		}

		PhysicsComponent* physics = object->findComponent<PhysicsComponent>();
		if (physics)
		{
			if (!net->isServer())
				physics->velocity = Vector3(packet.vx, packet.vy, packet.vz); // Set the velocity, but only on the client
			
			// previous position
			Vector3 prev = physics->position; 
			Vector3 next = Vector3(packet.x, packet.y, packet.z);

			/*
			float l = (next - prev).length();

			physics->position = next;	
			l = std::min(l, 40.0f);

			Vector3 n = 0;
			if (l != 0)
				n = (next - prev) / l;

			physics->velocity = n * l * 0.5f;
			physics->velocity.y = 0;

			*/



			syncPosPrev = syncPos;
			syncPos = next;
		}
	}
}