
#ifndef MODULEFACTORY_H
#define MODULEFACTORY_H

#include "Common.h"
#include "Module.h"

class ModuleFactory
{
	typedef std::map< std::string, Module >		ModuleTemplates;
	typedef std::map< unsigned int, Module* >	Modules;

public:
	ModuleFactory();
	~ModuleFactory();

	void	loadModules( EventLua::Object script );
	Module*	getModule( unsigned int id, std::string name );

private:
	ModuleTemplates moduleTemplates;
	Modules			modules;
};

#endif