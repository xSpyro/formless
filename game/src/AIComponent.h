
#ifndef AICOMPONENT_H
#define AICOMPONENT_H

#include "Component.h"
#include "Framework.h"
class Object;
class PhysicsComponent;
class CoreComponent;
class PotentialField;
class NetworkSubsystem;
class NetworkComponent;

/*	
 *	The AI component basics are very similar to the controller component.
 *	It can do all that the controller component can, and more. It's the controlling of 
 *	a player that is not user controlled.
 */

const float VIEW_DIST		= 600.0f;
const float SKILL_RANGE		= 200.0f;

class AIComponent : public Component
{
public:
	struct Player
	{
		Object*				object;
		PhysicsComponent*	phys;
		CoreComponent*		core;
		Vector2				xzDistance;
		float				distance;

		Player()
		{
			object	= NULL;
			phys	= NULL;
			core	= NULL;
			xzDistance	 = Vector2(0,0);
		}
	};

	typedef std::vector<Player> Players;
	typedef std::vector<PotentialField*> Fields;

public:
	AIComponent( Subsystem* subsystem = NULL );
	virtual ~AIComponent();

	void receive(KeyEvent* key);
	void receive(KeyBindEvent* keyBind);
	void receive(MouseEvent* mouse);
	void receive(EffectEvent * ev);
	void receive(QueryEvent<Vector3> * qv);
	void receive( KillPlayerEvent* ev );
	void receive( RespawnPlayerEvent* ev );
	void receive(DestroyEvent * ev);
	void receive(CreatedEvent * ev);
	void receive(GameStartEvent* ev);
	void receive(LinkEvent* ev);

	void update();

	void clearStates();

	void setSpeed( float speed );
	void setActiveSlot( int slot );
	void updateMovement();

	void bindObject(Object* obj);
	void unbindObject();

	void bindToComponents();

	void setTargetPosition(const Vector3& position);
	const Vector3 & getTargetPosition() const;

	const Vector3 getDirection() const;

	void giveCommand(std::string command, int state);

	void	addGamePlayer(Object* newPlayer);
	void	setMaster(Object* player);
	Object* getMaster();

	void					getMyTeam(std::vector<Player>& inVector);
	void					getOtherTeam(std::vector<Player>& inVector);
	AIComponent::Player&	getStructMe();

	bool	isIdle();

public:
	std::vector<std::string> commands;
	
	unsigned int masterUserID;

private:
	void handlePotentialFields();
	void handleAttack();

	void buildPotentialFields();
	void clearPotentialFields();

private:
	PhysicsComponent*				physcomponent;
	CoreComponent*					corecomponent;
	NetworkComponent*				netcomponent;
	Vector3							velocity;
	std::map< std::string, int >	commandStates;
	float							speed;

	int	activeSlot;

	int	cooldown;
	int	stunTicker;
	int	blockSkills;

	Vector3	targetPosition;

	Vector3	toMiddle;

private:
	Player	master;
	Player	me;

	Players	myTeam;
	Players	otherTeam;
	Players	allPlayers;
	Players myTeamInRange;
	Players otherTeamInRange;

	Fields	pFields;

	NetworkSubsystem* netSub;

	Timer	attackTimer;
	Timer	healTimer;
	Timer	gameCountdown;

	bool	idle;

	bool	linked;
	Object*	linkedWith;

	//Way too strict and hard coded, temporary solution
	int		boltSlot;
	int		healSlot;
	int		chargeSlot;
};

#endif