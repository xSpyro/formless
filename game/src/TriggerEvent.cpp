#include "TriggerEvent.h"
#include "EventEnum.h"
#include "Object.h"

TriggerEvent::TriggerEvent(TriggerException ex, bool enter /* = false */) : Event(EVENT_TRIGGER), other(0), enter(enter), exceptionType(ex)
{}

TriggerEvent::TriggerEvent(Object * other, bool enter) : Event(EVENT_TRIGGER), other(other), enter(enter), exceptionType(TRIGGER_NONE)
{}

TriggerEvent::TriggerEvent(Object * other, bool enter, IntersectResult res) : Event(EVENT_TRIGGER), other(other), enter(enter), res(res), exceptionType(TRIGGER_NONE)
{}

TriggerEvent::~TriggerEvent()
{}

bool TriggerEvent::entering()
{
	return enter;
}

bool TriggerEvent::leaving()
{
	return !enter;
}

Object * TriggerEvent::getOther()
{
	return other;
}

IntersectResult TriggerEvent::getResult()
{
	return res;
}

void TriggerEvent::setEntering(bool e)
{
	enter = e;
}

void TriggerEvent::setOther(Object * o)
{
	other = o;
}

void TriggerEvent::setResult(IntersectResult r)
{
	res = r;
}

void TriggerEvent::sendTo(Object * to)
{
	to->receive<TriggerEvent>(this);
}

TriggerException TriggerEvent::getException()
{
	return exceptionType;
}