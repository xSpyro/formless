#include "TimerSubsystem.h"
#include "TimerComponent.h"
#include "Object.h"

#include "GameHandler.h"

TimerSubsystem::TimerSubsystem( GameHandler* gameHandler, int size)
	: Subsystem( gameHandler, "TimerSubsystem" )
{
	components.reserve(size);
}

TimerSubsystem::~TimerSubsystem()
{
}

Component* TimerSubsystem::createComponent( EventLua::Object script )
{
	TimerComponent* comp = (TimerComponent*)this->getComponent();
	*comp = TimerComponent( &script, this );

	return comp;
}

Component* TimerSubsystem::getComponent()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if( !it->isBound() )
			return &(*it);
	}

	components.push_back( TimerComponent( this ) );
	return &components.back();
}

void TimerSubsystem::update()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if(it->isBound() && it->activated && !(it->getObject()->stacked))
			it->update();
	}
}

void TimerSubsystem::clear()
{
	components.clear();
}

std::string	TimerSubsystem::getDebugString() const
{
	unsigned int out = 0;
	for( Components::const_iterator it = components.begin(); it != components.end(); ++it )
	{
		if(it->isBound() && it->activated && !(it->getObject()->stacked))
			++out;
	}

	std::stringstream format;
	format << "instances: " << out;
	return format.str();
}
