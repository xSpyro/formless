#ifndef TRAIT_H
#define TRAIT_H

struct Trait
{
	Trait()
	{
		offset = 0.0f;
		modifier = 0.0f;
	}

	float offset;
	float modifier;
};

#endif
