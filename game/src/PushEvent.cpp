#include "PushEvent.h"
#include "EventEnum.h"
#include "Object.h"

PushEvent::PushEvent(): push(Vector3(0.0f, 0.0f, 0.0f)), Event(EVENT_PUSH, 0, true)
{}

PushEvent::PushEvent(Vector3 velocity) : push(velocity), Event(EVENT_PUSH, 0, true)
{}

PushEvent::~PushEvent()
{
}

void PushEvent::sendTo(Object * to)
{
	to->receive<PushEvent>(this);
}

Vector3 PushEvent::getMovement()
{
	return push;
}

void PushEvent::setMovement(Vector3 move)
{
	push = move;
}