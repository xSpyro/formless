#ifndef GAME_START_EVENT_H
#define GAME_START_EVENT_H
#include "Event.h"

class GameStartEvent : public Event // Try to activate a skill
{
public:
	GameStartEvent();
	~GameStartEvent();


	void sendTo(Object * o);

private:

};

#endif