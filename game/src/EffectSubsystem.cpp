#include "EffectSubsystem.h"

#include "EffectComponent.h"

EffectSubsystem::EffectSubsystem(GameHandler* gameHandler)
	: Subsystem(gameHandler, "EffectSubsystem")
{
	components.reserve(10000);
}

EffectSubsystem::~EffectSubsystem()
{

}

Component* EffectSubsystem::createComponent(EventLua::Object script)
{
	EffectComponent* comp = (EffectComponent*)this->getComponent();

	comp->setName(script.get("name").queryString());

	return comp;
}

Component* EffectSubsystem::getComponent()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if( !(*it).isAlive() )
		{
			(*it) = EffectComponent( this );
			return &(*it);
		}
	}
	if (components.size() == components.capacity())
		return 0;

	components.push_back( EffectComponent( this ) );
	return &components.back();
}

void EffectSubsystem::update()
{
}

void EffectSubsystem::clear()
{
	components.clear();
}

std::string EffectSubsystem::getDebugString() const
{
	unsigned int out = 0;
	for( Components::const_iterator it = components.begin(); it != components.end(); ++it )
	{
		if(it->isBound() && it->activated)
			++out;
	}

	std::stringstream format;
	format << "instances: " << out;
	return format.str();
}