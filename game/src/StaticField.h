
#ifndef STATIC_FIELD_H
#define STATIC_FIELD_H

#include "PotentialField.h"

class StaticField : public PotentialField
{

public:
	StaticField();
	virtual ~StaticField();

	float calcPotential(Vector2 position);

private:
};

#endif