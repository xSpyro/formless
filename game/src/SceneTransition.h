
#ifndef SCENE_TRANSITION
#define SCENE_TRANSITION

#include "Common.h"

class SceneTransition
{

public:

    virtual ~SceneTransition() { }

    virtual void onBegin(int frames) = 0;
	virtual void onFadeChange() = 0;

    virtual void onFadeIn() = 0;
	virtual void onFadeOut() = 0;
};

#endif