#include "CoreSubsystem.h"
#include "CoreComponent.h"
#include "AnimationComponent.h"
#include "ShapeAnimation.h"
#include "Object.h"


CoreSubsystem::CoreSubsystem( GameHandler* gameHandler, int size )
	: Subsystem( gameHandler, "CoreSubsystem" )
{
	components.reserve( size );
}

CoreSubsystem::~CoreSubsystem()
{
}

Component* CoreSubsystem::createComponent( EventLua::Object script )
{
	CoreComponent* comp = (CoreComponent*)this->getComponent();

	load(script, comp);

	return comp;
}

Component* CoreSubsystem::getComponent()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if( !it->isBound() )
		{
			(*it) = CoreComponent( this );
			return &(*it);
		}
	}

	components.push_back( CoreComponent( this ) );
	return &components.back();
}

void CoreSubsystem::update()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if(it->isBound() && it->activated)
			it->update();
	}
}

void CoreSubsystem::clear()
{
	components.clear();
}

void CoreSubsystem::load( EventLua::Object script, CoreComponent* comp )
{
	std::string name = script.get( "name" ).queryString();
	comp->name = name;

	float life = script.get( "life" ).queryFloat();
	comp->life.setBase( life );

	float mana = script.get( "mana" ).queryFloat();
	comp->mana.setBase( mana );

	comp->lifeRegen.setBase(script.get("lifeRegen").queryFloat());
	comp->manaRegen.setBase(script.get("manaRegen").queryFloat());

	float speed = script.get( "speed" ).queryFloat();
	comp->speed.setBase( speed );

	int totalslots = script.get( "nrSlots" ).queryInteger();
	int skillslots = script.get( "nrSkills" ).queryInteger();
	comp->setSlots( totalslots, skillslots );

	comp->life.revive();
	comp->mana.revive();
}