#ifndef LISTENER_SOUND_SUB_COMPONENT_H

#define LISTENER_SOUND_SUB_COMPONENT_H

class PhysicsComponent;

#include "SoundSubComponent.h"
class ListenerSoundSubComponent : public SoundSubComponent
{
public:
	ListenerSoundSubComponent(SoundComponent* p);
	~ListenerSoundSubComponent();

	void updateSubComponent();
	
	const Vector3 getPosition();

	const Vector3 getVelocity();

	const int getID();

	void setPosition(const Vector3& position);

	void setVelocity(const Vector3& velocity);
	
	void setID(const int id);

	void receive(RespawnPlayerEvent * res);
private:
	Vector3 position;
	Vector3 velocity;
	int id;

	PhysicsComponent* parentPhys;
};


#endif	