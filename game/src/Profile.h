
#ifndef PROFILE_H
#define PROFILE_H

#include "Framework.h"

class Profile
{
public:

	Profile(const std::string & name, bool auto_log = true);
	~Profile();

	void reset();

	void begin();
	void end();

	

private:

	void output();
	void update();

private:

	Logger log;
	std::string name;
	Timer timer;
	Timer second;

	unsigned int times;
	float acc;

	float avg_ms[2];
	float max_ms[2];
	float min_ms[2];

	bool auto_log;
};

#endif