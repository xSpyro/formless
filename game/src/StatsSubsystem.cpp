
#include "StatsSubsystem.h"
#include "StatsComponent.h"

StatsSubsystem::StatsSubsystem( GameHandler* gameHandler )
	: Subsystem( gameHandler, "StatsSubsystem" )
{
	components.reserve(1000);
}

StatsSubsystem::~StatsSubsystem()
{

}


Component* StatsSubsystem::createComponent(EventLua::Object script)
{
	return getComponent();
}

Component* StatsSubsystem::getComponent()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if( !(*it).isAlive() )
		{
			(*it) = StatsComponent( this );
			return &(*it);
		}
	}
	if (components.size() == components.capacity())
		return 0;

	components.push_back( StatsComponent( this ) );
	return &components.back();
}

void StatsSubsystem::update()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if(it->isBound() && it->activated)
			it->update();
	}
}

void StatsSubsystem::clear()
{
	components.clear();
}

std::string StatsSubsystem::getDebugString() const
{
	unsigned int out = 0;
	for( Components::const_iterator it = components.begin(); it != components.end(); ++it )
	{
		if(it->isBound() && it->activated)
			++out;
	}

	std::stringstream format;
	format << "instances: " << out;
	return format.str();
}