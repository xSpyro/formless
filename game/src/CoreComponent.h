#ifndef CORECOMPONENT_H
#define CORECOMPONENT_H

#include "Component.h"
#include "Attribute.h"

class Module;
class PhysicsComponent;
struct CoreAttrPacket;
struct ChangeModulePacket;

class CoreComponent : public Component
{
public:
	typedef std::vector< Module* >	Modules;

private:
	typedef std::vector<bool> Bools;

public:
	CoreComponent( Subsystem* subsystem = NULL );
	virtual ~CoreComponent();

	void		setSlots( int total, int skills );			// Adjusts the vector size
	int			getTotalSlots();
	int			getSkillSlots();

	void			addModule( int slot, unsigned int id, const std::string name );	// Will overwrite any previous module in [slot]
	std::string		removeModule( int slot );
	int				removeModule( const std::string module );
	Module*			getModule( int slot );						// Returns NULL if [slot] is empty
	Object*			activateModule( int slot, bool fromServer = false);	// Returns NULL if [slot] is empty OR insufficient resources. If fromServer the it tries to force creating independent of resources
	void			getModuleSlots( Modules& mods );

	void		bindToComponents();
	void		setCoreAttributes();

	void		receive( EffectEvent* effect );
	void		receive( ActivateSkillEvent* effect );
	void		receive( ConfirmSkill* effect );
	void		receive( KillPlayerEvent* kill );
	void		receive( RespawnPlayerEvent* res );
	void		receive( QueryEvent<Vector3>* effect );
	void		receive( DestroyEvent * de);
	void		receive( LinkEvent * link );

	void		revive();
	void		drain();

	void		update();

	void		setWaitAt(int pos, bool val = true);
	bool		getWaitAt(int pos);

	CoreAttrPacket		getAttrPacket();
	ChangeModulePacket	getModulePacket(const unsigned int & slot);

	bool		isPlayerAlive();

	Object * getLink(); // Link that I own
	std::map<Object *, Object *> getLinks(); // Links that I am target for. (link, owner)

	void unlink(Object * link, Object * target = 0);

private:
	bool		pay(Module * mod);

public:
	Attribute	life;
	Attribute	mana;
	Attribute	lifeRegen;
	Attribute	manaRegen;
	Attribute	speed;
	Attribute	damageDealt;
	Attribute	damageRecieved;

	std::string name;
	std::string idleAnimation;
	unsigned int id;

	unsigned int team;
private:
	int		nrSkills;
	int		nrSlots;			// Use get/set functions for this to ensure that the module vector size stays correct
	Modules modules;
	Bools	waiting;

	bool	playerAlive;

	PhysicsComponent * phys;
	float offset;
	float dir;

	int hitTicker;
	int tickHover;
	Object * link;
	std::map<Object *, Object *> linked; // Link, owner
};

#endif
