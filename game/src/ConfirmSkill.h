#ifndef CONFIRM_SKILL_H
#define CONFIRM_SKILL_H
#include "Event.h"

class ConfirmSkill : public Event
{
public:
	ConfirmSkill(int skill = 0, bool allow = true);
	~ConfirmSkill();

	int getSkill();
	bool isAllowed();

	void sendTo(Object * o);
private:
	int skill;
	bool allowed;
};

#endif