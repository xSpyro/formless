#ifndef PREPARE_KILL_EVENT_H
#define PREPARE_KILL_EVENT_H
#include "Event.h"

class PrepareKillEvent : public Event
{
public:
	PrepareKillEvent(Object * killer = 0, Object * sender = 0);
	virtual ~PrepareKillEvent();

	virtual void sendTo(Object* to);

	void setKiller(Object * o);
	void setKilled(Object * o);

	Object * getKiller();
	Object * getKilled();
private:
	Object * killer;
};

#endif