
#ifndef MUSIC_PLAYER_H

#define MUSIC_PLAYER_H

#include "Common.h"
#include "SoundSubsystem.h"

class SoundSubsystem;


class MusicPlayer
{
	typedef std::vector<std::string> Playlist;
	typedef std::map<std::string, Playlist> Songs;

	

public:
	MusicPlayer(SoundSubsystem* s);
	~MusicPlayer();

	void init();

	void queryAllSongs();
	
	void update();

	void play(const std::string& song);
	
	void playWithRandomPosition(const std::string& song);

	void stop();

	void randomizeSongFromCurrentContainer(bool randomPosition);

	void next();

	void previous();

	void tellCurrentPlaylist(const std::string& c);

	void setVolume(const float volume);

	const float getVolume() const;

	const std::string getNameOfCurrentPlaylist() const;

	void mute();

	void unmute();

	const bool isMuted() const;

	void lowerVolume();

	void resetVolume();

private:
	Songs songs;

	int id;

	SoundSubsystem* sound;

	std::string nameOfCurrentContainer;
	
	Playlist* currentContainer;

	Playlist::iterator currentSong;

	Playlist::iterator secondSong;

	float volume;

	float oldVolume;

	bool muted;
};


#endif