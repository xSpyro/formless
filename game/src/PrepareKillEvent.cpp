#include "PrepareKillEvent.h"
#include "EventEnum.h"
#include "Object.h"

PrepareKillEvent::PrepareKillEvent(Object * killer /* = 0 */, Object * sender /* = 0 */) : Event(EVENT_PREPARE_KILL, sender), killer(killer)
{
}

PrepareKillEvent::~PrepareKillEvent()
{
}

void PrepareKillEvent::sendTo(Object* to)
{
	to->receive<PrepareKillEvent>(this);
}

void PrepareKillEvent::setKiller(Object * o)
{
	killer = o;
}

void PrepareKillEvent::setKilled(Object * o)
{
	sender = o;
}

Object * PrepareKillEvent::getKiller()
{
	return killer;
}

Object * PrepareKillEvent::getKilled()
{
	return sender;
}