
#include "EffectComponent.h"

EffectComponent::EffectComponent( Subsystem* subsystem /* = NULL */, std::string name /* = "" */ )
	: Component(subsystem), name(name)
{

}

EffectComponent::~EffectComponent()
{

}

void EffectComponent::update()
{

}

void EffectComponent::bindToComponents()
{

}

void EffectComponent::setName(const std::string n)
{
	name = n;
}

std::string EffectComponent::getName() const
{
	return name;
}