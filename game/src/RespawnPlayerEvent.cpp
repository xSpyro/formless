#include "RespawnPlayerEvent.h"
#include "EventEnum.h"
#include "Object.h"

RespawnPlayerEvent::RespawnPlayerEvent( float x, float y, float z, bool countRespawn ) : Event(EVENT_RESPAWN_PLAYER), position(Vector3(x,y,z)), count(countRespawn)
{

}

RespawnPlayerEvent::RespawnPlayerEvent( Vector3 pos, bool countRespawn ) : Event(EVENT_RESPAWN_PLAYER, 0), position(pos), count(countRespawn)
{

}

RespawnPlayerEvent::~RespawnPlayerEvent()
{
}

Vector3 RespawnPlayerEvent::getPosition() const
{
	return position;
}

bool RespawnPlayerEvent::getCount() const
{
	return count;
}

void RespawnPlayerEvent::setPosition(Vector3 pos)
{
	position = pos;
}

void RespawnPlayerEvent::setCount(bool countRespawn)
{
	count = countRespawn;
}

void RespawnPlayerEvent::sendTo(Object* to)
{
	to->receive<RespawnPlayerEvent>(this);
}