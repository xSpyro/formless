
#include "GameHandler.h"

#include "ModuleFactory.h"
#include "LogicHandler.h"
#include "Object.h"
#include "RenderComponent.h"
#include "PhysicsComponent.h"
#include "NetworkComponent.h"
#include "Subsystem.h"
#include "NetworkSubsystem.h"
#include "NetworkSubsystemClient.h"
#include "NetworkSubsystemServer.h"
#include "PhysicsSubsystem.h"
#include "../../formats/src/LabelEnum.h"
#include "Terrain.h"
#include "SharedFormatStore.h"

#include "SoundSubsystem.h"

#include "Events.h"

#include "ObjectFactory.h"

#include "MemLeaks.h"

GameHandler::GameHandler()
	: log("GameHandler"), heightMap(0), blendMap(0)
{
	moduleFactory	= New ModuleFactory();
	logic			= NULL;

	lua				= NULL;
	formatStore = 0;

	currentTransition = NULL;
	frameTransition = 12;
}

GameHandler::GameHandler(LogicHandler* logicHandler)
	: log("GameHandler"), heightMap(0), blendMap(0)
{
	moduleFactory	= New ModuleFactory();
	logic			= logicHandler;

	lua				= logic->getScript();

	//loadLua();
	formatStore = 0;

	currentTransition = NULL;
	frameTransition = 12;
}

GameHandler::~GameHandler()
{
	delete moduleFactory;
	if(logic)
		delete logic;
}

void GameHandler::init()
{
	// load init scripts
	File file;
	if (FileSystem::get("scripts/init.lua", &file))
	{
		lua->execute(file.data, file.size);
	}
}

void GameHandler::update()
{
	if (currentTransition == NULL)
	{
		logic->update();

		if (!nextSceneTransition.empty())
		{
			
			if (!nextTransition)
			{
				/*
				leaveAll();
				exec_stack.push_back(next_scene);
				next_scene->onEnter(exec_stack.size() * 1000);
				*/

				// will always have a transitions if we got here
			}
			else
			{
				currentSceneTransition = nextSceneTransition;
				currentTransition = nextTransition;
				currentTransition->onBegin(frameTransition);
				stepTransition = 0;
			}
	
			nextSceneTransition = "";
			nextTransition = NULL;
		}
	}
	else
	{
		// run transition
		++stepTransition;

        if (stepTransition <= frameTransition)
        {
			//renderer->clear(NULL);
			
			// render from old scene
			currentTransition->onFadeIn();

			// change
			if (stepTransition == frameTransition)
			{
				logic->gotoSceneOnUpdate(currentSceneTransition);
				logic->update();

				currentSceneTransition = "";


				currentTransition->onFadeChange();
			}
        }
		else if (stepTransition < frameTransition * 2)
		{
			// force render scene
			//renderer->clear(NULL);
			// render the frame from the new scene
			//renderFrame();
			currentTransition->onFadeOut();
			//renderer->present();	
		}
		else
		{
			Delete currentTransition;
			currentTransition = NULL;

			stepTransition = 0;
		}
	}
}

void GameHandler::clear()
{
	logic->clear();
}

void GameHandler::render()
{
	logic->render();
}

#pragma region Scenes

void GameHandler::activateScene( const std::string name )
{
	logic->activateScene( name );
}

bool GameHandler::deactivateScene( const std::string name )
{
	return logic->deactivateScene( name );
}

void GameHandler::gotoScene( const std::string name )
{
	setHeightMap(0);
	setBlendMap(0);
	logic->gotoScene( name );
}

void GameHandler::activateSceneOnUpdate(const std::string name)
{
	logic->activateSceneOnUpdate(name);
}

void GameHandler::deactivateSceneOnUpdate(const std::string name)
{
	logic->deactivateSceneOnUpdate(name);
}

void GameHandler::gotoSceneOnUpdate(const std::string name)
{
	logic->gotoSceneOnUpdate(name);
}

void GameHandler::gotoSceneUsingTransition(const std::string & name, SceneTransition * transition, int time)
{
	if (time <= 0)
		time = 20;

	frameTransition = time;
	nextSceneTransition = name;

	// assume scene does exists for some weird reason
	if (transition)
		nextTransition = transition;

	SoundSubsystem* sound = (SoundSubsystem*)getSubsystem("Sound");
	sound->playSound("transition0.mp3", 0.7f, false, true);

}

void GameHandler::gotoSceneUsingLua(const std::string & name)
{
	EventLua::Object obj = getScript()->get("sceneTransitions");

	if (obj)
	{
		obj(name.c_str());
	}
}

bool GameHandler::isInScene(const std::string & name)
{
	Scene * testTo = logic->getScene(name);

	std::vector < Scene* > * scenes = logic->getActiveScenes();
	if (!scenes->empty())
	{
		return scenes->front() == testTo;
	}

	return false;
}

bool GameHandler::isInTransition() const
{
	return currentTransition != NULL;
}

#pragma endregion

Module* GameHandler::getModule(unsigned int id, const std::string name)
{
	return moduleFactory->getModule(id, name);
}

#pragma region Objects

Object* GameHandler::createObject(const std::string name, Object* creator /* = NULL */, Object* parent /* = NULL */)
{
	return logic->createObject(name, creator, parent);
}

Object* GameHandler::createObjectExt(const std::string name, Vector3 position, Vector3 destination, Object* creator /* = NULL */, Object* parent /* = NULL */)
{
	Object* obj = logic->createObject(name, creator, parent);

	if (obj)
	{
		obj->sendInternal(&PositionEvent(position));
		/*
		PhysicsComponent* phys = obj->findComponent<PhysicsComponent>();

		if(phys)
		{
			phys->position = position;
			phys->destination = destination;

			log("Object Created: ", phys->position, " - : ", phys->destination);
		}
		*/
	}

	return obj;
}

void GameHandler::destroyObject(Object* object)
{
	logic->destroyObject(object);
}

Object* GameHandler::getObject(const std::string name)
{
	return logic->getObject(name);
}

void GameHandler::getObjects(const std::string name, std::vector<Object*>& vec)
{
	logic->getObjects(name, vec);
}

Object* GameHandler::getControlledObject()
{
	NetworkSubsystem* net = (NetworkSubsystem*)logic->getSubsystem("Network");

	return net->queryControlledObject();
}

std::vector<Object*> * GameHandler::getCoreObjects()
{
	return logic->getCoreObjects();
}

std::vector<Object*> * GameHandler::getSkillObjects()
{
	return logic->getSkillObjects();
}

bool GameHandler::registerObjectTemplate(const std::string name, LuaType type /* = TYPE_OBJECT */)
{
	File file;
	LuaFileManager * lfm = getLuaFileManager();
	if (!lfm->getFile("objects/" + name + ".lua", file, type))
	{
		FileSystem::get("objects/" + name + ".lua", &file);
		lfm->addFile("objects/" + name + ".lua", file, type);
	}

	return logic->registerObjectTemplate(name, file.data, file.size);
}

bool GameHandler::registerTriggerTemplate(const std::string name, LuaType type /* = TYPE_TRIGGER */)
{
	return registerObjectTemplate(name, type);
}

bool GameHandler::registerSkillTemplate(const std::string name, LuaType type /* = TYPE_SKILL */)
{
	return registerObjectTemplate(name, type);
}

bool GameHandler::registerSceneryTemplate(const std::string name, LuaType type /* = TYPE_OBJECT */)
{
	File file;
	LuaFileManager * lfm = getLuaFileManager();
	if (!lfm->getFile("objects/Scenery/" + name + ".lua", file, type))
	{
		FileSystem::get("objects/" + name + ".lua", &file);
		lfm->addFile("objects/Scenery/" + name + ".lua", file, type);
	}

	return logic->registerObjectTemplate(name, file.data, file.size);
}

bool GameHandler::registerTemplate(const std::string name, EventLua::Object table)
{
	return logic->getObjectFactory()->registerTemplate(name, table);
}

bool GameHandler::loadObjectTemplates(std::istream & stream)
{
	return logic->loadObjectTemplates(stream);
}

bool GameHandler::saveObjectTemplates(std::ostream & stream)
{
	return logic->saveObjectTemplates(stream);
}

bool GameHandler::loadObjects(std::istream & f)
{
	return logic->loadObjects(f);
}

bool GameHandler::saveObjects(std::ostream & f) // THE RENDERERCOMPONENT DOESNT USE ANY OF IT'S STUFF...
{
	return logic->saveObjects(f);
}

bool GameHandler::loadHeightMap(std::istream & f)
{
	if (heightMap)
	{
		return heightMap->load(f);
	}
	return true;
}

bool GameHandler::saveHeightMap(std::ostream & f)
{
	if (heightMap)
	{
		LabelEnum label = LABEL_HEIGHTMAP;
		f.write((char *)&label, sizeof(LABEL_HEIGHTMAP));
		return heightMap->save(f);
	}
	return false;
}

bool GameHandler::loadBlendMap(std::istream & f)
{
	if (blendMap)
	{
		return blendMap->load(f);
	}
	return true;
}

bool GameHandler::saveBlendMap(std::ostream & f)
{
	if (blendMap)
	{
		LabelEnum label = LABEL_BLENDMAP;
		f.write((char *)&label, sizeof(LABEL_BLENDMAP));
		return blendMap->save(f);
	}
	return false;
}

#pragma endregion

void GameHandler::onInput(int sym, int state)
{
	logic->onInput(sym, state);
}

void GameHandler::onMouseMovement(int x, int y)
{
	logic->onMouseMovement(x, y);
}

void GameHandler::onLoad()
{
	logic->onLoad();
}

void GameHandler::onConnected(unsigned int id)
{
	logic->onConnected(id);
}

void GameHandler::setNetworkInterface(ClientInterface * network)
{
	this->network = network;
}

LuaFileManager * GameHandler::getLuaFileManager()
{
	return logic->getLuaFileManager();
}

EventHandler *	GameHandler::getEventHandler()
{
	return logic->getEventHandler();
}

ClientInterface * GameHandler::getNetwork()
{
	return network;
}

void GameHandler::disconnect()
{
	//network->disconnect();

	clear();

	NetworkSubsystem* net = (NetworkSubsystem*)logic->getSubsystem("Network");

	if (net)
	{
		// now remove all connection data thats not
		// related to the internal net coding

		net->clear();
	}
}

void GameHandler::setLogicHandler(LogicHandler* logicHandler)
{
	logic	= logicHandler;
	lua		= logic->getScript();

	loadItems();
}

void GameHandler::setHeightMap(Terrain * terrain)
{
	PhysicsSubsystem * phys = (PhysicsSubsystem *) logic->getSubsystem("Physics");
	if (phys)
		phys->setTerrain(terrain);

	heightMap = terrain;
}

void GameHandler::setBlendMap(Surface * blend)
{
	blendMap = blend;
}

Surface * GameHandler::getBlendMap()
{
	return blendMap;
}

Terrain * GameHandler::getTerrainMap()
{
	return heightMap;
}

void GameHandler::setFormatStore(SharedFormatStore * store)
{
	formatStore = store;
}

SharedFormatStore * GameHandler::getFormatStore()
{
	return formatStore;
}

EventLua::State* GameHandler::getScript()
{
	return logic->getScript();
}

void GameHandler::loadItems()
{
	File file;
	FileSystem::get("items/items.lua", &file);
	if( !lua->execute(file.data, file.size) )
	{
		std::cout << lua->getLastError() << std::endl;
	}
	else
	{
		moduleFactory->loadModules(lua->get("modules"));
	}

}

Subsystem * GameHandler::getSubsystem(std::string name)
{
	return logic->getSubsystem(name);
}

LogicHandler * GameHandler::getLogic() const
{
	return logic;
}

int GameHandler::getKey(int sym)
{
	return logic->getKey(sym);
}

bool GameHandler::loadString(std::istream & f, std::string & string)
{
	int size = 0;
	f.read((char *)&size, sizeof(int));
	if (size >= 0)
	{
		char * c = new char[size];
		f.read(c, sizeof(char) * size);
		string = c;
		delete c;

		return true;
	}
	return false;
}

void GameHandler::onSkillEffect(float damage, bool stun, bool link, bool outOfMana, int killStreak, float x, float y, float z, bool fromServer)
{
	if(fromServer)
		logic->onSkillEffect(damage, stun, link, outOfMana, killStreak, x, y, z);
	else
	{
		NetworkSubsystem* net = (NetworkSubsystem*)logic->getSubsystem("Network");
		if(net->isServer())
		{
			logic->onSkillEffect(damage, stun, link, outOfMana, killStreak, x, y, z);

			NetworkSubsystemServer* netServer = (NetworkSubsystemServer*)net;

			SkillEffectPacket packet;

			packet.damage		= damage;
			packet.stun			= stun;
			packet.link			= link;
			packet.killStreak	= killStreak;
			packet.outOfMana	= outOfMana;
			packet.x			= x;
			packet.y			= y;
			packet.z			= z;

			netServer->broadcastSkillEffect(packet);
		}
	}
}

void GameHandler::onAccountUpdate(AccountPacket* packet)
{
	logic->onAccountUpdate(packet);
}

void GameHandler::onKillObject(Object* killed, Object* killer)
{
	logic->onKillObject(killed, killer);
}

std::string	GameHandler::getDebugString() const
{
	return logic->getObjectFactory()->getDebugString();
}

void GameHandler::reloadAllLuaFiles(LuaType type)
{
	LuaFileManager::FileIds files;
	logic->getLuaFileManager()->getFiles(files, type);

	for (LuaFileManager::FileIds::iterator it = files.begin(); it != files.end(); it++)
	{
		bool useBase = true;
		if (std::string::npos != it->name.find(":\\"))
			useBase = false;

		File f;
		FileSystem::get(it->name, &f, useBase);
		lua->execute(f.data, f.size);
		logic->getLuaFileManager()->addFile(it->name, f, it->type, true);
	}
	logic->getObjectFactory()->reloadTemplates();
}