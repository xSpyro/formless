#ifndef SKILLSUBSYSTEM_H
#define SKILLSUBSYSTEM_H

#include "Subsystem.h"
class SkillComponent;

/*
 * The specific tasks are described in the corresponding component header file.
 */

class SkillSubsystem : public Subsystem
{
	typedef std::vector< SkillComponent > Components;

public:
	SkillSubsystem( GameHandler* gameHandler = NULL, int size = 1000 );
	virtual ~SkillSubsystem();

	Component*	createComponent( EventLua::Object script );
	Component*	getComponent();
	void		update();
	void		clear();

	std::string getDebugString() const;

private:
	Components components;
};

#endif
