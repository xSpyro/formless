#ifndef TRIGGER_EVENT_H
#define TRIGGER_EVENT_H
#include "Event.h"
#include "../framework/src/IntersectResult.h"

class Object;

enum TriggerException
{
	TRIGGER_NONE, // All non exceptions have this
	TRIGGER_TERRAIN
};

class TriggerEvent : public Event
{
public:
	TriggerEvent(TriggerException ex, bool enter = false);
	TriggerEvent(Object * other = 0, bool enter = false);
	TriggerEvent(Object * other, bool enter, IntersectResult res);
	virtual ~TriggerEvent();

	void setOther(Object * other);
	void setEntering(bool enter);
	void setResult(IntersectResult res);

	bool entering();
	bool leaving();
	Object * getOther();
	IntersectResult getResult();

	TriggerException getException();

	virtual void sendTo(Object * to);
protected:
	Object * other;
	bool enter;
	IntersectResult res;
	TriggerException exceptionType;
};

#endif