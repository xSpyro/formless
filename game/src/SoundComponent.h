
#ifndef SOUND_COMPONENT_H

#define SOUND_COMPONENT_H

#include "Component.h"

#include "SoundSubComponent.h"

class SoundSubComponent;
#include "../framework/src/AudioSystem.h"

#include "PhysicsComponent.h"
#include "Object.h"



enum Properties
{
	PROPERTY_NOTHING,
	PROPERTY_ON_MOVEMENT,
	PROPERTY_END
};



class SoundComponent : public Component
{
public:
	SoundComponent();
	SoundComponent(Subsystem* subSystem, int id, std::string file, FMOD::Channel* const chan, int prop, const float vol, AudioSystem* const audio);
	SoundComponent(Component& rhs, int id, std::string file, FMOD::Channel* const chan, int prop, const float vol, AudioSystem* const audio);


	~SoundComponent();

	void				update();

	void				addSoundSubComponent(SoundSubComponent* const subComp);

	void				listenToMovement();

	void				receive(KeyEvent * key);

	void				receive(MoveEvent * move);

	void				receive(RespawnPlayerEvent * res);

	void				receive(EffectEvent * effect);

	void				receive(KillPlayerEvent * kill);

	const std::string	getSoundName() const;

	const int			getChannelID() const;

	FMOD::Channel*		getChannel() const;

	const bool			getLooping() const;

	const bool			getIs3D() const;

	const bool			isPlaying() const;

	const bool			isStartPlaying() const;

	const bool			isStopPlaying() const;

	void				setIsplaying(const bool p);

	void				setStartPlaying(const bool p);

	void				setStopPlaying(const bool p);

	void				setChannel(FMOD::Channel* const chan);

	void				setID(const int id);

	void				setLooping(const bool l);

	void				set3D(const bool d);

	const float			getVolume() const;

	const int			getProperty() const;

	void				makeChildsDirty();

	AudioSystem* const getAudio() const;

	const bool			getHasSound() const;

	void bindToComponents();
private:

	int channelID;
	std::string sound;
	bool loop;
	bool playing;
	bool startPlaying;
	bool stopPlaying;
	bool is3D;
	float volume;

	FMOD::Channel* channel;

	std::vector<SoundSubComponent*> subComponents;

	AudioSystem* audio;

	bool hasSound;

public:
	bool isPlayer;

};


#endif