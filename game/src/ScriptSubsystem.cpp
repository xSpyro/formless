
#include "ScriptSubsystem.h"
#include "ScriptComponent.h"

ScriptSubsystem::ScriptSubsystem( GameHandler* gameHandler, int size )
	: Subsystem( gameHandler, "ScriptSubsystem" )
{
	components.reserve(size);
}

ScriptSubsystem::~ScriptSubsystem()
{
}

Component* ScriptSubsystem::createComponent(EventLua::Object script)
{
	ScriptComponent* comp = (ScriptComponent*)this->getComponent();

	if (comp == 0)
		return 0;

	EventLua::Object callbacks = script.get("callbacks");

	comp->callbacks.load(callbacks);

	return comp;
}

Component* ScriptSubsystem::getComponent()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if( !it->isBound() )
		{
			(*it) = ScriptComponent( this );
			return &(*it);
		}
	}
	if (components.size() == components.capacity())
		return 0;


	components.push_back( ScriptComponent( this ) );
	return &components.back();
}

void ScriptSubsystem::update()
{
	/*
	for( Components::iterator it = components.begin(); it < components.end(); it++ )
	{
		if(it->isBound() && it->activated)
			it->update();
	}
	*/
}

void ScriptSubsystem::clear()
{
	components.clear();
}

std::string ScriptSubsystem::getDebugString() const
{
	unsigned int out = 0;
	for( Components::const_iterator it = components.begin(); it != components.end(); ++it )
	{
		if(it->isBound() && it->activated)
			++out;
	}

	std::stringstream format;
	format << "instances: " << out;
	return format.str();
}