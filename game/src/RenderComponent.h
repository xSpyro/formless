#ifndef RENDERCOMPONENT_H
#define RENDERCOMPONENT_H

#include "Component.h"
#include "Framework.h"

class Renderer;

/*	
 *	The render component handles render information for an object.
 *	If an object has no render component, it cannot be rendered and is strictly logical.
 */

class RenderComponent : public Component
{
public:
	RenderComponent( Subsystem* subsystem = NULL );
	virtual ~RenderComponent();

	void load(Renderer* renderer);

	void receive(PositionEvent * pos);
	void receive(RotationEvent * rot);
	void receive(ScaleEvent * scale);
	void receive(KillPlayerEvent * kill);
	void receive(RespawnPlayerEvent * res);
	void receive(QueryEvent<Vector3> * qv);
	void receive(QueryEvent<Quaternion> * qq);

	virtual void unbindObject();

	void update();

	bool load(std::istream &stream, int label);
	bool save(std::ostream &stream);

	void updateMatrix();

	void setDebugMatrix(const Matrix & matrix);

public:
	Model		model;

	std::string	modelName;
	std::string	shader;
	std::string texture;

	Vector3 position; // TODO: USE THESE!!!
	Vector3 scale;
	Quaternion rotation;

	Model		modelDebug;
};

#endif