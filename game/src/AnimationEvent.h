#ifndef ANIMATION_EVENT_H
#define ANIMATION_EVENT_H
#include "Event.h"

class AnimationEvent : public Event
{
public:
	AnimationEvent(std::string animation = "", Object * sender = 0);
	virtual ~AnimationEvent();

	void sendTo(Object * to);

	std::string getAnimation();
	void setAnimation(std::string animation);
private:
	std::string animation;
};

#endif