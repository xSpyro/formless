
#ifndef EFFECTSUBSYSTEM_H
#define EFFECTSUBSYSTEM_H

#include "Subsystem.h"

class EffectComponent;

class EffectSubsystem : public Subsystem
{
	typedef std::vector<EffectComponent> Components;

public:
	EffectSubsystem(GameHandler* gameHandler = NULL);
	virtual ~EffectSubsystem();

	Component*	createComponent(EventLua::Object script);
	Component*	getComponent();

	void		update();
	void		clear();

	std::string getDebugString() const;

private:
	Components components;

};

#endif