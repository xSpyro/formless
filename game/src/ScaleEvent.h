#ifndef SCALE_EVENT_H
#define SCALE_EVENT_H

#include "Event.h"
#include "Framework.h"

/* Scale Event applies the given scaleRate to the already existing scale in PhysicsComponent */

class ScaleEvent : public Event
{
public:
	ScaleEvent(float x = 0, float y = 0, float z = 0, bool apply = true);
	ScaleEvent(Vector3 scaleRate);
	virtual ~ScaleEvent();

	virtual void sendTo(Object * to);

	void setScale(Vector3 scaleRate);
	void setApply(bool scaleApply);
	Vector3 getScale();
	bool	getApply();
private:
	Vector3 scale;
	bool	apply;
};

#endif