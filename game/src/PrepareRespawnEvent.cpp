#include "PrepareRespawnEvent.h"
#include "EventEnum.h"
#include "Object.h"

PrepareRespawnEvent::PrepareRespawnEvent(Object * sender /* = 0 */) : Event(EVENT_PREPARE_RESPAWN, sender)
{
}

PrepareRespawnEvent::~PrepareRespawnEvent()
{
}

void PrepareRespawnEvent::sendTo(Object* to)
{
	to->receive<PrepareRespawnEvent>(this);
}