#include "ScaleEvent.h"
#include "EventEnum.h"
#include "Object.h"


ScaleEvent::ScaleEvent(float x, float y, float z, bool apply ) : Event(EVENT_SCALE, 0,  true), scale(x, y, z), apply(apply)
{}
ScaleEvent::ScaleEvent(Vector3 scaleRate) : Event(EVENT_SCALE, 0,  true), scale(scaleRate)
{}

ScaleEvent::~ScaleEvent()
{}

Vector3 ScaleEvent::getScale()
{
	return scale;
}

bool ScaleEvent::getApply()
{
	return apply;
}

void ScaleEvent::setScale(Vector3 scaleRate)
{
	scale = scaleRate;
}

void ScaleEvent::setApply(bool scaleApply)
{
	apply = scaleApply;
}

void ScaleEvent::sendTo(Object * to)
{
	to->receive<ScaleEvent>(this);
}

