
#ifndef EFFECTCOMPONENT_H
#define EFFECTCOMPONENT_H

#include "Component.h"

class EffectComponent : public Component
{
public:
	EffectComponent( Subsystem* subsystem = NULL, std::string name = "" );
	virtual ~EffectComponent();

	void update();

	void bindToComponents();

	void		setName(const std::string name);
	std::string getName() const;

private:
	std::string name;
};

#endif