#include "EffectEvent.h"
#include "EventEnum.h"
#include "Object.h"



EffectEvent::EffectEvent(SkillEffect * effect, bool first, Object * from, Object * to, bool remarkable, bool end) : Event(EVENT_EFFECT, to), effect(effect), first(first), end(end), remarkable(remarkable), from(from)
{
}

EffectEvent::~EffectEvent()
{}

SkillEffect * EffectEvent::getEffect()
{
	return effect;
}

bool EffectEvent::isFirst()
{
	return first;
}

bool EffectEvent::isEnd()
{
	return end;
}

bool EffectEvent::isRemarkable()
{
	return remarkable;
}

bool EffectEvent::isUnrearkable()
{
	return !remarkable;
}

IntersectResult & EffectEvent::getResult()
{
	return result;
}

Object * EffectEvent::getFrom()
{
	return from;
}

void EffectEvent::setEffect(SkillEffect * e)
{
	effect = e;
}

void EffectEvent::setFirst(bool f)
{
	first = f;
}

void EffectEvent::setEnd(bool e)
{
	end = e;
}

void EffectEvent::setRemarkable(bool remark)
{
	remarkable = remark;
}

void EffectEvent::setFrom(Object * o)
{
	from = o;
}

void EffectEvent::sendTo(Object * to)
{
	to->receive<EffectEvent>(this);
}