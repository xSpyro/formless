#include "SoundSubComponent.h"


SoundSubComponent::SoundSubComponent(SoundComponent* p)
	: dirty(false)
{
	parent = p;
}
SoundSubComponent::~SoundSubComponent()
{

}

void SoundSubComponent::update()
{
	if (!dirty)
		this->updateSubComponent();
}

void SoundSubComponent::setDirty(const bool d)
{
	dirty = d;
}

const bool SoundSubComponent::getDirty() const
{
	return dirty;
}

SoundComponent* const SoundSubComponent::getParent()
{
	return parent;
}

