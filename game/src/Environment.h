#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H
#include <vector>

class Object;

class Environment
{
public:
	Environment(Object * caller = 0, unsigned int maxSize = 50);
	~Environment();

	Object * getCaller();
	Object * getData(int pos);

	void setCaller(Object * caller);
	int addData(Object * o);

	void clearData();
	bool isFull();
private:
	typedef std::vector<Object *> Objects;
	
	unsigned int maxSize;

	Object * caller;
	Objects data;
};

#endif