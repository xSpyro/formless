
#ifndef EXAMPLESCENE_H
#define EXAMPLESCENE_H

#include "Scene.h"
#include "Framework.h"

class Object;

class ExampleScene : public Scene
{
public:

	ExampleScene();
	ExampleScene(LogicHandler* handler, Renderer* rend, Window* wndw);
	virtual ~ExampleScene();

	virtual void update();
	virtual void render();

	virtual void onEnter();
	virtual void onLeave();

	virtual bool onInput(int sym, int state);		// If return false give input to eventHandler
	virtual bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	virtual void onCreateObject(Object* object);	// Do not call this manually!

private:
	Layer * layer;
	Camera camera;

	Object* player;
};

#endif