
#include "AnimationSubsystem.h"

#include "MemLeaks.h"

#include "Importer.h"

#include "AnimationEvent.h"

#include "SharedFormatStore.h"

// uses the formats-lib here, instead of using it in the whole project
#pragma comment( lib, "formats.lib" )

AnimationSubsystem::AnimationSubsystem( GameHandler* gameHandler, int size, SharedFormatStore* s)
	: Subsystem(gameHandler, "AnimationSubsystem"), store(s)
{
	components.reserve(1000);

	/*
	{
		File file;
		if (FileSystem::get("animations/test.pas", &file))
		{
			ShapeAnimation * animation = New ShapeAnimation;
			if (Importer::readShapeFromFile(file, animation))
			{
				animations["test.pas"] = animation;
			}
		}
	}


	{
		File file;
		if (FileSystem::get("animations/fireball0.pas", &file))
		{
			ShapeAnimation * animation = New ShapeAnimation;
			if (Importer::readShapeFromFile(file, animation))
			{
				animations["test2"] = animation;
			}
		}
	}
	*/
	
}

AnimationSubsystem::~AnimationSubsystem()
{
	for(Animations::iterator i = animations.begin(); i != animations.end(); ++i)
	{
		delete i->second;
	}
}

Component* AnimationSubsystem::createComponent(EventLua::Object script)
{
	AnimationComponent* comp = (AnimationComponent*)getComponent();
	
	if (store)
	{
		AnimationEvent ev;
		std::string fileName = script.get("name").queryString("error");

		Animations::iterator it = animations.find(fileName);
		if(it == animations.end())
		{
			store->importFromFile(fileName);
		}

		comp->resetShape();
		comp->startShape(fileName);
	}

	{
		EventLua::Object scriptColor = script.get("baseColor");
		if( scriptColor.isTable() )
		{
			Color color(scriptColor.get("r").queryFloat(), scriptColor.get("g").queryFloat(), scriptColor.get("b").queryFloat(), scriptColor.get("a").queryFloat());
			comp->setBaseColor(color);
		}
		else
		{
			Color color(0.4f, 0.2f, 0.15f, 1.0f);
			comp->setBaseColor(color);
		}
	}
	
	{
		EventLua::Object scriptColor = script.get("lightColor");
		if( scriptColor.isTable() )
		{
			Vector3 color(scriptColor.get("r").queryFloat(), scriptColor.get("g").queryFloat(), scriptColor.get("b").queryFloat());
			comp->setLightColor(color);
		}
		else
		{
			Vector3 color(1.0f, 1.0f, 0.0f);
			comp->setLightColor(color);
		}
		}

	return comp;
}

Component* AnimationSubsystem::getComponent()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if( !it->isAlive() && it->dead())
		{
			it->reset();
			return &(*it);
		}
	}
	
	if (components.size() == components.capacity())
		return 0;

	components.push_back( AnimationComponent( this ) );

	components.back().reset();

	return &components.back();
}

void AnimationSubsystem::update()
{
	for (Components::iterator i = components.begin(); i != components.end(); ++i)
	{
		if( i->activated && (i->isAlive() || !i->dead()))
		{
			i->update();
		}
	}
}

void AnimationSubsystem::clear()
{
	components.clear();
}

void AnimationSubsystem::render()
{
	// we render trough the render framework pipeline
}

ShapeAnimation * AnimationSubsystem::getAnimation(const std::string & name)
{
	Animations::iterator i = animations.find(name);
	if (i != animations.end())
	{
		return i->second;
	}
	else
	{
		if (store->importFromFile(name))
		{
			return getAnimation(name);
		}
	}

	return NULL;
}

bool AnimationSubsystem::insertAnimation(const std::string & name, ShapeAnimation * animation)
{
	Animations::iterator i = animations.find(name);
	if (i == animations.end())
	{
		animations[name] = animation;
		return true;
	}

	return false;
}

std::string	AnimationSubsystem::getDebugString() const
{
	unsigned int count = 0;

	for (Components::const_iterator i = components.begin(); i != components.end(); ++i)
	{
		if( i->activated && i->isAlive())
		{
			++count;
		}
	}

	std::stringstream format;
	format << "instances: " << count;

	return format.str();
}

void AnimationSubsystem::generateRandomFrame(ShapeAnimation * animation, int frame)
{
	

	int n = (int)random(-3, 16);

	if (n < 1) n = 1;

	int prev = -1;
	for (int i = 0; i < n; ++i)
	{
		int bind = prev;
		prev = animation->addNode(frame, bind, Vector3::random(6, 6, 6) * 1.5);
	}

	animation->linkNode(frame, prev, 0);
}

void AnimationSubsystem::generateCircleFrame(ShapeAnimation * animation, int frame)
{
	int n = (int)random(5, 48);

	float add = 6.28f / n;

	float offset = random(0, 10);

	int prev = -1;
	for (float i = offset - 3.14f; i <= offset + 3.14f; i += add)
	{
		float x = cos(i) * (n / 5.0f) * 1.5f;
		float y = sin(i) * (n / 5.0f) * 1.5f;

		int bind = prev;
		prev = animation->addNode(frame, bind, Vector3(x, y, 0));
	}

	animation->linkNode(frame, prev, 0);
}