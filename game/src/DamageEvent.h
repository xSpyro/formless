#ifndef DAMAGE_EVENT_H
#define DAMAGE_EVENT_H
#include "Event.h"
#include "Attribute.h"

class DamageEvent : public Event
{
public:
	DamageEvent(Attribute damage, Object * senderAndDefender = 0, Object * attacker = 0); // To give components a chance to change/do something to the damage
	~DamageEvent();

	Object * getDefender();
	Object * getAttacker();

	void sendTo(Object * to);
	
public:
	Attribute damage;

private:
	Object * attacker;
};

#endif