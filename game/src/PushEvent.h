#ifndef PUSH_EVENT_H
#define PUSH_EVENT_H
#include "Events.h"
#include "Framework.h"

class PushEvent : public Event
{
public:
	PushEvent();
	PushEvent(Vector3 velocity);
	virtual ~PushEvent();

	virtual void sendTo(Object * to);

	Vector3 getMovement();

	void	setMovement(Vector3 push);

protected:
	Vector3	push;
};

#endif