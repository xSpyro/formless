#ifndef LINK_EVENT_H
#define LINK_EVENT_H

#include "Event.h"
#include "Framework.h"

class LinkEvent : public Event
{
public:
	LinkEvent(Object * linkObject, Object * owner, Object * target, bool state = false);
	virtual ~LinkEvent();

	virtual void sendTo(Object * to);

	void setLink(Object* obj);
	void setOwner(Object* obj);
	void setTarget(Object* obj);
	void setState(bool linkState);
	Object* getLink();
	Object* getOwner();
	Object* getTarget();
	bool	getState();
private:
	Object * link;
	Object * owner;
	Object * target;
	bool	 state;
};

#endif