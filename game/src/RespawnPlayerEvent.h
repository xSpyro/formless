#ifndef RESPAWN_PLAYER_EVENT_H
#define RESPAWN_PLAYER_EVENT_H
#include "Event.h"
#include "../framework/src/Vector3.h"

class RespawnPlayerEvent : public Event
{
public:
	RespawnPlayerEvent( float x = 0, float y = 0, float z = 0, bool countRespawn = true );
	RespawnPlayerEvent( Vector3 position, bool countRespawn = true );
	virtual ~RespawnPlayerEvent();

	Vector3 getPosition() const;
	bool	getCount() const;
	void	setPosition(Vector3 pos);
	void	setCount(bool countRespawn);

	virtual void sendTo(Object* to);
private:
	Vector3 position;
	bool	count;
};

#endif