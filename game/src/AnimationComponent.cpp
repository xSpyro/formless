
#include "AnimationComponent.h"

#include "AnimationSubsystem.h"

#include "Object.h"
#include "PhysicsComponent.h"

#include "KeyEvent.h"
#include "AnimationEvent.h"

#include "EffectEvent.h"
#include "LinkEvent.h"
#include "KillPlayerEvent.h"
#include "DestroyEvent.h"

#include "ControllerComponent.h"

#include "GameHandler.h"
#include "Terrain.h"

AnimationComponent::AnimationComponent( Subsystem* subsystem )
{
	as = reinterpret_cast < AnimationSubsystem* > (subsystem);

	spawn = true;

	delta = 0;
	mt = NULL;

	lossFactor = 0;
	gravityFactor = 0;

	linked = false;
	forceLinked = false;
	link = &animation;

	physics = NULL;

	has_animation = false;

	baseColor = Color(1.0f, 1.0f, 1.0f, 1.0f);
}

AnimationComponent::~AnimationComponent()
{
	if (mt)
	{
		mt->free();
	}
}

void AnimationComponent::reset()
{
	stream.reset();
	spawn = true;
	model.setVisible(true);

	lossFactor = 0;
	gravityFactor = 0;
	link = &animation;

	linked = false;
	forceLinked = false;

	physics = NULL;

	has_animation = false;

	baseColor = Color(1.0f, 1.0f, 1.0f, 1.0f);
}

void AnimationComponent::load(Renderer * renderer, Camera * camera)
{
	stream.init(renderer, camera);

	if (mt == NULL)
	{
		mt = renderer->texture()->create(0, 0, TEXTURE_MULTI);
		mt->addMultiTexture(renderer->texture()->get("textures/ps_parts.png"));
		mt->addMultiTexture(renderer->texture()->get("textures/ps_hori_stream.png"));
	}

	model.detach();
	model.add(Renderable(stream.getRenderMesh(), mt, NULL));
	model.setVisible(true);

	renderable = const_cast < Renderable* > (&model.getRenderables().front());

	delta = 0;

	linked = false;
}

void AnimationComponent::update()
{
	if (mt == NULL)
		return;

	animation.update();

	Vector3 offset = 0;
	float angle = 0;

	if (physics)
	{
		offset = physics->getPosition();
		physics->rotation.getAxisAngle(Vector3(0, 0, 0), angle);
		angle += (3.1415f / 2);

		model.getNativeMatrix().identity();
		model.getNativeMatrix().translate(offset);

		model.setSharedMatrix(NULL);
	}
	else if (forceLinked)
	{
		model.setSharedMatrix(NULL);
		model.getTransform().identity();
		model.getTransform().translate((forceLinks[0] + forceLinks[1]) / 2);

		model.setSharedMatrix(NULL);
	}

	Material mat;
	mat.blend = baseColor;

	if (has_animation)
	{

		ShapeFrame shape = link->getFrame();

		//link->debug();


		ShapeFrame::Desc desc;
		desc.offset = offset;
		desc.angle = angle;
		desc.scale = 1.0f;
		desc.time = timer.now();

		int linkOffset = 128;

		desc.linkOffset = &linkOffset;

		shape.createLerpedResult(&result, &desc);

		if (!spawn)
		{
			result.effect.die = 1;
		}

		result.effect.gravity = gravityFactor;

		stream.setShapeData(&result);


		
		Vector3 center;
	
		if (spawn)
		{
			for (int i = 0; i < result.count; i += 1)
			{
				for (int j = 0; j < result.nodes[i].count; ++j)
				{
					//stream.insertConnection((float)i, result.nodes[i].cache[j]);

					Vector3 src = result.nodes[i].position;
					Vector3 dst2 = result.nodes[(int)std::max(std::min(result.nodes[i].cache[j], 127.0f), 0.0f)].position;
					center = center + result.nodes[i].position;
			
					stream.insert(src + (dst2 - src) * random(0, 1), result.nodes[i].cache[j]);
				}

				/*
				for (int a = 0; a < 4; ++a)
				{

					Vector3 src = result.nodes[i].position;
					Vector3 dst2 = result.nodes[(int)std::max(std::min(result.nodes[i].cache[0], 127.0f), 0.0f)].position;
					center = center + result.nodes[i].position;
			
					stream.insert(src + (dst2 - src) * random(0, 1), result.nodes[i].cache[0]);
				}
				*/
			}

			Vector3 src = center / (float)result.count;

			for (int i = 0; i < result.count; ++i)
			{
				Vector3 dst1 = result.nodes[(int)std::max(std::min(result.nodes[i].cache[0], 127.0f), 0.0f)].position;
				//stream.insert(src + (dst1 - src) * random(0, 1), result.nodes[i].cache[0]);
			}

			lossFactor -= 0.05f;

			if (lossFactor < 0)
				lossFactor = 0;

			gravityFactor = 0;


			mat.blend.r += lossFactor;
			mat.blend.g += lossFactor;
			mat.blend.b += lossFactor;
			
		}
	}

	renderable->setMaterial(&mat);


	if (linked && linkPlayer)
	{
		if (!linkPlayer->isAlive())
		{
			linked = false;
			linkPlayer = NULL;
			linkOther = NULL;
		}
		else
		{
			if(linkOther)	//Then we are the link object itself
			{
				//drawLink(physics->position, linkPlayer->position);
				drawLink(physics->getPosition(), linkOther->position);
			}
			//else
				//drawLink(offset, linkPlayer->position);
		}
	}
	else if (forceLinked)
	{
		drawLink(forceLinks[0], forceLinks[1]);
	}

	stream.update();

	if (stream.isEmpty() && !forceLinked)
	{
		spawn = true;

		// removed temporarily since animations in editor was removed completely
		//renderable.setVisible(false);
		/*renderable.setVisible(0);*/
		
		if (physics)
		{
			physics->unlock();
		}
	}
}

void AnimationComponent::render()
{
	stream.render();
}

void AnimationComponent::receive(DestroyEvent * ev)
{
	if (ev->getSender() != object)
		return;
	stream.reset();
	spawn = false;
	
	linkOther = NULL;
	linkPlayer = NULL;
}

void AnimationComponent::receive(KillPlayerEvent * ev)
{
	Component::receive(ev);
	if (ev->getKilled() == object)
	{
		spawn = false;
		model.setVisible(0);
	}
}

void AnimationComponent::receive(RespawnPlayerEvent * ev)
{
	Component::receive(ev);
	spawn = true;
	model.setVisible(true);
}

void AnimationComponent::receive(AnimationEvent * ev)
{
	model.setVisible(true);
	animation.executeShape(as->getAnimation(ev->getAnimation()));
}

void AnimationComponent::receive(EffectEvent * ev)
{
	if (ev->isRemarkable() && ev->getSender() == object)
	{
		result.effect.die = 0.5f;
		lossFactor += 0.5;

		lossFactor = std::min(lossFactor, 0.9f);
	}
}

void AnimationComponent::receive(SkillEvent * ev)
{
	gravityFactor = 1;
}

void AnimationComponent::receive(LinkEvent * ev)
{
	if (physics && ev->getState() == true)
	{
		if (ev->getOwner() == object)
		{
			linked = true;
			linkPlayer = ev->getTarget()->findComponent<PhysicsComponent>();
			linkOther = NULL;
		}
		else if (ev->getTarget() == object)
		{
			linked = true;
			linkPlayer = ev->getOwner()->findComponent<PhysicsComponent>();
			linkOther = NULL;
		}
		else if (ev->getLink() == object)
		{
			linked = true;
			linkPlayer = ev->getOwner()->findComponent<PhysicsComponent>();
			linkOther = ev->getTarget()->findComponent<PhysicsComponent>();
			result.count = 0;
		}
		else
		{
			linked = false;
			linkPlayer = NULL;
			linkOther = NULL;
		}
	}
	else
	{
		linked = false;
		linkPlayer = NULL;
		linkOther = NULL;
	}
}

/*
void AnimationComponent::unbindObject()
{
}
*/

void AnimationComponent::bindToComponents()
{
	physics = object->findComponent<PhysicsComponent>();

	if (physics)
	{
		physics->lock();
	}

	model.setVisible(true);
}

void AnimationComponent::overrideAnimation(const ShapeAnimationInst * link)
{
	this->link = link;
}

void AnimationComponent::setBaseColor(const Color & color)
{
	baseColor = color;
}

void AnimationComponent::setLightColor(const Vector3 & color)
{
	lightColor = color;
}

Color AnimationComponent::getBaseColor() const
{
	return baseColor;
}

Vector3 AnimationComponent::getLightColor() const
{
	return lightColor;
}

bool AnimationComponent::dying()
{
	return spawn == 0;
}

bool AnimationComponent::dead()
{
	if (stream.isEmpty())
	{
		model.setVisible(false);
	}

	return stream.isEmpty();
}

const Renderable * AnimationComponent::getRenderable() const
{
	return renderable;
}

Model * AnimationComponent::getModel()
{
	return &model;
}

void AnimationComponent::pause()
{
	animation.setSpeed(0.0f);
}

void AnimationComponent::resume()
{
	animation.setSpeed(1.0f);
}

void AnimationComponent::startShape(const std::string & shape)
{
	animation.changeShape(as->getAnimation(shape));

	has_animation = true;
}

void AnimationComponent::startShape(const ShapeAnimation * shape)
{
	// should be const
	animation.changeShape(const_cast < ShapeAnimation* > (shape));

	has_animation = true;
}

void AnimationComponent::resetShape()
{
	animation.reset();
}

void AnimationComponent::forceLink(const Vector3 & from, const Vector3 & to)
{
	forceLinked = true;
	forceLinks[0] = from;
	forceLinks[1] = to;
}

const Vector3 & AnimationComponent::getForceLinkPosition() const
{
	return forceLinks[0];
}

void AnimationComponent::kill()
{
	spawn = false;
	model.setVisible(false);
}

void AnimationComponent::drawLink(const Vector3 & from, const Vector3 & to)
{
	Terrain * terrain = NULL;
	if (as)
	{
		terrain = as->getGameHandler()->getTerrainMap();
	}

	float len = (from - to).length();
	if(len <= 0 || (to.x == 0 && to.y == 0 && to.z == 0) || (from.x == 0 && from.y == 0 && from.z == 0))
		return;

	Vector3 d = (to - from);
	Vector3 dn = d / len;
	Vector3 c = dn.cross(Vector3(-dn.y, dn.x, dn.z));

	Vector3 previous;
	int count = 1;

	previous = from;

	float div = 1 / (len * 0.01f);
	for (float f = random(0.1f, 0.3f) * div; f < 1.0f; f += random(0.1f, 0.2f) * div)
	{
		Vector3 r = dn * random(-3.0f, 3.0f) * 1;
		Vector3 p = from + d * f + dn * r + c * r;

		if (terrain)
		{
			float miny = terrain->sample(p.x, p.z);
			if (p.y < miny + 20)
				p.y = miny + 20;
		}


		if (count < 2 && result.count > 16)
		{
			for (int i = 0; i < result.count; i += 16)
			{
				stream.insertFreeConnection(result.nodes[i].position, p);
			}
			++count;
		}
		else
		{
			stream.insertFreeConnection(previous, p);
		}

		previous = p;
	}

	stream.insertFreeConnection(previous, to);
}