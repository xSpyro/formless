#include "AnimationEvent.h"
#include "EventEnum.h"
#include "Object.h"

//EVENT_ANIMATION

AnimationEvent::AnimationEvent(std::string animation, Object * sender) : Event(EVENT_ANIMATION, sender), animation(animation)
{}

AnimationEvent::~AnimationEvent()
{}

void AnimationEvent::sendTo(Object * to)
{
	to->receive<AnimationEvent>(this);
}

std::string AnimationEvent::getAnimation()
{
	return animation;
}

void AnimationEvent::setAnimation(std::string ani)
{
	animation = ani;
}

