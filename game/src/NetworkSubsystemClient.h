
#ifndef NETWORKSUBSYSTEM_CLIENT_H
#define NETWORKSUBSYSTEM_CLIENT_H

#include "NetworkSubsystem.h"

#include "CommonPackets.h"

#include "../framework/src/Timer.h"
#include "PlayerInfo.h"

class GameHandler;
class ClientInterface;

struct Statistics
{
	std::string gameTimeStr;
	int gameMins;
	int gameSecs;
	int kills[NR_TEAMS];
	bool gameStarted;
	bool allReady;
};

struct RespawnTimer
{
	bool respawning;
	int time;
};

class NetworkSubsystemClient : public NetworkSubsystem
{

	typedef std::vector < Object* > Connections;
	typedef std::vector < std::string > TextBuffer;
	typedef std::map< unsigned int, PlayerInfo > PlayerList;

public:

	NetworkSubsystemClient(GameHandler * game, ClientInterface * network);

	void update();

	void clear();

	ClientInterface * getNetwork();

	void onPacket(RawPacket & packet);

	void onDisconnected();

	std::string getName( unsigned int netId );

	//void processCreatePacket(CreatePacket & packet);

	void queryClientList(std::vector < Object* > & list);
	void queryClientList(std::vector< Object* > & list, const int team);
	void queryTextBuffer(std::vector < std::string > & list);
	void queryPlayers( std::vector< PlayerInfo > & list );

	Object * queryControlledObject() const;

	bool isServer();

	//float		gameTimerNow();
	//std::string gameTimerNowText();
	void		timeInMinsSecs(float time, int& mins, int& secs);
	std::string	timeToText(float time);

	Statistics*		getStats();
	RespawnTimer*	getRespawnTimer();

	void		sendUserInfoRequest(unsigned int userID);

private:

	void				processDisconnectPacket(DisconnectPacket & packet);
	void				processConnectPacket(ConnectPacket & packet);
	//void				processLevelDataResponsePacket(LevelDataResponsePacket & packet);
	void				processReadyPacket(ReadyPacket & packet);
	void				processGameStartPacket(GameStartPacket & packet);
	void				processGameEndPacket(GameEndPacket & packet);
	void				processTimerPacket(TimerPacket & packet);
	void				processRespawnTimerPacket(RespawnTimerPacket & packet);
	void				processServerStatePacket(ServerStatePacket & packet);
	NetworkComponent*	processCreatePacket(CreatePacket & packet);
	void				processDestroyPacket(DestroyPacket & packet);
	void				processKillPlayerPacket(KillPlayerPacket & packet);
	void				processRespawnPlayerPacket(RespawnPlayerPacket & packet);
	void				processLinkPacket(LinkPacket & packet);
	void				processTeamSelectPacket(TeamSelectPacket & packet);
	void				processUpdatePacket(RawPacket & packet);
	void				processTextPacket(TextPacket & packet);
	void				processPlayerListPacket(PlayerListPacket * packet);
	void				processUserPacket(UserPacket & packet);
	void				processChangeModulePacket(ChangeModulePacket & packet);
	void				processCoreAttrPacket(CoreAttrPacket & packet);
	void				processAccountAgePacket(AccountPacket & packet);
	void				processSkillEffectPacket(SkillEffectPacket & packet);
	void				processAchievementResponsePacket(AchievementResponsePacket & packet);
	void				processAICommandPacket(AICommandPacket & packet);

private:

	ClientInterface * network;

	Connections connections;
	TextBuffer	textBuffer;
	PlayerList	players;

	Statistics		stats;
	RespawnTimer	respawnTimer;

	Logger		log;

	Timer		requestTimer;
};

#endif