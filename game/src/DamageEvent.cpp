#include "DamageEvent.h"
#include "EventEnum.h"
#include "Object.h"

DamageEvent::DamageEvent(Attribute damage, Object * senderAndDefender /* = 0 */, Object * attacker /* = 0 */) : Event(EVENT_DAMAGE, senderAndDefender), damage(damage), attacker(attacker)
{}

DamageEvent::~DamageEvent()
{}

Object * DamageEvent::getAttacker()
{
	return attacker;
}

Object * DamageEvent::getDefender()
{
	return sender;
}

void DamageEvent::sendTo(Object * to)
{
	to->receive<DamageEvent>(this);
}