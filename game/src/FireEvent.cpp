#include "FireEvent.h"
#include"Object.h"
#include "EventEnum.h"

FireEvent::FireEvent(Vector3 target, float range, bool high) : Event(EVENT_FIRE), target(target), range(range), high(high)
{}

FireEvent::~FireEvent()
{}

void FireEvent::setTarget(Vector3 tar)
{
	target = tar;
}

void FireEvent::setRange(float r)
{
	range = r;
}

Vector3 FireEvent::getTarget()
{
	return target;
}

float FireEvent::getRange()
{
	return range;
}

bool FireEvent::highArc()
{
	return high;
}

void FireEvent::sendTo(Object * o)
{
	o->receive<FireEvent>(this);
}