#include "ConfirmSkill.h"
#include "EventEnum.h"
#include "Object.h"

ConfirmSkill::ConfirmSkill(int skill, bool allowed) : Event(EVENT_CONFIRM_SKILL), skill(skill), allowed(allowed)
{}

ConfirmSkill::~ConfirmSkill()
{}

int ConfirmSkill::getSkill()
{
	return skill;
}

bool ConfirmSkill::isAllowed()
{
	return allowed;
}

void ConfirmSkill::sendTo(Object * o)
{
	o->receive<ConfirmSkill>(this);
}