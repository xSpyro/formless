
#include "ServerInterface.h"
#include "RequestInterface.h"

#include "NetworkSubsystemServer.h"

#include "CommonPackets.h"
#include "Events.h"

#include "GameHandler.h"
#include "Object.h"

#include "ObjectTemplate.h"

#include "NetworkComponent.h"
#include "PhysicsComponent.h"
#include "CoreComponent.h"
#include "ControllerComponent.h"
#include "StatsComponent.h"
#include "AIComponent.h"

#include "Achievements.h"

NetworkSubsystemServer::NetworkSubsystemServer(GameHandler * game, ServerInterface * network)
	: NetworkSubsystem( game ), network(network), log(std::cout), gameStarted(false), respawnTimerStarted(false)
{
	for(int i = 0; i < NR_TEAMS; i++)
	{
		kills[i] = 0;
	}
	gameStarted = false;
	allReady = false;
	rambo = false;

	//if( !FileSystem::get( "PitOfTorus.flf", &levelData ) )
	//	std::cout << "Level not found" << std::endl;

	gameTimeMins = 5.0f;
}

void NetworkSubsystemServer::update()
{
	static int delta = 0;

	if (++delta < 6)
		return;

	if( components.size() == 0 )
	{
		gameStarted	= false;
		allReady	= false;

		for(int i = 0 ; i < NR_TEAMS; i++)
		{
			kills[i] = 0;
			teams[i].clear();
		}
	}

	updateComponents();

	delta = 0;

	updateGame();
}

void NetworkSubsystemServer::onPacket(RawPacket & raw)
{
	// when you get the connect packet set client it and
	// mount a component to the player object

	// this is also where the server processes the packets
	// but certain packets can only arrive to the clients and same for server
	
	switch (raw.header.id)
	{
	case READY_PACKET:

		processReadyPacket(*reinterpret_cast < ReadyPacket* > (raw.data));
		break;

	case REQUEST_READY_PACKET:

		processRequestReadyPacket(*reinterpret_cast < RequestReadyPacket * > (raw.data));
		break;

	case COMPANION_PACKET:
		
		processCompanionPacket(*reinterpret_cast < CompanionPacket * > (raw.data));
		break;

	case CLIENT_STATE_PACKET:

		processClientState(raw);
		break;

	//case LEVEL_DATA_REQUEST_PACKET:

	//	processLevelDataRequestPacket(*reinterpret_cast < LevelDataRequestPacket* > (raw.data));
	//	break;

	case TEAM_SELECT_PACKET:

		processTeamSelectPacket(*reinterpret_cast < TeamSelectPacket* > (raw.data));
		break;

	case ENTER_LOBBY_PACKET:

		processEnterLobbyPacket(*reinterpret_cast < EnterLobbyPacket* > (raw.data));
		break;

	case REQUEST_PACKET:

		processRequestPacket(*reinterpret_cast < RequestPacket* > (raw.data));
		break;

	case PLAYER_LIST_REQUEST_PACKET:

		processPlayerListRequestPacket(*reinterpret_cast < PlayerListRequestPacket* > (raw.data));
		break;

	case CHANGE_MODULE_PACKET:

		processChangeModulePacket(*reinterpret_cast < ChangeModulePacket* > (raw.data));
		break;

	case CORE_ATTR_PACKET:

		processCoreAttrPacket(*reinterpret_cast < CoreAttrPacket* > (raw.data));
		break;

	case TEXT_PACKET:

		processTextPacket(*reinterpret_cast < TextPacket* > (raw.data));
		break;

	case USER_REQUEST_PACKET:

		processUserRequestPacket(*reinterpret_cast < UserRequestPacket* > (raw.data));
		break;

	case USER_PACKET:

		processUserPacket(*reinterpret_cast < UserPacket* > (raw.data));
		break;
	}
}

void NetworkSubsystemServer::onConnect(unsigned int uid)
{
	if(allReady && !gameStarted)
	{
		network->disconnect(uid);
	}
}

void NetworkSubsystemServer::onDisconnect(unsigned int uid)
{
	// find object
	Components::iterator i = components.find(uid);

	if( players.find( uid ) != players.end() )
	{
		int team = players[ uid ].team;
		
		if( gameStarted )
		{
			MasterSendAsync asend;
			GameEndPacket packet;
			packet.user_id = players[uid].uid;
			if( i != components.end() )
			{
				StatsComponent* stats = i->second->getObject()->findComponent<StatsComponent>();
				if( stats )
				{
					packet.kills = stats->kills;
				}
			}
			asend( packet, sizeof(GameEndPacket));
		}

		teams[ team ].remove( uid );
		teams[ team ].remove( uid * 10 );	//Possible companion
		players.erase( uid );
		players.erase( uid * 10 );	//Possible companion
	}

	// delete owned objects

	if (i != components.end())
	{
		//Remove a possible companion to the disconnected player from connections
		Components::iterator companion = components.find(i->second->getID()*10);
		if(companion != components.end())
		{
			for(std::vector<int>::iterator it = AIconnections.begin(); it != AIconnections.end(); ++it)
			{
				if((*it) == companion->second->getID())
				{
					AIconnections.erase(it);
					break;
				}
			}

			components.erase( companion );
		}

		//Destroy the disconnecting object (which will itself handle the destruction of the companion object)
		getGameHandler()->destroyObject(i->second->getObject());
		components.erase( i );
	}
	else if (components.empty())
	{
		//If we were the only ones left, this will be the case
		AIconnections.clear();
	}

	sendServerState();
}

void NetworkSubsystemServer::onCreateObject(Object * object)
{

}

void NetworkSubsystemServer::onDestroyObject(Object * object)
{
	/*
	NetworkComponent * net = object->findComponent<NetworkComponent>();

	if (net)
	{
		DestroyPacket packet;

		packet.uid = net->getID();

		network->broadcast(packet);
	}
	*/
}

void NetworkSubsystemServer::onLink(std::string name, unsigned int owner, unsigned int target, bool status)
{
	LinkPacket packet;

	ZeroMemory(packet.link, sizeof(packet.link));
	strcpy_s(packet.link, 32, name.c_str());

	packet.owner = owner;
	packet.target = target;
	packet.status = status;

	network->broadcast(packet);
}

void NetworkSubsystemServer::broadcastObject(Object * object)
{
	CreatePacket create;

	std::string obj = object->getTemplate()->name;
	create.creator = 0;
	create.uid = network->getUID();

	ZeroMemory(create.object, sizeof(create.object));
	strcpy_s(create.object, 32, obj.c_str());

	create.x = 0;
	create.y = 0;
	create.z = 0;

	create.team = 0;
}

void NetworkSubsystemServer::respondObject(Object * object, unsigned int uid)
{
	CreatePacket create;

	std::string obj = object->getTemplate()->name;
	create.creator = 0;
	create.uid = uid;

	create.setObjectName(obj);

	create.x = 0;
	create.y = 0;
	create.z = 0;

	create.team = 0;

	network->respond(create);
}

void NetworkSubsystemServer::broadcastPlayerDeath(unsigned int uid, unsigned int team, unsigned int killerId, bool count)
{
	KillPlayerPacket packet;
	packet.uid = uid;
	packet.killerId = killerId;
	packet.count = count;

	if(team > 0)
	{
		kills[team]++;
		playerToRespawn(uid);
		if (killerId != 0)
		{
			if( !rambo )
			{
				MasterRequestAsync areq;
				AchievementRequestPacket packet;
				packet.rambo = true;
				packet.user_id = players[killerId].getUserID();
				packet.type = Achievement::TYPE_FIRST_BLOOD;
				areq(packet, sizeof(packet));
				rambo = true;
			}

			Components::iterator it = components.find( killerId );
			if( it != components.end() )
			{
				Object* obj = it->second->getObject();
				if( obj )
				{
					StatsComponent* stats = obj->findComponent<StatsComponent>();
					if( stats && stats->killStreak >= 5 )
					{
						MasterRequestAsync areq;
						AchievementRequestPacket packet;
						packet.killstreak = stats->killStreak;
						packet.type = Achievement::TYPE_5_KILLSTREAK;
						packet.user_id = players[killerId].getUserID();
						areq( packet, sizeof(packet) );
					}
				}
			}

			log.notice("Player ", players[uid].getName(), " ", uid, " killed by player ", players[killerId].getName());
		}
		else
			log.notice("Player ", players[uid].getName(), " ", uid, " died.");
	}

	network->broadcast(packet);
}

void NetworkSubsystemServer::broadcastPlayerRespawn(unsigned int uid, Vector3 position, bool count)
{
	RespawnPlayerPacket packet;
	packet.uid = uid;
	packet.x = position.x;
	packet.y = position.y;
	packet.z = position.z;
	packet.count = count;

	network->broadcast(packet);
}

void NetworkSubsystemServer::broadcastAICommand(unsigned int uid, std::string command, int state, Vector3 targetPosition)
{
	AICommandPacket packet;
	packet.uid			= uid;
	packet.commandState = state;
	strcpy_s(packet.command, 32, command.c_str());
	packet.targetX		= targetPosition.x;
	packet.targetY		= targetPosition.y;
	packet.targetZ		= targetPosition.z;

	network->broadcast(packet);
}

bool NetworkSubsystemServer::isServer()
{
	return true;
}

const std::string & NetworkSubsystemServer::getPlayerName(unsigned int net_id) const
{
	static std::string result = "-error-";

	Players::const_iterator i = players.find(net_id);
	if (i != players.end())
	{
		result = i->second.getName();
	}

	return result;
}


float NetworkSubsystemServer::gameTimerNow()
{
	return gameTimer.now();
}

bool NetworkSubsystemServer::gameTimerAccumulate(float peak)
{
	return gameTimer.accumulate(peak);
}

ServerInterface* NetworkSubsystemServer::getNetwork()
{
	return network;
}

void NetworkSubsystemServer::broadcastSkillEffect(SkillEffectPacket & packet)
{
	network->broadcast(packet);
}

void NetworkSubsystemServer::setGameTime(float minutes)
{
	gameTimeMins = minutes;
}

float NetworkSubsystemServer::getGameTime() const
{
	return gameTimeMins;
}




void NetworkSubsystemServer::updateComponent(NetworkComponent * com)
{
	Object * object =  com->getObject();

	// its a dummy
	UpdatePacket update;
	update.uid = com->getID();

	if (!com->update(update))
		return;

	if (object)
	{
		PhysicsComponent * physics = object->findComponent<PhysicsComponent>();
		if (physics)
		{
			//std::cout<< "U: " << physics->position << "\n";
			if (physics->position != physics->position)
			{
				physics->position = Vector3(0, 0, 0);
				log.warn("Invalid position found. Reseting to (0, 0, 0)");
			}

			update.x = physics->position.x;
			update.y = physics->position.y;
			update.z = physics->position.z;

			if (physics->velocity != physics->velocity)
			{
				physics->velocity = Vector3(0, 0, 0);
				log.warn("Invalid velocity found. Reseting to (0, 0, 0)");
			}

			update.vx = physics->velocity.x;
			update.vy = physics->velocity.y;
			update.vz = physics->velocity.z; 
			//physics->rotation.getAxisAngle(Vector3(0,0,0), update.angle);
		}

		ControllerComponent * controller = object->findComponent<ControllerComponent>();
		if (controller)
		{
			Vector3 p = controller->getMousePosition();
			update.lx = p.x;
			update.ly = p.y;
			update.lz = p.z;
		}

		CoreComponent * core = object->findComponent<CoreComponent>();
		if (core)
		{
			update.life = core->life.getCurrent();
			update.mana = core->mana.getCurrent();
		}

		network->broadcast(update);
	}
}

void NetworkSubsystemServer::updateComponents()
{
	unsigned int count = 0;

	// loop all network components
	for (Components::iterator i = components.begin(); i != components.end();)
	{
		
		if (!i->second->isReady())
		{
			++i;
			continue;
		}

		Object * object =  i->second->getObject();

		// if is bound
		if (object == NULL)
		{
			if (i->second->getID() >= 10000)
			{
				for(std::vector<int>::iterator it = AIconnections.begin(); it != AIconnections.end(); ++it)
				{
					if((*it) == i->second->getID())
					{
						AIconnections.erase(it);
						break;
					}
				}
			}

			i = components.erase(i);
		}
		else
		{
			updateComponent(i->second);

			++i;
		}
	}
}

void NetworkSubsystemServer::updateGame()
{
	TimerPacket timePacket;
	timePacket.gameStarted = gameStarted;
	timePacket.allReady = allReady;

	if( gameStarted )
	{
		if( !respawnTimerStarted )
		{
			if( respawnTimers[0].accumulate( 10 ) )
			{
				respawnTimers[1].restart();
				respawnTimerStarted = true;
			}
		}
		for( int i = 0; i < 2; i++ )
		{
			if( respawnTimers[i].accumulate( 20 ) )
			{
				respawnDeadClients(i);
				//log("Respawn Timer ", i, " triggered.");
			}
		}

		if( gameTimer.accumulate(1.0f) )
		{
			timePacket.time = gameTimeMins * 60.0f - gameTimer.now();
			for(unsigned int i = 0; i < NR_TEAMS; i++)
			{
				timePacket.kills[i] = kills[i];
			}
			network->broadcast(timePacket);

			if(!respawningPlayers[0].empty())
			{
				RespawnTimerPacket resTimer0;
				resTimer0.time	= 20 - respawnTimers[0].getAccumulated();
				resTimer0.timer	= 0;
				for(unsigned int i = 0; i < respawningPlayers[0].size(); i++)
				{
					resTimer0.playerIDs[i] = respawningPlayers[0][i];
				}
				network->broadcast(resTimer0);
			}
			
			if(!respawningPlayers[1].empty())
			{
				RespawnTimerPacket resTimer1;
				resTimer1.time	= 20 - respawnTimers[1].getAccumulated();
				resTimer1.timer	= 1;
				for(unsigned int i = 0; i < respawningPlayers[1].size(); i++)
				{
					resTimer1.playerIDs[i] = respawningPlayers[1][i];
				}
				network->broadcast(resTimer1);
			}
		}
	}
	else if( allReady )
	{
		if( gameCountdown.accumulate(1.0f) )
		{
			timePacket.time = 5.0f - gameCountdown.now();
			for(unsigned int i = 0; i < NR_TEAMS; i++)
			{
				timePacket.kills[i] = kills[i];
			}
			network->broadcast(timePacket);
		}
	}
	else
	{
		if( gameCountdown.accumulate(1.0f) )
		{
			timePacket.time = 0.0f;
			for(unsigned int i = 0; i < NR_TEAMS; i++)
			{
				timePacket.kills[i] = kills[i];
			}
			network->broadcast(timePacket);
		}
	}

	if( allReady && !gameStarted && gameCountdown.now() >= 5.0f )
	{
		broadcastGameStart();
	}

	if( gameStarted && gameTimer.now() >= gameTimeMins * 60.0f )
	{
		broadcastGameEnd();
	}
}


void NetworkSubsystemServer::processReadyPacket(ReadyPacket & packet)
{
	// wait on the server for all connected clients to be ready

	Components::iterator i = components.find(network->getResponseId());
	if (i != components.end())
	{
		i->second->setReady(packet.clientReady);
		packet.uid = network->getResponseId();

		ReadyPacket toClient;
		toClient.clientReady = packet.clientReady;
		toClient.uid = packet.uid;
		network->broadcast(toClient);
	}

	//Check if a companion is available, if it is, ready it up too
	Components::iterator c = components.find(network->getResponseId()*10);
	if (c != components.end())
	{
		c->second->setReady(packet.clientReady);
		packet.uid = network->getResponseId();

		ReadyPacket toClient;
		toClient.clientReady = packet.clientReady;
		toClient.uid = packet.uid * 10;
		network->broadcast(toClient);
	}

	if( packet.clientReady )
		log("Client ready ", network->getResponseId());
	else
		log("Client canceled ready ", network->getResponseId());

	if( !gameStarted )
		checkClientsReady();
}

void NetworkSubsystemServer::processRequestReadyPacket(RequestReadyPacket & packet)
{
	for(Components::iterator it = components.begin(); it != components.end(); ++it)
	{
		ReadyPacket response;

		response.uid			= it->second->getID();
		response.clientReady	= it->second->isReady();

		network->respond(response);
	}
}

void NetworkSubsystemServer::processCompanionPacket(CompanionPacket & packet)
{
	if(packet.add == true)
	{
		std::cout << "Player ID " << packet.uid << " added a companion." << std::endl;

		CreatePacket create;

		std::string obj = "AIPlayer";
		create.creator = 0;
		create.uid = network->getResponseId() * 10;

		create.setObjectName( obj );

		create.x = 0;
		create.y = 0;
		create.z = 0;

		NetworkComponent * com = processCreatePacket(create);
		if (com)
		{
			com->setReady(false);

			log("Companion ", create.uid, " created for ", network->getResponseId());

			Object* obj = com->getObject();

			if (obj)
			{
				CoreComponent* core = obj->findComponent<CoreComponent>();
				core->team = packet.team;
				obj->sendInternal(&KillPlayerEvent());
				broadcastPlayerDeath(create.uid, 0, 0, false);

				AIconnections.push_back(create.uid);

				// broadcast the client to other clients
				broadcastWithWorldState();

				sendServerState();

				AIComponent* ai = obj->findComponent<AIComponent>();
				if(ai)
					ai->masterUserID = packet.masterUserID;

				ReadyPacket toClient;
				toClient.clientReady = false;
				toClient.uid = create.uid;
				network->broadcast(toClient);

				EnterLobbyPacket enterLobby;
				enterLobby.uid = create.uid;
				enterLobby.team = packet.team;
				processEnterLobbyPacket(enterLobby);
			}
			else
			{
				log.alert("NetworkComponent Unbound!");
			}
		}
	}
	else
	{
		std::cout << "Player ID " << packet.uid << " removed a companion." << std::endl;

		DestroyPacket destroy;

		destroy.uid = packet.uid * 10;

		Components::iterator i = components.find(destroy.uid);

		if (i != components.end())
		{
			log("Object Destroyed ", destroy.uid);

			if(i->second->getObject() != NULL)
				getGameHandler()->destroyObject(i->second->getObject());

			components.erase(i);
		}

		for(std::vector<int>::iterator it = AIconnections.begin(); it != AIconnections.end(); ++it)
		{
			if((*it) == destroy.uid)
			{
				AIconnections.erase(it);
				break;
			}
		}

		players.erase(destroy.uid);

		broadcastWithWorldState();
		
		sendServerState();
	}

}

void NetworkSubsystemServer::processTeamSelectPacket(TeamSelectPacket & packet)
{
	if ( packet.team == -1)
	{
		// we leave our current team if any
		packet.errorCode = 0;

		// reset the teams
		leaveTeam(network->getResponseId());

		// remove object
		Components::iterator i = components.find(network->getResponseId());
		if (i != components.end())
		{
			getGameHandler()->destroyObject(i->second->getObject());
			components.erase( i );
		}

		sendServerState();
		

		//onJoinServer(network->getResponseId(), -1);
	}
	else
	{
		if( packet.team != 0 && teams[ packet.team ].size() < 4 )
		{
			packet.errorCode = 0;
		}
		else if( packet.team == 0 )
		{
			//Spectator
			packet.errorCode = 0;
			onJoinServer(network->getResponseId(), 0);
		}
		else
		{
			packet.errorCode = 1;
		}
	}

	printTeams();

	network->respond( packet );
}

void NetworkSubsystemServer::processEnterLobbyPacket(EnterLobbyPacket & packet)
{
	allReady = false;

	unsigned int id = packet.uid;

	if(id < 1000)						//If the ID is not set to any player
		id = network->getResponseId();

	if(id < 10000)						//If the ID indicates a player and not an AI
		onJoinServer( id, 0);
	else
		broadcastPlayerDeath(id, 0, 0, false);

	Components::iterator comp = components.find( id );

	int team = packet.team;

	if( comp != components.end() )
	{
		CoreComponent* core = comp->second->getObject()->findComponent<CoreComponent>();
		core->team = team;
		broadcastWithWorldState();
	}

	Players::iterator it = players.find( id );

	if( it == players.end() )
	{
		MasterRequestAsync request;
		QueryUsernamePacket query;

		if(packet.uid < 10000)		//If it's not a companion, we recieved the Masterserver User ID in packet.uid
			query.uid = packet.uid;
		else if(comp != components.end())	//If it is a companion, we have to fetch that ID. Although, it is the user ID of
		{									//the player, since companions have no users. Send it as negative, the master server will handle it
			AIComponent* ai = comp->second->getObject()->findComponent<AIComponent>();
			if(ai)
				query.uid = ai->masterUserID * -1;
		}

		query.nid = id;

		if( request( query, sizeof(query) ) )
		{
			players[ id ] = PlayerInfo( 0, team, "Unknown" );
			players[ id ].nid = id;
		}

		teams[ team ].remove( id );
		teams[ team ].push_back( id );

		log( "Unknown player joined the lobby. Requesting user info..." );

		sendServerState();
	}
}

void NetworkSubsystemServer::processRequestPacket(RequestPacket & packet)
{
	Components::iterator i = components.find(packet.uid);

	//std::cout << "ProcessRequest " << packet->uid << std::endl;

	if (i != components.end())
	{
		//processRequest(packet->type, i->second);
	}
}

void NetworkSubsystemServer::processPlayerListRequestPacket(PlayerListRequestPacket & packet)
{
	PlayerListPacket response;
	PlayerList buffer;

	for( Players::const_iterator it = players.cbegin(); it != players.cend(); ++it )
	{
		buffer.registerItem( it->second );
	}

	char* data = buffer.getDataPointer();
	unsigned int size = buffer.getDataSize();

	response.results = buffer.getNumberOfEntries();
	response.size = sizeof(PlayerListPacket) - RawPacket::HeaderSize + size;

	network->respond( reinterpret_cast< char* > (&response), sizeof(PlayerListPacket) );

	if( response.results > 0 )
	{
		network->respond( data, size );
	}
}

void NetworkSubsystemServer::processChangeModulePacket(ChangeModulePacket & packet)
{
	unsigned int ID = network->getResponseId();

	if(packet.companion == true && ID < 10000)
		ID *= 10;

	Components::iterator i = components.find(ID);

	if (i != components.end())
	{
		Object * object = i->second->getObject();

		// forward
		CoreComponent * core = object->findComponent<CoreComponent>();

		if( packet.add )
			core->addModule((int)packet.slot, packet.moduleid, packet.module);
		else
			core->removeModule((int)packet.slot);

		ControllerComponent * cont = object->findComponent<ControllerComponent>();

		if (cont)
		{
			cont->setSpeed(core->speed.getMax());
		}
		else
		{
			AIComponent* ai = object->findComponent<AIComponent>();

			if(ai)
				ai->setSpeed(core->speed.getMax());
		}

		sendModuleChange(i->first, object, (int)packet.slot, packet.add);
	}
}

void NetworkSubsystemServer::processCoreAttrPacket(CoreAttrPacket & packet)
{
	unsigned int ID = network->getResponseId();

	if(packet.companion == true && ID < 10000)
		ID *= 10;

	Components::iterator i = components.find(ID);

	if(i != components.end())
	{
		Object * object = i->second->getObject();

		CoreComponent * core = object->findComponent<CoreComponent>();

		core->setSlots(packet.nrSlots, packet.nrSkills);
		core->life.setBase(packet.life);
		core->mana.setBase(packet.mana);
		core->lifeRegen.setBase(packet.lifeRegen);
		core->manaRegen.setBase(packet.manaRegen);
		core->speed.setBase(packet.speed);
		core->name = packet.name;

		ControllerComponent * cont = object->findComponent<ControllerComponent>();
		if(cont)
		{
			cont->setSpeed(core->speed.getMax());
		}
		else
		{
			AIComponent* aicomp = object->findComponent<AIComponent>();
			if(aicomp)
				aicomp->setSpeed(core->speed.getMax());
		}

		sendCoreChange( i->first, object, packet.companion );
	}
}


void NetworkSubsystemServer::processTextPacket(TextPacket & packet)
{
	packet.senderId = network->getResponseId();
	packet.team = players[ network->getResponseId() ].team;
	packet.setName( players[ network->getResponseId() ].name );

	network->broadcast( packet );

	log("Chat Message \"", packet.text, "\" (", network->getResponseId(), ")");
}

void NetworkSubsystemServer::processUserRequestPacket(UserRequestPacket & packet)
{
	log("Recieved request for username with NetID: ", packet.uid);
	UserPacket response;
	response.nid = packet.uid;

	if( players.find( packet.uid ) != players.end() )
	{
		response.uid = players[ packet.uid ].uid;
		response.team = players[ packet.uid ].team;
		response.setName( players[ packet.uid ].name );
	}
	else
	{
		response.uid = 0;
		response.team = 0;
		response.setName( "Unknown");
	}

	network->respond( response );
}

void NetworkSubsystemServer::processUserPacket(UserPacket & packet)
{
	unsigned int id = packet.nid;

	players[ id ] = PlayerInfo( packet.uid, players[id].team, packet.name );
	players[ id ].nid = id;

	log(players[id].name, " entered the lobby (UserID: ", players[id].uid, "; NetID: ", id, ")");

	/* Might be needed sometime, leaving it as comments... */
	//UserPacket user;
	//user.nid = id;
	//user.uid = players[ id ].uid;
	//user.team = players[ id ].team;
	//user.setName( players[ id ].name );

	//network->broadcast( user );
}

void NetworkSubsystemServer::respondWithWorldState()
{
	// loop all network components
	for (Components::iterator i = components.begin(); i != components.end(); ++i)
	{
		CreatePacket create;

		Object * object =  i->second->getObject();
		if (object == NULL)
			continue;

		std::string obj = object->getTemplate()->name;

		ZeroMemory(create.object, sizeof(create.object));
		strcpy_s(create.object, 32, obj.c_str());

		create.x = create.y = create.z = 0;
		create.uid = i->first;

		PhysicsComponent * physics = object->findComponent<PhysicsComponent>();
		if (physics)
		{
			create.x = physics->position.x;
			create.y = physics->position.y;
			create.z = physics->position.z;
		}
		CoreComponent * core = object->findComponent<CoreComponent>();
		if (core)
		{
			create.team = core->team;
		}

		network->respond(create);

		//CoreComponent * core = object->findComponent<CoreComponent>();
		if (core)
		{
			// then we must also submit the modules and core
			//CoreAttrPacket
			CoreAttrPacket pcore = core->getAttrPacket();
			pcore.uid = i->first;
			network->respond(pcore);

			for (int j = 0; j < core->getTotalSlots(); ++j)
			{
				if (core->getModule(j))
				{
					ChangeModulePacket pmod = core->getModulePacket(j);
					pmod.uid = i->first;
					network->respond(pmod);
				}
			}
		}

		
	}
}

void NetworkSubsystemServer::broadcastWithWorldState()
{
	unsigned int count = 0;

	// loop all network components
	for (Components::iterator i = components.begin(); i != components.end(); ++i)
	{
		CreatePacket create;

		Object * object =  i->second->getObject();
		if (object == NULL)
			continue;

		++count;

		std::string obj = object->getTemplate()->name;

		ZeroMemory(create.object, sizeof(create.object));
		strcpy_s(create.object, 32, obj.c_str());

		create.x = create.y = create.z = 0;
		create.uid = i->first;

		PhysicsComponent * physics = object->findComponent<PhysicsComponent>();
		if (physics)
		{
			create.x = physics->position.x;
			create.y = physics->position.y;
			create.z = physics->position.z;
		}
		CoreComponent * core = object->findComponent<CoreComponent>();
		if (core)
		{
			create.team = core->team;
		}

		network->broadcast(create);

		sendInventoryChanges(i->first, object);
	}

	log("BroadcastedState ", count);
}

void NetworkSubsystemServer::broadcastGameStart()
{
	time_t start = time(NULL);

	for(Components::iterator it = components.begin(); it != components.end(); ++it)
	{
		Object* obj;
		obj = it->second->getObject();

		if(obj)
		{
			CoreComponent* core = obj->findComponent<CoreComponent>();

			if(core)
			{
				if(core->team > 0)
				{
					Vector3 randomPos(random(-200.0f, 200.0f), 0.0f, random(-200.0f, 200.0f));
					it->second->getObject()->sendInternal( &RespawnPlayerEvent(randomPos, false) );
					broadcastPlayerRespawn( it->second->getID(), randomPos, false );
				}
				else
					log("Starting game with a network component whose object has a core but no team! (Spectate mode)");
			}
			else
				log("Starting game with a network component whose object has no core!");

			obj->sendInternal(&GameStartEvent());
			
		}
		else
			log("Starting game with a network component that has no object!");
	}

	// Master server sync
	for( unsigned int i = 1; i < NR_TEAMS; ++i )
	{
		for( Team::const_iterator it = teams[i].begin(); it != teams[i].end(); ++it )
		{
			MasterSendAsync async;
			GameStartPacket start;
			start.user_id = players[ *it ].getUserID();
			async( start, sizeof(start) );
		}
	}

	network->broadcast(GameStartPacket());

	gameStarted = true;
	for(unsigned int i = 0; i < NR_TEAMS; i++)
	{
		kills[i] = 0;
	}
	respawnTimers[0].restart();
	respawnTimers[1].restart();
	gameTimer.restart();
}

void NetworkSubsystemServer::broadcastGameEnd()
{
	for(Components::iterator it = components.begin(); it != components.end(); ++it)
	{
		it->second->setReady(false);

		Object* obj;
		obj = it->second->getObject();

		if(obj)
		{
			// Master server sync
			MasterSendAsync async;
			GameEndPacket end;
			end.user_id = players[ it->second->getID() ].getUserID();
			end.full_time = true;
			if( it != components.end() )
			{
				StatsComponent* stats = it->second->getObject()->findComponent<StatsComponent>();
				if( stats )
				{
					end.kills = stats->kills;
				}
			}
			async( end, sizeof(end) );

			obj->sendInternal(&KillPlayerEvent());
			broadcastPlayerDeath(it->second->getID(), 0, 0, false);
		}
		else
			log("Ending game with a network component that has no object!");
	}

	allReady = false;
	gameStarted = false;

	// Master server sync
	//for( unsigned int i = 1; i < NR_TEAMS; ++i )
	//{
	//	for( Team::const_iterator it = teams[i].begin(); it != teams[i].end(); ++it )
	//	{
	//		MasterSendAsync async;
	//		GameEndPacket end;
	//		end.user_id = players[ *it ].getUserID();
	//		end.kills = players[ *it ].kills;
	//		async( end, sizeof(end) );
	//	}
	//}

	network->broadcast(GameEndPacket());
}

void NetworkSubsystemServer::processRequest(unsigned int type, NetworkComponent * component)
{
	Object * object = component->getObject();
	if (object == NULL)
		return;
	
	//std::cout << "Process RequestPacket" << std::endl;

	switch ( type )
	{
	case CREATE_PACKET:

		respondObject(object, component->getID());
		break;
	}
}

void NetworkSubsystemServer::processClientState(RawPacket & raw)
{
	Components::iterator i = components.find(network->getResponseId());

	// pre component
	ClientStatePacket & packet = *reinterpret_cast < ClientStatePacket* > (raw.data);

	if (packet.input > 0)
	{
		//log("Input from ", network->getResponseId(), " - ", packet.input);
	}

	if (i != components.end())
	{
		// forward
		i->second->onPacket(raw);
	}
}

void NetworkSubsystemServer::sendServerState()
{
	ServerStatePacket response;

	// create a server state packet
	unsigned int cons = network->getConnectionCount();

	for (unsigned int i = 0; i < cons; ++i)
	{
		unsigned int uid = network->getConnectionID(i);

		response.net_id[i] = uid;
	}

	for (unsigned int i = 0; i < AIconnections.size(); i++)
	{
		unsigned int uid = AIconnections[i];

		response.net_id[cons + i] = uid;
	}

	response.connections = cons + AIconnections.size();

	network->broadcast(response);

	log("ServerStatePacket sent (", cons + AIconnections.size(), ")");
}

void NetworkSubsystemServer::sendInventoryChanges(unsigned int uid, Object * object)
{
	CoreComponent * core = object->findComponent<CoreComponent>();
	if (core)
	{
		// then we must also submit the modules and core
		//CoreAttrPacket
		CoreAttrPacket pcore = core->getAttrPacket();
		pcore.uid = uid;
		network->broadcast(pcore);

		for (int j = 0; j < core->getTotalSlots(); ++j)
		{
			if (core->getModule(j))
			{
				ChangeModulePacket pmod = core->getModulePacket(j);
				pmod.uid = uid;
				network->broadcast(pmod);
			}
		}
	}
}

void NetworkSubsystemServer::sendCoreChange(unsigned int uid, Object* object, bool companion )
{
	CoreComponent * core = object->findComponent<CoreComponent>();
	if( core )
	{
		CoreAttrPacket packet = core->getAttrPacket();
		if(companion && uid < 10000)
			packet.uid = uid * 10;
		else
			packet.uid = uid;
		packet.companion = false;

		network->broadcast(packet);
	}
}

void NetworkSubsystemServer::sendModuleChange(unsigned int uid, Object* object, unsigned int module, bool add )
{
	CoreComponent * core = object->findComponent<CoreComponent>();
	if( core )
	{
		if( core->getModule( module ) )
		{
			ChangeModulePacket packet = core->getModulePacket(module);
			packet.uid = uid;
			packet.add = add;
			network->broadcast(packet);
		}
	}
}

void NetworkSubsystemServer::onJoinServer( unsigned int uid, int team )
{
	CreatePacket create;

	std::string obj = "Player";
	create.creator = 0;
	create.uid = network->getResponseId();

	create.setObjectName( obj );

	create.x = 0;
	create.y = 0;
	create.z = 0;

	NetworkComponent * com = processCreatePacket(create);
	if (com)
	{
		com->setReady(false);

		log("Player Created ", create.uid);

		Object* obj = com->getObject();

		if (obj)
		{
			obj->findComponent<CoreComponent>()->team = team;
			//obj->sendInternal(&KillPlayerEvent());
			broadcastPlayerDeath(uid, 0, 0, false);
		}
		else
		{
			log.alert("NetworkComponent Unbound!");
		}
	}


	// broadcast the client to other clients

	broadcastWithWorldState();

	// we dont submit this object since its just to put all modules on the correct
	// spots and we submit on the enter arena
}


void NetworkSubsystemServer::checkClientsReady()
{
	int nrReady = 0;

	for(Components::iterator it = components.begin(); it != components.end(); ++it)
	{
		

		bool isSpectator = false;

		Object * obj = it->second->getObject();
		if (obj)
		{
			CoreComponent * cc = obj->findComponent<CoreComponent>();
			if (cc->team == 0)
				isSpectator = true;
		}

		if(it->second->isReady() || isSpectator)
		{
			nrReady++;
		}
	}

	if(nrReady == components.size())
	{
		log("All clients ready. Starting game countdown");
		allReady = true;
		gameCountdown.restart();
	}
	else
	{
		allReady = false;
	}
}

void NetworkSubsystemServer::respawnDeadClients(int timer)
{
	if (timer == 0 || timer == 1)
	{	
		for (int n = respawningPlayers[timer].size() - 1; n >= 0; --n)
		{
			std::cout << "Trying to respawn " << respawningPlayers[timer][n] << "\n";
			Components::iterator it = components.find(respawningPlayers[timer][n]);
			if( it != components.end() )
			{
				if (!it->second->getObject()->findComponent<CoreComponent>()->isPlayerAlive())
				{
					Vector3 randomPos(random(-200.0f, 200.0f), 0.0f, random(-200.0f, 200.0f));
					it->second->getObject()->sendInternal( &RespawnPlayerEvent(randomPos) );
					broadcastPlayerRespawn(respawningPlayers[timer][n], randomPos);
					std::cout << "Respawn message sent for " << respawningPlayers[timer][n] << "\n";
				}
			}
		}
		respawningPlayers[timer].clear();
	}
	else
	{
		for(Components::iterator it = components.begin(); it != components.end(); ++it)
		{
			if( !it->second->getObject()->findComponent<CoreComponent>()->isPlayerAlive() )
			{
				Vector3 randomPos(random(-200.0f, 200.0f), 0.0f, random(-200.0f, 200.0f));
				it->second->getObject()->sendInternal( &RespawnPlayerEvent(randomPos) );
				broadcastPlayerRespawn( it->second->getID(), randomPos);
			}
		} 
	}
	/*	Vector3 randomPos(random(-200.0f, 200.0f), 0.0f, random(-200.0f, 200.0f));
		it->second->getObject()->sendInternal( &RespawnPlayerEvent(randomPos) );
		broadcastPlayerRespawn( it->second->getID(), randomPos );
	*/
}


void NetworkSubsystemServer::playerToRespawn(unsigned int id)
{
	if (respawnTimers[0].getAccumulated() > 5.0f && respawnTimers[0].getAccumulated() <= 15.0f)
	{
		respawningPlayers[0].push_back(id);
	}
	else
	{
		respawningPlayers[1].push_back(id);
	}
}

void NetworkSubsystemServer::leaveTeam(unsigned int net_id)
{
	// reset the players data
	players.erase(network->getResponseId());

	// leave teams
	for (unsigned int x = 0; x < NR_TEAMS; ++x)
	{
		for (Team::iterator i = teams[x].begin(); i != teams[x].end();)
		{
			if (*i == net_id)
			{
				i = teams[x].erase(i);
			}
			else
			{
				 ++i;
			}
		}
	}
}

void NetworkSubsystemServer::printTeams()
{
	for (unsigned int x = 0; x < NR_TEAMS; ++x)
	{
		log("Team: ", x);
		for (Team::iterator i = teams[x].begin(); i != teams[x].end(); ++i)
		{
			log("* member: ", *i);
		}
	}
}