#ifndef FUNCTION_TABLE_H
#define FUNCTION_TABLE_H
#include "Common.h"
#include "EventEnum.h"

class Object;

class FunctionTable
{
public:
	FunctionTable();
	~FunctionTable();

	void load(const EventLua::Object & script);
	void clear();

	EventLua::Object * getFunction(std::string func, Object * caller); // Assumes that it will just be called and REMEMBER TO CALL functionDone afterwards
	void functionDone();
private:
	typedef std::map<std::string, EventLua::Object> Table;
	Table table;
};

#endif