#ifndef SKILL_EFFECT_H
#define SKILL_EFFECT_H

struct SkillEffect
{
	float damage;
	float particleDamage;
	float range;
	int stuntime;
	float knockback;
	float speedChange; // Only applied once
	bool blockSkills;
	bool knockbackFromOwner; // knockback is calculated from the player instead of the skill object.
	bool noDamageOffset;
};

#endif