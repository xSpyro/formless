#ifndef EFFECT_SOUND_SUB_COMPONENT_H

#define EFFECT_SOUND_SUB_COMPONENT_H

#include "SoundSubComponent.h"


class EffectSoundSubComponent : public SoundSubComponent
{
public:
	EffectSoundSubComponent(SoundComponent* p);
	~EffectSoundSubComponent();


	void update();

private:

};


#endif