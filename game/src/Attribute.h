#ifndef ATTRIBUTE_H
#define ATTRIBUTE_H
#include "Trait.h"

class Attribute
{
public:
	Attribute(float base = 0, float offset = 0, float modifier = 1, bool allowZero = true, bool multiplicativeMod = false);

	bool damage(float damage); // returns if alive
	bool pay(float cost); // returns true if could remove change name?

	float getBase();
	float getMax();
	float getOffset();
	float getModifier();
	float getCurrent();
	float getPercentage();
	float getRegen();
	Attribute * getRegenAttribute();

	void add(Trait trait);
	void remove(Trait trait);

	void setCurrent(float current);
	void setBase(float base);
	void setRegen(Attribute * reg);
	void setModifier(float mod);
	void setOffset(float off);
	void changeOffset(float change);
	void changeModifier(float change, bool add = true);
	bool isMulticative();

	void update();
	void revive();
	void drain();
	void reset();
private:
	void updateValue();

	float current;

	float base;
	float max;
	float offset; // e.g. +40
	float modifier; // e.g. +10%
	Attribute * regen;

	bool multiplicativeMod;
	bool allowZero; // If pay allows it to be zero
};

#endif