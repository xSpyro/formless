#include "MovementSoundSubComponent.h"
#include "KillPlayerEvent.h"

#include "EffectEvent.h"


#include "SoundSubsystem.h"

MovementSoundSubComponent::MovementSoundSubComponent(SoundComponent* p)
	: SoundSubComponent(p)
{
	parentPhys = NULL;
}

MovementSoundSubComponent::~MovementSoundSubComponent()
{

}

void MovementSoundSubComponent::updateSubComponent()
{
	if( parentPhys == NULL )
		parentPhys = parent->getObject()->findComponent<PhysicsComponent>();

	if (parent->getObject() != NULL && parent->getSoundName() != "")
	{
		if(parentPhys)
		{
			Vector3 vel = parentPhys->movement;

			if( vel.lengthNonSqrt() >= 1.0f)
			{
				if (!parent->isPlaying())
				{
					parent->setStartPlaying(true);
				}
			}

			else if ( vel.lengthNonSqrt() <= 0.1f)
			{
				if (parent->isPlaying())
				{
					parent->setStopPlaying(true);
				}
			}

		}
	}
}



void MovementSoundSubComponent::receive(EffectEvent* effect)
{
	if( parent && effect->isRemarkable())
	{
		if (parent->getObject() != NULL)
		{
			if(parentPhys && effect->getEffect()->damage > 0.0f)
			{
				SoundSubsystem* s = (SoundSubsystem*)parent->getSubsystem();

				if (parent->isPlayer)
					parent->getAudio()->play("self_hurt0.mp3", s->getMasterVolume() * s->getFXVolume() * 0.3f, false);
				else
					parent->getAudio()->play3D("hurt3.mp3", parentPhys->getPosition(), s->getMasterVolume() * s->getFXVolume() * 0.75f, false, true);
			}

		}
	}
} 
void MovementSoundSubComponent::receive(KillPlayerEvent * kill)
{

	if( parent )
	{
		if (kill->getCount())
		{
			if(parentPhys)
			{
				SoundSubsystem* s = (SoundSubsystem*)parent->getSubsystem();

				if (parent->isPlayer)
				{
					parent->getAudio()->play("death0.ogg",s->getMasterVolume() * s->getFXVolume() * 0.5f, false);
				}
				else
				{
					parent->getAudio()->play3D("death2.mp3", parentPhys->getPosition(), s->getMasterVolume() * s->getFXVolume() * 0.7f, false, true);
				}
			}
		}
	}

}