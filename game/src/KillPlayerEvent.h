#ifndef KILL_PLAYER_EVENT_H
#define KILL_PLAYER_EVENT_H
#include "Event.h"

class KillPlayerEvent : public Event
{
public:
	KillPlayerEvent(Object * killer = 0, Object * senderAndKilled = 0, bool countDeath = true); // The killed one should always be sender
	virtual ~KillPlayerEvent();

	virtual void sendTo(Object* to);

	void setKiller(Object * o);
	void setKilled(Object * o);
	void setCount(bool countDeath);

	Object *	getKiller();
	Object *	getKilled();
	bool		getCount();
private:
	Object *	killer;
	bool		count;
};

#endif