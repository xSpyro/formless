#include "KillPlayerEvent.h"
#include "EventEnum.h"
#include "Object.h"

KillPlayerEvent::KillPlayerEvent(Object * killer /* = 0 */, Object * sender /* = 0 */, bool countDeath /* = true */) : Event(EVENT_KILL_PLAYER, sender), killer(killer), count(countDeath)
{}

KillPlayerEvent::~KillPlayerEvent()
{
}

void KillPlayerEvent::sendTo(Object* to)
{
	to->receive<KillPlayerEvent>(this);
}


void KillPlayerEvent::setKiller(Object * o)
{
	killer = o;
}

void KillPlayerEvent::setKilled(Object * o)
{
	sender = o;
}

void KillPlayerEvent::setCount(bool countDeath)
{
	count = countDeath;
}

Object * KillPlayerEvent::getKiller()
{
	return killer;
}

Object * KillPlayerEvent::getKilled()
{
	return sender;
}

bool KillPlayerEvent::getCount()
{
	return count;
}