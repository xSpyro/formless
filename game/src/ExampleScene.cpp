
#include "ExampleScene.h"
#include "Common.h"
#include "Object.h"
#include "LogicHandler.h"
#include "RenderComponent.h"
#include "GameConnector.h"
#include "ScriptEnvironment.h"
#include "PhysicsComponent.h"

#include "CoreComponent.h"

#include "Framework.h"

ExampleScene::ExampleScene()
	: Scene()
{
	
}

ExampleScene::ExampleScene(LogicHandler* handler, Renderer* rend, Window* wndw)
	: Scene(handler, rend, wndw)
{
	layer = renderer->createLayer();

	camera.setPosition(Vector3(0,50,-30.0f));
	camera.setLookAt(Vector3(0, 0, 0));
	camera.setClipRange(10, 1000);
	camera.setProjection3D(45, 16 / 9.0f);

	// camera set

	Viewport * vp = layer->addViewport(&camera);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));
}

ExampleScene::~ExampleScene()
{

}

void ExampleScene::update()
{
	camera.update();
}

void ExampleScene::render()
{

}

void ExampleScene::onEnter()
{

	player = parent->createObject("Player");
	parent->createObject("Ball");

	player->findComponent<CoreComponent>()->addModule( 2, "fireball" );
	player->findComponent<CoreComponent>()->addModule( 0, "iceball" );
	/*env->push(player);
	create("Ball");
	env->pop();*/

	renderer->insertLayer(0, layer);
}

void ExampleScene::onLeave()
{
	renderer->clearLayers();
}

bool ExampleScene::onInput(int sym, int state)
{
	return false;
}

bool ExampleScene::onMouseMovement(int x, int y)
{
	return false;
}

void ExampleScene::onCreateObject(Object* object)
{
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();

	if(rendcomp)
	{
		rendcomp->load(renderer);
		layer->add(&rendcomp->model);
	}	
}