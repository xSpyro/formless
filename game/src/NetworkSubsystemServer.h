
#ifndef NETWORKSUBSYSTEM_SERVER_H
#define NETWORKSUBSYSTEM_SERVER_H

#include "Common.h"
#include "NetworkSubsystem.h"

#include "CommonPackets.h"

#include "PlayerInfo.h"
#include "PlayerList.h"
#include "../framework/src/Timer.h"
#include "../framework/src/Vector3.h"
#include "../framework/src/File.h"


class ServerInterface;
class GameHandler;

class Object;

class NetworkSubsystemServer : public NetworkSubsystem
{

	typedef std::queue < unsigned int > NetIdQueue;
	typedef std::map< unsigned int, PlayerInfo > Players;
	typedef std::list< unsigned int > Team;
	typedef std::vector< unsigned int > PlayerIds;

public:

	NetworkSubsystemServer(GameHandler * game, ServerInterface * network);

	void update();

	void onPacket(RawPacket & packet);
	void onConnect(unsigned int uid);
	void onDisconnect(unsigned int uid);

	void onCreateObject(Object * object);
	void onDestroyObject(Object * object);

	void onLink(std::string linkName, unsigned int owner, unsigned int target, bool status); // Broadcast link

	void broadcastObject(Object * object);
	void respondObject(Object * object, unsigned int uid);

	void broadcastPlayerDeath(unsigned int uid, unsigned int team = 0, unsigned int killerId = 0, bool count = true);
	void broadcastPlayerRespawn(unsigned int uid, Vector3 position = Vector3(0,0,0), bool count = true);

	void broadcastAICommand(unsigned int uid, std::string command, int state, Vector3 targetPosition);

	bool isServer();

	const std::string & getPlayerName(unsigned int net_id) const;

	float	gameTimerNow();
	bool	gameTimerAccumulate(float peak);

	ServerInterface * getNetwork();
	
	void broadcastSkillEffect(SkillEffectPacket & packet);

	void	setGameTime(float minutes);
	float	getGameTime() const;

private:

	void updateComponent(NetworkComponent * com);
	void updateComponents();
	

	void updateGame();

	void processReadyPacket(ReadyPacket & packet);
	void processRequestReadyPacket(RequestReadyPacket & packet);
	void processCompanionPacket(CompanionPacket & packet);
	void processTeamSelectPacket(TeamSelectPacket & packet);
	void processEnterLobbyPacket(EnterLobbyPacket & packet);
	void processRequestPacket(RequestPacket & packet);
	void processPlayerListRequestPacket(PlayerListRequestPacket & packet);
	void processChangeModulePacket(ChangeModulePacket & packet);
	void processCoreAttrPacket(CoreAttrPacket & packet);
	void processTextPacket(TextPacket & packet);
	void processUserRequestPacket(UserRequestPacket & packet);
	void processUserPacket(UserPacket & packet);

	void respondWithWorldState();
	void broadcastWithWorldState();

	void broadcastGameStart();
	void broadcastGameEnd();

	void processRequest(unsigned int type, NetworkComponent * component);
	void processClientState(RawPacket & raw);

	void sendServerState();

	void sendInventoryChanges(unsigned int uid, Object * object);

	void sendCoreChange(unsigned int uid, Object* object, bool companion = false );
	void sendModuleChange(unsigned int uid, Object* object, unsigned int module, bool add = true );

	void onJoinServer( unsigned int uid, int team );
	void checkClientsReady();
	void respawnDeadClients(int timer = -1);

	void playerToRespawn(unsigned int id);

private:

	void leaveTeam(unsigned int net_id);
	void printTeams();

private:

	ServerInterface * network;

	NetIdQueue idqueue;

	Logger log;

	//TODO Replace with a proper team deployment
	Players players;
	Team teams[NR_TEAMS];
	int	kills[NR_TEAMS];

	Timer	gameTimer;
	Timer	gameCountdown;
	bool	allReady;
	bool	gameStarted;
	bool	rambo;

	Timer	respawnTimers[2];
	bool	respawnTimerStarted;
	PlayerIds respawningPlayers[2];

	float	gameTimeMins;

	//File levelData;

	std::vector<int> AIconnections;
};

#endif