#ifndef POSITION_EVENT_H
#define POSITION_EVENT_H

#include "Event.h"
#include "Framework.h"

/* Position Event sets the position in PhysicsComponent to the given value, it does not translate */

class PositionEvent : public Event
{
public:
	PositionEvent(float x = 0, float y = 0, float z = 0);
	PositionEvent(Vector3 position);
	virtual ~PositionEvent();

	virtual void sendTo(Object * to);

	void setPosition(Vector3 position);
	Vector3 getPosition();
private:
	Vector3 pos;
};

#endif