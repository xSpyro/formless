#ifndef COLLISION_EVENT_H
#define COLLISION_EVENT_H
#include "Event.h"
#include "../framework/src/IntersectResult.h"

class CollisionEvent : public Event
{
public:
	CollisionEvent();
	CollisionEvent(Object * rhs, Object * lhs, IntersectResult res);
	virtual ~CollisionEvent();

	void set(Object * rhs, Object * lhs, IntersectResult res);

	Object * getLeft();
	Object * getRight();
	IntersectResult getResult();

	virtual void sendTo(Object * to);
protected:
	Object * rhs;
	Object * lhs;
	IntersectResult res;
};

#endif
