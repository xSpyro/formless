#include "MoveEvent.h"
#include "EventEnum.h"
#include "Object.h"

MoveEvent::MoveEvent(): dir(Vector3(0.0f, 0.0f, 0.0f)), Event(EVENT_MOVE, 0, true), speed(0), instant(false)
{}

MoveEvent::MoveEvent(Vector3 dir, float speed, bool instant) : instant(instant), dir(dir), speed(speed) ,Event(EVENT_MOVE, 0, true)
{}

MoveEvent::~MoveEvent()
{
}

void MoveEvent::sendTo(Object * to)
{
	to->receive<MoveEvent>(this);
}

Vector3 MoveEvent::getMovement()
{
	return dir;
}

bool MoveEvent::isInstant()
{
	return instant;
}

float MoveEvent::getSpeed()
{
	return speed;
}

void MoveEvent::setMovement(Vector3 move)
{
	dir = move;
}

void MoveEvent::setSpeed(float s)
{
	speed = s;
}

void MoveEvent::setInstant(bool inst)
{
	instant = inst;
}