
#include "ExplorationField.h"

#include "AIComponent.h"
#include "PhysicsComponent.h"

ExplorationField::ExplorationField(AIComponent* comp /* = NULL */)
	: PotentialField(), component(comp)
{

}

ExplorationField::~ExplorationField()
{

}

float ExplorationField::calcPotential(Vector2 position)
{
	float returnValue = 0.0f;

	if(component)
	{
		if(component->isIdle())
		{
			Vector2 myPos			= Vector2(component->getStructMe().phys->position.x, component->getStructMe().phys->position.z);
			float	distance		= (myPos - position).length();

			returnValue = distance;
		}
	}

	return returnValue;
}