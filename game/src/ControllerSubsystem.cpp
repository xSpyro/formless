
#include "ControllerSubsystem.h"
#include "ControllerComponent.h"

ControllerSubsystem::ControllerSubsystem( GameHandler* gameHandler, int size )
	: Subsystem( gameHandler, "ControllerSubsystem" )
{
	components.reserve(size);
}

ControllerSubsystem::~ControllerSubsystem()
{
}

Component* ControllerSubsystem::createComponent(EventLua::Object script)
{
	ControllerComponent* comp = (ControllerComponent*)this->getComponent();
	if (comp == 0)
		return 0;

	EventLua::Object keybinds = script.get("keybinds");

	EventLua::Object key, val;
	while (keybinds.next(key, val))
	{
		std::string keybind = val.queryString();

		comp->keybinds.push_back(keybind);
	}

	return comp;
}

Component* ControllerSubsystem::getComponent()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if( !(*it).isBound() )
		{
			(*it) = ControllerComponent( this );
			return &(*it);
		}
	}
	if (components.size() == components.capacity())
		return 0;

	components.push_back( ControllerComponent( this ) );
	return &components.back();
}

void ControllerSubsystem::update()
{
	for( Components::iterator it = components.begin(); it < components.end(); it++ )
	{
		if(it->isBound() && it->activated)
			it->update();
	}
}

void ControllerSubsystem::clear()
{
	components.clear();
}