
#include "AISubsystem.h"
#include "AIComponent.h"

AISubsystem::AISubsystem( GameHandler* gameHandler, int size )
	: Subsystem( gameHandler, "AISubsystem" )
{
	components.reserve(size);
}

AISubsystem::~AISubsystem()
{
}

Component* AISubsystem::createComponent(EventLua::Object script)
{
	AIComponent* comp = (AIComponent*)this->getComponent();
	if (comp == 0)
		return 0;

	EventLua::Object commands = script.get("commands");

	EventLua::Object key, val;
	while (commands.next(key, val))
	{
		std::string command = val.queryString();

		comp->commands.push_back(command);
	}

	return comp;
}

Component* AISubsystem::getComponent()
{
	for( Components::iterator it = components.begin(); it != components.end(); ++it )
	{
		if( !(*it).isBound() )
		{
			(*it) = AIComponent( this );
			return &(*it);
		}
	}
	if (components.size() == components.capacity())
		return 0;

	components.push_back( AIComponent( this ) );
	return &components.back();
}

void AISubsystem::update()
{
	for( Components::iterator it = components.begin(); it < components.end(); it++ )
	{
		if(it->isBound() && it->activated)
			it->update();
	}
}

void AISubsystem::clear()
{
	components.clear();
}