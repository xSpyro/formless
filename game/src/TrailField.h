
#ifndef TRAIL_FIELD_H
#define TRAIL_FIELD_H

#include "PotentialField.h"
class AIComponent;

const int TRAIL_SIZE = 100;

class TrailField : public PotentialField
{

public:
	TrailField(AIComponent* component = NULL);
	virtual ~TrailField();

	float	calcPotential(Vector2 position);
	void	update(Vector2 AIposition);

private:
	AIComponent* component;
	Vector2 trail[TRAIL_SIZE];
};

#endif