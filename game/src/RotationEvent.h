#ifndef ROTATION_EVENT_H
#define ROTATION_EVENT_H

#include "Event.h"
#include "Framework.h"

/* Rotation Event applies the given angle rotation to the given axis to the already existing rotation vector in PhysicsComponent */

class RotationEvent : public Event
{
public:
	RotationEvent(float x = 0, float y = 0, float z = 0, float a = 0, bool apply = true);
	RotationEvent(Vector3 axis, float angle, bool apply = true);
	virtual ~RotationEvent();

	virtual void sendTo(Object * to);

	void setAxis(Vector3 rotationAxis);
	void setAngle(float rotationAngle);
	void setApply(bool rotationApply);
	Vector3 getAxis();
	float	getAngle();
	bool	getApply();
private:
	Vector3 axis;
	float	angle;
	bool	apply;
};

#endif