#ifndef TERRAIN_H
#define TERRAIN_H
#include "../../framework/src/Surface.h"
#include "../../framework/src/Vector3.h"
#include <iostream>
class Surface;

class Terrain
{
public:
	float scale;
	float height;
	Vector3 offset;
	Surface surface;

	Terrain();
	bool save(std::ostream & stream);
	bool load(std::istream & stream);

	float sample(float x, float z);
	bool Terrain::inside(float x, float z);

	float findDistance(Vector3 from, Vector3 to); // Returns the height difference at the highest value at the line.
private:
	bool valid(int x, int z); // According to surface coords
	float at(int x, int z); // According to surface coords
};

#endif