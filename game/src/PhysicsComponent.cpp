
#include "PhysicsComponent.h"
#include "RenderComponent.h"
#include "PushEvent.h"
#include "MoveEvent.h"
#include "Object.h"
#include "FireEvent.h"
#include "../framework/src/Intersect.h"
#include "Math.h"
#include "Terrain.h"
#include "GameHandler.h"
#include "CoreComponent.h"

#include "../../formats/src/LabelEnum.h"

#include "Subsystem.h"

// TODO: Remove later after testing?
#include "NetworkComponentDummy.h"

PhysicsComponent::PhysicsComponent( Subsystem* subsystem )
	: Component( subsystem ), gravity(0, 0, 0), unmovable(false), scale(1.0f, 1.0f, 1.0f), useRealSlowdown(true), repulsion(0.0f)
{
	realSlowdown	= 0.8f;
	currentSlowdown	= 0.8f;
	velocity	= Vector3(0.0f, 0.0f, 0.0f);
	movement	= Vector3(0.0f, 0.0f, 0.0f);
	offset		= Vector3( 0.0f, 0.0f, 0.0f );
	hoverPower	= 0.0f;
	speed		= 0.0f;
	maxSpeed	= 0.0f;

	rotation.identity();

	useDest = false;
	useGravity = true;
	useProjectileRotation = false;

	linked		= false;
	ownerPhys	= NULL;
	targetPhys	= NULL;
	ownerCore	= NULL;
	targetCore	= NULL;

	arenaCollisions = true;
}

PhysicsComponent::~PhysicsComponent()
{
}

void PhysicsComponent::receive(PushEvent * move)
{
	if (isStatic())
		return;

	if (move->getSender() != object)
		return;

	velocity += move->getMovement();
}

void PhysicsComponent::receive(MoveEvent * me)
{
	if (isStatic())
		return;

	if (me->getSender() != object)
		return;

	Vector3 move = me->getMovement() * me->getSpeed();
	float newSpeedInOld = movement.dot(move);

	if (newSpeedInOld <= speed)
	{
		velocity += movement * (speed - newSpeedInOld);
		speed = newSpeedInOld;
	}
	maxSpeed = me->getSpeed();
	movement = me->getMovement();

	if (me->isInstant())
	{
		speed = maxSpeed;
		velocity = 0;
	}

}

void PhysicsComponent::receive(PositionEvent * pos)
{
	if (pos->getSender() != object)
		return;

	position = pos->getPosition();
	sphere.origin = position;
}

void PhysicsComponent::receive(RotationEvent * rot)
{
	if (rot->getSender() != object)
		return;

	if(rot->getApply())
		rotateWith(rot->getAxis(), rot->getAngle());
	else
		rotation.setAxisAngle(rot->getAxis(), rot->getAngle());
}

void PhysicsComponent::receive(ScaleEvent * scale)
{
	if (scale->getSender() != object)
		return;

	if(scale->getApply())
		scaleWith(scale->getScale());
	else
		this->scale = scale->getScale();
}

void PhysicsComponent::receive(CreatedEvent * create)
{
	if (create->getSender() != object)
		return;

	sphere.origin = position;

	matrix.identity();

	matrix.scale(scale);
	matrix.rotate(Vector3(0,1,0), 0.0f);
	matrix.translate(position);
}

void PhysicsComponent::receive(EffectEvent * ev) // Warning. How this function worsk have been changed too much from the first intended way so be careful
{
	if (ev->getSender() != object)
		return;

	if (isStatic())
		return;

	if (ev->isEnd())
		return;

	float str = ev->getEffect()->knockback;

	if (str != 0)
	{
		Object * s = ev->getFrom();

		if (!s || s == object)
			return;

		if (ev->getEffect()->knockbackFromOwner)
		{
			// Try to find an owner and use that if it exists.
			CoreComponent * core = s->findFamilyComponent<CoreComponent>();
			if (core)
				s = core->getObject();
		}

		PhysicsComponent * c = s->findComponent<PhysicsComponent>();
		if (!c || c == this)
			return;

		NetworkComponent * net = object->findComponent<NetworkComponent>();
		if (net && !net->isOnServer())
			return;

		Vector3 change;
		IntersectResult ir;
		if(collision(c, &ir))
		{
			change = ir.normal;
		}
		else
			change = getPosition() - c->getPosition();

		float tol = 0.000001f;
		if (change.x == 0 && change.y == 0 && change.z == 0)
			change = Vector3(tol, 0, 0);
		if (change != change)
			change = Vector3(tol, 0, 0);
		change.normalize();
		change = change * str;
		velocity += change;
	}
}

void PhysicsComponent::receive(FireEvent * fire)
{
	if (isStatic())
		return;

	if (fire->getSender() != object)
		return;

	Vector3 target = fire->getTarget();
	Vector3 dir = target - position;
	float y = dir.y;
	dir.y = 0;
	float x = dir.length(); // x in the imagined 2d plane made from dir and the up vector
	dir.normalize();

	disableSlowdown();
	float g = - gravity.y;

	float v = sqrt(fire->getRange() * g);
	float variant = pow(v, 4) - 2 * g * y * pow(v, 2) - pow(g, 2) * pow(x, 2);
	float a;
	if (variant < 0)
	{
		a = (float)D3DX_PI / 4.0f;
	}
	else
	{
		if (fire->highArc())
			variant = - sqrt(variant);
		else
			variant = sqrt(variant);

		float t = (pow(v, 2) - g * y + variant);
		t = t / (2 * (pow(x, 2) + pow(y, 2)));
		t = (x / v) * sqrt(t);
		a = acos(t);
	}

	velocity = dir * (v * cos(a));
	velocity.y += v * sin(a);
}

void PhysicsComponent::receive(KillPlayerEvent * kill)
{
	Component::receive(kill);
}

void PhysicsComponent::receive(RespawnPlayerEvent * res)
{
	Component::receive(res);
	respawn(res->getPosition());
}

void PhysicsComponent::receive(QueryEvent<Vector3> * qv)
{
	if (qv->getSender() != object)
		return;

	switch (qv->getType())
	{
	case QUERY_POSITION:
		qv->setData(getPosition(), 1);
		break;
	case QUERY_SCALE:
		qv->setData(getScale(), 1);
		break;
	default:
		return;
	}
}

void PhysicsComponent::receive(QueryEvent<Quaternion> * qq)
{
	if (qq->getSender() != object)
		return;

	switch (qq->getType())
	{
	case QUERY_ROTATION:
		qq->setData(getRotation(), 1);
		break;
	default:
		return;
	}
}

void PhysicsComponent::receive(LinkEvent* link)
{
	if (link->getSender() != object)
		return;

	if(!linked && link->getState() == true)
	{
		if(object == link->getLink())
		{
			linked = true;
			ownerPhys	= link->getOwner()->findComponent<PhysicsComponent>();
			targetPhys	= link->getTarget()->findComponent<PhysicsComponent>();
			ownerCore	= link->getOwner()->findComponent<CoreComponent>();
			targetCore	= link->getTarget()->findComponent<CoreComponent>();

			if(ownerPhys == NULL)
			{
				subsystem->getLogger()->alert("PhysicsComponent: Trying to link, but link owner has no PhysicsComponent. Cancelling link!");
				linked = false;
				targetPhys	= NULL;
				ownerCore	= NULL;
				targetCore	= NULL;
				return;
			}
			if(targetPhys == NULL)
			{
				subsystem->getLogger()->alert("PhysicsComponent: Trying to link, but link target has no PhysicsComponent. Cancelling link!");
				linked = false;
				ownerPhys	= NULL;
				ownerCore	= NULL;
				targetCore	= NULL;
				return;
			}

			object->setCreator(link->getOwner());

			Vector3 pos = targetPhys->position;
			subsystem->getGameHandler()->onSkillEffect(0, false, true, false, 0, pos.x, pos.y, pos.z, false);

			//Placing of the link and it's collision box happens every update
		}
	}
	else if(link->getState() == false && link->getLink() == object)
	{
		bool temp = linked;
		linked = false;
		subsystem->getGameHandler()->destroyObject(object);
		ownerPhys	= NULL;
		targetPhys	= NULL;
		ownerCore	= NULL;
		targetCore	= NULL;
	}
}

void PhysicsComponent::applyOffset()
{
	if (isStatic())
		return;
	position += offset;
}

bool PhysicsComponent::collision(PhysicsComponent * comp, IntersectResult * res) 
{
	IntersectResult rsvs, rsvb, rbvs, rbvb;
	bool svs, svb = false, bvs = false, bvb = false;
	svs = Intersect::intersect(sphere, comp->sphere, &rsvs);
	if (comp->useBox())
	{
		svb = Intersect::intersect(sphere, comp->box, &rsvb);
		rsvb.dist = -rsvb.dist;
	}
	if (useBox())
	{ 
		bvs = Intersect::intersect(comp->sphere, box, &rbvs);
	}
	if (useBox() && comp->useBox())
		bvb = Intersect::intersect(box, comp->box, &rbvb);
	if (!svs && !svb && !bvs && !bvb)
		return false;

	bool have = svs;
	res->squared = true;
	res->dist = rsvs.getDist();
	res->normal = rsvs.normal;
	if (svb && (rsvb.getDist() > res->dist || !have))
	{
		res->dist = rsvb.getDist();
		res->normal = rsvb.normal;
		have = true;
	}
	if (bvs && (rbvs.getDist() > res->dist || !have))
	{
		res->dist = rbvs.getDist();
		res->normal = rbvs.normal;
		have = true;
	}
	if (bvb && (rbvb.getDist() > res->dist || !have))
	{
		res->dist = rbvb.getDist();
		res->normal = rbvb.normal;

	}
	return true;
}

void PhysicsComponent::handleCollision(PhysicsComponent * comp, IntersectResult * res)
{
	if (isStatic())
		return;
	float str = res->getDist() * 0.1f;
	Vector3 change = res->normal * str * 1;
	velocity += change;
}

void PhysicsComponent::handleRepulsion(PhysicsComponent * comp, IntersectResult * res)
{
	if (isStatic() || comp->repulsion == 0)
		return;

	float str = res->getDist() * comp->repulsion * 0.1f;

	if (str > comp->repulsion)
		str = comp->repulsion;
	else if (str < -comp->repulsion)
		str = -comp->repulsion;

	Vector3 change = res->normal * str;
	velocity += change;
}

void PhysicsComponent::update()
{
	if (!isStatic())
	{
		// TODO: When you change movement you add it's velocity to velocity
		speed += 0.5f * currentSlowdown; // 0.5 = acceleration
		if (speed < 0)
			speed = 0;
		if (speed > maxSpeed)
			speed = maxSpeed;

		if (useDest)
		{
			Vector3 dir = destination - position;
			dir.normalize(); // change later
			velocity += dir;
		}

		Vector3 oldVel = velocity;
		velocity = velocity * currentSlowdown;
		oldVel = velocity - oldVel;

		position = position + movement * speed * currentSlowdown + velocity;

		if(linked)
			updateLinkBox();

		velocity += gravity;
	}

	if (useProjectileRotation)
	{
		Matrix tempMatrix;
		Vector3 projectileDir = velocity + movement * speed;
		tempMatrix.lookAt(Vector3(0, 0, 0), projectileDir, Vector3(0, 1, 0));

		rotation.identity();
		rotation.rotationMatrix(tempMatrix);
		rotation.invert();
		rotation.rotationAxisApply(Vector3(0, 1, 0), (float)D3DX_PI * 0.5f);
	}

	updateMatrixAndVolumes();

	//If we are linked, check if both the linkers are still alive. If not, remove the link
	//We are only linked if we are the link object ourselves. Removing the link means destroying ourselves
	if(linked)
	{
		if(ownerCore != NULL && targetCore != NULL)
		{
			if(!ownerCore->isPlayerAlive() || !targetCore->isPlayerAlive())
			{
				ownerCore->unlink(object);
				//targetCore->unlink(object);	//targetCore is now NULL! Don't call this!
				subsystem->getGameHandler()->destroyObject(object);
			}
		}
	}
}

void PhysicsComponent::updateMatrixAndVolumes()
{
	Vector3 scale = getScale();
	Quaternion rotation = getRotation();
	Vector3 position = getPosition();
	PhysicsComponent * pphys = (PhysicsComponent *)parent;

	matrix.identity();

	matrix.scale(scale);
	matrix.rotateQuaternion(rotation);
	Vector3 transformed = boxOffset;
	transformed.transform(matrix);

	box.setPosition(position + transformed);
	box.setRotation(boxRotOffset * rotation);

	transformed = sphereOffset;
	transformed.transform(matrix);
	sphere.origin = position + transformed;

	if(useBox() && object)
	{
		RenderComponent * rend = object->findComponent<RenderComponent>();
		if (rend)
			rend->setDebugMatrix(box.createMatrix());
	}

	matrix.translate(position);
}

bool PhysicsComponent::updateYPosition(Terrain* heightmap)
{
	if (isStatic())
		return false;
	if (useGravity == false)
		return false;

	if (heightmap != 0)
	{
		Vector3 pos = getPosition();
		if(heightmap->inside(pos.x, pos.z))
		{
			//The player is on the height map and finds the y position
			float temp = heightmap->sample(pos.x, pos.z);
			bool rtn = false;
			if (pos.y <= temp)
			{
				rtn = true;
				if (velocity.y < 0)
					velocity.y = 0.0f;
			}
			temp += offset.y;
			if (pos.y <= temp)
			{
				if (temp - pos.y < hoverPower)
				{
					position.y = pos.y + temp - position.y;
				}
				else
					velocity.y += hoverPower; // TODO: Change velocity instead?

				return rtn;
			}

			return false;
		}
	}

	if(position.y <= offset.y)
	{ 
		position.y = offset.y;
		velocity.y = 0;
		return true;
	}
	//Outside the height map! Either below it, or outside the xz-bounds.
	return false;
}

void PhysicsComponent::bindToComponents()
{
	RenderComponent* temp = object->findComponent<RenderComponent>();

	if(temp)
		temp->model.setSharedMatrix(&matrix);
}

void PhysicsComponent::setPosition(Vector3 pos)
{
	position = pos;
}

void PhysicsComponent::setRotation(Quaternion rot)
{
	rotation = rot;
}

void PhysicsComponent::setScale(Vector3 s)
{
	Vector3 relative(s.x / scale.x, s.y / scale.y, s.z / scale.z);
	float power = (relative.x + relative.y + relative.z) / 3;
	sphere.radius *= power * 0.5f;
	box.setDimensions(box.getDimensions() * relative);
	scale = s;
}

Vector3 PhysicsComponent::getPosition()
{
	if (parent)
	{
		PhysicsComponent * phys = (PhysicsComponent *) parent;
		Vector3 rtn = position;
		rtn.transform(phys->matrix);
		return rtn;
	}
	return position;
}

Quaternion PhysicsComponent::getRotation()
{
	if (parent)
	{
		PhysicsComponent * phys = (PhysicsComponent *) parent;
		return rotation * phys->getRotation();
	}
	return rotation;
}

Vector3 PhysicsComponent::getScale()
{
	if (parent)
	{
		PhysicsComponent * phys = (PhysicsComponent *) parent;
		Vector3 s = phys->getScale();
		return Vector3(scale.x * s.x, scale.y * s.y, scale.z * s.z);
	}
	return scale;
}

void PhysicsComponent::translateWith(Vector3 translation)
{
	position += translation;
}

void PhysicsComponent::rotateWith(Vector3 axis, float angle)
{
	rotation.rotationAxisApply(axis, angle);
}

void PhysicsComponent::scaleWith(Vector3 rate)
{
	float power = (rate.x + rate.y + rate.z) / 3;
	sphere.radius *= power;
	box.setDimensions(box.getDimensions() * rate);
	scale.x *= rate.x;
	scale.y *= rate.y;
	scale.z *= rate.z;
}

bool PhysicsComponent::load(std::istream &f, int label)
{
	switch (label)
	{
	case LABEL_POSITION:
		f.read((char *)&position, sizeof(Vector3));
		break;
	case LABEL_SCALE:
		f.read((char *)&scale, sizeof(Vector3));
		break;
	case LABEL_ROTATION:
		f.read((char*)&rotation, sizeof(Quaternion));
		break;
	default:
		break;
	}
	return true;
}

bool PhysicsComponent::save(std::ostream &f)
{
	LabelEnum label;
	label = LABEL_POSITION;
	f.write((char*)&label, sizeof(LabelEnum));
	f.write((char*)&position, sizeof(Vector3));

	label = LABEL_SCALE;
	f.write((char*)&label, sizeof(LabelEnum));
	f.write((char*)&scale, sizeof(Vector3));

	label = LABEL_ROTATION;
	f.write((char*)&label, sizeof(LabelEnum));
	f.write((char*)&rotation, sizeof(Quaternion));

	return true;
}

Vector3 PhysicsComponent::getActualVelocity()
{
	return velocity + movement * speed;
}

bool PhysicsComponent::useBox()
{
	return (box.getDimensions().x != 0 && box.getDimensions().y != 0 && box.getDimensions().z != 0);
}


void PhysicsComponent::enableSlowdown()
{
	currentSlowdown = realSlowdown;
	useRealSlowdown = true;
}

void PhysicsComponent::disableSlowdown()
{
	if(useRealSlowdown)
	{
		realSlowdown	= currentSlowdown;
		useRealSlowdown = false;
	}
	currentSlowdown	= 1.0f;
}

bool PhysicsComponent::isLinked()
{
	return linked;
}

bool PhysicsComponent::isStatic()
{
	return unmovable;
}

void PhysicsComponent::setStatic(bool stat)
{
	unmovable = stat;
}

void PhysicsComponent::disableArenaCollision()
{
	arenaCollisions = false;
}

bool PhysicsComponent::collideWithArena() const
{
	return arenaCollisions;
}

void PhysicsComponent::setParentComponent(Component * dad)
{
	Component::setParentComponent(dad);
	if (!parent)
		return;

	PhysicsComponent * phys = (PhysicsComponent *) parent;
	updateMatrixAndVolumes();
}

void PhysicsComponent::reset()
{
	realSlowdown	= 0.8f;
	currentSlowdown	= 0.8f;
	velocity	= Vector3(0.0f, 0.0f, 0.0f);
	movement	= Vector3(0.0f, 0.0f, 0.0f);

	position	= Vector3(0.0f, 0.0f, 0.0f);
	rotation	= Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
}

void PhysicsComponent::respawn(Vector3 pos)
{
	reset();

	position	= pos;
}

void PhysicsComponent::updateLinkBox()
{
	//Set the position of the link to the link owner's position
	position = ownerPhys->position;

	//Get the direction from owner to target
	Vector3 direction = targetPhys->position - ownerPhys->position;
	float length = direction.length();

	//Make the collision box as long as the distance between the players, and 1 unit broad.
	//The dimensions are given from the center and out, so give the real dimensions / 2.
	Vector3 scale = getScale();
	Vector3 dimension = Vector3(length * 0.5f, 1.0f, 1.0f);
	setScale(dimension);


	//Rotate the link to lie on the line between the two players
	//The collision box will rotate with the object
	Matrix m;
	m.lookAt(Vector3(0, 0, 0), direction, Vector3(0, 1, 0));

	rotation.identity();
	rotation.rotationMatrix(m);
	rotation.invert();
	rotation.rotationAxisApply(Vector3(0, 1, 0), (float)D3DX_PI * 0.5f); // UGLY HACK? (Of some reason the rotation is wrong by pi/2)
	// */
}

PhysicsComponent * PhysicsComponent::getTarget()
{
	return targetPhys;
}