
#ifndef NETWORKSUBSYSTEM_H
#define NETWORKSUBSYSTEM_H

#include "Subsystem.h"
class NetworkComponent;

#include "RawPacket.h"
#include "CommonPackets.h"

#include "PlayerInfo.h"
class Object;

/*
 * The specific tasks are described in the corresponding component header file.
 */

static const unsigned int	NR_TEAMS	= 3;

class NetworkSubsystem : public Subsystem
{
	typedef std::map< unsigned int, NetworkComponent * > Components;

public:
	NetworkSubsystem( GameHandler* gameHandler = NULL, int size = 1000);
	virtual ~NetworkSubsystem();

	Component*	createComponent(EventLua::Object script);
	Component*	getComponent(unsigned int id);
	NetworkComponent* getClientComponent(unsigned int id);
	NetworkComponent* getDummyComponent(unsigned int id);
	unsigned int getID();
	virtual std::string getName( unsigned int netId );
	virtual void		update();
	virtual void		clear();

	virtual void onPacket(RawPacket & packet) { }
	virtual void onConnect(unsigned int uid) { }
	virtual void onDisconnect(unsigned int uid) { }
	virtual void onDisconnected() { } // when you got force disconnected

	virtual void onCreateObject(Object * object) { }
	virtual void onDestroyObject(Object * object) { }
	virtual void onLink(std::string linkName, unsigned int owner, unsigned int target, bool status) {}

	virtual void queryClientList(std::vector < Object* > & list) { }
	virtual void queryClientList(std::vector < Object* > & list, const int team) { }
	virtual void queryTextBuffer(std::vector < std::string > & list) { };
	virtual void queryPlayers(std::vector < PlayerInfo > & list) { };
	virtual Object * queryControlledObject() const { return NULL; }

	virtual NetworkComponent * processCreatePacket(CreatePacket & packet);

	virtual void updateComponent(NetworkComponent * com) { }

	virtual bool isServer() = 0;

protected:
	Components components;

	unsigned int client_id;

private:
	NetworkComponent * processDefaultCreatePacket(CreatePacket & packet);
	NetworkComponent * processAICreatePacket(CreatePacket & packet);
};

#endif