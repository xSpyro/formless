

#ifndef ANIMATIONSUBSYSTEM_H
#define ANIMATIONSUBSYSTEM_H

#include "Subsystem.h"

class Object;

#include "AnimationComponent.h"

#include "ShapeAnimation.h"

class SharedFormatStore;

class AnimationSubsystem : public Subsystem
{
	typedef std::vector<AnimationComponent> Components;

	// keep a list of all loaded animations aswell
	typedef std::map < std::string, ShapeAnimation* > Animations;

public:
	AnimationSubsystem( GameHandler* gameHandler = NULL, int size = 1000, SharedFormatStore* s = NULL);
	virtual ~AnimationSubsystem();

	Component*	createComponent(EventLua::Object script);
	Component*	getComponent();
	void		update();
	void		clear();

	void		render(); // special case for animations
	// we batch render them with this call


	ShapeAnimation * getAnimation(const std::string & name);

	bool insertAnimation(const std::string & name, ShapeAnimation * animation);

	std::string	getDebugString() const;

private:

	void generateRandomFrame(ShapeAnimation * animation, int frame);
	void generateCircleFrame(ShapeAnimation * animation, int frame);

private:
	Components components;

	Animations animations;

	SharedFormatStore* store;

};

#endif
