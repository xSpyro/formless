
#include "NetworkComponent.h"
#include "NetworkSubsystemClient.h"
#include "NetworkSubsystemServer.h"

#include "Object.h"

#include "CoreComponent.h"
#include "ControllerComponent.h"
#include "PhysicsComponent.h"

#include "Events.h"
#include "PrepareKillEvent.h"
#include "KillPlayerEvent.h"

NetworkComponent::NetworkComponent( Subsystem* subsystem, unsigned int id )
	: Component( subsystem ), id(id)
{
	submit		= false;
}

NetworkComponent::~NetworkComponent()
{
}

void NetworkComponent::onPacket(RawPacket & packet)
{
}

void NetworkComponent::onDisconnect(unsigned int uid)
{
}

void NetworkComponent::bindObject(Object* obj)
{
	Component::bindObject(obj);
}

void NetworkComponent::unbindObject()
{
	Component::unbindObject();
}

void NetworkComponent::receive(PrepareKillEvent* kill)
{
	if( !killPlayer(kill->getKiller()))
	{
		//This component's subsystem was client, not server
	}
}

void NetworkComponent::receive(PrepareRespawnEvent * res)
{
	if( !respawnPlayer() )
	{
		//This component's subsystem was client, not server
	}
}

void NetworkComponent::receive(LinkEvent * link)
{
}

void NetworkComponent::bindToComponents()
{
}

unsigned int NetworkComponent::getID() const
{
	return id;
}

std::string NetworkComponent::getName( unsigned int netId ) const
{
	//NetworkSubsystemClient* netclient = (NetworkSubsystemClient*)parent;
	return ((NetworkSubsystem*)subsystem)->getName( netId );
}

void NetworkComponent::setReady(bool state)
{
	submit = state;
}

bool NetworkComponent::isReady() const
{
	return submit;
}

bool NetworkComponent::isOnServer() const
{
	NetworkSubsystem * sub = (NetworkSubsystem *)subsystem;
	return sub->isServer();
}

bool NetworkComponent::killPlayer(Object * killer)
{
	NetworkSubsystem* net = (NetworkSubsystem*)subsystem;
	if( net->isServer() )
	{
		NetworkSubsystemServer* netserver = (NetworkSubsystemServer*)subsystem;
		CoreComponent* core = object->findComponent<CoreComponent>();

		unsigned int killId = 0;
		if (killer)
		{
			NetworkComponent * net = killer->findComponent<NetworkComponent>();
			if (net)
				killId = net->getID();
		}
		netserver->broadcastPlayerDeath(id, core->team, killId);

		object->sendInternal(&KillPlayerEvent(killer, object));
		
		if (killer)
			killer->sendInternal(&KillPlayerEvent(killer, object));

		return true;
	}
	else
		return false;
}

bool NetworkComponent::respawnPlayer()
{
	NetworkSubsystem* net = (NetworkSubsystem*)subsystem;
	if( net->isServer() )
	{
		NetworkSubsystemServer* netserver = (NetworkSubsystemServer*)subsystem;
		netserver->broadcastPlayerRespawn(id);

		object->sendInternal(&RespawnPlayerEvent());

		return true;
	}
	else
		return false;
}