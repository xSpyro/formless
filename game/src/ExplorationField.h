
#ifndef EXPLORATION_FIELD_H
#define EXPLORATION_FIELD_H

#include "PotentialField.h"
class AIComponent;

class ExplorationField : public PotentialField
{

public:
	ExplorationField(AIComponent* component = NULL);
	virtual ~ExplorationField();

	float	calcPotential(Vector2 position);

private:
	AIComponent* component;
};

#endif