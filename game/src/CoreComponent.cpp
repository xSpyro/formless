
#include "CoreComponent.h"
#include "CoreSubsystem.h"
#include "Object.h"
#include "ObjectTemplate.h"
#include "GameHandler.h"
#include "LogicHandler.h"
#include "Events.h"
#include "Module.h"
#include "ModuleFactory.h"
#include "SkillComponent.h"
#include "PhysicsComponent.h"
#include "EventHandler.h"
#include "AnimationComponent.h"
#include "ScriptEnvironment.h"

#include "CommonPackets.h"

CoreComponent::CoreComponent( Subsystem* subsystem )
	: Component( subsystem ), damageRecieved(0, 0, 1, true, true), life(0, 0, 1, false), link(0)
{
	hitTicker	= 0;
	nrSlots		= 0;
	nrSkills	= 0;
	name		= "";
	idleAnimation = "";

	team		= 0;
	id			= 0;

	playerAlive	= true;

	modules.reserve(100);
}

CoreComponent::~CoreComponent()
{
}

void CoreComponent::setSlots( int total, int skills )
{
	for (int n = total; n < nrSlots; ++n)
	{
		removeModule(n);
	}

	nrSlots		= total;
	nrSkills	= skills;

	modules.resize( nrSlots );
	waiting.resize( nrSlots );
}

int CoreComponent::getTotalSlots()
{
	return nrSlots;
}

int CoreComponent::getSkillSlots()
{
	return nrSkills;
}

void CoreComponent::addModule( int slot, unsigned int id, std::string name )
{
	if( slot < nrSlots && slot >= 0 )
	{
		if( modules[slot] )
			removeModule( slot );

		modules[slot] = subsystem->getGameHandler()->getModule( id, name );
		
		if(modules[slot])
		{
			// Add module attributes to Core
			life.add( modules[slot]->life );
			mana.add( modules[slot]->mana );
			lifeRegen.add(modules[slot]->lifeRegen);
			manaRegen.add(modules[slot]->manaRegen);
			speed.add(modules[slot]->speed);
			damageDealt.add(modules[slot]->damageDealt);
			damageRecieved.add(modules[slot]->damageRecieved);
		}
	}
}

std::string CoreComponent::removeModule( int slot )
{
	std::string rtn("");

	if( slot < nrSlots && slot >= 0 )
	{
		if (modules[slot])
		{
			rtn = modules[slot]->name;

			// Clean up attributes added to Core by module
			life.remove( modules[slot]->life );
			mana.remove( modules[slot]->mana );
			lifeRegen.remove(modules[slot]->lifeRegen);
			manaRegen.remove(modules[slot]->manaRegen);
			speed.remove(modules[slot]->speed);
			damageDealt.remove(modules[slot]->damageDealt);
			damageRecieved.remove(modules[slot]->damageRecieved);

			modules[slot] = NULL;
		}
	}

	return rtn;
}

int CoreComponent::removeModule( const std::string module )
{
	int rtn = -1;

	for(unsigned int i = 0; i < modules.size(); ++i)
	{
		if( modules[i] && modules[i]->name == module )
		{
			rtn = i;

			removeModule(rtn);

			break;
		}
	}

	return rtn;
}

Module* CoreComponent::getModule( int slot )
{
	if( slot < nrSlots && slot >= 0 )
	{
		return modules[slot];
	}

	return NULL;
}

Object* CoreComponent::activateModule( int slot, bool fromServer)
{
	if( slot < nrSkills && slot >= 0 )
	{
		if( object->getLogic() && modules[slot])
		{
			if (pay(modules[slot]))
			{
				EventLua::Object cast = subsystem->getGameHandler()->getScript()->get( "cast" );
				cast( modules[slot]->manaCost, modules[slot]->lifeCost );
				Object * rtn = object->getLogic()->createObject( modules[slot]->skill, object );

				object->send(&SkillEvent(rtn, modules[slot]->skill));
				return rtn;
			}
			else
			{
				if (fromServer)
				{
					Object * rtn = object->getLogic()->createObject( modules[slot]->skill, object );

					object->send(&SkillEvent(rtn, modules[slot]->skill));

					if (rtn)
						return rtn;
				}

				subsystem->getGameHandler()->onSkillEffect(0, false, false, true, 0, phys->position.x, phys->position.y, phys->position.z, false);

				return NULL;
			}
		}
	}

	return NULL;
}

void CoreComponent::getModuleSlots( Modules& mods )
{
	for(unsigned int i = 0; i < modules.size(); ++i)
	{
		mods.push_back(modules[i]);
	}
}

void CoreComponent::bindToComponents()
{
	phys = object->findComponent<PhysicsComponent>();
	offset = 0.0f;
	dir = 1.0f;
	tickHover = 0;
	if (phys)
		offset = phys->offset.y;

	life.setRegen(&lifeRegen);
	mana.setRegen(&manaRegen);
}

void CoreComponent::setCoreAttributes()
{
	std::string anim = "null";

	if(name == "")
	{
		return;
	}

	CoreSubsystem * cs = reinterpret_cast < CoreSubsystem* > (getSubsystem());
	if (cs)
	{
		EventLua::Object obj = cs->getGameHandler()->getScript()->get("cores");

		std::string name_fixed = name;

		unsigned int posOfSpace = name.find(" ");
		if (posOfSpace != std::string::npos)
		{
			name_fixed = name.replace(posOfSpace, 1, "_");
		}

		EventLua::Object table = obj.get(name_fixed.c_str());

		anim = table["idleAnimation"].queryString();


		//std::cout << "PLAYER ANIMATION:"  << anim << std::endl;
	}

	
	AnimationComponent * ac = object->findComponent< AnimationComponent > ( );
	if(ac != NULL)
	{
		ac->resetShape();
		ac->startShape(anim);
	}

}

void CoreComponent::receive( ActivateSkillEvent * skill )
{
	Object * result = NULL;

	//if (skill->waitForConfirm())
	//	setWaitAt(skill->getSkill());
	//else

	if (link && modules[skill->getSkill()]->skill + "Link" == link->getTemplate()->name)
	{
		unlink(link);
		skill->setSuccsess(true);
		return;
	}

	result = activateModule( skill->getSkill(), skill->isFromServer());

	if( result )
	{
		SkillComponent * sc = result->findComponent<SkillComponent>();

		if (sc && sc->getCasterAnimation() != "")
		{
			this->getObject()->sendInternal(&AnimationEvent(sc->getCasterAnimation()));
		}

		skill->setResult(result);
	}
}

void CoreComponent::receive( ConfirmSkill * skill)
{
	if (getWaitAt(skill->getSkill()))
	{
		activateModule( skill->getSkill() );
		setWaitAt(skill->getSkill(), false);
	}
}

void CoreComponent::receive( KillPlayerEvent* kill )
{
	Component::receive(kill);
	if (kill->getKilled() == object)
	{
		playerAlive = false;
		drain();

		object->killChildren();
	}
}

void CoreComponent::receive( RespawnPlayerEvent* res )
{
	Component::receive(res);
	playerAlive = true;
	revive();
}

void CoreComponent::receive(QueryEvent<Vector3> * qv)
{
	switch (qv->getType())
	{
	case CORE_HEALTH:
		qv->setData(Vector3(life.getCurrent(), life.getMax(), 0 /* TODO: regen?*/ ));
		break;
	case CORE_MANA:
		qv->setData(Vector3(mana.getCurrent(), mana.getMax(), 0 /* TODO: regen?*/ ));
		break;
	default:
		return;
		break;
	}
}

void CoreComponent::receive(DestroyEvent * de)
{
	Object * sender = de->getSender();
	if (sender == link)
	{
		unlink(link);
	}

	linked.erase(sender); // In case it is linked to you 

	if (sender == object && link)
	{
		unlink(link);
	}
}

void CoreComponent::receive( LinkEvent * le )
{
	if (le->getState())
	{
		if (le->getOwner() == object)
		{
			if (link) // unlink the old link
			{
				unlink(link);
			}

			link = le->getLink();
			subsystem->getGameHandler()->getEventHandler()->subscribe(object, EVENT_DESTROY, link);
		}
		if (le->getTarget() == object)
		{
			linked[le->getLink()] = le->getOwner();
			subsystem->getGameHandler()->getEventHandler()->subscribe(object, EVENT_DESTROY, le->getLink());
		}
	}
	else
	{
		if (le->getOwner() == object)
		{
			if(link == NULL)
				subsystem->getGameHandler()->getEventHandler()->unsubscribe(object, EVENT_DESTROY, le->getLink());
			else
				subsystem->getGameHandler()->getEventHandler()->unsubscribe(object, EVENT_DESTROY, link);
			link = 0;
		}
		if (le->getTarget() == object)
		{
			linked.erase(le->getLink());
			subsystem->getGameHandler()->getEventHandler()->unsubscribe(object, EVENT_DESTROY, le->getLink());
		}
	}
}

void CoreComponent::receive( EffectEvent* ev )
{
	SkillEffect * e = ev->getEffect();

	if (ev->isEnd())
		return;

	if (e->damage > 0)
	{
		damageRecieved.setBase(e->damage);
		e->damage = damageRecieved.getMax();
		if (e->damage < 1)
			e->damage = 1;

		std::stringstream damageString;
		damageString << e->damage;
		subsystem->getGameHandler()->onSkillEffect(e->damage, false, false, false, 0, phys->position.x, phys->position.y, phys->position.z, false);
	}

	if (life.getCurrent() > 0)
	{
		if (!life.damage(e->damage))
		{
			if(object)
			{
				subsystem->getLogger()->notice("A core component died and is killing its object.");
				Object * killer = 0;
				if (ev->getFrom())
				{
					CoreComponent * core = ev->getFrom()->findFamilyComponent<CoreComponent>();
					if (core)
						killer = core->getObject();
				}
				object->kill(killer);
			}
			else
			{
				subsystem->getLogger()->alert("A core component died without having an object.");
			}
		}
		if (life.getCurrent() > life.getMax())
			life.setCurrent(life.getMax());

		if (e->damage > 0)
			hitTicker = 1; // To halt the mana regen

		mana.damage(e->particleDamage);
		if (mana.getCurrent() > mana.getMax())
			mana.setCurrent(mana.getMax());

		EventLua::Object damage = subsystem->getGameHandler()->getScript()->get( "damage" );
		damage( e->damage, e->particleDamage );

		if (e->knockback > 100) // TODO: replace 100 with something else or perhaps put this in lua?
		{
			// TODO: Break links
		}
	}
}

void CoreComponent::revive()
{
	life.revive();
	mana.revive();
	speed.revive();
}

void CoreComponent::drain()
{
	life.drain();
	mana.drain();
}

void CoreComponent::update()
{
	if (phys)
	{
		const float halftime = 60.0f;
		const float halfrange = 0.25f;
		/*if (newOff > offset * 2)
			dir = -1.0f;
		else if (newOff < offset)
			dir = 1.0f;*/
		if (tickHover > halftime + halftime)
			tickHover = 0;

		phys->offset.y = offset * (1 + halfrange + halfrange * (float)sin(tickHover/halftime * D3DX_PI));
		tickHover++;
	}
	

	if (hitTicker > 0)
	{
		const int time = 60 * 5; // 7 seconds
		if (hitTicker >= time) 
		{
			lifeRegen.revive();
			hitTicker = 0;
		}
		else
		{
			float mod = ((float)hitTicker)/((float)time);
			mod = (2.0f/300.0f)*(pow(100.0f, mod) - 1.0f/100.0f) + (1.0f/3.0f); // mod becomes a value between 1/3 to 1 according to the calculation: (1/100*2/3)*100^(x)+1/3
			lifeRegen.setCurrent(mod * lifeRegen.getMax());
			hitTicker++;
		}
	}

	mana.update();
	if (mana.getCurrent() == 0)
	{
		float min = std::min(1.0f, mana.getMax());
		mana.setCurrent(min);
	}

	life.update();
}

void CoreComponent::setWaitAt(int pos, bool val)
{
	if (pos >= 0 && pos < nrSkills)
	{
		waiting[pos] = val;
	}
}

bool CoreComponent::getWaitAt(int pos)
{
	bool rtn = false;
	if (pos >= 0 && pos < nrSkills)
	{
		rtn = waiting[pos];
	}
	return rtn;
}

CoreAttrPacket CoreComponent::getAttrPacket()
{
	CoreAttrPacket packet;

	packet.nrSlots	= nrSlots;
	packet.nrSkills	= nrSkills;
	packet.life		= life.getBase();
	packet.mana		= mana.getBase();
	packet.lifeRegen = lifeRegen.getBase();
	packet.manaRegen = manaRegen.getBase();
	packet.speed	= speed.getBase();
	strcpy_s(packet.name, 32, name.c_str());

	return packet;
}

ChangeModulePacket CoreComponent::getModulePacket(const unsigned int & slot)
{
	ChangeModulePacket packet;

	packet.slot = slot;
	packet.moduleid = modules[slot]->id;
	strcpy_s(packet.module, 32, modules[slot]->name.c_str());
	packet.add = true;

	return packet;
}

bool CoreComponent::isPlayerAlive()
{
	return playerAlive;
}

Object * CoreComponent::getLink()
{
	return link;
}

std::map<Object *, Object *> CoreComponent::getLinks()
{
	return linked;
}

void CoreComponent::unlink(Object * l,  Object * target)
{
	PhysicsComponent * temp = link->findComponent<PhysicsComponent>();
	if (temp)
		temp = temp->getTarget();
	if (temp)
		target = temp->getObject();

	object->sendInternal(&LinkEvent(l, object, target, false));
	if (target)
		target->sendInternal(&LinkEvent(l, object, target, false));
	l->sendInternal(&LinkEvent(l, object, target, false));

	link = 0;
}

bool CoreComponent::pay(Module * mod)
{
	if (mana.pay(mod->manaCost))
	{
		if (life.pay(mod->lifeCost))
			return true;
		mana.damage(-mod->manaCost); // Revert mana cost
	}
	return false;
}
