#ifndef SCRIPT_ENVIRONMENT_H
#define SCRIPT_ENVIRONMENT_H
#include "Environment.h"
#include <stack>

class ScriptEnvironment
{
public:
	ScriptEnvironment();
	ScriptEnvironment(unsigned int maxSize);
	~ScriptEnvironment();

	Object * getCaller();
	Object * getData(int id);

	bool push(Object * caller = 0);
	void pop();

	int addData(Object * o); // returns id
	void clearData();

	int getEnvId();

	bool isEmpty();
	bool isFull();
private:
	unsigned int maxSize;

	Environment base;
	std::stack<Environment> envStack;
};


extern ScriptEnvironment * env;

#endif