#ifndef ACTIVATE_SKILL_H
#define ACTIVATE_SKILL_H
#include "Event.h"

class ActivateSkillEvent : public Event // Try to activate a skill
{
public:
	ActivateSkillEvent(int skill = 0, bool waitForConfirm = true, Object * sender = 0, Object ** result = NULL, bool fromServer = false);
	~ActivateSkillEvent();

	bool waitForConfirm();
	int getSkill();

	void setFromServer(bool server);
	bool isFromServer();

	void sendTo(Object * o);

	void setResult(Object * obj);
	Object * getResult();

	void setSuccsess(bool succsess);
	bool isSuccsess();
private:
	bool wait;

	bool fromServer;
	bool succsess;

	int skill;
	Object ** result;
};

#endif