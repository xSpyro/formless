#ifndef CSV_MANAGER__H
#define CSV_MANAGER__H

#include "DatabaseManager.h"

class CSVManager : public DatabaseManager
{
	typedef std::vector< DatabaseEntry > Entries;

public:
	CSVManager();
	virtual ~CSVManager();

	bool						openDatabase( std::string name );
	void						closeDatabase();

	unsigned int				getNrEntries();
	DatabaseEntry*				nextEntry();											// Will loop around to first entry after the last
	DatabaseEntry*				getEntry( unsigned int entry );							// Returns NULL if [entry] does not exist
	DatabaseEntry*				findEntry( unsigned int field, std::string search );	// Returns NULL if no entry with [field] contains [search]
	std::string					getFieldLabel( unsigned int field );					// Returns an empty string if [field] does not exist
	bool						setFieldLabel( unsigned int field, std::string label ); // Returns false if [field] does not exist
	unsigned int				addFieldLabel( std::string label );						// Returns the field number associated with the new label
	std::string					getField( unsigned int entry, unsigned int field );		// Returns an empty string if either [entry] or [field] does not exist
	bool						saveEntry( DatabaseEntry* entry );						// Returns false if no database is currently open or if [entry] is NULL
	void						deleteEntry( unsigned int entry );

	void						clearEntries();											// CAUTION: This function will wipe the current database!

private:
	std::string					baseDir;
	std::string					currentDb;
	Entries::iterator			currentEntry;
	Entries						entries;
	DatabaseEntry				labels;
};

#endif
