
#include "Logger.h"

Logger::Logger(const std::string & name, int l)
	: className(name)
{
#ifndef LOG_DISABLE

	// open the file for writing
	// but first format the name

	state = LOG_ON;
	level = l;
	std::string path = "logs/" + name + ".txt";
	stream = new std::ofstream(path.c_str());
	shared = false;


	time_t curtime = time(0);
	tm now;
	localtime_s(&now, &curtime);
	char dest[BUFSIZ]={0};
	const char format[]="[Logger Created] %a, %B %d %Y - %X";
	strftime(dest, sizeof(dest)-1, format, &now);
	
	
	(*stream) << dest << std::endl;

#endif
}

Logger::Logger(std::ostream & s, int l)
{
#ifndef LOG_DISABLE

	state = LOG_ON;
	level = l;
	stream = &s;
	shared = true;

#endif
}

Logger::~Logger()
{
	close();
}

void Logger::close()
{
#ifndef LOG_DISABLE

	if (!shared && stream)
	{
		(*stream) << "[Logger Closed]" << std::endl;
		delete stream;
		stream = NULL;
	}

#endif
}

void Logger::off()
{
	state = LOG_OFF;
}

void Logger::on()
{
	state = LOG_ON;
}

void Logger::overrideLevel(int l)
{
	level = l;
}

std::ostream & Logger::getOutputStream()
{
	return *stream;
}
