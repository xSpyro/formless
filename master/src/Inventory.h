#ifndef INVENTORY__H
#define INVENTORY__H

#include "Common.h"
#include "ItemList.h"

class Inventory
{
	typedef std::vector < ItemInfo > Items;

public:
	Inventory();
	~Inventory();

	void registerItem( const ItemInfo & item );
	void clear();

	bool find( const std::string & name ) const;
	bool findCore( const std::string & name ) const;
	bool findModule( const std::string & name ) const;

	unsigned int size() const;

	ItemList* getFullList();

public:
	Items cores;
	Items modules;

private:
	ItemList allItems;
};

#endif
