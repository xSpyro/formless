
/* Logger
 * contact: admin@devmoon.se
 * 
 * should be combatible with all unix operation systems
 *
 * example: (cpp)
 *
 * Logger log("MyLogName"); // second parameter is lowest log level to show LOG_NOTSET (default)
 * log("Hello Logger!");
 * log.alert("Alert Logger!");
 *
 *
 *
 * Logger log(std::cout);
 * log("Hello std::cout");
**/

#ifndef LOGGER
#define LOGGER 

#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <ctime>
#include <clocale>

#define LOG_SEPARATOR ""
#define LOG_TIME_WIDTH 10
#define LOG_FLAG_WIDTH 7
#define LOG_NAME_WIDTH 12

// common strings
#define LOG_OK "Success"
#define LOG_FAILED "Error"

#ifndef LOG_DISABLE
#define LOG_FUNC_TEMPLATE1(name, l) \
	template < class A1 > void name(const A1 & a1) \
	{ \
		if (state && level <= l) \
		{ \
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << #l; \
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix(); \
			(*stream) << std::setw(0) << a1 << std::endl; \
			\
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << #l; \
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix(); \
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | "; \
			output() << std::setw(0) << a1 << std::endl; \
		} \
	}
#else
#define LOG_FUNC_TEMPLATE1(name, l) \
	template < class A1 > void name(const A1 & a1) { }
#endif

#ifndef LOG_DISABLE
#define LOG_FUNC_TEMPLATE2(name, l) \
	template < class A1, class A2 > void name(const A1 & a1, const A2 & a2) \
	{ \
		if (state && level <= l) \
		{ \
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << #l; \
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix(); \
			(*stream) << std::setw(0) << a1 << LOG_SEPARATOR << a2 << std::endl; \
			\
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << #l; \
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix(); \
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | "; \
			output() << std::setw(0) << a1 << LOG_SEPARATOR << a2 << std::endl; \
		} \
	}
#else
#define LOG_FUNC_TEMPLATE2(name, l) \
	template < class A1, class A2 > void name(const A1 & a1, const A2 & a2) { }
#endif

#ifndef LOG_DISABLE
#define LOG_FUNC_TEMPLATE3(name, l) \
	template < class A1, class A2, class A3 > void name(const A1 & a1, const A2 & a2, const A3 & a3) \
	{ \
		if (state && level <= l) \
		{ \
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << #l; \
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix(); \
			(*stream) << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << std::endl; \
			\
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << #l; \
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix(); \
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | "; \
			output() << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << std::endl; \
		} \
	}
#else
#define LOG_FUNC_TEMPLATE3(name, l) \
	template < class A1, class A2, class A3 > void name(const A1 & a1, const A2 & a2, const A3 & a3) { }
#endif

#ifndef LOG_DISABLE
#define LOG_FUNC_TEMPLATE4(name, l) \
	template < class A1, class A2, class A3, class A4 > void name(const A1 & a1, const A2 & a2, const A3 & a3, const A4 & a4) \
	{ \
		if (state && level <= l) \
		{ \
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << #l; \
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix(); \
			(*stream) << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << std::endl; \
			\
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << #l; \
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix(); \
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | "; \
			output() << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << std::endl; \
		} \
	}
#else
#define LOG_FUNC_TEMPLATE4(name, l) \
	template < class A1, class A2, class A3, class A4 > void name(const A1 & a1, const A2 & a2, const A3 & a3, const A4 & a4) { }
#endif

#ifndef LOG_DISABLE
#define LOG_FUNC_TEMPLATE5(name, l) \
	template < class A1, class A2, class A3, class A4, class A5 > void name(const A1 & a1, const A2 & a2, const A3 & a3, const A4 & a4, const A5 & a5) \
	{ \
		if (state && level <= l) \
		{ \
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << #l; \
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix(); \
			(*stream) << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << LOG_SEPARATOR << a5 << std::endl; \
			\
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << #l; \
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix(); \
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | "; \
			output() << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << LOG_SEPARATOR << a5 << std::endl; \
		} \
	}
#else
#define LOG_FUNC_TEMPLATE5(name, l) \
	template < class A1, class A2, class A3, class A4, class A5 > void name(const A1 & a1, const A2 & a2, const A3 & a3, const A4 & a4, const A5 & a5) { }
#endif

#ifndef LOG_DISABLE
#define LOG_FUNC_TEMPLATE6(name, l) \
	template < class A1, class A2, class A3, class A4, class A5, class A6 > void name(const A1 & a1, const A2 & a2, const A3 & a3, const A4 & a4, const A5 & a5, const A6 & a6) \
	{ \
		if (state && level <= l) \
		{ \
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << #l; \
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix(); \
			(*stream) << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << LOG_SEPARATOR << a5 << LOG_SEPARATOR << a6 << std::endl; \
			\
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << #l; \
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix(); \
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | "; \
			output() << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << LOG_SEPARATOR << a5 << LOG_SEPARATOR << a6 << std::endl; \
		} \
	}
#else
#define LOG_FUNC_TEMPLATE6(name, l) \
	template < class A1, class A2, class A3, class A4, class A5, class A6 > void name(const A1 & a1, const A2 & a2, const A3 & a3, const A4 & a4, const A5 & a5, const A6 & a6) { }
#endif

#define LOG_FUNC_TEMPLATE(name, l) \
	LOG_FUNC_TEMPLATE1(name, l) \
	LOG_FUNC_TEMPLATE2(name, l) \
	LOG_FUNC_TEMPLATE3(name, l) \
	LOG_FUNC_TEMPLATE4(name, l) \
	LOG_FUNC_TEMPLATE5(name, l) \
	LOG_FUNC_TEMPLATE6(name, l)

class Logger
{
public:

	enum
	{
		LOG_NOTSET,
		LOG_DEBUG,
		LOG_INFO,
		LOG_NOTICE,
		LOG_WARN,
		LOG_ALERT,
		LOG_FATAL
	};

private:

	enum
	{
		LOG_OFF,
		LOG_ON
	};

	
	static const int NOTSET = 0;
	static const int DEBUG = 1;
	static const int INFO = 2;
	static const int NOTICE = 3;
	static const int WARN = 4;
	static const int ALERT = 5;
	static const int FATAL = 6;

public:

	Logger(const std::string & fname, int level = LOG_NOTSET);
	Logger(std::ostream & stream, int level = LOG_NOTSET);
	~Logger();

	void close();

	void off();
	void on();

	void overrideLevel(int level);
	std::ostream & getOutputStream();

	template < class A1 >
	void operator () (const A1 & a1)
	{
#ifndef LOG_DISABLE
		if (state && level == LOG_NOTSET)
		{
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			(*stream) << std::setw(0) << a1 << std::endl;
			
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | ";
			output() << a1 << std::endl;
		}
#endif
	}

	template < class A1, class A2 >
	void operator () (const A1 & a1, const A2 & a2)
	{
#ifndef LOG_DISABLE
		if (state && level == LOG_NOTSET)
		{
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			(*stream) << std::setw(0) << a1 << LOG_SEPARATOR << a2 << std::endl;
			
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | ";
			output() << a1 << LOG_SEPARATOR << a2 << std::endl;
		}
#endif
	}

	template < class A1, class A2, class A3 >
	void operator () (const A1 & a1, const A2 & a2, const A3 & a3)
	{
#ifndef LOG_DISABLE
		if (state && level == LOG_NOTSET)
		{
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			(*stream) << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << std::endl;
			
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | ";
			output() << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << std::endl;
		}
#endif
	}

	template < class A1, class A2, class A3, class A4 >
	void operator () (const A1 & a1, const A2 & a2, const A3 & a3, const A4 & a4)
	{
#ifndef LOG_DISABLE
		if (state && level == LOG_NOTSET)
		{
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			(*stream) << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << std::endl;
			
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | ";
			output() << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << std::endl;
		}
#endif
	}

	template < class A1, class A2, class A3, class A4, class A5 >
	void operator () (const A1 & a1, const A2 & a2, const A3 & a3, const A4 & a4, const A5 & a5)
	{
#ifndef LOG_DISABLE
		if (state && level == LOG_NOTSET)
		{
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			(*stream) << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << LOG_SEPARATOR << a5 << std::endl;
			
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | ";
			output() << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << LOG_SEPARATOR << a5 << std::endl;
		}
#endif
	}

	template < class A1, class A2, class A3, class A4, class A5, class A6 >
	void operator () (const A1 & a1, const A2 & a2, const A3 & a3, const A4 & a4, const A5 & a5, const A6 & a6)
	{
#ifndef LOG_DISABLE
		if (state && level == LOG_NOTSET)
		{
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			(*stream) << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << LOG_SEPARATOR << a5 << LOG_SEPARATOR << a6 << std::endl;
			
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | ";
			output() << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << LOG_SEPARATOR << a5 << LOG_SEPARATOR << a6 << std::endl;
		}
#endif
	}

	template < class A1, class A2, class A3, class A4, class A5, class A6, class A7 >
	void operator () (const A1 & a1, const A2 & a2, const A3 & a3, const A4 & a4, const A5 & a5, const A6 & a6, const A7 & a7)
	{
#ifndef LOG_DISABLE
		if (state && level == LOG_NOTSET)
		{
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			(*stream) << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << LOG_SEPARATOR << a5 << LOG_SEPARATOR << a6 << LOG_SEPARATOR << a7 << std::endl;
			
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | ";
			output() << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << LOG_SEPARATOR << a5 << LOG_SEPARATOR << a6 << LOG_SEPARATOR << a7 << std::endl;
		}
#endif
	}

	template < class A1, class A2, class A3, class A4, class A5, class A6, class A7, class A8 >
	void operator () (const A1 & a1, const A2 & a2, const A3 & a3, const A4 & a4, const A5 & a5, const A6 & a6, const A7 & a7, const A8 & a8)
	{
#ifndef LOG_DISABLE
		if (state && level == LOG_NOTSET)
		{
			(*stream) << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			(*stream) << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			(*stream) << std::setw(0) << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << LOG_SEPARATOR << a5 << LOG_SEPARATOR << a6 << LOG_SEPARATOR << a7 << LOG_SEPARATOR << a8 << std::endl;
			
			output() << std::setw(LOG_FLAG_WIDTH) << std::left << " ";
			output() << std::setw(LOG_TIME_WIDTH) << std::right << prefix();
			output() << "@" << std::setw(LOG_NAME_WIDTH) << std::left << className << std::setw(0) << " | ";
			output() << a1 << LOG_SEPARATOR << a2 << LOG_SEPARATOR << a3 << LOG_SEPARATOR << a4 << LOG_SEPARATOR << a5 << LOG_SEPARATOR << a6 << LOG_SEPARATOR << a7 << LOG_SEPARATOR << a8 << std::endl;
		}
#endif
	}

	LOG_FUNC_TEMPLATE(debug, DEBUG)
	LOG_FUNC_TEMPLATE(info, INFO)
	LOG_FUNC_TEMPLATE(notice, NOTICE)
	LOG_FUNC_TEMPLATE(warn, WARN)
	LOG_FUNC_TEMPLATE(alert, ALERT)
	LOG_FUNC_TEMPLATE(fatal, FATAL)

	static Logger & print()
	{
		static Logger log("Global");
		return log;
	}

private:

	static std::ostream & output()
	{
		// global logger, logs everything
		static Logger global("All");
		return *(global.stream);
	}

	static std::string prefix()
	{
		static time_t t;
		static tm ptm;
		static char buffer[12];

		time(&t);
		localtime_s(&ptm, &t);

		strftime(buffer, 12, "%X | ", &ptm);
		return buffer;
	}

private:

	std::string className;
	std::ostream * stream;
	int level;
	bool state, shared;
};

#undef LOG_SEPARATOR
#undef LOG_TIME_WIDTH
#undef LOG_FLAG_WIDTH
#undef LOG_NAME_WIDTH

#undef LOG_FUNC_TEMPLATE
#undef LOG_FUNC_TEMPLATE1
#undef LOG_FUNC_TEMPLATE2
#undef LOG_FUNC_TEMPLATE3
#undef LOG_FUNC_TEMPLATE4
#undef LOG_FUNC_TEMPLATE5
#undef LOG_FUNC_TEMPLATE6

#endif