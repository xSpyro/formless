#include "Inventory.h"

Inventory::Inventory()
{
}

Inventory::~Inventory()
{
}

void Inventory::registerItem( const ItemInfo & item )
{
	allItems.registerItem( item );

	switch( item.type )
	{
	case 1:
		cores.push_back( item );
		break;
	case 2:
	case 3:
		modules.push_back( item );
		break;
	}
}

void Inventory::clear()
{
	cores.clear();
	modules.clear();
}

bool Inventory::find( const std::string & name ) const
{
	bool result = false;

	for( Items::const_iterator it = cores.begin(); it != cores.end() && !result; ++it )
	{
		if( name == std::string(it->getItemName()) )
		{
			result = true;
		}
	}

	for( Items::const_iterator it = modules.begin(); it != modules.end() && !result; ++it )
	{
		if( name == std::string(it->getItemName()) )
		{
			result = true;
		}
	}

	return result;
}

bool Inventory::findCore( const std::string & name ) const
{
	bool result = false;

	for( Items::const_iterator it = cores.begin(); it != cores.end() && !result; ++it )
	{
		if( name == std::string(it->getItemName()) )
		{
			result = true;
		}
	}

	return result;
}

bool Inventory::findModule( const std::string & name ) const
{
	bool result = false;

	for( Items::const_iterator it = modules.begin(); it != modules.end() && !result; ++it )
	{
		if( name == std::string(it->getItemName()) )
		{
			result = true;
		}
	}

	return result;
}

unsigned int Inventory::size() const
{
	return (cores.size() + modules.size());
}

ItemList* Inventory::getFullList()
{
	return &allItems;
}
