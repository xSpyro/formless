#ifndef DATABASE_ENTRY__H
#define DATABASE_ENTRY__h

#include "Common.h"

class DatabaseEntry
{
	typedef std::vector< std::string > Fields;
public:
	DatabaseEntry();
	~DatabaseEntry();

	void			addField( std::string data );
	void			addField( unsigned int data );

	bool			setField( unsigned int id, std::string data );
	bool			setField( unsigned int id, unsigned int data );

	std::string		getField( unsigned int id );
	unsigned int	getNrFields();

	void			clear();

private:
	Fields			fields;
};

#endif
