
#ifndef MASTER_H
#define MASTER_H

#include "Logger.h"

#include "ServerInterface.h"
#include "CommonPackets.h"

#include "ServerList.h"

#include "../../include/lua/EventLua.h"

class AccountManager;
class ItemManager;
class AchievementManager;

class Master : public ServerInterface
{
	typedef std::map< std::string, clock_t > PingList;

public:

	Master(boost::asio::io_service & service);
	~Master();

	bool startup();
	int run();

	void onPacket(RawPacket & packet);

private:

	void checkServers(std::clock_t tick);

	void processPingPacket(PingPacket & packet);
	void processAuthRequest(MasterAuthPacket & packet);
	void processCreateUserPacket(CreateUserPacket & packet);
	void processDropUserPacket(DropUserPacket & packet);
	void processQueryUsernamePacket(QueryUsernamePacket & packet);
	void processAddItemPacket(AddItemPacket & packet);
	void processMoveItemPacket(MoveItemPacket & packet);
	void processInventoryRequest(InventoryRequestPacket & packet);
	void processProfileLoadRequest(ProfileLoadPacket & packet);
	void processProfileListPacket(ProfileListPacket & packet);
	void processServerListRequest(QueryServerListPacket & packet);
	void processServerUpdatePacket(ServerUpdatePacket & packet);
	void processNewServer(RegisterServerPacket & packet);
	void processDropServerPacket(DropServerPacket & packet);
	void processGameStartPacket(GameStartPacket & packet);
	void processGameEndPacket(GameEndPacket & packet);
	void processAccountAgePacket(AccountPacket & packet);
	void processAchievementRequestPacket(AchievementRequestPacket & packet);
	void processAchievementListRequestPacket(AchievementListRequestPacket & packet);
	void processServerJoinPacket(ServerJoinPacket & packet);
	void processServerLeavePacket(ServerLeavePacket & packet);

private:
	
	static Master* me;
	static void loadScript( const char* filename );

private:

	Logger log;

	ServerList servers;

	AccountManager*		accManager;
	ItemManager*		itemManager;
	AchievementManager* achManager;

	PingList pingList;
	std::clock_t pulse;

	EventLua::State lua;
};

#endif