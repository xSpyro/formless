#include "ItemManager.h"
#include "CSVManager.h"

ItemManager::ItemManager( EventLua::State* lua )
	: lua( lua )
{
	dbManager	= new CSVManager();

	srand( (unsigned int)time(NULL) );

	itemIDs		= 0;

	if( dbManager->openDatabase( "itemcounter.csv" ) )
	{
		itemIDs = atoi( dbManager->getField( 0, 0 ).c_str() );
	}
	else
	{
		DatabaseEntry dbEntry;
		dbEntry.addField("1");
		dbManager->addFieldLabel( "NextItemID" );
		dbManager->saveEntry( &dbEntry );
		itemIDs = 1;
	}

	dbManager->closeDatabase();
}

ItemManager::~ItemManager()
{
	delete dbManager;
}

unsigned int ItemManager::loadAllItems()
{
	unsigned int results = 0;

	if( dbManager->openDatabase( "items.csv" ) )
	{
		results = dbManager->getNrEntries();

		for( unsigned int i = 0; i < results; ++i )
		{
			DatabaseEntry* dbEntry = dbManager->nextEntry();

			// DatabaseEntry fields are strings only, so convert strings to ints first
			unsigned int uid	= atoi( dbEntry->getField( 0 ).c_str() );
			unsigned int user	= atoi( dbEntry->getField( 1 ).c_str() );
			unsigned int type	= atoi( dbEntry->getField( 2 ).c_str() );
			std::string name	= dbEntry->getField( 3 ).c_str();

			// Now we can create our item
			ItemInfo item( uid, user, type, 0, 0, name );
			items.push_back( item );

			if( uid > itemIDs )
			{
				itemIDs = uid + 1;
			}
		}
	}
	else
	{
		/* File doesn't exist, so we create labels */
		dbManager->addFieldLabel( "ItemID" );
		dbManager->addFieldLabel( "UserID" );
		dbManager->addFieldLabel( "Type" );
		dbManager->addFieldLabel( "Name" );
	}

	dbManager->closeDatabase();

	return results;
}

bool ItemManager::loadItem( unsigned int uid )
{
	bool result = false;

	if( dbManager->openDatabase( "items.csv" ) )
	{
		std::stringstream search;
		search << uid;

		DatabaseEntry* dbEntry = dbManager->findEntry( 0, search.str() );

		if( dbEntry != NULL )
		{
			// DatabaseEntry fields are strings only, so convert strings to ints first
			unsigned int user	= atoi( dbEntry->getField( 1 ).c_str() );
			unsigned int type	= atoi( dbEntry->getField( 2 ).c_str() );
			std::string name	= dbEntry->getField( 3 ).c_str();

			// Now we can create our item
			ItemInfo item( uid, user, type, 0, 0, name );
			items.push_back( item );

			result = true;
		}
	}
	else
	{
		/* File doesn't exist, so we create labels */
		dbManager->addFieldLabel( "ItemID" );
		dbManager->addFieldLabel( "UserID" );
		dbManager->addFieldLabel( "Type" );
		dbManager->addFieldLabel( "Name" );
	}

	dbManager->closeDatabase();

	return result;
}

bool ItemManager::saveItem( unsigned int uid )
{
	bool result = false;
	Items::const_iterator it = items.cbegin();
	
	while( it != items.cend() && it->uid != uid )
	{
		++it;
	}

	if( it != items.cend() )
	{
		if( !dbManager->openDatabase( "items.csv" ) )
		{
			/* File doesn't exist, so we create labels */
			dbManager->addFieldLabel( "ItemID" );
			dbManager->addFieldLabel( "UserID" );
			dbManager->addFieldLabel( "Type" );
			dbManager->addFieldLabel( "Name" );
		}

		DatabaseEntry dbEntry;
		dbEntry.addField( it->uid );
		dbEntry.addField( it->userid );
		dbEntry.addField( it->type );
		dbEntry.addField( it->name );

		result = dbManager->saveEntry( &dbEntry );

		dbManager->closeDatabase();
	}

	return result;
}

unsigned int ItemManager::addItem( ItemInfo& item )
{
	unsigned int result = 0;

	if( !itemValid( item.getItemName() ) )
	{
		loadValidItems();
	}

	if( item.uid == 0 && itemValid( item.getItemName() ) )
	{
		item.uid = itemIDs++;
		result = item.uid;
		updateCounter();

		item.type = getValidItemType( item.getItemName() );

		items.push_back( item );

		saveItem( item.uid );
	}
	else
	{
		std::cout << "*** INVALID ITEM ***" << std::endl;
		std::cout << "Item \"" << item.getItemName() << "\" is invalid. Try updating items.lua" << std::endl;
		std::cout << "********************" << std::endl;
	}

	return result;
}

ItemInfo* ItemManager::getItem( unsigned int uid )
{
	ItemInfo* item = NULL;

	Items::iterator it = findItem( uid );

	if( it != items.end() )
	{
		item = &(*it);
	}

	return item;
}

unsigned int ItemManager::loadValidItems()
{
	/* Lua reading. Maybe fix this in a better way... */
	lua->execute( "../resources/items/items.lua" );
	EventLua::Object modules	= lua->get( "modules" );
	EventLua::Object cores		= lua->get( "cores" );

	validCores.clear();
	validModules.clear();

	EventLua::Object mKey, mVal;
	while( modules.next( mKey, mVal ) )
	{
		std::string name = mVal.get( "name" ).queryString( "" );
		EventLua::Object typeTbl = mVal.get( "type" );
		int type = 0;

		if( typeTbl.isTable() )
		{
			type = typeTbl.get(1).queryInteger(0);
		}

		if( name != "" )
		{
			validModules.push_back( ItemInfo( 0, 0, type, 0, 0, name ) );
			//validModules++;
		}
	}

	EventLua::Object cKey, cVal;
	while( cores.next( cKey, cVal ) )
	{
		std::string name = cVal.get( "name" ).queryString( "" );
		EventLua::Object typeTbl = cVal.get( "type" );
		int type = 0;

		if( typeTbl.isTable() )
		{
			type = typeTbl.get(1).queryInteger(0);
		}

		if( name != "" )
		{
			validCores.push_back( ItemInfo( 0, 0, type, 0, 0, name ) );
			//validCores++;
		}
	}
	/* End of Lua reading */

	return nrValidItemsTotal();
}

unsigned int ItemManager::loadLockedItemList()
{
	/* Lua reading. Maybe fix this in a better way... */
	lua->execute( "../resources/items/items.lua" );
	EventLua::Object excludeList = lua->get( "excludeList" );

	lockedCores.clear();
	lockedModules.clear();

	EventLua::Object key, val;
	while( excludeList.next( key, val ) )
	{
		std::string name = val.get( "name" ).queryString( "" );
		EventLua::Object typeTbl = val.get( "type" );
		int type = 0;

		if( typeTbl.isTable() )
		{
			type = typeTbl.get(1).queryInteger(0);
		}

		if( name != "" )
		{
			switch( type )
			{
			case 1:

				lockedCores.push_back( ItemInfo( 0, 0, type, 0, 0, name ) );
				break;
			case 2:
			case 3:

				lockedModules.push_back( ItemInfo( 0, 0, type, 0, 0, name ) );
				break;
			}
		}
	}
	/* End of Lua reading */

	return (lockedCores.size() + lockedModules.size());
}

unsigned int ItemManager::nrValidItemsTotal()
{
	return ((validCores.size() + validModules.size()) - (lockedCores.size() + lockedModules.size()));
}

unsigned int ItemManager::nrValidCores()
{
	return (validCores.size() - lockedCores.size());
}

unsigned int ItemManager::nrValidModules()
{
	return (validModules.size() - lockedModules.size());
}

ItemInfo ItemManager::getRandomValidCore( const Inventory& excludeList )
{
	ItemInfo result;

	std::vector< const ItemInfo* > selection;

	// Build selection list
	for( Items::const_iterator it = validCores.begin(); it != validCores.end(); ++it )
	{
		std::string firstItem = it->getItemName();

		if( !excludeList.findCore( firstItem ) )
		{
			selection.push_back( &(*it) );
		}
	}

	// Remove locked items from selection
	for( Items::const_iterator cit = lockedCores.begin(); cit != lockedCores.end(); ++cit )
	{
		std::string lockedName = cit->getItemName();
		for( std::vector< const ItemInfo* >::iterator it = selection.begin(); it != selection.end(); ++it )
		{
			if( lockedName == std::string( (*it)->getItemName() ) )
			{
				it = selection.erase( it );
			}
		}
	}

	if( selection.size() > 0 )
	{
		unsigned int rnd = (unsigned int)(rand() % selection.size());
		result = *selection[rnd];
	}

	return result;
}

ItemInfo ItemManager::getRandomValidModule( const Inventory& excludeList )
{
	ItemInfo result;

	std::vector< const ItemInfo* > selection;

	// Build selection list
	for( Items::const_iterator it = validModules.begin(); it != validModules.end(); ++it )
	{
		std::string firstItem = it->getItemName();

		if( !excludeList.findModule( firstItem ) )
		{
			selection.push_back( &(*it) );
		}
	}

	// Remove locked items from selection
	for( Items::const_iterator cit = lockedModules.begin(); cit != lockedModules.end(); ++cit )
	{
		std::string lockedName = cit->getItemName();
		for( std::vector< const ItemInfo* >::iterator it = selection.begin(); it != selection.end(); )
		{
			if( lockedName == std::string( (*it)->getItemName() ) )
			{
				it = selection.erase( it );
			}
			else
			{
				++it;
			}
		}
	}

	if( selection.size() > 0 )
	{
		unsigned int rnd = (unsigned int)(rand() % selection.size());
		result = *selection[rnd];
	}

	return result;
}

void ItemManager::defaultInventory( unsigned int user, Inventory& inv )
{
	lua->execute( "../resources/items/items.lua" );
	EventLua::Object defaultInv	= lua->get( "defaultInv" );

	EventLua::Object key, val;
	while( defaultInv.next( key, val ) )
	{
		std::string name = val.get( "name" ).queryString( "" );
		EventLua::Object typeTbl = val.get( "type" );
		int type = -1;

		if( typeTbl.isTable() )
		{
			type = typeTbl.get(1).queryInteger(-1);
		}

		if( name != "" && type >= 0 )
		{
			ItemInfo item( 0, user, type, 0, 0, name );
			addItem( item );
			inv.registerItem( item );
		}
	}
}

void ItemManager::companionInventory( unsigned int user, Inventory& inv )
{
	lua->execute( "../resources/items/items.lua" );
	EventLua::Object companionInv	= lua->get( "companionInv" );

	EventLua::Object key, val;
	while( companionInv.next( key, val ) )
	{
		std::string name = val.get( "name" ).queryString( "" );
		EventLua::Object typeTbl = val.get( "type" );
		int type = -1;

		if( typeTbl.isTable() )
		{
			type = typeTbl.get(1).queryInteger(-1);
		}

		if( name != "" && type >= 0 )
		{
			ItemInfo item( 0, user, type, 0, 0, name );
			addItem( item );
			inv.registerItem( item );
		}
	}
}

void ItemManager::dbgInventory( unsigned int user, Inventory& inv )
{
	/* Lua reading. Maybe fix this in a better way... */
	lua->execute( "../resources/items/items.lua" );
	EventLua::Object modules	= lua->get( "modules" );
	EventLua::Object cores		= lua->get( "cores" );

	EventLua::Object mKey, mVal;
	while( modules.next( mKey, mVal ) )
	{
		std::string name = mVal.get( "name" ).queryString( "" );
		EventLua::Object typeTbl = mVal.get( "type" );
		int type = -1;

		if( typeTbl.isTable() )
		{
			type = typeTbl.get(1).queryInteger(-1);
		}

		if( name != "" && type >= 0 )
		{
			ItemInfo item( 0, user, type, 0, 0, name );
			addItem( item );
			inv.registerItem( item );
		}
	}

	EventLua::Object cKey, cVal;
	while( cores.next( cKey, cVal ) )
	{
		std::string name = cVal.get( "name" ).queryString( "" );
		EventLua::Object typeTbl = cVal.get( "type" );
		int type = -1;

		if( typeTbl.isTable() )
		{
			type = typeTbl.get(1).queryInteger(-1);
		}

		if( name != "" && type >= 0)
		{
			ItemInfo item( 0, user, type, 0, 0, name );
			addItem( item );
			inv.registerItem( item );
		}
	}
	/* End of Lua reading */
}

ItemManager::Items::iterator ItemManager::findItem( unsigned int uid )
{
	Items::iterator it = items.begin();

	while( it != items.end() && it->uid != uid )
	{
		++it;
	}

	return it;
}

bool ItemManager::itemValid( std::string name )
{
	bool result = false;

	for( Items::const_iterator it = validCores.cbegin(); it != validCores.cend() && !result; ++it )
	{
		if( it->getItemName() == name )
		{
			result = true;
		}
	}

	for( Items::const_iterator it = validModules.cbegin(); it != validModules.cend() && !result; ++it )
	{
		if( it->getItemName() == name )
		{
			result = true;
		}
	}

	return result;
}

unsigned int ItemManager::getValidItemType( std::string name )
{
	unsigned int result = 0;

	for( Items::const_iterator it = validCores.cbegin(); it != validCores.cend() && result == 0; ++it )
	{
		if( it->getItemName() == name )
		{
			result = it->type;
		}
	}

	for( Items::const_iterator it = validModules.cbegin(); it != validModules.cend() && result == 0; ++it )
	{
		if( it->getItemName() == name )
		{
			result = it->type;
		}
	}

	return result;
}

void ItemManager::updateCounter()
{
	if( !dbManager->openDatabase( "itemcounter.csv" ) )
	{
		/* File doesn't exist, so we create labels */
		dbManager->addFieldLabel( "NextItemID" );
	}

	dbManager->getEntry(0)->setField( 0, itemIDs );

	dbManager->closeDatabase();
}
