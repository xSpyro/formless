
#ifndef ITEM_LIST_H
#define ITEM_LIST_H

#include "Common.h"
#include "ItemInfo.h"

class ItemList
{
	typedef std::vector < ItemInfo > Items;

public:
	void registerItem( const ItemInfo & item );
	void clear();

	char * getDataPointer();
	unsigned int getDataSize();

	unsigned int getNumberOfEntries() const;

private:
	Items items;
};

#endif