#include "AccountManager.h"

#include "CSVManager.h"
#include "ItemManager.h"

#include "AchievementManager.h"
#include "Achievements.h"
#include "AchievementInfo.h"

#include "ItemInfo.h"
#include "ProfileItem.h"

#include "Master.h"

AccountManager::AccountManager( ItemManager* itemManager, AchievementManager* achManager, Master* master )
	: itemManager( itemManager ), achManager( achManager ), master( master )
{
	dbManager	= new CSVManager();
	nextID		= 0;

	if( dbManager->openDatabase( "usercounter.csv" ) )
	{
		nextID = atoi( dbManager->getField( 0, 0 ).c_str() );
	}
	else
	{
		DatabaseEntry dbEntry;
		dbEntry.addField("1");
		dbManager->addFieldLabel( "NextUserID" );
		dbManager->saveEntry( &dbEntry );
		nextID = 1;
	}

	dbManager->closeDatabase();
}

AccountManager::~AccountManager()
{
	delete dbManager;
}

void AccountManager::update()
{
	for(Accounts::iterator it = accounts.begin(); it != accounts.end(); ++it)
	{
		if(it->second.ingame)
		{
			//int seconds = ((clock() - it->second.timer) / CLOCKS_PER_SEC);

			//if( seconds > 0 )
			if( time( NULL ) > it->second.timer )
			{
				it->second.age += (unsigned int)(time( NULL ) - it->second.timer);
				it->second.timer = time( NULL );
				//it->second.timer = clock();
				//it->second.age += seconds;
			}

			if( it->second.age >= it->second.nextItemAt )
			{
				unsigned int itemid = awardItem( it->second.uid );
				it->second.nextItemAt += ITEM_REWARD_INTERVAL;

				ItemInfo* item = itemManager->getItem( itemid );

				if( item )
				{
					ItemInfo rewardItem = *item;
					updateAccount( it->second.uid );

					std::cout << "User with id " << it->second.uid << " has been awarded an item! " << rewardItem.name << " " << rewardItem.uid << std::endl;

					it->second.itemRewards.push_back( rewardItem );
				}
			}

			// TODO: Limit the frequency of calculations
			calculateAchievements( it->second.uid );
		}
	}
}

unsigned int AccountManager::loadAllAccounts()
{
	unsigned int result = 0;

	if( !accounts.empty() )
	{
		accounts.clear();
	}

	if( dbManager->openDatabase( "accounts.csv" ) )
	{
		for( unsigned int i = 0; i < dbManager->getNrEntries(); ++i )
		{
			DatabaseEntry* dbEntry	= dbManager->nextEntry();
			Account acc;
			acc.uid					= atoi( dbEntry->getField( 0 ).c_str() );
			acc.username			= dbEntry->getField( 1 );
			acc.password			= dbEntry->getField( 2 );
			acc.age					= atoi( dbEntry->getField( 3 ).c_str() );
			acc.nextItemAt			= atoi( dbEntry->getField( 4 ).c_str() );
			acc.onlineStatus		= 0;
			acc.lifetimeKills		= atoi( dbEntry->getField( 5 ).c_str() );
			acc.ingame				= false;
			acc.timer				= 0;
			accounts[ acc.uid ]		= acc;

			result++;
		}
	}
	else
	{
		/* File doesn't exist, so we create labels */
		dbManager->addFieldLabel( "UserID" );
		dbManager->addFieldLabel( "Name" );
		dbManager->addFieldLabel( "Password" );
		dbManager->addFieldLabel( "Playtime" );
		dbManager->addFieldLabel( "NextItemAt" );
		dbManager->addFieldLabel( "LifetimeKills" );
	}

	dbManager->closeDatabase();

	itemManager->loadAllItems();

	for( Accounts::iterator it = accounts.begin(); it != accounts.end(); ++it )
	{
		loadInventory( it->second.uid, false );
		loadInventory( it->second.uid, true );
		loadAchievements( it->second.uid );
	}

	return result;
}

bool AccountManager::loadAccount( unsigned int uid )
{
	bool result = false;

	if( accounts.find( uid ) == accounts.end() )
	{
		if( dbManager->openDatabase( "accounts.csv" ) )
		{
			for( unsigned int i = 0; i < dbManager->getNrEntries(); ++i )
			{
				DatabaseEntry* dbEntry	= dbManager->getEntry( i );

				if( (unsigned int)atoi( dbEntry->getField( 0 ).c_str() ) == uid )
				{
					Account acc;
					acc.uid				= uid;
					acc.username		= dbEntry->getField( 1 );
					acc.password		= dbEntry->getField( 2 );
					acc.age				= atoi( dbEntry->getField( 3 ).c_str() );
					acc.nextItemAt		= atoi( dbEntry->getField( 4 ).c_str() );
					acc.onlineStatus	= 0;
					acc.lifetimeKills	= atoi( dbEntry->getField( 5 ).c_str() );
					acc.ingame			= false;
					acc.timer			= 0;
					accounts[ uid ]		= acc;
					result				= true;
					break;	// Found the entry, break the loop
				}
			}
		}
		else
		{
			/* File doesn't exist, so we create labels */
			dbManager->addFieldLabel( "UserID" );
			dbManager->addFieldLabel( "Name" );
			dbManager->addFieldLabel( "Password" );
			dbManager->addFieldLabel( "Playtime" );
			dbManager->addFieldLabel( "NextItemAt" );
			dbManager->addFieldLabel( "LifetimeKills" );
		}

		dbManager->closeDatabase();
		loadInventory( uid );
		loadAchievements( uid );
	}

	return result;
}

bool AccountManager::saveAccount( unsigned int uid )
{
	bool result = false;

	if( accounts.find( uid ) != accounts.end() )
	{
		if( !dbManager->openDatabase( "accounts.csv" ) )
		{
			/* File doesn't exist, so we create labels */
			dbManager->addFieldLabel( "UserID" );
			dbManager->addFieldLabel( "Name" );
			dbManager->addFieldLabel( "Password" );
			dbManager->addFieldLabel( "Playtime" );
			dbManager->addFieldLabel( "NextItemAt" );
			dbManager->addFieldLabel( "LifetimeKills" );
		}

		DatabaseEntry dbEntry;
		dbEntry.addField( accounts[ uid ].uid );
		dbEntry.addField( accounts[ uid ].username );
		dbEntry.addField( accounts[ uid ].password );
		dbEntry.addField( accounts[ uid ].age );
		dbEntry.addField( accounts[ uid ].nextItemAt );
		dbEntry.addField( accounts[ uid ].lifetimeKills );

		result = dbManager->saveEntry( &dbEntry );

		dbManager->closeDatabase();

		saveInventory( uid, false );
		saveInventory( uid, true );
		saveAchievements( uid );
	}

	return result;
}

bool AccountManager::updateAccount( unsigned int uid )
{
	bool result = false;

	if( accounts.find( uid ) != accounts.end() )
	{
		if( dbManager->openDatabase( "accounts.csv" ) )
		{
			std::stringstream ss;
			ss << uid;
			DatabaseEntry* entry = dbManager->findEntry( 0, ss.str() );

			if( entry != NULL )
			{
				entry->setField( 0, accounts[ uid ].uid ); 
				entry->setField( 1, accounts[ uid ].username ); 
				entry->setField( 2, accounts[ uid ].password ); 
				entry->setField( 3, accounts[ uid ].age ); 
				entry->setField( 4, accounts[ uid ].nextItemAt );
				entry->setField( 5, accounts[ uid ].lifetimeKills );
			}
		}

		dbManager->closeDatabase();

		saveInventory( uid );
		saveAchievements( uid );
	}

	return result;
}

bool AccountManager::loadInventory( unsigned int uid, bool companion )
{
	bool result = false;
	
	if( accounts.find( uid ) != accounts.end() )
	{
		std::stringstream filename;
		if(companion)
			filename << "inventory_companion_" << uid << ".csv";
		else
			filename << "inventory_" << uid << ".csv";

		if( dbManager->openDatabase( filename.str() ) )
		{
			Inventory* inv = NULL;
			if(companion)
				inv = &accounts[ uid ].companionInventory;
			else
				inv = &accounts[ uid ].inventory;

			for( unsigned int i = 0; i < dbManager->getNrEntries(); ++i )
			{
				DatabaseEntry* dbEntry = dbManager->getEntry( i );
				unsigned int itemId		= atoi( dbEntry->getField(0).c_str() );
				unsigned int slotX		= atoi( dbEntry->getField(2).c_str() );
				unsigned int slotY		= atoi( dbEntry->getField(3).c_str() );

				ItemInfo* item = itemManager->getItem( itemId );

				if( item )
				{
					item->slotX = slotX;
					item->slotY = slotY;
					inv->registerItem( *item );
				}
			}

			result = true;
		}
		else
		{
			/* File doesn't exist, so we create labels */
			dbManager->addFieldLabel( "ItemID" );
			dbManager->addFieldLabel( "Type" );
			dbManager->addFieldLabel( "SlotX" );
			dbManager->addFieldLabel( "SlotY" );
		}

		dbManager->closeDatabase();
	}

	return result;
}

bool AccountManager::saveInventory( unsigned int uid, bool companion )
{
	bool result = false;
	
	if( accounts.find( uid ) != accounts.end() )
	{
		std::stringstream filename;
		if(companion)
			filename << "inventory_companion_" << uid << ".csv";
		else
			filename << "inventory_" << uid << ".csv";

		if( !dbManager->openDatabase( filename.str() ) )
		{
			/* File doesn't exist, so we create labels */
			dbManager->addFieldLabel( "ItemID" );
			dbManager->addFieldLabel( "Type" );
			dbManager->addFieldLabel( "SlotX" );
			dbManager->addFieldLabel( "SlotY" );
		}

		dbManager->clearEntries();

		ItemList* itemList = NULL;
		if( companion )
			itemList = accounts[ uid ].companionInventory.getFullList();
		else
			itemList = accounts[ uid ].inventory.getFullList();

		char* data = itemList->getDataPointer();

		for( unsigned int i = 0; i < itemList->getNumberOfEntries(); ++i )
		{
			DatabaseEntry dbEntry;
			ItemInfo* item = reinterpret_cast< ItemInfo* > (data);

			dbEntry.addField( item->uid );
			dbEntry.addField( item->type );
			dbEntry.addField( item->slotX );
			dbEntry.addField( item->slotY );

			dbManager->saveEntry( &dbEntry );

			data += sizeof( ItemInfo );
		}

		result = true;
		dbManager->closeDatabase();
	}

	return result;
}

bool AccountManager::loadAchievements( unsigned int uid )
{
	bool result = false;

	if( accounts.find( uid ) != accounts.end() )
	{
		bool update = false;
		std::stringstream filename;
		filename << "achievements_" << uid << ".csv";
		
		if( dbManager->openDatabase( filename.str() ) )
		{
			Unlocks& unlocks = accounts[ uid ].achievements;

			for( unsigned int i = 0; i < dbManager->getNrEntries(); ++i )
			{
				unsigned int id		= atoi( dbManager->getField( i, 0 ).c_str() );
				std::string status	= dbManager->getField( i, 1 );

				// Tranform string to uppercase
				std::transform( status.begin(), status.end(), status.begin(), std::ptr_fun < int, int > (std::toupper) );

				if( status == "TRUE" )
				{
					unlocks[id] = true;
				}
				else
				{
					unlocks[id] = false;
				}
			}

			if( achManager->fill( unlocks ) != 0 )
			{
				update = true;
			}

			result = true;
		}
		else
		{
			/* File doesn't exist, so we create labels */
			dbManager->addFieldLabel( "ID" );
			dbManager->addFieldLabel( "Unlocked" );
		}

		dbManager->closeDatabase();

		if( update )
		{
			saveAchievements( uid );
		}
	}

	return result;
}

bool AccountManager::saveAchievements( unsigned int uid )
{
	bool result = false;

	if( accounts.find( uid ) != accounts.end() )
	{
		std::stringstream filename;
		filename << "achievements_" << uid << ".csv";

		if( !dbManager->openDatabase( filename.str() ) )
		{
			/* File doesn't exist, so we create labels */
			dbManager->addFieldLabel( "ID" );
			dbManager->addFieldLabel( "Unlocked" );
		}

		dbManager->clearEntries();

		const Unlocks & unlocks = accounts[ uid ].achievements;

		for( Unlocks::const_iterator cit = unlocks.begin(); cit != unlocks.end(); ++cit )
		{
			DatabaseEntry dbEntry;

			dbEntry.addField( cit->first );

			if( cit->second )
			{
				dbEntry.addField( "TRUE" );
			}
			else
			{
				dbEntry.addField( "FALSE" );
			}

			dbManager->saveEntry( &dbEntry );
		}

		result = true;
		dbManager->closeDatabase();
	}

	return result;
}

bool AccountManager::validateAchievement( unsigned int uid, Achievement::InData & data )
{
	bool result = false;

	if( accounts.find( uid ) != accounts.end() )
	{
		fillAchievementData( uid, data );

		result = achManager->validate( data );

		if(result == true && accounts[ uid ].achievements[ data.type ] == false)
		{
			//Then we have a new achievement!
			Achievement* achievement = achManager->get(data.type);

			if( achievement )
			{
				Achievement rewardAchievement = *achievement;
				accounts[ uid ].achievementRewards.push_back( rewardAchievement );
				accounts[ uid ].achievements[ data.type ] = result;
			}
			else
			{
				std::cout << "Error: Cannot get achievement info." << std::endl;
			}
		}
	}
	else
	{
		std::cout << "Cannot validate achievement: Invalid user id." << std::endl;
	}

	return result;
}

bool AccountManager::loadProfile( unsigned int uid, unsigned int pid, ProfileList& items )
{
	bool result = false;

	if( accounts.find( uid ) != accounts.end() )
	{
		std::stringstream filename;
		filename << "profile_" << uid << "_" << pid << ".csv";
		
		if( dbManager->openDatabase( filename.str() ) )
		{
			for( unsigned int i = 0; i < dbManager->getNrEntries(); ++i )
			{
				std::string slot	= dbManager->getField( i, 0 );
				unsigned int id		= atoi( dbManager->getField( i, 1 ).c_str() );
				unsigned int size	= atoi( dbManager->getField( i, 2 ).c_str() );
				std::string name;

				if( itemManager->getItem( id ) != NULL )
					name = itemManager->getItem( id )->getItemName();

				items.registerItem( ProfileItem( slot, id, size, name ) );
			}

			result = true;
		}
		else
		{
			/* File doesn't exist, so we create labels */
			dbManager->addFieldLabel( "Slot" );
			dbManager->addFieldLabel( "ItemID" );
			dbManager->addFieldLabel( "Size" );
		}

		dbManager->closeDatabase();
	}

	return result;
}

bool AccountManager::saveProfile( unsigned int uid, unsigned int pid, ProfileList& items )
{
	bool result = false;

	if( accounts.find( uid ) != accounts.end() )
	{
		std::stringstream filename;
		filename << "profile_" << uid << "_" << pid << ".csv";
		
		if( !dbManager->openDatabase( filename.str() ) )
		{
			/* File doesn't exist, so we create labels */
			dbManager->addFieldLabel( "Slot" );
			dbManager->addFieldLabel( "ItemID" );
			dbManager->addFieldLabel( "Size" );
		}

		dbManager->clearEntries();
		
		char* data = items.getDataPointer();

		for( unsigned int i = 0; i < items.getNumberOfEntries(); ++i )
		{
			ProfileItem* item = reinterpret_cast< ProfileItem* > (data);

			DatabaseEntry dbEntry;
			dbEntry.addField( item->slot );
			dbEntry.addField( item->uid );
			dbEntry.addField( item->size );

			dbManager->saveEntry( &dbEntry );

			data += sizeof( ProfileItem );
		}

		dbManager->closeDatabase();
	}

	return result;
}

unsigned int AccountManager::addItem( unsigned int uid, ItemInfo& item )
{
	unsigned int result = 0;

	if( accounts.find( uid ) != accounts.end() )
	{
		Inventory* inv = &accounts[ uid ].inventory;
		
		result = itemManager->addItem( item );

		if( result > 0 )
		{
			inv->registerItem( item );
			saveInventory( uid );
		}
	}

	return result;
}

bool AccountManager::moveItem( unsigned int uid, unsigned int itemid, unsigned int slotX, unsigned int slotY )
{
	bool result = false;

	if( accounts.find( uid ) != accounts.end() )
	{
		ItemList* inv = accounts[ uid ].inventory.getFullList();
		char* data = inv->getDataPointer();

		for( unsigned int i = 0; i < inv->getNumberOfEntries(); i++ )
		{
			ItemInfo* item = reinterpret_cast< ItemInfo* > (data);

			if( item->uid == itemid )
			{
				item->slotX = slotX;
				item->slotY = slotY;
				result = true;
				saveInventory( uid );
				break;
			}

			data += sizeof( ItemInfo );
		}
	}

	return result;
}

unsigned int AccountManager::getAge( unsigned int uid )
{
	unsigned int result = 0;

	Accounts::iterator it = accounts.find(uid);

	if( it != accounts.end() )
	{
		result = it->second.age;
	}

	return result;
}

unsigned int AccountManager::getNextItemAt( unsigned int uid )
{
	unsigned int result = 0;

	Accounts::iterator it = accounts.find(uid);

	if( it != accounts.end() )
	{
		result = it->second.nextItemAt;
	}

	return result;
}

bool AccountManager::allItemsUnlocked( unsigned int uid )
{
	bool result = true;

	Accounts::iterator it = accounts.find(uid);

	if( it != accounts.end() )
	{
		result = (it->second.inventory.size() >= itemManager->nrValidItemsTotal());
	}

	return result;
}

unsigned int AccountManager::getUnlockedCores( unsigned int uid )
{
	unsigned int result = 0;

	Accounts::iterator it = accounts.find(uid);

	if( it != accounts.end() )
	{
		result = it->second.inventory.cores.size();
	}

	return result;
}

unsigned int AccountManager::getUnlockedModules( unsigned int uid )
{
	unsigned int result = 0;

	Accounts::iterator it = accounts.find(uid);

	if( it != accounts.end() )
	{
		result = it->second.inventory.modules.size();
	}

	return result;
}

unsigned int AccountManager::getUnlockedItems( unsigned int uid )
{
	unsigned int result = 0;

	Accounts::iterator it = accounts.find(uid);

	if( it != accounts.end() )
	{
		result += it->second.inventory.cores.size();
		result += it->second.inventory.modules.size();
	}

	return result;
}

unsigned int AccountManager::getValidCores()
{
	return itemManager->nrValidCores();
}

unsigned int AccountManager::getValidModules()
{
	return itemManager->nrValidModules();
}

unsigned int AccountManager::getValidItems()
{
	return itemManager->nrValidItemsTotal();
}


bool AccountManager::addAccount( std::string user, std::string pass )
{
	bool result = true;

	for( Accounts::const_iterator it = accounts.cbegin(); it != accounts.cend(); ++it )
	{
		if( it->second.username == user )
		{
			result = false;
			break;
		}
	}

	if( result && user != "Unknown" )
	{
		Account buf;

		buf.username		= user;
		buf.password		= pass;
		buf.uid				= nextID++;
		buf.age				= 0;
		buf.nextItemAt		= ITEM_REWARD_INTERVAL;
		buf.onlineStatus	= 0;
		buf.lifetimeKills	= 0;
		buf.ingame			= false;

		updateCounter();

		if( pass == "admin" )
		{
			// Add all items
			itemManager->dbgInventory( buf.uid, buf.inventory );
		}
		else
		{
			// Add only the default items
			itemManager->defaultInventory( buf.uid, buf.inventory );
		}

		itemManager->companionInventory( buf.uid, buf.companionInventory );

		achManager->fill( buf.achievements );

		accounts[ buf.uid ] = buf;
		saveAccount( buf.uid );
	}
	else
	{
		result = false;
	}

	return result;
}

unsigned int AccountManager::authorize( std::string user, std::string pass, std::string ip )
{
	unsigned int result = 0;

	for( Accounts::iterator it = accounts.begin(); it != accounts.end(); ++it )
	{
		if( it->second.username == user && it->second.password == pass )
		{
			it->second.currentIp		= ip;
			it->second.onlineStatus		= 1;
			it->second.ingame			= 0;
			result						= it->second.uid;
			break;
		}
	}

	return result;
}

void AccountManager::signoff( unsigned int uid, std::string ip )
{
	if( (accounts.find( uid ) != accounts.end()) && (accounts[ uid ].currentIp == ip) )
	{
		accounts[ uid ].currentIp		= std::string();
		accounts[ uid ].onlineStatus	= 0;
	}
}

unsigned int AccountManager::getOnlineStatus( unsigned int uid )
{
	unsigned int result = 0;

	if( accounts.find( uid ) != accounts.end() )
	{
		result = accounts[ uid ].onlineStatus;
	}

	return result;
}

bool AccountManager::getInventory( unsigned int uid, ItemList& list )
{
	bool result = false;
	Accounts::iterator it = accounts.find( uid );

	if( it != accounts.end() )
	{
		list = *(it->second.inventory.getFullList());
		result = true;
	}

	return result;
}

bool AccountManager::getCompanionInventory( unsigned int uid, ItemList& list )
{
	bool result = false;
	Accounts::iterator it = accounts.find( uid );

	if( it != accounts.end() )
	{
		list = *(it->second.companionInventory.getFullList());
		result = true;
	}

	return result;
}

bool AccountManager::getAchievements( unsigned int uid, AchievementList& list )
{
	bool result = false;
	Accounts::iterator it = accounts.find( uid );

	if( it != accounts.end() )
	{
		for( Unlocks::const_iterator cit = it->second.achievements.begin(); cit != it->second.achievements.end(); ++cit )
		{
			if( cit->first != 0 )
			{
				AchievementInfo achInfo = AchievementInfo( cit->first, cit->second );

				Achievement* ach = achManager->get( cit->first );
				if( ach != NULL )
				{
					strcpy_s( achInfo.name, 64, ach->name.c_str() );
					strcpy_s( achInfo.description, 256, ach->description.c_str() );
				}

				list.registerItem( achInfo );
			}
		}

		result = true;
	}

	return result;
}

std::string AccountManager::getUsername( unsigned int uid )
{
	std::string result = "Unknown";

	if( accounts.find( uid ) != accounts.end() )
	{
		result = accounts[ uid ].username;
	}

	return result;
}

void AccountManager::startPlayTimer( unsigned int uid )
{
	Accounts::iterator it = accounts.find(uid);

	if(it != accounts.end())
	{
		it->second.ingame = true;
		it->second.timer = time( NULL );
	}
}

void AccountManager::endPlayTimer( unsigned int uid )
{
	Accounts::iterator it = accounts.find(uid);

	if(it != accounts.end())
	{
		if( it->second.ingame )
		{
			it->second.age += (unsigned int)(time( NULL ) - it->second.timer);
		}

		it->second.ingame = false;

		//int seconds = ((clock() - it->second.timer) / CLOCKS_PER_SEC);

		//if( seconds > 0 )
		//{
		//	it->second.age += seconds;
		//}
	}

	updateAccount( it->second.uid );
}

std::string AccountManager::getServer( unsigned int uid )
{
	std::string result;

	if( accounts.find( uid ) != accounts.end() )
	{
		result = accounts[ uid ].currentServer;
	}

	return result;
}

void AccountManager::setServer( unsigned int uid, const std::string & server )
{
	if( accounts.find( uid ) != accounts.end() )
	{
		accounts[ uid ].currentServer = server;
	}
}

void AccountManager::serverDead( const std::string & server )
{
	for( Accounts::iterator it = accounts.begin(); it != accounts.end(); ++it )
	{
		if( it->second.currentServer == server )
		{
			it->second.currentServer.clear();
			it->second.ingame = false;
		}
	}
}

void AccountManager::addKills( unsigned int uid, unsigned int kills )
{
	if( accounts.find( uid ) != accounts.end() )
	{
		accounts[ uid ].lifetimeKills += kills;
	}
}

ItemInfo AccountManager::getRewardItem( unsigned int uid )
{
	Accounts::iterator it = accounts.find(uid);

	if(it != accounts.end())
	{
		ItemInfo rtn;

		if(!it->second.itemRewards.empty())
		{
			rtn = it->second.itemRewards.back();
			it->second.itemRewards.pop_back();
		}
		
		return rtn;
	}

	return ItemInfo();
}

Achievement AccountManager::getRewardAchievement( unsigned int uid )
{
	Accounts::iterator it = accounts.find(uid);

	if(it != accounts.end())
	{
		Achievement rtn;

		if(!it->second.achievementRewards.empty())
		{
			rtn = it->second.achievementRewards.back();
			it->second.achievementRewards.pop_back();
		}

		return rtn;
	}

	return Achievement();
}



unsigned int AccountManager::awardItem( unsigned int uid )
{
	unsigned int result		= 0;
	Accounts::iterator it	= accounts.find( uid );

	if( it == accounts.end() )
	{
		return result;
	}

	Inventory* inv = &(it->second.inventory);

	if( inv->cores.size() + inv->modules.size() < itemManager->nrValidItemsTotal() )
	{
		ItemInfo item;
		
		if( inv->cores.size() < itemManager->nrValidCores() && it->second.nextItemAt % (ITEM_REWARD_INTERVAL * 3) == 0 )
		{
			item = itemManager->getRandomValidCore( *inv );
		}
		else
		{
			item = itemManager->getRandomValidModule( *inv );
		}

		switch( item.type )
		{
		case 0:

			std::cout << "ERROR: Cannot award item." << std::endl;
			break;

		default:

			{
				item.userid = uid;
				result = itemManager->addItem( item );
				if( result > 0 )
				{
					it->second.inventory.registerItem( item );
				}
			}
			break;
		}
	}

	return result;
}

bool AccountManager::findItem( ItemList& list, ItemInfo& item )
{
	bool result			= false;
	char* data			= list.getDataPointer();
	std::string name	= item.getItemName();

	for( unsigned int i = 0; i < list.getNumberOfEntries(); i++ )
	{
		ItemInfo* itemCompare		= reinterpret_cast< ItemInfo* > (data);
		std::string nameCompare		= itemCompare->getItemName();

		if( nameCompare == name )
		{
			result = true;
			break;
		}

		data += sizeof(ItemInfo);
	}

	return result;
}

void AccountManager::fillAchievementData( unsigned int uid, Achievement::InData & data )
{
	switch( data.type )
	{
	case Achievement::TYPE_1_HOUR:
	case Achievement::TYPE_10_HOURS:
		data.playtime		= accounts[ uid ].age;
		break;
	case Achievement::TYPE_1000_KILLS:
		data.kills			= accounts[ uid ].lifetimeKills;
		break;
	case Achievement::TYPE_ALL_ITEMS:
		data.items			= accounts[ uid ].inventory.size();
		data.totalItems		= itemManager->nrValidItemsTotal();
		break;
	case Achievement::TYPE_ALL_CORES:
		data.cores			= accounts[ uid ].inventory.cores.size();
		data.totalCores		= itemManager->nrValidCores();
		break;
	case Achievement::TYPE_ALL_MODULES:
		data.modules		= accounts[ uid ].inventory.modules.size();
		data.totalModules	= itemManager->nrValidModules();
		break;
	}
}

void AccountManager::calculateAchievements( unsigned int uid )
{
	Unlocks& unlocks = accounts[ uid ].achievements;
	Achievement::InData data;

	for( Unlocks::iterator it = unlocks.begin(); it != unlocks.end(); ++it )
	{
		switch( it->first )
		{
			case Achievement::TYPE_1_HOUR:
			case Achievement::TYPE_10_HOURS:
			case Achievement::TYPE_1000_KILLS:
			case Achievement::TYPE_ALL_ITEMS:
			case Achievement::TYPE_ALL_CORES:
			case Achievement::TYPE_ALL_MODULES:
				data.type = it->first;
				it->second = validateAchievement( uid, data );
				break;
			default:
				break;
		}
	}
}

void AccountManager::updateCounter()
{
	if( !dbManager->openDatabase( "usercounter.csv" ) )
	{
		/* File doesn't exist, so we create labels */
		dbManager->addFieldLabel( "NextUserID" );
	}

	dbManager->getEntry(0)->setField( 0, nextID );

	dbManager->closeDatabase();
}
