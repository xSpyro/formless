#ifndef ACHIEVEMENT_MANAGER__H
#define ACHIEVEMENT_MANAGER__H

#include "Common.h"
#include "Achievement.h"

class DatabaseManager;

class AchievementManager
{
	typedef std::map< unsigned int, Achievement* > Achievements;
	typedef std::map< unsigned int, bool > Unlocks; // Used for achievement unlocks, mirrors the map in Account

public:
	AchievementManager();
	~AchievementManager();

	unsigned int initAll();

	unsigned int loadAll();

	bool load( unsigned int uid );
	bool save( unsigned int uid );

	Achievement* get( unsigned int uid );

	unsigned int fill( Unlocks & list ); // Fills [list] with missing achievements, returning the number of achievements added

	bool validate( const Achievement::InData & data );

private:
	void clear();
	Achievement* create( unsigned int type, std::string name, std::string description );

private:
	Achievements achievements;
	DatabaseManager* dbManager;
};

#endif
