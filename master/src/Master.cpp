#include "Master.h"

#include "AccountManager.h"
#include "ItemManager.h"
#include "AchievementManager.h"

Master::Master(boost::asio::io_service & service)
	: ServerInterface(service, 1022), log(std::cout)
{
	me = this;
	pulse = 60 * CLOCKS_PER_SEC;

	achManager	= new AchievementManager();
	itemManager = new ItemManager( &lua );
	accManager	= new AccountManager( itemManager, achManager, this );

	EventLua::Object g = lua.getGlobals();
	g.bind( "include", &loadScript );
}

Master::~Master()
{
	delete accManager;
	delete itemManager;
	delete achManager;
}

bool Master::startup()
{
	disableConnectResponse();

	unsigned int loadedAchievements = achManager->initAll();
	log("Initiated ", loadedAchievements, " achievements");

	unsigned int loadedAccounts = accManager->loadAllAccounts();
	log("Found ", loadedAccounts, " accounts in database");

	unsigned int loadedItems = itemManager->loadValidItems();
	unsigned int loadedCores = itemManager->nrValidCores();
	unsigned int loadedModules = itemManager->nrValidModules();
	log("Found ", loadedItems, " valid items (", loadedCores, " cores and ", loadedModules, " modules)");

	unsigned int lockedItems = itemManager->loadLockedItemList();
	log( lockedItems, " items excluded from random award list" );


	return true;
}

int Master::run()
{
	std::clock_t nextPing = std::clock() + pulse;
	while ( true )
	{
		poll();

		if( std::clock() >= nextPing )
		{
			checkServers(std::clock());
			nextPing = std::clock() + pulse;
		}

		accManager->update();
	}
}

void Master::onPacket(RawPacket & packet)
{
	switch ( packet.header.id )
	{
	case PING_PACKET:

		processPingPacket(*reinterpret_cast < PingPacket* > (packet.data));
		break;

	case MASTER_AUTH_PACKET:

		processAuthRequest(*reinterpret_cast < MasterAuthPacket* > (packet.data));
		break;

	case CREATE_USER_PACKET:

		processCreateUserPacket(*reinterpret_cast < CreateUserPacket* > (packet.data));
		break;

	case DROP_USER_PACKET:

		processDropUserPacket(*reinterpret_cast < DropUserPacket* > (packet.data));
		break;

	case QUERY_USERNAME_PACKET:

		processQueryUsernamePacket(*reinterpret_cast < QueryUsernamePacket* > (packet.data));
		break;

	case ADD_ITEM_PACKET:

		processAddItemPacket(*reinterpret_cast < AddItemPacket* > (packet.data));
		break;

	case MOVE_ITEM_PACKET:

		processMoveItemPacket(*reinterpret_cast < MoveItemPacket* > (packet.data));
		break;

	case INVENTORY_REQUEST_PACKET:

		processInventoryRequest(*reinterpret_cast < InventoryRequestPacket* > (packet.data));
		break;

	case PROFILE_LOAD_PACKET:

		processProfileLoadRequest(*reinterpret_cast < ProfileLoadPacket* > (packet.data));
		break;

	case PROFILE_LIST_PACKET:

		processProfileListPacket(*reinterpret_cast < ProfileListPacket* > (packet.data));
		break;

	case QUERY_SERVER_LIST_PACKET:

		processServerListRequest(*reinterpret_cast < QueryServerListPacket* > (packet.data));
		break;

	case SERVER_UPDATE_PACKET:

		processServerUpdatePacket(*reinterpret_cast < ServerUpdatePacket* > (packet.data));
		break;

	case REGISTER_SERVER_PACKET:

		processNewServer(*reinterpret_cast < RegisterServerPacket* > (packet.data));
		break;

	case DROP_SERVER_PACKET:

		processDropServerPacket(*reinterpret_cast < DropServerPacket* > (packet.data));
		break;

	case GAME_START_PACKET:
		
		processGameStartPacket(*reinterpret_cast < GameStartPacket* > (packet.data));
		break;

	case GAME_END_PACKET:

		processGameEndPacket(*reinterpret_cast < GameEndPacket* > (packet.data));
		break;

	case ACCOUNT_AGE_PACKET:

		processAccountAgePacket(*reinterpret_cast < AccountPacket* > (packet.data));
		break;

	case ACHIEVEMENT_REQUEST_PACKET:

		processAchievementRequestPacket(*reinterpret_cast < AchievementRequestPacket* > (packet.data));
		break;

	case ACHIEVEMENT_LIST_REQUEST_PACKET:

		processAchievementListRequestPacket(*reinterpret_cast < AchievementListRequestPacket* > (packet.data));
		break;

	case SERVER_JOIN_PACKET:

		processServerJoinPacket(*reinterpret_cast < ServerJoinPacket* > (packet.data));
		break;

	case SERVER_LEAVE_PACKET:

		processServerLeavePacket(*reinterpret_cast < ServerLeavePacket* > (packet.data));
		break;
	}
}


void Master::checkServers(std::clock_t tick)
{
	//log("Checking server activity ...");
	std::vector< std::string > remove;
	for( PingList::iterator it = pingList.begin(); it != pingList.end(); ++it )
	{
		if( tick - it->second > pulse )
		{
			remove.push_back( it->first );
		}
	}

	for( std::vector< std::string >::iterator it = remove.begin(); it != remove.end(); ++it )
	{
		pingList.erase( *it );
		servers.dropEntry( *it );
		accManager->serverDead( *it );
		log.notice("Dropping server ", *it, " due to lost activity");
	}
}

void Master::processPingPacket(PingPacket & packet)
{
	PingList::iterator pos = pingList.find( getResponseIp() );
	if( pos != pingList.end() )
	{
		pos->second = std::clock();
		//log( getResponseIp(), " will be stone dead in a moment" );
	}
}

void Master::processAuthRequest(MasterAuthPacket & packet)
{
	log("Auth from ", packet.login, " with password ", packet.password);

	unsigned int uid = accManager->authorize( packet.login, packet.password, getResponseIp() );

	AuthResultPacket result; 

	if( uid != 0 )
	{
		result.state = 1;
		result.uid = uid;
		result.setName( packet.login );
		log("User id: ", uid, " signing on. Online status: ", accManager->getOnlineStatus( uid ) );
	}
	else
	{
		log.notice("Auth from ", packet.login, " with password ", packet.password, " failed. IP: ", getResponseIp());
		result.state = 0;
		result.uid = 0;
	}

	respond(result);
}

void Master::processCreateUserPacket(CreateUserPacket & packet)
{
	log("Creating user account. Name: ", packet.username, "; Pass: ", packet.password);
	
	CreateUserPacket result;
	result.success = accManager->addAccount( packet.username, packet.password );
	
	if( !result.success )
	{
		log.notice("Account creation FAILED");
	}
	
	respond( result );
}

void Master::processDropUserPacket(DropUserPacket & packet)
{
	accManager->signoff( packet.uid, getResponseIp() );
	log("User id: ", packet.uid, " signing off. Online status: ", accManager->getOnlineStatus( packet.uid ) );
	respond( NetPacket() );
}

void Master::processQueryUsernamePacket(QueryUsernamePacket & packet)
{
	UserPacket response;
	response.uid = packet.uid;
	response.nid = packet.nid;
	response.team = 0;

	std::stringstream username;

	if(packet.uid < 0)
	{
		std::string mastername = accManager->getUsername( packet.uid * -1 );
		mastername = mastername.substr(0, 4);
		username << "Companion (" << mastername << ")";
		response.uid = packet.uid * -1;
	}
	else
		username << accManager->getUsername( packet.uid );

	response.setName( username.str() );

	respond( response );
}

void Master::processMoveItemPacket(MoveItemPacket & packet)
{
	accManager->moveItem( packet.userid, packet.itemid, packet.slotX, packet.slotY );
	//std::cout << "Item [" << packet.itemid << "] is now in holder \"" << packet.holder << "\" at pos " << packet.slot << std::endl;
}

void Master::processAddItemPacket(AddItemPacket & packet)
{
	ItemInfo item( 0, packet.uid, packet.type, 0, 0, packet.name );
	accManager->addItem( packet.uid, item );
}

void Master::processInventoryRequest(InventoryRequestPacket & packet)
{
	ItemList items;

	if(packet.companion == false)
		accManager->getInventory( packet.uid, items );
	else
		accManager->getCompanionInventory( packet.uid, items );

	char* data = items.getDataPointer();
	unsigned int size = items.getDataSize();

	//log("Item Size Result ", size);
	
	ItemListPacket response;

	response.results = items.getNumberOfEntries();

	//log("Response results ", response.results);

	response.size = sizeof(ItemListPacket) - RawPacket::HeaderSize + size;

	//log("Response size ", response.size);

	// send header with increased size of packet
	respond(reinterpret_cast < char* > (&response), sizeof(ItemListPacket), data, size);
}

void Master::processProfileLoadRequest(ProfileLoadPacket & packet)
{
	ProfileList items;
	accManager->loadProfile( packet.uid, packet.pid, items );

	char* data = items.getDataPointer();
	unsigned int size = items.getDataSize();

	//log("Profile Size Result ", size);

	ProfileListPacket response;

	response.results = items.getNumberOfEntries();
	response.uid = packet.uid;
	response.pid = packet.pid;
	response.size = sizeof(ProfileListPacket) - RawPacket::HeaderSize + size;

	// send header with increased size of packet
	respond( reinterpret_cast < char* > (&response), sizeof(ProfileListPacket) , data, size);
}

void Master::processProfileListPacket(ProfileListPacket & packet)
{
	std::vector< ProfileItem > items;
	ProfileList profile;

	items.clear();
	char * offset = &reinterpret_cast < char* > (&packet)[RawPacket::HeaderSize];
	unsigned int results = *reinterpret_cast < unsigned int* > (offset);
	offset += sizeof(unsigned int); // skip the results int
	offset += sizeof(unsigned int); // skip the uid int
	offset += sizeof(unsigned int); // skip the pid int

	for (unsigned int i = 0; i < results; ++i)
	{
		ProfileItem * item = reinterpret_cast < ProfileItem* > (offset);
		items.push_back( *item );
		offset += sizeof(ProfileItem);

		profile.registerItem( *item );
	}

	accManager->saveProfile( packet.uid, packet.pid, profile );
}

void Master::processServerListRequest(QueryServerListPacket & packet)
{
	log("Request Server List");

	char * data = servers.getDataPointer();
	unsigned int size = servers.getDataSize();

	ServerListPacket response;

	//log("Calculated size ", size);

	

	response.results = servers.getNumberOfEntries();
	response.size = sizeof(ServerListPacket) - RawPacket::HeaderSize + size;

	//log("Size of custom data ", response.size);
	//log("Size of size ", size);

	//log("Size of header ", RawPacket::HeaderSize);

	//log("Sent header size ", sizeof(ServerListPacket));

	log(" * Number of Servers ", servers.getNumberOfEntries());



	// send header with increased size of packet
	respond(reinterpret_cast < char* > (&response), sizeof(ServerListPacket), data, size);

	/*
	if (response.results > 0)
	{
		// now send content if any
		respond(data, size);
	}
	*/
}

void Master::processServerUpdatePacket(ServerUpdatePacket & packet)
{
	//std::cout << "Recieved a " << packet.size << " bytes update from " << getResponseIp() << ": " << packet.details << std::endl;
	respond(NetPacket());
}

void Master::processNewServer(RegisterServerPacket & packet)
{
	std::string ip = getResponseIp();
	std::string name = packet.name;
	ServerAddedPacket response;

	if(!servers.findEntry(ip))
	{
		servers.registerEntry(ServerInfo(ip, name));
		pingList[ip] = std::clock();
		response.error = 0;
		log("Server Registered ", ip);
	}
	else
	{
		pingList[ip] = std::clock();
		log(ip, " is getting better");
		response.error = 1;
	}

	respond(response);
}

void Master::processDropServerPacket(DropServerPacket & packet)
{
	std::string ip = getResponseIp();
	servers.dropEntry(ip);

	pingList.erase( ip );

	accManager->serverDead( ip );

	log("Server Dropped ", ip);

	respond(NetPacket());
}

void Master::processGameStartPacket(GameStartPacket & packet)
{
	accManager->startPlayTimer(packet.user_id);
}

void Master::processGameEndPacket(GameEndPacket & packet)
{
	accManager->addKills( packet.user_id, packet.kills );
	Achievement::InData data;
	data.type	= Achievement::TYPE_1000_KILLS;
	accManager->validateAchievement( packet.user_id, data );

	if( packet.full_time )
	{
		data.kills = packet.kills;
		data.type = Achievement::TYPE_TROLL;
		accManager->validateAchievement( packet.user_id, data );
	}

	accManager->endPlayTimer(packet.user_id);
}

void Master::processAccountAgePacket(AccountPacket & packet)
{
	AccountPacket response;

	response.userID				= packet.userID;
	response.age				= accManager->getAge(packet.userID);
	response.nextItemAt			= accManager->getNextItemAt(packet.userID);
	response.prevItemAt			= accManager->getNextItemAt(packet.userID) - ITEM_REWARD_INTERVAL;
	response.nrUnlockedCores	= accManager->getUnlockedCores(packet.userID);
	response.nrUnlockedModules	= accManager->getUnlockedModules(packet.userID);
	response.nrValidCores		= accManager->getValidCores();
	response.nrValidModules		= accManager->getValidModules();
	response.allUnlocked		= accManager->allItemsUnlocked(packet.userID);
	
	std::string itemname = "";
	ItemInfo item = accManager->getRewardItem(packet.userID);
	if(item.uid != 0)
		itemname = item.getItemName();
	strcpy_s(response.itemname, 32, itemname.c_str());

	std::string achname;
	std::string achdesc;
	Achievement achievement = accManager->getRewardAchievement(packet.userID);
	if(achievement.type != Achievement::TYPE_UNKNOWN)
	{
		achname = achievement.name;
		achdesc = achievement.description;
	}
	strcpy_s(response.achievementName, 64, achname.c_str());
	strcpy_s(response.achievementDesc, 256, achdesc.c_str());
	

	respond(response);
}

void Master::processAchievementRequestPacket(AchievementRequestPacket & packet)
{
	Achievement::InData data;
	
	// Fill the data structure here
	data.type		= packet.type;
	data.kills		= packet.kills;
	data.killstreak = packet.killstreak;
	data.rambo		= packet.rambo;

	// Validate
	bool result = accManager->validateAchievement( packet.user_id, data );

	Achievement* ach = achManager->get( data.type );

	// Fill the return packet
	AchievementResponsePacket response;
	response.user_id	= packet.user_id;
	response.type		= packet.type;
	response.unlocked	= result;

	if( ach != NULL )
	{
		response.setName( ach->name );
	}

	respond( response );
}

void Master::processAchievementListRequestPacket(AchievementListRequestPacket & packet)
{
	AchievementList list;
	accManager->getAchievements( packet.user_id, list );

	char* data = list.getDataPointer();
	unsigned int size = list.getDataSize();

	AchievementListPacket response;
	response.results = list.getNumberOfEntries();
	response.size = sizeof(AchievementListPacket) - RawPacket::HeaderSize + size;

	respond( reinterpret_cast < char* > (&response), sizeof(AchievementListPacket), data, size );
}

void Master::processServerJoinPacket(ServerJoinPacket & packet)
{
	accManager->setServer( packet.user_id, packet.server );
}

void Master::processServerLeavePacket(ServerLeavePacket & packet)
{
	accManager->setServer( packet.user_id, std::string() );
	accManager->endPlayTimer( packet.user_id );
}

Master* Master::me = NULL;
void Master::loadScript( const char* filename )
{
	std::string file = "../resources/";
	file += filename;
	me->lua.execute( file.c_str() );
}
