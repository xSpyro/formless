#ifndef ITEM_MANAGER__H
#define ITEM_MANAGER__H

#include "Common.h"
#include "Inventory.h"

#include "../../include/lua/EventLua.h"

class DatabaseManager;

class ItemManager
{
	typedef std::vector< ItemInfo > Items;

public:
	ItemManager( EventLua::State* lua );
	~ItemManager();

	unsigned int	loadAllItems();

	bool			loadItem( unsigned int uid );
	bool			saveItem( unsigned int uid );

	// Returns the newly generated itemID for [item], or 0 on failure
	unsigned int	addItem( ItemInfo& item );

	// Returns item with id [uid], or NULL on failure
	ItemInfo*		getItem( unsigned int uid );

	unsigned int	loadValidItems();

	unsigned int	loadLockedItemList();

	unsigned int	nrValidItemsTotal();
	unsigned int	nrValidCores();
	unsigned int	nrValidModules();

	ItemInfo		getRandomValidCore( const Inventory& excludeList );
	ItemInfo		getRandomValidModule( const Inventory& excludeList );
	
	void			defaultInventory( unsigned int user, Inventory& inv );
	void			companionInventory( unsigned int user, Inventory& inv );
	void			dbgInventory( unsigned int user, Inventory& inv );

private:
	Items::iterator findItem( unsigned int uid );
	bool			itemValid( std::string name );
	unsigned int	getValidItemType( std::string name );
	void			updateCounter();

private:
	DatabaseManager*	dbManager;
	Items				items;
	unsigned int		itemIDs;

	Items				validCores;
	Items				validModules;
	Items				lockedCores;
	Items				lockedModules;

	EventLua::State*	lua;
};

#endif
