#ifndef DATABASE_MANAGER__H
#define DATABASE_MANAGER__H

#include "Common.h"
#include "DatabaseEntry.h"

class DatabaseManager
{
public:
	DatabaseManager();
	virtual ~DatabaseManager();

	virtual bool					openDatabase( std::string name )						= 0;
	virtual void					closeDatabase()											= 0;

	virtual unsigned int			getNrEntries()											= 0;
	virtual DatabaseEntry*			nextEntry()												= 0;
	virtual DatabaseEntry*			getEntry( unsigned int entry )							= 0;
	virtual DatabaseEntry*			findEntry( unsigned int field, std::string search )		= 0;
	virtual std::string				getFieldLabel( unsigned int field )						= 0;
	virtual bool					setFieldLabel( unsigned int field, std::string label )	= 0;
	virtual unsigned int			addFieldLabel( std::string label )						= 0;
	virtual std::string				getField( unsigned int entry, unsigned int field )		= 0;
	virtual bool					saveEntry( DatabaseEntry* entry )						= 0;
	virtual void					deleteEntry( unsigned int entry )						= 0;
	virtual void					clearEntries()											= 0;
};

#endif
