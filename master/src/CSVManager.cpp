#include "CSVManager.h"


CSVManager::CSVManager()
{
	baseDir			= "../databases/";
	currentDb		= "";
	currentEntry	= entries.begin();
}

CSVManager::~CSVManager()
{
}

bool CSVManager::openDatabase( std::string name )
{
	bool result = false;

	if( entries.empty() )
	{
		currentDb = name;
		labels.clear();
		std::ifstream iFile;
		
		iFile.open( baseDir + currentDb );

		if( iFile.is_open() )
		{
			unsigned int pos = 1;
			std::string line;

			/* Extract the labels */
			std::getline( iFile, line );
			if( !iFile.eof() )
			{
				std::string field;
				unsigned int start = 0;
				unsigned int end = line.find_first_of( ';', start );

				while( end != line.npos )
				{
					field = line.substr( start, end - start );
					labels.addField( field );
					start = end + 1;
					end = line.find_first_of( ';', start );
				}

				if( start < line.size() )
				{
					field = line.substr( start );
					labels.addField( field );
				}
			}

			/* Read the rest of the data */
			
			/* Variables used to adjust the labels entry. Measured in number of fields */
			unsigned int maxEntrySize	= 0;
			unsigned int entrySize		= 0;
			
			while( !iFile.eof() )
			{
				std::getline( iFile, line );

				if( !iFile.eof() )
				{
					DatabaseEntry dbEntry;
					std::string field;
					unsigned int start = 0;
					unsigned int end = line.find_first_of( ';', start );

					while( end != line.npos )
					{
						field = line.substr( start, end - start );
						dbEntry.addField( field );
						entrySize++;
						start = end + 1;
						end = line.find_first_of( ';', start );
					}

					if( start < line.size() )
					{
						field = line.substr( start );
						dbEntry.addField( field );
						entrySize++;
					}

					entries.push_back( dbEntry );
					
					if( entrySize > maxEntrySize )
					{
						maxEntrySize = entrySize;
					}

					entrySize = 0;
				}
			}

			/* Insert generic labels for any fields currently missing one */
			for( unsigned int i = labels.getNrFields(); i < maxEntrySize; i++ )
			{
				std::stringstream label;
				label << "Label_" << i;
				labels.addField( label.str() );
			}

			currentEntry = entries.begin();
			result = true;
		}

		iFile.close();

	}

	return result;
}

void CSVManager::closeDatabase()
{
	std::ofstream oFile;

	oFile.open( baseDir + currentDb, std::ios_base::out | std::ios_base::trunc );

	if( oFile.is_open() )
	{
		/* Write labels */
		for( unsigned int i = 0; i < labels.getNrFields(); i++ )
		{
			oFile << labels.getField( i );
			if( i < labels.getNrFields() - 1 )
			{
				oFile << ';';
			}
			else
			{
				oFile << std::endl;
			}
		}

		/* Write the rest of the data */
		for( Entries::iterator it = entries.begin(); it != entries.end(); ++it )
		{
			for( unsigned int i = 0; i < labels.getNrFields(); i++ )
			{
				oFile << it->getField( i );

				if( i < labels.getNrFields() - 1 )
				{
					oFile << ';';
				}
				else
				{
					oFile << std::endl;
				}
			}
		}
	}

	oFile.close();

	entries.clear();
	labels.clear();
	currentDb.clear();
}

unsigned int CSVManager::getNrEntries()
{
	return entries.size();
}

DatabaseEntry* CSVManager::nextEntry()
{
	DatabaseEntry* result = NULL;

	if( !entries.empty() )
	{
		if( currentEntry == entries.end() )
		{
			currentEntry = entries.begin();
		}

		result = &(*currentEntry);

		++currentEntry;
	}

	return result;
}

DatabaseEntry* CSVManager::getEntry( unsigned int entry )
{
	DatabaseEntry* result = NULL;

	if( entry < entries.size() )
	{
		result = &entries.at( entry );
	}

	return result;
}

DatabaseEntry* CSVManager::findEntry( unsigned int field, std::string search )
{
	DatabaseEntry* result = NULL;

	for( Entries::iterator it = entries.begin(); it != entries.end(); ++it )
	{
		if( it->getField( field ) == search )
		{
			result = &(*it);
		}
	}

	return result;
}

std::string CSVManager::getFieldLabel( unsigned int field )
{
	return labels.getField( field );
}

bool CSVManager::setFieldLabel( unsigned int field, std::string label )
{
	return labels.setField( field, label );
}

unsigned int CSVManager::addFieldLabel( std::string label )
{
	unsigned int result = labels.getNrFields();

	labels.addField( label );

	return result;
}

std::string CSVManager::getField( unsigned int entry, unsigned int field )
{
	std::string result;

	if( entry < entries.size() )
	{
		result = entries.at( entry ).getField( field );
	}

	return result;
}

bool CSVManager::saveEntry( DatabaseEntry* entry )
{
	bool result = false;

	if( !currentDb.empty() && entry != NULL )
	{
		entries.push_back( *entry );

		for( unsigned int i = labels.getNrFields(); i < entries.back().getNrFields(); i++ )
		{
			std::stringstream label;
			label << "Label_" << i;
			labels.addField( label.str() );
		}

		result = true;
	}

	return result;
}

void CSVManager::deleteEntry( unsigned int entry )
{
	if( entry < entries.size() )
	{
		entries.erase( entries.begin() + entry );
	}
}

void CSVManager::clearEntries()
{
	entries.clear();
}
