#include "AchievementManager.h"
#include "CSVManager.h"
#include "Achievements.h"

AchievementManager::AchievementManager()
{
	dbManager = new CSVManager();
}

AchievementManager::~AchievementManager()
{
	clear();
	delete dbManager;
}

unsigned int AchievementManager::initAll()
{
	achievements[ Achievement::TYPE_UNKNOWN ]		= new Achievement();
	achievements[ Achievement::TYPE_1_HOUR ]		= new OneHourAchievement();
	achievements[ Achievement::TYPE_10_HOURS ]		= new TenHoursAchievement();
	achievements[ Achievement::TYPE_FIRST_BLOOD ]	= new FirstBloodAchievement();
	achievements[ Achievement::TYPE_1000_KILLS ]	= new ThousandKillsAchievement();
	achievements[ Achievement::TYPE_5_KILLSTREAK ]	= new FiveKillstreakAchievement();
	achievements[ Achievement::TYPE_ALL_ITEMS ]		= new AllItemsAchievement();
	achievements[ Achievement::TYPE_ALL_CORES ]		= new AllCoresAchievement();
	achievements[ Achievement::TYPE_ALL_MODULES ]	= new AllModulesAchievement();
	achievements[ Achievement::TYPE_TROLL ]			= new ZeroKillsAchievement();

	return achievements.size();
}

unsigned int AchievementManager::loadAll()
{
	unsigned int results = 0;

	if( !achievements.empty() )
	{
		clear();
	}

	if( dbManager->openDatabase( "achievements.csv" ) )
	{
		results = dbManager->getNrEntries();

		for( unsigned int i = 0; i < results; ++i )
		{
			DatabaseEntry* dbEntry = dbManager->nextEntry();

			// DatabaseEntry fields are strings only, so convert strings to ints first
			unsigned int	type			= atoi( dbEntry->getField( 0 ).c_str() );
			std::string		name			= dbEntry->getField( 1 ).c_str();
			std::string		description		= dbEntry->getField( 2 ).c_str();

			Achievement* achi				= create( type, name, description );

			if( achi != NULL )
			{
				achievements[ type ]		= achi;
			}
		}
	}
	else
	{
		/* File doesn't exist, so we create labels */
		dbManager->addFieldLabel( "Type" );
		dbManager->addFieldLabel( "Name" );
		dbManager->addFieldLabel( "Description" );
	}

	dbManager->closeDatabase();

	return results;
}

bool AchievementManager::load( unsigned int uid )
{
	bool result = false;

	if( achievements.find( uid ) == achievements.end() )
	{
		if( dbManager->openDatabase( "achievements.csv" ) )
		{
			std::stringstream search;
			search << uid;

			DatabaseEntry* dbEntry = dbManager->findEntry( 0, search.str() );

			if( dbEntry != NULL )
			{
				// DatabaseEntry fields are strings only, so convert strings to ints first
				unsigned int	type			= atoi( dbEntry->getField( 0 ).c_str() );
				std::string		name			= dbEntry->getField( 1 ).c_str();
				std::string		description		= dbEntry->getField( 2 ).c_str();

				// For now, just make a default Achievement
				Achievement* achi				= create( type, name, description );

				if( achi != NULL )
				{
					achievements[ type ]		= achi;
					result						= true;
				}
			}
		}
		else
		{
			/* File doesn't exist, so we create labels */
			dbManager->addFieldLabel( "Type" );
			dbManager->addFieldLabel( "Name" );
			dbManager->addFieldLabel( "Description" );
		}

		dbManager->closeDatabase();
	}

	return result;
}

bool AchievementManager::save( unsigned int uid )
{
	bool result = false;

	Achievements::const_iterator cit = achievements.find( uid );
	if( cit != achievements.cend() )
	{
		if( !dbManager->openDatabase( "achievements.csv" ) )
		{
			/* File doesn't exist, so we create labels */
			dbManager->addFieldLabel( "Type" );
			dbManager->addFieldLabel( "Name" );
			dbManager->addFieldLabel( "Description" );
		}

		DatabaseEntry dbEntry;
		dbEntry.addField( cit->second->type );
		dbEntry.addField( cit->second->name );
		dbEntry.addField( cit->second->description );

		result = dbManager->saveEntry( &dbEntry );

		dbManager->closeDatabase();
	}

	return result;
}

Achievement* AchievementManager::get( unsigned int uid )
{
	Achievement* result = NULL;

	Achievements::iterator it = achievements.find( uid );
	if( it != achievements.end() )
	{
		result = it->second;
	}

	return result;
}

unsigned int AchievementManager::fill( Unlocks & list )
{
	unsigned int result = 0;

	for( Achievements::const_iterator cit = achievements.begin(); cit != achievements.end(); ++cit )
	{
		if( list.find( cit->first ) == list.end() )
		{
			list[ cit->first ] = false;
			result++;
		}
	}

	return result;
}

bool AchievementManager::validate( const Achievement::InData & data )
{
	bool result = false;

	Achievements::const_iterator cit = achievements.find( data.type );
	if( cit != achievements.end() )
	{
		result = cit->second->validate( data );
	}
	else
	{
		std::cout << "Cannot find achievement type to validate." << std::endl;
	}

	return result;
}

void AchievementManager::clear()
{
	for( Achievements::iterator it = achievements.begin(); it != achievements.end(); ++it )
	{
		delete it->second;
		it->second = NULL;
	}

	achievements.clear();
}

Achievement* AchievementManager::create( unsigned int type, std::string name, std::string description )
{
	Achievement* result = NULL;

	switch( type )
	{
	case Achievement::TYPE_UNKNOWN:

		result = new Achievement( name, description );
		break;

	case Achievement::TYPE_1_HOUR:

		result = new OneHourAchievement( name, description );
		break;

	case Achievement::TYPE_10_HOURS:

		result = new TenHoursAchievement( name, description );
		break;

	case Achievement::TYPE_FIRST_BLOOD:

		result = new FirstBloodAchievement( name, description );
		break;

	case Achievement::TYPE_1000_KILLS:

		result = new ThousandKillsAchievement( name, description );
		break;

	case Achievement::TYPE_5_KILLSTREAK:

		result = new FiveKillstreakAchievement( name, description );
		break;

	case Achievement::TYPE_ALL_ITEMS:

		result = new AllItemsAchievement( name, description );
		break;

	case Achievement::TYPE_ALL_CORES:

		result = new AllCoresAchievement( name, description );
		break;

	case Achievement::TYPE_ALL_MODULES:

		result = new AllModulesAchievement( name, description );
		break;

	case Achievement::TYPE_TROLL:

		result = new ZeroKillsAchievement( name, description );
		break;

	default:

		std::cout << "Invalid achievement type." << std::endl;
		break;
	}

	return result;
}
