
#ifndef SERVER_LIST_H
#define SERVER_LIST_H

#include "Common.h"

#include "ServerInfo.h"

class ServerList
{
	typedef std::vector < ServerInfo > Servers;

public:

	void registerEntry(const ServerInfo & server);
	bool dropEntry(const std::string & ip);
	bool findEntry(const std::string & ip);

	char * getDataPointer();
	unsigned int getDataSize();

	unsigned int getNumberOfEntries() const;

private:

	Servers servers;
};

#endif