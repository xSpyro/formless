#ifndef ACCOUNT_MANAGER__H
#define ACCOUNT_MANAGER__H

#include "Common.h"
#include "Account.h"
#include "Achievement.h"
#include "AchievementList.h"

class DatabaseManager;
class ItemManager;
class AchievementManager;
class Master;

#define ITEM_REWARD_INTERVAL 60

class AccountManager
{
	typedef std::map< unsigned int, Account > Accounts;
	typedef std::map< unsigned int, bool > Unlocks; // Used for achievement unlocks, mirrors the map in Account

public:
	AccountManager( ItemManager* itemManager, AchievementManager* achManager, Master* master );
	~AccountManager();

	void				update();

	unsigned int		loadAllAccounts();	// Loads all accounts and their inventories. Returns number of accounts loaded

	bool				loadAccount( unsigned int uid );
	bool				saveAccount( unsigned int uid );
	bool				updateAccount( unsigned int uid );

	bool				loadInventory( unsigned int uid, bool companion = false );
	bool				saveInventory( unsigned int uid, bool companion = false );

	bool				loadAchievements( unsigned int uid );
	bool				saveAchievements( unsigned int uid );
	bool				validateAchievement( unsigned int uid, Achievement::InData & data );

	bool				loadProfile( unsigned int uid, unsigned int pid, ProfileList& items );
	bool				saveProfile( unsigned int uid, unsigned int pid, ProfileList& items );

	unsigned int		addItem( unsigned int uid, ItemInfo& item );
	bool				moveItem( unsigned int uid, unsigned int itemid, unsigned int slotX, unsigned int slotY );

	ItemInfo			getRewardItem( unsigned int uid );
	Achievement			getRewardAchievement( unsigned int uid );
	unsigned int		getAge( unsigned int uid );
	unsigned int		getNextItemAt( unsigned int uid );
	bool				allItemsUnlocked( unsigned int uid );
	unsigned int		getUnlockedCores( unsigned int uid );
	unsigned int		getUnlockedModules( unsigned int uid );
	unsigned int		getUnlockedItems( unsigned int uid );
	unsigned int		getValidCores();
	unsigned int		getValidModules();
	unsigned int		getValidItems();

	bool				addAccount( std::string username, std::string password );
	unsigned int		authorize( std::string username, std::string password, std::string ip );	// Returns user id if succeeded, 0 otherwise
	void				signoff( unsigned int uid, std::string ip );
	unsigned int		getOnlineStatus( unsigned int uid );
	bool				getInventory( unsigned int uid, ItemList& list );
	bool				getCompanionInventory( unsigned int uid, ItemList& list );
	bool				getAchievements( unsigned int uid, AchievementList& list );
	std::string			getUsername( unsigned int uid );
	
	void				startPlayTimer( unsigned int uid );
	void				endPlayTimer( unsigned int uid );

	std::string			getServer( unsigned int uid );
	void				setServer( unsigned int uid, const std::string & server );
	void				serverDead( const std::string & server );
	void				addKills( unsigned int uid, unsigned int kills );

private:
	unsigned int		awardItem( unsigned int uid );
	bool				findItem( ItemList& list, ItemInfo& item );
	void				fillAchievementData( unsigned int uid, Achievement::InData & data );
	void				calculateAchievements( unsigned int uid );
	void				updateCounter();

private:
	DatabaseManager*	dbManager;
	ItemManager*		itemManager;
	AchievementManager* achManager;
	Accounts			accounts;
	unsigned int		nextID;

	Master*				master;
};

#endif
