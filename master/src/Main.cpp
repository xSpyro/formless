
#include "Master.h"

int main()
{
	boost::asio::io_service service;

	Master master(service);
	
	if (master.startup())
	{
		return master.run();
	}
	
	return 0;
}