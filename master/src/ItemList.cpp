#include "ItemList.h"

//void ItemList::registerEntry(const ServerInfo & server)
//{
//	servers.push_back(server);
//}
//
//bool ItemList::dropEntry(const std::string & ip)
//{
//	for (Servers::iterator i = servers.begin(); i != servers.end(); ++i)
//	{
//		if (std::string(i->getIp()) == ip)
//		{
//			servers.erase(i);
//			return true;
//		}
//	}
//
//	return false;
//}

void ItemList::registerItem( const ItemInfo & item )
{
	items.push_back( item );
}

void ItemList::clear()
{
	items.clear();
}

char * ItemList::getDataPointer()
{
	if (items.empty())
		return NULL;

	return reinterpret_cast < char* > (&items.front());
}

unsigned int ItemList::getDataSize()
{
	return items.size() * sizeof(ItemInfo);
}

unsigned int ItemList::getNumberOfEntries() const
{
	return items.size();
}
