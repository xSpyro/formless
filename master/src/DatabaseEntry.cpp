#include "DatabaseEntry.h"

DatabaseEntry::DatabaseEntry()
{
}

DatabaseEntry::~DatabaseEntry()
{
	clear();
}

void DatabaseEntry::addField( std::string data )
{
	fields.push_back( data );
}

void DatabaseEntry::addField( unsigned int data )
{
	std::stringstream str;
	str << data;
	fields.push_back( str.str() );
}

bool DatabaseEntry::setField( unsigned int id, std::string data )
{
	bool result = false;

	if( id < fields.size() )
	{
		fields.at( id ) = data;
		result = true;
	}

	return result;
}

bool DatabaseEntry::setField( unsigned int id, unsigned int data )
{
	bool result = false;

	if( id < fields.size() )
	{
		std::stringstream str;
		str << data;
		fields.at( id ) = str.str();
		result = true;
	}

	return result;
}

std::string DatabaseEntry::getField( unsigned int id )
{
	std::string result;

	if( id < fields.size() )
	{
		result = fields.at( id );
	}

	return result;
}

unsigned int DatabaseEntry::getNrFields()
{
	return fields.size();
}

void DatabaseEntry::clear()
{
	fields.clear();
}
