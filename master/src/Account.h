
#ifndef ACCOUNT_H
#define ACCOUNT_H

#include "Inventory.h"
//#include "ItemList.h"
#include "ProfileList.h"
#include "Achievement.h"

class Account
{
public:

	std::string		username;
	std::string		password;

	std::string		currentIp;
	std::string		currentServer;

	unsigned int	uid;
	unsigned int	age;
	unsigned int	nextItemAt;
	unsigned int	onlineStatus;
	unsigned int	lifetimeKills;

	Inventory		inventory;
	Inventory		companionInventory;

	time_t			timer;
	bool			ingame;

	std::map< unsigned int, bool >	achievements;
	std::vector<Achievement>		achievementRewards;

	std::vector<ItemInfo>			itemRewards;
};

#endif