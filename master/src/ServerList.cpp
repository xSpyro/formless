
#include "ServerList.h"

void ServerList::registerEntry(const ServerInfo & server)
{
	servers.push_back(server);
}

bool ServerList::dropEntry(const std::string & ip)
{
	for (Servers::iterator i = servers.begin(); i != servers.end(); ++i)
	{
		if (std::string(i->getIp()) == ip)
		{
			servers.erase(i);
			return true;
		}
	}

	return false;
}

bool ServerList::findEntry(const std::string & ip)
{
	for (Servers::iterator i = servers.begin(); i != servers.end(); ++i)
	{
		if (std::string(i->getIp()) == ip)
		{
			return true;
		}
	}

	return false;
}

char * ServerList::getDataPointer()
{
	if (servers.empty())
		return NULL;

	return reinterpret_cast < char* > (&servers.front());
}

unsigned int ServerList::getDataSize()
{
	return servers.size() * sizeof(ServerInfo);
}

unsigned int ServerList::getNumberOfEntries() const
{
	return servers.size();
}