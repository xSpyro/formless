
#ifndef SUBSYSTEM_H
#define SUBSYSTEM_H

#include "Common.h"
#include "Logger.h"
class Component;
class GameHandler;

/*
 *	Base class in the Subsystem hierarchy. Interface!
 *	Holds all components of the right type. The vector is not specified
 *	until further down in the hierarchy, since the components need to be
 *	of the right class type.
 */

class Subsystem
{
public:
	Subsystem();
	Subsystem(GameHandler* gameHandler);
	Subsystem(GameHandler* gameHandler, const std::string loggername);
	virtual ~Subsystem();

	virtual Component*	createComponent(EventLua::Object script) = 0;
	virtual void		update() = 0;
	virtual void		clear() { }
	virtual std::string	getDebugString() const { return "no debug"; }

	GameHandler*		getGameHandler();
	Logger*				getLogger();

	

protected:
	Logger				log;
	GameHandler*		game;
	
};

#endif