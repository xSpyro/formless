
#ifndef SCENE_H
#define SCENE_H

#include "Common.h"

class LogicHandler;
class Renderer;
class Object;
class Window;
struct AccountPacket;

class Scene
{
public:

	Scene();
	Scene(LogicHandler* handler, Renderer* rend, Window* wndw);
	virtual ~Scene();

	virtual void update() = 0;
	virtual void render() = 0;

	virtual void onEnter();							// Defines what should be done upon entering the scene
	virtual void onLeave();							// Defines what should be done upon leaving the scene
	virtual void onLoad();							// Defines what happens after a load

	virtual bool onInput(int sym, int state);		// If return false give input to eventHandler
	virtual bool onMouseMovement(int x, int y);		// If return false give input to eventHandler

	virtual bool onCreateObject(Object* object) { return false; };
	virtual bool onDestroyObject(Object* object) { return false; };
	virtual void onKillObject(Object* killed, Object* killer) { };

	virtual void onConnected(unsigned int id);

	virtual void onFocus();
	virtual void onFocusLost();

	virtual void onSkillEffect(float damage, bool stun, bool link, bool outOfMana, int killStreak, float x, float y, float z);
	virtual void onAccountUpdate(AccountPacket* packet);

protected:

	virtual void buildLayers() { };
	virtual void clearLayers() { };

	LogicHandler*	parent;
	Renderer*		renderer;
	Window*			window;

	int				mx;
	int				my;
};

#endif