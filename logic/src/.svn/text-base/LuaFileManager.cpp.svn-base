#include "LuaFileManager.h"
#include "../../formats/src/LabelEnum.h"
#include <iostream>

LuaFileManager::LuaFileManager()
{}

LuaFileManager::~LuaFileManager()
{}

bool LuaFileManager::addFile(const std::string & name, File & file, LuaType type, bool overwrite /* = false */)
{
	if (overwrite || !exists(name, type))
	{
		copyFile(find(name, type), file);

		return true;
	}
	return false;
}

bool LuaFileManager::exists(const std::string & name, LuaType type)
{
	for( Types::iterator it = files.begin(); it != files.end() ; ++it)
	{
		if (type & it->first || type == TYPE_UNKOWN || it->first == TYPE_UNKOWN)
		{
			if (it->second[name].size != 0)
			{
				return true;
			}
		}
	}
	return false;
}

bool LuaFileManager::getFile(const std::string & name, File & file, LuaType type)
{
	bool rtn;
	copyFile(file, find(name, type, rtn));
	return rtn;
}

bool LuaFileManager::getFiles(StringVector & out, LuaType type)
{
	if( !files.empty())
	{
		for( Types::iterator it = files.begin(); it != files.end() ; ++it)
		{
			if (type & it->first || type == it->first)
			{
				for (Files::iterator it2 = it->second.begin(); it2 != it->second.begin(); ++it2)
				{
					out.push_back(it2->first);
				}
			}
		}

		return true;
	}
	return false;
}

bool LuaFileManager::getFiles(FileIds & out, LuaType type /* = TYPE_UNKOWN */)
{
	if( !files.empty())
	{
		for( Types::iterator it = files.begin(); it != files.end() ; ++it)
		{
			if (type & it->first || type == TYPE_UNKOWN || it->first == TYPE_UNKOWN)
			{
				for (Files::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
				{
					
					out.push_back(FileId(it2->first, it->first));
				}
			}
		}

		return true;
	}
	return false;
}

bool LuaFileManager::rename(const std::string & oldName, const std::string & newName, LuaType type /* = TYPE_UNKOWN */)
{
	if (exists(newName, type))
		return false;
	if (exists(oldName, type))
	{
		copyFile(find(newName, type), find(oldName, type));
		remove(oldName, type);
		return true;
	}
	return false;
}

bool LuaFileManager::remove(const std::string & name, LuaType type)
{
	bool found;
	File * f = &find(name, type, found);
	if (found)
	{
		delete [] f->data;
		f->size = 0;
		return true;
	}
	return false;
}

bool LuaFileManager::load(std::istream & f)
{
	LabelEnum label;
	std::string name = "";
	LuaType type;
	while(!f.eof())
	{
		type = TYPE_UNKOWN;
		f.read((char *)&label, sizeof(LabelEnum));
		switch (label) // TODO: enforce ordering(i.e. remove the switch)
		{
		case LABEL_NAME:
			if (!loadString(f, name))
			{
				// TODO: Log error
				return false;
			}
			f.read((char *)&label, sizeof(LabelEnum));
			if (label == LABEL_TYPE)
			{
				f.read((char *)&type, sizeof(LuaType));
				f.read((char *)&label, sizeof(LabelEnum));
			}
			if (label != LABEL_DATA)
			{
				// TODO: Log error
				return false;
			}
			{
				File * file = new File;
				f.read((char *)&file->size, sizeof(int));
				file->data = new char[file->size];
				f.read((char *)file->data, sizeof(char) * file->size);

				if (!addFile(name, *file, type))
				{
					if (exists(name, type))
						;// TODO: Log warning
					else
					{
						delete file;
						// TODO: log error
						return false;
					}
				}
				else
				{
					std::cout << "FOUND NEW FILE!!!! YEAH!!! " << name << "\n";
				}
				delete file;
			}
			break;
		case LABEL_END:
			return true;
		default:
			return false;
			break;
		}
	}
	return false;
}

bool LuaFileManager::save(std::ostream & f)
{
	LabelEnum label = LABEL_LUA_FILES;
	f.write((char*)&label, sizeof(label));
	std::cout << "size : " << files.size() << std::endl;
	FileVector files;
	getFileVector(files);

	for (FileVector::iterator it = files.begin(); it != files.end(); ++it)
	{
		label = LABEL_NAME;
		f.write((char *)&label, sizeof(LabelEnum));
		int size = it->first.name.size();
		f.write((char *)&size, sizeof(int));
		f.write((char *)it->first.name.c_str(), sizeof(char) * size);

		label = LABEL_TYPE;
		f.write((char *)&label, sizeof(LabelEnum));
		f.write((char *)&it->first.type, sizeof(LuaType));

		File * file = it->second;
		label = LABEL_DATA;
		f.write((char *)&label, sizeof(LabelEnum));
		f.write((char *)&file->size, sizeof(int));
		f.write((char *)file->data, sizeof(char) * file->size);
	}
	label = LABEL_END;
	f.write((char *)&label, sizeof(LabelEnum));
	return true;
}

bool LuaFileManager::loadString(std::istream & f, std::string & string)
{
	int size = 0;
	f.read((char *)&size, sizeof(int));
	if (size >= 0)
	{
		char * c = new char[size + 1];
		f.read(c, sizeof(char) * size);
		c[size] = '\0';
		string = c;
		delete [] c;
		return true;
	}
	return false;
}

void LuaFileManager::getFileVector(FileVector & f, LuaType type)
{
	for( Types::iterator it = files.begin(); it != files.end() ; ++it)
	{
		if (type & it->first || type == TYPE_UNKOWN || it->first == TYPE_UNKOWN)
		{
			for( Files::iterator it2 = it->second.begin(); it2 != it->second.end() ; ++it2) 
				f.push_back(FileWId( FileId(it2->first, it->first), &it2->second));
		}
	}
}

File & LuaFileManager::find(std::string name, LuaType type)
{
	bool ignore;
	return find(name, type, ignore);
}

File & LuaFileManager::find(std::string name, LuaType type, bool & success)
{
	File * found = 0;
	for( Types::iterator it = files.begin(); it != files.end() ; ++it)
	{
		if (type & it->first || type == TYPE_UNKOWN || it->first == TYPE_UNKOWN)
		{
			if (it->second[name].size != 0)
			{
				success = true;
				return it->second[name];
			}
		}
	}
	success = false;
	return files[type][name];
}

void LuaFileManager::copyFile(File & dst, File & src)
{
	if (dst.data)
		delete [] dst.data;

	dst.size = src.size;
	dst.data = new char[dst.size];

	memcpy(dst.data, src.data, sizeof(char) * src.size);
}