
#include "Scene.h"
#include "LogicHandler.h"

Scene::Scene()
{
	parent		= NULL;
	renderer	= NULL;
}

Scene::Scene(LogicHandler* handler, Renderer* rend, Window* wndw)
{
	parent		= handler;
	renderer	= rend;
	window		= wndw;
}

Scene::~Scene()
{

}

void Scene::onEnter()
{

}

void Scene::onLeave()
{

}

void Scene::onLoad()
{

}

bool Scene::onInput(int sym, int state)
{
	return false;
}

bool Scene::onMouseMovement(int x, int y)
{
	mx = x;
	my = y;

	return false;
}

void Scene::onConnected(unsigned int id)
{

}

void Scene::onFocus()
{

}

void Scene::onFocusLost()
{

}

void Scene::onSkillEffect(float damage, bool stun, bool link, bool outOfMana, int killStreak, float x, float y, float z)
{

}

void Scene::onAccountUpdate(AccountPacket* packet)
{

}