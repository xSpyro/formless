
#include "Subsystem.h"
#include "Component.h"

Subsystem::Subsystem()
	: log("Subsystem")
{
	game = NULL;
}

Subsystem::Subsystem(GameHandler* gameHandler)
	: log("Subsystem")
{
	game = gameHandler;
}

Subsystem::Subsystem(GameHandler* gameHandler, const std::string loggername)
	: log(loggername)
{
	game = gameHandler;
}

Subsystem::~Subsystem()
{
}

GameHandler* Subsystem::getGameHandler()
{
	return game;
}

Logger* Subsystem::getLogger()
{
	return &log;
}