
#ifndef SCREEN_H
#define SCREEN_H

#include "Common.h"
class Scene;

/*
 *	Screen is a simple container to all the current active scenes.
 *	It's the logic handlers assistant to keep track of what scenes
 *	currently exists and what scenes are activated.
 */

class Screen
{
	typedef std::vector<Scene*> Scenes;

public:

	Screen();
	~Screen();

	void	update();
	void	render();
	void	addScene(Scene* scene);			// Adds scene to screen and calls onEnter()
	bool	removeScene(Scene* scene);		// Removes scene from screen and calls onLeave()
	void	removeAllScenes();
	Scenes*	getAllScenes();

	bool	onInput(int sym, int state);	// If return false give input to eventHandler
	bool	onMouseMovement(int x, int y);	// If return false give input to eventHandler

private:

	Scenes scenes;

};

#endif