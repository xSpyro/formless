#ifndef TIMER_EVENT_H
#define TIMER_EVENT_H
#include "Event.h"

class TimerEvent : public Event
{
public:
	TimerEvent();
	TimerEvent(int identity);
	virtual ~TimerEvent();

	virtual void	sendTo(Object * to);

	int				getID();

	void			setID(int identity);

protected:
	int id;
};

#endif