
#include "Screen.h"
#include "Scene.h"

Screen::Screen()
{
}

Screen::~Screen()
{
	scenes.clear();
}

void Screen::update()
{
	scenes.back()->update();
}

void Screen::render()
{
	for(Scenes::iterator it = scenes.begin(); it != scenes.end(); ++it)
	{
		(*it)->render();
	}
}

void Screen::addScene(Scene* scene)
{
	if(!scenes.empty())
		scenes.back()->onFocusLost();
	scenes.push_back(scene);
	scenes.back()->onEnter();
}

bool Screen::removeScene(Scene* scene)
{
	bool found = false;

	for(Scenes::iterator it = scenes.begin(); it != scenes.end() && !found; ++it)
	{
		if((*it) == scene)
		{
			(*it)->onLeave();
			scenes.erase(it);
			if(!scenes.empty())
				scenes.back()->onFocus();
			found = true;
			break;
		}
	}

	return found;
}

void Screen::removeAllScenes()
{
	for(Scenes::iterator it = scenes.begin(); it != scenes.end(); ++it)
	{
		(*it)->onLeave();
	}
	scenes.clear();
}

std::vector<Scene*> * Screen::getAllScenes()
{
	return &scenes;
}

bool Screen::onInput(int sym, int state)
{
	if (scenes.empty())
		return false;

	return scenes.back()->onInput(sym, state);
}

bool Screen::onMouseMovement(int x, int y)
{
	if (scenes.empty())
		return false;

	return scenes.back()->onMouseMovement(x, y);
}