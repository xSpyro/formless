#include "TimerEvent.h"
#include "EventEnum.h"
#include "Object.h"

TimerEvent::TimerEvent()
	: Event(EVENT_TIMER)
{
	id = 0;
}

TimerEvent::TimerEvent(int identity) 
	: Event(EVENT_TIMER)
{
	id = identity;
}

TimerEvent::~TimerEvent()
{
}

void TimerEvent::sendTo(Object * to)
{
	to->receive<TimerEvent>(this);
}

int TimerEvent::getID()
{
	return id;
}

void TimerEvent::setID(int identity)
{
	id = identity;
}