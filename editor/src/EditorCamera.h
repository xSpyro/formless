#ifndef EDITORCAMERA_H
#define EDITORCAMERA_H

#include "Common.h"

class EditorCamera : public Camera
{
public:
	EditorCamera();
	~EditorCamera();

	// Euler rotation
	void rotate(const float dx, const float dy, const float dz);
	// This rotates around a given point
	void rotate(const float dtheta, const float dphi);
	
	void updateAsShapeCamera();
	void updateAsLevelCamera();

	void adjustRadius(const float dradius);

	void setRadius(const float rad);

	void setTheta(const float theta);
	void setPhi(const float phi);

	void setRotation(const float x, const float y, const float z);
	void setRotation(const Vector3& rotation);

	const float getTheta() const;
	const float getPhi() const;

	const Vector3 getRotation() const;

	const float getRadius() const;

private:

	Matrix rotation;
	Matrix translation;
	float theta;
	float phi;
	
	// The distance to the point rotating around
	float radius;

	// Point we rotating around
	Vector3 lookat;
};

#endif