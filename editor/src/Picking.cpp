#include "Picking.h"

#include "PhysicsComponent.h"
#include "PositionEvent.h"
#include "QueryEvent.h"

Picking::Picking()
{
	marked = NULL;
}

Picking::~Picking()
{
}


void Picking::update(const Vector2& mouse, Camera* const camera, const bool pickMany)
{
	camera->update();
	// Fetch the matrix for the object and
	// translate it according to translation
	// create ray
	Ray ray;
	camera->pick(mouse,&ray);

	// create plane from the objects position and the cameras orientation
	Plane plane;

	Vector3 normal = camera->getOrientation();
	Vector3 position, oldPosition;

	QueryEvent<Vector3> qe = QueryEvent<Vector3>(QUERY_POSITION);
	marked->send( &qe );

	position = qe.getData();
	
	oldPosition = position;
	D3DXPlaneFromPointNormal((D3DXPLANE*)&plane, (D3DXVECTOR3*)&position, (D3DXVECTOR3*)&normal);

	Vector3 direction = Vector3(0,0,0);

	IntersectResult res;
	// intersect with the plane to see where we shall translate the object
	if(Intersect::intersect(ray, plane, &res))
	{
		PositionEvent ev;
		direction = ray.origin + (ray.direction * res.dist);
		ev.setPosition(direction);
		marked->send(&ev);
	}

	position = direction;

	direction = position - oldPosition;

	if(pickMany)
	{
		moveMany(direction);
	}
	
	//for (unsigned int i = 0; i < prev.size(); ++i)
	//{
	//	Object* object = (Object*)prev[i]->getParent()->getUserdata();

	//	if (object == marked)
	//		continue;

	//	PositionEvent ev;
	//	
	//	
	//	qe = QueryEvent<Vector3>(QUERY_POSITION);
	//	object->send( &qe );

	//	position = qe.getData();

	//	ev.setPosition(position + direction);
	//	object->send(&ev);
	//}

}

void Picking::moveMany( const Vector3& direction)
{
	for (unsigned int i = 0; i < prev.size(); ++i)
	{
		Object* object = (Object*)prev[i]->getParent()->getUserdata();

		if (object == marked)
			continue;

		PositionEvent ev;
		
		QueryEvent<Vector3> qe = QueryEvent<Vector3>(QUERY_POSITION);
		object->send( &qe );

		Vector3 position = qe.getData();

		ev.setPosition(position + direction);
		object->send(&ev);
	}
}

void Picking::pickObject(Layer* layer, const Vector2& mouse, Camera* const camera)
{
	camera->update();
	// create ray
	Ray ray;
	camera->pick(mouse,&ray);

	// pick target with ray
	Renderable* rend = (Renderable*)layer->pick(ray, NULL);
	if(rend)
	{
		marked = (Object*)rend->getParent()->getUserdata();

		Material mat;
		mat.blend = Color(1.0f, 0.0f, 0.0f, 1.0f);

		rend->setMaterial(&mat);

		selected[rend] = rend;

		for (unsigned int i = 0; i < prev.size(); i++)
		{
			if (rend == prev[i])
				return;
		}
		prev.push_back(rend);
		
	}
	else
	{
		marked = NULL;
	}
}

void Picking::pickObject(Layer* layer, const Vector2& first, const Vector2& second, Camera* const camera)
{
	Vector2 min(0,0), max(0,0);
	
	// Get the min max points for the max from the two points: first and second
	if(first.x <= second.x)
	{
		min.x = first.x;
		max.x = second.x;
	}
	else if(first.x > second.x)
	{
		min.x = second.x;
		max.x = first.x;
	}

	if(first.y <= second.y)
	{
		min.y = first.y;
		max.y = second.y;
	}
	else if (first.y > second.y)
	{
		min.y = second.y;
		max.y = first.y;
	}

	// check which of the nodes who are inside the box

	std::vector<const Renderable*> renderables;

	layer->getRenderables(renderables);

	for(unsigned int i = 0; i < renderables.size(); i++)
	{
		Renderable* renderable = (Renderable*)renderables[i];
		Object* object = (Object*)renderable->getParent()->getUserdata();

		Vector3 position = Vector3(0,0,0);
		PhysicsComponent* comp = object->findComponent<PhysicsComponent>();

		if(comp)
		{
			position = comp->position;
		}


		D3DXVECTOR4 screen_pos = D3DXVECTOR4(0,0,0,1);


		Matrix m;
		D3DXMatrixIdentity((D3DXMATRIX*)&m);
		
		m = camera->getViewProjectionMatrixNonTransposed();
		
		D3DXVec3Transform(&screen_pos, (D3DXVECTOR3*)&position, (D3DXMATRIX*)&m);
	
		screen_pos.x = (screen_pos.x)/(screen_pos.w);
		screen_pos.y = (screen_pos.y)/(screen_pos.w);
		screen_pos.z = (screen_pos.z)/(screen_pos.w);

		
		// if object is inside the box
		if((screen_pos.x > min.x && screen_pos.y > min.y ) && (screen_pos.x < max.x && screen_pos.y < max.y))
		{

			Material mat;
			mat.blend = Color(1.0f, 0.0f, 0.0f, 1.0f);

			renderable->setMaterial(&mat);
			
			prev.push_back(renderable);
			selected[renderable] = renderable;
		}

	}

}

void Picking::pickObject(std::vector<Object*>& objects, const Vector2& first, const Vector2& second, Camera* const camera)
{
	Vector2 min(0,0), max(0,0);

	// Get the min max points for the max from the two points: first and second
	if(first.x <= second.x)
	{
		min.x = first.x;
		max.x = second.x;
	}
	else if(first.x > second.x)
	{
		min.x = second.x;
		max.x = first.x;
	}

	if(first.y <= second.y)
	{
		min.y = first.y;
		max.y = second.y;
	}
	else if (first.y > second.y)
	{
		min.y = second.y;
		max.y = first.y;
	}

	int counter = 0;
	for(unsigned int i = 0; i < objects.size(); i++)
	{
		Object* object = objects[i];

		Vector3 position = Vector3(0,0,0);
		PhysicsComponent* comp = object->findComponent<PhysicsComponent>();

		if(comp)
		{
			position = comp->position;
		}


		D3DXVECTOR4 screen_pos = D3DXVECTOR4(0,0,0,0);


		Matrix m;
		D3DXMatrixIdentity((D3DXMATRIX*)&m);

		m = camera->getViewProjectionMatrix();

		D3DXVec3Transform(&screen_pos, (D3DXVECTOR3*)&position, (D3DXMATRIX*)&m);
		
		screen_pos.x = (screen_pos.x)/(screen_pos.w);
		screen_pos.y = (screen_pos.y)/(screen_pos.w);
		screen_pos.z = (screen_pos.z)/(screen_pos.w);

		// if object is inside the box
		
		
		if((screen_pos.x > min.x && screen_pos.x < max.x ) && (screen_pos.y > min.y && screen_pos.y < max.y))
		{
			
			std::cout << "MinMax:" << min.x << " " << min.y << "   " << max.x << " " << max.y << std::endl;
			std::cout << "ScreenPos:" << screen_pos.x << " " << screen_pos.y << " " << screen_pos.z << std::endl;
			std::cout << "Position" << position.x << " " << position.y << " " << position.z << std::endl;
			Material mat;

			mat.blend = Color(1.0f, 0.0f, 0.0f, 1.0f);


			/*renderable->setMaterial(&mat);

			prev.push_back(renderable);
		*/

			counter++;
		}
	}

	std::cout << counter << " objects" << std::endl;
}

const bool Picking::isObjectMarked() const
{
	return marked != NULL;
}

Object* Picking::getMarked() const
{
	return marked;
}

void Picking::unpick()
{
	marked = NULL;
}


void Picking::clearPicking()
{
	for(unsigned int i = 0; i < prev.size(); i++)
	{

		Material mat;
		mat.blend = Color(1.0f, 1.0f, 1.0f, 1.0f);

		prev[i]->setMaterial(&mat);
	}
	prev.clear();
}


void Picking::clearSelected()
{
	selected.clear();
}


void Picking::fetchLinks(std::vector<Object*>& obj)
{
	for(unsigned int i = 0; i < prev.size(); i++)
	{
		obj.push_back((Object*)prev[i]->getParent()->getUserdata());
	}
}

void Picking::fetchSelected(std::vector<Object*>& obj)
{
	for(RenderableMap::iterator it = selected.begin(); it != selected.end(); ++it)
	{
		obj.push_back((Object*)it->first->getParent()->getUserdata() );
	}

}


void Picking::fetchRenderables(std::vector<Renderable*>& ren)
{	
	for(unsigned int i = 0; i < prev.size(); i++)
	{
		ren.push_back(prev[i]);
	}
}

bool Picking::hasSelected()
{
	if(selected.size() > 0)
	{
		return true;
	}

	return false;
}

bool Picking::hasPreviousSelected()
{
	if (prev.size() > 0)
	{
		return true;
	}

	return false;
}