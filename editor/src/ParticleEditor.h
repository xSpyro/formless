#ifndef PARTICLEEDITOR_H

#define PARTICLEEDITOR_H

/**
 * ParticleAnimation includes Editor, and ParticleCustomBehavior
 */

/** 
 * TODO:
 *
 *
 *
 * G�r en controller som abstrakterar alla 
 * funktioner.
 *
 *
 *
 *
 *
 */

// Inneh�ller alla includes
#include "ParticleEditorController.h"

//enum Movement
//{
//	MOVE_LEFT = 1,
//	MOVE_RIGHT = 2,
//    MOVE_UP = 3,
//    MOVE_DOWN = 4
//};



class ParticleEditor : public Editor
{
public:
	ParticleEditor();
	~ParticleEditor();

	bool startup(void * window, Renderer* renderer);
	bool shutdown();

	void update();

	void runFrame();

	void* getRenderTarget();

	void onInput(int key, int state);
	void onMouseMovement(int x, int y);

	void onCreateObject(Object * object);
	void onEnter();

	void ReceiveMessage(int id, int message);

	void ReceiveMessage(int id, std::string& message);
	void ReceiveMessage(int id, float message);
	void ReceiveMessage(int id, std::string& name, std::string& message);
	bool addEmitter();

	/** 
	 * All functions makes changes on current_animation.
	 */


	//void setCurrentAnimation(int id);
	//void setCurrentEmitter(int id);
	//void spawnEmitter(int id);



public:

	void addType(std::string type, std::string texture);

	void deleteEmitter(ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * e);

	//void addEmitter();

	/**
	 * Loop animation is exactly the same as play, except life is unlimited for emitter.
	 */
	void initEmitter(ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * p);

	void loopAnimation();
	void playAnimation();

	void playSelectedEmitter();
	void playSelectedAnimation();

	void removeEmitter(int id);
	void removeAnimation(int id);

	/**
	 * Play all animations.
	 */
	void playAllAnimations();

	//void newAnimation();
	void addAnimation();


	/** 
	 * Set functions used to set attributes
	 * for current emitter in current animation.
	 */

	float getRandAngleX();
	float getRandAngleY();
	float getRandAngleZ();
		
	float getSpawnBoxX();
	float getSpawnBoxY();
	float getSpawnBoxZ();

	int getLife();
	float getIntensityDir();
	float getDiffusion();

	float getSpinMin();
	float getSpinMax();

	float getSpawnRate();
	float getAffectRate();
	float getStartScale();
	float getEndScale();

	float getStartSpeed();
	float getMiddleSpeed();
	float getEndSpeed();
	float getColorPath();
	float getSpeedPath();
	float getGravity();
	float getDistorsion();
	float getDisplacementPos();
	float getLifeAffectRate();

	float getStartColorR();
	float getStartColorG();
	float getStartColorB();

	float getMiddleColorR();
	float getMiddleColorG();
	float getMiddleColorB();

	float getEndColorR();
	float getEndColorG();
	float getEndColorB();

	int getNrEmitters();
	int getNrAnimations();

	//void addType(std::string file);

	bool saveToFile(std::string file);
	bool loadAnimation(std::string file);


private:

	void addTextureRenderable(std::string file);

	//typedef std::vector< ParticleAnimation > ParticleAnimationVector;
	typedef std::vector< std::string > TypeNameVector;
	typedef std::vector< Renderable * > RenderableVector;
	RenderableVector renderables;

	TypeNameVector typeNames;

	int selected_typeID;

	UINT particle_id;

	RenderModule renderMod;

	Logger ps_log;

	// Tools
	Picking picking;
	ParticleSystem psystem;

	Camera camera;

	Model* grid;

	Layer* gridlayer;
	Viewport* gridview;

	std::string type_name;

	EditorCamera * ecamera;

	ParticleEditorController effects;

	Mesh * basic_mesh;
	Mesh * heat_mesh;

	Renderable * basic_renderable;
	Renderable * heat_renderable;

};




#endif