#include "AddNode.h"

AddNode::AddNode(Editor* ed)
	: editor(ed),
	position(0.0f, 0.0f, 0.0f), id(0)
{
}

AddNode::~AddNode()
{

}

void AddNode::undo()
{
	RemoveNode action(editor);

	action.setBranches(branches);
	action.setId(id);
	action.setPosition(position);

	action.redo();
}

void AddNode::redo()
{
	// send action to editor
	editor->tellAddNodeAction(this);
}

void AddNode::setBranches(const std::vector<int>& br)
{
	for(unsigned int i = 0; i < br.size(); i++)
	{
		branches.push_back(br[i]);
	}
}

void AddNode::setPosition(const Vector3& position)
{
	this->position = position;
}

void AddNode::setId(const int id)
{
	this->id = id;
}

const Vector3 AddNode::getPosition() const
{
	return position;
}

const int AddNode::getId() const
{
	return id;
}

const std::string AddNode::toString()
{
	return "";
}