#include "RemoveNode.h"


RemoveNode::RemoveNode(Editor* const ed)
	: editor(ed),
	position(0.0f, 0.0f, 0.0f), id(0)
{

}

RemoveNode::~RemoveNode()
{

}

void RemoveNode::undo()
{
	AddNode action(editor);

	action.setBranches(branches);
	action.setId(id);
	action.setPosition(position);

	action.redo();
}

void RemoveNode::redo()
{
	editor->tellRemoveNodeAction(this);
}

void RemoveNode::setBranches(const std::vector<int>& br)
{
	for(unsigned int i = 0; i < br.size(); i++)
	{
		branches.push_back(br[i]);
	}
}

void RemoveNode::setPosition(const Vector3& position)
{
	this->position = position;
}

void RemoveNode::setId(const int id)
{
	this->id = id;
}

const Vector3 RemoveNode::getPosition() const
{
	return position;
}

const int RemoveNode::getId() const
{
	return id;
}

const std::string RemoveNode::toString()
{
	return "";
}
