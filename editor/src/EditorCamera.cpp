#include "EditorCamera.h"


EditorCamera::EditorCamera() : Camera()
{
	
	pitch_angle = 0.0f;
	yaw_angle = 0.0f;
	roll_angle = 0.0f;

	radius = 30.0f;	
	theta  = 0.0f;
	phi = 0.0f;

	setPosition(Vector3(0,0,-50));
}

EditorCamera::~EditorCamera()
{
}


void EditorCamera::rotate(const float dx, const float dy, const float dz)
{
	D3DXMatrixIdentity((D3DXMATRIX*)&rotation);
	// Add angles
	pitch_angle += dx;
	yaw_angle += dy;
	roll_angle += dz;

	//Pi * 2
	float revolution = 2 * (float)D3DX_PI;

	//Keep angles between 0 and 2PI.
	if (pitch_angle > revolution)
	{
		pitch_angle -= revolution;
	}
	else if (pitch_angle < 0)
	{
		pitch_angle += revolution;
	}

	if (yaw_angle > revolution)
	{
		yaw_angle -= revolution;
	}
	else if (yaw_angle < 0)
	{
		yaw_angle += revolution;
	}

	if (roll_angle > revolution)
	{
		roll_angle -= revolution;
	}
	else if (roll_angle < 0)
	{
		roll_angle += revolution;
	}

	// Create rotationmatrix 
	D3DXMatrixRotationYawPitchRoll((D3DXMATRIX*)&rotation, yaw_angle, pitch_angle, roll_angle);
}

void EditorCamera::rotate(const float dtheta, const float dphi)
{
	theta += dtheta;
	phi += dphi;

	

	//Pi * 2
	float revolution =  4 * (float)D3DX_PI;

	if (theta > 0 && theta < revolution)
	{
		theta += dtheta;
	}

	if (phi > 0 && phi < revolution)
	{
		phi += dphi;
	}


	//Keep angles between 0 and 2PI.

	/*
	if (theta > revolution)
	{
		theta -= revolution;
	}
	else if (theta < 0)
	{
		theta += revolution;
	}

	if (phi > revolution)
	{
		phi -= revolution;
	}
	else if (phi < 0)
	{
		phi += revolution;
	}
	*/
}

void EditorCamera::updateAsShapeCamera()
{
	Vector3 lookat = Vector3(0,0,0.0f);
	D3DXMatrixTranslation((D3DXMATRIX*)&translation, 0.0f, 0.0f, radius);

	//setProjectionOrtho(-8 * radius, 8 * radius, -4.5f * radius, 4.5f * radius);

	

	Vector3 position(0.0f,0.0f,radius);

	Vector3 up(0.0f, 1.0f, 0.0f);

	Matrix m;
	D3DXMatrixIdentity((D3DXMATRIX*)&m);
	
	D3DXMatrixMultiply((D3DXMATRIX*)&m, (D3DXMATRIX*)&translation, (D3DXMATRIX*)&rotation);
	D3DXVec3TransformCoord((D3DXVECTOR3*)&position,(D3DXVECTOR3*)&position, (D3DXMATRIX*)&rotation);


	D3DXVec3TransformNormal((D3DXVECTOR3*)&up, (D3DXVECTOR3*)&up, (D3DXMATRIX*)&rotation);

	setPosition(position);

	
	setUp(up);

	Vector3 orient = lookat - position;
	orient.normalize();
	setOrientation(orient);


	Camera::update();
}

void EditorCamera::updateAsLevelCamera()
{
	Vector3 lookat = Vector3(0,0,1.0f);

	D3DXVec3TransformNormal((D3DXVECTOR3*)&lookat,(D3DXVECTOR3*)&lookat, (D3DXMATRIX*)&rotation);

	lookat.normalize();
	setOrientation(lookat);

	Vector3 up(0,1.0f,0);
	D3DXVec3TransformNormal((D3DXVECTOR3*)&up, (D3DXVECTOR3*)&up, (D3DXMATRIX*)&rotation);
	up.normalize();

	setUp(up);

	Camera::update();
}

void EditorCamera::adjustRadius(const float dradius)
{
	radius += dradius;
}

void EditorCamera::setRadius(const float rad)
{
	radius = rad;
}
void EditorCamera::setTheta(const float theta)
{
	this->theta = theta;
}

void EditorCamera::setPhi(const float phi)
{
	this->phi = phi;
}

void EditorCamera::setRotation(const float x, const float y, const float z)
{
	pitch_angle = x;
	yaw_angle = y;
	roll_angle = z;

	D3DXMatrixRotationYawPitchRoll((D3DXMATRIX*)&rotation, yaw_angle, pitch_angle, roll_angle);
}

void EditorCamera::setRotation(const Vector3& rotation)
{
	pitch_angle = rotation.x;
	yaw_angle = rotation.y;
	roll_angle = rotation.z;

	D3DXMatrixRotationYawPitchRoll((D3DXMATRIX*)&rotation, yaw_angle, pitch_angle, roll_angle);
}


const float EditorCamera::getTheta() const
{
	return theta;
}

const float EditorCamera::getPhi() const
{
	return phi;
}


const Vector3 EditorCamera::getRotation() const
{
	return Vector3(pitch_angle, yaw_angle, roll_angle);
}

const float EditorCamera::getRadius() const
{
	return radius;
}