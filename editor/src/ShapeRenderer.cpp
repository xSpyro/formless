#include "ShapeRenderer.h"
#include "PositionEvent.h"
#include "PhysicsComponent.h"

ShapeRenderer::ShapeRenderer()
{

}

ShapeRenderer::ShapeRenderer(Layer* const layer, LogicHandler* const logic)
{
	this->layer = layer;
	this->logic = logic;
}

ShapeRenderer::~ShapeRenderer()
{
}

void ShapeRenderer::updateNodes(Vector3* nodes, unsigned int nrOfNodes)
{
	// adjust how many objects we have
	if (nrOfNodes > objects.size())
	{
		unsigned int nr = nrOfNodes - objects.size();
		for(unsigned int i = 0; i < nr; i++)
		{
			objects.push_back(logic->createObject("EditorBall"));
			PhysicsComponent* comp = objects.back()->findComponent<PhysicsComponent>();

			if (comp)
			{
				comp->useGravity = false; 
			}
		}

	}
	else if (nrOfNodes < objects.size())
	{
		unsigned int nr = objects.size() - nrOfNodes;
		for(unsigned int i = 0; i < nr; i++)
		{
			logic->destroyObject(objects.back());
			objects.pop_back();
		}
	}


	// update position on the objects
	for (unsigned int i = 0; i < objects.size(); i++)
	{
		PositionEvent ev(nodes[i]);
		objects[i]->send(&ev);
	}
}


Layer* ShapeRenderer::getLayer() const
{
	return layer;
}


void ShapeRenderer::setActive(const bool active)
{
	this->active = active;

	if (active)
	{
		for(unsigned int i = 0; i < objects.size(); i++)
		{
			objects[i]->activateComponents();
		}
	}

	else
	{
		for(unsigned int i = 0; i < objects.size(); i++)
		{
			objects[i]->deactivateComponents();
		}
	}

	layer->setVisible(active);
}

const bool ShapeRenderer::isActive() const
{
	return active;
}