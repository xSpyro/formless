#include "ParticleEditor.h"
#include "MemLeaks.h"

#include <sstream>

#include <math.h>
#include <fstream>

#include "EffectFormat.h"

//
//void ParticleEditor::addType(std::string file)
//{
//	typeNames.push_back(file);
//	ps_log.notice("Added type: ", file);
//}
bool ParticleEditor::saveToFile(std::string file)
{
	bool result = false;
	ps_log.notice("save to: ", "effects/" + file);
	effects.setBehaviorNames("effects/" + file);
	result = formatStore.exportToFile("effects/" + file);
	effects.clearVector();
	return result;
}
bool ParticleEditor::loadAnimation(std::string file)
{
	effects.clearVector();
	//exit(0);
	ps_log.notice("imnport from: ", file);
	return formatStore.importFromFile(file);
}

void ParticleEditor::addTextureRenderable(std::string file)
{
	Texture * tex = NULL;
	tex = renderer->texture()->get( "textures/" +file );
	ps_log.notice("filename: ", file);
	if( tex == NULL)
		return;
	if( selected_typeID == 0  && basic_renderable != NULL )
		basic_renderable->setTexture( tex );
	else if( selected_typeID == 1 && heat_renderable != NULL )
		heat_renderable->setTexture( tex );

	memset(effects.getSelectedEmitter()->texture_name, 0, 64);
	strcpy_s(effects.getSelectedEmitter()->texture_name, 64, file.c_str());

	ps_log.notice("Texture added: ", file);

	//gridlayer->add( New Renderable(basic_mesh, tex, NULL) );
}

ParticleEditor::ParticleEditor() : Editor(), ps_log("ParticleEditor")
{
	particle_id = 0;
	typeNames.push_back("basic");
	typeNames.push_back("heat");
	selected_typeID = 0;
	type_name = "basic";

	basic_renderable = NULL;
	heat_renderable  = NULL;
}

ParticleEditor::~ParticleEditor()
{
}


bool ParticleEditor::startup(void * window, Renderer* renderer)
{
	Editor::startup(window, renderer);

	renderMod.setParticleSystem(&psystem);
	psystem.init(renderer);

	renderMod.init(renderer, RM_TERRAIN | RM_EFFECTS);
	renderMod.build();
	
	camera.setPosition( Vector3( 0, 0, 10) );
	camera.setLookAt( Vector3(0, 0, 0) );
	camera.update();

	ecamera = scene->getCamera("main");
	ecamera->setPosition( Vector3( 0, 0, -10) );
	ecamera->setLookAt( Vector3(0, 0, 0) );
	ecamera->update();


	ecamera->setPosition(Vector3(0,70,-50.0f));
	ecamera->setLookAt(Vector3(0, 0, 0));
	ecamera->setClipRange(10, 1000);
	ecamera->setProjection3D(45, 16 / 9.0f);

	psystem.init(renderer);
	psystem.createType(typeNames[0], renderer->shader()->get("shaders/cpu_particle.vs"));
	psystem.createType(typeNames[1], renderer->shader()->get("shaders/cpu_particle.vs"));


	BaseShader* vshader = NULL;
	vshader = renderer->shader()->get("shaders/cpu_particle.vs");
	BaseShader* gshader = NULL;
	gshader = renderer->shader()->get("shaders/cpu_particle.gs");
	BaseShader* pshader = NULL;
	pshader = renderer->shader()->get("shaders/cpu_particle.ps");

	
	pshader->setSamplers();

	basic_mesh = psystem.getParticleMesh("basic");
	heat_mesh  = psystem.getParticleMesh("heat");

	if(vshader && pshader && gshader)
	{
		gridlayer = scene->addLayer("main", 0, &camera);//renderer->createLayer();

		gridview = scene->getViewport();//gridlayer->addViewport(&camera);

		Texture *tex = renderer->texture()->get( "textures/cpu_parts.png" );
		Texture * heat_tex = renderer->texture()->get( "textures/partikel_lol.jpg" );
		
		gridview->getEffect()->blendModeParticle();
		//gridview->getEffect()->blendModeAlpha();

		basic_renderable = New Renderable(basic_mesh, tex, NULL);
		heat_renderable  = New Renderable(heat_mesh, heat_tex, NULL);

		gridview->setShaders(vshader, gshader, pshader);

		gridlayer->add( basic_renderable );
		gridlayer->add( heat_renderable );

		//renderer->insertLayer(100, gridlayer);
	}
		
	ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE b;
	b.behavior = New ParticleCustomBehavior();
	//memset(current_emitter, 0, sizeof(struct ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE);
	//current_animation = New ParticleAnimation();
	//current_emitter   = New ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE();

		formatStore.registerFormat("paf", New EffectFormat(&effects.effect_ps, &renderMod));

		effects.addAnimation();
		effects.addEmitter();

		strcpy_s(effects.getSelectedEmitter()->type_name, 64, "basic");



	//renderMod.setPlayerPosition(Vector3(0, 0, 0));
	//renderMod.setCursorPosition(Vector3(0, 0, 0));

	//ParticleAnimation anim;
	//ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE pack;

	//effect_ps.animations.push_back(anim);
	//effect_ps.animations[0].effects.push_back(pack);

	//std::stringstream os;
	//os << "PB2010" << particle_id;

	//memset(effect_ps.animations[0].effects[0].behave_name, 0, 64);
	//strcpy_s(effect_ps.animations[0].effects[0].behave_name, 64, os.str().c_str() );

	//effect_ps.animations[0].effects[0].behavior = new ParticleCustomBehavior();

	/*addType("trollface.png", "trollface.png");
	//typeNames.push_back("trollface.png");

	selected_typeID = typeNames.size()-1;
	memset(effects.getSelectedEmitter()->type_name, 0, 64);
	strcpy_s(effects.getSelectedEmitter()->type_name, 64, typeNames[2].c_str());

	//playAllAnimations();
	particle_id++;
	effects.getSelectedEmitter()->spawnRate = 5.0f;
	effects.getSelectedEmitter()->life = 1000;
	effects.getSelectedEmitter()->lifeAffectRate = 0.0001f;
	playAnimation();*/
	if(window)
		return true;
	return this->isOpen();
}

bool ParticleEditor::shutdown()
{
	psystem.kill();
	return false;
}

void ParticleEditor::update()
{

	if (isInputDown('A') )
	{
	}

	if (isInputDown('O') || isInputDown('S'))
	{
	}
	if (isInputDown('E') || isInputDown('D'))
	{
	}
	if (isInputDown(188) || isInputDown('W'))
	{
	}
	if(isInputDown(27))
	{
		this->close();
	}

	if(isInputDown(600))
	{
	}
	
	if (isInputDown(602))
	{
	}
	ecamera->update();

	logic->update();

	psystem.update();
		//ecamera->update();

		//logic->update();
	
	renderMod.updateCamera(*ecamera);
	renderMod.update();
}

void ParticleEditor::runFrame()
{
	process();

	static Timer timeframe;
	if (timeframe.accumulate(1 / 60.0f))
	{
		update();
	}
	gridlayer->setVisible(true);

	renderMod.preFrame();

	scene->render();

	scene->present();		
		

		//scene->render();

		//scene->present();
}


void ParticleEditor::onInput(int key, int state)
{
	if (key == 610)
	{
	}
	if (key == 600 && state == 1)
	{

		
	}
	else if (key == 600 && state == 0)
	{

	}

	scene->onInput(key, state);
}

void ParticleEditor::onMouseMovement(int x, int y)
{
	old = mouse;
	
	this->getMouseVector(&mouse.x, &mouse.y);


	if (isInputDown(600))
	{
		Vector2 rot = mouse - old;
		
		ecamera->rotate(-rot.y, rot.x, 0.0f);
		
		ecamera->updateAsLevelCamera();
	}

	scene->onMouseMovement(x,y);
}


void ParticleEditor::ReceiveMessage(int id, int message)
{
	switch(message)
	{	
	case MOVE_RIGHT_PRESS:
		ecamera->strafe(-5.5f);
		break;
	case MOVE_LEFT_PRESS:
		ecamera->strafe(5.5f);
		break;
	case MOVE_UP_PRESS:
		ecamera->move(5.5f);
		break;
	case MOVE_DOWN_PRESS:
		ecamera->move(-5.5f);
		break;
	}
	switch(id)
	{
		case P_PLAY_SELECTED_A:
			playSelectedAnimation();
			break;
		case P_PLAY_SELECTED_E:
			playSelectedEmitter();
			break;
		case P_DELETE_E:

				//removeEmitter(selectedEmitterID);
			break;
		case P_DELETE_A:

			//	removeAnimation(selectedAnimationID);
			break;

		//case P_ADD_EMITTER:
		//	addEmitter();
		//	break;
			
		//case P_ADD_ANIMATION:
			//addAnimation();
		//	break;

		case P_LOOP_ANIMATION:
			loopAnimation();
			break;

		case P_PLAY_ANIMATION:
			playAnimation();
			break;

		case P_PLAY_ALL:
			playAllAnimations();
			break;

		case P_SELECT_TYPE:
			selected_typeID = message;
			memset(effects.getSelectedEmitter()->type_name, 0, 64);
			strcpy_s(effects.getSelectedEmitter()->type_name, 64, typeNames[message].c_str());
			std::cout << "type: " << typeNames[message] << "\n";
			ps_log.notice("type: ", typeNames[message]);
			break;

		default:
			//message = message;
			break;
	}
	effects.ReceiveMessage(id, message);
}

void ParticleEditor::ReceiveMessage(int id, std::string& message)
{
	switch(id)
	{	
		case P_F_MESSAGE_SAVE_FILE:
			 saveToFile(message);
			break;

		case P_F_MESSAGE_OPEN_FILE:
			loadAnimation(message);
			break;

		case P_MESSAGE_OPEN_TEXTURE:
			//addTextureRenderable(message);
			break;

		case P_ADD_TYPE:
			addType(message, message);
			//typeNames.push_back(message);
			//psystem.createType(message, renderer->shader()->get("shaders/cpu_particle.vs"));
			break;

		default:
			break;
	}

	effects.ReceiveMessage(id, message);

}
void ParticleEditor::ReceiveMessage(int id, float message)
{
	//switch(id)
	//{
	//}
	effects.ReceiveMessage(id, message);
}
void ParticleEditor::addAnimation()
{
	effects.addAnimation();
	/*if( current_animation != NULL && current_animation->effects.size() > 0 )
	{
		for(UINT i = 0; i < current_animation->effects.size(); i++)
		{
			deleteEmitter(&current_animation->effects[i]);
		}
		effect_ps.animations.push_back( * current_animation);
		//delete current_animation;// = NULL;
		current_animation = new ParticleAnimation();
	}
	else
		ps_log.notice("Tried to add null animation");*/
}
//void ParticleEditor::setCurrentAnimation(int id)
//{
//	//if( current_animation == NULL )
//	//	current_animation = &effect_ps.animations[id];
//	//else
//	//{
//	//	//delete current_animation;
//	//	current_animation = &effect_ps.animations[id];
//	//}
//}
//void ParticleEditor::setCurrentEmitter(int id)
//{
//	//if( selectedAnimationID > -1 )
//	//{
//	//	//if( current_emitter != NULL )
//	//		//delete current_animation;
//
//	//	current_emitter = &effect_ps.animations[selectedAnimationID].effects[id];
//	//}
//
//}
//void ParticleEditor::spawnEmitter(int id)
//{
//	//if( current_animation != NULL )
//	//{
//	//	deleteEmitter(current_emitter);	
//
//	//	current_animation->effects[id].behavior = New ParticleCustomBehavior();
//	//	psystem.createBehavior(current_animation->effects[id].behave_name, current_animation->effects[id].behavior);
//
//	//	current_animation->effects[id].emitter = NULL;
//	//	current_animation->effects[id].emitter = psystem.spawnEmitter( type_name, current_animation->effects[id].behave_name );
//
//	//	initEmitter(&current_animation->effects[id]);
//	//}
//}
void ParticleEditor::addType(std::string type, std::string texture)
{
	typeNames.push_back(type);
	Renderable * rend;
	renderables.push_back(rend);
	int id = (int)renderables.size()-1;

	if( !psystem.createType(type, renderer->shader()->get("shaders/cpu_particle.vs")) )
		exit(0);

	Mesh * mesh = psystem.getParticleMesh(type);

	//gridlayer = scene->addLayer("main", 0, &camera);

	//gridview = scene->getViewport();

	Texture *tex = renderer->texture()->get( "textures/" + texture );

	ps_log.notice("Added type: ", texture);
	
	renderables[id] = NULL;
	renderables[id] = New Renderable(mesh, tex, NULL);
	if( renderables[id] == NULL )
		exit(0);

	gridlayer->add( renderables[id] );
}
void ParticleEditor::deleteEmitter(ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * e)
{
	if(e != NULL)
	{
		psystem.killBehavior(typeNames[selected_typeID], e->behave_name);
		//delete e->behavior;
		//delete e->emitter;
		e->behavior = NULL;
		e->emitter  = NULL;
	}
} 
//void ParticleEditor::addEmitter()
//{
	/*if( current_animation != NULL )
	{
		memset(current_emitter->behave_name, 0, 64);
		strcpy_s(current_emitter->behave_name, 64, "CurrentEmitter");
		deleteEmitter(current_emitter);

		std::stringstream os;
		os << "PB2010" << particle_id;
		memset(current_emitter->behave_name, 0, 64);
		strcpy_s(current_emitter->behave_name, 64, os.str().c_str() );

		ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE pack;

		memset(pack.behave_name, 0, 64);
		strcpy_s(pack.behave_name, 64, current_emitter->behave_name);
		pack.behavior		 = NULL;//current_emitter->behavior;
		pack.diffusion		 = current_emitter->diffusion;
		pack.emitter		 = NULL;//current_emitter->emitter;
		pack.emitterType	 = current_emitter->emitterType;
		pack.endColor		 = current_emitter->endColor;
		pack.endScale		 = current_emitter->endScale;
		pack.life			 = current_emitter->life;
		pack.middleColor	 = current_emitter->middleColor;
		pack.randAngle		 = current_emitter->randAngle;
		pack.spawnBox		 = current_emitter->spawnBox;
		pack.spawnRate		 = current_emitter->spawnRate;
		pack.startSpeed		 = current_emitter->startSpeed;
		pack.middleSpeed	 = current_emitter->middleSpeed;
		pack.endSpeed		 = current_emitter->endSpeed;
		pack.spinMax		 = current_emitter->spinMax;
		pack.spinMin		 = current_emitter->spinMin;
		pack.startColor		 = current_emitter->startColor;
		pack.startScale		 = current_emitter->startScale;
		pack.colorPath		 = current_emitter->colorPath;
		pack.speedPath		 = current_emitter->speedPath;
		pack.gravity		 = current_emitter->gravity;
		pack.distorsion		 = current_emitter->distorsion;
		pack.displacementPos = current_emitter->displacementPos;
		pack.lifeAffectRate  = current_emitter->lifeAffectRate;
		pack.affectRate      = current_emitter->affectRate;

		current_animation->effects.push_back(*current_emitter);
		ps_log.notice("Emitter added: ", particle_id);
		particle_id++;
		
	}*/
//}
void ParticleEditor::loopAnimation()
{
	if( effects.getSelectedEmitter() != NULL )
	{
		//strcpy_s(current_emitter->behave_name, 64, "CurrentEmitter");
		deleteEmitter( 
			effects.getSelectedEmitter() );	

		effects.getSelectedEmitter()->behavior = New ParticleCustomBehavior();
		
		//EffectFormat e(NULL);
		//e.initBehaviorByPackage(effects.getSelectedEmitter(), effects.getSelectedEmitter()->behavior);

		psystem.createBehavior( 
							    effects.getSelectedEmitter()->behave_name,
							    effects.getSelectedEmitter()->behavior 
								);

		initEmitter( effects.getSelectedEmitter() );	
			

	
		effects.getSelectedEmitter()->emitter = NULL;

		effects.getSelectedEmitter()->emitter = psystem.spawnEmitter( 
			effects.getSelectedEmitter()->type_name, effects.getSelectedEmitter()->behave_name );
		if( effects.getSelectedEmitter()->emitter == NULL )
		{
			return;
		}
		effects.getSelectedEmitter()->emitter->setLife( 0 );
		effects.getSelectedEmitter()->emitter->setLifeAffectRate( effects.getSelectedEmitter()->lifeAffectRate );

		//initEmitter( effects.getSelectedEmitter() );
	}




	/*if( effects.getSelectedEmitter() != NULL )
	{
		//strcpy_s(current_emitter->behave_name, 64, "CurrentEmitter");
		deleteEmitter( 
			effects.getSelectedEmitter() );	

		effects.getSelectedEmitter()->behavior = New ParticleCustomBehavior();
		
		EffectFormat e(NULL);
		//e.initBehaviorByPackage(effects.getSelectedEmitter(), effects.getSelectedEmitter()->behavior);

		psystem.createBehavior( 
							    effects.getSelectedEmitter()->behave_name,
							    effects.getSelectedEmitter()->behavior );
		
		psystem.addToGroup(
			type_name, 
			effects.getSelectedEmitter()->behave_name,
			0,
			effects.getSelectedEmitter()->spawnRate
			);*/

		//effects.getSelectedEmitter()->emitter = NULL;

		//effects.getSelectedEmitter()->emitter = psystem.spawnEmitter( 
		//		type_name, effects.getSelectedEmitter()->behave_name );

		//initEmitter( 
		//	effects.getSelectedEmitter() );
	//}
}
void ParticleEditor::initEmitter(ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * p)
{
	//p->emitter->setLife( 2 );
	//ps_log.notice("life ", p->life);

	//p->emitter->setLifeAffectRate( p->affectRate );
	//ps_log.notice("life_affect_rate ", p->lifeAffectRate);

	//p->emitter->setSpawnBox( p->spawnBox );

	//p->emitter->setSpawnRate( p->spawnRate );
	
	p->behavior->endColor    = p->endColor;
	p->behavior->startColor  = p->startColor;
	p->behavior->middleColor = p->middleColor;
	
	p->behavior->endScale   = p->endScale;
	p->behavior->startScale = p->startScale;
	//ps_log.notice("s_scale ", p->startScale);
	
	p->behavior->randAngleX   = p->randAngle.x;
	p->behavior->randAngleY   = p->randAngle.y;
	p->behavior->randAngleZ   = p->randAngle.z;
	p->behavior->intensityDir = p->intensityDir;
	p->behavior->colorPath    = p->colorPath;
	p->behavior->speedPath    = p->speedPath;

	p->behavior->spinMin = p->spinMin;
	p->behavior->spinMax = p->spinMax;

	p->behavior->gravity      = p->gravity;

	p->behavior->distorsion   = p->distorsion;

	p->behavior->startSpeed  = p->startSpeed;	
	p->behavior->middleSpeed = p->middleSpeed;	
	p->behavior->endSpeed    = p->endSpeed;

	p->behavior->displacementPos = p->displacementPos;
	p->behavior->lifeAffectRate = p->affectRate;
	//ps_log.notice("life_affect_rate ", p->affectRate);
	

}
void ParticleEditor::playAnimation()
{
	if( effects.getSelectedEmitter() != NULL )
	{
		//strcpy_s(current_emitter->behave_name, 64, "CurrentEmitter");
		deleteEmitter( 
			effects.getSelectedEmitter() );	

		effects.getSelectedEmitter()->behavior = New ParticleCustomBehavior();
		
		//EffectFormat e(NULL);
		//e.initBehaviorByPackage(effects.getSelectedEmitter(), effects.getSelectedEmitter()->behavior);

		psystem.createBehavior( 
							    effects.getSelectedEmitter()->behave_name,
							    effects.getSelectedEmitter()->behavior 
								);

		initEmitter( effects.getSelectedEmitter() );	
			

		effects.getSelectedEmitter()->emitter = NULL;

		effects.getSelectedEmitter()->emitter = psystem.spawnEmitter( 
			effects.getSelectedEmitter()->type_name, effects.getSelectedEmitter()->behave_name );
		if( effects.getSelectedEmitter()->emitter == NULL )
		{
			return;
		}
		effects.getSelectedEmitter()->emitter->setLife( effects.getSelectedEmitter()->life );
		effects.getSelectedEmitter()->emitter->setLifeAffectRate( effects.getSelectedEmitter()->lifeAffectRate );
		effects.getSelectedEmitter()->emitter->setSpawnRate( effects.getSelectedEmitter()->spawnRate );

		//initEmitter( effects.getSelectedEmitter() );
	}
	//if( current_emitter != NULL )
	//{
	//	strcpy_s(current_emitter->behave_name, 64, "CurrentEmitter");
	//	deleteEmitter(current_emitter);	
	//	current_emitter->behavior = New ParticleCustomBehavior();
	//	psystem.createBehavior(current_emitter->behave_name, current_emitter->behavior);

	//	current_emitter->emitter = NULL;
	//	current_emitter->emitter = psystem.spawnEmitter( type_name, current_emitter->behave_name );

	//	initEmitter(current_emitter);
	//}
}
void ParticleEditor::playSelectedEmitter()
{
	if( effects.getSelectedEmitter() != NULL )
	{
		if( effects.getSelectedEmitter()->behavior != NULL )
		{
			deleteEmitter(effects.getSelectedEmitter());
		}
		effects.getSelectedEmitter()->behavior = New ParticleCustomBehavior();
		psystem.createBehavior(effects.getSelectedEmitter()->behave_name, effects.getSelectedEmitter()->behavior);
		if( effects.getSelectedEmitter()->behavior != NULL )
		{
			effects.getSelectedEmitter()->emitter = psystem.spawnEmitter( effects.getSelectedEmitter()->type_name, effects.getSelectedEmitter()->behave_name );
		
			initEmitter(effects.getSelectedEmitter());

			effects.getSelectedEmitter()->emitter->setLife( effects.getSelectedEmitter()->life );
			effects.getSelectedEmitter()->emitter->setLifeAffectRate( effects.getSelectedEmitter()->lifeAffectRate );
			effects.getSelectedEmitter()->emitter->setSpawnRate( effects.getSelectedEmitter()->spawnRate );
		}
	}
}
void ParticleEditor::playSelectedAnimation()
{
	if( effects.getSelectedAnimation() != NULL )
	{
		for(UINT e = 0; e < effects.getSelectedAnimation()->effects.size(); e++)
		{
			if( effects.getSelectedAnimation()->effects[e].behavior != NULL )
			{
				deleteEmitter(&effects.getSelectedAnimation()->effects[e]);
			}
		}

		for(UINT e = 0; e < effects.getSelectedAnimation()->effects.size(); e++)
		{


			effects.getSelectedAnimation()->effects[e].behavior = New ParticleCustomBehavior();
			psystem.createBehavior(effects.getSelectedAnimation()->effects[e].behave_name, effects.getSelectedAnimation()->effects[e].behavior);
			if( effects.getSelectedAnimation()->effects[e].behavior != NULL )
			{
				effects.getSelectedAnimation()->effects[e].emitter = psystem.spawnEmitter( 
					effects.getSelectedAnimation()->effects[e].type_name, 
					effects.getSelectedAnimation()->effects[e].behave_name );

				initEmitter(&effects.getSelectedAnimation()->effects[e]);

				effects.getSelectedAnimation()->effects[e].emitter->setLife( effects.getSelectedAnimation()->effects[e].life );
				effects.getSelectedAnimation()->effects[e].emitter->setLifeAffectRate( effects.getSelectedAnimation()->effects[e].lifeAffectRate );
				effects.getSelectedAnimation()->effects[e].emitter->setSpawnRate(effects.getSelectedAnimation()->effects[e].spawnRate );

			}
		}

	}
}

void ParticleEditor::playAllAnimations()
{
	//memset(current_emitter->behave_name, 0, 64);
	//strcpy_s(current_emitter->behave_name, 64, "CurrentEmitter");
	deleteEmitter(effects.getSelectedEmitter());


	for(int a = 0; a < effects.getNrAnimations(); a++)
	{
		for(int e = 0; e < effects.getNrEmitters(); e++)
		{
			if( effects.getEmitter(a, e)->behavior != NULL )
			{
				deleteEmitter( effects.getEmitter(a, e) );
			}
		}
	}

	for(int a = 0; a < effects.getNrAnimations(); a++)
	{
		for(int e = 0; e < effects.getNrEmitters(); e++)
		{
			effects.getEmitter(a, e)->behavior = New ParticleCustomBehavior();
			psystem.createBehavior(effects.getEmitter(a, e)->behave_name, effects.getEmitter(a, e)->behavior);
			if( effects.getEmitter(a, e)->behavior != NULL )
			{
				effects.getEmitter(a, e)->emitter = psystem.spawnEmitter( effects.getEmitter(a, e)->type_name, effects.getEmitter(a, e)->behave_name );
			
				initEmitter(effects.getEmitter(a, e));
				effects.getEmitter(a, e)->emitter->setLife( effects.getEmitter(a, e)->life );
				effects.getEmitter(a, e)->emitter->setLifeAffectRate( effects.getEmitter(a, e)->affectRate );
				effects.getEmitter(a, e)->emitter->setSpawnRate( effects.getEmitter(a, e)->spawnRate );
			}
		}
	}
}

float ParticleEditor::getRandAngleX()
{
	return effects.getRandAngleX();
}
float ParticleEditor::getRandAngleY()
{
	return effects.getRandAngleY();
}
float ParticleEditor::getRandAngleZ()
{
	return effects.getRandAngleZ();
}
float ParticleEditor::getSpawnBoxX()
{
	return effects.getSpawnBoxX();
}
float ParticleEditor::getSpawnBoxY()
{
	return effects.getSpawnBoxY();
}
float ParticleEditor::getSpawnBoxZ()
{
	return effects.getSpawnBoxZ();
}
int ParticleEditor::getLife()
{
	return effects.getLife();
}
float ParticleEditor::getIntensityDir()
{
	return effects.getIntensityDir();
}
float ParticleEditor::getDiffusion()
{
	return effects.getDiffusion();
}
float ParticleEditor::getSpinMin()
{
	return effects.getSpinMin();
}
float ParticleEditor::getSpinMax()
{
	return effects.getSpinMax();
}
float ParticleEditor::getSpawnRate()
{
	return effects.getSpawnRate();
}
float ParticleEditor::getAffectRate()
{
	return effects.getAffectRate();
}
float ParticleEditor::getStartScale()
{
	return effects.getStartScale();
}
float ParticleEditor::getEndScale()
{
	return effects.getEndScale();
}
float ParticleEditor::getStartSpeed()
{
	return effects.getStartSpeed();
}
float ParticleEditor::getMiddleSpeed()
{
	return effects.getMiddleSpeed();
}
float ParticleEditor::getEndSpeed()
{
	return effects.getEndSpeed();
}
float ParticleEditor::getColorPath()
{
	return effects.getColorPath();
}
float ParticleEditor::getSpeedPath()
{
	return effects.getSpeedPath();
}
float ParticleEditor::getGravity()
{
	return effects.getGravity();
}
float ParticleEditor::getDistorsion()
{
	return effects.getDistorsion();
}
float ParticleEditor::getDisplacementPos()
{
	return effects.getDisplacementPos();
}
float ParticleEditor::getLifeAffectRate()
{
	return effects.getLifeAffectRate();
}
float ParticleEditor::getStartColorR()
{
	return effects.getStartColorR();
}
float ParticleEditor::getStartColorG()
{
	return effects.getStartColorG();
}
float ParticleEditor::getStartColorB()
{
	return effects.getStartColorB();
}
float ParticleEditor::getMiddleColorR()
{
	return effects.getMiddleColorR();
}
float ParticleEditor::getMiddleColorG()
{
	return effects.getMiddleColorG();
}
float ParticleEditor::getMiddleColorB()
{
	return effects.getMiddleColorB();
}
float ParticleEditor::getEndColorR()
{
	return effects.getEndColorR();
}
float ParticleEditor::getEndColorG()
{
	return effects.getEndColorG();
}
float ParticleEditor::getEndColorB()
{
	return effects.getEndColorB();
}

int ParticleEditor::getNrEmitters()
{
	return effects.getNrEmitters();
}
int ParticleEditor::getNrAnimations()
{
	return effects.getNrAnimations();
}


void ParticleEditor::ReceiveMessage(int id, std::string& name, std::string& message)
{

}
bool ParticleEditor::addEmitter()
{
	if( effects.addEmitter() )
	{
		psystem.addToGroup(
		typeNames[selected_typeID],
		typeNames[selected_typeID], 
		effects.getSelectedEmitter()->behave_name,
		effects.getSelectedEmitter()->life, 
		effects.getSelectedEmitter()->spawnRate,
		effects.getSelectedEmitter()->affectRate
		);

		return true;
	}


	return false;
}

void ParticleEditor::onCreateObject(Object * object)
{
}

void ParticleEditor::onEnter()
{
	//ecamera = scene->getCamera("main");
	//ecamera->setPosition( Vector3( 0, 0, -10) );
	//ecamera->setLookAt( Vector3(0, 0, 0) );
	//ecamera->update();


	//ecamera->setPosition(Vector3(0,70,-50.0f));
	//ecamera->setLookAt(Vector3(0, 0, 0));
	//ecamera->setClipRange(10, 1000);
	//ecamera->setProjection3D(45, 16 / 9.0f);

	//std::cout << "Entered particle editor\n";

	scene->bindEditor(this);
	renderMod.bind();
	renderMod.setPlayerPosition(Vector3(900, 0, 0));
	renderMod.setCursorPosition(Vector3(0, 0, 0));
}