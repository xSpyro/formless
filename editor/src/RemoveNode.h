#ifndef REMOVE_NODE_H

#define REMOVE_NODE_H

#include "Action.h"

#include "AddNode.h"

#include "Editor.h"

class AddNode;
class Editor;

class RemoveNode : public Action
{
public:

	RemoveNode(Editor* const ed);
	~RemoveNode();

	void undo();
	void redo();

	void setBranches(const std::vector<int>& br);
	void setPosition(const Vector3& position);
	void setId(const int id);

	const Vector3 getPosition() const;
	const int getId() const;
	const std::string toString();

private:
	Editor* editor;

	std::vector<int> branches;

	Vector3 position;
	int id;
};


#endif