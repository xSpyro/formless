#ifndef SHAPEEDITOR_H

#define SHAPEEDITOR_H

#include "Editor.h"
#include "Graphics.h"
#include "ShapeCreator.h"

#include "FileHandler.h"


#include "Arrows.h"

#include "Actions.h"

#include "RenderModule.h"

#include "EditorGrid.h"

#include "Boxing.h"

/*	File: ShapeEditor.h
*	This file contains all functions and communication from wpf down to the c++-dll. 
*	Use this to create, remove and edit different kind of ShapeAnimations
*/


enum ShapeMessage
{
	MESSAGE_SWITCH_WORKSPACE,
	MESSAGE_REMOVE_WORKSPACE,
	MESSAGE_VISIBILITY,
	MESSAGE_PLAY_ANIMATION,
	MESSAGE_COPY_WORKSPACE,
	MESSAGE_OPEN_FILE,
	MESSAGE_SAVE_FILE,
	MESSAGE_OPEN_POINTS_FROM_OBJ,
	MESSAGE_SET_FRAME,
	MESSAGE_DISABLE_FRAME,
	MESSAGE_SELECT_TOOL,
	MESSAGE_SELECT_ANIMATION,
	MESSAGE_SELECT_LINKING,
	MESSAGE_DOUBLE_LINK,
	MESSAGE_CLEAR_ALL,
	MESSAGE_MOVE_CAMERA,
	MESSAGE_UNDO,
	MESSAGE_REDO,
	MESSAGE_SET_GRAVITY,
	MESSAGE_SET_DEATH,
	MESSAGE_SET_SPEED,
	MESSAGE_RELOAD_SHADERS,
	MESSAGE_SCALE_OBJECTS,
	MESSAGE_ROTATE_OBJECTS_IN_YAXIS,
	MESSAGE_ADD_NODE,
	MESSAGE_REMOVE_NODE,
	MESSAGE_UNLINK_NODES,
	MESSAGE_NODES_VISIBILITY,
	MESSAGE_COPY_MARKED_NODES,
	MESSAGE_ALIGN_MOUSE,
	MESSAGE_ROTATE_OBJECTS_IN_XAXIS,
	MESSAGE_ROTATE_OBJECTS_IN_ZAXIS,
	MESSAGE_END
};

enum Tool
{
	TOOL_PICKING,
	TOOL_SHIFT_PICKING,
	TOOL_LINK,
	TOOL_REMOVAL,
	TOOL_UNLINK,
	TOOL_BOXPICKING,
	TOOL_END
};

enum CameraMovement
{
	CAMERA_MOVE_UP,
	CAMERA_MOVE_DOWN,
	CAMERA_MOVE_LEFT,
	CAMERA_MOVE_RIGHT,
	CAMERA_MOVE_FRONT,
	CAMERA_MOVE_BEHIND,
	CAMERA_MOVE_TO_GAME_SPECIFIC,
	CAMERA_END
};

enum MouseAlignments
{
	MOUSE_ALIGNMENT_NONE,
	MOUSE_ALIGNMENT_X,
	MOUSE_ALIGNMENT_Y,
	MOUSE_ALIGNMENT_END
};

class ShapeEditor : public Editor
{
public:
	ShapeEditor();
	~ShapeEditor();

	bool startup(void * window, Renderer* renderer);
	bool shutdown();

	void update();

	void runFrame();

	void* getRenderTarget();

	void onInput(int key, int state);
	void onMouseMovement(int x, int y);

	void onResizeWindow(unsigned int width, unsigned int height) { }
	void onCreateObject(Object * object);
	void onEnter();

	
	void ReceiveMessage(int id, int message);
	void ReceiveMessage(int id, std::string& message);
	void ReceiveMessage(int id, float message);
	void ReceiveMessage(int id, std::string& name, std::string& message);

	// switches workspace, i.e., switched from a frame to another
	void switchWorkspace(const int workspace);

	// Removes a frame, with all it's objects
	void removeWorkspace(const int workspace);

	// sets a workspace visible or invisible
	void workspaceVisible(const int workspace);

	// Plays an animation
	void playAnimation(const int state);

	// copies a marked workspace to the current active worpspace
	void copyWorkspaceToCurrent(const int workspace);


	void setFrame(const int frame);


	void disableFrame(const int frame);

	// Opens a .pas file
	void openFile(const std::string& file);

	// Saves a file in the .pas-format
	void saveFile(const std::string& file);

	// Open an obj-file to load points from and insert into editor
	void openFileFromOBJ(const std::string& file);

	
	void setWorkspace(const int workspace);
	
	void selectTool(const int tool);
	
	void selectAnimation(const int state);
	
	void selectLinking(const int state);

	// toggles double linkage for the nodes, meaning when you link them you will 
	// have a link from one node, to another one, and back to the previous node 
	void doubleLink(const int state);
	
	// clear all frames, points i.e., the whole animation
	void clearAll();
	
	// align the camera
	void moveCamera(const CameraMovement move);
	
	void reloadShaders();
	
	// scale all marked objects
	void scaleObjects(const int scale);
	
	// rotate the marked objects around axes
	void rotateObjectsInXAxis(const int rotation);
	void rotateObjectsInYAxis(const int rotation);
	void rotateObjectsInZAxis(const int rotation);
	// adds a node in (0,0,0)
	void addNode();
	
	// remove a node or multiple nodes
	void removeNode();
	
	// unlink nodes
	void unlinkNodes();

	// copy marked nodes and put them on the same position as the marked ones
	void copyMarkedNodes();

	// toggles visibility of the nodes
	void setNodesVisibility(const bool visibility);
	
	// sets color for particles in the animation
	void setParticleColor(const float r, const float g, const float b, const float a);
	
	// saves an action, so you can redo it
	void saveCurrentActionState();
	
	// undo-function
	void restoreActionState(const std::vector<unsigned int>& frames, std::vector<ShapeResult>& results);

	// get the next message to wpf, this containing the frames for the
	// framebox in the small tool-window
	const std::string getNextFrameString();

	ShapeCreator* const creator() const;

	// gets gravity for the current frame and sends it to wpf
	const float getGravityForFrame() const;
	
	// gets speed for current frame and sends it to wpf
	const float getSpeedForFrame() const;
	
	// gets death for current frame and sends it to wpf
	const float getDeathForFrame() const;

	void setMouseAlignment(const int alignment);

private:
	Model* grid;
	Layer* gridlayer;
	Viewport* gridview;
	

	// Tools
	Picking picking;
	ShapeCreator* shape;
	ShapeRenderer* shapeRend;

	Tool tool;

	bool particles;
	bool doubleLinkage;
	bool linking;
	Arrows* arrows;

	Timer timer;

	Vector2 first;

	Actions actions;

	RenderModule renderMod;

	EditorGrid editorGrid;

	EditorGrid editorYGrid;

	Boxing boxing;

	MouseAlignments mouseAlignment;

	Vector2 alignMouse;
};

#endif