#include "Arrows.h"


Arrows::Arrows(Layer* const layer)
{
}
Arrows::~Arrows()
{
}

bool Arrows::init(Renderer* const renderer)
{

	this->renderer = renderer;
	BaseShader* gs = renderer->shader()->get("shaders/arrow.gs");
	BaseShader* ps = renderer->shader()->get("shaders/arrow.ps");
	BaseShader* vs = renderer->shader()->get("shaders/arrow.vs");
	effect.bind(vs, ps, gs);	
	return false;

}

void Arrows::render(const unsigned int nr)
{
	effect.apply();
	renderer->getContext()->IASetVertexBuffers(0,0,NULL,NULL, NULL);
	renderer->getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	renderer->getContext()->Draw(nr, 0);
	
}