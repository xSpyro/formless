#include "ShapeObjects.h"

#include "PhysicsComponent.h"
#include "PositionEvent.h"

ShapeObjects::ShapeObjects()
		: gravity(0.0f), speed(0.0f), death(0.0f), scale(0)
{
	logic = NULL;
}

ShapeObjects::ShapeObjects(ShapeObjects& obj, Layer* layer, unsigned int frame, ShapeAnimation& animation)
	: gravity(0.0f), speed(0.0f), death(0.0f), scale(0)
{
	this->logic = obj.logic;
	this->frame = frame;
	this->layer = layer;
	
	for(FrameObjects::iterator it = obj.objects.begin(); it != obj.objects.end(); it++)
	{
		Object* object = logic->createObject("EditorBall");
		objects[object] = it->second;
		
		// Find position for the old point
		Vector3 sourcePos;
		
		PhysicsComponent* comp = it->first->findComponent<PhysicsComponent>();

		if(comp)
		{
			sourcePos = comp->position;
		}

		// set the old points position to the new one position
		comp = object->findComponent<PhysicsComponent>();
		
		if(comp)
		{
			PositionEvent ev(sourcePos);
			object->send(&ev);
			animation.addNode(frame, -1, sourcePos);

			comp->useGravity = false;
		}


		
	}
	
}

ShapeObjects::ShapeObjects(GameHandler* logic, const unsigned int frame, Layer* layer)
		: gravity(0.0f), speed(0.0f), death(0.0f), scale(0)
{
	this->logic = logic;
	this->frame = frame;
	this->layer = layer;
}

ShapeObjects::~ShapeObjects()
{
	this->clearShapeNodes(NULL);
	this->setLayerVisibility(false);
}

void ShapeObjects::updateShapes()
{
}

void ShapeObjects::renderShapes()
{
}

const unsigned int ShapeObjects::getShapeNode(Object* obj)
{
	// if this object exists
	FrameObjects::iterator it;
	it = objects.find(obj);

	if(it != objects.end())
	{
		return it->second;
	}

	return 0xFFFFFFFF;

}



Object* ShapeObjects::insertShapeNode(unsigned int pos, const Vector3& position)
{
	Object* obj = logic->createObject("EditorBall");
	
	PhysicsComponent* comp = obj->findComponent<PhysicsComponent>();

	if(comp)
	{
		comp->useGravity = false;
	}


	PositionEvent ev(position);
	obj->send(&ev);
	objects[obj] = pos;
	return obj;
}

Object* ShapeObjects::insertShapeNode(const unsigned int pos, const float x, const float y, const float z)
{
	Object* obj = logic->createObject("EditorBall");


	PhysicsComponent* comp = obj->findComponent<PhysicsComponent>();

	if(comp)
	{
		comp->useGravity = false;
	}
	
	PositionEvent ev(x,y,z);
	obj->send(&ev);

	objects[obj] = pos;
	return obj;
}

void ShapeObjects::removeShapeNode(const unsigned int pos)
{

	// find the frameobject with value @pos
	FrameObjects::iterator it;
	
	for(it = objects.begin(); it != objects.end(); it++)
	{
		if(it->second == pos)
		{
			logic->destroyObject(it->first);
			objects.erase(it);
			break;
		}
	}

	for(it = objects.begin(); it != objects.end(); it++)
	{
		if(it->second > pos)
		{
			it->second--;
		}
	}

}

void ShapeObjects::clearShapeNodes(ShapeAnimation* const animation)
{
	// clear all nodes

	if(animation)
	{
		for(FrameObjects::iterator it = objects.begin(); it != objects.end(); it++)
		{
			// tell logic to destroy the object with pointer at it->first
			logic->destroyObject(it->first);

			animation->removeNode(this->frame, it->second);
		}
	}
	else
	{
		for(FrameObjects::iterator it = objects.begin(); it != objects.end(); it++)
		{
			// tell logic to destroy the object with pointer at it->first
			logic->destroyObject(it->first);
		}
	}

	// clear the map so we don't have old values
	objects.clear();
}

const unsigned int ShapeObjects::getFrame() const
{
	return frame;
}

const unsigned int ShapeObjects::getSize() const
{
	return objects.size();
}

void ShapeObjects::setLayerVisibility(const bool visible)
{
	layer->setVisible(visible);
}

void ShapeObjects::activateObjects()
{
	for(FrameObjects::iterator it = objects.begin(); it != objects.end(); it++)
	{
		//if(!it->first)
			//std::cout << "sas" << std::endl;
		it->first->activateComponents();
	}
}

void ShapeObjects::deactivateObjects()
{
	for(FrameObjects::iterator it = objects.begin(); it != objects.end(); it++)
	{
		it->first->deactivateComponents();
	}
}

void ShapeObjects::fetchNodes(std::vector<Object*>& objs)
{
	for(FrameObjects::iterator it = objects.begin(); it != objects.end(); it++)
	{
		objs.push_back(it->first);
	}
}


void ShapeObjects::setGravity(const float gravity)
{
	this->gravity = gravity;
}

void ShapeObjects::setSpeed(const float speed)
{
	this->speed = speed;
}

void ShapeObjects::setDeath(const float death)
{
	this->death = death;
}


const float ShapeObjects::getGravity() const
{
	return gravity;
}

const float ShapeObjects::getSpeed() const
{
	return speed;
}

const float ShapeObjects::getDeath() const
{
	return death;
}


void ShapeObjects::scaleObjects(const int scale, std::vector<Object*>& objs)
{
	for(std::vector<Object*>::iterator it = objs.begin(); it != objs.end(); it++)
	{
		Object* object = (*it);

		if (objects.find(object) != objects.end())
		{
			PhysicsComponent* comp = object->findComponent<PhysicsComponent>();

			if (comp)
			{
				Vector3 normal = comp->position - Vector3(0,0,0);

				normal = normal*0.05f;

				if (scale > 0)
				{
					comp->position += normal;
				}
				else
				{
					comp->position -= normal;
				}	
			}
		}
	}
}

void ShapeObjects::rotateObjects(const int rotation, const Vector3& axis, std::vector<Object*>&objs)
{
	for(std::vector<Object*>::iterator it = objs.begin(); it != objs.end(); it++)
	{
		Object* object = (*it);

		if (objects.find(object) != objects.end())
		{
			PhysicsComponent* comp = object->findComponent<PhysicsComponent>();

			if (comp)
			{
				

				//normal = normal*0.01f;

				
				if (rotation > 0)
				{
					// fetch the normal
					Vector3 normal = comp->position - Vector3(0,0,0);

					// rotate the normal and update the objects position

					D3DXMATRIX m;
					D3DXMatrixIdentity(&m);

					//DANIEL WAS HERE
					if(axis.x > 0.0f)
					{
						D3DXMatrixRotationX(&m, (float)D3DX_PI * 0.01f);
					}
					else if(axis.y > 0.0f)
					{
						D3DXMatrixRotationY(&m, (float)D3DX_PI * 0.01f);
					}
					else if(axis.z > 0.0f)
					{
						D3DXMatrixRotationZ(&m, (float)D3DX_PI * 0.01f);
					}
					//DANIEL
					
					

					D3DXVec3TransformNormal((D3DXVECTOR3*)&normal, (D3DXVECTOR3*)&normal, &m);
					comp->position = Vector3(0,0,0) + normal;
				}
				else
				{
					// fetch the normal
					Vector3 normal = comp->position - Vector3(0,0,0);

					// rotate the normal and update the objects position

					D3DXMATRIX m;
					D3DXMatrixIdentity(&m);

					//DANIEL WAS HERE
					if(axis.x > 0.0f)
					{
						D3DXMatrixRotationX(&m, (float)D3DX_PI * -0.01f);
					}
					else if(axis.y > 0.0f)
					{
						D3DXMatrixRotationY(&m, (float)D3DX_PI * -0.01f);
					}
					else if(axis.z > 0.0f)
					{
						D3DXMatrixRotationZ(&m, (float)D3DX_PI * -0.01f);
					}
					//DANIEL

					D3DXVec3TransformNormal((D3DXVECTOR3*)&normal, (D3DXVECTOR3*)&normal, &m);
					comp->position = Vector3(0,0,0) + normal;
				}
			}
		}
	}
}