#ifndef EDITORSCENE_H

#define EDITORSCENE_H


#include "Scene.h"
#include "Common.h"
#include "EditorCamera.h"
#include "AnimationComponent.h"

class Editor;


const unsigned int START_LAYER_NR = 1000;
class EditorScene : public Scene
{
	typedef std::map<std::string, EditorCamera *> Cameras;
	typedef std::map<std::string, Layer *> Layers;
	typedef std::map<std::string, unsigned int> LayerPriority;
	typedef std::map<std::string, Viewport *> Viewports;

public:
	EditorScene();
	EditorScene(LogicHandler* handler, Renderer* rend, Window* wndw);

	~EditorScene();

	void update();

	void bindEditor(Editor * editor);
	void clear();
	void render();
	void present();
	void onEnter();							//Defines what should be done upon entering the scene

	bool onInput(int sym, int state);		// If return false give input to eventHandler
	bool onMouseMovement(int x, int y);		// If return false give input to eventHandler
				
	bool onCreateObject(Object* object);	//Do not call this manually!
	bool onDestroyObject(Object* object);	//Do not call this manually!

	EditorCamera* const addCamera(const std::string& name);
	EditorCamera* const getCamera(const std::string& name);

	Layer* const addLayer(const std::string& name, const unsigned int priority, Camera* camera,
		BaseShader* vs = NULL, BaseShader* gs = NULL, BaseShader* ps = NULL);


	Layer* const getLayer(const std::string& name);
	void setWorkingLayer(const std::string& name);
	Layer* const getWorkingLayer() const;
	void setWorkspace(const std::string& name);
	void toggleAllLayers(const bool visible);

	Viewport* const getViewport();
	
	Viewport* const getViewport(const std::string& name);

	void setLayers(const bool state);

private:
	Cameras cameras;

	Layers layers;

	LayerPriority prio;

	Viewports viewports;
	
	Layer* workspace;

	Editor * editor;

	bool show;

	Viewport* vp;
	
	AnimationComponent* animation;
};


#endif