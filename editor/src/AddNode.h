#ifndef ADD_NODE_H

#define ADD_NODE_H


#include "Action.h"

#include "RemoveNode.h"

#include "Editor.h"

class RemoveNode;
class Editor;


class AddNode : public Action
{
public:
	AddNode(Editor* const ed);
	~AddNode();

	void undo();
	void redo();

	void setBranches(const std::vector<int>& br);
	void setPosition(const Vector3& position);
	void setId(const int id);

	const Vector3 getPosition() const;
	const int getId() const;
	const std::string toString();

private:
	Editor* editor;

	std::vector<int> branches;

	Vector3 position;
	int id;
};


#endif