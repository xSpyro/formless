#ifndef SHAPE_ACTION_H

#define SHAPE_ACTION_H


#include "Action.h"



#include "Editor.h"

class Editor;


class ShapeAction : public Action
{
public:
	ShapeAction(Editor* const ed);
	ShapeAction(Editor* const ed, std::vector<unsigned int> f, std::vector<ShapeResult> r);
	~ShapeAction();

	void undo();
	void redo();

	const std::string toString();

private:
	std::vector<unsigned int> frames;
	std::vector<ShapeResult> results;
	Editor* editor;
};


#endif