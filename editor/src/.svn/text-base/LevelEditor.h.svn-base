#ifndef LEVELEDITOR_H
#define LEVELEDITOR_H

#include "Editor.h"
#include "RenderModule.h"
#include "RotationEvent.h"
#include "ScaleEvent.h"

enum LevelMessage
{
	LMESSAGE_MOVE_CAMERA,
	LMESSAGE_ROTATE_OBJECT_X,
	LMESSAGE_ROTATE_OBJECT_Y,
	LMESSAGE_ROTATE_OBJECT_Z,
	LMESSAGE_SCALE_OBJECT,
	LMESSAGE_PUT_OBJECT,
	LMESSAGE_DELETE_OBJECT,
	LMESSAGE_BRUSH,
	LMESSAGE_OPEN_FILE,
	LMESSAGE_SAVE_FILE,
	LMESSAGE_NEW_FILE,
	LMESSAGE_BRUSH_INTENSITY_TERRAIN,
	LMESSAGE_BRUSH_INTENSITY_SMOOTH,
	LMESSAGE_BRUSH_SIZE,
	LMESSAGE_ADD_LAYER,
	LMESSAGE_SHOW_LAYER,
	LMESSAGE_HIDE_LAYER,
	LMESSAGE_SET_WORK_LAYER,
	LMESSAGE_SAVE_LUA,
	LMESSAGE_RENAME_LUA,
	LMESSAGE_TOGGLE_EFFECT,
	LMESSAGE_SNAP_TO_TERRAIN,
	LMESSAGE_LEVEL_VALUE,
	LMESSAGE_SHIFT_HELD
};

enum TerrainSnapModes
{
	TERRAIN_SNAP_OFF,
	TERRAIN_SNAP_ON
};

enum Brush
{
	BRUSH_SAND,
	BRUSH_METAL,
	BRUSH_BRICK,
	BRUSH_DESERT,
	BRUSH_BLUR,
	BRUSH_LOWER,
	BRUSH_RAISE,
	BRUSH_SMOOTH,
	BRUSH_LEVEL
};

enum RotateX
{
	X_INCR_PRESS,
	X_INCR_RELEASE,
	X_DECR_PRESS,
	X_DECR_RELEASE
};

enum RotateY
{
	Y_INCR_PRESS,
	Y_INCR_RELEASE,
	Y_DECR_PRESS,
	Y_DECR_RELEASE
};

enum RotateZ
{
	Z_INCR_PRESS,
	Z_INCR_RELEASE,
	Z_DECR_PRESS,
	Z_DECR_RELEASE
};


enum RotateStates
{
    ROTATE_X_POS,
    ROTATE_X_NEG,
    ROTATE_Y_POS,
    ROTATE_Y_NEG,
    ROTATE_Z_POS,
    ROTATE_Z_NEG
};

enum Scale
{
	SCALE_UP_PRESS,
	SCALE_UP_RELEASE,
    SCALE_DOWN_PRESS,
	SCALE_DOWN_RELEASE
};

enum ScaleStates
{
	SCALE_UP,
    SCALE_DOWN
};



const float MAX_BRUSH_SIZE = 170.0f;


class LevelEditor : public Editor
{
public:
	
	LevelEditor();
	~LevelEditor();
	
	void init();

	bool startup(void * window, Renderer* renderer);
	bool shutdown();

	void update();

	void runFrame();

	void * getRenderTarget();

	void onInput(int key, int state);
	void onMouseMovement(int x, int y);

	void onCreateObject(Object * object);
	void onEnter();
	void onLoad();

	void ReceiveMessage(int id, int message);
	void ReceiveMessage(int id, std::string& message);
	void ReceiveMessage(int id, float message);
	void ReceiveMessage(int id, std::string& name, std::string& message);

	void createHeightMap();
	bool pickOnHeightmap(IntersectResult * res);
	bool updateMouseWorldPosition();

	void toVisible();
	void toWireframe();

	void hideLayer(std::string name);
	void showLayer(std::string name);

	void moveObjectRelativeCamera();

private:

	RenderModule renderMod;

	// Tools
	Picking picking;

	EditorCamera * camera;

	Model mouseModel;
	Object * mouseObject;
	Object * objectMarker;
	Vector3 mouseWorldPosition;
	Vector3 camToObject;

	int currentBrush;

	float smoothBrushIntensity;
	float terrainBrushIntensity;
	float brushScale;
	float levelBrushValue;

	Color currentBlend;
	
	//scaling, rotating objects, moving camera
	bool scaleState[2];
	bool moveState[4];
	bool rotationState[6];

	bool snapToTerrain;
	bool placingObject;
	bool shiftHeld;


	//rotation variables
	float rotationDelta;
	RotationEvent positiveX;
	RotationEvent negativeX;
	RotationEvent positiveY;
	RotationEvent negativeY;
	RotationEvent positiveZ;
	RotationEvent negativeZ;

	//scaling variables
	ScaleEvent scaleUp;
	ScaleEvent scaleDown;

};


#endif