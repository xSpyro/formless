#ifndef EDITOR_H
#define EDITOR_H

#include "Common.h"
#include "Picking.h"
#include "EditorCamera.h"
#include "EditorScene.h"
#include "AnimationSubsystem.h"

#include "SharedFormatStore.h"

#include "ShapeAction.h"



enum MovementMessages
{
	MOVE_LEFT_PRESS,
	MOVE_LEFT_RELEASE,
	MOVE_RIGHT_PRESS,
	MOVE_RIGHT_RELEASE,
    MOVE_UP_PRESS,
	MOVE_UP_RELEASE,
	MOVE_DOWN_PRESS,
	MOVE_DOWN_RELEASE
};

enum MovementStates
{
	MOVE_LEFT,
	MOVE_RIGHT,
	MOVE_UP,
	MOVE_DOWN
};

class GameHandler;

class Action;
class ShapeAction;
class RemoveNode;

class Editor : public Window
{

public:
	Editor();
	virtual ~Editor();

	virtual bool startup(void * window, Renderer* renderer);
	virtual bool shutdown();

	virtual void update() = 0;
	void run();

	virtual void runFrame() = 0;

	virtual void onInput(int key, int state) = 0;
	virtual void onMouseMovement(int x, int y) = 0;

	virtual void onCreateObject(Object * object) = 0;
	virtual void onEnter() = 0;

	void* getRenderTarget();

	Object* createObject(const std::string& name);

	EditorScene* getScene() const;

	virtual void ReceiveMessage(int id, int message) = 0;
	virtual void ReceiveMessage(int id, float message) = 0;
	virtual void ReceiveMessage(int id, std::string& message) = 0;
	virtual void ReceiveMessage(int id, std::string& name, std::string& message) = 0;
	
	virtual void saveCurrentActionState() { };
	virtual void restoreActionState(const std::vector<unsigned int>& frames, std::vector<ShapeResult>& results)	{ };

protected:
	Renderer* renderer;

	EditorScene* scene;

	// current layer
	Layer* workspace;

	// Current mouse-position and old mouse-position
	Vector2 mouse;
	Vector2 old;
	
	// Model thats currently picked
	Model* workmodel;

	PhysicsSubsystem* physics;
	RenderSubsystem* rend;
	ControllerSubsystem* controller;
	ScriptSubsystem* script;
	AnimationSubsystem* animation;
	LogicHandler* logic;
	GameHandler* game;

	SharedFormatStore formatStore;
};

#endif