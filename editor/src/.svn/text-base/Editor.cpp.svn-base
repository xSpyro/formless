#include "Editor.h"
#include "GameHandler.h"

#include "MemLeaks.h"

#include "ParticleAnimationSequenceFormat.h"
#include "LevelFormat.h"




Editor::Editor() : Window()
{
	game = NULL;
	physics = NULL;
	rend = NULL;
	controller = NULL;
	script = NULL;
	logic = NULL;
	renderer = NULL;
	scene = NULL;
}

Editor::~Editor()
{
}

bool Editor::startup(void * window, Renderer* renderer)
{
	// inits layer, viewport and subsystems
	FileSystem::setBaseDirectory("../resources/");

	this->renderer = renderer;
	if (window == NULL)
	{
		this->init(1280, 720, "Editor");
		this->open();
		renderer->join(this);
	}
	logic = New LogicHandler();
	game = New GameHandler(logic);

	//allows 10000 nodes in ShapeEditor animations(all nodes of all frames added together)
	physics = New PhysicsSubsystem(game, 10000);
	rend = New RenderSubsystem(game, 10000);
	controller  = New ControllerSubsystem();
	script = New ScriptSubsystem();

	animation = New AnimationSubsystem();


	scene = New EditorScene(logic, renderer, this);
	logic->createScene("EditorScene", scene);

	logic->registerSubsystem("Physics", physics);
	logic->registerSubsystem("Render", rend);
	logic->registerSubsystem("Controller", controller);
	logic->registerSubsystem("Script", script);
	logic->registerSubsystem("Animation", animation);
	GameConnector::setGame(game);

	
	//if(logic->registerObjectTemplate("Player"))
		//std::cout << "Found Player.lua" << std::endl;

	game->registerObjectTemplate("EditorBall");
	game->registerObjectTemplate("Box");
	game->registerObjectTemplate("OldBox");
	game->registerObjectTemplate("Ruin");

	game->registerObjectTemplate("Bone1");
	game->registerObjectTemplate("Bone2");
	game->registerObjectTemplate("Gate");
	game->registerObjectTemplate("Spire");
	game->registerObjectTemplate("Monument");
	game->registerObjectTemplate("Pillar2");
	game->registerObjectTemplate("Pillar3");
	game->registerObjectTemplate("Ruined");
	game->registerObjectTemplate("Skelle");
	game->registerObjectTemplate("Tile");
	game->registerObjectTemplate("TileFloor");
	game->registerObjectTemplate("Pillar");
	game->registerObjectTemplate("Stairs");
	game->registerObjectTemplate("Plate");
	game->registerObjectTemplate("BrokenPillar");

	game->registerObjectTemplate("StairsLong");
	game->registerObjectTemplate("StairsShort");
	game->registerObjectTemplate("StairsTurn");



	
	
	logic->activateScene("EditorScene");


	
	EditorCamera* camera = scene->getCamera("main");

	camera->setLookAt(Vector3(0,0,0));

	// register extensions
	formatStore.registerFormat("pas", New ParticleAnimationSequenceFormat(animation));
	formatStore.registerFormat("flf", New LevelFormat(game));


	return this->isOpen();

}
bool Editor::shutdown()
{
	// removes subsystems and renderer
	if(physics)
	{
		delete physics;
		physics = NULL;
	}
	if(rend)
	{
		delete rend;
		rend = NULL;
	}
	if(controller)
	{
		delete controller;
		controller = NULL;
	}
	if(script)
	{
		delete script;
		script = NULL;
	}
	if(renderer)
	{
		delete renderer;
		renderer = NULL;
	}
	
	return true;
}

void Editor::run()
{
	while (isOpen())
	{ 
		runFrame();


	}
}

void* Editor::getRenderTarget()
{
	return renderer->getSharedWPFSurface();
}

Object* Editor::createObject(const std::string& name)
{

	return logic->createObject(name);
}


EditorScene* Editor::getScene() const
{
	return scene;
}