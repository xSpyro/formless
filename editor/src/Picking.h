#ifndef PICKING_H
#define PICKING_H

#include "Common.h"

class Picking
{
	typedef std::vector<Renderable * > RenderableVector;
	typedef std::map<Renderable *, Renderable *> RenderableMap;
	typedef std::vector<Object * > ObjectVector;
public:
	Picking();
	~Picking();

	void update(const Vector2& mouse, Camera* const camera, const bool pickMany = true);
	void moveMany( const Vector3& direction);
	
	void pickObject(Layer* layer, const Vector2& mouse, Camera* const camera);
	void pickObject(Layer* layer, const Vector2& first, const Vector2& second, Camera* const camera);
	void pickObject(std::vector<Object*>& objects, const Vector2& first, const Vector2& second, Camera* const camera);
	const bool isObjectMarked() const;

	Object* getMarked() const;

	void unpick();
	void clearPicking();
	void clearSelected();
	void fetchLinks(ObjectVector& obj);
	void fetchSelected(ObjectVector& obj);
	void fetchRenderables(std::vector<Renderable*>& ren);

	bool hasSelected();

	bool hasPreviousSelected();
private:

	// Picked object, null if no object is picked
	Object* marked;

	RenderableVector prev;
	RenderableMap selected;

	Vector2 old;
};

#endif