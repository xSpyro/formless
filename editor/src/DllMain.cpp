
#include "DllMain.h"

#include "LevelEditor.h"
#include "ShapeEditor.h"
#include "ParticleEditor.h"

#include "MemLeaks.h"

LevelEditor * level;

ParticleEditor* particle;
ShapeEditor* shape;

Editor* current;

Renderer* renderer;

bool API EditorSetup(void * window)
{
	particle = New ParticleEditor();
	level = New LevelEditor();
	shape = New ShapeEditor();

	renderer = New Renderer;
	renderer->join((HWND)window, 1280, 720, RENDERER_ENABLE_WPF);

	
	particle->startup(window, renderer);
	particle->getScene()->setLayers(false);

	level->startup(window, renderer);
	level->getScene()->setLayers(false);

	shape->startup(window, renderer);
	shape->getScene()->setLayers(false);

	current = particle;

	InitMemoryCheck();
	return true;
}

void API EditorExit()
{
	current = NULL;
	if (particle)
	{
		particle->shutdown();
		delete particle;
		particle = NULL;
	}
	
	if (level)
	{
		level->shutdown();
		delete level;
		level = NULL;
	}

	if (shape)
	{
		shape->shutdown();
		delete shape;
		shape = NULL;
	}

	if (renderer)
	{
		delete renderer;
		renderer = NULL;
	}
	
	
}

void* API EditorGetRenderTarget()
{
	return current->getRenderTarget();
}

void API EditorRender()
{
	current->runFrame();
}

void API EditorOnInput(int sym, int state)
{
	current->injectInput(sym, state);
}

void API EditorOnMouseMove(float x, float y, float nx, float ny)
{
	current->injectMouseMovement((int)x, (int)y, nx, ny);
}

void API EditorSet(int editor)
{
	switch(editor)
	{
		case 0:
			current->getScene()->setLayers(false);
			current = particle;
			current->onEnter();
			current->getScene()->setLayers(true);
			break;
		case 1:
			current->getScene()->setLayers(false);
			current = level;
			current->onEnter();
			current->getScene()->setLayers(true);
			break;
		case 2:
			current->getScene()->setLayers(false);
			current = shape;
			current->getScene()->setLayers(true);
			current->onEnter();
			
			break;
	}
}

void API EditorCreateLayer(std::string name, unsigned int priority)
{
	//current->getScene()->addLayer(name, priority);
}

void API EditorSelectLayer(std::string name)
{
	current->getScene()->setWorkingLayer(name);
}

void API EditorSendMessage(int id, int message)
{
	current->ReceiveMessage(id, message);
}

void API EditorSendFloatMessage(int id, float message)
{
	current->ReceiveMessage(id, message);
}

void API EditorSendStringMessage(int id, std::string message)
{
	current->ReceiveMessage(id, message);
}

void API EditorSendTwoStringMessage(int id, std::string name, std::string message)
{
	current->ReceiveMessage(id, name, message);
}

const unsigned int API EditorGetMaxframe()
{
	return shape->creator()->getMaxframe();
}

const unsigned int API EditorGetCurrentPlayframe()
{
	return 0;//shape->creator()->getPlayingFrame();
}

void API EditorGetNextFrameString(std::string& frame)
{
	frame = shape->getNextFrameString();
}

float API EditorGetGravity()
{
	return shape->getGravityForFrame();
}

float API EditorGetSpeed()
{
	return shape->getSpeedForFrame();
}

float API EditorGetDeath()
{
	return shape->getDeathForFrame();
}

void API EditorSetParticleColor(const float r, const float g, const float b, const float a)
{
	shape->setParticleColor(r,g,b,a);
}

Col API EditorGetParticleColor()
{
	Color col = shape->creator()->getColor();
	Col c;

	c.r = col.r;
	c.g = col.g;
	c.b = col.b;
	c.a = col.a;
	
	return c;
}

// ParticleEditor swarm-functions

DLL float API EditorGetRandAngleX()
{
	return particle->getRandAngleX();
}
DLL float API EditorGetRandAngleY()
{
	return particle->getRandAngleY();
}
DLL float API EditorGetRandAngleZ()
{
	return particle->getRandAngleZ();
}

DLL int API EditorGetLife()
{
	return particle->getLife();
}
DLL float API EditorGetIntensityDir()
{
	return particle->getIntensityDir();
}
DLL float API EditorGetDiffusion()
{
	return particle->getDiffusion();
}

DLL float API EditorSpinMin()
{
	return particle->getSpinMin();
}
DLL float API EditorSpinMax()
{
	return particle->getSpinMax();
}

DLL float API EditorGetSpawnRate()
{
	return particle->getSpawnRate();
}
DLL float API EditorGetAffectRate()
{
	return particle->getAffectRate();
}
DLL float API EditorGetStartScale()
{
	return particle->getStartScale();
}
DLL float API EditorGetEndScale()
{
	return particle->getEndScale();
}

DLL float API EditorGetStartSpeed()
{
	return particle->getStartSpeed();
}
DLL float API EditorGetMiddleSpeed()
{
	return particle->getMiddleSpeed();
}
DLL float API EditorGetEndSpeed()
{
	return particle->getEndSpeed();
}
DLL float API EditorGetColorPath()
{
	return particle->getColorPath();
}
DLL float API EditorGetSpeedPath()
{
	return particle->getSpeedPath();
}
DLL float API EditorParticleGetGravity()
{
	return particle->getGravity();
}
DLL float API EditorGetDistorsion()
{
	return particle->getDistorsion();
}
DLL float API EditorGetDisplacementPos()
{
	return particle->getDisplacementPos();
}
DLL float API EditorGetLifeAffectRate()
{
	return particle->getLifeAffectRate();
}

DLL float API EditorGetStartColorR()
{
	return particle->getStartColorR();
}
DLL float API EditorGetStartColorG()
{
	return particle->getStartColorG();
}
DLL float API EditorGetStartColorB()
{
	return particle->getStartColorB();
}

DLL float API EditorGetMiddleColorR()
{
	return particle->getMiddleColorR();
}
DLL float API EditorGetMiddleColorG()
{
	return particle->getMiddleColorG();
}
DLL float API EditorGetMiddleColorB()
{
	return particle->getMiddleColorB();
}

DLL float API EditorGetEndColorR()
{
	return particle->getEndColorR();
}
DLL float API EditorGetEndColorG()
{
	return particle->getEndColorG();
}
DLL float API EditorGetEndColorB()
{
	return particle->getEndColorB();
}
DLL int API EditorGetNrAnimations()
{
	return particle->getNrAnimations();
}
DLL int API EditorGetNrEmitters()
{
	return particle->getNrEmitters();
}