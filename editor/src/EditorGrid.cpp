#include "EditorGrid.h"

#include "Framework.h"

EditorGrid::EditorGrid()
{
}

EditorGrid::~EditorGrid()
{
}


bool EditorGrid::init(Renderer* const renderer)
{
	this->renderer = renderer;

	BaseShader* gs = renderer->shader()->get("shaders/grid.gs");
	BaseShader* ps = renderer->shader()->get("shaders/grid.ps");
	BaseShader* vs = renderer->shader()->get("shaders/grid.vs");

	effect.blendModeSolidAlpha();

	effect.bind(vs, ps, gs);

	D3D11_BUFFER_DESC desc;
	D3D11_SUBRESOURCE_DATA data;

	if (gridPositions.empty())
		constructGridInXZPlane();

	desc.CPUAccessFlags = 0;
	desc.StructureByteStride = sizeof(GridVertex);
	desc.ByteWidth = sizeof(GridVertex) * gridPositions.size();
	desc.MiscFlags = 0;
	desc.Usage = D3D11_USAGE_IMMUTABLE;
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	data.pSysMem = &gridPositions[0];

	data.SysMemPitch = sizeof(GridVertex) * gridPositions.size();

	D3D11_INPUT_ELEMENT_DESC	gridPositionVertex[] = 
	{
		{	"POSITION",				0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	0,	D3D11_INPUT_PER_VERTEX_DATA,	0},
		{	"COLOR",				0,	DXGI_FORMAT_R32G32B32A32_FLOAT,	0,	12, D3D11_INPUT_PER_VERTEX_DATA,	0}
	};


	HRESULT hr = renderer->getDevice()->CreateInputLayout(gridPositionVertex, 2, vs->getCompiledChunk(), vs->getCompiledChunkSize() ,&inputLayout);
	if (FAILED(hr))
	{
		std::cout << "Failed to create InputLayout with message: " << hr << std::endl;
	}
	hr = renderer->getDevice()->CreateBuffer(&desc, &data, &vertexBuffer);

	if (FAILED(hr))
	{
		std::cout << "Failed to create buffer" << std::endl;
	}

	return false;
}	

void EditorGrid::render(const Color& blend, const float gradient)
{
	// MUST FIX SO NO MEMORY LEAKAGE
	static PerRenderable buffer;
	static ConstantBuffer * cbuffer = new ConstantBuffer(renderer, sizeof(buffer));

	// set constant buffer per renderable

	buffer.blend = blend;
	buffer.gradient.r = gradient;
	cbuffer->set(&buffer, sizeof(buffer));

	ID3D11Buffer * b = cbuffer->getBuffer();
	renderer->getContext()->VSSetConstantBuffers(6, 1, &b);
	renderer->getContext()->GSSetConstantBuffers(6, 1, &b);



	unsigned int stride = sizeof(GridVertex), offset = 0;
	
	renderer->getContext()->IASetInputLayout(inputLayout);
	renderer->getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	renderer->getContext()->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	renderer->getContext()->IASetIndexBuffer(NULL, DXGI_FORMAT_R32_UINT, 0);
	
	

	effect.apply();

	
	renderer->getContext()->Draw(gridPositions.size(),0);

} 



void EditorGrid::constructGridInXZPlane()
{

	std::cout << "in xz plane func" << std::endl;
	for (float x = -25; x < 25.0f; x+=1.0f)
	{
		for (float z = -25; z < 25.0f; z+=1.0f)
		{
			gridPositions.push_back(GridVertex(Vector3(x,0.0f,z), Color(0.3f,0.3f,1.0f,1.0f)));
		}
	}

	int c = 0;
	int d = 0;
	for (float x = -25; x < 24.0f; x+=0.2f)
	{

		if (d % 5 == 0)
		{
			d = 0;
		}
		else
		{
			for (float z = -25; z < 24.0f; z+=0.2f)
			{

				if (c % 5 == 0)
				{
					c = 0;
				}
				else
					gridPositions.push_back(GridVertex(Vector3(x,0.0f,z), Color(0,1,0,1.0f)));

				c++;
			}
		}

		d++;


	}



}


void EditorGrid::constructGridInXYPlane()
{
	for (float x = -25; x < 25.0f; x+=1.0f)
	{
		for (float y = -25; y < 25.0f; y+=1.0f)
		{
			gridPositions.push_back(GridVertex(Vector3(x,y,0.0f), Color(0.3f,0.3f,1.0f,1.0f)));
		}
	}

	int c = 0;
	int d = 0;
	for (float x = -25; x < 24.0f; x+=0.2f)
	{

		if ( d != 0 && d % 5 == 0)
		{
			d = 0;
		}
		else
		{
			for (float y = -25; y < 24.0f; y +=0.2f)
			{
			
				if ( c != 0 && c % 5 == 0)
				{
					c = 0;
				}
				else
					gridPositions.push_back(GridVertex(Vector3(x,y,0.0f), Color(0,1,0,1.0f)));

				c++;
			}
		}

		d++;


	}
}