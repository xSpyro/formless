#ifndef FILE_HANDLER_H

#define FILE_HANDLER_H

#include "Common.h"

#include <fstream>

#include "Framework.h"

#include "ShapeFile.h"

struct FileDesc
{
	std::string fileName;
	int size;
	int elements;
	char* data;
};

class FileHandler
{
public:

	// saves a file binary
	static bool writeBinary(const FileDesc& desc);

	// loads a file binary
	static bool readBinary(const FileDesc& desc);

	//
	static void readPartBinary(std::fstream& f, const FileDesc& desc, const int start);

	//
	static void writePartBinary(std::fstream& f, const FileDesc& desc, const int start);

	// 
	static int getBinareFileSize(const std::string& file);
	

};

#endif