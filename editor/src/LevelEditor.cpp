#include "LevelEditor.h"
#include "GameHandler.h"
#include "RenderComponent.h"
#include "PositionEvent.h"
#include "PhysicsComponent.h"
#include "MemLeaks.h"
#include "ScaleEvent.h"
#include "QueryEvent.h"
#include "ObjectTemplate.h"

LevelEditor::LevelEditor() : Editor()
{
	
}

LevelEditor::~LevelEditor()
{
}

void LevelEditor::init()
{
	scaleState[SCALE_UP] = false;
	scaleState[SCALE_DOWN] = false;

	moveState[MOVE_LEFT] = false;
	moveState[MOVE_RIGHT] = false;
	moveState[MOVE_UP] = false;
	moveState[MOVE_DOWN] = false;

	rotationState[ROTATE_X_POS] = false;
	rotationState[ROTATE_X_NEG] = false;
	rotationState[ROTATE_Y_POS] = false;
	rotationState[ROTATE_Y_NEG] = false;
	rotationState[ROTATE_Z_POS] = false;
	rotationState[ROTATE_Z_NEG] = false;

	mouseObject = NULL;
	snapToTerrain = true;
	placingObject = false;
	shiftHeld = false;

	currentBrush = 0;
	smoothBrushIntensity = 0.0f;
	terrainBrushIntensity = 0.0f;
	brushScale = 30.0f;
	rotationDelta = 0.1f;
	levelBrushValue = 0.0f;

	scaleUp.setScale(Vector3(1.02f, 1.02f, 1.02f) );
	scaleDown.setScale(Vector3(0.98f, 0.98f, 0.98f) );

	negativeX.setAngle( -rotationDelta ); 
	negativeX.setAxis( Vector3(1, 0, 0) );
	positiveX.setAngle( rotationDelta );
	positiveX.setAxis( Vector3(1, 0, 0) );

	negativeY.setAngle( -rotationDelta );
	negativeY.setAxis( Vector3(0, 1, 0) );
	positiveY.setAngle( rotationDelta );
	positiveY.setAxis( Vector3(0, 1, 0) );

	negativeZ.setAngle( -rotationDelta);
	negativeZ.setAxis( Vector3(0, 0, 1) );
	positiveZ.setAngle( rotationDelta);
	positiveZ.setAxis( Vector3(0, 0, 1) );

	
	camera = scene->getCamera("main");
	camera->setPosition(Vector3(0,150,-50.0f));
	camera->setLookAt(Vector3(0, 0, 0));
	camera->setClipRange(10, 2000);
	camera->setProjection3D(45, 16 / 9.0f);


	renderMod.init(renderer, RM_ALL);
	renderMod.build();

	renderMod.addCircleOfLinks();

	renderMod.setVisualScale(brushScale);

	scene->addLayer("Base Layer", 2, camera, renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));
	scene->getViewport("Base Layer")->getEffect()->blendModeSolid();


	scene->addLayer("Collision Boxes", 1, camera,  renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));
	
	Viewport * collisionvp = scene->getViewport("Collision Boxes");
	collisionvp->setVisible(true);
	collisionvp->getEffect()->blendModeWireframe();
	collisionvp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));


	scene->addLayer("wireframe", 10, camera, renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));
	
	Viewport * wireframevp = scene->getViewport("wireframe");
	wireframevp->setVisible(true);
	wireframevp->getEffect()->blendModeWireframe();
	wireframevp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));
	


	//objects will be added to this layer by default
	scene->setWorkspace("Base Layer");
	
}

bool LevelEditor::startup(void * window, Renderer* renderer)
{
	Editor::startup(window, renderer);

	init();

	if(window)
		return true;
	return this->isOpen();
}

bool LevelEditor::shutdown()
{
	return false;
}

void LevelEditor::update()
{
	
	updateMouseWorldPosition();


	// Svorak layout
	//THIS IS FOR WHEN RUNNING Main.cpp!!!
	//if (isInputDown('A') )
	//{
	//	camera->strafe(2.0f);
	//}

	//if (isInputDown('O') || isInputDown('S'))
	//{
	//	camera->move(-2.0f);
	//}

	//if (isInputDown('E') || isInputDown('D'))
	//{
	//	camera->strafe(-2.0f);
	//}

	//if (isInputDown(222) || isInputDown('W'))
	//{
	//	camera->move(2.0f);
	//}

	//if(isInputDown(27))
	//{
	//	this->close();
	//}

	//if(isInputDown('R') )
	//{
	//	currentBrush = BRUSH_SAND;
	//}
	//if(isInputDown('L') )
	//{
	//	currentBrush = BRUSH_BRICK;
	//}

	//if(mouseObject != NULL)
	//{

	//	if(isInputDown(37) )
	//	{
	//		mouseObject->send( &positiveY );
	//	}
	//	else if(isInputDown(39) )
	//	{
	//		mouseObject->send( &negativeY );
	//	}

	//	if(isInputDown(38) )
	//	{
	//		mouseObject->send( &scaleUp );
	//	}
	//	else if(isInputDown(40) )
	//	{
	//		mouseObject->send( &scaleDown );
	//	}

	//}


	////CAMERA MOVEMENTS
	if(moveState[MOVE_LEFT])
	{
		this->camera->strafe(4.0f);

	}
	if(moveState[MOVE_RIGHT])
	{
		this->camera->strafe(-4.0f);

	}
	if(moveState[MOVE_UP])
	{
		this->camera->move(4.0f);

	}
	if(moveState[MOVE_DOWN])
	{
		this->camera->move(-4.0f);

	}

	if( mouseObject != NULL)
	{

		//OBJECT ROTATIONS
		if( rotationState[ROTATE_X_POS])
		{
			mouseObject->send( &positiveX );
		}
		if( rotationState[ROTATE_X_NEG])
		{
			mouseObject->send( &negativeX);
		}
		if( rotationState[ROTATE_Y_POS])
		{
			mouseObject->send( &positiveY );
		}
		if( rotationState[ROTATE_Y_NEG])
		{
			mouseObject->send( &negativeY );
		}
		if( rotationState[ROTATE_Z_POS])
		{
			mouseObject->send( &positiveZ );
		}
		if( rotationState[ROTATE_Z_NEG])
		{
			mouseObject->send( &negativeZ );
		}
	
		//OBJECT SCALING
		if( scaleState[SCALE_UP] )
		{
			mouseObject->send( &scaleUp );
		}
		if( scaleState[SCALE_DOWN] )
		{
			mouseObject->send( &scaleDown );
		}
	}

	if(isInputDown(602))
	{
		IntersectResult res2;

		int halfHeightMapSize = (int)(renderMod.getHeightmapSize() * 0.5f);

		int xMax = (int)(halfHeightMapSize - brushScale - 15.0f);
		int xMin = (int)(-halfHeightMapSize + brushScale + 15.0f);

		int zMax = (int)(halfHeightMapSize - brushScale - 15.0f);
		int zMin = (int)(-halfHeightMapSize + brushScale + 15.0f);
		
		if( mouseWorldPosition.x < xMax && mouseWorldPosition.x > xMin && mouseWorldPosition.z < zMax && mouseWorldPosition.z > zMin)
		{
			
			if(pickOnHeightmap( &res2))
			{

				float newX = res2.u * renderMod.getTerrainSurface().getWidth();
				float newY = res2.v * renderMod.getTerrainSurface().getHeight();
			
				float colorX = res2.u * renderMod.getBlendTexture().getWidth();
				float colorY = renderMod.getBlendTexture().getHeight() - res2.v * renderMod.getBlendTexture().getHeight();
				

				std::cout << newX << " " << newY << std::endl;
				std::cout << colorX << " " << colorY << std::endl;

				float f = brushScale/renderMod.getHeightmapSize() * renderMod.getBlendTexture().getHeight();
				float v = brushScale/renderMod.getHeightmapSize() * renderMod.getTerrainSurface().getWidth();


				if(currentBrush == BRUSH_RAISE)
				{
					renderMod.getTerrainSurface().draw( (int)newX, (int)newY, v, Color(terrainBrushIntensity, 0.0f, 0.0f, 0.0f), smoothBrushIntensity);
					renderMod.updateTerrain();
				}
				else if(currentBrush == BRUSH_LOWER)
				{
					renderMod.getTerrainSurface().draw( (int)newX, (int)newY, v, Color(-terrainBrushIntensity, 0.0f, 0.0f, 0.0f), smoothBrushIntensity);
					renderMod.updateTerrain();
				}
				else if(currentBrush == BRUSH_SMOOTH)
				{
					renderMod.getTerrainSurface().blur((int)(newX), (int)(newY), v, smoothBrushIntensity);
					renderMod.updateTerrain();
				}
				else if(currentBrush == BRUSH_SAND)
				{
					renderMod.getBlendTexture().begin().draw((int)colorX, (int)colorY, f, Color(terrainBrushIntensity, -terrainBrushIntensity, -terrainBrushIntensity, -terrainBrushIntensity), 0.01f);
					renderMod.getBlendTexture().begin().clamp();
					renderMod.getBlendTexture().update();
				}
				else if(currentBrush == BRUSH_METAL)
				{
					renderMod.getBlendTexture().begin().draw((int)colorX, (int)colorY, f, Color(-terrainBrushIntensity, terrainBrushIntensity, -terrainBrushIntensity, -terrainBrushIntensity), 0.01f);
					renderMod.getBlendTexture().begin().clamp();
					renderMod.getBlendTexture().update();
				}
				else if(currentBrush == BRUSH_BRICK)
				{
					renderMod.getBlendTexture().begin().draw((int)colorX, (int)colorY, f, Color(-terrainBrushIntensity, -terrainBrushIntensity, terrainBrushIntensity, -terrainBrushIntensity), 0.01f);
					renderMod.getBlendTexture().begin().clamp();
					renderMod.getBlendTexture().update();
				}

				else if(currentBrush == BRUSH_DESERT)
				{
					renderMod.getBlendTexture().begin().draw((int)colorX, (int)colorY, f, Color(-terrainBrushIntensity, -terrainBrushIntensity, -terrainBrushIntensity, terrainBrushIntensity), 0.01f);
					renderMod.getBlendTexture().begin().clamp();
					renderMod.getBlendTexture().update();
				}
				else if(currentBrush == BRUSH_BLUR)
				{
					renderMod.getBlendTexture().begin().blur((int)colorX, (int)colorY, f, smoothBrushIntensity);
					renderMod.getBlendTexture().begin().clamp();
					renderMod.getBlendTexture().update();
				}
				else if(currentBrush == BRUSH_LEVEL)
				{
					renderMod.getTerrainSurface().drawToValue( (int)newX, (int)newY, v, Color(levelBrushValue, 0.0f, 0.0f, 0.0f));
					renderMod.updateTerrain();
				}

			}
		}
	}

	camera->update();

	game->update();

	renderMod.updateCamera(*camera);
	renderMod.update();

	renderMod.adjustCircleToTerrain();
}

void LevelEditor::runFrame()
{
	// call process before update, since we want keyinputs
	process();

	update();

	renderMod.preFrame();

	scene->render();

	scene->present();
}


void LevelEditor::onInput(int key, int state)
{
	std::cout << key << std::endl;

	if (key == 610)
	{
		//state negative scroll down, positive up
		if(state < 0  &&  brushScale > 5.0f )
		{
			brushScale -= 5.0f;
		}
		else if(state > 0 )
		{
			brushScale += 5.0f;
		}

		renderMod.setVisualScale(brushScale);
	}




	// BEGIN        DEBUGGING STUFF    DEBUGGING STUFF    DEBUGGING STUFF    DEBUGGING STUFF    DEBUGGING STUFF

	//if ( key == 66 && state == 1 )
	//{
	//	mouseObject = game->createObjectExt("Bone1", mouseWorldPosition, mouseWorldPosition);
	//	placingObject = true;
	//	//make it wireframe!
	//	toWireframe();
	//}
	//
	//if ( key == 78 && state == 1 )
	//{
	//	mouseObject = game->createObjectExt("Monument", mouseWorldPosition, mouseWorldPosition);
	//	placingObject = true;
	//	//make it wireframe!
	//	toWireframe();
	//}

	//if ( key == 77 && state == 1 )
	//{
	//	mouseObject = game->createObjectExt("BrokenPillar", mouseWorldPosition, mouseWorldPosition);
	//	placingObject = true;
	//	//make it wireframe!
	//	toWireframe();
	//}
	

	////o-key = open
	//if( key == 79 && state == 1)
	//{
	//	formatStore.importFromFile("GraveOfAnht.flf");
	//	renderMod.updateTerrain();
	//}

	////k-key = save..
	//if( key == 75 && state == 1)
	//{
	//	if(!formatStore.exportToFile("GraveOfAnht.flf") )
	//	{
	//		std::cout << "HOLYSHITSOMETHINGBADHAPPENED" << std::endl;
	//	}
	//}


	////r-key  = remove
	//if( key == 82 && state == 1)
	//{
	//	if(mouseObject)
	//	{
	//		game->destroyObject(mouseObject);
	//		mouseObject = NULL;
	//	}
	//}

	//if ( key == 80 && state == 1 )
	//{
	//	game->createObjectExt("Ruin", mouseWorldPosition, mouseWorldPosition);
	//}

	// END                 DEBUGGING STUFF    DEBUGGING STUFF    DEBUGGING STUFF    DEBUGGING STUFF    DEBUGGING STUFF





	if (key == 600 && state == 1) //press
	{
		if(!shiftHeld)
		{
			placingObject = false;

			Object* tempObj = NULL;
			Layer* layer = scene->getWorkingLayer();

			picking.pickObject(layer, mouse, camera);

			tempObj = picking.getMarked();

			if(mouseObject != NULL && tempObj == NULL)
			{
				//if mouseObject is going to be NULL, better remove related object from wireframe-layer then
				toVisible();
				mouseObject = tempObj;
			}

			//selecting an object when one already is selected
			if(mouseObject != NULL && tempObj != NULL && tempObj != mouseObject)
			{
				toVisible();
				mouseObject = tempObj;
			}

			if(mouseObject == NULL)
			{
				mouseObject = tempObj;
			}
		
			if( mouseObject)
			{
				//QueryEvent<Vector3> qe = QueryEvent<Vector3>(QUERY_POSITION);
				//mouseObject->send( &qe );

				//camToObject =  qe.getData() - camera->getPosition();

				toWireframe();
			}
		}
		else if(shiftHeld && mouseObject != NULL)
		{
			//if lmouse pressed while mouseObject != NULL and shiftHeld == true
			//create a copy of mouseObject which is placed where mouseWorldPos is now!
			ObjectTemplate * tempTemp = mouseObject->getTemplate();
			PositionEvent	posEv;
			ScaleEvent		scaEv;
			RotationEvent	rotEv;

			QueryEvent<Vector3> qPos = QueryEvent<Vector3>(QUERY_POSITION);
			QueryEvent<Vector3> qSca = QueryEvent<Vector3>(QUERY_SCALE);
			QueryEvent<Quaternion> qRot = QueryEvent<Quaternion>(QUERY_ROTATION);

			mouseObject->send( &qPos );
			mouseObject->send( &qSca );
			mouseObject->send( &qRot );

			posEv.setPosition(qPos.getData());
			scaEv.setScale(qSca.getData());
			scaEv.setApply(false);

			Quaternion quat = qRot.getData();
			Vector3 tAxis = Vector3(0,0,0);
			float	tAngle = 0.0f;
			quat.getAxisAngle(tAxis, tAngle);

			rotEv.setApply(false);
			rotEv.setAngle(tAngle);
			rotEv.setAxis(tAxis);
			
			Object * copyObject = game->createObjectExt(tempTemp->name, mouseWorldPosition, mouseWorldPosition);

			copyObject->send(&posEv);
			copyObject->send(&scaEv);
			copyObject->send(&rotEv);


		}

	}
	else if (key == 600 && state == 0) //release
	{

	}

}

void LevelEditor::toVisible()
{

	RenderComponent * tempCo = mouseObject->findComponent<RenderComponent>();

	if(tempCo == NULL)
	{
		return;
	}

	scene->getWorkingLayer()->add(&tempCo->model);
	scene->getLayer("wireframe")->remove( &tempCo->model );


}

void LevelEditor::toWireframe()
{
	
	RenderComponent * tempCo = mouseObject->findComponent<RenderComponent>();

	if(tempCo == NULL)
	{
		return;
	}
	
	scene->getLayer("wireframe")->add( &tempCo->model);
	scene->getWorkingLayer()->remove( &tempCo->model);
}

void LevelEditor::onMouseMovement(int x, int y)
{
	// Save old mouse-position and get the new one
	old = mouse;
	
	this->getMouseVector(&mouse.x, &mouse.y);


	if(isInputReleased(600))
	{

	}

	if(isInputDown(600) && picking.isObjectMarked() )
	{
		//std::cout << "gonna call update now!" << std::endl;
		
		if(snapToTerrain)
		{
			if( mouseObject)
			{
				PositionEvent posEv;
				//RotationEvent rotEv;

				//Vector3 up = Vector3(0.0f, 1.0f, 0.0f);

				//Vector3 fwd = up.cross(res.normal);

				
				posEv.setPosition(mouseWorldPosition );
				mouseObject->send(&posEv);
			}
		}
		else
		{
			picking.update(mouse, camera, false);
		}
		
	}

	if (isInputDown(600) && !picking.isObjectMarked())
	{
		Vector2 rot = mouse - old;
		
		camera->rotate(-rot.y, rot.x, 0.0f);
		
		camera->updateAsLevelCamera();
	}
	
}


bool LevelEditor::pickOnHeightmap(IntersectResult * res)
{
	Ray ray;

	Vector2 mouseVector;

	this->getMouseVector(&mouseVector.x, &mouseVector.y);

	camera->pick(mouseVector, &ray);

	Renderable * rend = (Renderable*)renderMod.getTerrainLayer()->pick(ray, res);

	if(rend)
	{
		//Mouse hits height map

		return true;
	}

	return false;
}

bool LevelEditor::updateMouseWorldPosition()
{
	Ray ray;

	Vector2 mouseVector;

	this->getMouseVector(&mouseVector.x, &mouseVector.y);

	camera->pick(mouseVector, &ray);

	IntersectResult res;
	Renderable* rend = (Renderable*)renderMod.getTerrainLayer()->pick(ray, &res);


	if(rend)
	{
		//Mouse hits height map
		Vector3 resultpos = ray.origin + ray.direction * res.dist;

		mouseWorldPosition = resultpos;

		renderMod.setPlayerPosition(mouseWorldPosition);


		if(placingObject)
		{
			PositionEvent posEv;
	
			posEv.setPosition(mouseWorldPosition );
			mouseObject->send(&posEv);

		}

		return true;
	}
	else
	{
		//Mouse is outside of height map

		camera->pick(mouseVector, &ray);

		Plane plane;
		plane.a = 0;
		plane.b = 1;
		plane.c = 0;
		plane.d = 10;

		if(Intersect::intersect(ray, plane, &res))
		{
			Vector3 resultpos = ray.origin + ray.direction * res.dist;

			mouseWorldPosition = resultpos;

			//Temporarily set a model at that position, draw a sphere
			Matrix* mat = &mouseModel.getTransform();

			mat->m[12] = resultpos.x;
			mat->m[13] = resultpos.y;
			mat->m[14] = resultpos.z;
			//End of temporary
		}

		return false;
	}
}

void LevelEditor::ReceiveMessage(int id, int message)
{
	std::cout << "Received message with id: " << id << " and message: "<< message << std::endl;

	switch(id)
	{
	case LMESSAGE_MOVE_CAMERA:
		switch(message)
		{
		case MOVE_LEFT_PRESS:
			moveState[MOVE_LEFT] = true;

			break;
		case MOVE_LEFT_RELEASE:
			moveState[MOVE_LEFT] = false;

			break;
		case MOVE_RIGHT_PRESS:
			moveState[MOVE_RIGHT] = true;

			break;
		case MOVE_RIGHT_RELEASE:
			moveState[MOVE_RIGHT] = false;

			break;
		case MOVE_UP_PRESS:
			moveState[MOVE_UP] = true;

			break;
		case MOVE_UP_RELEASE:
			moveState[MOVE_UP] = false;

			break;
		case MOVE_DOWN_PRESS:
			moveState[MOVE_DOWN] = true;

			break;
		case MOVE_DOWN_RELEASE:
			moveState[MOVE_DOWN] = false;

			break;

		default:
			break;
		}

		break;

	case LMESSAGE_ROTATE_OBJECT_X:
		switch(message)
		{
		case X_INCR_PRESS: 
			rotationState[ROTATE_X_POS] = true;
			break;
		case X_INCR_RELEASE:
			rotationState[ROTATE_X_POS] = false;
			break;

		case X_DECR_PRESS:
			rotationState[ROTATE_X_NEG] = true;
			break;

		case X_DECR_RELEASE:
			rotationState[ROTATE_X_NEG] = false;
			break;

		default:
			break;
		}
		break;

	case LMESSAGE_ROTATE_OBJECT_Y:
		switch(message)
		{
		case Y_INCR_PRESS:
			rotationState[ROTATE_Y_POS] = true;
			break;

		case Y_INCR_RELEASE:
			rotationState[ROTATE_Y_POS] = false;
			break;

		case Y_DECR_PRESS:
			rotationState[ROTATE_Y_NEG] = true;
			break;

		case Y_DECR_RELEASE:
			rotationState[ROTATE_Y_NEG] = false;
			break;
			
		default:
			break;
		}

		break;

	case LMESSAGE_ROTATE_OBJECT_Z:

		switch(message)
		{
		case Z_INCR_PRESS:
			rotationState[ROTATE_Z_POS] = true;
			break;

		case Z_INCR_RELEASE:
			rotationState[ROTATE_Z_POS] = false;
			break;

		case Z_DECR_PRESS:
			rotationState[ROTATE_Z_NEG] = true;
			break;

		case Z_DECR_RELEASE:
			rotationState[ROTATE_Z_NEG] = false;
			break;
			
		default:
			break;
		}

		break;

	case LMESSAGE_SCALE_OBJECT:
		switch(message)
		{
		case SCALE_UP_PRESS:
			scaleState[SCALE_UP] = true;
			break;

		case SCALE_UP_RELEASE:
			scaleState[SCALE_UP] = false;
			break;

		case SCALE_DOWN_PRESS:
			scaleState[SCALE_DOWN] = true;
			break;

		case SCALE_DOWN_RELEASE:
			scaleState[SCALE_DOWN] = false;
			break;
		default:
			break;
		}

		break;

	case LMESSAGE_DELETE_OBJECT:
		if(mouseObject)
		{
			game->destroyObject(mouseObject);
			mouseObject = NULL;
		}
		break;

	case LMESSAGE_BRUSH:
		currentBrush = message;
		break;

	case LMESSAGE_BRUSH_INTENSITY_TERRAIN:
		terrainBrushIntensity = (float) message/1000.0f;
		break;

	case LMESSAGE_BRUSH_INTENSITY_SMOOTH:
		smoothBrushIntensity = (float) message/100.0f;
		break;
		
	case LMESSAGE_BRUSH_SIZE:
		brushScale = (float) message * MAX_BRUSH_SIZE/100.0f;
		renderMod.setVisualScale(brushScale);

		break;


	case LMESSAGE_SNAP_TO_TERRAIN:
		{
			switch(message)
			{
			case TERRAIN_SNAP_OFF:
				snapToTerrain = false;
				break;

			case TERRAIN_SNAP_ON:
				snapToTerrain = true;
				break;

			default:
				break;
			}
		}
		break;

	case LMESSAGE_SHIFT_HELD:
		if(message == 0) //released
		{
			shiftHeld = false;
			placingObject = false;
		}
		else //pressed
		{
			shiftHeld = true;
		}
		break;

	default:
		break;
	}

}	

void LevelEditor::ReceiveMessage(int id, std::string& message)
{
	std::cout << "Received message with id: " << id << " and message: "<< message << std::endl;

	switch(id)
	{
	case LMESSAGE_OPEN_FILE:
		
		scene->clear();
		scene->getLayer("Collision Boxes")->clear();
		scene->getLayer("Base Layer")->clear();
		scene->getLayer("wireframe")->clear();
		formatStore.importFromFile(message);
		renderMod.updateTerrain();
		break;

	case LMESSAGE_SAVE_FILE:
		{
			//HACK HACKY HACK HACK solution:
			//first point the filemanager to a different version of Box, without a render component
			//because we don't want a visible box in the saved file!
			File f;
			FileSystem::get("Box.lua", &f, true);
			game->getLuaFileManager()->addFile("objects/Box.lua", f, TYPE_OBJECT, true);


			if(!formatStore.exportToFile(message))
			{
				std::cout << "Failed to export file in LevelEditor!!!" << std::endl;
			}
			else
				std::cout << "exportToFile successful!" << std::endl;

			//after that, set back to the other version of box!
			FileSystem::get("objects/Box.lua", &f, true);
			game->getLuaFileManager()->addFile("objects/Box.lua", f, TYPE_OBJECT, true);


		}
		break;

	case LMESSAGE_NEW_FILE:
		break;

	case LMESSAGE_SHOW_LAYER:
		std::cout << "SHOW_LAYER " << message << std::endl;
		showLayer(message);
		break;

	case LMESSAGE_HIDE_LAYER:
		std::cout << "HIDE_LAYER " << message << std::endl;
		hideLayer(message);
		break;

	case LMESSAGE_ADD_LAYER:
		break;

	case LMESSAGE_SET_WORK_LAYER:
		std::cout << "SET_WORK_LAYER  " << message << std::endl;
		scene->setWorkspace(message);
		break;

	case LMESSAGE_TOGGLE_EFFECT:
		{
			if(message == "fog on")
			{
				renderMod.setFog(true);
			}
			else if(message == "fog off")
			{
				renderMod.setFog(false);
			}
			else if(message == "ssao on")
			{
				renderMod.setSSAO(true);
			}
			else if(message == "ssao off")
			{
				renderMod.setSSAO(false);
			}
			else if(message == "shadow on")
			{
				renderMod.setShadows(true);
			}
			else if(message == "shadow off")
			{
				renderMod.setShadows(false);
			}
		}
		break;
	
	case LMESSAGE_PUT_OBJECT:
		
		mouseObject = game->createObjectExt(message, mouseWorldPosition, mouseWorldPosition);


		placingObject = true;
		toWireframe();
		break;

	default:
		break;
	}
}

void LevelEditor::ReceiveMessage(int id, std::string& name, std::string& message)
{
	switch(id)
	{
	case LMESSAGE_SAVE_LUA:
		{
			std::cout << "save trigger!  name: " << name << "  message: " << message << std::endl;

			File f;
			f.data = new char[message.size()];
			f.size = message.size();

			memcpy(f.data, message.c_str(), sizeof(char) * message.size() );
			
			game->getLuaFileManager()->addFile(name, f, TYPE_UNKOWN, true); // TODO: change to support types properly
		}
		break;

	case LMESSAGE_RENAME_LUA:

		std::cout << "rename trigger! name:" << name << " message: " << message << std::endl;

		game->getLuaFileManager()->rename(name, message);
		break;

	default:
		break;
	}
	
}


void LevelEditor::ReceiveMessage(int id, float message)
{
	switch(id)
	{
	case LMESSAGE_LEVEL_VALUE:
		levelBrushValue = message;
		break;

	default:
		break;
	}
}

void LevelEditor::onCreateObject(Object * object)
{
	//RenderComponent * component = object->findComponent<RenderComponent>();

	//std::cout << "LevelEditor: onCreateObject" << std::endl;
	//if(component)
	//{
	//	renderMod.addObject(component);
	//	std::cout << "		-added component to layer!" << std::endl;
	//}
}

void LevelEditor::onEnter()
{
	scene->bindEditor(this);
	//scene->getWorkingLayer()->debug();

	
	std::cout << "onEnter!" << std::endl;
	renderMod.bind();
	game->setHeightMap(&renderMod.getTerrain());
	game->setBlendMap(&renderMod.getBlendTexture().begin());
}

void LevelEditor::onLoad()
{
	renderMod.updateTerrain();
	std::cout << "onLoad!" << std::endl;
}



void LevelEditor::hideLayer(std::string name)
{

	scene->getLayer(name)->setVisible(false);
}

void LevelEditor::showLayer(std::string name)
{
	scene->getLayer(name)->setVisible(true);
}

void LevelEditor::moveObjectRelativeCamera()
{

	if(mouseObject)
	{

		PositionEvent ev;
		ev.setPosition(camToObject + camera->getPosition());
		mouseObject->send(&ev);
	}
}
