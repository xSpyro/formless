#include "Boxing.h"


Boxing::Boxing()
	: visible(false)
{

}


Boxing::~Boxing()
{

}


bool Boxing::init(Renderer* const renderer)
{
	this->renderer = renderer;

	BaseShader* gs = renderer->shader()->get("shaders/boxing.gs");
	BaseShader* ps = renderer->shader()->get("shaders/boxing.ps");
	BaseShader* vs = renderer->shader()->get("shaders/boxing.vs");

	effect.bind(vs, ps, gs);

	D3D11_BUFFER_DESC desc;
	D3D11_SUBRESOURCE_DATA data;
	
	desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	desc.StructureByteStride = sizeof(Vector3) * 2;
	desc.ByteWidth = sizeof(Vector3) * 2;
	desc.MiscFlags = 0;
	desc.Usage = D3D11_USAGE_DYNAMIC;
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	data.pSysMem = &mouse[0];

	data.SysMemPitch = sizeof(Vector3) * 2;

	D3D11_INPUT_ELEMENT_DESC	mouseVertex[] = 
	{
		{	"POSITION",				0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	0,	D3D11_INPUT_PER_VERTEX_DATA,	0},
		{	"SECOND_POSITION",		0,	DXGI_FORMAT_R32G32B32_FLOAT,    0,  12,	D3D11_INPUT_PER_VERTEX_DATA,	0},
	};

	
	HRESULT hr = renderer->getDevice()->CreateInputLayout(&mouseVertex[0], 2, vs->getCompiledChunk(), vs->getCompiledChunkSize(), &inputLayout);
	if (FAILED(hr))
	{
		std::cout << "Failed to create InputLayout" << std::endl;
	}
	hr = renderer->getDevice()->CreateBuffer(&desc, &data, &vertexBuffer);

	if (FAILED(hr))
	{
		std::cout << "Failed to create buffer" << std::endl;
	}

	return true;
}

void Boxing::updateMousePositions(const Vector3& first, const Vector3& second)
{
	mouse[0] = first;
	mouse[1] = second;

	Vector3* positions = NULL;

	D3D11_MAPPED_SUBRESOURCE resource;
	renderer->getContext()->Map(vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

	positions = (Vector3*)resource.pData;

	positions[0] = first;
	positions[1] = second;
	renderer->getContext()->Unmap(vertexBuffer, 0);

}

void Boxing::render()
{

	unsigned int stride = sizeof(Vector3)*2, offset = 0;

	renderer->getContext()->IASetInputLayout(inputLayout);
	renderer->getContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	renderer->getContext()->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	renderer->getContext()->IASetIndexBuffer(NULL, DXGI_FORMAT_R32_UINT, 0);

	effect.apply();

	renderer->getContext()->Draw(1,0);

}

const bool Boxing::isVisible() const
{
	return visible;
}
void Boxing::setVisible(const bool v)
{
	visible = v;
}
