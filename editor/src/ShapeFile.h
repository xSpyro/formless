#ifndef SHAPE_FILE_H

#define SHAPE_FILE_H

#include "Common.h"
#include "ShapeAnimation.h"
#include "FileHandler.h"
#include "ShapeCreator.h"
#include "EditorScene.h"

class ShapeFile
{
	// This is the header, 64bytes 
	// an integer of 4 bytes, and 60bytes unused data
	struct ShapeHeader
	{
		int nr;
		char args[60];
	};

public:
	static bool loadShapeFile(const std::string& file, ShapeAnimation& animation, ShapeCreator* creator = NULL, EditorScene* scene = NULL);
	static bool saveShapeFile(const std::string& file, ShapeAnimation& animation);

	// Load all points from an OBJ-file
	static bool loadPointsOBJ(const std::string& file, ShapeCreator* const creator);

	static const Vector3 loadPoint(std::fstream& f);
};

#endif