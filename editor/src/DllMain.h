
#ifndef DLLMAIN_H
#define DLLMAIN_H

#define DLL extern "C" __declspec(dllexport)
#define API __stdcall

/*
DLLEXPORT bool API EditorSetup(void * window);
DLLEXPORT void* API EditorGetRenderTarget();
DLLEXPORT void API EditorRender();

DLLEXPORT void API EditorOnMouseMove(float x, float y, float nx, float ny);
DLLEXPORT void API EditorOnInput(int sym, int state);
*/

#include "Runtime.h"

#endif