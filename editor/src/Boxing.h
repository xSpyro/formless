#ifndef BOXING_H

#define BOXING_H

#include "Common.h"

class Boxing
{
public:
	Boxing();
	~Boxing();

	bool init(Renderer* const renderer);

	void updateMousePositions(const Vector3& first, const Vector3& second);
	void render();

	const bool isVisible() const;

	void setVisible(const bool v);

private:
	Effect effect;

	Renderer* renderer;

	ID3D11Buffer* vertexBuffer;

	Vector3 mouse[2];

	ID3D11InputLayout* inputLayout;

	bool visible;

};

#endif