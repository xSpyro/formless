
#include <string>

struct Col
{
	float r,g,b,a;
};

DLL bool API EditorSetup(void * window);
DLL void API EditorExit();
DLL void* API EditorGetRenderTarget();
DLL void API EditorRender();

DLL void API EditorOnMouseMove(float x, float y, float nx, float ny);
DLL void API EditorOnInput(int sym, int state);
DLL void API EditorSet(int editor);
DLL void API EditorSendMessage(int id, int message);
DLL void API EditorSendStringMessage(int id, std::string message);
DLL void API EditorSendTwoStringMessage(int id, std::string name, std::string message);
DLL void API EditorSendFloatMessage(int id, float message);

DLL const unsigned int API EditorGetMaxframe();
DLL const unsigned int API EditorGetCurrentPlayframe();

DLL void API EditorGetNextFrameString(std::string& frame);

DLL float API EditorGetGravity();
DLL float API EditorGetSpeed();
DLL float API EditorGetDeath();

DLL void API EditorSetParticleColor(const float r, const float g, const float b, const float a);
DLL Col API EditorGetParticleColor();

// ParticleEditor swarm-functions

DLL float API EditorGetRandAngleX();
DLL float API EditorGetRandAngleY();
DLL float API EditorGetRandAngleZ();

DLL int API EditorGetLife();
DLL float API EditorGetIntensityDir();
DLL float API EditorGetDiffusion();

DLL float API EditorSpinMin();
DLL float API EditorSpinMax();

DLL float API EditorGetSpawnRate();
DLL float API EditorGetAffectRate();
DLL float API EditorGetStartScale();
DLL float API EditorGetEndScale();

DLL float API EditorGetStartSpeed();
DLL float API EditorGetMiddleSpeed();
DLL float API EditorGetEndSpeed();
DLL float API EditorGetColorPath();
DLL float API EditorGetSpeedPath();
DLL float API EditorParticleGetGravity();
DLL float API EditorGetDistorsion();
DLL float API EditorGetDisplacementPos();
DLL float API EditorGetLifeAffectRate();

DLL float API EditorGetStartColorR();
DLL float API EditorGetStartColorG();
DLL float API EditorGetStartColorB();

DLL float API EditorGetMiddleColorR();
DLL float API EditorGetMiddleColorG();
DLL float API EditorGetMiddleColorB();

DLL float API EditorGetEndColorR();
DLL float API EditorGetEndColorG();
DLL float API EditorGetEndColorB();

DLL int API EditorGetNrAnimations();
DLL int API EditorGetNrEmitters();