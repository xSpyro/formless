#ifndef ACTION_H

#define ACTION_H

#include "Common.h"

class Action
{
public:
	Action();
	virtual ~Action();

	virtual void undo() = 0;

	virtual void redo() = 0;

	virtual const std::string toString() = 0;


};

#endif