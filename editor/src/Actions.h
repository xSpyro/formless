#ifndef ACTIONS_H

#define ACTIONS_H

#include "Action.h"


class Actions
{
public:
	typedef std::vector<Action*> ActionStore;

	// 
	Actions();
	~Actions();

	void pushAction(Action* const action);

	void undoLatestAction();

	void redoLatestAction();

private:

	// stack
	ActionStore actions;

	int stackPointer;

};


#endif