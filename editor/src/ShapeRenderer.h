#ifndef SHAPERENDERER_H

#define SHAPERENDERER_H

#include "Common.h"
#include "ShapeNode.h"

class ShapeRenderer
{
public:
	ShapeRenderer();
	
	ShapeRenderer(Layer* const layer, LogicHandler* const logic);

	~ShapeRenderer();

	void updateNodes(Vector3* nodes, unsigned int nrOfNodes);

	Layer* getLayer() const;

	void setActive(const bool active);

	const bool isActive() const;

private:
	LogicHandler* logic;
	Layer* layer;
	std::vector<Object*> objects;

	bool active;
};

#endif