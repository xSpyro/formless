#include "ShapeEditor.h"

#include "MemLeaks.h"

#include "RenderComponent.h"

#include "QueryEvent.h"

ShapeEditor::ShapeEditor() : Editor()
{
	grid = NULL;

	shapeRend = NULL;
	
	tool = TOOL_PICKING;

	arrows = NULL;
	particles = true;
	doubleLinkage = false;
	linking = true;

	first = Vector2(0.0f,0.0f);

	mouseAlignment = MOUSE_ALIGNMENT_NONE;
}

ShapeEditor::~ShapeEditor()
{
}

bool ShapeEditor::startup(void * window, Renderer* renderer)
{
	Editor::startup(window, renderer);

	BaseShader* vshader = renderer->shader()->get("shaders/SkySphere.vs");
	
	BaseShader* pshader = renderer->shader()->get("shaders/SkySphere.ps");

	EditorCamera* camera = scene->getCamera("main");


	// width, height, znear, zfar

	//camera->setProjectionOrtho(1280, 800, 1, 1000);
	//camera->setClipRange(1, 10000);
	//if(vshader && pshader)
	{
		
		scene->addLayer("play", 60001, scene->getCamera("main"));

		
		// animations
		shapeRend = New ShapeRenderer(scene->getLayer("play"), logic);

		shape = New ShapeCreator(&picking, game, shapeRend);

		renderMod.init(renderer, RM_PARTICLES);
		renderMod.build();
		renderMod.addLinkedParticle(shape->getAnimationComponent());

		shape->overrideAnimation();

		ReceiveMessage(MESSAGE_SWITCH_WORKSPACE, 0);
		this->moveCamera(CAMERA_MOVE_FRONT);
		
	}

	{
		Layer* layer = scene->addLayer("arrowLayer", 0xFFFFFF00, camera);

		arrows = New Arrows(layer);

		arrows->init(renderer);

		
		editorGrid.init(renderer);

		editorYGrid.constructGridInXYPlane();

		editorYGrid.init(renderer);

		boxing.init(renderer);
	}


	saveCurrentActionState();
	
	if(window)
		return true;

	return this->isOpen();
}

bool ShapeEditor::shutdown()
{

	Delete shape;
	Delete shapeRend;

	if(arrows)
	{
		Delete arrows;
		arrows = NULL;
	}
	return false;
}

void ShapeEditor::update()
{
	// Svorak layout
	if(isInputDown(27))
	{
		this->close();
	}

	if(isInputDown(600))
	{
		
	}

	EditorCamera* camera = scene->getCamera("main");

	if(tool == TOOL_PICKING)
	{
		if(picking.isObjectMarked())
		{
			Vector2 tempMouse = mouse;
			if (mouseAlignment == MOUSE_ALIGNMENT_X)
			{
				tempMouse.y = alignMouse.y;
			}

			else if (mouseAlignment == MOUSE_ALIGNMENT_Y)
			{
				tempMouse.x = alignMouse.x;
			}

			picking.update(tempMouse, camera);
		}
	}
	shape->updateFrame();
	

	camera->updateAsShapeCamera();
	camera->updateTimeFrame(timer.now());
	logic->update();

	renderMod.updateCamera(*camera);

	renderMod.getLinkedParticleLayer()->setVisible(particles);

	renderMod.update();
	
}


void ShapeEditor::runFrame()
{
	// call process before update, since we want keyinputs
	process();



	static Timer timeframe;

	if (timeframe.accumulate(1 / 60.0f))	
	{
		update();
	}

	int nr = shape->renderFrame();

	renderMod.preFrame();

	

	Vector3 rot = scene->getCamera("main")->getOrientation();

	rot.normalize();


	float xLed = rot.dot(Vector3(0,1,0));
	float zLed = xLed;

	xLed = abs(xLed);

	zLed = 1 - xLed;

	zLed = pow(zLed, 6.f);

	xLed = pow(xLed, 6.f);

	
	if (xLed < 0.0f || xLed > 1.0f)
	{
		std::cout << "Xled out of bounds" << std::endl;
	}

	if (zLed < 0.0f || zLed > 1.0f)
		std::cout << "zLed out of bounds" << std::endl;


	scene->render();

	float rad = scene->getCamera("main")->getRadius();

	rad = 1 - (rad*0.01f);

	editorGrid.render(Color(1,1,1,xLed), rad);

	editorYGrid.render(Color(1,1,1,zLed), rad);

	

	if ( boxing.isVisible() )
		boxing.render();

	if(linking)
	{
		arrows->render(nr);
	}

	
	scene->present();
}



void ShapeEditor::onInput(int key, int state)
{
	
	EditorCamera* camera = scene->getCamera("main");
	if (key == 610)
	{
		camera->adjustRadius(state * 0.03f);
	}
	if (key == 602 && state == 1)
	{

		shape->link(doubleLinkage);
		saveCurrentActionState();

	}
	if (key == 600 && state == 1)
	{
		alignMouse = mouse;

		if ( !shape->getSliding() )
		{
		
			picking.pickObject(scene->getWorkingLayer(), mouse, camera);

			if (!picking.isObjectMarked())
			{
				first = mouse;
				boxing.setVisible(true);
			}

		}
	}

	else if (key == 600 && state == 0)
	{
		
		if ( !shape->getSliding() )
		{

			if ( boxing.isVisible() )
			{
				if (!picking.hasPreviousSelected() || tool == TOOL_SHIFT_PICKING)
				{
					Layer* layer = scene->getWorkingLayer();
					picking.pickObject(layer, first, mouse, camera);
				}

				else if ( picking.hasPreviousSelected() || picking.isObjectMarked() )
				{
					picking.clearPicking();
					picking.unpick();
				}
			}

			else
			{
				picking.unpick();
				picking.clearPicking();
				saveCurrentActionState();
			}

			boxing.updateMousePositions(Vector3(0,0,0), Vector3(0,0,0));

			boxing.render();

			boxing.setVisible(false);
		}
	}

	if (isInputDown(603) && state == 1)
	{
	}
	if (isInputDown(604) && state == 1)
	{
	}

	if (key == 601 && state == 1)
	{
	}	


}

void ShapeEditor::onMouseMovement(int x, int y)
{
	EditorCamera* camera = scene->getCamera("main");
	// Save old mouse-position and get the new one
	old = mouse;
	this->getMouseVector(&mouse.x, &mouse.y);
	
	if ( boxing.isVisible() )
	{
		Vector3 f = Vector3(first.x, first.y, 0.0f);
		Vector3 m = Vector3(mouse.x, mouse.y, 0.0f);

		boxing.updateMousePositions(f, m);

		
	}

	if (isInputDown(601) && !picking.isObjectMarked())
	{
		Vector2 rot = mouse - old;

		camera->rotate(-rot.y, rot.x, 0.0f);
	}
}

void ShapeEditor::ReceiveMessage(int id, int message)
{	
	//std::cout << "Received message with id: " << id << " and message: "<< message << std::endl;
	std::stringstream ss;
	unsigned int current = 0;
	switch(id)
	{
		case MESSAGE_VISIBILITY:
			scene->toggleAllLayers(message == 0);
			break;
		case MESSAGE_SWITCH_WORKSPACE:
			switchWorkspace(message);
			break;
		case MESSAGE_PLAY_ANIMATION:
			 playAnimation(message);
			break;
		case MESSAGE_COPY_WORKSPACE:
			shape->copyShapeNodes(message, shape->getWorkingFrame()->getFrame());
			break;

		case MESSAGE_SET_FRAME:
			setFrame(message);
			break;

		case MESSAGE_DISABLE_FRAME:
			disableFrame(message);
			break;

		case MESSAGE_REMOVE_WORKSPACE:
			removeWorkspace(message);
			break;
		case MESSAGE_SELECT_TOOL:
			selectTool(message);
			break;

		case MESSAGE_SELECT_ANIMATION:
			selectAnimation(message);
			break;

		case MESSAGE_SELECT_LINKING:
			selectLinking(message);
			break;

		case MESSAGE_DOUBLE_LINK:
			doubleLink(message);
			break;

		case MESSAGE_CLEAR_ALL:
			clearAll();
			break;
		case MESSAGE_MOVE_CAMERA:
				moveCamera((CameraMovement)message);
				break;
		case MESSAGE_UNDO:
			actions.undoLatestAction();
			break;
		case MESSAGE_REDO:
			actions.redoLatestAction();
			break;

		case MESSAGE_SET_GRAVITY:
			shape->setGravity((float)message);
			break;

		case MESSAGE_SET_SPEED:
			shape->setSpeed((float)message);
			break;

		case MESSAGE_SET_DEATH:
			shape->setDeath((float)message);
			break;

		case MESSAGE_RELOAD_SHADERS:
			reloadShaders();
			break;
			
		case MESSAGE_SCALE_OBJECTS:
			scaleObjects(message);
			break;

		case MESSAGE_ROTATE_OBJECTS_IN_XAXIS:
			rotateObjectsInXAxis(message);
			break;

		case MESSAGE_ROTATE_OBJECTS_IN_YAXIS:
			rotateObjectsInYAxis(message);
			break;

		case MESSAGE_ROTATE_OBJECTS_IN_ZAXIS:
			rotateObjectsInZAxis(message);
			break;

		case MESSAGE_ADD_NODE:
			addNode();
			break;

		case MESSAGE_REMOVE_NODE:
			removeNode();
			break;

		case MESSAGE_UNLINK_NODES:
			unlinkNodes();
			break;
		case MESSAGE_NODES_VISIBILITY:
			setNodesVisibility( message == 1 );

			break;

		case MESSAGE_COPY_MARKED_NODES:
			copyMarkedNodes();
			break;

		case MESSAGE_ALIGN_MOUSE:
				setMouseAlignment(message);
				break;

		default:
			break;
	}
}


void ShapeEditor::ReceiveMessage(int id, std::string& message)
{
	//std::cout << "Received message with id: " << id << " and message: "<< message << std::endl;

	switch(id)
	{	
		case MESSAGE_REMOVE_WORKSPACE:
			break;
		case MESSAGE_OPEN_FILE:
			openFile(message);		
			break;
		case MESSAGE_SAVE_FILE:
			saveFile(message);
			break;

		case MESSAGE_OPEN_POINTS_FROM_OBJ:
			openFileFromOBJ(message);
			break;
		default:

			break;
	}
}

void ShapeEditor::ReceiveMessage(int id, float message)
{

}

void ShapeEditor::ReceiveMessage(int id, std::string& name, std::string& message)
{

}

ShapeCreator* const ShapeEditor::creator() const
{
	return shape;
}

void ShapeEditor::switchWorkspace(const int workspace)
{
	std::stringstream ss;
	ss << workspace;

	// Sliding needs to be set to false here
	shape->setSliding(false);

	shape->setFrame(workspace);


	// if the frame does not exist, i.e first time clicking on it
	if(shape->getWorkingFrame() == NULL)
	{
		Layer* l = scene->addLayer(ss.str(), workspace, scene->getCamera("main"));
		shape->createFrame(workspace, l);
	}

	shape->setFrame(workspace);
	//shape->setVisibility(false);
	scene->setWorkingLayer(ss.str());
}
void ShapeEditor::removeWorkspace(const int workspace)
{
	unsigned int current = 0;
	
	if(shape->getWorkingFrame())
		current = shape->getWorkingFrame()->getFrame();

	shape->setFrame(workspace);

	// here we need to check so the working-frame is active
	if(shape->getWorkingFrame())
	{
		shape->removeFrame(workspace);
		shape->setFrame(current);
	}

	this->switchWorkspace(0);
}

void ShapeEditor::workspaceVisible(const int workspace)
{

}

void ShapeEditor::playAnimation(const int state)
{
	shape->setAnimationFrame(state);
}

void ShapeEditor::copyWorkspaceToCurrent(const int workspace)
{

}

void ShapeEditor::setFrame(const int frame)
{
	if (shape->getVisibility())
		scene->setWorkingLayer("play");
	shape->setSliding(true);
	shape->setFrame(frame);
}


void ShapeEditor::disableFrame(const int frame)
{
	std::cout << frame << " DISABLE FRAME AAAAH" << std::endl;
	std::stringstream ss;
	ss << shape->getWorkingFrame()->getFrame();
	scene->setWorkingLayer(ss.str());
	shape->setSliding(false);
	shape->setAnimationFrame(shape->getWorkingFrame()->getFrame());

}


void ShapeEditor::openFile(const std::string& file)
{
	if ( !shape->getSliding() )
	{
		this->clearAll();
		if (shape->loadAnimation(file, scene))
			std::cout << "Created animaiton with: "<< file << std::endl;
		saveCurrentActionState();
	}
}

void ShapeEditor::saveFile(const std::string& file)
{
	if ( !shape->getSliding() )
		shape->saveAnimation(file);
}

void ShapeEditor::openFileFromOBJ(const std::string& file)
{
	if ( !shape->getSliding() )
	{
		ShapeFile::loadPointsOBJ(file, shape);
		saveCurrentActionState();
	}
}

void ShapeEditor::setWorkspace(const int workspace)
{

}

void ShapeEditor::selectTool(const int tool)
{
	this->tool = (Tool)tool;

}

void ShapeEditor::selectAnimation(const int state)
{
	particles = state == 1;
}

void ShapeEditor::selectLinking(const int state)
{
	linking = state == 1;
}

void ShapeEditor::doubleLink(const int state)
{
	doubleLinkage = state == 1;
}

void ShapeEditor::clearAll()
{
	ReceiveMessage(MESSAGE_SWITCH_WORKSPACE, 0);
	shape->clear();
	ReceiveMessage(MESSAGE_SWITCH_WORKSPACE, 0);
}

void ShapeEditor::moveCamera(const CameraMovement move)
{
	EditorCamera* camera = scene->getCamera("main");


	switch(move)
	{
	case CAMERA_MOVE_UP:
		camera->setRotation((float)-D3DX_PI/2, 0.0f, 0.0f);
		break;
	case CAMERA_MOVE_DOWN:
		camera->setRotation((float)D3DX_PI/2, 0.0f, 0.0f);
		break;
	case CAMERA_MOVE_LEFT:
		camera->setRotation(0.0f, (float)D3DX_PI/2, 0.0f);
		break;
	case CAMERA_MOVE_RIGHT:
		camera->setRotation(0.0f, (float)-D3DX_PI/2, 0.0f);
		break;
	case CAMERA_MOVE_FRONT:
		camera->setRotation(0.0f, 0.0f, 0.0f);
		break;
	case CAMERA_MOVE_BEHIND:
		camera->setRotation(0.0f, (float)D3DX_PI, 0.0f);
		break;
	case CAMERA_MOVE_TO_GAME_SPECIFIC:
		camera->setRotation((float)-D3DX_PI/4, 0.0f, 0.0f);
		camera->setRadius(90.0f);
		break;
	}
}

void ShapeEditor::reloadShaders()
{
	renderer->shader()->reload();
}

void ShapeEditor::scaleObjects(const int scale)
{
	shape->scaleObjectsforCurrentFrame(scale);
}

void ShapeEditor::rotateObjectsInXAxis(const int rotation)
{
	shape->rotateObjectsforCurrentFrame(rotation, Vector3(1.0f, 0.0f, 0.0f) );
}

void ShapeEditor::rotateObjectsInYAxis(const int rotation)
{
	shape->rotateObjectsforCurrentFrame(rotation, Vector3(0.0f, 1.0f, 0.0f));
}

void ShapeEditor::rotateObjectsInZAxis(const int rotation)
{
	shape->rotateObjectsforCurrentFrame(rotation, Vector3(0.0f, 0.0f, 1.0f));
}

void ShapeEditor::addNode()
{
	if ( !shape->getSliding() )
	{
		int id = shape->createShapeNode(0, 0, 0);
		saveCurrentActionState();
	}
}

void ShapeEditor::removeNode()
{
	if (picking.isObjectMarked())
		picking.unpick();

	shape->removeShapeNode();
	saveCurrentActionState();
}

void ShapeEditor::unlinkNodes()
{
	shape->unlink();
	saveCurrentActionState();
}

void ShapeEditor::copyMarkedNodes()
{
	std::vector<Object*> objs;

	picking.fetchLinks(objs);

	float length = -9999;


	for( unsigned int i = 0; i < objs.size(); i++ )
	{
		for( unsigned int j = 0; j < objs.size(); j++ )
		{

			QueryEvent<Vector3> qe = QueryEvent<Vector3>(QUERY_POSITION);
			objs[i]->send( &qe );

			Vector3 position(0,0,0);

			position = qe.getData();


			QueryEvent<Vector3> qe2 = QueryEvent<Vector3>(QUERY_POSITION);
			objs[j]->send( &qe2 );

			Vector3 position2(0,0,0);

			position2 = qe2.getData();

			float l = position2.y - position.y;

			if (l < 0.0f)
			{
				l *= -1;
			}

			if (length < l )
			{
				length = l;

			}
		}


	}

	for( unsigned int i = 0; i < objs.size(); i++ )
	{
		QueryEvent<Vector3> qe = QueryEvent<Vector3>(QUERY_POSITION);
		objs[i]->send( &qe );

		Vector3 position(0,0,0);

		position = qe.getData();

		float offset = 5.0f;


		position.y += length + offset;


		shape->createShapeNode(position);

	}
}

void ShapeEditor::setNodesVisibility(const bool visibility)
{
	shape->setVisibility(visibility);
}


void ShapeEditor::setParticleColor(const float r, const float g, const float b, const float a)
{
	shape->adjustColor(r,g,b,a);
}

void ShapeEditor::saveCurrentActionState()
{
	std::vector<unsigned int> frames;
	std::vector<ShapeResult> results;

	shape->getAnimation().exportToVector(frames, results);

	ShapeAction* action = New ShapeAction(this, frames, results);
	actions.pushAction(action);
}

void ShapeEditor::restoreActionState(const std::vector<unsigned int>& frames, std::vector<ShapeResult>& results)
{
	clearAll();
	// fill animation with shape
	
	for(unsigned int i = 0; i < frames.size(); i++)
	{

		if(results[i].count > 0)
		{
			std::stringstream ss;
			//std::cout << frames[i] << std::endl;
			ss << frames[i];
			//std::cout << ss.str() << std::endl;
			Layer* l = scene->addLayer(ss.str(), frames[i], scene->getCamera("main"));
			scene->setWorkingLayer(ss.str());
			shape->createFrame(frames[i], l);
			shape->setFrame(frames[i]);
			shape->recreateShape(results[i]);
		}
		shape->getAnimation().recreateFrame(frames[i], &results[i]);
	}
}
const std::string ShapeEditor::getNextFrameString()
{
	return shape->getNextFrameString();
}

void ShapeEditor::onCreateObject(Object * object)
{
	PhysicsComponent * pc = object->findComponent<PhysicsComponent>();
	if (pc)
	{
		pc->disableArenaCollision();
	}
}

void ShapeEditor::onEnter()
{
	scene->bindEditor(this);
	renderMod.bind();
}


const float ShapeEditor::getGravityForFrame() const
{
	return shape->getGravity();
}

const float ShapeEditor::getSpeedForFrame() const
{
	return shape->getSpeed();
}

const float ShapeEditor::getDeathForFrame() const
{
	return shape->getDeath();
}

void ShapeEditor::setMouseAlignment(const int alignment)
{
	mouseAlignment = (MouseAlignments)alignment;
}