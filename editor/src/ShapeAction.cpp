#include "ShapeAction.h"


ShapeAction::ShapeAction(Editor* const ed)
	: editor(ed)
{
}

ShapeAction::ShapeAction(Editor* ed, std::vector<unsigned int> f, std::vector<ShapeResult> r)
	:editor(ed),
	frames(f),
	results(r)
{
}

ShapeAction::~ShapeAction()
{

}

void ShapeAction::undo()
{
	editor->restoreActionState(frames, results);
}

void ShapeAction::redo()
{
	editor->restoreActionState(frames, results);
}


const std::string ShapeAction::toString()
{
	return "";
}