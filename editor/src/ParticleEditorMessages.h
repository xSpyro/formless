#ifndef PARTICLEEDITORMESSAGES_H
#define PARTICLEEDITORMESSAGES_H

enum ParticleFileMessage
{
    P_F_MESSAGE_MOVE_CAMERA,
    P_F_MESSAGE_OPEN_FILE,
    P_F_MESSAGE_SAVE_FILE,
    P_F_MESSAGE_NEW_FILE,
};

enum ParticleMessage
{

        P_RAND_ANGLE_X = 200,
        P_RAND_ANGLE_Y,
        P_RAND_ANGLE_Z,

        P_DIR_INTENSITY,

        P_SPIN_MIN,
        P_SPIN_MAX,

        P_SPAWN_RATE,
        P_EMITT_LIFE,
        P_AFFECT_RATE,
        P_START_SCALE,
        P_END_SCALE,

        P_START_COLOR_R,
        P_START_COLOR_G,
        P_START_COLOR_B,

        P_MIDDLE_COLOR_R,
        P_MIDDLE_COLOR_G,
        P_MIDDLE_COLOR_B,

        P_END_COLOR_R,
        P_END_COLOR_G,
        P_END_COLOR_B,

        P_COLOR_PATH,
        P_SPEED_PATH,

        P_EMITTER_TYPE_BOX,
        P_EMITTER_TYPE_CIRCLE,

        P_START_SPEED,
        P_MIDDLE_SPEED,
        P_END_SPEED,

        P_GRAVITY,

        P_SELECT_ANIMATION,
        P_SELECT_BEHAVIOR,

		P_SELECT_TYPE,
        P_ADD_TYPE,
		P_ADD_TO_TYPE,
        P_ADD_EMITTER,
        P_NEW_EMITTER,
        P_ADD_ANIMATION,
        P_NEW_ANIMATION,
        P_LOOP_ANIMATION,
        P_PLAY_ANIMATION,

        P_DELETE_E,
        P_DELETE_A,

        P_PLAY_SELECTED_A,
        P_PLAY_SELECTED_E,

        P_PLAY_ALL,

        P_DISTORSION,

        P_DISPLACEMENT_POS,
        P_LIFE_AFFECT_RATE,
        P_EMITTER_LIFE_AFFECT_RATE,
        P_MESSAGE_OPEN_TEXTURE,

        P_SET_PARTICLE_SPAWN_MAX
};


#endif