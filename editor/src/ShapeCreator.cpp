#include "ShapeCreator.h"

#include "ShapeFile.h"
#include "PositionEvent.h"


#include "Importer.h"

#include "MemLeaks.h"

ShapeCreator::ShapeCreator()
	: nextFrameString(NULL), visibility(true), particleColor(5.0f, 6.0f, 10.0f, 255.0f)
{
	frame = NULL;
	maxFrame = 0;

}

ShapeCreator::ShapeCreator(Picking* const picking, GameHandler* const logic, ShapeRenderer* const renderer)
: nextFrameString(NULL), visibility(true), particleColor(5.0f, 6.0f, 10.0f, 255.0f)
{
	frame = NULL;
	this->picking = picking;
	this->logic = logic;
	this->renderer = renderer;
	sliding = false;
	
	/*File file;
	FileSystem::get("objects/EditorAnimation.lua", &file);*/
	logic->registerObjectTemplate("EditorAnimation");
	Object* obj = logic->createObject("EditorAnimation");


	visual = obj->findComponent<AnimationComponent>();
	
	if(visual == NULL)
	{
		std::cout << "Could not find any animationcomponent in object" << std::endl;
	}
	else
	{
		overrideAnimation();
	} 
	maxFrame = 0;
}


ShapeCreator::~ShapeCreator()
{
	for(Frames::iterator it = frames.begin(); it != frames.end(); it++)
	{
		delete it->second;
	}

	frames.clear();
}

void ShapeCreator::updateFrame()
{
	for(Frames::iterator it = frames.begin(); it != frames.end(); it++)
	{
		it->second->setLayerVisibility(false);
	}

	frame->setLayerVisibility(visibility);


	std::vector<Object*> objects;

	picking->fetchLinks(objects);
	for(unsigned int i = 0; i < objects.size(); i++)
	{

		Object* obj = objects[i];//picking->getMarked();

		// fetch picked object
		if(obj)
		{
			unsigned int pos = frame->getShapeNode(obj);
		
			// check so the object in question actually is a object in animation
			if(pos != 0xFFFFFFFF)
			{
				// get the position for the object
				Vector3 position;
				PhysicsComponent* comp = obj->findComponent<PhysicsComponent>();

				if(comp)
				{
					// move the animationnode
					position = comp->position;
					animation.moveNode(frame->getFrame(), pos, position);
				}

				
			}
		}
	}
}

const int ShapeCreator::renderFrame()
{
	visual->update();
	
	ShapeFrame shape = animationInst.getFrame();

	ShapeResult result;
	shape.createLerpedResult(&result);

	if(sliding)
	{
		Vector3 nodes[128];


		for(int i = 0; i < result.count; i++)
		{
			nodes[i] = result.nodes[i].position;
		}
		
		if (visibility)
		{
			renderer->updateNodes(&nodes[0], result.count);
		}
	}

	return result.count;
}


int ShapeCreator::createShapeNode(const float x, const float y, const float z)
{
	if (frame->getSize() < MAX_SHAPE_SIZE)
	{
		// if no object are picked meaning we are putting out a parent
		Object* parent = picking->getMarked();
		
	
		if (parent)	
		{
			int par = frame->getShapeNode(parent);
			int pos = animation.addNode(frame->getFrame(), par, Vector3(x,y,z));
			frame->insertShapeNode(pos, x,y,z);
			return pos;
			/*animation.linkNode(frame->getFrame(), pos, pos);*/
		}
		
		else
		{ 
			int pos = animation.addNode(frame->getFrame(), -1, Vector3(x,y, z));
			frame->insertShapeNode(pos, x, y, z);
			
			//animation.linkNode(frame->getFrame(), pos, pos);
			return pos;
			//this->linkLastFrame();
		}
	}

	return -1;
}

int ShapeCreator::createShapeNode(const Vector3& position)
{
	if (frame->getSize() < MAX_SHAPE_SIZE)
	{
		// if no object are picked meaning we are putting out a parent
		Object* parent = picking->getMarked();
		if (parent)
		{
			int par = frame->getShapeNode(parent);
			int pos = animation.addNode(frame->getFrame(), par, position);
			frame->insertShapeNode(pos, position);
			return pos;
		}
		else
		{
			int pos = animation.addNode(frame->getFrame(), -1, position);
			frame->insertShapeNode(pos, position);
			
			//this->linkLastFrame();
			//animation.linkNode(frame->getFrame(), pos, pos);
			return pos;
		}
	
		

	}
	return -1;
}

void ShapeCreator::createShapeNodeFromOBJ(const Vector3& position)
{
	if (frame->getSize() < MAX_SHAPE_SIZE)
	{
		// if no object are picked meaning we are putting out a parent
		Object* parent = picking->getMarked();
		if (!parent)
		{
			int pos = animation.addNode(frame->getFrame(), -1, position);
			frame->insertShapeNode(pos, position);
			//this->linkLastFrame();
			//animation.linkNode(frame->getFrame(), pos, pos);
		}

		else if (parent)
		{
			int par = frame->getShapeNode(parent);
			int pos = animation.addNode(frame->getFrame(), par, position);
			frame->insertShapeNode(pos, position);
		}

	}
}

void ShapeCreator::copyShapeNodes(const unsigned int from, const unsigned int to)
{
	//Frames::iterator f = frames.find(from);
	//Frames::iterator t = frames.find(to);

	//if(f != frames.end() && t != frames.end())
	//{
	//	std::vector<Object*> objs;

	//	f->second->fetchNodes(objs);
	//	this->setFrame(t->first);
	//	for(unsigned int i = 0; i < objs.size(); i++)
	//	{
	//		PhysicsComponent* comp = objs[i]->findComponent<PhysicsComponent>();

	//		this->createShapeNode(comp->position);
	//	}
	//}
	
	// fill animation with shape
	std::vector<unsigned int> f;
	std::vector<ShapeResult> results;

	animation.exportToVector(f, results);

	for(unsigned int i = 0; i < f.size(); i++)
	{
		if(f[i] == from)
		{
			if(results[i].count > 0)
			{
				std::stringstream ss;
				ss << f[i];
				this->setFrame(to);
				this->recreateShape(results[i]);
			}
			animation.recreateFrame(to, &results[i]);
			return;
		}
	}
}
void ShapeCreator::removeShapeNode()
{

	std::vector<Object*> objs;

	picking->fetchLinks(objs);


	if(!objs.empty())
	{
		for(unsigned int i = 0; i < objs.size(); i++)
		{
			const unsigned int pos = frame->getShapeNode(objs[i]);
			animation.removeNode(frame->getFrame(), pos);
			
			frame->removeShapeNode(pos);
		}
	}

	picking->clearPicking();
}

void ShapeCreator::removeShapeNode(const int node)
{
	if(frame)
	{
		frame->removeShapeNode(node);
		picking->clearPicking();
	}
}

void ShapeCreator::recreateShape(ShapeResult& res)
{
	for(int i = 0; i < res.count; i++)
	{
		Object* obj = frame->insertShapeNode(i, res.nodes[i].position);
	}
}

void ShapeCreator::createFrame(const unsigned int frame, Layer* layer)
{
	ShapeObjects* f = frames[frame];
	if(f == NULL)
	{
		f = frames[frame] = New ShapeObjects(logic, frame, layer);
	}

	if (frame > maxFrame)
		maxFrame = frame;

	nextFrameString = NULL;
}


void ShapeCreator::removeFrame(const unsigned int frame)
{
	// find the frameobject with value @pos
	Frames::iterator it = frames.find(frame);
	
	if(it != frames.end())
	{
		it->second->clearShapeNodes(&animation);
		if(this->frame == it->second)
			this->frame = NULL;
		Delete it->second;
		frames.erase(it);
		
		// if it was the maxFrame removed, then update it
		if(frame == maxFrame)
		{
			maxFrame = 0;
			for(Frames::iterator i = frames.begin(); i != frames.end(); i++)
			{
				if(i->first > maxFrame)
				{
					maxFrame = i->first;
				}
			}
		}
		animation.removeFrame(frame);
	}
	nextFrameString = NULL;
}

void ShapeCreator::copyFrame(const unsigned int destination, Layer* layer)
{
	if(frame)
		frames[destination] = New ShapeObjects(*frame, layer, destination, animation);
}



void ShapeCreator::setFrame(const unsigned int frame)
{
	if(!sliding)
	{	
		if(this->frame)
		{
			this->frame->deactivateObjects();
		}

		if(frames.find(frame) != frames.end())
		{
			this->frame = frames[frame];
			this->frame->activateObjects();
		}
		else
		{
			this->frame = NULL;
		}

		animationInst.jump(frame);
	}
	
}

void ShapeCreator::clear()
{
	maxFrame = 0;
	for(Frames::iterator it = frames.begin(); it != frames.end(); it++)
	{
		it->second->deactivateObjects();
		it->second->clearShapeNodes(&animation);
		Delete it->second;
	}

	

	frames.clear();

	this->frame = NULL;
	nextFrameString = NULL;
	
}
void ShapeCreator::mirrorFrame(const unsigned int destination)
{ 
	ShapeObjects* f = frames[destination];
	if(f == NULL)
	{
		frames[destination] = frame;
	}
}

void ShapeCreator::linkLastFrame()
{
	animation.linkNode(frame->getFrame(), frame->getSize() - 1, 0);
}

const unsigned int ShapeCreator::getMaxframe() const
{
	return maxFrame;
}

ShapeObjects* ShapeCreator::getWorkingFrame() const
{
	return frame;
}

void ShapeCreator::print()
{

}

bool ShapeCreator::getSliding() const
{ 
	return sliding;
}
void ShapeCreator::setSliding(const bool sliding)
{
	this->sliding = sliding;


}

void ShapeCreator::link(bool doubleLinkage)
{
	std::vector<Object*> objs;

	picking->fetchLinks(objs);

	
	if(!objs.empty())
	{
		for(unsigned int i = 0; i < objs.size() - 1; i++)
		{
			unsigned int first = frame->getShapeNode(objs[i]);
			unsigned int second = frame->getShapeNode(objs[i+1]);

			animation.linkNode(frame->getFrame(), first, second);
			if(doubleLinkage)
				animation.linkNode(frame->getFrame(), second, first);
		}
	}

	picking->clearPicking();
}

void ShapeCreator::unlink()
{
	std::vector<Object*> objs;

	picking->fetchLinks(objs);


	if(!objs.empty())
	{
		for(unsigned int i = 0; i < objs.size(); i++)
		{
			unsigned int first = frame->getShapeNode(objs[i]);
			animation.unlinkNode(frame->getFrame(), first);
		}
	}

	picking->clearPicking();
}

bool ShapeCreator::loadAnimation(const std::string& file, EditorScene* const scene)
{
	//formatStore.importFromFile(file);
	return ShapeFile::loadShapeFile(file, animation, this, scene);
}

bool ShapeCreator::saveAnimation(const std::string& file)
{
	return ShapeFile::saveShapeFile(file, animation);
	
}


 void ShapeCreator::setAnimationFrame(const unsigned int frame)
 {
	 animationInst.jump(frame);
 }


 void ShapeCreator::clearEmptyFrames()
 {
	 std::vector<unsigned int> removal;

	 for(Frames::iterator it = frames.begin(); it != frames.end(); it++)
	 {
		 if(it->second->getSize() == 0)
		 {
			 removal.push_back(it->first);
		 }
	 }
	 for(unsigned int i = 0; i < removal.size(); i++)
	 {
		 this->removeFrame(removal[i]);
	 }
 }

 ShapeAnimation& ShapeCreator::getAnimation()
 {
	 return animation;
 }

 const std::string ShapeCreator::getNextFrameString()
 {
	 // gets the next frame to be sent to C#

	 // if none is sent, or if it have sent enough
	 if(nextFrameString == NULL && frames.size() > 0)
	 {
		 Frames::iterator it = frames.begin();
		 nextFrameString = it->second;
		 std::stringstream ss;
		 ss << nextFrameString->getFrame();
		 return ss.str();
	 }

	 // if something is sent and need to continue sending
	 else if(frames.size() > 0)
	 {
		for(Frames::iterator it = frames.begin(); it != frames.end(); it++)
		{
			if(nextFrameString->getFrame() < it->second->getFrame())
			{
				nextFrameString = it->second;
				std::stringstream ss;
				ss << nextFrameString->getFrame();
				return ss.str();
			}
		}
		return "";
	 }

	 return "";

 }


 AnimationComponent* const ShapeCreator::getAnimationComponent() const
 {
	 return visual;
 }


 void ShapeCreator::overrideAnimation() 
 {
	 animationInst.jumpShape(&animation);
	 visual->overrideAnimation(&animationInst);
	 visual->startShape(&animation);
 }


 void ShapeCreator::setGravity(const float g)
 {
	 animation.setFrameGravity(frame->getFrame(), g * 0.01f);
 }

 void ShapeCreator::setSpeed(const float s)
 {
	 animation.setFrameSpeed(frame->getFrame(), s * 0.01f);

 }

 void ShapeCreator::setDeath(const float d)
 {
	 animation.setFrameDeath(frame->getFrame(), d * 0.01f); 
 }

 const float ShapeCreator::getGravity()
 {
	 float g = animation.getFrameGravity( frame->getFrame() ) * 100.0f;

	 return g;
 }

 const float ShapeCreator::getSpeed()
 {
	 float speed = animation.getFrameSpeed( frame->getFrame() ) * 100.0f;

	 return speed;
 }

 const float ShapeCreator::getDeath()
 {
	 return animation.getFrameDeath( frame->getFrame() ) * 100.0f;
 }

 void ShapeCreator::scaleObjectsforCurrentFrame(const int scale)
 {
	std::vector<Object*> objects;
	picking->fetchLinks(objects);

	frame->scaleObjects(scale, objects);

	frame->fetchNodes(objects);

	 for (unsigned int i = 0; i < objects.size(); i++)
	 {
		 unsigned int id = frame->getShapeNode(objects[i]);
		 
		 PhysicsComponent* comp = objects[i]->findComponent<PhysicsComponent>();

		 if (comp)
		 {
			animation.moveNode(frame->getFrame(), id, comp->position);
		 }
	 }

 }

 void ShapeCreator::rotateObjectsforCurrentFrame(const int rotation, const Vector3& axis)
 {
	 std::vector<Object*> objects;
	 picking->fetchLinks(objects);

	 frame->rotateObjects(rotation, axis, objects);

	 frame->fetchNodes(objects);

	 for (unsigned int i = 0; i < objects.size(); i++)
	 {
		 unsigned int id = frame->getShapeNode(objects[i]);

		 PhysicsComponent* comp = objects[i]->findComponent<PhysicsComponent>();

		 if (comp)
		 {
			 animation.moveNode(frame->getFrame(), id, comp->position);
		 }
	 }
 }

 void ShapeCreator::setVisibility(const bool visibility)
 {
	 this->visibility = visibility;

	 renderer->getLayer()->setVisible(visibility);
	 
 }

 const bool ShapeCreator::getVisibility() const
 {
	 return visibility;
 }

 void ShapeCreator::adjustColor(const float r, const float g, const float b, const float a)
 {

	 particleColor = Color(r,g,b,a);
	 
	 Color temp = particleColor;

	 temp.r /= 255.0f;
	 temp.g /= 255.0f;
	 temp.b /= 255.0f;
	 visual->setBaseColor(temp);
 }

 const Color ShapeCreator::getColor()
 {
	 return particleColor;
 }