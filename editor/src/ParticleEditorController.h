#ifndef PARTICLEEDITORCONTROLLER_H
#define PARTICLEEDITORCONTROLLER_H

#include "Editor.h"

#include "ParticleAnimation.h"

#include "Logger.h"

#include "RenderModule.h"

#include "ParticleEffect.h"

#include "ParticleEditorMessages.h"

class ParticleEditorController
{


public:

	ParticleEditorController();
	~ParticleEditorController();

	void ReceiveMessage(int id, int message);
	
	void ReceiveMessage(int id, std::string& message);
	void ReceiveMessage(int id, float message);
	void ReceiveMessage(int id, std::string& name, std::string& message);

	bool addEmitter();
	bool addAnimation();

	bool removeSelectedEmitter(int id);
	bool removeSelectedAnimation(int id);

	void clearVector();

	void setBehaviorNames(std::string file);

	ParticleEffect effect_ps;
	// Beh�vs inte nog?
private:

	void initDefaultSelected();

	void setRandAngleX(float a);
	void setRandAngleY(float a);
	void setRandAngleZ(float a);
	void setIntensityDir(float i);

	void setEmitterType(int type);
	void setLife(int life);
	
	void setSpinMin(float m);
	void setSpinMax(float m);

	void setSpawnRate(float r);
	void setAffectRate(float r);

	void setStartScale(float s);
	void setEndScale(float s);

	void setStartColorR(float r);
	void setStartColorG(float r);
	void setStartColorB(float r);

	void setMiddleColorR(float r);
	void setMiddleColorG(float r);
	void setMiddleColorB(float r);

	void setEndColorR(float r);
	void setEndColorG(float r);
	void setEndColorB(float r);

	void setStartSpeed(float s);
	void setMiddleSpeed(float s);
	void setEndSpeed(float s);

	void setColorPath(float p);
	void setSpeedPath(float p);

	void setGravity(float g);

	void setDistorsion(float d);

	void setEmitterType(ParticleMessage msg);

	void setDisplacementPos(float p);
	void setLifeAffectRate(float r);


public:
	//------[ GET FUNCTIONS ]------//
	float getRandAngleX();
	float getRandAngleY();
	float getRandAngleZ();
		
	float getSpawnBoxX();
	float getSpawnBoxY();
	float getSpawnBoxZ();

	int getLife();
	float getIntensityDir();
	float getDiffusion();

	float getSpinMin();
	float getSpinMax();

	float getSpawnRate();
	float getAffectRate();
	float getStartScale();
	float getEndScale();

	float getStartSpeed();
	float getMiddleSpeed();
	float getEndSpeed();
	float getColorPath();
	float getSpeedPath();
	float getGravity();
	float getDistorsion();
	float getDisplacementPos();
	float getLifeAffectRate();

	float getStartColorR();
	float getStartColorG();
	float getStartColorB();

	float getMiddleColorR();
	float getMiddleColorG();
	float getMiddleColorB();

	float getEndColorR();
	float getEndColorG();
	float getEndColorB();

	int getNrEmitters();
	int getNrAnimations();

	ParticleAnimation * getSelectedAnimation();
	ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * getSelectedEmitter();

	ParticleAnimation * getAnimation(int id);
	ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * getEmitter(int a, int b);

	void removeEmitter(int id);
	void removeAnimation(int id);


private:

	void initByEmitter(ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * dst, ParticleAnimation::EMITTER_BEHAVIOR_PACKAGE * src);

	Logger log;



	int selectedAnimationID;
	int selectedEmitterID;
	UINT particle_id;

};


#endif



