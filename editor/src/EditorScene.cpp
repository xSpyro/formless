
#include "EditorScene.h"

#include "RenderComponent.h"

#include "Editor.h"
#include "MemLeaks.h"

EditorScene::EditorScene()
	: Scene()
{
	workspace = NULL;
	show = false;
	vp = NULL;

	editor = NULL;
}

EditorScene::EditorScene(LogicHandler* handler, Renderer* rend, Window* wndw) 
	: Scene(handler, rend, wndw)
{
	workspace = NULL;
	show = false;

	editor = NULL;

	EditorCamera* camera = addCamera("main");
	workspace = addLayer("main", 0, camera);
	
	camera->setLookAt(Vector3(0, 0, 0));
	camera->setClipRange(1, 1000);
	camera->setProjection3D(45, 1280.0f / 720.0f);
	
	vp = viewports["main"];

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, renderer->shader()->get("shaders/obj.ps"));

}
EditorScene::~EditorScene()
{
	for(Cameras::iterator it = cameras.begin(); it != cameras.end(); it++)
	{
		delete it->second;
	}

	cameras.clear();
}


void EditorScene::update()
{

}

void EditorScene::clear()
{
	renderer->setClearColor(Color(0,0,0,1));

	renderer->clear(NULL);

}

void EditorScene::render()
{

	renderer->render();
	
	
}

void EditorScene::present()
{
	renderer->present();
}

void EditorScene::onEnter()
{
}

bool EditorScene::onInput(int sym, int state)
{
	return false;
}

bool EditorScene::onMouseMovement(int x, int y)
{
	return false;
}	

bool EditorScene::onCreateObject(Object* object)	
{
	
	RenderComponent* rendcomp = object->findComponent<RenderComponent>();

	if(editor)
		editor->onCreateObject(object);
	
	if(rendcomp)
	{
		rendcomp->load(renderer);
		workspace->add(&rendcomp->model);
		rendcomp->model.setUserdata(object);

	}

	std::cout << "OnCreateObject" << std::endl;

	return true;
}

bool EditorScene::onDestroyObject(Object* object)
{
	return true;
}

EditorCamera* const EditorScene::addCamera(const std::string& name)
{
	// check if the camera already exist
	EditorCamera* camera = cameras[name];

	// if it does not, create one
	if(camera == NULL)
	{
		cameras[name] = camera = New EditorCamera();
	}
	
	return camera;
}


EditorCamera* const EditorScene::getCamera(const std::string& name)
{
	return cameras[name];
}



Layer* const EditorScene::addLayer(const std::string& name, const unsigned int priority, Camera* camera, BaseShader* vs, BaseShader* gs, BaseShader* ps)
{
	// check if layer exists
	Layer* layer = layers[name];

	// create if it does not
	if(layer == NULL)
	{
		layers[name] = layer = renderer->createLayer();
		prio[name] = priority + START_LAYER_NR;



		BaseShader* vshader = renderer->shader()->get("/shaders/SkySphere.vs");

		BaseShader* pshader = renderer->shader()->get("/shaders/SkySphere.ps");

		Viewport* view = layer->addViewport(camera);

		viewports[name] = view;

		/**/
		
		if(vs == NULL && ps == NULL && gs == NULL)
		{
			view->setShaders(vshader, NULL, pshader);
			view->getEffect()->blendModeParticle();
		}
		else
		{
			view->setShaders(vs, gs, ps);
		}

		renderer->insertLayer(priority + START_LAYER_NR, layer);
	}

	return layer;
}


Layer* const EditorScene::getLayer(const std::string& name)
{
	return layers[name];
}

void EditorScene::setWorkingLayer(const std::string& name)
{
	// Set visibility to 0
	toggleAllLayers(false);
	workspace = layers[name];
	
	// show only this layer
	workspace->setVisible(true);
	viewports[name]->setVisible(true);
}

Layer* const EditorScene::getWorkingLayer() const
{
	return workspace;
}

void EditorScene::setWorkspace(const std::string& name)
{
	workspace = layers[name];
}

void EditorScene::toggleAllLayers(const bool visible)
{

	for(Layers::iterator it = layers.begin(); it != layers.end(); it++)
	{
		it->second->setVisible(visible);

		Viewports::iterator i = viewports.find(it->first);

		if ( i != viewports.end() )
		{
			i->second->setVisible(visible);
		}
	}
}

Viewport* const EditorScene::getViewport()
{
	return vp;
}

Viewport* const EditorScene::getViewport(const std::string& name)
{
	return viewports[name];
}

void EditorScene::setLayers(const bool state)
{
	if(!state)
	{
		renderer->clearLayers();

		for(Viewports::iterator it = viewports.begin(); it != viewports.end(); it++)
		{
			it->second->setVisible(false);
		}
	}

	else
	{
		int i = 0;
		for(Layers::iterator it = layers.begin(); it != layers.end(); it++, i++)
		{
			if(prio.find(it->first) != prio.end())
			{
				renderer->insertLayer(prio[it->first], it->second);
				continue;
			}
			renderer->insertLayer(i, it->second);
		}

		for(Viewports::iterator it = viewports.begin(); it != viewports.end(); it++)
		{
			it->second->setVisible(true);
		}
	}
}

void EditorScene::bindEditor(Editor * editor)
{
	this->editor = editor;
}