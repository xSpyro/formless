
#ifndef EDITORGRID_H
#define EDITORGRID_H

#include "Common.h"
#include "Framework.h"

class EditorGrid
{
	struct GridVertex
	{
		Vector3 position;
		Color color;

		GridVertex() : position(0,0,0), color(0,0,0,0)
		{}

		GridVertex(Vector3 pos, Color col) : position(pos), color(col)
		{}


	};

	struct PerRenderable
	{
		Color blend;
		Color gradient;
	};


public:
	EditorGrid();
	~EditorGrid();

	bool init(Renderer* const renderer);

	void render(const Color& blend, const float gradient);
	

	void constructGridInXZPlane();
	void constructGridInXYPlane();

private:
	Effect effect;
	
	Renderer* renderer;

	ID3D11Buffer* vertexBuffer;

	std::vector<GridVertex> gridPositions;

	ID3D11InputLayout* inputLayout;

};

#endif