#ifndef SHAPECREATOR_H

#define SHAPECREATOR_H

#include "Picking.h"
#include "ShapeAnimation.h"
#include "ShapeAnimationInst.h"
#include "Common.h"
#include "ShapeObjects.h"
#include "PhysicsComponent.h"
#include "ShapeRenderer.h"
#include "EditorScene.h"
#include "AnimationComponent.h"


const unsigned int MAX_SHAPE_SIZE = 128;
class ShapeCreator
{
	typedef std::map<unsigned int, ShapeObjects*> Frames;

public:
	ShapeCreator();
	ShapeCreator(Picking* const picking, GameHandler* const logic, ShapeRenderer* const renderer);

	~ShapeCreator();

	void updateFrame();

	const int renderFrame();

	int createShapeNode(const float x, const float y, const float z);
	int createShapeNode(const Vector3& position);
	void createShapeNodeFromOBJ(const Vector3& position);
	void copyShapeNodes(const unsigned int from, const unsigned int to);
	void removeShapeNode();
	void removeShapeNode(const int node);

	void recreateShape(ShapeResult& res);
	void createFrame(const unsigned int frame, Layer* layer);
	void removeFrame(const unsigned int frame);

	void copyFrame(const unsigned int destination, Layer* layer);
	void setFrame(const unsigned int frame);

	void clear();
	void linkLastFrame();
	// mirrors the current frame to destination frame
	void mirrorFrame(const unsigned int destination);

	const unsigned int getMaxframe() const;

	ShapeObjects* getWorkingFrame() const;

	void print();

	 bool getSliding() const;
	 void setSliding(const bool sliding);

	 void link(bool doubleLinkage);
	 void unlink();

	 bool loadAnimation(const std::string& file, EditorScene* const scene);
	 bool saveAnimation(const std::string& file);
	 
	 

	 void setAnimationFrame(const unsigned int frame);

	 void clearEmptyFrames();

	ShapeAnimation& getAnimation();

	 // this function holds the next frame string that should be sent to the listbox in ShapeEditor in c# 
	 const std::string getNextFrameString();

	 AnimationComponent* const getAnimationComponent() const;

	 void overrideAnimation();

	 void setGravity(const float g);
	 void setSpeed(const float s);
	 void setDeath(const float d);

	 const float getGravity();
	 const float getSpeed();
	 const float getDeath();

	 void scaleObjectsforCurrentFrame(const int scale);

	 void rotateObjectsforCurrentFrame(const int rotation, const Vector3& axis);
	
	 void setVisibility(const bool visibility);

	 const bool getVisibility() const;

	 void adjustColor(const float r, const float g, const float b, const float a);

	 const Color getColor();

private:
	Picking* picking;

	GameHandler* logic;

	ShapeAnimation animation;

	ShapeAnimationInst animationInst;

	ShapeObjects* frame;

	Frames frames;

	ShapeRenderer* renderer;

	bool sliding;

	unsigned int maxFrame;

	AnimationComponent* visual;

	ShapeObjects* nextFrameString;

	bool visibility;

	Color particleColor;
};


#endif /* SHAPECREATOR_H */