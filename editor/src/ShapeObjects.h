#ifndef SHAPEOBJECTS_H
#define SHAPEOBJECTS_H


#include "Common.h"
#include "ShapeNode.h"
#include "ShapeAnimation.h"
#include "GameHandler.h"

class ShapeObjects
{
	// int is what shapenode the object represents
	// so sort object-pointer here and when we get picking just
	// check what object-pointer it is and we get our int-value
	// what shapenode we are moving
	typedef std::map<Object*, unsigned int > FrameObjects;

public:
	ShapeObjects();
	// copy-constructor
	ShapeObjects(ShapeObjects& obj, Layer* layer, unsigned int frame, ShapeAnimation& animation);
	ShapeObjects(GameHandler* logic, const unsigned int frame, Layer* layer);
	~ShapeObjects();

	void updateShapes();
	void renderShapes();

	const unsigned int getShapeNode(Object* obj);

	Object* insertShapeNode(const unsigned int pos, const Vector3& position);
	Object* insertShapeNode(const unsigned int pos, const float x, const float y, const float z);
	void removeShapeNode(const unsigned int pos);

	void clearShapeNodes(ShapeAnimation* const animation);

	const unsigned int getFrame() const;

	const unsigned int getSize() const;

	void activateObjects();
	void deactivateObjects();

	void setLayerVisibility(const bool visible);

	void fetchNodes(std::vector<Object*>& objs);

	void setGravity(const float gravity);
	void setSpeed(const float speed);
	void setDeath(const float death);


	const float getGravity() const;
	const float getSpeed() const;
	const float getDeath() const;


	void scaleObjects(const int scale, std::vector<Object*>& objs);
	void rotateObjects(const int rotation, const Vector3& axis, std::vector<Object*>&objs);

private:
	GameHandler* logic;
	FrameObjects objects;
	unsigned int frame;
	Layer* layer;

	// particle settings
	float gravity;
	float speed;
	float death;

	int scale;
};

#endif