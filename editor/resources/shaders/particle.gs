
#include "shaders/common"

#define MAX_NODES 128
#define MAX_BRANCHES 16
#define HALF_BRANCHES 8
#define FLOAT_ELEMENTS 20 * 128

struct InData
{
	float4 position : POSITION;
	float3 velocity : VELOCITY;
	float3 target : TARGET;
	float3 origin : ORIGIN;
	float route : ROUTE;
	float life : LIFE;
	float seed : SEED;
};

struct OutData
{
	float4 position : SV_POSITION;
	float4 color : COLOR0;
	float2 uv : TEXCOORD0;
	float life : COLOR1;
	float delta : COLOR2;
	uint type : TYPE;
};
/*
cbuffer
{
    float3 positions[4] =
    {
        float3( -1, 1, 0 ),
        float3( 1, 1, 0 ),
        float3( -1, -1, 0 ),
        float3( 1, -1, 0 ),
    };
    float2 texcoords[4] = 
    { 
        float2(0,1), 
        float2(1,1),
        float2(0,0),
        float2(1,0),
    };
};
*/

[maxvertexcount(4)]
void main(point InData input[1], inout TriangleStream<OutData> SpriteStream)
{
	OutData output = (OutData)0;
	
	static const float3 positions[4] =
    {
        float3( 0, 1, 0 ),
        float3( 0, -1, 0 ),
        float3( 1, 1, 0 ),
        float3( 1, -1, 0 ),
    };
	
	static const float3 positions_2d[4] =
    {
        float3( -1, 1, 0 ),
        float3( -1, -1, 0 ),
        float3( 1, 1, 0 ),
        float3( 1, -1, 0 ),
    };
	
	/*
	static const float3 positions[4] =
    {
        float3( -1, 1, 0 ),
        float3( -1, -1, 0 ),
        float3( 1, 1, 0 ),
        float3( 1, -1, 0 ),
    };
	*/
	
    static const float2 texcoords[4] = 
    { 
        float2(0, 0), 
        float2(1, 0),
        float2(0, 1),
        float2(1, 1),
    };
	
	float2x2 rot_mat;
	
	//float3 d = input[0].target - input[0].position;
	
	float3 d = input[0].target - input[0].origin;
	float l = length(d);
	
	float delta = length(input[0].origin - input[0].position) / l;
	
	delta = clamp(delta * 1, 0, 1);

	// project velocity to camera to get xy
	float3 velocity = mul(d, (float3x3)view);
	
	float a = atan2(velocity.y, velocity.x);
	
	// get angle of velocity xy
	rot_mat[0] = float2(cos(a), sin(a));
	rot_mat[1] = float2(sin(a), -cos(a));


	
	// now append the offset angle
	// hack
	// offset angle will be the lerp between
	// particles that are not facing camera
	// and facing down
	
	float3x3 orot_mat;
	orot_mat[0] = float3(1, 0, 0);
	orot_mat[1] = float3(0, 0, -1);
	orot_mat[2] = float3(0, 1, 0);
	
	//output.color = input[0].color;
	output.life = input[0].life;
	
	
	
	output.delta = delta;
	output.type = 0;
	
	if (input[0].seed < 10)
	{
	
		for (int i = 0; i < 4; i++)
		{
			float3 position = positions_2d[i] * 1 * input[0].life * (input[0].seed * 0.2);

			position.xy = mul(position.xy, rot_mat);
			
			position = mul(position, (float3x3)inv_view);
			position += input[0].position;
			
			output.position = mul( float4(position, 1.0), view_projection);
			output.uv = texcoords[i];
			
			SpriteStream.Append(output);
		}
		
		SpriteStream.RestartStrip();
	
	}
	else
	{

		output.type = 1;
		for (int i = 0; i < 2; i++)
		{
			float3 position = positions[i] * 1;
			
			// rotate
			position.xy = mul(position.xy, rot_mat);

			position = mul(position, (float3x3)inv_view);
			//
			position += input[0].origin;
			
			output.position = mul( float4(position, 1.0), view_projection);
			output.uv = texcoords[i];
			
			SpriteStream.Append(output);
		}
		
		for (int i = 2; i < 4; i++)
		{
			float3 position = positions[i] * 1;
			
			// rotate
			position.xy = mul(position.xy, rot_mat);

			position = mul(position, (float3x3)inv_view);
			//
			position += input[0].target;
			
			output.position = mul( float4(position, 1.0), view_projection);
			output.uv = texcoords[i];
			
			SpriteStream.Append(output);
		}
		
		SpriteStream.RestartStrip();
	
	}
	
}