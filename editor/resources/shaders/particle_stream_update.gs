

#include "shaders/common"

#define MAX_NODES 128
#define MAX_BRANCHES 16
#define HALF_BRANCHES 8
#define FLOAT_ELEMENTS 20 * 128

cbuffer ShapeResult : register(b4)
{
	float4 aggressivePack4[FLOAT_ELEMENTS / 4];
};

float3 getRoutePosition(int route)
{
	return aggressivePack4[route * 5 + 4].xyz;
}

float getNextRoute(float route, float seed)
{
	float nroute = aggressivePack4[floor(route) * 5 + floor(seed) / 4][(int)seed % 4];
	
	//if (abs(nroute - route) < 1)
	//	nroute += 1;
		
	return nroute;
}


struct InData
{
	float4 position : POSITION;
	float3 velocity : VELOCITY;
	float3 target : TARGET;
	float3 origin : ORIGIN;
	float route : ROUTE;
	float life : LIFE;
	float seed : SEED;
};

Texture2D Rand : register(t0);
SamplerState RandSampler : register(s0);

float3 getRandOffset(float seed)
{
	float x = sin(seed + timeframe);
	float y = cos(seed - timeframe * 2);
	
	return Rand.SampleLevel(RandSampler, float2(x, y), 0).xyz * 2 - 1;
}

[maxvertexcount(1)]
void main(point InData input[1], inout PointStream<InData> OutStream)
{
	InData res;
	
	res = input[0];
	
	if (res.life > 0)
	{

		// get next waypoint
		float3 waypoint = getRoutePosition((int)res.route);
		
		res.target = waypoint + getRandOffset(res.seed + res.position.x * res.position.y + res.position.z * 2);
		
		float3 d = (res.target - res.position.xyz);
		//float sqr_len = d.x * d.x + d.y * d.y + d.z * d.z;
		float l = length(d);
		float3 dn = d / l;
	
		
		
		
		float m = (res.seed / HALF_BRANCHES) - HALF_BRANCHES;
		float rsin = sin(timeframe * 50);
		
		float3 crs = cross(float3(-dn.y, -dn.x, -dn.z), dn);
		
		
		if (l < 2)
		{
			res.origin = waypoint + getRandOffset(res.seed / 16.0);
			res.route = getNextRoute(res.route, res.seed);
			res.target = getRoutePosition((int)res.route);
		}
		
		// movement
		
		if (res.seed < 10)
		{
			res.velocity += d * (0.001 + (res.seed * 0.001));
			res.position.xyz += res.velocity;
			res.position.xyz += dn * (0.001 + (res.seed * 0.001));
			res.position.xyz += d * (0.001 * (res.seed * 0.01));
			
			res.life -= 0.006;
			
			res.velocity *= (0.98 - res.seed * 0.01);
		}
		else
		{
			res.velocity += d * (0.02 + (res.seed * 0.01));
			//res.position.xyz += res.velocity;
			res.position.xyz += dn * 0.4;
			res.position.xyz += d * (0.4 + ((res.seed - 12) * 0.1));
			
			res.life -= 0.008;
		}

		
		
		
		
		OutStream.Append(res);
	}
}