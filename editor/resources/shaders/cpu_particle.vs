
struct InData
{
	float4 position : POSITION;
	float3 velocity : VELOCITY;
	float4 color : COLOR;
	float scale : SCALE;
	float angle : ANGLE;
	float oangle: OANGLE;
	float life : LIFE;
};

InData main(InData input)
{
	return input;
}