
struct InData
{
	float4 position : POSITION;
	float3 velocity : VELOCITY;
	float3 target : TARGET;
	float3 origin : ORIGIN;
	float route : ROUTE;
	float life : LIFE;
	float seed : SEED;
};

InData main(InData input)
{
	return input;
}