
#include "shaders/common"

struct InData
{
	float4 position : POSITION;
	float3 velocity : VELOCITY;
	float4 color : COLOR;
	float scale : SCALE;
	float angle : ANGLE;
	float oangle: OANGLE;
	float life : LIFE;

};

struct OutData
{
	float4 position : SV_POSITION;
	float4 color : COLOR0;
	float2 uv : TEXCOORD0;
	float life : COLOR1;
};
/*
cbuffer
{
    float3 positions[4] =
    {
        float3( -1, 1, 0 ),
        float3( 1, 1, 0 ),
        float3( -1, -1, 0 ),
        float3( 1, -1, 0 ),
    };
    float2 texcoords[4] = 
    { 
        float2(0,1), 
        float2(1,1),
        float2(0,0),
        float2(1,0),
    };
};
*/

[maxvertexcount(4)]
void main(point InData input[1], inout TriangleStream<OutData> SpriteStream)
{
	if (input[0].life <= 0.0)
		return;


	OutData output = (OutData)0;
	
    static const float3 positions[4] =
    {
        float3( -1, 1, 0 ),
        float3( 1, 1, 0 ),
        float3( -1, -1, 0 ),
        float3( 1, -1, 0 ),
    };
	
    static const float2 texcoords[4] = 
    { 
        float2(0,0), 
        float2(1,0),
        float2(0,1),
        float2(1,1),
    };
	
	float2x2 rot_mat;

	if (input[0].angle < 0)
	{
		// project velocity to camera to get xy
		float3 velocity = mul(input[0].velocity, (float3x3)view);
		
		float a = atan2(velocity.y, velocity.x);
		
		// get angle of velocity xy
		rot_mat[0] = float2(cos(a), sin(a));
		rot_mat[1] = float2(sin(a), -cos(a));
	}
	else
	{
		float c = cos(input[0].angle);
		float s = sin(input[0].angle);
		
		rot_mat[0] = float2(c, -s);
		rot_mat[1] = float2(s, c);

		/*
		//float zc = cos(input[0].oangle);
		//float zs = sin(input[0].oangle);
		float zc = cos(3.14f / 2);
		float zs = sin(3.14f / 2);
		

		
		rot_mat[0] = float3(c, -s, 0);
		rot_mat[1] = float3(s, c * zc, -zs);
		rot_mat[2] = float3(0, zs, zc);
		
		//rot_mat[0] = float3(zc, 0, zs);
		//rot_mat[1] = float3(0, 1, 0);
		//rot_mat[2] = float3(-zs, 0, zc);
		
		//rot_mat[0] = float3(zc, -zs, 0);
		//rot_mat[1] = float3(zs, zc, 0);
		//rot_mat[2] = float3(0, 0, 1);
		*/
	}
	
	// now append the offset angle
	// hack
	// offset angle will be the lerp between
	// particles that are not facing camera
	// and facing down
	
	float3x3 orot_mat;
	orot_mat[0] = float3(1, 0, 0);
	orot_mat[1] = float3(0, 0, -1);
	orot_mat[2] = float3(0, 1, 0);
	
	output.color = input[0].color;
	output.life = input[0].life;
	
	for (int i = 0; i < 4; i++)
    {
		float3 position = positions[i] * input[0].scale;
		
		// rotate
		
		/*
		position.xy = mul(position.xy, (float2x2)rot_mat);
		if (input[0].oangle > 0.5f)
			position = mul(position, orot_mat);
		else
		*/
			position = mul(position, (float3x3)inv_view);
		position += input[0].position;
		
		output.position = mul( float4(position, 1.0), view_projection);
		output.uv = texcoords[i];
		
		SpriteStream.Append(output);
	}
	
}