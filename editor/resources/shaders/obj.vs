#ifndef SKYSPHERE_VS
#define SKYSPHERE_VS

#include "common"

struct InputData
{
	float4 position 	: POSITION;
	float3 normal		: NORMAL;
	float2 texcoord		: TEXCOORD;
};

struct OutputData
{
	float4 position 	: SV_POSITION;
	float4 blend_col 	: BLENDCOLOR;
	float3 normal		: NORMAL;
	float2 texcoord		: TEXCOORD;
};

OutputData main(InputData input)
{
	OutputData output = (OutputData)0;
	
	output.position = mul(input.position, model);
	output.position = mul(output.position, view_projection);
	output.normal = mul(input.normal, model);
	output.texcoord = input.texcoord;
	output.blend_col = blend;
	return output;
}

#endif