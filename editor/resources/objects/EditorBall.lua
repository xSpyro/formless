EditorBall =
{
	components =
	{
		physics =
		{
			scale = {0.5, 0.5, 0.5 },
			position = {0, 0, 0},
			rotation = {3, 3, 3}
		},
		render =
		{
			model = "boll"
		},
		script =
		{
			callbacks =
			{
				onDestroy = function() end,
			}
		}
	}
}