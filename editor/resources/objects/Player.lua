Player =
{
	components =
	{
		physics =
		{
			scale = {1, 1, 1},
			position = {2, 2, 2},
			rotation = {3, 3, 3}
		},
		render =
		{
			model = "boll"
		},
		controller =
		{
			keybinds =
			{
				"up",
				"left",
				"down",
				"right"
			}
		},
		script =
		{
			callbacks =
			{
				onCreated = function(id)
					subscribeKeyBind("push")
					subscribeKeyBind("attack")
				end,
				onDestroy = function() end,
				onKeyBind = function(name, state)
					if state == 1 then
						if name == "push" then
							move(0, 1.0, 0.0, 0.0);
						end
						if name == "attack" then
							create("Ball")
						end
					end
				end
			}
		}
	}
}