﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Controls.Primitives;

namespace wpf
{
    public class ToggleButtonCompare : IComparer<ToggleButton>
    {
        public int Compare(ToggleButton tb1, ToggleButton tb2)
        {
            int first = Int32.Parse( tb1.Content.ToString() );
            int second = Int32.Parse( tb2.Content.ToString() );

            return first.CompareTo(second);
        }
    }
}
