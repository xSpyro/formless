﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

using wpf.EditorControllers;


namespace wpf
{
    class LayerItem : Border
    {
        LayerContent content;
        LevelEditorController parent;

        TextBox textBox;

        bool locked;

        public LayerItem(ToggleButton toggle, LevelEditorController father)
        {
            ToggleButton toggleSelection = new ToggleButton();


            content = new LayerContent(toggleSelection, toggle);

            this.BorderThickness = new Thickness(1);
            this.CornerRadius = new CornerRadius(1);
            this.Margin = new Thickness(1, 1, 1, 1);
            this.Width = 100;

            toggleSelection.IsChecked = false;
            toggleSelection.Checked += toggleSelect;

            toggle.MouseRightButtonUp += summonTextBox;
            toggle.Checked += toggleShowLayer;
            toggle.Unchecked += toggleHideLayer;


            textBox = new TextBox();
            textBox.Visibility = Visibility.Hidden;

            parent = father;
            locked = false;

            Color c = new Color();
            c.A = 50;
            c.B = 255;
            c.G = 255;
            c.R = 255;

            SolidColorBrush scb = new SolidColorBrush(c);

            this.BorderBrush = scb;
            this.Child = content;
        }

        public void setLocked(bool state)
        {
            locked = state;
        }

        public bool isLocked()
        {
            return locked;
        }

        public string getLayerName()
        {
            return ((ToggleButton)content.Children[1]).Content.ToString();
        }


        public void setSelectionToggle(bool state)
        {
            ((ToggleButton)content.Children[0]).IsChecked = state;
        }

        private void toggleSelect(object sender, RoutedEventArgs e)
        {
            parent.toggleExclusive(getLayerName());
        }


        private void toggleShowLayer(object sender, RoutedEventArgs e)
        {

            parent.showLayer(getLayerName());
        }

        private void toggleHideLayer(object sender, RoutedEventArgs e)
        {
            parent.hideLayer(getLayerName());
        }

        /***********************************
         * Summons a textBox depending on the
         * position of sender (a Button)
         * */
        private void summonTextBox(object sender, RoutedEventArgs e)
        {
            if (!locked)
            {
                ToggleButton tempbtn = (ToggleButton)sender;

                //slight offset in x... stylistic!
                Point boxPos = tempbtn.TranslatePoint(new Point(-20, 0), parent.wnd.rightGrid);
                Thickness boxMargin = new Thickness(boxPos.X, boxPos.Y, 0, 0);

                textBox.Text = "";
                textBox.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                textBox.VerticalAlignment = System.Windows.VerticalAlignment.Top;
                textBox.Margin = boxMargin;
                textBox.Visibility = Visibility.Visible;

                textBox.Loaded += loadedTextBox;
                textBox.PreviewKeyDown += closeTextBox;
                textBox.Focus();

                //add to baseGrid only first time(when it is not already in it)
                if (textBox.Parent == null)
                    parent.wnd.rightGrid.Children.Add(textBox);
            }
        }

        private void loadedTextBox(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).Focus();
        }

        private void closeTextBox(object sender, KeyEventArgs key)
        {
            if (key.Key == Key.Enter)
            {
                ((ToggleButton)content.Children[1]).Content = textBox.Text;
                textBox.Visibility = Visibility.Hidden;
            }
        }
    }

    /***
     * FrameContent : StackPanel
     * 
     * contains the Controls necessary for a frame
     * 
     * **/
    public class LayerContent : StackPanel
    {
        public LayerContent(ToggleButton one, ToggleButton two)
        {
            this.Orientation = Orientation.Horizontal;

            Color c = new Color();
            c.A = 20;
            c.B = 255;
            c.G = 255;
            c.R = 255;
            SolidColorBrush scb = new SolidColorBrush(c);

            one.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;

            one.MinWidth = 10;
            one.Width = 15;

            two.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;


            two.MinWidth = 10;
            two.Width = 83;


            this.Background = scb;

            this.Children.Add(one);
            this.Children.Add(two);
            
        }
    }
}
