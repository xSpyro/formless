﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;


namespace wpf
{
    public class FrameSlider
    {
        public Label sliderLabel;
        public Slider slider;

        public FrameSlider(int width, int height, Thickness margin)
        {
            sliderLabel = new Label();
            slider = new Slider();

            slider.IsSnapToTickEnabled = false;
            slider.Width = width;
            //slider.Width = slider.ActualWidth;
            slider.Height = height;
            slider.Minimum = 0;
            slider.Maximum = 0;


            slider.Margin = margin;

            sliderLabel.Content = slider.Value;
            sliderLabel.Margin = margin;
            slider.AutoToolTipPlacement = AutoToolTipPlacement.BottomRight;
            slider.TickPlacement = TickPlacement.None;
           
            sliderLabel.Visibility = Visibility.Visible;
        }

        public void setInterval(int min, int max)
        {
            slider.Minimum = min;
            slider.Maximum = max;
        }

        //this doesnt call ValueChanged in the slider....
        public void slideTo(int pos)
        {
            slider.Value = pos;
            
        }

        //not in use
        //maybe for future use if ToolTip doesnt suffice
        public void setLabel(int num)
        {
            TranslateTransform tt = new TranslateTransform(num, 0);

            sliderLabel.RenderTransform = tt;

            sliderLabel.Content = num.ToString();
        }

    }
}
