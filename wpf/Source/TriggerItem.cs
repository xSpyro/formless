﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace wpf
{
    class TriggerItem : Border
    {
        TriggerContent content;
        LevelEditorTools parent;

        TextBox textBox;
        ScriptBox scriptBox;

        //i actually dont need to save the content of the scriptbox, cause i overrided the close() in a way...
        String scriptBoxContent;
        String scriptBoxTitle;


        public TriggerItem(ToggleButton toggleTrigger, ToggleButton toggleName, TextBox tb, LevelEditorTools father)
        {
            content = new TriggerContent(toggleTrigger, toggleName);

            this.BorderThickness = new Thickness(1);
            this.CornerRadius = new CornerRadius(3);
            this.Margin = new Thickness(1, 1, 1, 1);



            toggleName.Checked += summonTextBox;

            toggleTrigger.Click += toggle;
            toggleTrigger.IsChecked = true;

            textBox = tb;
            textBox.Visibility = Visibility.Hidden;

            parent = father;

            Color c = new Color();
            c.A = 50;
            c.B = 255;
            c.G = 255;
            c.R = 255;

            scriptBoxTitle = "";
            scriptBoxContent = "";

            SolidColorBrush scb = new SolidColorBrush(c);

            this.BorderBrush = scb;
            this.Child = content;
        }

        private void toggle(object sender, RoutedEventArgs e)
        {

            if (scriptBox == null)
            {
                //if scriptBox was closed
                //create new one, assign old content!

                scriptBox = new ScriptBox();

                scriptBox.Closing += onCloseScriptBox;

                scriptBox.textBox1.Text = scriptBoxContent;
                
                scriptBox.Title = scriptBoxTitle;
                
                scriptBox.Show();
                scriptBox.Focus();

                ((ToggleButton)content.Children[0]).IsChecked = false;
            }
            else
            {
                scriptBoxContent = scriptBox.textBox1.Text;
                scriptBox.Close();
            }

        }


        private void onCloseScriptBox(object sender, CancelEventArgs e)
        {
            scriptBoxContent = scriptBox.textBox1.Text;
            //scriptBox.Visibility = Visibility.Hidden;

            scriptBox = null;

            ((ToggleButton)content.Children[0]).IsChecked = true;

            //save the content to lua file manager
            parent.saveLua(scriptBoxTitle, scriptBoxContent);

            //cancel the closing, because once you call Close(), you cannot call Show() on that same object...
            //this causes a problem: the application wont shut down even if you think you've closed the windows...
            //e.Cancel = true;
        }


        /***********************************
         * Summons a textBox depending on the
         * position of sender (a Button)
         * */
        private void summonTextBox(object sender, RoutedEventArgs e)
        {
            ToggleButton tempbtn = (ToggleButton)sender;

            Point boxPos = tempbtn.TranslatePoint(new Point(-90, 0), parent);
            Thickness boxMargin = new Thickness(boxPos.X, boxPos.Y, 0, 0);

            textBox.Text = "";
            textBox.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            textBox.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            textBox.Margin = boxMargin;
            textBox.Visibility = Visibility.Visible;

            textBox.Focus();

            //add to baseGrid only first time(when it is not already in it)
            if (textBox.Parent == null)
            {
                textBox.Loaded += loadedTextBox;
                textBox.KeyUp += closeTextBox;
                parent.baseGrid.Children.Add(textBox);
            }

        }

        private void loadedTextBox(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).Focus();
        }

        private void closeTextBox(object sender, KeyEventArgs key)
        {
            if ( key.Key == Key.Enter)
            {
                ((ToggleButton)content.Children[0]).Content = textBox.Text;
                textBox.Visibility = Visibility.Hidden;
                ((ToggleButton)content.Children[1]).IsChecked = false;

                string oldTitle = scriptBoxTitle;

                scriptBoxTitle = textBox.Text;

                string newTitle = scriptBoxTitle;

                //send to the lua file manager
                parent.renameLua(oldTitle, newTitle);
                
            }
        }
    }

    /***
     * FrameContent : StackPanel
     * 
     * contains the Controls necessary for a frame
     * 
     * **/
    public class TriggerContent : StackPanel
    {
        public TriggerContent(ToggleButton toggle1, ToggleButton toggle2)
        {
            this.Orientation = Orientation.Horizontal;

            Color c = new Color();
            c.A = 20;
            c.B = 255;
            c.G = 255;
            c.R = 255;
            SolidColorBrush scb = new SolidColorBrush(c);

            toggle1.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            toggle2.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;

            toggle1.Width = 120;

            toggle2.MinWidth = 10;
            toggle2.Width = 20;

            toggle2.Content = "T";


            this.Background = scb;

            this.Children.Add(toggle1);
            this.Children.Add(toggle2);
        }
    }
}
