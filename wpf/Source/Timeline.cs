﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

using System.Windows.Interop;

using interopScene;

namespace wpf
{



    public class Timeline : WrapPanel
    {
        enum Message
        {
            MESSAGE_SWITCH_WORKSPACE,
            MESSAGE_REMOVE_WORKSPACE,
            MESSAGE_VISIBILITY,
            MESSAGE_PLAY_ANIMATION,
            MESSAGE_COPY_WORKSPACE,
            MESSAGE_OPEN_FILE,
            MESSAGE_SAVE_FILE,
            MESSAGE_OPEN_POINTS_FROM_OBJ,
            MESSAGE_SET_FRAME,
            MESSAGE_DISABLE_FRAME,
            MESSAGE_SELECT_TOOL,
            MESSAGE_SELECT_ANIMATION,
            MESSAGE_SELECT_LINKING,
            MESSAGE_DOUBLE_LINK,
            MESSAGE_CLEAR_ALL,
            MESSAGE_MOVE_CAMERA,
            MESSAGE_UNDO,
            MESSAGE_REDO,
            MESSAGE_END
        };
        Functions fun;
        SolidColorBrush selectedColor;
        SolidColorBrush unselectedColor;

        private Editor editor;
        public Timeline()
        {
            fun = new Functions();

            Color c2 = new Color();
            c2.A = 45;
            c2.B = 255;
            c2.G = 255;
            c2.R = 255;
            selectedColor = new SolidColorBrush(c2);

            Color c3 = new Color();
            c3.A = 20;
            c3.B = 255;
            c3.G = 255;
            c3.R = 255;
            unselectedColor = new SolidColorBrush(c3);
        }

        public void toggleExclusive(ToggleButton btn)
        {

            foreach (FrameItem it in this.Children)
            {
                FrameContent content = (FrameContent)it.Child;
                ToggleButton temp = (ToggleButton)content.Children[0];
                CheckBox temp2 = (CheckBox)content.Children[1];

                //if we found the FrameItem with the ToggleButton that was sent
                if (temp.Equals(btn))
                {
                    temp.IsChecked = true;
                    temp.Background = selectedColor;
                    content.Background = selectedColor;
                }
                else
                {
                    temp.IsChecked = false;
                    content.Background = unselectedColor;
                }
            }
        }

        public void clearCheckBoxes(int frameMultiplier)
        {
            int i = 0;
            foreach (FrameItem it in this.Children)
            {
                FrameContent content = (FrameContent)it.Child;

                CheckBox temp = (CheckBox)content.Children[1];
                
                if(temp.IsChecked == true)
                {
                    //if(i*frameMultiplier != 0)
                    editor.SendMessage((int)Message.MESSAGE_REMOVE_WORKSPACE, i * frameMultiplier);
                    editor.SendMessage((int)Message.MESSAGE_SWITCH_WORKSPACE, 0);
                    temp.IsChecked = false;
                }
                ++i;
            }
        }

        public void copyFrameFromCheckBox(int frameMultiplier)
        {
            int i = 0;
            foreach (FrameItem it in this.Children)
            {
                FrameContent content = (FrameContent)it.Child;

                CheckBox temp = (CheckBox)content.Children[1];

                if (temp.IsChecked == true)
                {
                    editor.SendMessage((int)Message.MESSAGE_COPY_WORKSPACE, i * frameMultiplier); 
                    temp.IsChecked = false;
                }

                ++i;
            }
        }
        public void addFrame(FrameItem item)
        {
            this.Children.Add(item);
        }

        public void setEditor(Editor editor)
        {
            this.editor = editor;
        }
    }
}
