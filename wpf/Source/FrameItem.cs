﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace wpf
{
    /***
     * FrameItem : Border
     * 
     * For making a "frame" in the timeline
     * with a Border and the FrameContent
     * **/
    public class FrameItem : Border
    {
        FrameContent content;
        
        public FrameItem(ToggleButton tbtn, CheckBox cb)
        {
            content = new FrameContent(tbtn, cb);
            this.BorderThickness = new Thickness(1);
            this.CornerRadius = new CornerRadius(3);
            this.Margin = new Thickness(1, 1, 1, 2);

            Color c = new Color();
            c.A = 50;
            c.B = 255;
            c.G = 255;
            c.R = 255;
            SolidColorBrush scb = new SolidColorBrush(c);

            this.BorderBrush = scb;
            this.Child = content;
        }
    }

    /***
     * FrameContent : StackPanel
     * 
     * contains the Controls necessary for a frame
     * 
     * **/
    public class FrameContent : StackPanel
    {
        public FrameContent(ToggleButton tbtn, CheckBox cb)
        {
            //this.Orientation = Orientation.Vertical;

            Color c = new Color();
            c.A = 20;
            c.B = 255;
            c.G = 255;
            c.R = 255;
            SolidColorBrush scb = new SolidColorBrush(c);

            cb.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;

            this.Background = scb;

            this.Children.Add(tbtn);
            this.Children.Add(cb);
        }
    }
}
