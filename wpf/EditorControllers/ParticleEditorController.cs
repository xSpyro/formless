﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using interopScene;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Windows.Interop;

using Microsoft.Win32;

using wpf;

namespace wpf.EditorControllers
{
    class ParticleEditorController : EditorController
    {
        enum Message
        {
            MESSAGE_MOVE_CAMERA,
            MESSAGE_OPEN_FILE,
            MESSAGE_SAVE_FILE,
            MESSAGE_NEW_FILE

        };

        enum Movement
        {
            LEFT_PRESS,
            LEFT_RELEASE,
            RIGHT_PRESS,
            RIGHT_RELEASE,
            UP_PRESS,
            UP_RELEASE,
            DOWN_PRESS,
            DOWN_RELEASE
        };
        private Microsoft.Win32.SaveFileDialog SaveDialog()
        {

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".paf";

            dlg.Filter = "effects(.paf)|*.paf;";

            dlg.ShowDialog();

            return dlg;
        }
        private Microsoft.Win32.OpenFileDialog OpenDialog()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".paf";

            dlg.Filter = "effects (.paf)|*.paf;";
            dlg.ShowDialog();

            return dlg;
        }

        public ParticleEditorController(Editor ed) : base(ed)
        {
            particleEditorTools = new ParticleEditorTools(ed);
            
        }

        public override bool hideEditor()
        {
            base.hideEditor();

            particleEditorTools.Visibility = Visibility.Hidden;

            return true;
        }

        public override bool showEditor()
        {
            base.showEditor();

            particleEditorTools.Visibility = Visibility.Visible;

            return true;
        }

        public override void keyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.W)
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Movement.UP_PRESS);
            }
            if (e.Key == Key.A)
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Movement.LEFT_PRESS);
            }
            if (e.Key == Key.S)
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Movement.DOWN_PRESS);
            }
            if (e.Key == Key.D)
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Movement.RIGHT_PRESS);
            }
        }

        public override void keyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.W)
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Movement.UP_RELEASE);
            }
            if (e.Key == Key.A)
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Movement.LEFT_RELEASE);
            }
            if (e.Key == Key.S)
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Movement.DOWN_RELEASE);
            }
            if (e.Key == Key.D)
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Movement.RIGHT_RELEASE);
            }
        }

        public override void saveFile()
        {
            SaveFileDialog dlg = SaveDialog();
            if (dlg.FilterIndex == 1)
            {
                save(dlg.SafeFileName);
                particleEditorTools.UpdateAnimations();
                particleEditorTools.UpdateEmitters();
            }
        }
        public void save(string file)
        {
            editor.SendMessage((int)Message.MESSAGE_SAVE_FILE, file);
        }
        public void open(string file)
        {
            editor.SendMessage((int)Message.MESSAGE_OPEN_FILE, file);
        }
        public override void openFile()
        {
            OpenFileDialog dlg = OpenDialog();
            if (dlg.FilterIndex == 1)
            {
                open(dlg.FileName);
                particleEditorTools.UpdateAnimations();
                particleEditorTools.UpdateEmitters();
            }
        }

        public override void newFile()
        {

        }

        public ParticleEditorTools particleEditorTools;

    }
}
