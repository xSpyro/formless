﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using interopScene;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Windows.Interop;

using Microsoft.Win32;

using wpf;
namespace wpf.EditorControllers
{
    class ShapeEditorController : EditorController
    {

        public ShapeEditorController(Editor ed, MainWindow wnd) : base(ed)
        {
            shapeEditorTools = new ShapeEditorTools(ed);

            shapeSlider = new FrameSlider(900, 30, new Thickness(90, 0, 0, 85));

            timeline = new Timeline();
            timeline.setEditor(editor);

            shapeSlider.slider.HorizontalAlignment = HorizontalAlignment.Left;
            shapeSlider.slider.VerticalAlignment = VerticalAlignment.Bottom;

            shapeSlider.slider.ValueChanged += changeFrame;

            shapeSlider.slider.GotMouseCapture += sliding;

            shapeSlider.slider.LostMouseCapture += disableFrame;

            this.wnd = wnd;

            wnd.button1.Click += clearButtonClick;
            wnd.button8.Click += play;
            wnd.copybutton.Click += copyClick;
            wnd.textBlock1.MouseWheel += setFrameMultiplier;
            wnd.textBlock2.MouseWheel += setFrameMultiplier;
        }

        public void update()
        {

            shapeEditorTools.update();

            maxFrame = (int)editor.GetMaxframe();
            shapeSlider.setInterval(0, maxFrame);

            //shapeEditorTools.update();
            if (!isSliding)
            {

                //shapeSlider.slideTo((int)editor.GetCurrentPlayframe());
            }
            if (isPlaying)
            {
                

                if (isAnimationForward)
                {
                    shapeSlider.slider.Value++;
                    if (shapeSlider.slider.Value >= maxFrame)
                    {
                        isAnimationForward = false;
                    }
                }
                else
                {
                    shapeSlider.slider.Value--;
                    if (shapeSlider.slider.Value <= 0)
                    {
                        isAnimationForward = true;
                    }
                }


            }
        }

        public override bool hideEditor() 
        {
            base.hideEditor();

            shapeSlider.slider.Visibility = Visibility.Hidden;
            shapeSlider.sliderLabel.Visibility = Visibility.Hidden;
            shapeEditorTools.Visibility = Visibility.Hidden;
            timeline.Visibility = Visibility.Hidden;

            wnd.copybutton.Visibility = Visibility.Hidden;
            wnd.button1.Visibility = Visibility.Hidden;
            wnd.button1.Visibility = Visibility.Hidden;
            wnd.button8.Visibility = Visibility.Hidden;
            wnd.textBlock1.Visibility = Visibility.Hidden;
            wnd.textBlock2.Visibility = Visibility.Hidden;
            wnd.scrollViewer1.Visibility = Visibility.Hidden;
            
            return true;
        }

        public override bool showEditor()
        {
            base.showEditor();

            shapeSlider.slider.Visibility = Visibility.Visible;
            shapeSlider.sliderLabel.Visibility = Visibility.Visible;
            shapeEditorTools.Visibility = Visibility.Visible;
            timeline.Visibility = Visibility.Visible;

            wnd.copybutton.Visibility = Visibility.Visible;
            wnd.button1.Visibility = Visibility.Visible;
            wnd.button1.Visibility = Visibility.Visible;
            wnd.button8.Visibility = Visibility.Visible;
            wnd.textBlock1.Visibility = Visibility.Visible;
            wnd.textBlock2.Visibility = Visibility.Visible;
            wnd.scrollViewer1.Visibility = Visibility.Visible;

            return true;
        }

        private Microsoft.Win32.SaveFileDialog SaveDialog()
        {

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".pas";

            dlg.Filter = "Particle Animation Sequence(.pas)|*.pas";

            dlg.ShowDialog();

            return dlg;

        }

        private Microsoft.Win32.OpenFileDialog OpenDialog()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".pas";

            dlg.Filter = "Particle Animation Sequence(.pas)|*.pas;|Wavefront OBJ-file (.obj)|*.obj;";
            dlg.ShowDialog();

            return dlg;
        }

        public override void saveFile()
        {
            SaveFileDialog dlg = SaveDialog();
            if (dlg.FilterIndex == 1)
            {
                save(dlg.FileName);
            }
        }

        public override void openFile()
        {
            OpenFileDialog dlg = OpenDialog();
            if (dlg.FilterIndex == 1)
            {
                open(dlg.FileName);
            }
            else if (dlg.FilterIndex == 2)
            {
                openOBJ(dlg.FileName);
            }
        }

        public override void newFile()
        {
            
        }

        public override void keyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {

                //when shift is down(held) then linking starts
                //shapeEditorTools.radioButton2.IsChecked = true;
                //editor.SendMessage((int)Message.SELECT_TOOL, (int)Tool.LINK);

                if (isVisible())
                    editor.SendMessage((int)Message.MESSAGE_SELECT_TOOL, (int)Tool.TOOL_SHIFT_PICKING);
            }


            if (e.Key == Key.D1)
            {
                setTool(Tool.TOOL_PICKING);
            }

            if (e.Key == Key.D2)
            {
                setTool(Tool.TOOL_LINK);
            }

            if (e.Key == Key.D3)
            {
                setTool(Tool.TOOL_REMOVAL);
            }

            if (e.Key == Key.D4)
            {
                setTool(Tool.TOOL_UNLINK);
            }

            if (e.Key == Key.F3)
            {
                editor.SendMessage((int)Message.MESSAGE_RELOAD_SHADERS, 0);
            }

            if (e.Key == Key.Z && Keyboard.Modifiers == ModifierKeys.Control)
            {
                undo();
            }

            if (e.Key == Key.Y && Keyboard.Modifiers == ModifierKeys.Control)
            {
                redo();
            }

            if (e.Key == Key.V && Keyboard.Modifiers == ModifierKeys.Control)
            {
                editor.SendMessage((int)Message.MESSAGE_COPY_NODES, 0);
            }

            if (e.Key == Key.X && Keyboard.Modifiers == ModifierKeys.Control)
            {
                editor.SendMessage((int)Message.MESSAGE_ALIGN_MOUSE, (int)MouseAlignments.MOUSE_ALIGNMENT_X);
            }

            if (e.Key == Key.Y && Keyboard.Modifiers == ModifierKeys.Control)
            {
                editor.SendMessage((int)Message.MESSAGE_ALIGN_MOUSE, (int)MouseAlignments.MOUSE_ALIGNMENT_Y);
            }

            if (e.Key == Key.R && Keyboard.Modifiers == ModifierKeys.Control)
            {
                editor.SendMessage((int)Message.MESSAGE_ALIGN_MOUSE, (int)MouseAlignments.MOUSE_ALIGNMENT_NONE);
            }

            if (e.Key == Key.A)
            {
                editor.SendMessage((int)Message.MESSAGE_ADD_NODE, 0);
            }

            if (e.Key == Key.Delete)
            {
                editor.SendMessage((int)Message.MESSAGE_REMOVE_NODE, 0);
            }

            if (e.Key == Key.Add)
            {
                // scale
                editor.SendMessage((int)Message.MESSAGE_SCALE_OBJECTS, (int)1);
            }
            
            if (e.Key == Key.Subtract)
            {
                // scale
                editor.SendMessage((int)Message.MESSAGE_SCALE_OBJECTS, (int)-1);
            }

            if (e.Key == Key.NumPad7)
            {
                // rot x
                editor.SendMessage((int)Message.MESSAGE_ROTATE_OBJECTS_IN_XAXIS, (int)1);
            }

            if (e.Key == Key.NumPad4)
            {
                // rot x
                editor.SendMessage((int)Message.MESSAGE_ROTATE_OBJECTS_IN_XAXIS, (int)-1);
            }

            if (e.Key == Key.NumPad8)
            {
                // rot y
                editor.SendMessage((int)Message.MESSAGE_ROTATE_OBJECTS_IN_YAXIS, (int)1);
            }

            if (e.Key == Key.NumPad5)
            {
                // rot y
                editor.SendMessage((int)Message.MESSAGE_ROTATE_OBJECTS_IN_YAXIS, (int)-1);
            }

            if (e.Key == Key.NumPad9)
            {
                // rot z
                editor.SendMessage((int)Message.MESSAGE_ROTATE_OBJECTS_IN_ZAXIS, (int)1);
            }

            if (e.Key == Key.NumPad6)
            {
                // rot z
                editor.SendMessage((int)Message.MESSAGE_ROTATE_OBJECTS_IN_ZAXIS, (int)-1);
            }


        }

        public override void keyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {

                // when shift is down(held) then linking starts
                // shapeEditorTools.radioButton2.IsChecked = true;
                // editor.SendMessage((int)Message.SELECT_TOOL, (int)Tool.LINK);

                if (isVisible())
                    editor.SendMessage((int)Message.MESSAGE_SELECT_TOOL, (int)Tool.TOOL_PICKING);
            }
        }

        public void undo()
        {
            editor.SendMessage((int)Message.MESSAGE_UNDO, (int)0);
        }

        public void redo()
        {
            editor.SendMessage((int)Message.MESSAGE_REDO, (int)0);
        }

        public void sliding(object sender, RoutedEventArgs e)
        {
            if (!isPlaying)
            {
                isSliding = true;
                editor.SendMessage((int)Message.MESSAGE_SET_FRAME, 0);
            }
        }

        public void play(object sender, RoutedEventArgs e)
        {
            if (isPlaying == true)
            {
                isPlaying = false;
                wnd.button8.Content = "Play";
                editor.SendMessage((int)Message.MESSAGE_DISABLE_FRAME, 0);

            }
            else
            {
                isPlaying = true;
                wnd.button8.Content = "Pause";
                editor.SendMessage((int)Message.MESSAGE_SET_FRAME, (int)shapeSlider.slider.Value);
            }
        }

        public void changeFrame(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_PLAY_ANIMATION, (int)shapeSlider.slider.Value);
        }

        public void disableFrame(object sender, RoutedEventArgs e)
        {
            if (!isPlaying)
            {
                isSliding = false;
                editor.SendMessage((int)Message.MESSAGE_DISABLE_FRAME, (int)shapeSlider.slider.Value);
            }
        }

        public void frameToggle(object sender, RoutedEventArgs e)
        {
            int frame = 0;
            if (sender.GetType() == typeof(FrameItem))
            {
                FrameItem tempFrame = (FrameItem)sender;
                FrameContent tempContent = (FrameContent)tempFrame.Child;
                ToggleButton tempButton = (ToggleButton)tempContent.Children[0];

                timeline.toggleExclusive(tempButton);

                //does not proudly present: the worst code line in history. almost.
                frame = Int32.Parse(tempButton.Content.ToString());
            }
            else if (sender.GetType() == typeof(ToggleButton))
            {
                ToggleButton tb = (ToggleButton)sender;
                timeline.toggleExclusive(tb);
                frame = Int32.Parse(tb.Content.ToString());
            }

            frame *= frameMultiplier;

            editor.SendMessage((int)Message.MESSAGE_SWITCH_WORKSPACE, frame);

            shapeEditorTools.updateSliderValues();
        }

        public void copyClick(object sender, RoutedEventArgs e)
        {
            timeline.copyFrameFromCheckBox(frameMultiplier);
        }

        private void clearButtonClick(object sender, RoutedEventArgs e)
        {
            timeline.clearCheckBoxes(frameMultiplier);
        }

        public void save(string file)
        {
            editor.SendMessage((int)Message.MESSAGE_SAVE_FILE, file);
        }
        public void open(string file)
        {
            editor.SendMessage((int)Message.MESSAGE_OPEN_FILE, file);
            shapeEditorTools.updateSliderValues();
        }

        public void openOBJ(string file)
        {
            editor.SendMessage((int)Message.MESSAGE_OPEN_OBJ_FILE, file);
        }

        public void setTool(Tool tool)
        {
        }

        public void reloadShaders()
        {
            editor.SendMessage((int)Message.MESSAGE_RELOAD_SHADERS, 0);
        }

        public FrameSlider getShapeSlider()
        {
            return shapeSlider;
        }

        public Timeline getTimeline()
        {
            return timeline;
        }

        public int getFrameMultiplier()
        {
            return frameMultiplier;
        }

        public int getMaxFrame()
        {
            return maxFrame;
        }

        public bool getSliding()
        {
            return isSliding;
        }

        public bool getPlaying()
        {
            return isPlaying;
        }

        

        public void setFrameMultiplier(object sender, RoutedEventArgs e)
        {

            MouseWheelEventArgs ev = (MouseWheelEventArgs)e;

            int m = ev.Delta;


            if (frameMultiplier > 1 && frameMultiplier < 1000)
            {
                if (m > 0)
                    frameMultiplier *= 10;
                else
                    frameMultiplier /= 10;
            }

            else if (frameMultiplier >= 1000)
            {
                if (m < 0)
                    frameMultiplier /= 10;
            }

            else if (frameMultiplier <= 1)
            {
                if (m > 1)
                {
                    frameMultiplier *= 10;
                }
            }

            wnd.textBlock1.Content = "x" + frameMultiplier.ToString();

        }

        private ShapeEditorTools shapeEditorTools;

        public FrameSlider shapeSlider;

        private int frameMultiplier = 100;

        private bool isSliding = false;

        private bool isPlaying = false;

        private bool isAnimationForward = true;

        private int maxFrame = 0;

        public Timeline timeline;

        private MainWindow wnd;

    }
}
