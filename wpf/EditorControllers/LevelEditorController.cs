﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using interopScene;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Windows.Interop;


using Microsoft.Win32;

using wpf;

namespace wpf.EditorControllers
{
    class LevelEditorController : EditorController
    {

        enum RotateObjectX
        {
            INCR_PRESS,
            INCR_RELEASE,
            DECR_PRESS,
            DECR_RELEASE
        };

        enum RotateObjectY
        {
            INCR_PRESS,
            INCR_RELEASE,
            DECR_PRESS,
            DECR_RELEASE
        };

        enum RotateObjectZ
        {
            INCR_PRESS,
            INCR_RELEASE,
            DECR_PRESS,
            DECR_RELEASE
        };

        enum Scale
        {
            SCALE_UP_PRESS,
            SCALE_UP_RELEASE,
            SCALE_DOWN_PRESS,
            SCALE_DOWN_RELEASE
        };


        enum Movement
        {
            LEFT_PRESS,
            LEFT_RELEASE,
            RIGHT_PRESS,
            RIGHT_RELEASE,
            UP_PRESS,
            UP_RELEASE,
            DOWN_PRESS,
            DOWN_RELEASE
        };

        public LevelEditorController(Editor ed, MainWindow wnd)
            : base(ed)
        {
            levelEditorTools = new LevelEditorTools(ed, wnd);

            this.wnd = wnd;

            otherTabcontrolMargin = wnd.tabControl1.Margin;
            myTabcontrolMargin = new Thickness(otherTabcontrolMargin.Left, otherTabcontrolMargin.Top, 120, 10);

            layerPanel = new StackPanel();
            layerPanel.HorizontalAlignment = HorizontalAlignment.Left;
            layerPanel.VerticalAlignment = VerticalAlignment.Top;
            layerPanel.Width = 100;


            layerScroll = new ScrollViewer();
            layerScroll.UseLayoutRounding = true;
            layerScroll.Width = 120;
            layerScroll.Height = 650;
            layerScroll.CanContentScroll = true;
            layerScroll.HorizontalAlignment = HorizontalAlignment.Right;
            layerScroll.VerticalAlignment = VerticalAlignment.Top;
            layerScroll.HorizontalScrollBarVisibility = ScrollBarVisibility.Hidden;
            layerScroll.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            layerScroll.Margin = new Thickness(0, 55, 0, 0);
            layerScroll.Content = layerPanel;
            

            //addLayerButton = new Button();
            //addLayerButton.Content = "Add Layer";
            //addLayerButton.Width = 98;
            //addLayerButton.Margin = new Thickness(0, 0, 10, 10);
            //addLayerButton.VerticalAlignment = VerticalAlignment.Bottom;
            //addLayerButton.HorizontalAlignment = HorizontalAlignment.Right;
            //addLayerButton.Click += addLayer;

            wnd.rightGrid.Children.Add(layerScroll);
            //wnd.rightGrid.Children.Add(addLayerButton);
            
            ToggleButton tb1 = new ToggleButton();
            ToggleButton tb2 = new ToggleButton();


            tb1.Content = "Base Layer";
            tb1.IsChecked = true;

            tb2.Content = "Collision Boxes";
            tb2.IsChecked = false;

            LayerItem baseLayerItem = new LayerItem(tb1, this);
            LayerItem colBoxLayerItem = new LayerItem(tb2, this);

            //lock it so it cant be renamed
            baseLayerItem.setLocked(true);
            colBoxLayerItem.setLocked(true);

            layerPanel.Children.Add(baseLayerItem);
            layerPanel.Children.Add(colBoxLayerItem);
            
            
            //layerPanel.Children.Add(new LayerItem(toggle, textBox, this));
            //layerPanel.Children.Add(new LayerItem(toggle, textBox, this)); 

            toggleExclusive("Base Layer");
        }

        public void update()
        {
            //levelEditorTools.update();

        }

        private Microsoft.Win32.SaveFileDialog SaveDialog()
        {

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".flf";

            dlg.Filter = "Formless Level File (.flf)|*.flf";

            dlg.ShowDialog();

            return dlg;

        }

        private Microsoft.Win32.OpenFileDialog OpenDialog()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".flf";

            dlg.Filter = "Formless Level File (.flf)|*.flf";
            dlg.ShowDialog();

            return dlg;
        }

        public override void saveFile()
        {
            SaveFileDialog dlg = SaveDialog();
            if (dlg.FilterIndex == 1)
            {
                save(dlg.FileName);
            }
        }

        public override void openFile()
        {
            OpenFileDialog dlg = OpenDialog();
            if (dlg.FilterIndex == 1)
            {
                open(dlg.FileName);
            }
        }

        public override void newFile()
        {

        }

        public override void keyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {

            if (e.Key == Key.NumPad7)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_ROTATE_OBJECT_Y, (int)RotateObjectY.DECR_PRESS);
            }
            if (e.Key == Key.NumPad9)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_ROTATE_OBJECT_Y, (int)RotateObjectY.INCR_PRESS);
            }

            if (e.Key == Key.NumPad4)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_ROTATE_OBJECT_X, (int)RotateObjectX.DECR_PRESS);
            }
            if (e.Key == Key.NumPad6)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_ROTATE_OBJECT_X, (int)RotateObjectX.INCR_PRESS);
            }

            if (e.Key == Key.NumPad1)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_ROTATE_OBJECT_Z, (int)RotateObjectX.DECR_PRESS);
            }
            if (e.Key == Key.NumPad3)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_ROTATE_OBJECT_Z, (int)RotateObjectX.INCR_PRESS);
            }

            if (e.Key == Key.NumPad8)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_SCALE_OBJECT, (int)Scale.SCALE_UP_PRESS);
            }
            if (e.Key == Key.NumPad2)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_SCALE_OBJECT, (int)Scale.SCALE_DOWN_PRESS);
            }


            if (e.Key == Key.W)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_MOVE_CAMERA, (int)Movement.UP_PRESS);
            }
            if (e.Key == Key.A)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_MOVE_CAMERA, (int)Movement.LEFT_PRESS);
            }
            if (e.Key == Key.S)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_MOVE_CAMERA, (int)Movement.DOWN_PRESS);
            }
            if (e.Key == Key.D)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_MOVE_CAMERA, (int)Movement.RIGHT_PRESS);
            }

            if (!levelEditorTools.hotkeysLocked)
            {
                if (e.Key == Key.D1)
                {
                    levelEditorTools.setBrush(0);
                }
                if (e.Key == Key.D2)
                {
                    levelEditorTools.setBrush(1);
                }
                if (e.Key == Key.D3)
                {
                    levelEditorTools.setBrush(2);
                }
                if (e.Key == Key.D4)
                {
                    levelEditorTools.setBrush(3);
                }
                if (e.Key == Key.D5)
                {
                    levelEditorTools.setBrush(4);
                }
                if (e.Key == Key.D6)
                {
                    levelEditorTools.setBrush(5);
                }
                if (e.Key == Key.D7)
                {
                    levelEditorTools.setBrush(6);
                }
                if (e.Key == Key.D8)
                {
                    levelEditorTools.setBrush(7);
                }
                if (e.Key == Key.D9)
                {
                    levelEditorTools.setBrush(8);
                }
            }
            if (e.Key == Key.LeftShift)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_SHIFT_HELD, 1);
            }
        }

        public override void keyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_DELETE_OBJECT, 0); //sending zero cause it doesnt matter
            }
            if (e.Key == Key.NumPad7)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_ROTATE_OBJECT_Y, (int)RotateObjectY.DECR_RELEASE);
            }
            if (e.Key == Key.NumPad9)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_ROTATE_OBJECT_Y, (int)RotateObjectY.INCR_RELEASE);
            }

            if (e.Key == Key.NumPad4)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_ROTATE_OBJECT_X, (int)RotateObjectX.DECR_RELEASE);
            }
            if (e.Key == Key.NumPad6)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_ROTATE_OBJECT_X, (int)RotateObjectX.INCR_RELEASE);
            }

            if (e.Key == Key.NumPad1)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_ROTATE_OBJECT_Z, (int)RotateObjectX.DECR_RELEASE);
            }
            if (e.Key == Key.NumPad3)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_ROTATE_OBJECT_Z, (int)RotateObjectX.INCR_RELEASE);
            }

            if (e.Key == Key.NumPad8)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_SCALE_OBJECT, (int)Scale.SCALE_UP_RELEASE);
            }
            if (e.Key == Key.NumPad2)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_SCALE_OBJECT, (int)Scale.SCALE_DOWN_RELEASE);
            }


            if (e.Key == Key.W)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_MOVE_CAMERA, (int)Movement.UP_RELEASE);
            }
            if (e.Key == Key.A)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_MOVE_CAMERA, (int)Movement.LEFT_RELEASE);
            }
            if (e.Key == Key.S)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_MOVE_CAMERA, (int)Movement.DOWN_RELEASE);
            }
            if (e.Key == Key.D)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_MOVE_CAMERA, (int)Movement.RIGHT_RELEASE);
            }
            if (e.Key == Key.LeftShift)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_SHIFT_HELD, 0);
            }
        }

        public override bool hideEditor()
        {
            base.hideEditor();

            //resize tabcontrol & that jazz

            wnd.tabControl1.Margin = otherTabcontrolMargin;

            levelEditorTools.Visibility = Visibility.Hidden;

            layerScroll.Visibility = Visibility.Hidden;

            //addLayerButton.Visibility = Visibility.Hidden;

            return true;
        }

        public override bool showEditor()
        {
            base.showEditor();

            //resize tabcontrol & that jazz

            wnd.tabControl1.Margin = myTabcontrolMargin;

            levelEditorTools.Visibility = Visibility.Visible;
            layerScroll.Visibility = Visibility.Visible;
            //addLayerButton.Visibility = Visibility.Visible;
            
            

            return true;
        }


        public void showLayer(string name)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_SHOW_LAYER, name);
        }

        public void hideLayer(string name)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_HIDE_LAYER, name);
        }

        public void save(string file)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_SAVE_FILE, file);
        }
        public void open(string file)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_OPEN_FILE, file);
        }

        public void undo()
        {
        }

        public void redo()
        {
        }

        public void addLayer(object sender, RoutedEventArgs e)
        {
            ToggleButton b1 = new ToggleButton();


            LayerItem layer = new LayerItem(b1, this);
            layerPanel.Children.Add(layer);

            //editor.SendMessage(ADD_LAYER, 0);
        }

        public void toggleExclusive(string layerName)
        {

            foreach (LayerItem it in layerPanel.Children)
            {
                //if we found the FrameItem with the ToggleButton that was sent
                if (layerName.Equals(it.getLayerName()))
                {
                    it.setSelectionToggle(true);
                    editor.SendMessage((int)LevelMessage.MESSAGE_SET_WORK_LAYER, layerName);
                }
                else
                {
                    it.setSelectionToggle(false);
                }
            }
        }


        private LevelEditorTools levelEditorTools;

        private Thickness myTabcontrolMargin;
        private Thickness otherTabcontrolMargin;

        public ScrollViewer layerScroll;
        public StackPanel layerPanel;

        public MainWindow wnd;

    }
}
