﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

using interopScene;

namespace wpf.EditorControllers
{
    class EditorController
    {
        public EditorController(Editor ed)
        {
            editor = ed;
        }

        public virtual bool hideEditor()
        {
            visibility = false;
            return true;
        }

        public virtual bool showEditor()
        {
            visibility = true;
            return true;
        }

        public virtual void keyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
        }

        public virtual void keyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
        }

        public virtual void saveFile()
        {
        }

        public virtual void openFile()
        {
        }

        public virtual void newFile()
        {
        }
        
        public bool isVisible()
        {
            return visibility;
        }


        private bool visibility = false;

        protected Editor editor;
    }
}
