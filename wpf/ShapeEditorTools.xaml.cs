﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Windows.Interop;

using interopScene;


namespace wpf
{

    enum Tool
    {
        TOOL_PICKING,
        TOOL_SHIFT_PICKING,
        TOOL_LINK,
        TOOL_REMOVAL,
        TOOL_UNLINK,
        TOOL_BOXPICKING,
        TOOL_END
    };

    enum ButtonState
    {
        BUTTON_STATE_ON = 1,
        BUTTON_STATE_OFF = 0,
        BUTTON_STATE_END
    };

    enum Camera
    {
        CAMERA_MOVE_UP,
        CAMERA_MOVE_DOWN,
        CAMERA_MOVE_LEFT,
        CAMERA_MOVE_RIGHT,
        CAMERA_MOVE_FRONT,
        CAMERA_MOVE_BEHIND,
        CAMERA_MOVE_TO_GAME_SPECIFIC,
        CAMERA_MOVE_END
    };

    enum Message
    {
        MESSAGE_SWITCH_WORKSPACE,
        MESSAGE_REMOVE_WORKSPACE,
        MESSAGE_VISIBILITY,
        MESSAGE_PLAY_ANIMATION,
        MESSAGE_COPY_WORKSPACE,
        MESSAGE_OPEN_FILE,
        MESSAGE_SAVE_FILE,
        MESSAGE_OPEN_OBJ_FILE,
        MESSAGE_SET_FRAME,
        MESSAGE_DISABLE_FRAME,
        MESSAGE_SELECT_TOOL,
        MESSAGE_SELECT_ANIMATION,
        MESSAGE_SELECT_LINKING,
        MESSAGE_DOUBLE_LINK,
        MESSAGE_CLEAR_ALL,
        MESSAGE_MOVE_CAMERA,
        MESSAGE_UNDO,
        MESSAGE_REDO,
        MESSAGE_SET_GRAVITY,
        MESSAGE_SET_DEATH,
        MESSAGE_SET_SPEED,
        MESSAGE_RELOAD_SHADERS,
        MESSAGE_SCALE_OBJECTS,
        MESSAGE_ROTATE_OBJECTS_IN_YAXIS,
        MESSAGE_ADD_NODE,
        MESSAGE_REMOVE_NODE,
        MESSAGE_UNLINK_NODES,
        MESSAGE_NODES_VISIBILITY,
        MESSAGE_COPY_NODES,
        MESSAGE_ALIGN_MOUSE,
        MESSAGE_ROTATE_OBJECTS_IN_XAXIS,
	    MESSAGE_ROTATE_OBJECTS_IN_ZAXIS,
        MESSAGE_END
    };

    enum MouseAlignments
    {
        MOUSE_ALIGNMENT_NONE,
        MOUSE_ALIGNMENT_X,
        MOUSE_ALIGNMENT_Y,
        MOUSE_ALIGNMENT_END
    };

    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class ShapeEditorTools : Window
    {

        private Editor editor;

        private FrameSlider gravitySlider;

        private FrameSlider deathSlider;

        private FrameSlider speedSlider;
        
        private FrameSlider scaleSlider;

        private FrameSlider particleColorRSlider;

        private FrameSlider particleColorGSlider;

        private FrameSlider particleColorBSlider;

        private FrameSlider particleColorASlider;

        private FrameSlider rotateXSlider;
        private FrameSlider rotateYSlider;
        private FrameSlider rotateZSlider;

        private int oldScale = 0;

        private int oldRotatiton = 0;

        public ShapeEditorTools(Editor editor)
        {
            InitializeComponent();
            this.editor = editor;
            checkBox1.IsChecked = true;
            checkBox2.IsChecked = true;
            checkBox4.IsChecked = true;
            
            gravitySlider = new FrameSlider(220, 200, new Thickness(90,0,0,85));

            gravitySlider.slider.HorizontalAlignment = HorizontalAlignment.Right;
            gravitySlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            gravitySlider.slider.Orientation = Orientation.Vertical;


            deathSlider = new FrameSlider(190, 200, new Thickness(90, 0, 0, 85));

            deathSlider.slider.HorizontalAlignment = HorizontalAlignment.Right;
            deathSlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            deathSlider.slider.Orientation = Orientation.Vertical;

            speedSlider = new FrameSlider(160, 200, new Thickness(90, 0, 0, 85));

            speedSlider.slider.HorizontalAlignment = HorizontalAlignment.Right;
            speedSlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            speedSlider.slider.Orientation = Orientation.Vertical;

            scaleSlider = new FrameSlider(130, 200, new Thickness(90, 0, 0, 85));
            scaleSlider.slider.HorizontalAlignment = HorizontalAlignment.Right;
            scaleSlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            scaleSlider.slider.Orientation = Orientation.Vertical;

            particleColorASlider = new FrameSlider(10, 300, new Thickness(90, 0, 0, 85));
            particleColorASlider.slider.HorizontalAlignment = HorizontalAlignment.Center;
            particleColorASlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            particleColorASlider.slider.Orientation = Orientation.Horizontal;

            particleColorBSlider = new FrameSlider(10, 320, new Thickness(90, 0, 0, 85));
            particleColorBSlider.slider.HorizontalAlignment = HorizontalAlignment.Center;
            particleColorBSlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            particleColorBSlider.slider.Orientation = Orientation.Horizontal;

            particleColorGSlider = new FrameSlider(10, 340, new Thickness(90, 0, 0, 85));
            particleColorGSlider.slider.HorizontalAlignment = HorizontalAlignment.Center;
            particleColorGSlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            particleColorGSlider.slider.Orientation = Orientation.Horizontal;

            particleColorRSlider = new FrameSlider(10, 360, new Thickness(90, 0, 0, 85));
            particleColorRSlider.slider.HorizontalAlignment = HorizontalAlignment.Center;
            particleColorRSlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            particleColorRSlider.slider.Orientation = Orientation.Horizontal;

            rotateXSlider = new FrameSlider(80, 200, new Thickness(90, 0, 0, 85));
            rotateXSlider.slider.HorizontalAlignment = HorizontalAlignment.Right;
            rotateXSlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            rotateXSlider.slider.Orientation = Orientation.Vertical;

            rotateYSlider = new FrameSlider(60, 200, new Thickness(90, 0, 0, 85));
            rotateYSlider.slider.HorizontalAlignment = HorizontalAlignment.Right;
            rotateYSlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            rotateYSlider.slider.Orientation = Orientation.Vertical;

            rotateZSlider = new FrameSlider(40, 200, new Thickness(90, 0, 0, 85));
            rotateZSlider.slider.HorizontalAlignment = HorizontalAlignment.Right;
            rotateZSlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            rotateZSlider.slider.Orientation = Orientation.Vertical;

            baseGrid.Children.Add(gravitySlider.slider);
            baseGrid.Children.Add(deathSlider.slider);
            baseGrid.Children.Add(speedSlider.slider);
            baseGrid.Children.Add(scaleSlider.slider);

            baseGrid.Children.Add(particleColorRSlider.slider);
            baseGrid.Children.Add(particleColorGSlider.slider);
            baseGrid.Children.Add(particleColorBSlider.slider);
            baseGrid.Children.Add(particleColorASlider.slider);

            baseGrid.Children.Add(rotateXSlider.slider);
            baseGrid.Children.Add(rotateYSlider.slider);
            baseGrid.Children.Add(rotateZSlider.slider);
            //this.AddVisualChild(gravitySlider.slider);

            gravitySlider.setInterval(-100, 100);
            deathSlider.setInterval(0, 100);
            speedSlider.setInterval(0, 100);
            scaleSlider.setInterval(-10, 10);
            rotateXSlider.setInterval(-360, 360);
            rotateYSlider.setInterval(-360, 360);
            rotateZSlider.setInterval(-360, 360);

            particleColorRSlider.setInterval(0, 255);
            particleColorGSlider.setInterval(0, 255);
            particleColorBSlider.setInterval(0, 255);
            particleColorASlider.setInterval(0, 255);

            gravitySlider.slider.ValueChanged += setGravity;
            deathSlider.slider.ValueChanged += setDeath;
            speedSlider.slider.ValueChanged += setSpeed;

            scaleSlider.slider.ValueChanged += scaleObjects;
            scaleSlider.slider.MouseLeave += scaleDragLeave;
            scaleSlider.slider.MouseLeftButtonUp += scaleDragLeave;
            scaleSlider.slider.PreviewMouseLeftButtonUp += scaleDragLeave;

            rotateXSlider.slider.ValueChanged += rotateObjectsX;
            rotateXSlider.slider.MouseLeave += rotateXDragLeave;
            rotateXSlider.slider.MouseLeftButtonUp += rotateXDragLeave;
            rotateXSlider.slider.PreviewMouseLeftButtonUp += rotateXDragLeave;

            rotateYSlider.slider.ValueChanged += rotateObjectsY;
            rotateYSlider.slider.MouseLeave += rotateYDragLeave;
            rotateYSlider.slider.MouseLeftButtonUp += rotateYDragLeave;
            rotateYSlider.slider.PreviewMouseLeftButtonUp += rotateYDragLeave;

            rotateZSlider.slider.ValueChanged += rotateObjectsZ;
            rotateZSlider.slider.MouseLeave += rotateZDragLeave;
            rotateZSlider.slider.MouseLeftButtonUp += rotateZDragLeave;
            rotateZSlider.slider.PreviewMouseLeftButtonUp += rotateZDragLeave;
            
            particleColorRSlider.slider.ValueChanged += setParticleColor;
            particleColorGSlider.slider.ValueChanged += setParticleColor;
            particleColorBSlider.slider.ValueChanged += setParticleColor;
            particleColorASlider.slider.ValueChanged += setParticleColor;
            update();
}
        public void update()
        {
            String mess = editor.getNextFrameString();

            if (mess == "0")
            {
                listBox1.Items.Clear();

                listBox1.Items.Add("Frame: " + mess);
            }

            else if (mess != "")
            {
                listBox1.Items.Add("Frame: " + mess);
            }

            float r = editor.getParticleColor(0);
            float g = editor.getParticleColor(1);
            float b = editor.getParticleColor(2);
            float a = editor.getParticleColor(3);

            particleColorRSlider.slider.Value = r;
            particleColorGSlider.slider.Value = g;
            particleColorBSlider.slider.Value = b;
            particleColorASlider.slider.Value = a;


        }
        
        private void LoadPoints(object sender, RoutedEventArgs e)
        {
            //editor.SendMessage((int)Message.OPEN_OBJ_FILE, MainWindow.OpenDialog());
        }

        private void MoveChecked(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_SELECT_TOOL, (int)Tool.TOOL_PICKING);
        }

        private void LinkChecked(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_SELECT_TOOL, (int)Tool.TOOL_LINK);
        }

        private void AnimationChecked(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_SELECT_ANIMATION, (int)ButtonState.BUTTON_STATE_ON);
        }
        private void AnimationUnchecked(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_SELECT_ANIMATION, (int)ButtonState.BUTTON_STATE_OFF);
        }

        private void LinkingChecked(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_SELECT_LINKING, (int)ButtonState.BUTTON_STATE_ON);
        }

        private void LinkingUnchecked(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_SELECT_LINKING, (int)ButtonState.BUTTON_STATE_OFF);
        }

        private void DoubleLinkChecked(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_DOUBLE_LINK, (int)ButtonState.BUTTON_STATE_ON);
        }
        private void DoubleLinkUnChecked(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_DOUBLE_LINK, (int)ButtonState.BUTTON_STATE_OFF);
        }

        private void RemovalChecked(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_SELECT_TOOL, (int)Tool.TOOL_REMOVAL);
        }
    
        private void clearNodes(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_CLEAR_ALL, 0);
        }

        private void CameraButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;

            if ((String)button.Content == "Up")
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Camera.CAMERA_MOVE_UP);
            }
            else if ((String)button.Content == "Down")
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Camera.CAMERA_MOVE_DOWN);
            }
            else if ((String)button.Content == "Left")
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Camera.CAMERA_MOVE_LEFT);
            }
            else if ((String)button.Content == "Right")
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Camera.CAMERA_MOVE_RIGHT);
            }
            else if ((String)button.Content == "Front")
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Camera.CAMERA_MOVE_FRONT);
            }
            else if ((String)button.Content == "Behind")
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Camera.CAMERA_MOVE_BEHIND);
            }
            else if ((String)button.Content == "GameView")
            {
                editor.SendMessage((int)Message.MESSAGE_MOVE_CAMERA, (int)Camera.CAMERA_MOVE_TO_GAME_SPECIFIC);
            }
        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void radioButton4_Checked(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_SELECT_TOOL, (int)Tool.TOOL_UNLINK);
        }

        public void undo(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_UNDO, 0);
        }

        public void redo(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_REDO, 0);
        }

        public void setGravity(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_SET_GRAVITY, (int)gravitySlider.slider.Value);
        }

        public void setSpeed(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_SET_SPEED, (int)speedSlider.slider.Value);
        }

        public void setDeath(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_SET_DEATH, (int)deathSlider.slider.Value);
        }

        public void scaleObjects(object sender, RoutedEventArgs e)
        {
            if (oldScale > 0)
            {
                if (scaleSlider.slider.Value > oldScale)
                {
                    editor.SendMessage((int)Message.MESSAGE_SCALE_OBJECTS, (int)scaleSlider.slider.Value);
                    oldScale = (int)scaleSlider.slider.Value;
                }
            }
            else if (oldScale < 0)
            {
                if (scaleSlider.slider.Value < oldScale)
                {
                    editor.SendMessage((int)Message.MESSAGE_SCALE_OBJECTS, (int)scaleSlider.slider.Value);
                    oldScale = (int)scaleSlider.slider.Value;
                }
            }
            else if (oldScale == 0)
            {
                oldScale = (int)scaleSlider.slider.Value;
            }
           
        }

        public void scaleDragLeave(object sender, RoutedEventArgs e)
        {
            scaleSlider.slider.Value = 0;
            oldScale = 0;
        }


        public void rotateObjectsX(object sender, RoutedEventArgs e)
        {
            if (oldRotatiton > 0)
            {
                if (rotateXSlider.slider.Value > oldRotatiton)
                {
                    editor.SendMessage((int)Message.MESSAGE_ROTATE_OBJECTS_IN_XAXIS, (int)rotateXSlider.slider.Value);
                    oldRotatiton = (int)rotateXSlider.slider.Value;
                }
 
            }
            else if (oldRotatiton < 0)
            {
                if (rotateXSlider.slider.Value < oldRotatiton)
                {
                    editor.SendMessage((int)Message.MESSAGE_ROTATE_OBJECTS_IN_XAXIS, (int)rotateXSlider.slider.Value);
                    oldRotatiton = (int)rotateXSlider.slider.Value;
                }

            }
            else if (oldRotatiton == 0)
            { 
                oldRotatiton = (int)rotateXSlider.slider.Value;
            }
        }

        public void rotateObjectsY(object sender, RoutedEventArgs e)
        {
            if (oldRotatiton > 0)
            {
                if (rotateYSlider.slider.Value > oldRotatiton)
                {
                    editor.SendMessage((int)Message.MESSAGE_ROTATE_OBJECTS_IN_YAXIS, (int)rotateYSlider.slider.Value);
                    oldRotatiton = (int)rotateYSlider.slider.Value;
                }
            }
            else if (oldRotatiton < 0)
            {
                if (rotateYSlider.slider.Value < oldRotatiton)
                {
                    editor.SendMessage((int)Message.MESSAGE_ROTATE_OBJECTS_IN_YAXIS, (int)rotateYSlider.slider.Value);
                    oldRotatiton = (int)rotateYSlider.slider.Value;
                }
            }
            else if (oldRotatiton == 0)
            {
                oldRotatiton = (int)rotateYSlider.slider.Value;
            }
        }
        
        public void rotateObjectsZ(object sender, RoutedEventArgs e)
        {
            if (oldRotatiton > 0)
            {
                if (rotateZSlider.slider.Value > oldRotatiton)
                {
                    editor.SendMessage((int)Message.MESSAGE_ROTATE_OBJECTS_IN_ZAXIS, (int)rotateZSlider.slider.Value);
                    oldRotatiton = (int)rotateZSlider.slider.Value;
                }
            }
            else if (oldRotatiton < 0)
            {
                if (rotateZSlider.slider.Value < oldRotatiton)
                {
                    editor.SendMessage((int)Message.MESSAGE_ROTATE_OBJECTS_IN_ZAXIS, (int)rotateZSlider.slider.Value);
                    oldRotatiton = (int)rotateZSlider.slider.Value;
                }
            }
            else if (oldRotatiton == 0)
            {
                oldRotatiton = (int)rotateZSlider.slider.Value;
            }
        }
        public void rotateXDragLeave(object sender, RoutedEventArgs e)
        {
            rotateXSlider.slider.Value = 0;
            oldRotatiton = 0;
        }

        public void rotateYDragLeave(object sender,RoutedEventArgs e)
        {
            rotateYSlider.slider.Value = 0;
            oldRotatiton = 0;
        }

        public void rotateZDragLeave(object sender, RoutedEventArgs e)
        {
            rotateZSlider.slider.Value = 0;
            oldRotatiton = 0;
        }

        public void updateSliderValues()
        {
            gravitySlider.slider.Value = (int)editor.getGravity();
            speedSlider.slider.Value = (int)editor.getSpeed();
            deathSlider.slider.Value = (int)editor.getDeath();
        }

        private void addNodeClick(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_ADD_NODE, (int)0);
        }

        private void removeNodeClick(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_REMOVE_NODE, (int)0);
        }

        private void showNodesChecked(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_NODES_VISIBILITY, (int)1);
        }

        private void showNodesUnchecked(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_NODES_VISIBILITY, (int)0);
        }

        private void setParticleColor(object sender, RoutedEventArgs e)
        {
            editor.setParticleColor((float)particleColorRSlider.slider.Value, (float)particleColorGSlider.slider.Value, (float)particleColorBSlider.slider.Value, (float)particleColorASlider.slider.Value);
        }

        private void unlinkNodesClick(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_UNLINK_NODES, (int)0);
        }

        private void circleButtonClick(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_OPEN_OBJ_FILE, "../resources/models/cylinder.obj");
        }

        private void sphereButtonClick(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_OPEN_OBJ_FILE, "../resources/models/sphere.obj");
        }

        private void boxButtonClick(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)Message.MESSAGE_OPEN_OBJ_FILE, "../resources/models/box.obj");
        }
    }
}
