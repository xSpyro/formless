﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Microsoft.Win32;


using interopScene;

namespace wpf
{
    /// <summary>
    /// Interaction logic for ParticleEditorTools.xaml
    /// </summary>
    /// 
    enum Movement
    {
        MOVE_LEFT = 1,
        MOVE_RIGHT = 2,
        MOVE_UP = 3,
        MOVE_DOWN = 4
    };

    enum ParticleMessage
    {

        P_RAND_ANGLE_X = 200,
        P_RAND_ANGLE_Y,
        P_RAND_ANGLE_Z,

        P_DIR_INTENSITY,

        P_SPIN_MIN,
        P_SPIN_MAX,

        P_SPAWN_RATE,
        P_EMITT_LIFE,
        P_AFFECT_RATE,
        P_START_SCALE,
        P_END_SCALE,

        P_START_COLOR_R,
        P_START_COLOR_G,
        P_START_COLOR_B,

        P_MIDDLE_COLOR_R,
        P_MIDDLE_COLOR_G,
        P_MIDDLE_COLOR_B,

        P_END_COLOR_R,
        P_END_COLOR_G,
        P_END_COLOR_B,

        P_COLOR_PATH,
        P_SPEED_PATH,

        P_EMITTER_TYPE_BOX,
        P_EMITTER_TYPE_CIRCLE,

        P_START_SPEED,
        P_MIDDLE_SPEED,
        P_END_SPEED,

        P_GRAVITY,

        P_SELECT_ANIMATION,
        P_SELECT_BEHAVIOR,

        P_SELECT_TYPE,
        P_ADD_TYPE,
        P_ADD_TO_TYPE,
        P_ADD_EMITTER,
        P_NEW_EMITTER,
        P_ADD_ANIMATION,
        P_NEW_ANIMATION,
        P_LOOP_ANIMATION,
        P_PLAY_ANIMATION,

        P_DELETE_E,
        P_DELETE_A,

        P_PLAY_SELECTED_A,
        P_PLAY_SELECTED_E,

        P_PLAY_ALL,

        P_DISTORSION,

        P_DISPLACEMENT_POS,
        P_LIFE_AFFECT_RATE,
        P_EMITTER_LIFE_AFFECT_RATE,
        P_MESSAGE_OPEN_TEXTURE,

        P_SET_PARTICLE_SPAWN_MAX, 
    };
    public partial class ParticleEditorTools : Window
    {
        Editor editor;
        int selected;
        int counter;
        int selected_typeID;
        List<int> animations;
        List<string> type_names;
        //Boolean nullAnimation;
        float PI = 3.14159265f;
        
        public ParticleEditorTools(Editor editor)
        {
            float radx = PI / 180.0f;
            selected_typeID = 0;
            //nullAnimation = true;
            InitializeComponent();
            selected = 0;

            counter = 0;
            String name = "Animation: " + counter.ToString();
            animations = new List<int>();
            type_names = new List<string>();

            type_names.Add("basic");
            type_names.Add("heat");

            Types.Items.Add("basic");
            Types.Items.Add("heat");

            animations.Add(1);

            listBox1.Items.Add("Animation: " + counter.ToString());
            listBox2.Items.Add("Emitter: " + counter.ToString());
          
            startColorR.Maximum = 1.0f;
            startColorG.Maximum = 1.0f;
            startColorB.Maximum = 1.0f;

            middleColorR.Maximum = 1.0f;
            middleColorG.Maximum = 1.0f;
            middleColorB.Maximum = 1.0f;

            endColorR.Maximum = 1.0f;
            endColorG.Maximum = 1.0f;
            endColorB.Maximum = 1.0f;

            colorPath.Minimum = 0.0f;
            colorPath.Maximum = 1.0f;

            speedParticlePath.Minimum = 0.0f;
            speedParticlePath.Maximum = 1.0f;

            endScaleSlider.Maximum = 40.0f;
            endScaleSlider.Minimum = 0.0f;

            setStartScaleSlider.Maximum = 40.0f;
            setStartScaleSlider.Minimum = 0.0f;

            randAngleX.Maximum = 360.0f;
            randAngleY.Maximum = 360.0f;
            randAngleZ.Maximum = 360.0f;

            randAngleX.Minimum = 0.0f;
            randAngleY.Minimum = 0.0f;
            randAngleZ.Minimum = 0.0f;

            intensityDir.Maximum = 360.0f;
            intensityDir.Minimum = 0.0f;

            emSpawnRateSlider.Maximum = 50;

           // setStartScaleSlider.Minimum = -100;
            //setStartScaleSlider.Maximum = 1.0f;
            startParticleSpeed.Minimum = -15.0f;
            startParticleSpeed.Maximum = 15.0f;

            middleParticleSpeed.Minimum = -15.0f;
            middleParticleSpeed.Maximum = 15.0f;

            endParticleSpeed.Minimum = -15.0f;
            endParticleSpeed.Maximum = 15.0f;

            gravity.Minimum = -2.0f;
            gravity.Maximum = 2.0f;

            distorsionSlider.Minimum = 0.0f;
            distorsionSlider.Maximum = 1.0f;

            displacementPosSlider.Maximum = 360;
            emitterAffectRateSlider.Minimum = 0.0f;
            emitterAffectRateSlider.Maximum = 2.0f;

            particleAffectRateSlider.Minimum = 0.0f;
            particleAffectRateSlider.Maximum = 0.05f;

            setStartScaleSlider.AutoToolTipPlacement = System.Windows.Controls.Primitives.AutoToolTipPlacement.BottomRight;

            maxSpawnParticlesSlider.Minimum = 1;
            maxSpawnParticlesSlider.Maximum = 1000;

            this.editor = editor;

            setLife.Minimum = 1;
            setLife.Maximum = 600;

            spinMinValue.Minimum = -1.0;
            spinMinValue.Maximum = 360*radx;
            spinMaxValue.Minimum = -1.0;
            spinMaxValue.Maximum = 360*radx;
            listBox1.SelectedIndex = 0;
            listBox2.SelectedIndex = 0;


            //randAngleZ.Value = 0;

            /*startColorB.Value = 1;
            startColorR.Value = 1;
            startColorG.Value = 1;

            middleColorR.Value = 1;
            endColorG.Value    = 1;
            colorPath.Value = 0.5;
            emSpawnRateSlider.Value = 5;
            speedParticlePath.Value = 0.5;
            randAngleY.Value = 90;

            setStartScaleSlider.Value = 1;
            endScaleSlider.Value = 2;

            startParticleSpeed.Value = 5;
            intensityDir.Value = 0;

            displacementPosSlider.Value = 0;
            particleAffectRateSlider.Value = 0.018;*/

           // editor.SendMessage((int)ParticleMessage.SET_START_SCALE, hej);
           // setLife.Value = 500;
           // startParticleSpeed.Value = 5.0f;

            emitterType.Items.Add("Basic");
            emitterType.Items.Add("Heat");
            emitterType.SelectedIndex = 0;
        }

        private void emittSpawnRate(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_SPAWN_RATE, (float)emSpawnRateSlider.Value);
            dirTextBox.Text = emSpawnRateSlider.Value.ToString();
        }

        private void setStatScale(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

            editor.SendMessage((int)ParticleMessage.P_START_SCALE, (float)setStartScaleSlider.Value);
            dirTextBox.Text = setStartScaleSlider.Value.ToString();
        }

        private void endScale(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_END_SCALE, (float)endScaleSlider.Value);
            dirTextBox.Text = endScaleSlider.Value.ToString();
        }



        private void randAngleXSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_RAND_ANGLE_X, (float)(randAngleX.Value));
            dirTextBox.Text = randAngleX.Value.ToString();
        }

        private void randAngleZSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_RAND_ANGLE_Z, (float)(randAngleZ.Value));
            dirTextBox.Text = randAngleZ.Value.ToString();
        }
        private void dirIntensity(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_DIR_INTENSITY, (float)(intensityDir.Value));
            dirTextBox.Text = intensityDir.Value.ToString();
        }


        private void spinMinValueSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_SPIN_MIN, (float)(spinMinValue.Value));
            dirTextBox.Text = spinMinValue.Value.ToString();
        }

        private void spinMaxValueSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_SPIN_MAX, (float)(spinMaxValue.Value));
            dirTextBox.Text = spinMaxValue.Value.ToString();
        }

        private void startColorRSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_START_COLOR_R, (float)(startColorR.Value));
            dirTextBox.Text = startColorR.Value.ToString();
        }

        private void startColorGSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_START_COLOR_G, (float)(startColorG.Value));
            dirTextBox.Text = startColorG.Value.ToString();
        }

        private void startColorBSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_START_COLOR_B, (float)(startColorB.Value));
            dirTextBox.Text = startColorB.Value.ToString();
        }

        private void middleColorRSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_MIDDLE_COLOR_R, (float)(middleColorR.Value));
            dirTextBox.Text = middleColorR.Value.ToString();
        }

        private void middleColorGSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_MIDDLE_COLOR_G, (float)(middleColorG.Value));
            dirTextBox.Text = middleColorG.Value.ToString();
        }

        private void middleColorBSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_MIDDLE_COLOR_B, (float)(middleColorB.Value));
            dirTextBox.Text = middleColorB.Value.ToString();
        }

        private void endColorRSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_END_COLOR_R, (float)(endColorR.Value));
            dirTextBox.Text = endColorR.Value.ToString();
        }

        private void endColorGSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_END_COLOR_G, (float)(endColorG.Value));
            dirTextBox.Text = endColorG.Value.ToString();
        }

        private void endColorBSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_END_COLOR_B, (float)(endColorB.Value));
            dirTextBox.Text = endColorB.Value.ToString();
        }

        private void setLifeSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_EMITT_LIFE, (int)(setLife.Value));
            dirTextBox.Text = setLife.Value.ToString();
        }

        private void startParticleSpeedSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_START_SPEED, (float)(startParticleSpeed.Value));
            dirTextBox.Text = startParticleSpeed.Value.ToString();
        }
        private void middleParticleSpeedSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_MIDDLE_SPEED, (float)(middleParticleSpeed.Value));
            dirTextBox.Text = middleParticleSpeed.Value.ToString();
        }
        private void endParticleSpeedSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_END_SPEED, (float)(endParticleSpeed.Value));
            dirTextBox.Text = endParticleSpeed.Value.ToString();
        }

        private void speedParticlePathSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_SPEED_PATH, (float)(speedParticlePath.Value));
            dirTextBox.Text = speedParticlePath.Value.ToString();
        }

        private void randAngleYSlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_RAND_ANGLE_Y, (float)(randAngleY.Value));
            dirTextBox.Text = randAngleY.Value.ToString();
        }
        private void ColorPathChange(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_COLOR_PATH, (float)(colorPath.Value));
            dirTextBox.Text = colorPath.Value.ToString();
        }
        private void GravitySlider(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_GRAVITY, (float)(gravity.Value));
            dirTextBox.Text = gravity.Value.ToString();
        }
        private void dirTextBoxFunc(object sender, TextChangedEventArgs e)
        {
            editor.SendMessage((int)ParticleMessage.P_PLAY_ANIMATION, (int)ParticleMessage.P_PLAY_ANIMATION);
        }


        private void NewAnimation(object sender, RoutedEventArgs e)
        {
            //editor.SendMessage((int)ParticleMessage.P_NEW_ANIMATION, (int)ParticleMessage.P_NEW_ANIMATION);
        }

        private void NewEmitter(object sender, RoutedEventArgs e)
        {
            //editor.SendMessage((int)ParticleMessage.P_NEW_EMITTER, (int)ParticleMessage.P_NEW_EMITTER);
            //listBox1.Items.Clear();
        }
        private void ShowBehaviors(int id)
        {
            listBox2.Items.Clear();
            for (int i = 0; i < editor.getNrEmitters(); i++)
            {
                listBox2.Items.Add("Behavior: " + i.ToString());
            }
        }
        private void AddEmitter(object sender, RoutedEventArgs e)
        {

            //listBox2.Items.Add("Emitter");
            if (listBox1.SelectedIndex < counter + 1 && listBox1.SelectedIndex >= 0)
            {
                editor.SendMessage((int)ParticleMessage.P_ADD_EMITTER, (int)ParticleMessage.P_ADD_EMITTER);
                //nullAnimation = false;
                animations[listBox1.SelectedIndex]++;
                ShowBehaviors(listBox1.SelectedIndex);
            }
        }
        private void AddAnimation(object sender, RoutedEventArgs e)
        {
            //if (!nullAnimation)
            //{
                // editor.SendMessage((int)ParticleMessage.ADD_EMITTER, 1);
                editor.SendMessage((int)ParticleMessage.P_ADD_ANIMATION, (int)ParticleMessage.P_ADD_ANIMATION);
                //listBox1.Items.Add("Animation: " + counter.ToString());
                animations.Add(0);
                counter++;
                listBox1.Items.Clear();

                UpdateAnimations();
                //for (int i = 0; i < counter; i++)
                //{
                //   // listBox1.Items.Add("Animations: " + animations[i].ToString());
                //}
                //nullAnimation = true;
            //}
        }
        private void PlayAnimation(object sender, RoutedEventArgs e)
        {
            UpdateValues();
            editor.SendMessage((int)ParticleMessage.P_PLAY_ANIMATION, (int)ParticleMessage.P_PLAY_ANIMATION);
        }

        private void LoopAnimation(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)ParticleMessage.P_LOOP_ANIMATION, (int)ParticleMessage.P_LOOP_ANIMATION);
        }

        private void PlayAllAnimations(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)ParticleMessage.P_PLAY_ALL, (int)ParticleMessage.P_PLAY_ALL);
        }

        public void UpdateAnimations()
        {
            listBox1.Items.Clear();
            for (int i = 0; i < editor.getNrAnimations(); i++)
            {
                listBox1.Items.Add("Animation: " + i.ToString());
                //for (int j = 0; j < editor.getNrEmitters(); j++)
                //{
                //    listBox2.Items.Add("Emitter: " + j.ToString());
                //}
            }
            listBox1.SelectedIndex = editor.getNrAnimations() - 1;
        }
        public void UpdateEmitters()
        {
            listBox2.Items.Clear();
            if (listBox1.SelectedIndex > -1)
            {
                for (int i = 0; i < editor.getNrEmitters(); i++)
                {
                    listBox2.Items.Add("Emitter: " + i.ToString());
                }
            }
        }

        public void UpdateValues()
        {
            //if (listBox1.SelectedIndex > -1 && listBox2.SelectedIndex > -1)
            //{
                randAngleX.Value               = editor.getRandAngleX();
                randAngleY.Value               = editor.getRandAngleY();
                randAngleZ.Value               = editor.getRandAngleZ();
                setLife.Value                  = editor.getLife();
                intensityDir.Value             = editor.getIntensityDir();
                spinMinValue.Value             = editor.spinMin();
                spinMaxValue.Value             = editor.spinMax();
                emSpawnRateSlider.Value        = editor.getSpawnRate();
                emitterAffectRateSlider.Value  = editor.getLifeAffectRate();
                particleAffectRateSlider.Value = editor.getAffectRate();
                setStartScaleSlider.Value      = editor.getStartScale();
                endScaleSlider.Value           = editor.getEndScale();
                startParticleSpeed.Value       = editor.getStartSpeed();
                middleParticleSpeed.Value      = editor.getMiddleSpeed();
                endParticleSpeed.Value         = editor.getEndSpeed();
                colorPath.Value                = editor.getColorPath();
                speedParticlePath.Value        = editor.getSpeedPath();
                gravity.Value                  = editor.particleGetGravity();
                distorsionSlider.Value         = editor.getDistorsion();
                displacementPosSlider.Value    = editor.getDisplacementPos();
                startColorR.Value              = editor.getStartColorR();
                startColorG.Value              = editor.getStartColorG();
                startColorB.Value              = editor.getStartColorB();
                middleColorR.Value             = editor.getMiddleColorR();
                middleColorG.Value             = editor.getMiddleColorG();
                middleColorB.Value             = editor.getMiddleColorB();
                endColorR.Value                = editor.getEndColorR();
                endColorG.Value                = editor.getEndColorG();
                endColorB.Value                = editor.getEndColorB();
            //}

            /*
             * diffusion
             */
        }

        private void AnimationList(object sender, SelectionChangedEventArgs e)
        {

        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selected = -1;

            selected = listBox1.SelectedIndex;
            if (selected > -1 && selected < editor.getNrAnimations())
            {
                editor.SendMessage((int)ParticleMessage.P_SELECT_ANIMATION, (int)listBox1.SelectedIndex);
                dirTextBox.Text = listBox1.SelectedIndex.ToString();


                listBox2.Items.Clear();
                for (int i = 0; i < editor.getNrEmitters(); i++)
                {
                    listBox2.Items.Add("Emitter: " + i.ToString());
                }
            }
            else
                editor.SendMessage((int)ParticleMessage.P_SELECT_ANIMATION, -1);

           // counter = (int)listBox1.SelectedIndex;
            //editor.SendMessage((int)ParticleMessage.P_SELECT_ANIMATION, (int)listBox1.SelectedIndex);
            
            //listBox2.Items.Clear();
            //if (listBox1.SelectedIndex < counter + 1 && listBox1.SelectedIndex >= 0)
            //{
            //    for (int x = 0; x < animations[listBox1.SelectedIndex]; x++)
            //    {
            //        listBox2.Items.Add("Behavior: " + x.ToString());
            //    }
            //}
        }

        private void listBox2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (selected != -1)
            //{
                editor.SendMessage((int)ParticleMessage.P_SELECT_BEHAVIOR, (int)listBox2.SelectedIndex);
                dirTextBox.Text = listBox2.SelectedIndex.ToString();
                if( listBox2.SelectedIndex > -1 && listBox2.SelectedIndex < editor.getNrEmitters() )
                    UpdateValues();
            //}
        }

        private void deleteEmitterFunction(object sender, RoutedEventArgs e)
        {
            if (listBox1.SelectedIndex > -1)
            {
                editor.SendMessage((int)ParticleMessage.P_DELETE_E, (int)ParticleMessage.P_DELETE_E);
                listBox2.Items.Clear();
                for (int i = 0; i < editor.getNrEmitters(); i++)
                {
                    listBox2.Items.Add("Emitter: " + i.ToString());
                }
            }
        }

        private void deleteAnimationFunction(object sender, RoutedEventArgs e)
        {
            if ( listBox1.SelectedIndex > -1 )
            {
                //dirTextBox.Text = "Deleting: index: " + selected.ToString();
                animations.Remove(listBox1.SelectedIndex);
                editor.SendMessage((int)ParticleMessage.P_DELETE_A, (int)ParticleMessage.P_DELETE_A);
                counter--;
                listBox1.Items.Clear();

                for (int i = 0; i < editor.getNrAnimations(); i++)
                {
                    listBox1.Items.Add("Animations: " + i.ToString());
                }
            }
            else
                dirTextBox.Text = "Failed deleting" + listBox1.SelectedIndex.ToString();
        }

        private void playSelectedEmitterFunction(object sender, RoutedEventArgs e)
        {
            if (listBox2.SelectedIndex > -1)
            {
                editor.SendMessage((int)ParticleMessage.P_PLAY_SELECTED_E, (int)ParticleMessage.P_PLAY_SELECTED_E);
                UpdateValues();
            }
        }

        private void playSelectedAnimationFunction(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)ParticleMessage.P_PLAY_SELECTED_A, (int)ParticleMessage.P_PLAY_SELECTED_A);
        }

        private void distorsionSliderFunction(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_DISTORSION, (float)distorsionSlider.Value);
            dirTextBox.Text = distorsionSlider.Value.ToString();
        }

        private void displacementSliderChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            editor.SendMessage((int)ParticleMessage.P_DISPLACEMENT_POS, (float)displacementPosSlider.Value);
            dirTextBox.Text = displacementPosSlider.Value.ToString();
        }

        private void particleAffectRate(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (particleAffectRateSlider.Value == 0)
            {
                editor.SendMessage((int)ParticleMessage.P_LIFE_AFFECT_RATE, 0.001f);
                float toDouble = (float)particleAffectRateSlider.Value;
                dirTextBox.Text = "Cannot ser zero";
            }
            else
            {
                editor.SendMessage((int)ParticleMessage.P_LIFE_AFFECT_RATE, (float)particleAffectRateSlider.Value);
                float toDouble = (float)particleAffectRateSlider.Value;
                dirTextBox.Text = toDouble.ToString();
            }
        }

        private void emitterAffectRate(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (emitterAffectRateSlider.Value == 0)
            {
                editor.SendMessage((int)ParticleMessage.P_EMITTER_LIFE_AFFECT_RATE, 0.001f);
                dirTextBox.Text = "Cannot set zero";
            }
            else
            {
                editor.SendMessage((int)ParticleMessage.P_EMITTER_LIFE_AFFECT_RATE, (float)emitterAffectRateSlider.Value);
                float toDouble = (float)(emitterAffectRateSlider.Value);
                dirTextBox.Text = toDouble.ToString();
            }

        }
        private Microsoft.Win32.OpenFileDialog OpenDialogTexture()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".png";

            dlg.Filter = "effects (.jpg, png, dds)|*.*;";
            dlg.ShowDialog();

            return dlg;
        }
        public void openTexture(string file)
        {

        }
        private void addPraticleTexture(object sender, RoutedEventArgs e)
        {
            //OpenFileDialog dlg = OpenDialogTexture();
            //if (dlg.FilterIndex == 1)
            //{
            //    editor.SendMessage((int)ParticleMessage.P_MESSAGE_OPEN_TEXTURE, dlg.SafeFileName);
            //    Types.Items.Add(dlg.SafeFileName);
            //}
        }

        private void maxSpawnParticles(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void emitterTypeChanged(object sender, SelectionChangedEventArgs e)
        {
            //editor.SendMessage((int)ParticleMessage.P_SELECT_TYPE, (int)emitterType.SelectedIndex);
            //dirTextBox.Text = emitterType.SelectedIndex.ToString();

            if (emitterType.SelectedIndex == 1)
            {
               /* startColorR.Value = 0.01f;
                startColorG.Value = 0.01f;
                startColorB.Value = 0.01f;

                middleColorR.Value = 0.01f;
                middleColorG.Value = 0.01f;
                middleColorB.Value = 0.01f;

                endColorR.Value = 0.01f;
                endColorG.Value = 0.01f;
                endColorB.Value = 0.01f;*/
            }
        }

        private void AddType_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = OpenDialogTexture();
            
            if (dlg.FilterIndex == 1)
            {
                if (dlg.SafeFileName != "")
                {
                    Types.Items.Add(dlg.SafeFileName);
                    //emitterType.Items.Add(dlg.SafeFileName);
                    editor.SendMessage((int)ParticleMessage.P_ADD_TYPE, dlg.SafeFileName);
                    //dlg.F
                }
            }
        }

        private void setType(object sender, RoutedEventArgs e)
        {
            if (selected_typeID > -1)
            {
                editor.SendMessage((int)ParticleMessage.P_SELECT_TYPE, (int)selected_typeID);
                dirTextBox.Text = selected_typeID.ToString();
            }
        }

        private void Types_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Types.SelectedIndex > -1)
            {
                selected_typeID = Types.SelectedIndex;
            }
        }


        //public void update()
        //{

        //    String mess = editor.getNextFrameString();

        //    if (mess == "0")
        //    {
        //        listBox1.Items.Clear();

        //        listBox1.Items.Add("If 0: " + mess);
        //    }

        //    else if (mess != "")
        //    {
        //        listBox1.Items.Add("If != empty: " + mess);
        //    }

        //}

        //private void KeyDownEventHandler(object sender, KeyEventArgs e)
        //{

        //    if (e.Key == Key.W)
        //    {
        //        editor.SendMessage(0, (int)Movement.MOVE_UP);
        //    }
        //    if (e.Key == Key.A)
        //    {
        //        editor.SendMessage(0, (int)Movement.MOVE_LEFT);
        //    }
        //    if (e.Key == Key.S)
        //    {
        //        editor.SendMessage(0, (int)Movement.MOVE_DOWN);
        //    }
        //    if (e.Key == Key.D)
        //    {
        //        editor.SendMessage(0, (int)Movement.MOVE_RIGHT);
        //    }

        //}







        





        //private void PNewAnimation(object sender, RoutedEventArgs e)
        //{

        //}

        //private void PAddEmitter(object sender, RoutedEventArgs e)
        //{

        //}

      //  private void slidingPosX(object sender, RoutedEventArgs e)
     //   {
            //change value of the textbox associated with the slider...
            //posXTextBox.Text = posXSlider.Value.ToString();
     //   }

     //   private void writingPosX(object sender, RoutedEventArgs e)
       // {
            //change value of the slider
            //int temp = 0;

            ////HANDLE IF TEXT IS NOT A NUMBER
            //if(Int32.TryParse(posXTextBox.Text, out temp) )
            //{
            //    posXSlider.Value = temp;
            //}
     //   }



        //private void posXTextBox_TextChanged(object sender, TextChangedEventArgs e)
        //{

        //}

        //private void posYSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{

        //}

        //private void posZSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        //{

        //}


    }
}
