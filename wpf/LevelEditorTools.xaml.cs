﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.IO;

using Microsoft.Win32;

using interopScene;


namespace wpf
{
    /// <summary>
    /// Interaction logic for LevelEditorTools.xaml
    /// </summary>  
    /// 
    
    enum LevelMessage
    {
        MESSAGE_MOVE_CAMERA,
        MESSAGE_ROTATE_OBJECT_X,
        MESSAGE_ROTATE_OBJECT_Y,
        MESSAGE_ROTATE_OBJECT_Z,
        MESSAGE_SCALE_OBJECT,
        MESSAGE_PUT_OBJECT,
        MESSAGE_DELETE_OBJECT,
        MESSAGE_BRUSH,
        MESSAGE_OPEN_FILE,
        MESSAGE_SAVE_FILE,
        MESSAGE_NEW_FILE,
        MESSAGE_BRUSH_INTENSITY_TERRAIN,
        MESSAGE_BRUSH_INTENSITY_SMOOTH,
        MESSAGE_BRUSH_SIZE,
        MESSAGE_ADD_LAYER,
        MESSAGE_SHOW_LAYER,
        MESSAGE_HIDE_LAYER,
        MESSAGE_SET_WORK_LAYER,
        MESSAGE_SAVE_LUA,
        MESSAGE_RENAME_LUA,
        MESSAGE_TOGGLE_EFFECT,
        MESSAGE_SNAP_TO_TERRAIN,
        MESSAGE_LEVEL_VALUE,
        MESSAGE_SHIFT_HELD
    };


    enum TerrainSnapModes
    {
        SNAP_OFF,
        SNAP_ON
    };

    public partial class LevelEditorTools : Window
    {
        Editor editor;
        Window parent;
      
        FrameSlider terrainIntensitySlider;
        FrameSlider smoothIntensitySlider;
        FrameSlider brushScaleSlider;

        public bool hotkeysLocked;


        public LevelEditorTools(Editor editor, Window father)
        {
            InitializeComponent();

            hotkeysLocked = false;

            terrainIntensitySlider = new FrameSlider(100, 20, new Thickness(20, 0, 0, 10) );
            terrainIntensitySlider.slider.HorizontalAlignment = HorizontalAlignment.Left;
            terrainIntensitySlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            terrainIntensitySlider.setInterval(0, 100);

            smoothIntensitySlider = new FrameSlider(100, 20, new Thickness(20, 0, 0, 40));
            smoothIntensitySlider.slider.HorizontalAlignment = HorizontalAlignment.Left;
            smoothIntensitySlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            smoothIntensitySlider.setInterval(0, 100);

            brushScaleSlider = new FrameSlider(100, 20, new Thickness(20, 0, 0, 70));
            brushScaleSlider.slider.HorizontalAlignment = HorizontalAlignment.Left;
            brushScaleSlider.slider.VerticalAlignment = VerticalAlignment.Bottom;
            brushScaleSlider.setInterval(0, 100);

            brushScaleSlider.slider.ValueChanged += setBrushScale;
            terrainIntensitySlider.slider.ValueChanged += setIntensityTerrain;
            smoothIntensitySlider.slider.ValueChanged += setIntensitySmooth;

            baseGrid.Children.Add(terrainIntensitySlider.slider);
            baseGrid.Children.Add(smoothIntensitySlider.slider);
            baseGrid.Children.Add(brushScaleSlider.slider);

            parent = father;

            this.editor = editor;

            string path = "..\\resources\\objects\\Scenery";

            try
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                

                foreach (FileInfo flInfo in dir.GetFiles())
                {
                    String name = flInfo.Name;

                    long size = flInfo.Length;
                    if (flInfo.Extension.Equals(".lua"))
                    {
                        Button tempBtn = new Button();
                        tempBtn.Height = 30;
                        tempBtn.Width = 140;

                        //removing the .lua extension, hack yeah.
                        name = name.Remove(name.Length - 4);

                        tempBtn.Content = name;

                        tempBtn.Click += putObject;


                        rightGrid.Children.Add(tempBtn);
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }


            fogButton.IsChecked = true;
            ssaoButton.IsChecked = true;
            shadowButton.IsChecked = true;
            terrainSnapBox.IsChecked = true;

            Loaded += LevelEditorToolsLoaded;

            brushScaleSlider.slideTo(20);
            smoothIntensitySlider.slideTo(20);
            terrainIntensitySlider.slideTo(20);


        }

        private void LevelEditorToolsLoaded(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_BRUSH_INTENSITY_TERRAIN, (int)terrainIntensitySlider.slider.Value);
            editor.SendMessage((int)LevelMessage.MESSAGE_BRUSH_INTENSITY_SMOOTH, (int)smoothIntensitySlider.slider.Value);
            editor.SendMessage((int)LevelMessage.MESSAGE_BRUSH_SIZE, (int)brushScaleSlider.slider.Value);

        }


        private void ToolsKeyDown(object sender,System.Windows.Input.KeyEventArgs e)
        {
            if (!hotkeysLocked)
            {
                if (e.Key == Key.D1)
                {
                    setBrush(0);
                }
                if (e.Key == Key.D2)
                {
                    setBrush(1);
                }
                if (e.Key == Key.D3)
                {
                    setBrush(2);
                }
                if (e.Key == Key.D4)
                {
                    setBrush(3);
                }
                if (e.Key == Key.D5)
                {
                    setBrush(4);
                }
                if (e.Key == Key.D6)
                {
                    setBrush(5);
                }
                if (e.Key == Key.D7)
                {
                    setBrush(6);
                }
                if (e.Key == Key.D8)
                {
                    setBrush(7);
                }
                if (e.Key == Key.D9)
                {
                    setBrush(8);
                }
            }
        }


        private void ToolsKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {

            if (e.Key == Key.S && Keyboard.Modifiers == ModifierKeys.Control)
            {
                saveFile();

            }
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                openFile();
            }
            if (e.Key == Key.LeftShift)
            {
                editor.SendMessage((int)LevelMessage.MESSAGE_SHIFT_HELD, 0);
            }
            
        }


        private Microsoft.Win32.SaveFileDialog SaveDialog()
        {

            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.DefaultExt = ".flf";

            dlg.Filter = "Formless Level File (.flf)|*.flf";

            dlg.ShowDialog();

            return dlg;

        }

        private Microsoft.Win32.OpenFileDialog OpenDialog()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".flf";

            dlg.Filter = "Formless Level File (.flf)|*.flf";
            dlg.ShowDialog();

            return dlg;
        }

        public void saveFile()
        {
            SaveFileDialog dlg = SaveDialog();
            if (dlg.FilterIndex == 1)
            {
                save(dlg.FileName);
            }
        }

        public void openFile()
        {
            OpenFileDialog dlg = OpenDialog();
            if (dlg.FilterIndex == 1)
            {
                open(dlg.FileName);
            }
        }
        public void save(string file)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_SAVE_FILE, file);
        }
        public void open(string file)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_OPEN_FILE, file);
        }

        private void addTrigger(object sender, RoutedEventArgs e)
        {
            ToggleButton toggleOne = new ToggleButton();
            ToggleButton toggleTwo = new ToggleButton();
            TextBox textBox = new TextBox();

            TriggerItem titem = new TriggerItem(toggleOne, toggleTwo, textBox, this);

            editor.SendMessage( (int)LevelMessage.MESSAGE_PUT_OBJECT, "Box");
            triggerPanel.Children.Add(titem);

        }


        private void loadedTextBox(object sender, RoutedEventArgs e)
        {
            ((TextBox)sender).Focus();
        }


        private void summonScriptBox(object sender, RoutedEventArgs e)
        {
            //scriptBox.Visibility = Visibility.Visible;
        }

        private void hideScriptBox(object sender, RoutedEventArgs e)
        {
            //scriptBox.Visibility = Visibility.Hidden;
        }


        /****
         * Loops through every ToggleButton in triggerPanel
         * to make exclusive toggling!
         * */
        private void toggleExclusive(ToggleButton tbtn)
        {
            foreach (TriggerItem it in triggerPanel.Children)
            {
                TriggerContent content = (TriggerContent)it.Child;
                ToggleButton temp = (ToggleButton)content.Children[1];

                if (temp.Equals(tbtn))
                {
                    temp.IsChecked = true;
                }
                else
                {
                    temp.IsChecked = false;
                }
            }
        }

        //ugly
        private TriggerItem getTriggerItem(Button tbtn)
        {

            foreach (TriggerItem it in triggerPanel.Children)
            {
                TriggerContent content = (TriggerContent)it.Child;
                Button temp = (Button)content.Children[0];

                if (temp.Equals(tbtn))
                {
                    return it;
                }
            }

            return null;
        }


        //ugly
        private TriggerItem getTriggerItem(ToggleButton tbtn)
        {
            foreach (TriggerItem it in triggerPanel.Children)
            {
                TriggerContent content = (TriggerContent)it.Child;
                ToggleButton temp = (ToggleButton)content.Children[1];

                if (temp.Equals(tbtn))
                {
                    return it;
                }
            }

            return null;
        }

        private void setIntensityTerrain(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_BRUSH_INTENSITY_TERRAIN, (int)terrainIntensitySlider.slider.Value);
        }


        private void setIntensitySmooth(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_BRUSH_INTENSITY_SMOOTH, (int)smoothIntensitySlider.slider.Value);
        }

        private void setBrushScale(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_BRUSH_SIZE, (int)brushScaleSlider.slider.Value);
            parent.Focus();
        }
        

        private void lowerButton_Click(object sender, RoutedEventArgs e)
        {
            setBrush(5);
        }

        private void raiseButton_Click(object sender, RoutedEventArgs e)
        {
            setBrush(6);
        }

        private void smoothButton_Click(object sender, RoutedEventArgs e)
        {
            setBrush(7);
        }

        private void blurButton_Click(object sender, RoutedEventArgs e)
        {
            setBrush(4);
        }

        private void levelButton_Click(object sender, RoutedEventArgs e)
        {
            setBrush(8);
        }
        
        private void toggleNumber(int number)
        {

            foreach( Label it in numberPanel.Children)
            {
                it.FontWeight = FontWeights.Normal;
                it.Foreground = Brushes.AntiqueWhite;
            }

            //dont want the last element 'cause its special
            for (int i = 0; i < rightBrushPanel.Children.Count-1; i++)
            {
                ((Button)rightBrushPanel.Children[i]).FontWeight = FontWeights.Normal;
                ((Button)rightBrushPanel.Children[i]).Foreground = Brushes.AntiqueWhite;
            }

            Button tempButton = (Button)rightBrushPanel.Children[4];
            WrapPanel tempPanel = (WrapPanel)tempButton.Content;
            ((Label)tempPanel.Children[0]).FontWeight = FontWeights.Normal;
            ((Label)tempPanel.Children[0]).Foreground = Brushes.AntiqueWhite;


            if (number < 4)
            {
                Label temp = (Label)numberPanel.Children[number];
                temp.FontWeight = FontWeights.UltraBold;
                temp.Foreground = Brushes.Red;
            }
            else
            {
                if (number < 8) //this is for the special "Level" button because it has WrapPanel as content
                {                    
                    Button temp = (Button)rightBrushPanel.Children[number - 4];
                    temp.FontWeight = FontWeights.UltraBold;
                    temp.Foreground = Brushes.Red;
                }
                else
                {
                    Button temp = (Button)rightBrushPanel.Children[number - 4];
                    WrapPanel wpTemp = (WrapPanel)temp.Content;
                    ((Label)wpTemp.Children[0]).FontWeight = FontWeights.UltraBold;
                    ((Label)wpTemp.Children[0]).Foreground = Brushes.Red;
                }

            }
        }

        public void setBrush(int number)
        {
            toggleNumber(number);

            editor.SendMessage((int)LevelMessage.MESSAGE_BRUSH, number);
        }


        private void setTextureOne(object sender, RoutedEventArgs e)
        {
            setBrush(0);
        }

        private void setTextureTwo(object sender, RoutedEventArgs e)
        {
            setBrush(1);   
        }

        private void setTextureThree(object sender, RoutedEventArgs e)
        {
            setBrush(2);
        }

        private void setTextureFour(object sender, RoutedEventArgs e)
        {
            setBrush(3);
        }

        public void saveLua(string name, string content)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_SAVE_LUA, name, content); // dont know what the second message should be..
        }

        public void renameLua(string oldName, string newName)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_RENAME_LUA, oldName, newName);
        }

        public void fogOn(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_TOGGLE_EFFECT, "fog on");
        }

        public void fogOff(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_TOGGLE_EFFECT, "fog off");
        }

        public void ssaoOn(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_TOGGLE_EFFECT, "ssao on");
        }

        public void ssaoOff(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_TOGGLE_EFFECT, "ssao off");
        }

        public void shadowOn(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_TOGGLE_EFFECT, "shadow on");
        }

        public void shadowOff(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_TOGGLE_EFFECT, "shadow off");
        }


        private void putObject(object sender, RoutedEventArgs e)
        {
            Button temp = (Button)sender;

            Console.WriteLine((string)temp.Content);

            //pray to god that the button has exactly right content!
            editor.SendMessage((int)LevelMessage.MESSAGE_PUT_OBJECT, (string)temp.Content);

            parent.Focus();
        }

        private void snapTerrainOn(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_SNAP_TO_TERRAIN, (int)TerrainSnapModes.SNAP_ON);
        }

        private void snapTerrainOff(object sender, RoutedEventArgs e)
        {
            editor.SendMessage((int)LevelMessage.MESSAGE_SNAP_TO_TERRAIN, (int)TerrainSnapModes.SNAP_OFF);
        }

        private void levelBoxLostFocus(object sender, RoutedEventArgs e)
        {
            sendLevelValue(((TextBox)sender).Text);
            setHotkeyLock(false);
        }

        private void levelBoxKeyRelease(object sender, System.Windows.Input.KeyEventArgs e)
        {
            TextBox senderTB = (TextBox)sender;
            if (e.Key == Key.Enter || e.Key == Key.Return)
            {
                sendLevelValue(senderTB.Text);
                rightBrushPanel.Children[4].Focus();
                setHotkeyLock(false);
            }
        }

        private void levelBoxGotFocus(object sender, RoutedEventArgs e)
        {
            setHotkeyLock(true);
        }

        private void sendLevelValue(string text)
        {
            Console.WriteLine("sending level value: " + text);
            float value = 0.0f;
            if (float.TryParse(text, out value))
            {
                Console.Write("  Successfully sent!");
                editor.SendMessage((int)LevelMessage.MESSAGE_LEVEL_VALUE, value);
            }
        }

        public void setHotkeyLock(bool value)
        {
            hotkeysLocked = value;
        }
    }
}
