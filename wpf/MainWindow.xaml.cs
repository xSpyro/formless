﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Windows.Interop;

using Microsoft.Win32;

using interopScene;

using wpf.EditorControllers;

namespace wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private D3DImageEx imageEx;
        private Editor editor;

        private Functions fun;

        private ShapeEditorController shapeController;
        private LevelEditorController levelController;
        private ParticleEditorController particleController;

        private EditorController currentController;


        public MainWindow()
        {
            InitializeComponent();
            
            fun = new Functions();

            this.Closed += CloseAllWindows;

            Loaded += MainWindow_Loaded;
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            IntPtr hwnd = (PresentationSource.FromVisual(this) as HwndSource).Handle;
 
            imageEx = new D3DImageEx();

            d3dScene0.Source = imageEx;
            d3dScene1.Source = imageEx;
            d3dScene2.Source = imageEx;


            editor = new Editor();
            editor.Setup(hwnd);

            shapeController = new ShapeEditorController(editor, this);
            levelController = new LevelEditorController(editor, this);
            particleController = new ParticleEditorController(editor);

            for (int i = 0; i < 200; ++i)
            {
                ToggleButton tempTB = fun.createButton(i.ToString(), 23, 20);
                CheckBox tempCB = fun.createCheckBox();

                Thickness tempt = new Thickness(0, 20, 0, 0);
                tempCB.Margin = tempt;

                FrameItem frame = new FrameItem(tempTB, tempCB);

                frame.MouseDown += frameToggle;
                tempTB.Click += frameToggle;

                shapeController.getTimeline().addFrame(frame);
            }

            baseGrid.Children.Add(shapeController.shapeSlider.slider);


            bottomGrid.Children.Add(shapeController.timeline);

            

            switchToShape();

            imageEx.SetBackBufferEx(D3DResourceTypeEx.ID3D11Texture2D, editor.GetRenderTarget());
            editor.SendMessage(0, 0.5f);
            CompositionTarget.Rendering += CompositionTarget_Rendering;
        }

        void switchToParticle()
        {
            currentController = particleController;
            editor.SwitchEditor(0);
            showParticleGUI();
            hideLevelGUI();
            hideShapeGUI();
        }

        void switchToLevel()
        {
            currentController = levelController;
            editor.SwitchEditor(1);
            hideParticleGUI();
            showLevelGUI();
            hideShapeGUI();
        }

        void switchToShape()
        {
            currentController = shapeController;
            editor.SwitchEditor(2);
            hideParticleGUI();
            hideLevelGUI();
            showShapeGUI();
        }

        void showParticleGUI()
        {
            //make the window visible
            particleController.showEditor();

            //and the other controls
        }

        void hideParticleGUI()
        {
            //the window

            particleController.hideEditor();

            //the other controls...
        }

        void showLevelGUI()
        {
            levelController.showEditor();
        }

        void hideLevelGUI()
        {
            levelController.hideEditor();
        }

        void showShapeGUI()
        {
            shapeController.showEditor();
        }

        void hideShapeGUI()
        {
            shapeController.hideEditor();
        }

        

        void d3d_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {

        }

        void d3d_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            
        }

        void d3d_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            // 604 is not the best way since the xbuttons is mapped there aswell
            editor.OnInput(610, e.Delta);
        }

        void d3d_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            System.Windows.Point pout = this.TranslatePoint(e.GetPosition(this), (Image)sender);

            float pX = (float)pout.X;
            float pY = (float)pout.Y;

            float nx = pX / (float)((Image)sender).ActualWidth;
            float ny = pY / (float)((Image)sender).ActualHeight;

            nx *= 2;
            nx -= 1;

            ny *= 2;
            ny -= 1;

            editor.OnMouseMove(pX, pY, nx, ny);
       }

        void d3d_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            editor.OnInput(600 + (int)e.ChangedButton, 1);
        }

        void d3d_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            editor.OnInput(600 + (int)e.ChangedButton, 0);
        }

        void CompositionTarget_Rendering(object sender, EventArgs e)
        {
            editor.Render();

            if ( shapeController.isVisible() )
                shapeController.update();
            

            InvalidateEditor();
        }

        void InvalidateEditor()
        {
            imageEx.Lock();
            imageEx.AddDirtyRect( new Int32Rect()
            {
                X = 0,
                Y = 0,
                Width = imageEx.PixelWidth,
                Height = imageEx.PixelHeight
            } );
            imageEx.Unlock();
        }

        private void CloseAllWindows(object sender, EventArgs e)
        {
            for (int intCounter = App.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
                App.Current.Windows[intCounter].Close();
            editor.Exit();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            currentController.saveFile();
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            currentController.openFile();
        }
        
        private void New_Click(object sender, RoutedEventArgs e)
        {
            currentController.newFile();
        }

        private void KeyDownEventHandler(object sender, System.Windows.Input.KeyEventArgs e)
        {

            currentController.keyDown(sender, e);


            if (e.Key == Key.R)
            {
                if ( shapeController.isVisible() )
                    shapeController.reloadShaders();
            }

        }

        private void KeyUpEventHandler(object sender,System.Windows.Input.KeyEventArgs e)
        {
            currentController.keyUp(sender, e);


            if (e.Key == Key.S && Keyboard.Modifiers  ==  ModifierKeys.Control)
            {
                currentController.saveFile();

            }
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                currentController.openFile();
            }
            if (e.Key == Key.N && Keyboard.Modifiers == ModifierKeys.Control)
            {
                currentController.newFile();
            }
            if (e.Key == Key.Delete)
            {
                
            }
            if (e.Key == Key.LeftShift || e.Key == Key.RightShift)
            {
                if (shapeController.isVisible())
                    shapeController.setTool(Tool.TOOL_PICKING);
            }

           
        }

        private void d3dScene_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            
        }

        //SWITCH TO PARTICLE EDITOR:
        private void d3dScene_SwitchEditor0(object sender, RoutedEventArgs e)
        {
            switchToParticle();

        }
        //SWITCH TO LEVEL EDITOR:
        private void d3dScene_SwitchEditor1(object sender, RoutedEventArgs e)
        {
            switchToLevel();

        }
        //SWITCH TO SHAPE EDITOR:
        private void d3dScene_SwitchEditor2(object sender, RoutedEventArgs e)
        {
            switchToShape();
        }

        private void frameToggle(object sender, RoutedEventArgs e)
        {
            shapeController.frameToggle(sender, e);
        }

        private void undo(object sender, RoutedEventArgs e)
        {
            //make this generic in the form of
            //currentController.undo()
            //when it is implemented...
            if ( shapeController.isVisible() )
            {
                shapeController.undo();
            }
        }

        private void redo(object sender, RoutedEventArgs e)
        {
            if ( shapeController.isVisible() )
            {
                shapeController.redo();
            }
        }
    }
}
