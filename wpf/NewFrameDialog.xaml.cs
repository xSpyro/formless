﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace wpf
{
    /// <summary>
    /// Interaction logic for NewFrameDialog.xaml
    /// </summary>
    public partial class NewFrameDialog : Window
    {
        private MainWindow parent;
        
        public NewFrameDialog(MainWindow pa)
        {
            parent = pa;
            InitializeComponent();
            this.Visibility = Visibility.Visible;
        }


        public void cancel(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        public void ok(object sender, RoutedEventArgs e)
        {
            //parent.addFrame( textBox1.GetLineText(0) );

            this.Close();
        }

        private void KeyUpEventHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ok(sender, e);
            }
            if (e.Key == Key.Escape)
            {
                this.Close();
            }

        }
    }
}
