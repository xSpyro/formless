
#ifndef BASE_MODULE_H
#define BASE_MODULE_H

#include "Framework.h"

class RenderModule;

class BaseModule : public Stage
{

public:

	virtual ~BaseModule() { }
	virtual void bind(RenderModule * rm) { }

	virtual void update() { }
	virtual void apply() { }
};

#endif