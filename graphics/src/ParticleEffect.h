#ifndef PARTICLE_EFFECT_H
#define PARTICLE_EFFECT_H

#include "ParticleAnimation.h"


class ParticleEffect
{
public:

	ParticleEffect();
	~ParticleEffect();

	typedef std::vector< ParticleAnimation > ParticleAnimationVector;
	ParticleAnimationVector animations;

	bool created;

private:

};

#endif