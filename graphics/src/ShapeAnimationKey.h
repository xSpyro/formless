
#ifndef SHAPE_ANIMATION_KEY_H
#define SHAPE_ANIMATION_KEY_H

#include "ShapeNode.h"
#include "ShapeEffectData.h"

struct ShapeAnimationKey
{
	ShapeAnimationKey()
		: count(0)
	{
	}

	void clear();

	int newNode();

	// 2d array aka texture with int pixels
	ShapeNode nodes[SHAPE_NODES_MAX];

	unsigned int count;

	ShapeEffectData data;
};

#endif