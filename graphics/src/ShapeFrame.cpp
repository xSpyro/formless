
#include "ShapeFrame.h"

#include "ShapeResult.h"
#include "ShapeAnimationKey.h"

/*
bool ShapeFrame::createLerpedResult(ShapeResult * out, const Vector3 & offset, float scale, float angle, float time)
{
	if(dst == NULL)
		return false;

	// now combine the src and dst keys even if the number of nodes vary

	// check the difference first to decide how many to bind to existing
	// and how many to reference from 0 0 0 relative

	ZeroMemory(out, sizeof(ShapeResult));

	int diff = std::abs((int)src->count - (int)dst->count);

	int count = std::max(src->count, dst->count);

	int results = 0;

	
	float rcos = cosf(-angle);
	float rsin = sinf(-angle);

	if (diff == 0)
	{
		// straight conversion
		
		for (int i = 0; i < count; ++i)
		{
			// copy the branches from the src frame
			for (int c = 0; c < SHAPE_BRANCHES_MAX; ++c)
			{
				out->nodes[i].cache[c] = (float)src->nodes[i].cache[c];
			}

			out->nodes[i].count = (float)src->nodes[i].branches.size();
			
			// position is lerped
			Vector3 & p = out->nodes[i].position;
			p = src->nodes[i].position.lerp(dst->nodes[i].position, delta) * scale;
			
			float temp[2];
			temp[0] = (p.x * rcos - p.z * rsin);
			temp[1] = (p.x * rsin + p.z * rcos);
			p.x = temp[0];
			p.z = temp[1];

			p += offset;
		}

		out->effect.die = src->data.die;
		out->effect.gravity = src->data.gravity;
		out->effect.speed = src->data.speed;

		results = count;
	}
	else 
	{
		const ShapeAnimationKey *lower, *higher;
		unsigned int count = 0;
		float delta_now = 0;

		if (src->count > dst->count)
		{
			lower = dst;
			higher = src;
			count = src->count;

			delta_now = delta;
		}
		else
		{
			lower = src;
			higher = dst;
			count = dst->count;

			delta_now = 1 - delta;
		}

		float next = (float)lower->count / (float)higher->count;


		float j = 0;
		for (unsigned int i = 0; i < count; ++i)
		{
			for (int c = 0; c < SHAPE_BRANCHES_MAX - 1; ++c)
			{
				// delta * 2 is just experimenting with having longer duration of higher or lower branchings
				//float res1 = lerp((float)higher->nodes[i].cache[c], (float)lower->nodes[(int)j].cache[c], delta_now);
				//float res2 = lerp((float)higher->nodes[i].cache[c + 1], (float)lower->nodes[(int)j].cache[c + 1], delta_now);

				out->nodes[i].cache[c] = (float)higher->nodes[i].cache[c];
			}

			out->nodes[i].count = (float)higher->nodes[i].branches.size();

			Vector3 & p = out->nodes[i].position;
			p = higher->nodes[i].position.lerp(lower->nodes[(int)j].position, delta_now) * scale;
			


			float temp[2];
			temp[0] = (p.x * rcos - p.z * rsin);
			temp[1] = (p.x * rsin + p.z * rcos);
			p.x = temp[0];
			p.z = temp[1];

			p += offset;

			j += next;
		}
		out->effect.die = lerp(src->data.die, dst->data.die, delta);
		out->effect.gravity = lerp(src->data.gravity, dst->data.gravity, delta);
		out->effect.speed = lerp(src->data.speed, dst->data.speed, delta);
		
		results = count;
	}

	// add links if any
	if (linked && results > 0)
	{
		float len = (offset - linkPoint).length();
		Vector3 d = (linkPoint - offset) / len;

		bool first = true;
		int count = 0;

		Vector3 r = d.cross(Vector3(0, 1, 0));

		for (float i = 20.0f; i < len; i += 10.0f, ++count)
		{

			appendLink(offset + d * i + r * sin(time * 15 + i), out, results, count, time);


			results += 1; // to fit the link node
		}	
	}

	// okay
	// fill in the blanks

	if(results > 0)
	{
		for (int i = results; i < SHAPE_NODES_MAX - results; i += results)
		{
			std::memcpy(&out->nodes[i], &out->nodes[0], results * sizeof(ShapeResult::Node));
		}
	}
	
	


	int left = SHAPE_NODES_MAX - results;

	if (left > 0)
	{
		std::memcpy(&out->nodes[SHAPE_NODES_MAX - left], &out->nodes[0], left * sizeof(ShapeResult::Node));
	}
	

	out->count = results;


	return true;
}
*/

bool ShapeFrame::createLerpedResult(ShapeResult * out, Desc * desc)
{
	if(dst == NULL)
		return false;


	Vector3 offset = 0;
	float scale = 1;
	float angle = 0;
	float time = 0;
	int temp = 128;
	int * linkOffset = &temp;

	if (desc)
	{
		offset = desc->offset;
		scale = desc->scale;
		angle = desc->angle;
		time = desc->time;

		if (desc->linkOffset)
			linkOffset = desc->linkOffset;
	}


	// now combine the src and dst keys even if the number of nodes vary

	// check the difference first to decide how many to bind to existing
	// and how many to reference from 0 0 0 relative

	ZeroMemory(out, sizeof(ShapeResult));

	int diff = std::abs((int)src->count - (int)dst->count);

	int count = std::max(src->count, dst->count);

	int results = 0;

	
	float rcos = cosf(-angle);
	float rsin = sinf(-angle);

	if (diff == 0)
	{
		// straight conversion
		
		for (int i = 0; i < count; ++i)
		{
			int back = 0;

			// copy the branches from the src frame
			for (int c = 0; c < SHAPE_BRANCHES_MAX; ++c)
			{
				if (src->nodes[i].cache[c - back] < 0)
				{
					back = c;
				}

				out->nodes[i].cache[c] = (float)src->nodes[i].cache[c - back];

				if (out->nodes[i].cache[c] < 0)
					out->nodes[i].cache[c] = 0;
			}

			out->nodes[i].count = (float)src->nodes[i].branches.size();
			
			// position is lerped
			Vector3 & p = out->nodes[i].position;
			p = src->nodes[i].position.lerp(dst->nodes[i].position, delta) * scale;
			
			float temp[2];
			temp[0] = (p.x * rcos - p.z * rsin);
			temp[1] = (p.x * rsin + p.z * rcos);
			p.x = temp[0];
			p.z = temp[1];

			p += offset;
		}

		out->effect.die = src->data.die;
		out->effect.gravity = src->data.gravity;
		out->effect.speed = src->data.speed;

		results = count;
	}
	else 
	{
		const ShapeAnimationKey *lower, *higher;
		unsigned int count = 0;
		float delta_now = 0;

		if (src->count > dst->count)
		{
			lower = dst;
			higher = src;
			count = src->count;

			delta_now = delta;
		}
		else
		{
			lower = src;
			higher = dst;
			count = dst->count;

			delta_now = 1 - delta;
		}

		float next = (float)lower->count / (float)higher->count;


		float j = 0;
		for (unsigned int i = 0; i < count; ++i)
		{
			int back = 0;

			for (int c = 0; c < SHAPE_BRANCHES_MAX; ++c)
			{
				// delta * 2 is just experimenting with having longer duration of higher or lower branchings
				//float res1 = lerp((float)higher->nodes[i].cache[c], (float)lower->nodes[(int)j].cache[c], delta_now);
				//float res2 = lerp((float)higher->nodes[i].cache[c + 1], (float)lower->nodes[(int)j].cache[c + 1], delta_now);

				
				
				//out->nodes[i].cache[c] = (float)lower->nodes[(int)j].cache[c];
				/*
				if ( (int)res1 == i )
					res1 += 1;

				if ( (int)res2 == res1 )
					res2 += 2;
				out->nodes[i].cache[c] = lerp(res1, res2, delta_now);
				*/

				if (higher->nodes[i].cache[c - back] < 0)
				{
					back = c;
				}

				out->nodes[i].cache[c] = (float)higher->nodes[i].cache[c - back];

				if (out->nodes[i].cache[c] < 0)
					out->nodes[i].cache[c] = 0;
			}

			out->nodes[i].count = (float)higher->nodes[i].branches.size();

			Vector3 & p = out->nodes[i].position;
			p = higher->nodes[i].position.lerp(lower->nodes[(int)j].position, delta_now) * scale;
			


			float temp[2];
			temp[0] = (p.x * rcos - p.z * rsin);
			temp[1] = (p.x * rsin + p.z * rcos);
			p.x = temp[0];
			p.z = temp[1];

			p += offset;

			j += next;
		}
		out->effect.die = lerp(src->data.die, dst->data.die, delta);
		out->effect.gravity = lerp(src->data.gravity, dst->data.gravity, delta);
		out->effect.speed = lerp(src->data.speed, dst->data.speed, delta);
		
		results = count;
	}

	/*

	// add links if any
	if (linked && results > 0 && desc)
	{
		*linkOffset = results;


		float len = (offset - linkPoint).length();
		Vector3 d = (linkPoint - offset) / len;

		bool first = true;
		int count = 0;

		Vector3 r = d.cross(Vector3(0, 1, 0));



		for (float i = 10.0f; i < len; i += (20.0f + random(-5, 5)))
		{
			appendLink(offset + d * i + r * sin(i + time * 15), out, results, count, time);

			results += 1; // to fit the link node
			count += 1;

			//break;
		}	



		appendLink(linkPoint, out, results, -1, time);
		results += 1; 
	}

	*/

	// okay
	// fill in the blanks

	if(results > 0)
	{
		for (int i = results; i < SHAPE_NODES_MAX - results; i += results)
		{
			std::memcpy(&out->nodes[i], &out->nodes[0], results * sizeof(ShapeResult::Node));
		}
	}
	
	


	int left = SHAPE_NODES_MAX - results;

	if (left > 0)
	{
		std::memcpy(&out->nodes[SHAPE_NODES_MAX - left], &out->nodes[0], left * sizeof(ShapeResult::Node));
	}
	

	out->count = results;




	for (unsigned int i = 0; i < SHAPE_NODES_MAX; ++i)
	{
		for (int j = 0; j < 16; ++j)
		{
			float branch = out->nodes[i].cache[j];
			if (branch < 0)
				std::cout << j << " " << (int)branch << std::endl;

		}
	}


	return true;
}

bool ShapeFrame::createLerpedAnimationKey(ShapeAnimationKey * out, float time)
{
	static ShapeResult temp;

	

	if (!createLerpedResult(&temp))
		return false;

	out->count = temp.count;

	for (int i = 0; i < temp.count; ++i)
	{

		out->nodes[i].clear();
		out->nodes[i].position = temp.nodes[i].position;

		for (int j = 0; j < temp.nodes[i].count; ++j)
		{
			out->nodes[i].addBranch((int)temp.nodes[i].cache[j]);
		}
	}

	return true;
}

void ShapeFrame::enableLink(bool state)
{
	linked = state;
}

void ShapeFrame::setLinkPoint(const Vector3 & position)
{
	linkPoint = position;
}

void ShapeFrame::appendLink(const Vector3 & position, ShapeResult * out, int index, int count, float time)
{
	out->nodes[index].position = position;

	float target = static_cast < float > (index);

	

	if (count == 0)
	{
		unsigned int n = (unsigned int)index / 2;
		unsigned int start = (unsigned int)(sinf(time * 5) + 1) * n;


		start = 0;
		n = 10;

		for (unsigned int i = start; i < (unsigned int)index - 20; i += n)
		{
			int p = static_cast < int > (out->nodes[i].count);


			out->nodes[i].cache[p] = target;
			out->nodes[i].count += 1.0f;
		}	
	}
	else
	{
		for (unsigned int i = index - 1; i < (unsigned int)index; i += 1)
		{
			for (int b = 0; b < SHAPE_BRANCHES_MAX; ++b)
				out->nodes[i].cache[b] = target;
			out->nodes[i].count = 1.0f;
		}	
	}


	if (count == -1)
	{
		int p = static_cast < int > (index);

		for (int b = 0; b < SHAPE_BRANCHES_MAX; ++b)
			out->nodes[index].cache[b] = target - 1;
		out->nodes[index].count = 1.0f;
	}

}

void ShapeFrame::bindAllTo(const Vector3 & position, ShapeResult * out, int index)
{
	//out->nodes[index].position = position;
	//out->nodes[index].count = 1.0f;

	//for (int b = 0; b < SHAPE_BRANCHES_MAX; ++b)
	//	out->nodes[0].cache[b] = index;

	
}

/*
void ShapeFrame::appendLink(const Vector3 & position, ShapeResult * out, int index, int count)
{
	out->nodes[index].position = position;

	float target = static_cast < float > (index);

	{
		int p = static_cast < int > (out->nodes[index - 1].count);
		out->nodes[index - 1].cache[p] = target;
		out->nodes[index - 1].count += 1.0f;
	}

	{
		int p = static_cast < int > (out->nodes[index].count);
		out->nodes[index].cache[p] = target - 1;
		out->nodes[index].count += 1.0f;
	}
}
*/