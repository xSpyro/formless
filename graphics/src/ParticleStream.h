
#ifndef PARTICLE_STREAM
#define PARTICLE_STREAM

#include "Common.h"

#include "ShapeResult.h"

class ParticleStream
{

	struct Particle
	{
		Vector3 position;
		Vector3 velocity;
		Vector3 target;
		Vector3 origin;
		float from;
		float route;
		float life;
		float seed;
	};

	typedef std::vector < Particle > Queue;

public:

	ParticleStream();
	~ParticleStream();

	void init(Renderer * renderer, Camera * camera);
	void reset();

	void insert(const Vector3 & position, float route = 0);
	void insertConnection(float from, float to);
	void insertFreeConnection(const Vector3 & src, const Vector3 & dst);

	void setShapeData(const ShapeResult * result);

	void update();

	void render();

	bool isEmpty();

	Mesh * getRenderMesh();

	

private: // old
public:

	Effect eff_update;
	Effect eff_render;

	Mesh * mesh[2];
	int src;
	int dst;

	unsigned int particles;
	Queue queue;

	ConstantBuffer * buffer;
	

	Renderer * renderer;

	Camera * camera;
};

#endif