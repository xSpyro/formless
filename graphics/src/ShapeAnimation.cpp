
#include "ShapeAnimation.h"
#include "ShapeNode.h"
#include "ShapeAnimationKey.h"

ShapeAnimation::ShapeAnimation()
{
	length = 0;
}

void ShapeAnimation::clear()
{
	length = 0;
	frames.clear();
}

unsigned int ShapeAnimation::getSize() const
{
	return frames.size();
}

int ShapeAnimation::addNode(unsigned int frame, int parent, const Vector3 & position)
{
	// create if needed
	Frames::iterator i = frames.find(frame);
	if (i == frames.end())
	{
		ShapeAnimationKey key;

		key.count = 0;

		frames[frame] = key;
	}
	
	if (length < frame)
	{
		length = frame;
	}

	// get it
	i = frames.find(frame);
	if (i != frames.end())
	{
		ShapeAnimationKey * key = &i->second;

		int index = key->newNode();
		ShapeNode * node = &key->nodes[index];

		node->position = position;

		// fix parent branching
		if (parent > -1)
		{
			key->nodes[parent].addBranch(index);
		}

		return index;
	}

	return -1;
}

void ShapeAnimation::moveNode(unsigned int frame, int node, const Vector3 & position)
{
	Frames::iterator i = frames.find(frame);
	if (i != frames.end())
	{
		i->second.nodes[node].position = position;
	}
}

bool ShapeAnimation::removeNode(unsigned int frame, int node)
{
	// lower length if necessary

	Frames::iterator i = frames.find(frame);
	if (i != frames.end())
	{
		ShapeAnimationKey& key = i->second;

		ShapeNode& n = key.nodes[node];

		n.clear();

		// align data
		if(i->second.count > 0)
		{
			for(unsigned int j = node; j < i->second.count - 1; j++)
			{
				 key.nodes[j] = key.nodes[j+1];
			}
		
			 // erase the last node
			 key.nodes[key.count - 1] = ShapeNode();
			 //key.nodes[i->second.count] = ShapeNode();
			key.count--;

			// remove all links from and to the node
			for(unsigned int j = 0; j < key.count; j++)
			{
				std::vector<unsigned int> positions;
				for(unsigned int k = 0; k < key.nodes[j].branches.size(); k++)
				{
					if(key.nodes[j].branches[k] == node)
						positions.push_back(k);
				}

				for(unsigned int k = 0; k < key.nodes[j].branches.size(); k++)
				{
					if(key.nodes[j].branches[k] > node)
						key.nodes[j].branches[k]--;
				}

				for(unsigned int k = 0; k < positions.size(); k++)
				{
					key.nodes[j].branches.erase(key.nodes[j].branches.begin() + (positions[k] - k));
				}
				for(unsigned int k = 0; k < SHAPE_BRANCHES_MAX; k++)
				{
					if(key.nodes[j].cache[k] == node)
						key.nodes[j].cache[k] = j;
				}

				for(unsigned int k = 0; k < SHAPE_BRANCHES_MAX; k++)
				{
					if(key.nodes[j].cache[k] > node)
						key.nodes[j].cache[k]--;
				}
			
			}
		}


		if (length == frame)
		{
			length = 0;
			for(Frames::iterator jt = frames.begin(); jt != frames.end(); jt++)
			{
				if(jt->first > length)
				{
					length = jt->first;
				}
			}
		}

		// always keep the last frame
		if ( i->second.count == 0 )
		{
			unsigned int frame = i->first;
			
			frames.erase(i);

			if (frame == 0)
			{
				ShapeAnimationKey key;

				key.count = 0;

				frames[frame] = key;
			}
		}

		return true;
	}

	return false;
}


bool ShapeAnimation::removeFrame(const unsigned int frame)
{
	Frames::iterator i = frames.find(frame);
	if (i != frames.end())
	{
		frames.erase(i);

		return true;
	}

	return false;
}


bool ShapeAnimation::recreateFrame(unsigned int frame, const ShapeResult * result)
{
	// use count in result to recreate the vector in a key
	Frames::iterator it = frames.find(frame);
	Frames::iterator f;
	if (it == frames.end())
	{
		it = frames.insert(frames.end(), Frames::value_type(frame, ShapeAnimationKey()));
	}
	
	{
		f = it;
		ShapeAnimationKey * key = &f->second;

		key->data = result->effect;

		// now fill this completely
		for (int i = 0; i < result->count; ++i)
		{
			const ShapeResult::Node & rnode = result->nodes[i];
			
			ShapeNode & node = key->nodes[i];

			node.position = rnode.position;
			
			node.branches.reserve((unsigned int)rnode.count);

			
			for (unsigned int j = 0; j < rnode.count; ++j)
			{
				node.addBranch(static_cast < int > (rnode.cache[j]));
				
				//node.cache[j] = (int)rnode.cache[j];
				//node.branches.push_back(node.cache[j]);
			} 
		}

		key->count = result->count;

		
		if (length < frame)
		{
			length = frame;
		}

		return true;
	}

	return false;
}

bool ShapeAnimation::linkNode(unsigned int frame, int index, int target)
{
	Frames::iterator i = frames.find(frame);
	if (i != frames.end())
	{
		ShapeAnimationKey * key = &i->second;

		ShapeNode * node = &key->nodes[index];

		node->addBranch(target);

		return true;
	}

	return false;
}

bool ShapeAnimation::unlinkNode(unsigned int frame, int target)
{
	Frames::iterator i = frames.find(frame);
	if (i != frames.end())
	{
		ShapeAnimationKey * key = &i->second;

		for (unsigned int i = 0; i < key->count; i++)
		{

			ShapeNode * node = &key->nodes[i];

			node->removeBranch(target);
		}

		if (key->count > 0)
		{
			ShapeNode* node = &key->nodes[target];
		
			node->clear();
		}
		
		return true;
	}

	return false;
}

ShapeFrame ShapeAnimation::getAnimationKey(unsigned int frame) const
{
	// find the key that lies closest backwards from frame
	// then find the first front key

	// previous

	ShapeFrame out;

	// validate pos
	if (frame > length)
	{
		return out;
	}
	else if (frame == length && length > 0)
	{
		// last frame
		const ShapeAnimationKey & key = (--frames.end())->second;

		out.src = out.dst = &key;
		out.delta = 1;
	}
	else if (frames.size() == 1) // see which two frames we are blending between
	{
		// single static pose

		const ShapeAnimationKey & key = frames.begin()->second;

		out.src = out.dst = &key;
		out.delta = 0;
	}
	else if (frames.size() > 1)
	{
		// animated
		unsigned int lerp[2];
		Frames::const_iterator i1 = getLeftBound(frame);

		const ShapeAnimationKey & frame1 = i1->second;
		lerp[0] = i1->first;

		i1++;
		const ShapeAnimationKey & frame2 = i1->second;
		lerp[1]= i1->first;

		out.src = &frame1;
		out.dst = &frame2;
		out.delta = (frame - lerp[0]) / static_cast < float > (lerp[1] - lerp[0]);
	}
	else
	{
		// default frame 0 0 0
	}

	return out;
}

ShapeFrame ShapeAnimation::getAnimationKeyMorph(const ShapeAnimationKey * key, float delta) const
{
	ShapeFrame out;

	out.src = key;
	out.dst = getFirstKey();

	if (out.dst == NULL)
		out.dst = out.src;
	out.delta = delta;

	return out;
}

void ShapeAnimation::setFrameGravity(unsigned int frame, const float g)
{
	Frames::iterator it = frames.find(frame);
	if(it == frames.end())
		it = frames.insert(frames.end(), Frames::value_type(frame, ShapeAnimationKey()));

	if ( it != frames.end() )
		it->second.data.gravity = g;
}

void ShapeAnimation::setFrameSpeed(unsigned int frame, const float s)
{
	Frames::iterator it = frames.find(frame);
	if(it == frames.end())
		it = frames.insert(frames.end(), Frames::value_type(frame, ShapeAnimationKey()));

	if ( it != frames.end() )
		it->second.data.speed = s;
}

void ShapeAnimation::setFrameDeath(unsigned int frame, const float d)
{
	Frames::iterator it = frames.find(frame);
	if(it == frames.end())
		it = frames.insert(frames.end(), Frames::value_type(frame, ShapeAnimationKey()));

	if ( it != frames.end() )
		it->second.data.die = d;
}

const float ShapeAnimation::getFrameGravity(unsigned int frame) 
{
	Frames::iterator it = frames.find(frame);
	if(it == frames.end())
		it = frames.insert(frames.end(), Frames::value_type(frame, ShapeAnimationKey()));

	if ( it != frames.end() )
		return it->second.data.gravity;

	return 0.0f;
}

const float ShapeAnimation::getFrameSpeed(unsigned int frame)
{
	Frames::iterator it = frames.find(frame);
	if(it == frames.end())
		it = frames.insert(frames.end(), Frames::value_type(frame, ShapeAnimationKey()));

	if ( it != frames.end() )
		return it->second.data.speed;

	return 0.0f;
}

const float ShapeAnimation::getFrameDeath(unsigned int frame)
{
	Frames::iterator it = frames.find(frame);

	if(it == frames.end())
		it = frames.insert(frames.end(), Frames::value_type(frame, ShapeAnimationKey()));

	if ( it != frames.end() )
		return it->second.data.die;

	return 0.0f;
}

bool ShapeAnimation::isEnd(unsigned int frame) const
{
	return frame >= length;
}

bool ShapeAnimation::exportToVector(std::vector < unsigned int > & out_f, std::vector < ShapeResult > & out_r) const
{
	out_f.reserve(frames.size());

	out_r.assign(frames.size(), ShapeResult());

	unsigned int j = 0;
	for (Frames::const_iterator i = frames.begin(); i != frames.end(); ++i)
	{
		// add frame
		out_f.push_back(i->first);

		ShapeFrame temp;
		temp.src = &i->second;
		temp.dst = &i->second;
		temp.delta = 0;

		temp.createLerpedResult(&out_r[j]);
		++j;
	}

	return true;
}

ShapeAnimation::Frames::const_iterator ShapeAnimation::getLeftBound(unsigned int pos) const
{
	// check for first frame thats n ot 0
	if (pos < frames.begin()->first)
		return frames.begin();


	for (Frames::const_iterator i = frames.begin(); i != frames.end();)
	{
		Frames::const_iterator i1 = i;
		++i;

		if (pos >= i1->first && pos < i->first)
			return i1;
	}

	return frames.end();
}

const ShapeAnimationKey * ShapeAnimation::getFirstKey() const
{
	if (frames.empty())
		return NULL;

	return &frames.begin()->second;
}

const ShapeAnimationKey * ShapeAnimation::getLastKey() const
{
	return &(--frames.end())->second;
}