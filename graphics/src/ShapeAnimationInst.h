
#ifndef SHAPE_ANIMATION_INST
#define SHAPE_ANIMATION_INST

#include "Common.h"

#include "ShapeFrame.h"


class ShapeAnimation;

#include "ShapeAnimationKey.h"

class ShapeAnimationInst
{
public:

	ShapeAnimationInst();

	// changes the base animation
	bool changeShape(ShapeAnimation * animation);

	// jump instantly
	bool jumpShape(ShapeAnimation * animation);

	// runs once then back to previous base animation
	bool executeShape(ShapeAnimation * animation);

	ShapeFrame getFrame() const;

	void update();

	void jump(unsigned int frame);

	void addSpeed(float speed);
	void setSpeed(float speed);

	void reset();

	void debug() const;

private:

	int frame;
	float next;
	float speed;

	float scale;
	float angle;

	ShapeAnimation * animation;
	ShapeAnimationKey from;

	ShapeAnimation * previous;

	bool morph;

	RenderComponent * com;
};

#endif