
#ifndef SCENE_LIGHT_BUFFER
#define SCENE_LIGHT_BUFFER

#include "Common.h"

struct SceneLightBuffer
{
	struct Entity
	{
		Vector3 position;
		float radius;
		Vector3 color;
		float other;
	};

	Entity entity[128];
	float count;

	SceneLightBuffer()
		: count(0.0f)
	{
	}
};

#endif