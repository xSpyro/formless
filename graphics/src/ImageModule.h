
#ifndef IMAGE_MODULE_H
#define IMAGE_MODULE_H

#include "BaseModule.h"

class ImageModule : public BaseModule
{

public:
	void bind(RenderModule * rm);

	void update();
	void apply();

private:

	Layer * images;
};

#endif