
#ifndef SHAPE_EFFECT_DATA_H
#define SHAPE_EFFECT_DATA_H

struct ShapeEffectData
{
	float speed;
	float gravity;
	float die;
	float notused[5];

	ShapeEffectData()
		: speed(1), gravity(0), die(0)
	{
	}
};

#endif