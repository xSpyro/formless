
#include "ParticleStream.h"

#include "Framework.h"

ParticleStream::ParticleStream()
{
	buffer = NULL;
	particles = 0;

	src = 0;
	dst = 1;

	mesh[0] = NULL;
	mesh[1] = NULL;
}

ParticleStream::~ParticleStream()
{
	if (buffer)
	{
		delete buffer;

		mesh[0]->release();
		mesh[1]->release();
	}
}

void ParticleStream::init(Renderer * renderer, Camera * camera)
{
	this->renderer = renderer;
	this->camera = camera;

	if (buffer == NULL)
	{
		buffer = new ConstantBuffer(renderer, 4096 * 4);


		mesh[0] = renderer->mesh()->create();
		mesh[1] = renderer->mesh()->create();
		src = 0;
		dst = 1;

		mesh[0]->createSwapMesh(mesh[1]);


		BaseShader * vs = renderer->shader()->get("shaders/particle.vs");
		BaseShader * gs_update = renderer->shader()->get("shaders/particle_stream_update.gs", SHADER_STREAM_OUT);

		eff_update.bind(vs, NULL, gs_update);




		{
			MeshStream * stream = mesh[0]->createEmptyStream("Particle");
			stream->addDesc("POSITION", sizeof(Vector3));
			stream->addDesc("VELOCITY", sizeof(Vector3));
			stream->addDesc("TARGET", sizeof(Vector3));
			stream->addDesc("ORIGIN", sizeof(Vector3));
			stream->addDesc("FROM", sizeof(float));
			stream->addDesc("ROUTE", sizeof(float));
			stream->addDesc("LIFE", sizeof(float));
			stream->addDesc("SEED", sizeof(float));

			stream->fill(6000);

			mesh[0]->setTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
			mesh[0]->submit(vs);
		}

		{
			MeshStream * stream = mesh[1]->createEmptyStream("Particle");
			stream->addDesc("POSITION", sizeof(Vector3));
			stream->addDesc("VELOCITY", sizeof(Vector3));
			stream->addDesc("TARGET", sizeof(Vector3));
			stream->addDesc("ORIGIN", sizeof(Vector3));
			stream->addDesc("FROM", sizeof(float));
			stream->addDesc("ROUTE", sizeof(float));
			stream->addDesc("LIFE", sizeof(float));
			stream->addDesc("SEED", sizeof(float));

			stream->fill(6000);

			mesh[1]->setTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
			mesh[1]->submit(vs);
		}

		particles = 0;
	}
}

void ParticleStream::reset()
{
	particles = 100;
}

void ParticleStream::insert(const Vector3 & position, float route)
{
	// next time we call update we add these particles into the mix

	//if (particles + queue.size() < 9000)
	//{
		Particle p;
		p.position = position + Vector3::random(1, 1, 1) * 0.1f;
		p.target = p.position;
		p.seed = random(0, 10);
		p.origin = p.position;
		p.from = 0;
		p.route = route;

		p.velocity = Vector3::random(1, 1, 1) * 0.1f;


		p.life = 1.0f;

		queue.push_back(p);
	//}

	particles = 100;
}

void ParticleStream::insertConnection(float from, float to)
{
	//if (particles + queue.size() < 10000)
	//{
		Particle p;
		p.position = 0;
		p.target = 0;
		p.seed = 10;// + random(0, 5);
		p.origin = 0;
		p.from = from;
		p.route = to;

		p.velocity = Vector3::random(1, 1, 1);


		p.life = random(1, 3);

		queue.push_back(p);
	//}

	particles = 100;
}

void ParticleStream::insertFreeConnection(const Vector3 & src, const Vector3 & dst)
{
	Particle p;
	p.position = 0;
	p.target = dst;
	p.seed = random(13, 16);// + random(0, 5);
	p.origin = src;
	p.from = 0;
	p.route = -1;

	p.velocity = Vector3::random(1, 1, 1);


	p.life = random(5, 10);

	queue.push_back(p);
}

void ParticleStream::setShapeData(const ShapeResult * result)
{

	buffer->set((void*)result->nodes, sizeof(ShapeResult));

	eff_update.getGS()->setConstantBuffer(4, buffer);
	//eff_render.getGS()->setConstantBuffer(4, buffer);
}

void ParticleStream::update()
{
	eff_update.apply();

	//eff_update.getGS()->setTexture(0, renderer->texture()->get("textures/rand.png"));

	// add from queue
	if (!queue.empty())
	{ 
		// copy the new particles to the back of the gpu buffer
		//mesh[dst]->getFirstStream()->pushAndUpdate(renderer, &queue.front(), queue.size(), particles);
		mesh[dst]->getFirstStream()->prependBeforeSO(renderer, &queue.front(), queue.size());


		//particles += queue.size();
		queue.clear();
	}

	mesh[dst]->streamFromAuto(mesh[src]);

	std::swap(src, dst);
	mesh[0]->swap();

	//particles = mesh[dst]->streamFrom(mesh[src], particles);
	



	//std::cout << particles << std::endl;



	// dying 
	if (particles > 0)
		particles -= 1;
}

void ParticleStream::render()
{
	/*
	Viewport::prepareCamera(renderer, camera);

	eff_render.apply();

	mesh[src]->bind();
	mesh[src]->draw(particles);
	*/
}

bool ParticleStream::isEmpty()
{
	return particles <= 0;
}

Mesh * ParticleStream::getRenderMesh()
{
	return mesh[0];
}