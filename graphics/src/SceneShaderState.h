
#ifndef SCENE_SHADER_STATE
#define SCENE_SHADER_STATE

#include "Common.h"

typedef struct SceneShaderState
{
	Matrix m[3];
	Vector3 position;
	float strength;
	Color color[2];
	Vector2 screen_pos;
	Vector2 dimension;
	float state[16];

	// state is graphical shader flags
	/*
	 * 0 = ghost mode
	 * 1
	 * 2
	 * 3
	 * 4
	 * 5
	 * 6
	 * 7
	 * 8 = fog
	 */

} Sun;

#endif