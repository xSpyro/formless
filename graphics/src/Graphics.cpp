
#include "Graphics.h"


#include "Events.h"
#include "Object.h"

Graphics::Graphics()
{
	
	srand((unsigned)time(0));
	
	
	for (int i = 0; i < 2000; i += 50)
	{
		//generateRandomFrame(i);

		generateCircleFrame(i);

		//generateRandomFrame(i + 200);
	}
	
	/*
	for (int i = 0; i < 2000; i += 100)
	{

		generateCircleFrame(i);
	}
	*/
	
	inst.changeShape(&animation);
}

void Graphics::generateRandomFrame(int frame)
{
	

	int n = (int)random(-3, 16);

	if (n < 1) n = 1;

	int prev = -1;
	for (int i = 0; i < n; ++i)
	{
		int bind = prev;
		prev = animation.addNode(frame, bind, Vector3::random(6, 6, 6));
	}

	animation.linkNode(frame, prev, 0);
}

void Graphics::generateCircleFrame(int frame)
{
	int n = (int)random(5, 48);

	float add = 6.28f / (n * 0.2f);

	float offset = random(0, 10);

	int prev = -1;
	for (float i = offset - 3.14f; i <= offset + 3.14f; i += add)
	{
		float x = cos(i) * (n / 5.0f);
		float y = sin(i) * (n / 5.0f);

		int bind = prev;
		prev = animation.addNode(frame, bind, Vector3(x, y, 0));
	}

	animation.linkNode(frame, prev, 0);
}

const ShapeResult * Graphics::showFrame(LogicHandler * logic, int frame)
{

	if (frame >= 0)
	{
		inst.jump(frame);
	}
	else if (frame == -1)
	{
		
		inst.update();
	}

	ShapeFrame shape = inst.getFrame();

	
	shape.createLerpedResult(&result);


	int neg = 0;

	int j = 0;
	for (Objects::iterator i = objects.begin(); i != objects.end(); ++i, ++j)
	{
		Object * obj = *i;

		PositionEvent ev(result.nodes[j].position);
		obj->send(&ev);

		++neg;
	}


	for (int i = 0; i < result.count - neg; ++i)
	{
		Object * asd = logic->createObject("Node");

		objects.push_back(asd);
	}

	return &result;
}

void Graphics::temp(float speed)
{
	inst.addSpeed(speed);
}

void Graphics::init(Renderer * renderer)
{
	this->renderer = renderer;

	overlay.init(renderer);

	camera.setProjection2D(1280, 720);

	Viewport * viewport = overlay.addViewport(&camera);


	BaseShader * vs = renderer->shader()->get("shaders/font.vs");
	BaseShader * ps = renderer->shader()->get("shaders/font.ps");

	viewport->getEffect()->blendModeAlpha();
	viewport->setShaders(vs, NULL, ps);



	font[0] = renderer->font()->get("fonts/trebuc.ttf:14");
	font[0]->create(renderer, vs);


	sprite = renderer->mesh()->create();

	sprite->addIndex(0);
	sprite->addIndex(1);
	sprite->addIndex(3);
	sprite->addIndex(3);
	sprite->addIndex(1);
	sprite->addIndex(2);

	MeshStream * stream = sprite->createSingleStream("Glyph", sizeof(Vertex));
	
	Vertex v[4];
	v[0].position = Vector2(0, 0);
	v[0].uv = v[0].position;
	v[1].position = Vector2(1, 0);
	v[1].uv = v[1].position;
	v[2].position = Vector2(1, 1);
	v[2].uv = v[2].position;
	v[3].position = Vector2(0, 1);
	v[3].uv = v[3].position;

	stream->append((char*)v, 4);

	sprite->submit(vs);
	sprite->setTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	material.blend = Color(1, 1, 1, 1);
}

void Graphics::drawSprite(const char * texture, float x, float y, float width, float height)
{
	if (texture)
	{
		Renderable renderable(sprite, renderer->texture()->get(std::string("textures/") + texture), NULL);
		renderable.setMaterial(&material);
		Model model(renderable);
		model.getTransform().scale(Vector3(width, height, 1));
		model.getTransform().translate(Vector3(x, y, 0));

		overlay.intermediate(&model);
	}
}

void Graphics::drawText(const char * string, float x, float y)
{
	font[0]->draw(&overlay, string, floor(x), floor(y));
}