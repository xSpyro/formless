
#ifndef SHAPE_RESULT_H
#define SHAPE_RESULT_H

#include "ShapeNode.h"

#include "ShapeEffectData.h"

struct ShapeResult
{
	struct Node
	{
		float cache[SHAPE_BRANCHES_MAX];
		Vector3 position;
		float count;
	};


	Node nodes[SHAPE_NODES_MAX];

	int count;

	// this is just extra info
	ShapeEffectData effect;

	ShapeResult()
	{  
		count = 0;
	}
};

#endif	