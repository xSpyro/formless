
#ifndef PAINT_TEXTURE_H
#define PAINT_TEXTURE_H

#include "Common.h"

class PaintTexture
{
public:

	void create(Renderer * renderer, int width, int height);

	void load(const std::string & fname);
	
	Texture * getTexture() const;
	int getWidth() const;
	int getHeight() const;

	Surface & begin();

	void update();

private:

	void makePaintable();

private:

	Renderer * renderer;

	bool paintable;

	int width;
	int height;

	Surface surface;
	Texture * texture;
};

#endif