
#include "ShapeNode.h"

void ShapeNode::addBranch(int node)
{
	if (node >= SHAPE_NODES_MAX - 1)
		return;

	branches.push_back(node);

	Branches::iterator it = branches.begin();
	for (unsigned int i = 0; i < SHAPE_BRANCHES_MAX; ++i)
	{
		if (cache[i] == -1)
		{
			cache[i] = node;
			return;
		}
		cache[i] = *it;

		++it;

		if (it == branches.end())
			it = branches.begin();
	}
}

void ShapeNode::removeBranch(int node)
{
	std::vector<unsigned int> removable;


	// find all branches to this node
	for(unsigned int i = 0; i < branches.size();  i++)
	{
		if(branches[i] == node)
			removable.push_back(i);
	}
	// remove them completely
	for(unsigned int i = 0; i < removable.size(); i++)
	{
		branches.erase(branches.begin() + removable[i]);

		for(unsigned int j = i+1; j < removable.size(); j++)
		{
			if(removable[j] > removable[i])
			{
				--removable[j];
			}
		}
	}


	//Branches::iterator it = branches.begin();
	for (unsigned int i = 0; i < SHAPE_BRANCHES_MAX; ++i)
	{
		if(cache[i] == node)
			cache[i] = -1;

		
		//++it;

		//if (it == branches.end())
			//it = branches.begin();
	}


}

void ShapeNode::clear()
{
	branches.clear();
	for(int i = 0; i < SHAPE_BRANCHES_MAX; i++)
	{
		cache[i] = -1;
	}

}