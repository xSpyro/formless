
#include "ShapeAnimationInst.h"

#include "ShapeAnimation.h"

ShapeAnimationInst::ShapeAnimationInst()
	: com(NULL)
{
	reset();
}

bool ShapeAnimationInst::changeShape(ShapeAnimation * animation)
{
	if (!animation)
		return false;

	// if we have a previous animation
	// morph between them
	if (this->animation && this->animation != animation)
	{
		// get a key for the currently animated result
		this->animation->getAnimationKey(frame).createLerpedAnimationKey(&from);
		this->animation = animation;

		morph = true;
		frame = 0;

		return true;
	}
	else
	{
		// change the animation shape
		this->animation = animation;
		frame = 0;
		next = 0;

		return true;
	}
}

bool ShapeAnimationInst::jumpShape(ShapeAnimation * animation)
{
	// change the animation shape
	this->animation = animation;
	frame = 0;
	next = 0;

	return true;
}

bool ShapeAnimationInst::executeShape(ShapeAnimation * animation)
{
	if (animation == NULL)
		return false;

	// store old
	if (previous == NULL)
		previous = this->animation;

	return changeShape(animation);
}

ShapeFrame ShapeAnimationInst::getFrame() const
{
	if (animation)
	{
		if (morph)
		{
			return animation->getAnimationKeyMorph(&from, frame / 10.0f);
		}
		else
		{
			return animation->getAnimationKey(frame);
		}
	}
	else
	{
		return ShapeFrame();
	}
}

void ShapeAnimationInst::update()
{
	if (animation)
	{
		next += speed;

		while (next >= 1.0f)
		{
			++frame;
			next -= 1.0f;
		}
		
		if (morph)
		{
			if (frame >= 10)
			{
				frame = 0;
				morph = false;
			}
		}
		else
		{
			if (animation->isEnd(frame))
			{
				// if previous is stored
				if (previous)
				{
					animation = previous;
					previous = NULL;
				}

				changeShape(animation);
			}
		}
	}
}

void ShapeAnimationInst::jump(unsigned int frame)
{
	this->frame = frame;
}

void ShapeAnimationInst::addSpeed(float speed)
{
	this->speed += speed;

	this->speed = std::min(std::max(this->speed, 0.01f), 50.0f);
}

void ShapeAnimationInst::setSpeed(float s)
{
	speed = s;
}

void ShapeAnimationInst::reset()
{
	animation = NULL;
	previous = NULL;
	frame = 0;
	next = 0;
	speed = 1.0f;

	morph = false;
}

void ShapeAnimationInst::debug() const
{
	if (animation)
	{
		std::cout << "anim size: " << animation->getSize() << std::endl;
	}
}