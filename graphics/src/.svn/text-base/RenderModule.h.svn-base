
#ifndef RENDER_MODULE_H
#define RENDER_MODULE_H

#include "Common.h"

#include "PlayerVisual.h"
#include "SceneShaderState.h"
#include "SceneLightBuffer.h"
#include "ScenePlayerBuffer.h"
#include "Terrain.h"

#include "ShapeAnimation.h"

#include "PaintTexture.h"

//#include "ParticleSystem.h"

//class ParticleSystem;

#include "AnimationComponent.h"

enum RenderModuleFlags
{
	RM_TERRAIN = 1,
	RM_PARTICLES = 2,
	RM_SKY = 4,
	RM_SHADOWS = 8,
	RM_OBJECTS = 16,
	RM_ALL = 31,
	RM_EFFECTS = 64
};

class BaseModule;

class RenderModule
{

	typedef std::list < BaseModule* > Modules;

public:

	RenderModule();

	void init(Renderer * renderer, int flags = RM_ALL);
	void initModTools();

	void renderToScreen(bool state);
	Texture * getOffscreenTexture() const;

	Renderer * getRenderer() const;

	Camera * getCamera();
	void updateCamera(const Camera & camera);

	void setHudMode();

	void build();

	void bind();
	void bindHud();

	void clean();

	void update();
	void preFrame();

	void addObject(RenderComponent * rendcomp);
	void addLinkedParticle(AnimationComponent * animcomp);

	Surface & getTerrainSurface();
	Terrain & getTerrain();
	Layer * getTerrainLayer();
	Layer * getCharacterLayer();
	Layer * getLinkedParticleLayer();
	void updateTerrain();

	void clearPlayers();
	void addPlayer(const ScenePlayerBuffer::Entity & data);

	void clearLights();
	void addLight(const SceneLightBuffer::Entity & data);

	void spawnEffect(const std::string & effect, const Vector3 & position, const Vector3 & normal);

	void setPlayerPosition(const Vector3 & pos);
	void setCursorPosition(const Vector3 & pos);

	void setVisualScale(float scale);

	PaintTexture & getBlendTexture();

	int getHeightmapSize();

	const Camera & getShadowCamera() const;

	void setParticleSystem(ParticleSystem * ps);

	ShapeAnimation * generateHudAnimation();
	ShapeAnimation * generateHudLifeAnimation();
	ShapeAnimation * generateHudManaAnimation();


	void addCircleOfLinks();
	void addCenterPowerLink();

	void adjustCircleToTerrain();

	unsigned int getLinks();
	const Vector3 & getLinkPosition(unsigned int index);

	void clearAutoScenery();


	// intermediate drawing
	void setDrawTexture(Texture * texture);
	void setDrawTexture(const std::string & texture);
	void drawSprite(float x, float y, float width, float height);
	void drawSpriteAdd(float x, float y, float width, float height);
	void drawSpritePart(float x, float y, float width, float height, float u1, float v1, float u2, float v2);
	void drawSpriteRot(float x, float y, float width, float height, float rot);
	void drawShinySprite(float x, float y, float width, float height, float shininess);
	void drawShine(float x, float y, float size);
	void drawShineExt(const std::string & texture, float x, float y, float size);
	void drawSpriteForceScreen(float x, float y, float width, float height);

	void drawLine(float x1, float y1, float x2, float y2);

	void setBlendColor(const Color & color);

	void setGhostMode(bool state);

	void setFog(bool state);
	void setShadows(bool state);
	void setSSAO(bool state);

	void setBoundingVolumesVisibility(bool state);

	const bool getFog() const;
	const bool getShadows() const;
	const bool getSSAO() const;
	const float getVisualScale() const;
	const bool getBoundingVolumesVisibility() const;
	float getHeightFromTerrain(const Vector2 & position);

	bool addParticleType(std::string type, std::string texture);

	std::string getDebugText() const;

private:

	void initIntermediateDrawing();

	void createHeightMap();
	void createSkySphere();

	void updateShaderState();
	void updateLightBuffer();
	void updatePlayerBuffer();

private:

	int flags;
	
	// effect system
	ParticleSystem * effectSystem;
	Layer * effectLayer;

	Renderable effectRenderable;


	int width;
	int height;

	bool renderScreen;
	Texture * offscreenTexture;

	Renderer * renderer;
	Timer timer;

	Camera		cameraSource;
	Camera *	camera;

	Model		heightmapModel;
	Mesh *		heightmapMesh;
	Terrain		heightmapTerrain;

	Model		skyModel;

	Model		tempModel;	

	Layer *		heightmapLayer;
	Layer *		characterLayer;
	Layer *		particleLayer;
	Layer *		skyLayer;

	Layer *		wireframeLayer;

	// visual
	ConstantBuffer * playerData;
	PlayerVisual visualBuffer;


	PaintTexture brushTexture;


	// utility
	Texture * noiseTexture;

	// post processing
	Texture * postTexture;
	Texture * gBufferTexture[2];
	Texture * ssaoTexture;
	Texture * ssaoTextureHalf;
	Texture * lightTexture;
	Texture * distorsion;
	Texture * heatEffect;
	Texture * blurVerticalTexture;
	Texture * blurHorizontalTexture;

	Texture * renderTargetTexture;

	ConstantBuffer * cameraConstantBuffer;

	BaseShader * ssaoPixelShader;
	BaseShader * mixPixelShader;
	BaseShader * blurPixelShader;
	BaseShader * blur4PixelShader;
	BaseShader * lightPixelShader;
	BaseShader * wireframePixelShader;

	// Hdr
	BaseShader * blurVertical;
	BaseShader * blurHorizontal;

	// shadowmapping
	Layer * shadowLayer;
	Layer * shadowObjectLayer;
	Layer * shadowEffectLayer;
	Texture * shadowTexture;
	Texture * shadowAccTexture;
	Texture * shadowObjectTexture;
	Camera shadowCamera;

	// sun lighting
	Sun shaderState;
	ConstantBuffer * shaderStateBuffer;

	// deferred lighting
	SceneLightBuffer lightBuffer;
	ConstantBuffer * lightConstantBuffer;

	// effect visual
	ScenePlayerBuffer playerBuffer;
	ConstantBuffer * playerConstantBuffer;

	// particle effects
	Texture * particleTexture;
	Texture * particleTextureHalf[2];

	// debug scripting
	EventLua::State state;


	// hud data
	ShapeAnimation hudAnimation;
	ShapeAnimation hudLifeAnimation;

	ConstantBuffer * fontConstantBuffer;

	Camera hudCamera;

	Layer * ghostLayer;
	Layer * hudLayer;
	Layer * hudLayerAdd;
	Layer * postTransitionLayer;
	Viewport * hudViewport;

	AnimationComponent * hudLife;
	AnimationComponent * hudCore;

	Viewport * effects3D;
	Viewport * effects2D;

	bool ghostMode;

	typedef std::vector< Renderable > TypeRenderablesVector;
	TypeRenderablesVector typeRenderables;

	Renderable effectHeatRenderable;
	Layer * heatLayer;
	Texture * heatEffects;

	// intermediate drawing
	Mesh * sprite;
	Material material;
	Texture * whiteTexture;
	Texture * drawTexture;

	BaseShader * vertexShader2D;
	BaseShader * pixelShader2D;

	BaseShader * pixelShaderTransition;

	struct Vertex
	{
		Vector2 position;
		Vector2 uv;
	};



	// visual effects
	typedef std::vector < AnimationComponent > Links;
	Links links;
	AnimationComponent centerPowerLink;



	Texture * shineTexture;


	// modules
	Modules modules;
	
};


#endif