#include "ParticleCustomBehavior.h"

ParticleCustomBehavior::ParticleCustomBehavior()
{
	//ZeroMemory(this, sizeof(ParticleCustomBehavior));
	pi = 3.14159265f;
	radx = pi / 180.0f;

	dir = Vector3(0, 0, 0);  // sp
	normalW = Vector3(0.5f, 1, 0);
	normalW.normalize();
	//dirW = Vector3(0, 0, 0); // sp

	startColor  = Color(0, 0, 0, 1);
	middleColor = Color(0, 0, 0, 1);
	endColor    = Color(0, 0, 0, 1);

	randAngleX = 0.0f;
	randAngleY = 0.0f;
	randAngleZ = 0.0f;


	intensityDir = 0.0f;

	spinMax = 0.0f;
	spinMin = 0.0f;

	startScale = 0.0f;
	endScale   = 0.0f;

	startSpeed = 0.0f;
	middleSpeed = 0.0f;
	endSpeed   = 0.0f;

	colorPath = 1.0f;
	speedPath = 1.0f;

	gravity = 0.0f;

	distorsion = 0.0f;

	displacementPos = 0.0f;
	lifeAffectRate = 0.018f;

	memset(type_name, 0, 64);
}
ParticleCustomBehavior::~ParticleCustomBehavior()
{

}
void ParticleCustomBehavior::spawn(Particle & particle) const
{
	float r = random(0, 360*radx);
	float rposx = cos(r)*displacementPos,
		  rposz = sin(r)*displacementPos;
	particle.position = Vector3(rposx, 1, rposz);
	particle.angle    = spinMin;

	particle.life = 1.0f;

	float PI = 3.14159265f;
	float d = random(0, 2.92f);

	float i = random(-intensityDir*radx, intensityDir*radx);
	float x = sin(randAngleX*radx + i),
		  y = sin(randAngleY*radx),
		  z = sin(randAngleZ*radx + i);



	D3DXVECTOR3 tangent; 
	D3DXVECTOR3 temp_normal = D3DXVECTOR3(0, 1, 0),
		        normalL     = D3DXVECTOR3(x, y, z);

	D3DXVec3Normalize(&normalL, &normalL);

	D3DXVECTOR3 c1;
	D3DXVECTOR3 c2;

	D3DXVec3Cross(&c1, &temp_normal, & D3DXVECTOR3(0, 0, 1));
	D3DXVec3Cross(&c2, &temp_normal, & D3DXVECTOR3(0, 1, 0));
	FLOAT dot;
	D3DXVECTOR3 temp;

	if( D3DXVec3Length(&c1)>D3DXVec3Length(&c2) )
	{
		tangent = c1;	
	}
	else
	{
		tangent = c2;	
	}

	/*temp_normal = 2.0f * temp_normal;// - 1.0f;
	temp_normal.x -= 1.0f;
	temp_normal.y -= 1.0f;
	temp_normal.z -= 1.0f;*/

	D3DXVECTOR3 N = normalL;
	dot = D3DXVec3Dot(&tangent, &N);
	temp = N * dot;
	temp = tangent - temp;
	D3DXVECTOR3 T;
	D3DXVec3Normalize(&T, &temp);

	D3DXVECTOR3 B;
	D3DXVec3Cross(&B, &N, &T);

	D3DXMATRIX TBN;

	TBN(0, 0) = T.x; TBN(1, 0) = T.y; TBN(2, 0) = T.z; TBN(3, 0) = 0;
	TBN(0, 1) = B.x; TBN(1, 1) = B.y; TBN(2, 1) = B.z; TBN(3, 1) = 0;
	TBN(0, 2) = N.x; TBN(1, 2) = N.y; TBN(2, 2) = N.z; TBN(3, 2) = 0;
	TBN(0, 3) = 0;   TBN(1, 3) = 0;   TBN(2, 3) = 0;   TBN(3, 3) = 1;

	D3DXVECTOR3 normalR;
	D3DXVec3TransformNormal(&normalR, &temp_normal, &TBN);
	D3DXVec3Normalize(&normalR, &normalR);

	//float3 bumpedNormalW = normalize(mul(normalT, TBN));

	particle.velocity = Vector3(
		(x + random(-distorsion, distorsion))*startSpeed,
		(y + random(-distorsion, distorsion))*startSpeed,
		(z + random(-distorsion, distorsion))*startSpeed 
		);

	//particle.velocity = Vector3(x, y, z) * startSpeed;
	particle.scale = startScale;
	particle.color = startColor;
	particle.color.a = 1.0f;
}
void ParticleCustomBehavior::process(Particle & particle) const
{
	//particle.angle = -1;// 45 * radx;
	particle.angle = lerp(spinMin, spinMax, particle.life);

	Vector3 g(0, gravity, 0);
	float t = 1.0f - particle.life;
	float t2 = particle.life;
	float speed = lerp3(particle.velocity.length(), middleSpeed, endSpeed, t, speedPath);// + random(0, 0.5f);
	//speed = particle.velocity.length();
	//speed = startSpeed;//particle.velocity.length();
	particle.position.x += particle.velocity.x * speed * 0.1f;
	particle.position.y += particle.velocity.y * speed * 0.1f + (10.0f*gravity*t*t) / 2.0f;
	particle.position.z += particle.velocity.z * speed * 0.1f;

	particle.color.r = lerp3(endColor.r, middleColor.r, startColor.r, t2, colorPath);
	particle.color.g = lerp3(endColor.g, middleColor.g, startColor.g, t2, colorPath);
	particle.color.b = lerp3(endColor.b, middleColor.b, startColor.b, t2, colorPath);
	//std::cout << "Color: " << colorPath << "\n";
	particle.color.a = particle.life;

	particle.scale  = lerp(endScale, startScale, t2);

	particle.life -= lifeAffectRate + 0.0001f;//0.018f;
	//particle.life -= 0.018f;
}
void ParticleCustomBehavior::setNormal(const Vector3 & n)
{
	normalW = n;
}










