
#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "Common.h"

#include "LogicHandler.h"

#include "ShapeAnimationInst.h"
#include "ShapeAnimation.h"
#include "ShapeResult.h"
#include "ShapeFrame.h"


class Graphics
{
	struct Vertex
	{
		Vector2 position;
		Vector2 uv;
	};

	typedef std::vector < Object* > Objects;

public:

	Graphics();

	const ShapeResult * showFrame(LogicHandler * logic, int frame);
	void temp(float speed);

	void init(Renderer * renderer);

	void drawSprite(const char * texture, float x, float y, float width, float height);

	void drawText(const char * string, float x, float y);

private:

	void generateRandomFrame(int frame);
	void generateCircleFrame(int frame);

private:

	LogicHandler * logic;

	ShapeAnimation animation;
	ShapeAnimationInst inst;
	ShapeResult result;

	Objects objects;


	// graphics stuff

	Mesh * sprite;
	Material material;

	Font * font[3];

	Renderer * renderer;
	Layer overlay;
	Camera camera;
};

#endif