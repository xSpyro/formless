
#include "PaintTexture.h"

void PaintTexture::create(Renderer * renderer, int width, int height)
{
	this->renderer = renderer;
	this->width = width;
	this->height = height;

	paintable = true;

	texture = renderer->texture()->create(width, height, TEXTURE_WRITE);

	surface.create(width, height);
}

void PaintTexture::load(const std::string & fname)
{
	if (paintable)
	{
		// delete old
		renderer->texture()->free(texture);
		texture = renderer->texture()->get(fname);
		paintable = false;
	}
}

Texture * PaintTexture::getTexture() const
{
	return texture;
}

int PaintTexture::getWidth() const
{
	return width;
}

int PaintTexture::getHeight() const
{
	return height;
}

void PaintTexture::makePaintable()
{
	if (!paintable)
	{
		renderer->texture()->free(texture);
		texture = renderer->texture()->create(width, height, TEXTURE_WRITE);
		paintable = true;
	}
}

Surface & PaintTexture::begin()
{
	makePaintable();

	return surface;
}

void PaintTexture::update()
{
	if (paintable)
	{
		texture->render(surface);
	}
}