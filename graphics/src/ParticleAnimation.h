#ifndef PARTICLE_ANIMATION_H
#define PARTICLE_ANIMATION_H

#include "ParticleCustomBehavior.h"
//#include "Editor.h"

#include <vector>

class ParticleAnimation
{

public:
	ParticleAnimation();
	~ParticleAnimation();

	void shutDown();

	void initBehavior(int id);

	struct EMITTER_BEHAVIOR_PACKAGE
	{
		Vector3 randAngle;
		Vector3 spawnBox;

		int emitterType;
		int life;

		float intensityDir;
	
		float diffusion;
		float spinMin;
		float spinMax;
		float spawnRate;
		float affectRate;
		float startScale;
		float endScale;
		float startSpeed;
		float middleSpeed;
		float endSpeed;
		float colorPath;
		float speedPath;
		float gravity;
		float distorsion;

		float displacementPos;
		float lifeAffectRate;

		Color startColor;
		Color middleColor;
		Color endColor;

		//std::string behave_name;
		char behave_name[64];
		
		ParticleCustomBehavior * behavior;
		ParticleEmitter        * emitter;

		char type_name[64];
		char texture_name[64];
		EMITTER_BEHAVIOR_PACKAGE()
		{
			memset(behave_name, 0, 64);
			memset(type_name, 0, 64);
			randAngle = Vector3(0, 0, 0);
			intensityDir = 0.0f;
			spawnBox  = Vector3(0, 0, 0);

			gravity = 0.0f;
			distorsion = 0.0f;

			emitterType = 1;
			life = 0;
	
			diffusion = 0.0f;
			spinMin = 0.0f;
			spinMax = 0.0f;
			spawnRate = 0.0f;
			affectRate = 0.0f;
			startScale = 0.0f;
			endScale = 0.0f;
			startSpeed = 0.0f;
			middleSpeed = 0.0f;
			endSpeed = 0.0f;
			startColor  = Color(0.0f, 0.0f, 0.0f, 1.0f);
			middleColor = Color(0.0f, 0.0f, 0.0f, 1.0f);
			endColor    = Color(0.0f, 0.0f, 0.0f, 1.0f);

			colorPath = 0.0f;
			speedPath = 0.0f;

			behavior = NULL;
			emitter  = NULL;

			displacementPos = 0.0f;
			lifeAffectRate = 0.018f;
		}
	};

	typedef std::vector< EMITTER_BEHAVIOR_PACKAGE > EmitterBehaviorPackageVector;

	EmitterBehaviorPackageVector effects;
	


private:



};

#endif


