
#ifndef SHAPE_ANIMATION_H
#define SHAPE_ANIMATION_H

#include "ShapeAnimationKey.h"
#include "ShapeFrame.h"

#include "ShapeAnimationInst.h"
#include "ShapeAnimation.h"
#include "ShapeResult.h"


class ShapeAnimation
{

	typedef std::map < unsigned int, ShapeAnimationKey > Frames;

public:

	ShapeAnimation();

	void clear();

	unsigned int getSize() const;

	int addNode(unsigned int frame, int parent, const Vector3 & position);
	void moveNode(unsigned int frame, int node, const Vector3 & position);
	bool removeNode(unsigned int frame, int node);

	bool removeFrame(const unsigned int frame);
	bool recreateFrame(unsigned int frame, const ShapeResult * result);

	bool linkNode(unsigned int frame, int node, int target);
	bool unlinkNode(unsigned int frame, int target);

	ShapeFrame getAnimationKey(unsigned int frame) const;
	ShapeFrame getAnimationKeyMorph(const ShapeAnimationKey * from, float delta) const;

	void setFrameGravity(unsigned int frame, const float g);
	void setFrameSpeed(unsigned int frame, const float s);
	void setFrameDeath(unsigned int frame, const float d);

	const float getFrameGravity(unsigned int frame);
	const float getFrameSpeed(unsigned int frame);
	const float getFrameDeath(unsigned int frame);

	bool isEnd(unsigned int frame) const;

	bool exportToVector(std::vector < unsigned int > & frames, std::vector < ShapeResult > & results) const;

private:

	Frames::const_iterator getLeftBound(unsigned int pos) const;

	const ShapeAnimationKey * getFirstKey() const;
	const ShapeAnimationKey * getLastKey() const;

private:

	Frames frames;

	unsigned int length;
};

#endif