
#include "RenderModule.h"

#include "MemLeaks.h"
#include "RenderComponent.h"
#include "AnimationComponent.h"
#include "EffectComponent.h"

#include "Object.h"
#include "Events.h"

enum
{
	STAGE_RENDERSCREEN = 90
};

RenderModule::RenderModule()
	:wireframeLayer(NULL)
{
	effectSystem = NULL;
	camera = &cameraSource;

	renderScreen = true;

	drawTexture = NULL;

	setFog(true);
	setShadows(true);
	setSSAO(true);


}

void RenderModule::init(Renderer * renderer, int flags)
{
	typeRenderables.reserve(100);
	this->flags = flags;

	this->renderer = renderer;	

	width = renderer->getWidth();
	height = renderer->getHeight();

	shaderState.dimension = Vector2(1.0f / static_cast < float > (width), 1.0f / static_cast < float > (height));

	heightmapTerrain.scale = 1500.0f;
	heightmapTerrain.height = 0.1f;
	heightmapTerrain.offset = Vector3(0, 0, 0);

	// init layers and textures
	heightmapLayer		= renderer->createLayer();
	characterLayer		= renderer->createLayer();
	skyLayer			= renderer->createLayer();

	shadowLayer			= renderer->createLayer();
	shadowObjectLayer	= renderer->createLayer();
	shadowEffectLayer	= renderer->createLayer();

	heatLayer           = renderer->createLayer();  //------------------------------- Added by meeee

	shineTexture		= renderer->texture()->get("textures/shine.png");

	noiseTexture		= renderer->texture()->get("textures/noise.png");
	cameraConstantBuffer = new ConstantBuffer(renderer, 512);

	offscreenTexture	= renderer->texture()->create(width, height, TEXTURE_DEFAULT);
	offscreenTexture->setDepthAddon(renderer->getDepthTexture());

	// our color texture
	postTexture			= renderer->texture()->create(width, height, TEXTURE_DEFAULT);
	postTexture->setDepthAddon(renderer->getDepthTexture());

	// our gbuffer texture with normal data and depth
	gBufferTexture[0]	= renderer->texture()->create(width, height, TEXTURE_DEFAULT);
	gBufferTexture[0]->setClearColor(Color(0, 0, 0, 1));

	gBufferTexture[1]	= renderer->texture()->create(width, height, TEXTURE_DEFAULT);
	gBufferTexture[1]->setClearColor(Color(0, 0, 0, 0));

	// ssao grayscale texture
	ssaoTexture			= renderer->texture()->create(width / 1, height / 1, TEXTURE_GRAYSCALE);
	ssaoTextureHalf		= renderer->texture()->create(width / 2, height / 2, TEXTURE_GRAYSCALE);

	// deferred lighting
	lightTexture		= renderer->texture()->create(width / 2, height / 2, TEXTURE_DEFAULT);

	particleTexture		= renderer->texture()->create(width, height, TEXTURE_DEFAULT);
	particleTextureHalf[0]	= renderer->texture()->create(width / 2, height / 2, TEXTURE_DEFAULT);
	particleTextureHalf[1]	= renderer->texture()->create(width / 2, height / 2, TEXTURE_DEFAULT);

	// heat effect
	heatEffects = renderer->texture()->create(width / 4, height / 4, TEXTURE_GRAYSCALE); //------------------------------- Added by meee....

	particleTexture->setDepthAddon(renderer->getDepthTexture());

	ssaoPixelShader		= renderer->shader()->get("shaders/ssao.ps");
	mixPixelShader		= renderer->shader()->get("shaders/mix.ps");
	blurPixelShader		= renderer->shader()->get("shaders/blur.ps");
	blur4PixelShader	= renderer->shader()->get("shaders/blur4.ps");
	lightPixelShader	= renderer->shader()->get("shaders/light.ps");

	wireframePixelShader = renderer->shader()->get("shaders/wireframe.ps");

	distorsion = renderer->texture()->get("textures/normal_water.jpg"); //------------------------------- Added by meee....

	mixPixelShader->bindTexture(8, distorsion); //------------------------------- Added by meee....

	// constant buffers for deferred lighting
	lightConstantBuffer = New ConstantBuffer(renderer, 1024 * 5);
	playerConstantBuffer = New ConstantBuffer(renderer, 512);


	// and for multiple renderings
	renderTargetTexture = renderer->texture()->create(0, 0, TEXTURE_MULTI);

	renderTargetTexture->addMultiTexture(gBufferTexture[0]);
	renderTargetTexture->addMultiTexture(gBufferTexture[1]);

	//camera = &shadowCamera;

	Viewport * vp	= heightmapLayer->addViewport(camera, renderTargetTexture, VIEWPORT_SCREEN_AND_TEXTURE);
	BaseShader * ps = renderer->shader()->get("shaders/heightmap.ps");
	ps->setSamplers();


	// create constant buffers for player visuals
	playerData = New ConstantBuffer(renderer, 512);

	vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, ps);


	// sun
	shaderStateBuffer = New ConstantBuffer(renderer, 512);
	//


	// WIREFRAME DEBUG
	{
		wireframeLayer = renderer->createLayer();
		Viewport * vp = wireframeLayer->addViewport(camera, NULL);

		vp->getEffect()->blendModeSolid();
		vp->getEffect()->blendModeDebug();
		vp->setShaders(renderer->shader()->get("shaders/wireframe.vs"), NULL, wireframePixelShader);

		wireframeLayer->setVisible(false);
	}
	

	// shadowmapping

	{
		shadowAccTexture = renderer->texture()->create(512, 512, TEXTURE_GRAYSCALE_WITH_DEPTH);
		shadowTexture = shadowAccTexture->getDepthAddon();

		Viewport * vp = shadowLayer->addViewport(&shadowCamera, shadowAccTexture);

		BaseShader * ps = renderer->shader()->get("shaders/particle_shadow.ps");

		vp->getEffect()->blendModeDepthWithAdd();
		vp->setShaders(renderer->shader()->get("shaders/particle.vs"), renderer->shader()->get("shaders/particle.gs"), ps);

		{
			Viewport * vp = shadowEffectLayer->addViewport(&shadowCamera, shadowAccTexture);
			BaseShader * ps = renderer->shader()->get("shaders/cpu_particle_shadow.ps");

			vp->getEffect()->blendModeDepthWithAdd();
			vp->setShaders(renderer->shader()->get("shaders/cpu_particle.vs"), renderer->shader()->get("shaders/cpu_particle.gs"), ps);
		}
	}

	{
		shadowObjectTexture = renderer->texture()->create(1024, 1024, TEXTURE_DEPTH);

		Viewport * vp = shadowObjectLayer->addViewport(&shadowCamera, shadowObjectTexture);

		vp->getEffect()->blendModeSolid();
		vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, NULL);
	}

	// assign to heightmap shader
	ps->bindTexture(8, shadowTexture);
	ps->bindTexture(7, shadowAccTexture);
	ps->bindTexture(6, shadowObjectTexture);

	

	// bind particle layer
	{	
		BaseShader * vs = renderer->shader()->get("shaders/particle.vs");
		BaseShader * gs = renderer->shader()->get("shaders/particle.gs");
		BaseShader * ps = renderer->shader()->get("shaders/particle.ps");


		gs->setConstantBuffer(3, shaderStateBuffer);
		//ps->setConstantBuffer(3, shaderStateBuffer);
		ps->bindTexture(6, shadowObjectTexture);

		ps->setConstantBuffer(0, cameraConstantBuffer);

		particleLayer = renderer->createLayer();
		Viewport * vp = particleLayer->addViewport(camera, renderTargetTexture, VIEWPORT_SCREEN_AND_TEXTURE);

		vp->getEffect()->blendModeDepthWithAlpha();
		vp->setShaders(vs, gs, ps);
		//vp->setSorting(1);
	}
	


	{
		// render our objects to both the current framebuffer and the gBufferTexture
		// render normals to the gbuffer

		Viewport * vp = characterLayer->addViewport(camera, renderTargetTexture, VIEWPORT_SCREEN_AND_TEXTURE);
		vp->getEffect()->blendModeSolid();

		BaseShader * ps = renderer->shader()->get("shaders/obj.ps");

		vp->getEffect()->blendModeSolid();
		vp->setShaders(renderer->shader()->get("shaders/obj.vs"), NULL, ps);


		// assign to heightmap shader
		ps->bindTexture(6, shadowObjectTexture);
	}

	if (flags & RM_EFFECTS)
	{
		effectSystem->init(renderer);
		effectSystem->createType("basic", renderer->shader()->get("shaders/cpu_particle.vs"));
		effectSystem->createType("heat", renderer->shader()->get("shaders/cpu_particle.vs"));

		BaseShader* vshader = NULL;
		vshader = renderer->shader()->get("shaders/cpu_particle.vs");
		BaseShader* gshader = NULL;
		gshader = renderer->shader()->get("shaders/cpu_particle.gs");
		BaseShader* pshader = NULL;
		pshader = renderer->shader()->get("shaders/cpu_particle.ps");


		gshader->setConstantBuffer(3, shaderStateBuffer);


		effectLayer = renderer->createLayer();

		//Viewport * vp = effectLayer->addViewport(camera, renderTargetTexture, VIEWPORT_SCREEN_AND_TEXTURE);
		effects3D = effectLayer->addViewport(camera, NULL);
		effects2D = effectLayer->addViewport(&hudCamera, NULL);

		//vp->getEffect()->blendModeDepthWithAlpha();
		effects3D->getEffect()->blendModeParticle();
		effects3D->setShaders(vshader, gshader, pshader);

		effects2D->getEffect()->blendModeParticle();
		effects2D->setShaders(vshader, gshader, pshader);

		Mesh * mesh = effectSystem->getParticleMesh("basic");                                               // changed  maybe i dont know
		Mesh * effect_mesh = effectSystem->getParticleMesh("heat");                                         // Added by meee....

		effectRenderable = Renderable(mesh, renderer->texture()->get("textures/cpu_parts.png"), NULL);
		effectHeatRenderable = Renderable(effect_mesh, renderer->texture()->get("textures/cpu_parts.png"), NULL);  // Added by meee....
		
		effectLayer->add(&effectRenderable);
		heatLayer->add(&effectHeatRenderable);                                                             // Added by meee....
		shadowEffectLayer->add(&effectRenderable);
		
		{
			Viewport * vp = heatLayer->addViewport(camera, heatEffects);
			vp->getEffect()->blendModeParticle();
			vp->setShaders(vshader, gshader, pshader);
		}
	}

	// Hdr grayscale - for tr0ll, by tr0ll, from tr0llr�sk
	blurVertical = NULL;
	blurVertical = renderer->shader()->get("shaders/blur_vertical.ps");
	blurHorizontal = NULL;
	blurHorizontal = renderer->shader()->get("shaders/blur_horizontal.ps");
	// HUD
	{
		hudCamera.setProjection2D(1280, 720);
		hudCamera.update();

		hudLife = New AnimationComponent(NULL);
		hudCore = New AnimationComponent(NULL);

		hudLayer = renderer->createLayer();
		hudLayerAdd = renderer->createLayer();
	
		ghostLayer = renderer->createLayer();
		

		initIntermediateDrawing();
		
		BaseShader * vs = renderer->shader()->get("shaders/particle.vs");
		BaseShader * gs = renderer->shader()->get("shaders/hud_particle.gs");
		BaseShader * ps = renderer->shader()->get("shaders/hud_particle.ps");

		{
			Viewport * vp = ghostLayer->addViewport(&hudCamera, NULL);
			vp->getEffect()->blendModeAdd();
			vp->setShaders(vs, gs, ps);
		}
		
		{
			hudViewport = hudLayer->addViewport(&hudCamera, NULL);
			hudViewport->getEffect()->blendModeAlpha();
			hudViewport->setShaders(vertexShader2D, NULL, pixelShader2D);

			Viewport * vpadd = hudLayerAdd->addViewport(&hudCamera, NULL);
			vpadd->getEffect()->blendModeAdd();
			vpadd->setShaders(vertexShader2D, NULL, pixelShader2D);
		}



		//generateHudAnimation();
		//generateHudLifeAnimation();

		//hudCore->load(renderer, NULL);
		//hudCore->startShape(&hudAnimation);

		//hudLife->load(renderer, NULL);
		//hudLife->startShape(&hudLifeAnimation);

		//hudLayer->add(hudLife->getRenderable());
		//ghostLayer->add(hudCore->getRenderable());
	}






	// end

	initModTools();


	// default values
	setVisualScale(20);

	setCursorPosition(Vector3(-999, -999, -999));
	setPlayerPosition(Vector3(-999, -999, -999));



	setGhostMode(false);




	blurHorizontalTexture = renderer->texture()->create(width / 4, height / 4, TEXTURE_GRAYSCALE);
	blurVerticalTexture = renderer->texture()->create(width / 2, height / 2, TEXTURE_GRAYSCALE);
}

void RenderModule::initModTools()
{
	brushTexture.create(renderer, 256, 256);
}

void RenderModule::renderToScreen(bool state)
{
	if (state == renderScreen)
		return;

	renderScreen = state;

	renderer->clearStage(STAGE_RENDERSCREEN);

	// render to screen
	if (renderScreen)
	{
		renderer->insertStateChange(STAGE_RENDERSCREEN, NULL);
		hudViewport->setTarget(NULL);

		//renderer->setRenderTarget(NULL);
	}
	else
	{
		renderer->insertStateChange(STAGE_RENDERSCREEN, offscreenTexture);	
		hudViewport->setTarget(offscreenTexture);

		//renderer->setRenderTarget(offscreenTexture);
	}
}

Texture * RenderModule::getOffscreenTexture() const
{
	return offscreenTexture;
}

Renderer * RenderModule::getRenderer() const
{
	return renderer;
}

Camera * RenderModule::getCamera()
{
	return &cameraSource;
}

void RenderModule::updateCamera(const Camera & camera)
{
	cameraSource = camera;
}

void RenderModule::setHudMode()
{
	cameraSource = hudCamera;
}

void RenderModule::build()
{
	if (flags & RM_TERRAIN)
		createHeightMap();

	//if (flags & RM_SKY)
	//	createSkySphere();
}

void RenderModule::bind()
{
	renderer->insertStateChange(0, postTexture);

	if (flags & RM_TERRAIN)
	{
		renderer->insertLayer(1, shadowLayer);
		renderer->insertLayer(2, shadowObjectLayer);
		renderer->insertLayer(3, shadowEffectLayer);
		renderer->insertLayer(11, heightmapLayer);

		BaseShader * ps = renderer->shader()->get("shaders/heightmap.ps");

		ps->setConstantBuffer(2, playerData);
		ps->setConstantBuffer(3, shaderStateBuffer);
		ps->setConstantBuffer(4, playerConstantBuffer);
	}

	/*
	if (flags & RM_SKY)
	{
		renderer->insertLayer(4, skyLayer);
	}
	*/

	if (flags & RM_OBJECTS)
	{
		renderer->insertLayer(15, characterLayer);
		
		BaseShader * ps = renderer->shader()->get("shaders/obj.ps");

		ps->setConstantBuffer(2, playerData);
		ps->setConstantBuffer(3, shaderStateBuffer);
	}

	if (flags & RM_EFFECTS)
	{
		effects3D->setVisible(true);
		effects2D->setVisible(false);

		renderer->clearStage(1200);

		renderer->insertLayer(17, effectLayer);
		renderer->insertLayer(18, heatLayer);

		BaseShader * ps = renderer->shader()->get("shaders/cpu_particle.ps");

		ps->setConstantBuffer(2, playerData);
		ps->setConstantBuffer(3, shaderStateBuffer);
	}

	if (flags & RM_PARTICLES)
	{
		// CHANGED
		//renderer->insertLayer(19, particleLayer);

		renderer->insertStateChange(20, particleTexture);




		renderer->insertLayer(21, particleLayer);
		
		/*
		renderer->insertStateChange(22, particleTextureHalf[0]);
		renderer->insertOverlay(23, particleTexture, renderer->shader()->get("shaders/blur4_v.ps"));

		renderer->insertStateChange(24, particleTextureHalf[1]);
		renderer->insertOverlay(25, particleTextureHalf[0], renderer->shader()->get("shaders/blur4_h.ps"));
		*/

		//renderer->insertStateChange(26, particleTextureHalf[0]);
	}

	//
	//


	

	renderer->insertStateChange(27, postTexture);

	

	renderer->insertLayer(28, ghostLayer);
	renderer->insertLayer(29, hudLayer);

	
	renderer->insertStateChange(35, blurHorizontalTexture);
	renderer->insertOverlay(36, gBufferTexture[1], blurHorizontal);
	blurHorizontal->setConstantBuffer(3, shaderStateBuffer);
	
	renderer->insertStateChange(37, blurVerticalTexture);
	renderer->insertOverlay(38, blurHorizontalTexture, blurVertical ); 
	blurVertical->setConstantBuffer(3, shaderStateBuffer);
	
	

	
	// render deferred lighting to texture
	renderer->insertStateChange(40, lightTexture);

	
	lightPixelShader->setConstantBuffer(3, shaderStateBuffer);
	lightPixelShader->setConstantBuffer(5, lightConstantBuffer);
	lightPixelShader->setConstantBuffer(0, cameraConstantBuffer);
	renderer->insertOverlay(45, renderTargetTexture, lightPixelShader);

	/*
	if (shaderState.state[10] > 0.5f)
	{
	
		// render to ssao texture
		renderer->insertStateChange(50, ssaoTexture);

		ssaoPixelShader->bindTexture(1, noiseTexture);
		ssaoPixelShader->setConstantBuffer(0, cameraConstantBuffer);
		ssaoPixelShader->setConstantBuffer(3, shaderStateBuffer);
		renderer->insertOverlay(55, gBufferTexture[0], ssaoPixelShader);


		// render ssao to blur target
		renderer->insertStateChange(70, ssaoTextureHalf);

		blurPixelShader		= renderer->shader()->get("shaders/blur.ps");
		renderer->insertOverlay(75, ssaoTexture, blurPixelShader);

	}
	*/

	


	renderer->clearStage(STAGE_RENDERSCREEN);

	// render to screen
	if (renderScreen)
	{
		std::cout << "render to screen" << std::endl;
		renderer->insertStateChange(STAGE_RENDERSCREEN, NULL);
	}
	else
	{
		std::cout << "render to texture" << std::endl;
		renderer->insertStateChange(STAGE_RENDERSCREEN, offscreenTexture);
	}


	mixPixelShader->setConstantBuffer(0, cameraConstantBuffer);
	mixPixelShader->setConstantBuffer(2, playerData);
	mixPixelShader->setConstantBuffer(3, shaderStateBuffer);
	mixPixelShader->setConstantBuffer(4, playerConstantBuffer);


	//mixPixelShader->bindTexture(1, ssaoTextureHalf);
	mixPixelShader->bindTexture(2, lightTexture);
	mixPixelShader->bindTexture(3, particleTexture);
	mixPixelShader->bindTexture(4, gBufferTexture[0]);
	mixPixelShader->bindTexture(5, gBufferTexture[1]);
	mixPixelShader->bindTexture(6, renderer->texture()->get("textures/texture_playervisual.png"));
	mixPixelShader->bindTexture(7, blurVerticalTexture );

	mixPixelShader->bindTexture(8, renderer->texture()->get("textures/normal_water.jpg"));
	mixPixelShader->bindTexture(9, heatEffects);



	// this mixes the color texture with the ssao shading
	renderer->insertOverlay(100, postTexture, mixPixelShader);


	//BaseShader * debugPixelShader = renderer->shader()->get("shaders/depth.ps");
	// debug
	//renderer->insertOverlay(110, shadowObjectTexture, debugPixelShader);
	//renderer->insertOverlay(110, brushTexture.getTexture(), NULL);


	
	renderer->insertLayer(200, wireframeLayer);
	
	
}

void RenderModule::bindHud()
{
	if (flags & RM_EFFECTS)
	{
		effects3D->setVisible(false);
		effects2D->setVisible(true);

		renderer->insertLayer(1200, effectLayer);
		//renderer->insertLayer(23, heatLayer);

		BaseShader * ps = renderer->shader()->get("shaders/cpu_particle.ps");

		ps->setConstantBuffer(2, playerData);
		ps->setConstantBuffer(3, shaderStateBuffer);
	}
}

void RenderModule::clean()
{
	particleLayer->clear();
	shadowLayer->clear();
	shadowObjectLayer->clear();

	wireframeLayer->clear();

	renderer->clearLayers();
}

void RenderModule::update()
{
	camera->updateTimeFrame(timer.now());
	shadowCamera.updateTimeFrame(timer.now());

	visualBuffer.delta = timer.now();
	playerData->set(&visualBuffer, sizeof(visualBuffer));

	updateShaderState();

	// temp light update
	updateLightBuffer();
	updatePlayerBuffer();

	if (effectSystem)
	{
		effectSystem->update();
	}

	hudCamera.updateTimeFrame(timer.now());
	hudCamera.update();

	//hudLife->update();
	//hudCore->update();

	for (Links::iterator i = links.begin(); i != links.end(); ++i)
	{
		i->update();
	}

	centerPowerLink.update();
}

void RenderModule::preFrame()
{
	if (!renderScreen)
		offscreenTexture->clear();

	blurHorizontalTexture->clear();
	blurVerticalTexture->clear();
	shadowTexture->clear();
	shadowAccTexture->clear();
	shadowObjectTexture->clear();

	postTexture->clear();
	gBufferTexture[0]->clear();
	gBufferTexture[1]->clear();

	ssaoTexture->clear();
	ssaoTextureHalf->clear();

	particleTexture->clear();
	particleTextureHalf[0]->clear();
	particleTextureHalf[1]->clear();

	heatEffects->clear();
}

void RenderModule::addObject(RenderComponent * rendcomp)
{
	rendcomp->load(renderer);
	characterLayer->add(&rendcomp->model);

	wireframeLayer->add(&rendcomp->modelDebug);

	shadowObjectLayer->add(&rendcomp->model);
}

void RenderModule::addLinkedParticle(AnimationComponent * animcomp)
{
	animcomp->load(renderer, camera);

	particleLayer->add(animcomp->getRenderable());

	shadowLayer->add(animcomp->getRenderable());
}

Surface & RenderModule::getTerrainSurface()
{
	return heightmapTerrain.surface;
}

Terrain & RenderModule::getTerrain()
{
	return heightmapTerrain;
}

Layer * RenderModule::getTerrainLayer()
{
	return heightmapLayer;
}

Layer * RenderModule::getCharacterLayer()
{
	return characterLayer;
}

Layer * RenderModule::getLinkedParticleLayer()
{
	return particleLayer;
}

void RenderModule::updateTerrain()
{

	if (flags & RM_TERRAIN)
	{
		BaseShader * vs = renderer->shader()->get("shaders/obj.vs");

		HeightMap::Desc desc;
		desc.scale = heightmapTerrain.scale;
		desc.height = heightmapTerrain.height;
		desc.ccw = true;
		desc.offset = heightmapTerrain.offset.y; // Add support for all offsets?
		desc.surface = &heightmapTerrain.surface;
		desc.texscale = 1.0f;
		HeightMap::update(renderer, vs, &desc, heightmapMesh);

		brushTexture.update();
	}
}

void RenderModule::clearPlayers()
{
	playerBuffer.count = 0.0f;
}

void RenderModule::addPlayer(const ScenePlayerBuffer::Entity & data)
{

	if (playerBuffer.count >= 7.0f)
		return;

	playerBuffer.entity[static_cast < int > (playerBuffer.count)] = data;

	playerBuffer.count += 1.0f;
}

void RenderModule::clearLights()
{
	lightBuffer.count = 0.0f;
}

void RenderModule::addLight(const SceneLightBuffer::Entity & data)
{
	if (lightBuffer.count >= 126.0f)
		return;


	lightBuffer.entity[static_cast < int > (lightBuffer.count)] = data;

	lightBuffer.count += 1.0f;
}

void RenderModule::spawnEffect(const std::string & effect, const Vector3 & position, const Vector3 & normal)
{
	if (effectSystem)
	{
		effectSystem->spawnGroup(effect, position, normal);
	}
}

void RenderModule::setPlayerPosition(const Vector3 & pos)
{
	visualBuffer.position = pos;
}

void RenderModule::setCursorPosition(const Vector3 & pos)
{
	visualBuffer.cursor = pos;
}

void RenderModule::setVisualScale(float scale)
{
	visualBuffer.scale = scale;
}

PaintTexture & RenderModule::getBlendTexture()
{
	return brushTexture;
}

int RenderModule::getHeightmapSize()
{
	return static_cast<int> (heightmapTerrain.scale);
}

const Camera & RenderModule::getShadowCamera() const
{
	return shadowCamera;
}

void RenderModule::setParticleSystem(ParticleSystem * ps)
{
	effectSystem = ps;
}

void RenderModule::createHeightMap()
{
	BaseShader * vs = renderer->shader()->get("shaders/obj.vs");

	vs->setConstantBuffer(3, shaderStateBuffer);

	Texture* height = renderer->texture()->get("textures/heightmap.jpg", TEXTURE_READ);
	height->read(heightmapTerrain.surface);

	heightmapTerrain.surface.blur();

	HeightMap::Desc desc;
	desc.scale = heightmapTerrain.scale;
	desc.height = heightmapTerrain.height;
	desc.ccw = true;
	desc.offset = heightmapTerrain.offset.y; // Add support for all offsets?
	desc.surface = &heightmapTerrain.surface;
	desc.texscale = 1.0f;
	HeightMap::generate(renderer, vs, &desc, &heightmapMesh);

	Renderable rend(heightmapMesh, NULL, NULL);

	Texture * mt = renderer->texture()->create(0, 0, TEXTURE_MULTI);

	//mt->addMultiTexture(renderer->texture()->get("textures/checkered.png"));

	mt->addMultiTexture(renderer->texture()->get("textures/texture_playervisual.png"));
	mt->addMultiTexture(brushTexture.getTexture());
	mt->addMultiTexture(renderer->texture()->get("textures/texture_sand.png"));
	mt->addMultiTexture(renderer->texture()->get("textures/texture_metal.png"));
	mt->addMultiTexture(renderer->texture()->get("textures/texture_brick.png"));
	mt->addMultiTexture(renderer->texture()->get("textures/terrain.png"));

	rend.setTexture(mt);
	heightmapModel.add(rend);

	heightmapLayer->add(&heightmapModel);

	shadowObjectLayer->add(&heightmapModel);
}

void RenderModule::createSkySphere()
{
	BaseShader * vs = renderer->shader()->get("shaders/sky.vs");
	BaseShader * ps = renderer->shader()->get("shaders/sky.ps");


	// fix layer
	Viewport * vp = skyLayer->addViewport(camera, NULL);
	vp->getEffect()->blendModeDebug();
	vp->setShaders(vs, NULL, ps);


	Mesh * mesh;

	SkySphere::Desc desc;
	desc.scale = 1000;
	desc.wireframe = false;
	if (SkySphere::generate(renderer, vs, &desc, &mesh))
	{
		skyModel.add(Renderable(mesh, NULL, NULL));
	}


	skyLayer->add(&skyModel);
}

void RenderModule::updateShaderState()
{
	cameraConstantBuffer->set(camera->getShaderData(), camera->getShaderDataSize());



	Vector3 p = visualBuffer.position;

	// get center of screen
	Vector3 mid = (p + Vector3(0, 30, 30));


	p.y = 0;

	float x = -10;
	float y = 15;
	float z = -10;

	float lx = 0;
	float ly = 0;
	float lz = 0;

	float near = -420;
	float far = 550;

	float scale = 4.75f;

	
	if (state.execute("../RenderModule.lua"))
	{
		EventLua::Object g = state.getGlobals();
		x = g["x"].queryFloat();
		y = g["y"].queryFloat();
		z = g["z"].queryFloat();

		float lx = g["lx"].queryFloat();
		float ly = g["ly"].queryFloat();
		float lz = g["lz"].queryFloat();

		near = g["near"].queryFloat();
		far = g["far"].queryFloat();

		scale = g["scale"].queryFloat();

		shadowCamera.setPosition(Vector3(x, y, z) + mid);
		shadowCamera.setLookAt(Vector3(lx, ly, lz) + mid);

		shadowCamera.setClipRange(near, far);
		shadowCamera.setProjectionOrtho(-140 * scale, 140 * scale, 100 * -scale, 100 * scale);
	}
	else
	{
		shadowCamera.setPosition(Vector3(x, y, z) + mid);
		shadowCamera.setLookAt(Vector3(lx, ly, lz) + mid);

		shadowCamera.setClipRange(near, far);
		shadowCamera.setProjectionOrtho(-140 * scale, 140 * scale, 100 * -scale, 100 * scale);
	}





	float v = timer.now() * 0.1f;


	shadowCamera.update();


	
	memcpy(&shaderState, (void *)&shadowCamera.getViewProjectionMatrix(), sizeof(Matrix) * 3);
	shaderState.position = shadowCamera.getOrientation();
	shaderState.position.normalize();

	shaderState.strength = 1;//std::min(pow(std::max((temp.position.y + 10) / 20, 0.1f), 4), 1.0f);

	shaderState.color[0] = Color(1, 0.5f, 0.3f, 1);
	shaderState.color[1] = Color(0.9f, 0.8f, 0.4f, 1);
	//shaderState.screen_pos = shadowCamera.projectToScreenUV(shaderState.position);

	shaderState.screen_pos = camera->getClipRange();

	shaderStateBuffer->set((void *)&shaderState, sizeof(shaderState));
}

void RenderModule::updateLightBuffer()
{

	{
		SceneLightBuffer::Entity entity;
		entity.position = Vector3(0, 100, 0);
		entity.color = Vector3(0.15f, 0.5f, 0.3f);
		entity.radius = 0.2f;

		addLight(entity);
	}
	

	lightConstantBuffer->set(&lightBuffer, sizeof(lightBuffer));

	clearLights();
}

void RenderModule::updatePlayerBuffer()
{
	/*
	{
		ScenePlayerBuffer::Entity entity;
		entity.position = visualBuffer.cursor;
		entity.position.y = 2;
		entity.team = 0;


		addPlayer(entity);
	}
	*/

	playerConstantBuffer->set(&playerBuffer, sizeof(playerBuffer));

	clearPlayers();
}

ShapeAnimation * RenderModule::generateHudAnimation()
{
	hudAnimation.clear();


	unsigned int frame = 0;
	for (int i = 0; i < 4; ++i)
	{

		
		int last;
		//for (float r = 3 * (i + 1); r < 8 * (i + 2) * i; r += 3.0f)
		//{
			for (float a = -3.14f; a < 3.14f; a += 0.07f)
			{
				float x = cos(a) * (30 + (i * 1));
				float y = sin(a) * (20 + (i * 1));

				last = hudAnimation.addNode(i * 20, -2, Vector3(x, y, 0));
			}

			hudAnimation.linkNode(i * 20, last, 0);

			

			//break;
		//}

		//break;

	}

	frame = 4 * 20;

	for (int i = 4; i > 0; --i)
	{

		
		int last;
		//for (float r = 3 * (i + 1); r < 8 * (i + 2) * i; r += 3.0f)
		//{
			for (float a = -3.14f; a < 3.14f; a += 0.07f)
			{
				float x = cos(a) * (30 + (i * 1));
				float y = sin(a) * (20 + (i * 1));

				last = hudAnimation.addNode(frame + (4 - i) * 20, -2, Vector3(x, y, 0));
			}

			hudAnimation.linkNode(frame + (4 - i) * 20, last, 0);

			//break;
		//}

		//break;

	}

	return &hudAnimation;
}

ShapeAnimation * RenderModule::generateHudLifeAnimation()
{

	hudLifeAnimation.clear();

	for (float i = -28.0f; i < 0.0f; i += 0.5f)
	{
		hudLifeAnimation.addNode(0, -2, Vector3(i, 15 + sin(i) * 1.5f, 0)); 
		hudLifeAnimation.addNode(10, -2, Vector3(i, 15 + sin(i) * 1.5f, 0));
	}

	return &hudLifeAnimation;
}

void RenderModule::addCircleOfLinks()
{
	float dist = 450;
	float detail = 6.28f / 20.0f;


	links.reserve(100);

	for (float i = -3.14f; i < 3.14f; i += detail)
	{
		float x = cos(i) * dist;
		float y = sin(i) * dist;
		float h = getHeightFromTerrain(Vector2(x, y));

		float xn = cos(i + detail) * dist;
		float yn = sin(i + detail) * dist;
		float hn = getHeightFromTerrain(Vector2(xn, yn));

		Links::iterator v = links.insert(links.end(), AnimationComponent());

		//v->load(renderer, NULL);
		

		addLinkedParticle(&(*v));

		v->forceLink(Vector3(x, h + 60, y), Vector3(xn, hn + 60, yn));
		v->setBaseColor(Color(0.2f, 0.4f, 0.3f, 1));
	}
}

void RenderModule::addCenterPowerLink()
{
	addLinkedParticle(&centerPowerLink);

	centerPowerLink.forceLink(Vector3(0, 60, 0), Vector3(0, 600, 0));
	centerPowerLink.setBaseColor(Color(0.2f, 0.4f, 0.3f, 1));

	centerPowerLink.getModel()->setVisible(true);
}

void RenderModule::adjustCircleToTerrain()
{
	float dist = 450;
	float detail = 6.28f / 20.0f;


	Links::iterator link = links.begin();

	for (float i = -3.14f; i < 3.14f; i += detail)
	{
		float x = cos(i) * dist;
		float y = sin(i) * dist;
		float h = getHeightFromTerrain(Vector2(x, y));

		float xn = cos(i + detail) * dist;
		float yn = sin(i + detail) * dist;
		float hn = getHeightFromTerrain(Vector2(xn, yn));


		link->forceLink(Vector3(x, h + 60, y), Vector3(xn, hn + 60, yn));

		link++;
	}
}

unsigned int RenderModule::getLinks()
{
	return links.size();
}

const Vector3 & RenderModule::getLinkPosition(unsigned int index)
{
	return links.at(index).getForceLinkPosition();
}

void RenderModule::clearAutoScenery()
{
	links.clear();

	centerPowerLink.getModel()->detach();

	std::cout << "links cleared" << std::endl;
}

void RenderModule::setDrawTexture(Texture * texture)
{
	drawTexture = texture;
}

void RenderModule::setDrawTexture(const std::string & texture)
{
	drawTexture = renderer->texture()->get(std::string("textures/") + texture);
}

void RenderModule::drawSprite(float x, float y, float width, float height)
{
	Texture * useTexture = drawTexture;

	if (useTexture == NULL)
		useTexture = whiteTexture;

	Renderable renderable(sprite, useTexture, NULL);
	renderable.setMaterial(&material);
	Model model(renderable);
	model.getTransform().scale(Vector3(width, height, 1));
	model.getTransform().translate(Vector3(x, y, 0));

	hudLayer->intermediate(&model);
}

void RenderModule::drawSpriteAdd(float x, float y, float width, float height)
{
	Texture * useTexture = drawTexture;

	if (useTexture == NULL)
		useTexture = whiteTexture;

	Renderable renderable(sprite, useTexture, NULL);
	renderable.setMaterial(&material);
	Model model(renderable);
	model.getTransform().scale(Vector3(width, height, 1));
	model.getTransform().translate(Vector3(x, y, 0));

	hudLayerAdd->intermediate(&model);
}

void RenderModule::drawSpritePart(float x, float y, float width, float height, float u1, float v1, float u2, float v2)
{
	float offset[4] = {u1, v1, u2, v2};
	fontConstantBuffer->set(offset, sizeof(offset));

	drawSprite(x, y, width, height);

	{
		float offset[4] = {0};
		fontConstantBuffer->set(offset, sizeof(offset));
	}
}

void RenderModule::drawSpriteRot(float x, float y, float width, float height, float rot)
{
	Texture * useTexture = drawTexture;

	if (useTexture == NULL)
		useTexture = whiteTexture;

	Renderable renderable(sprite, useTexture, NULL);
	renderable.setMaterial(&material);
	Model model(renderable);

	model.getTransform().translate(Vector3(-0.5f, -0.5f, 0));
	model.getTransform().rotate(Vector3(0, 0, 1), rot);
	model.getTransform().translate(Vector3(+0.5f, +0.5f, 0));
	model.getTransform().scale(Vector3(width, height, 1));
	model.getTransform().translate(Vector3(x, y, 0));

	hudLayer->intermediate(&model);
}


void RenderModule::drawShinySprite(float x, float y, float width, float height, float shininess)
{
	Texture * useTexture = drawTexture;

	if (useTexture == NULL)
		useTexture = whiteTexture;

	// shine
	{
		
		float add = 20;
		Renderable renderable(sprite, shineTexture, NULL);
		renderable.setMaterial(&material);
		renderable.getMaterial().blend = Color(0.5f, 0.5f, 0.5f, 1);
		Model model(renderable);


		model.getTransform().identity();
		model.getTransform().translate(Vector3(-0.5f, -0.5f, 0));
		model.getTransform().rotate(Vector3(0, 0, 1), timer.now() * 0.3f);
		model.getTransform().translate(Vector3(+0.5f, +0.5f, 0));
		model.getTransform().scale(Vector3(width + add * 2, height + add * 2, 1));
		model.getTransform().translate(Vector3(x - add, y - add, 0));

		hudLayerAdd->intermediate(&model);

		add = 15;
		model.getTransform().identity();
		model.getTransform().translate(Vector3(-0.5f, -0.5f, 0));
		model.getTransform().rotate(Vector3(0, 0, 1), -timer.now() * 0.2f);
		model.getTransform().translate(Vector3(+0.5f, +0.5f, 0));
		model.getTransform().scale(Vector3(width + add * 2, height + add * 2, 1));
		model.getTransform().translate(Vector3(x - add, y - add, 0));

		hudLayerAdd->intermediate(&model);

		add = 35;
		model.getTransform().identity();
		model.getTransform().translate(Vector3(-0.5f, -0.5f, 0));
		model.getTransform().rotate(Vector3(0, 0, 1), timer.now() * 0.1f);
		model.getTransform().translate(Vector3(+0.5f, +0.5f, 0));
		model.getTransform().scale(Vector3(width + add * 2, height + add * 2, 1));
		model.getTransform().translate(Vector3(x - add, y - add, 0));

		hudLayerAdd->intermediate(&model);
	}

	Renderable renderable(sprite, useTexture, NULL);
	renderable.setMaterial(&material);
	Model model(renderable);
	model.getTransform().scale(Vector3(width, height, 1));
	model.getTransform().translate(Vector3(x, y, 0));

	hudLayer->intermediate(&model);
}

void RenderModule::drawShine(float x, float y, float size)
{

		
	float add = size;
	Renderable renderable(sprite, shineTexture, NULL);
	renderable.setMaterial(&material);

	Model model(renderable);


	model.getTransform().identity();
	model.getTransform().translate(Vector3(-0.5f, -0.5f, 0));
	model.getTransform().rotate(Vector3(0, 0, 1), timer.now() * 0.3f);
	model.getTransform().translate(Vector3(+0.5f, +0.5f, 0));
	model.getTransform().scale(Vector3(add * 2, add * 2, 1));
	model.getTransform().translate(Vector3(x - add, y - add, 0));

	hudLayerAdd->intermediate(&model);

	add = size - 5;
	model.getTransform().identity();
	model.getTransform().translate(Vector3(-0.5f, -0.5f, 0));
	model.getTransform().rotate(Vector3(0, 0, 1), timer.now() * 0.2f);
	model.getTransform().translate(Vector3(+0.5f, +0.5f, 0));
	model.getTransform().scale(Vector3(add * 2, add * 2, 1));
	model.getTransform().translate(Vector3(x - add, y - add, 0));

	hudLayerAdd->intermediate(&model);

	add = size + 15;
	model.getTransform().identity();
	model.getTransform().translate(Vector3(-0.5f, -0.5f, 0));
	model.getTransform().rotate(Vector3(0, 0, 1), timer.now() * 0.1f);
	model.getTransform().translate(Vector3(+0.5f, +0.5f, 0));
	model.getTransform().scale(Vector3(add * 2, add * 2, 1));
	model.getTransform().translate(Vector3(x - add, y - add, 0));

	hudLayerAdd->intermediate(&model);

}

void RenderModule::drawShineExt(const std::string & texture, float x, float y, float size)
{
	float add = size;
	Renderable renderable(sprite, renderer->texture()->get(std::string("textures/") + texture), NULL);
	renderable.setMaterial(&material);

	Model model(renderable);


	model.getTransform().identity();
	model.getTransform().translate(Vector3(-0.5f, -0.5f, 0));
	model.getTransform().rotate(Vector3(0, 0, 1), timer.now() * 0.3f);
	model.getTransform().translate(Vector3(+0.5f, +0.5f, 0));
	model.getTransform().scale(Vector3(add * 2, add * 2, 1));
	model.getTransform().translate(Vector3(x - add, y - add, 0));

	hudLayerAdd->intermediate(&model);

	add = size - 5;
	model.getTransform().identity();
	model.getTransform().translate(Vector3(-0.5f, -0.5f, 0));
	model.getTransform().rotate(Vector3(0, 0, 1), timer.now() * 0.2f);
	model.getTransform().translate(Vector3(+0.5f, +0.5f, 0));
	model.getTransform().scale(Vector3(add * 2, add * 2, 1));
	model.getTransform().translate(Vector3(x - add, y - add, 0));

	hudLayerAdd->intermediate(&model);

	add = size + 15;
	model.getTransform().identity();
	model.getTransform().translate(Vector3(-0.5f, -0.5f, 0));
	model.getTransform().rotate(Vector3(0, 0, 1), timer.now() * 0.1f);
	model.getTransform().translate(Vector3(+0.5f, +0.5f, 0));
	model.getTransform().scale(Vector3(add * 2, add * 2, 1));
	model.getTransform().translate(Vector3(x - add, y - add, 0));

	hudLayerAdd->intermediate(&model);
}

void RenderModule::drawSpriteForceScreen(float x, float y, float width, float height)
{
	Texture * useTexture = drawTexture;

	if (useTexture == NULL)
		useTexture = whiteTexture;

	Renderable renderable(sprite, useTexture, NULL);
	renderable.setMaterial(&material);
	Model model(renderable);
	model.getTransform().scale(Vector3(width, height, 1));
	model.getTransform().translate(Vector3(x, y, 0));

	renderer->redirectFrameBuffer(NULL);
	renderer->setRenderTarget(NULL);
	postTransitionLayer->intermediate(&model);
}

void RenderModule::drawLine(float x1, float y1, float x2, float y2)
{
	Renderable renderable(sprite, renderer->texture()->get("textures/line.png"), NULL);
	renderable.setMaterial(&material);

	Model model(renderable);

	float angle = atan2(y2 - y1, x2 - x1);
	float length = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));


	model.getTransform().identity();
	model.getTransform().translate(Vector3(0, -0.5f, 0));
	//model.getTransform().rotate(Vector3(0, 0, 1), angle);
	//model.getTransform().translate(Vector3(+0.5f, +0.5f, 0));
	model.getTransform().scale(Vector3(length, 7, 1));
	model.getTransform().rotate(Vector3(0, 0, 1), angle);

	model.getTransform().translate(Vector3(x1, y1, 0));

	hudLayer->intermediate(&model);
}

void RenderModule::setBlendColor(const Color & color)
{
	material.blend = color;
}

void RenderModule::setGhostMode(bool state)
{
	ghostMode = state;

	if (state == false)
	{
		ghostLayer->setVisible(false);

		hudLife->kill();
		shaderState.state[0] = 0.0f;
	}
	else
	{
		ghostLayer->setVisible(true);

		hudLife->reset();
		shaderState.state[0] = 1.0f;
	}
}

void RenderModule::setFog(bool state)
{
	shaderState.state[8] = static_cast < float > (state);
}

void RenderModule::setShadows(bool state)
{
	shaderState.state[9] = static_cast < float > (state);
}

void RenderModule::setSSAO(bool state)
{
	shaderState.state[10] = static_cast < float > (state);
}

void RenderModule::setBoundingVolumesVisibility(bool state)
{
	if (wireframeLayer)
		wireframeLayer->setVisible(state);
}

const bool RenderModule::getFog() const
{
	return shaderState.state[8] > 0.0f;
}

const bool RenderModule::getShadows() const
{
	return shaderState.state[9] > 0.0f;
}

const bool RenderModule::getSSAO() const
{
	return shaderState.state[10] > 0.0f;
}

const float RenderModule::getVisualScale() const
{
	return visualBuffer.scale;
}

const bool RenderModule::getBoundingVolumesVisibility() const
{
	if (wireframeLayer) return wireframeLayer->isVisible();

	return false;
}

float RenderModule::getHeightFromTerrain(const Vector2 & position)
{
	return heightmapTerrain.sample(position.x, position.y);
}
bool RenderModule::addParticleType(std::string type, std::string texture)
{
	Renderable rend;
	typeRenderables.push_back(rend);
	int id = (int)typeRenderables.size()-1;
	if(!effectSystem->createType(type, renderer->shader()->get("shaders/cpu_particle.vs")))
		return false;

	Mesh * mesh = NULL;
	mesh = effectSystem->getParticleMesh(type);                                     
	
	if( mesh == NULL )
		return false;

	typeRenderables[id] = Renderable(mesh, renderer->texture()->get("textures/" + texture), NULL);

	effectLayer->add( &typeRenderables[id] );

	return true;
}
void RenderModule::initIntermediateDrawing()
{
	vertexShader2D = renderer->shader()->get("shaders/font.vs");
	pixelShader2D = renderer->shader()->get("shaders/font.ps");

	fontConstantBuffer = New ConstantBuffer(renderer, 256);

	float offset[4] = {0};

	fontConstantBuffer->set(offset, sizeof(offset));
	vertexShader2D->setConstantBuffer(2, fontConstantBuffer);

	pixelShaderTransition = renderer->shader()->get("shaders/transition.ps");
	pixelShaderTransition->bindTexture(1, distorsion);

	postTransitionLayer = renderer->createLayer();
	Viewport * vp = postTransitionLayer->addViewport(&hudCamera, NULL);

	vp->getEffect()->blendModeAlpha();
	vp->setShaders(vertexShader2D, NULL, pixelShaderTransition);


	sprite = renderer->mesh()->create();

	sprite->addIndex(0);
	sprite->addIndex(1);
	sprite->addIndex(3);
	sprite->addIndex(3);
	sprite->addIndex(1);
	sprite->addIndex(2);

	MeshStream * stream = sprite->createSingleStream("Glyph", sizeof(Vertex));
	
	Vertex v[4];
	v[0].position = Vector2(0, 0);
	v[0].uv = v[0].position;
	v[1].position = Vector2(1, 0);
	v[1].uv = v[1].position;
	v[2].position = Vector2(1, 1);
	v[2].uv = v[2].position;
	v[3].position = Vector2(0, 1);
	v[3].uv = v[3].position;

	stream->append((char*)v, 4);

	sprite->submit(vertexShader2D);
	sprite->setTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	material.blend = Color(1, 1, 1, 1);

	whiteTexture = renderer->texture()->get("textures/white.png");
}

std::string RenderModule::getDebugText() const
{
	std::string out;

	std::stringstream format;
	format << "Animations: " << particleLayer->getCount() << std::endl;
	format << "Links: " << (links.size() + 1) << std::endl;
	format << "Visible Animations: " << particleLayer->getVisibleCount() << std::endl;
	format << "Models: " << characterLayer->getCount() << std::endl;
	format << "Render Stages: " << renderer->getStageCount() << std::endl;
	format << "Render Performance: " << renderer->queryPerformance() << std::endl;

	out = format.str();

	return out;
}