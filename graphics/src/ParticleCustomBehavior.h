#ifndef PARTIClE_CUSTOM_BEHAVIOR_H
#define PARTIClE_CUSTOM_BEHAVIOR_H

#include "Common.h"

class ParticleCustomBehavior : public ParticleBehavior
{


public:
	ParticleCustomBehavior();
	~ParticleCustomBehavior();

	void spawn(Particle & particle) const;
	void process(Particle & particle) const;

	void setNormal(const Vector3 & n);

public:

	float pi;
	float radx;

	Vector3 dir;

	Vector3 normalW;

	Color startColor;
	Color middleColor;
	Color endColor;

	float randAngleX;
	float randAngleY;
	float randAngleZ;

	float intensityDir;

	float spinMax;
	float spinMin;

	float startScale;
	float endScale;

	float startSpeed;
	float middleSpeed;
	float endSpeed;

	float speedPath;
	float colorPath;

	float gravity;

	float distorsion;

	float displacementPos;
	float lifeAffectRate;

	char type_name[64];
	//std::string typeName;

};

#endif