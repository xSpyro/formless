
#include "EventList.h"


void EventList::add(const Event & ev)
{
	queue.push_back(ev);
}

void EventList::update()
{
	// then add any queued events if any
	events.insert(events.end(), queue.begin(), queue.end());

	queue.clear();

	// loop all and remove if needed
	for (Events::iterator i = events.begin(); i != events.end(); )
	{
		Event & ev = *i;

		ev.duration -= 1.0f / (1 + ev.level);

		if (ev.duration <= 0)
		{
			i = events.erase(i);
		}
		else
		{
			++i;
		}
	}

	// now sort our events based on level and duration
	events.sort();
}

const EventList::Events & EventList::getEvents() const
{
	return events;
}