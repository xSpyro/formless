
#ifndef SCENE_PLAYER_BUFFER
#define SCENE_PLAYER_BUFFER

#include "Common.h"

struct ScenePlayerBuffer
{
	struct Entity
	{
		Vector3 position;
		float team;
	};

	Entity entity[8];
	float count;

	ScenePlayerBuffer()
		: count(0.0f)
	{
	}
};

#endif