
#ifndef SHAPE_FRAME_H
#define SHAPE_FRAME_H

#include "Common.h"

struct ShapeAnimationKey;

struct ShapeResult;

struct ShapeFrame
{
	ShapeFrame() : src(NULL), dst(NULL), delta(0), linked(false)
	{
	}

	struct Desc
	{
		Vector3 offset;
		float scale;
		float angle;
		float time;
		int * linkOffset;

		Desc() : scale(1), angle(0), time(0), linkOffset(NULL) { }
	};

	//bool createLerpedResult(ShapeResult * out, const Vector3 & offset = 0, float scale = 1.0f, float angle = 0.0f, float time = 0.0f);
	bool createLerpedResult(ShapeResult * out, Desc * desc = NULL);

	bool createLerpedAnimationKey(ShapeAnimationKey * out, float time = 0.0f); 

	//void setLinkFrame(
	void enableLink(bool state);
	void setLinkPoint(const Vector3 & position);

	const ShapeAnimationKey* src;
	const ShapeAnimationKey* dst;

	float delta;

private:

	//void firstLink(const Vector3 & position, ShapeResult * out, int index, int count);
	void appendLink(const Vector3 & position, ShapeResult * out, int index, int count, float time);

	void bindAllTo(const Vector3 & position, ShapeResult * out, int index);

private:

	bool linked;
	Vector3 linkPoint;
};

#endif