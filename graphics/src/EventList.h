
#ifndef EVENT_LIST
#define EVENT_LIST

#include "Common.h"

class EventList
{
public:

	EventList()
	{

	}

	struct Event
	{
		int team[2];
		std::string name[2];

		std::string cause;
		int level;
		float duration;

		bool operator < (const Event & rhs) const
		{
			return duration < rhs.duration;
		}
	};

	typedef std::list < Event > Events;

	void add(const Event & ev);

	void update();

	const Events & getEvents() const;

private:

	Events events;

	Events queue;
};

#endif